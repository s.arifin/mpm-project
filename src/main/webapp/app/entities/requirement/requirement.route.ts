import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RequirementComponent } from './requirement.component';
import { RequirementEditComponent } from './requirement-edit.component';
import { RequirementPopupComponent } from './requirement-dialog.component';

@Injectable()
export class RequirementResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requirementRoute: Routes = [
    {
        path: 'requirement',
        component: RequirementComponent,
        resolve: {
            'pagingParams': RequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const requirementPopupRoute: Routes = [
    {
        path: 'requirement-new',
        component: RequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'requirement/:id/edit',
        component: RequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'requirement-popup-new',
        component: RequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'requirement/:id/popup-edit',
        component: RequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
