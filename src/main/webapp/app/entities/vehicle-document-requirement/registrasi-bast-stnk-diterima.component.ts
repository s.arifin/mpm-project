import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

// import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
// import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement/vehicle-document-requirement.service';
// import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';
// import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import * as BaseConstant from '../../shared/constants/base.constants';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement/vehicle-document-requirement.model';

@Component({
    selector: 'jhi-registration-bast-stnk-diterima',
    templateUrl: './registrasi-bast-stnk-diterima.component.html'
})
export class RegistrationBASTSTNKDiterimaComponent implements OnInit {

    currentAccount: any;
    // unitDeliverable: UnitDeliverable;
    // unitDeliverableParameters: UnitDeliverableParameters;
    // unitDeliverableUpdate: UnitDeliverable;
    // unitDeliverables: UnitDeliverable[];
    // dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    TotalData: VehicleDocumentRequirement;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    detailModal: boolean;
    printModal: boolean;
    revisiModal: boolean;
    vehicleDocumentRequirementbegbalupdate: VehicleDocumentRequirement;
    vehicleDocumentRequirementbegbal: VehicleDocumentRequirement[];

    // statusTypes: StatusType[];
    salesmans: Salesman[];

    // bastTo: string;
    // bastKembali: any;
    // bastSalesKembali: boolean;
    // selected: VehicleDocumentRequirement[];
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    filtered: any;

    constructor(
        // protected unitDeliverableService: UnitDeliverableService,
        // protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.printModal = false;
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        // this.bastTo = 'salesman';
        // this.bastKembali = 0;
        // this.unitDeliverable = new UnitDeliverable();
        // this.unitDeliverableUpdate = new UnitDeliverable();
        // this.unitDeliverables = new Array<UnitDeliverable>();
        this.isFiltered = false;
    }

    loadAll() {
        console.log('is filter load = ', this.isFiltered);
        this.loadingService.loadingStart();
        this.vehicleDocumentRequirementService.queryC({
            query: 'idDelType:103|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 1000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers), console.log('sukses', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal', res) }
        );
        console.log('vdr', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }

        if (this.isFiltered === true) {
        }
    }

    private loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator: this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales', res.json);
                this.salesmans = res.json;
            }
        )
    }
    filter() {
        // this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/VEHICLE-DOCUMENT-REQUIREMENT/REGISTRATION/BAST/STNK/begbal'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'ASC' : 'DESC')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.filtered = '';
        this.isFiltered = false;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal']);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadKorsal();
        });
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbal = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbal);
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
    }

    updateTanggalTerima(noka: any, deltipe: any) {
        this.vehicleDocumentRequirementService.queryUpdateTanggal({
            idframe: noka,
            iddeltype: deltipe
        }).subscribe(
            (res) => {
                console.log('muncul dong', res)
            })
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
        });
    }

    showDetail(idreq: any) {
        this.vehicleDocumentRequirementService.find(idreq).subscribe(
            (response) => {
                this.vehicleDocumentRequirementbegbalupdate = response;
                console.log('cekkk ---->', this.vehicleDocumentRequirementbegbal);
            });
        this.detailModal = true;
    }

    showPrint(id: any) {
        this.printModal = true;
    }
    print(noka: string, deltipe: any) {
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/stnk_internal_begbal/pdf', { idframe: noka });
        console.log('print', noka)
        this.loadAll();
    }

    doPrint() {
        this.printModal = false;
    }

    listUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                resolve();
            }
        )
    }
    showRevisi(id: any) {
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }
}
