import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model'
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service'
import { OrderItem, OrderItemService } from '../order-item';
import { PaymentApplication, PaymentApplicationService } from '../payment-application';
import { Facility, FacilityService } from '../facility';
import { Feature, FeatureService } from '../feature';
import { CommonUtilService, ToasterService } from '../../shared';
import { Product, ProductService } from '../product';
import { Motor, MotorService} from '../motor';
import { ResponseWrapper, Principal } from '../../shared';
import { Person, PersonService } from '../person';
import { ReligionType, ReligionTypeService } from '../religion-type';
import { WorkType, WorkTypeService } from '../work-type';
import { SalesUnitRequirement, SalesUnitRequirementService} from '../sales-unit-requirement';
import { VehicleSalesOrderService} from '../vehicle-sales-order'
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../shared-component';

import * as _ from 'lodash';
import { LoadingService } from '../../layouts';
import { Organization } from '../organization';

@Component({
    selector: 'jhi-registration-isi-nama-edit',
    templateUrl: './registration-isi-nama-edit.component.html'
})
export class RegistrationIsiNamaEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    public isSameWithCustomer: Boolean = false;
    idRequirement: string;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    isSaving: boolean;
    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    personalCustomers: PersonalCustomer[];
    customer: PersonalCustomer;
    orderitems: OrderItem[];
    salesUnitRequirement: SalesUnitRequirement;

    paymentapplications: PaymentApplication[];
    facilities: Facility[];
    features: Feature[];
    products: Product[];
    motors: Motor[];

    vehicleDocumentRequirements: VehicleDocumentRequirement[];

    hobies: any;
    homeStatues: any;
    expenditures: any;
    jobs: any;
    educations: any;
    hps: any;
    infos: any;
    brandOwners: any;
    vehicleTypes: any;
    vehicleUsers: any;
    buyFors: any;
    religions: any;
    idOrder: any;
    public disable: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected orderItemService: OrderItemService,
        protected personalCustomerService: PersonalCustomerService,
        protected paymentApplicationService: PaymentApplicationService,
        protected facilityService: FacilityService,
        protected featureService: FeatureService,
        protected productService: ProductService,
        protected principal: Principal,
        protected motorService: MotorService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected religionTypeService: ReligionTypeService,
        protected workTypeService: WorkTypeService,
        protected toasterService: ToasterService,
        protected commonUtilService: CommonUtilService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected loadingService: LoadingService,
        protected personService: PersonService
    ) {
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
        this.personalCustomer.idCustomer = null;
        this.personalCustomers = new Array<PersonalCustomer>();
        this.salesUnitRequirement = new SalesUnitRequirement;
        this.getReligionList();
    }

    getReligionList(): void {
        this.religionTypeService
            .query()
            .subscribe(
                (res: ResponseWrapper) => {this.religions = res.json},
                (res: ResponseWrapper) => this.onError(res.json)
            )
        this.workTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.jobs = res.json; });
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequirement = params['id'];
                this.load(params['id']);
            }
            if (params['idOrder']) {
                this.idOrder = params['idOrder'];
            }
        });
        this.isSaving = false;
        // this.orderItemService.query()
        //     .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentApplicationService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentapplications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({page: 0, size: 10000, sort: ['idProduct', 'asc']})
                .subscribe((res: ResponseWrapper) => {this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.productService.query()
        //     .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.featureService.query({page: 0, size: 10000, sort : ['idFeature', 'asc']})
        //     .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.vehicleDocumentRequirementService.find(id).subscribe((requirement) => {
            console.log('requirement', requirement);
            this.vehicleDocumentRequirement = requirement;
            this.vehicleSalesOrderService.findSurByOrderId(this.idOrder, this.principal.getIdInternal()).subscribe((sur) => {
                this.salesUnitRequirement = sur;
                const custId: string = this.salesUnitRequirement.customerId;
                if (this.vehicleDocumentRequirement.personOwner != null && this.salesUnitRequirement.idReqTyp !== 103) {
                const personOwner: string = this.vehicleDocumentRequirement.personOwner.idParty;
                if (custId !== null && custId !== undefined) {
                        this.personalCustomerService.find(custId).subscribe(
                            (res: PersonalCustomer) => {
                                this.customer = res;
                                console.log('res.cariparty sur owner : ', res.partyId);
                                console.log('cariparty personowner : ', personOwner);
                                if (res.partyId === personOwner) {
                                    this.disable = true;
                                    this.isSameWithCustomer = true;
                                    console.log('disable :', this.disable)
                                }
                            },
                            (err) => {
                                this.commonUtilService.showError(err);
                            }
                        )
                    } else if (custId !== null && custId !== undefined) {
                        this.personalCustomerService.find(custId).subscribe(
                            (res: PersonalCustomer) => {
                                this.customer = res;
                                if (res.partyId !== personOwner) {
                                    this.disable = false;
                                    this.isSameWithCustomer = false;
                                    console.log('disable :', this.disable)
                                }
                            },
                            (err) => {
                                this.commonUtilService.showError(err);
                            }
                        )
                }
            } else {
                const organizationOwner: string = this.vehicleDocumentRequirement.organizationOwner.idParty;
                if (custId !== null && custId !== undefined) {
                    this.organizationCustomerService.find(custId).subscribe(
                        (res: OrganizationCustomer) => {
                            this.customer = res;
                            if (res.partyId === organizationOwner) {
                                this.disable = true;
                                this.isSameWithCustomer = true;
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
                } else if (custId !== null && custId !== undefined) {
                    this.organizationCustomerService.find(custId).subscribe(
                        (res: OrganizationCustomer) => {
                            this.customer = res;
                            if (res.partyId !== organizationOwner) {
                                this.disable = false;
                                this.isSameWithCustomer = false;
                            }
                        },
                        (err) => {
                            this.commonUtilService.showError(err);
                        }
                    )
            }
            }
            })
        });
    }

    public selectMotor(isSelect?: boolean) {
        const productId = this.vehicleDocumentRequirement.vehicle.productId;
        if (productId !== null && productId !== undefined) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === productId.toLowerCase();
            });
            console.log('selectedProduct', selectedProduct);
            this.features = selectedProduct.features;
        }
    }

    previousState() {
        this.router.navigate(['/vehicle-document-requirement/registration/isi-nama']);
    }

    public onChangeSameWithCustomer(e): void {
        const checked: boolean = e.checked;
        if (checked) {
            this.disable = true;
             this.personalCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                 (res) => {
                     let newPerson: Person;
                     newPerson = res.person;

                     if (newPerson.dob != null) {
                         newPerson.dob = new Date(newPerson.dob);
                     }

                     this.salesUnitRequirement.personOwner = newPerson;
                    this.vehicleDocumentRequirement.personOwner = newPerson;
                 }
             )
        } else {
            this.salesUnitRequirement.personOwner = new Person();
            this.vehicleDocumentRequirement.personOwner = new Person();
             this.disable = false;
        }
     }

     public onChangeSameWithCustomerGc(e): void {
        const checked: boolean = e.checked;
        if (checked) {
            this.disable = true;
             this.organizationCustomerService.find(this.salesUnitRequirement.customerId).subscribe(
                 (res) => {
                     let newOrganization: Organization;
                     newOrganization = res.organization;

                     if (newOrganization.dob != null) {
                        newOrganization.dob = new Date(newOrganization.dob);
                     }

                     this.salesUnitRequirement.organizationOwner = newOrganization;
                    this.vehicleDocumentRequirement.organizationOwner = newOrganization;
                 }
             )
        } else {
            this.salesUnitRequirement.organizationOwner = new Organization();
            this.vehicleDocumentRequirement.organizationOwner = new Organization();
             this.disable = false;
        }
     }

    save() {
        console.log('this.vehicleDocumentRequirement', this.vehicleDocumentRequirement);
        this.isSaving = true;
        if (this.vehicleDocumentRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.queryUpdate(this.vehicleDocumentRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.create(this.vehicleDocumentRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<VehicleDocumentRequirement>) {
        result.subscribe((res: VehicleDocumentRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: VehicleDocumentRequirement) {
        // this.eventManager.broadcast({ name: 'requirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    verification() {
        console.log('masuk proses verification');
        this.vehicleDocumentRequirementService.process(
            {command: 'doTaskCheckVDR', idRequirement: this.idRequirement})
            .subscribe((rslt) => {
                this.toaster.showToaster('info', 'Valid', 'Data Verified');
            }, (err) => {
                this.toaster.showToaster('error', 'Error', err.message);
            });
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    trackPaymentApplicationById(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackRequirementById(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackProductById(index: string, item: Product) {
        return item.idProduct;
    }

    trackMotorById(index: string, item: Motor) {
        return item.idProduct;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    getVillage(event): void {
        console.log('event ==>', event);
    }

    getDistrict(event): void {
        console.log('event ==>', event);
    }

    getProvince(event): void {
        console.log('event ==>', event);
    }

    getCity(event): void {
        console.log('event ==>', event);
    }
}
