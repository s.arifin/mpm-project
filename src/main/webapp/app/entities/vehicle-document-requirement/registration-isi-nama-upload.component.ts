import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { CustomVehicleDocumentRequirement } from './custom-vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { StatusType, StatusTypeService } from '../status-type';
import { Motor, MotorService } from '../motor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-registration-isi-nama-upload',
    templateUrl: './registration-isi-nama-upload.component.html'
})
export class RegistrationIsiNamaUploadComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    vehicleDocumentRequirementsCancel: VehicleDocumentRequirement[];
    customVehicleDocumentRequirement: CustomVehicleDocumentRequirement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    downloadModal: boolean;

    statusTypes: StatusType[];
    motors: Motor[];

    udstk: string;
    cddb: string;

    constructor(
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected statusTypeService: StatusTypeService,
        protected motorService: MotorService
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.customVehicleDocumentRequirement = new Array<CustomVehicleDocumentRequirement>();
        this.downloadModal = false;
        this.udstk = '';
        this.cddb = '';
    }

    loadAll() {

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/registration/isi-nama', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/registration/isi-nama', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInRequirements();
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('requirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'Requirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'requirementListModification',
                    content: 'Deleted an requirement'
                    });
                });
            }
        });
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    showDownload(data: any) {
        this.downloadModal = true;
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirement = data;
    }

    // doDownload() {
    //     this.vehicleDocumentRequirementService.executeProcess(101, null, this.vehicleDocumentRequirement).subscribe(
    //         (value) => {
    //             console.log('download-this: ', value);
    //             this.udstk += this.vehicleDocumentRequirement.vehicle.idFrame + ';';
    //             this.udstk += 'idproduct;';
    //             this.udstk += this.vehicleDocumentRequirement.vehicle.idMachine + ';';
    //             this.udstk += this.vehicleDocumentRequirement.personOwner.name + ';';
    //             this.udstk += this.vehicleDocumentRequirement.personOwner.postalAddress.address1 + ';';
    //             this.udstk += this.vehicleDocumentRequirement.personOwner.postalAddress.village + ';';
    //             this.udstk += this.vehicleDocumentRequirement.personOwner.postalAddress.district + ';';
    //             this.udstk += this.vehicleDocumentRequirement.personOwner.postalAddress.city + ';';
    //             this.udstk += 'kodePost;';
    //             this.udstk += this.vehicleDocumentRequirement.personOwner.postalAddress.province + ';';
    //             this.udstk += 'saleType;';
    //             this.udstk += 'internal;';
    //             this.udstk += 'KodeFinCoy;';
    //             this.udstk += 'DP;';
    //             this.udstk += 'Tenor;';
    //             this.udstk += 'Cicilan;';
    //             this.udstk += 'ID POS;';

    //             let filename = 'UDSTK.udstk';
    //             let filetype = 'text/plain';

    //             let a = document.createElement('a');
    //             let dataURI = 'data:' + filetype + ';base64,' + btoa(this.udstk);
    //             a.href = dataURI;

    //             a['download'] = filename;
    //             let e = document.createEvent('MouseEvents');
    //             // Use of deprecated function to satisfy TypeScript.
    //             e.initMouseEvent('click', true, false, document.defaultView,
    //                 0, 0, 0, 0, 0,
    //                 true, false, false, false,
    //                 0, null);
    //             // e.initMouseEvent(type, canBubble, cancelable, view,
    //             //              detail, screenX, screenY, clientX, clientY,
    //             //              ctrlKey, altKey, shiftKey, metaKey,
    //             //              button, relatedTarget);
    //             a.dispatchEvent(e);
    //             // a.removeNode();

    //             // var blob = new Blob([this.udstk], { type: 'text/plain' });
    //             // var url= window.URL.createObjectURL(blob);
    //             // window.open(url);

    //             this.cddb += 'idproduct;';
    //             this.cddb += this.vehicleDocumentRequirement.vehicle.idMachine + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.personalIdNumber + ';';
    //             this.cddb += 'idcustomer;';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.gender + ';';
    //             this.cddb += 'tglLahir;';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.postalAddress.address1 + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.postalAddress.village + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.postalAddress.district + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.postalAddress.city + ';';
    //             this.cddb += 'kodePosSurat;';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.postalAddress.province + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.religionTypeId + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.workTypeId + ';';
    //             this.cddb += 'Pengeluaran;';
    //             this.cddb += 'Pendidikan;';
    //             this.cddb += 'NamaPenanggungJawab;';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.cellPhone1 + ';';
    //             this.cddb += this.vehicleDocumentRequirement.personOwner.cellPhone2 + ';';
    //             this.cddb += 'BersediaDihubungi;';
    //             this.cddb += 'MotorYgDimilikiSkr;';
    //             this.cddb += 'JenisMotorYgDimilikiSkr;';
    //             this.cddb += 'DigunakanUntuk;';
    //             this.cddb += 'Ygmenggunakan;';
    //             this.cddb += 'KodeSales;';
    //             this.cddb += 'protectedmail;';
    //             this.cddb += 'StatusRumah;';
    //             this.cddb += 'StatusNoHP;';
    //             this.cddb += 'Facebook;';
    //             this.cddb += 'Twitter;';
    //             this.cddb += 'Instagram;';
    //             this.cddb += 'Youtube(@gmail.com);';
    //             this.cddb += 'Hobi;';
    //             this.cddb += 'Keterangan;';
    //             this.cddb += 'Referal ID;';
    //             this.cddb += 'KodeFLPKoordinator;';
    //             this.cddb += 'seriMesinRO;';
    //             this.cddb += 'NoMesinRO;';
    //             this.cddb += 'TglSPG;';
    //             this.cddb += 'NoKK;';

    //             filename = 'CDDB.cddb';
    //             filetype = 'text/plain';

    //             a = document.createElement('a');
    //             dataURI = 'data:' + filetype + ';base64,' + btoa(this.cddb);
    //             a.href = dataURI;

    //             a['download'] = filename;
    //             e = document.createEvent('MouseEvents');
    //             // Use of deprecated function to satisfy TypeScript.
    //             e.initMouseEvent('click', true, false, document.defaultView,
    //                 0, 0, 0, 0, 0,
    //                 true, false, false, false,
    //                 0, null);
    //             a.dispatchEvent(e);
    //         },
    //         (err) => console.log(err),
    //         () => {
    //             setTimeout(() => {
    //                 this.loadAll();
    //                 this.downloadModal = false;
    //                 this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //             }, 1000);
    //         }
    //     );
    // }

    processFaktur(idRequirement: string) {
        this.vehicleDocumentRequirement.idRequirement = idRequirement;
        console.log('data', this.vehicleDocumentRequirement);

        this.vehicleDocumentRequirementService.executeProcess(102, null, this.vehicleDocumentRequirement).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }
}
