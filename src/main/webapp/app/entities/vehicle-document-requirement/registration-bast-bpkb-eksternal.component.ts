import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
import { StatusType, StatusTypeService } from '../status-type';
import { Internal, InternalService } from '../internal';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-registration-bast-bpkb-eksternal',
    templateUrl: './registration-bast-bpkb-eksternal.component.html'
})
export class RegistrationBASTBPKBEksternalComponent implements OnInit, OnDestroy {

    currentAccount: any;
    unitDeliverable: UnitDeliverable;
    unitDeliverables: UnitDeliverable[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    detailModal: boolean;
    printModal: boolean;
    revisiModal: boolean;

    statusTypes: StatusType[];
    internals: Internal[];

    constructor(
        protected unitDeliverableService: UnitDeliverableService,
        protected statusTypeService: StatusTypeService,
        protected internalService: InternalService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.unitDeliverable = new UnitDeliverable();
        this.unitDeliverables = new Array<UnitDeliverable>();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.unitDeliverableService.search({
                idDeliverableType: UnitDeliverableConstant.BAST_BPKB,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.unitDeliverableService.query({
            idDeliverableType: UnitDeliverableConstant.BAST_BPKB,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/unit-deliverable'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/unit-deliverable', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/unit-deliverable', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInUnitDeliverables();

        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitDeliverable) {
        return item.idDeliverable;
    }

    registerChangeInUnitDeliverables() {
        this.eventSubscriber = this.eventManager.subscribe('unitDeliverableListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idDeliverable') {
            result.push('idDeliverable');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDeliverables = data;
        if (data.length === 0) {
            this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        }
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.unitDeliverableService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.unitDeliverableService.update(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.unitDeliverableService.update(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: UnitDeliverable) {
        this.toasterService.showToaster('info', 'Unit Deliverable Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitDeliverableService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'unitDeliverableListModification',
                    content: 'Deleted an unitDeliverable'
                    });
                });
            }
        });
    }

    showDetail(id: any) {
        // this.detail
        this.detailModal = true;
    }

    showPrint(id: any) {
        // this.detail
        this.unitDeliverableService.find(id).subscribe((response) => {
            this.unitDeliverable = response;
        })
        this.printModal = true;
    }

    doPrint() {
        this.unitDeliverableService.update(this.unitDeliverable).subscribe((response) => {
            this.loadAll();
        });
        this.printModal = false;
    }

    showRevisi(id: any) {
        // this.detail
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    print(idframe: string) {
        this.toasterService.showToaster('Infor', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_bpkb2/pdf', {noka: idframe});
    }
}
