import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VehicleDocumentRequirementComponent } from './vehicle-document-requirement.component';
import { VehicleDocumentRequirementEditComponent } from './vehicle-document-requirement-edit.component';
import { VehicleDocumentRequirementLovPopupComponent } from './vehicle-document-requirement-as-lov.component';
import { VehicleDocumentRequirementPopupComponent } from './vehicle-document-requirement-dialog.component';

import { RegistrationDataTransaksiComponent } from './registration-data-transaksi.component';
import { RegistrationIsiNamaComponent } from './registration-isi-nama.component';
import { RegistrationIsiNamaDetailComponent } from './registration-isi-nama-detail.component';
import { RegistrationIsiNamaEditComponent } from './registration-isi-nama-edit.component';
import { RegistrationIsiNamaUploadComponent } from './registration-isi-nama-upload.component';
import { RegistrationPenerimaanFakturComponent } from './registration-penerimaan-faktur.component';
import { RegistrationPenerimaanBPKBSTNKComponent } from './registration-penerimaan-bpkb-stnk.component';
import { RegistrationPenerimaanBPKBSTNKListRefComponent } from './registration-penerimaan-bpkb-stnk-list-ref.component';
import { RegistrationPenerimaanBPKBSTNKInputComponent } from './registration-penerimaan-bpkb-stnk-input.component';
import { RegistrationPengurusanBiroJasaComponent } from './registration-pengurusan-biro-jasa.component';
import { RegistrationBASTOffTheRoadComponent } from './registration-bast-off-the-road.component';
import { RegistrationBASTSTNKInternalComponent } from './registration-bast-stnk-internal.component';
import { RegistrationBASTSTNKBegbalComponent } from './registration-bast-stnk-begbal.component';
import { RegistrationBASTBPKBBegbalComponent } from './registration-bast-bpkb-begbal.component';
import { RegistrationBASTSTNKEksternalComponent } from './registration-bast-stnk-eksternal.component';
import { RegistrationBASTBPKBInternalComponent } from './registration-bast-bpkb-internal.component';
import { RegistrationBASTBPKBEksternalComponent } from './registration-bast-bpkb-eksternal.component';
import { RegistrationPengurusanLuarWilayahReceiveComponent } from './registration-pengurusan-luar-wilayah-receive.component';
import { RegistrationPengurusanLuarWilayahRequestComponent } from './registration-pengurusan-luar-wilayah-request.component';
import { RegistrationPenerimaanFakturUploadComponent } from './registration-penerimaan-faktur-upload.component';
import { UnitDeliverableResolvePagingParams } from '../shared-component/services/share-paging-params';
import { VehicleDocumentRequirementSalesComponent } from './registrasi-sales/vehicle-document-requirement-sales.component';
import { VehicleDocumentRequirementSalesViewComponent } from './registrasi-sales/vehicle-document-requirement-sales-view.component';
import { RegistrationBASTSTNKDiterimaComponent  } from './registrasi-bast-stnk-diterima.component';
import { RegirasiBastStnkBegbalComponent } from './registration-bast-stnk-begbal-upload.component';
import { RegistrationBASTSTNKlistRefrenceComponent} from './registrasi-bast-stnk-list-refrence.component';
import { RegistrationPenerimaanSTNKInputComponent } from './registrasi-bast-stnk-input-penerimaan-stnk.component'
import { VehicleDocumentRequirementSalesViewBastComponent } from './registrasi-sales/vehicle-document-requirement-sales-viewbast.component';

@Injectable()
export class VehicleDocumentRequirementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vehicleDocumentRequirementRoute: Routes = [
    {
        path: 'vehicle-document-requirement',
        component: VehicleDocumentRequirementComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleDocumentRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/data-transaksi',
        component: RegistrationDataTransaksiComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/isi-nama',
        component: RegistrationIsiNamaComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/isi-nama/upload',
        component: RegistrationIsiNamaUploadComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration-penerimaan-faktur',
        component: RegistrationPenerimaanFakturComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration-penerimaan-faktur-upload',
        component: RegistrationPenerimaanFakturUploadComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/penerimaan/bpkb-stnk',
        component: RegistrationPenerimaanBPKBSTNKComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/penerimaan/bpkb-stnk/list-reference',
        component: RegistrationPenerimaanBPKBSTNKListRefComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/penerimaan/bpkb-stnk/input',
        component: RegistrationPenerimaanBPKBSTNKInputComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/pengurusan-biro-jasa',
        component: RegistrationPengurusanBiroJasaComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/off-the-road',
        component: RegistrationBASTOffTheRoadComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/stnk/internal',
        component: RegistrationBASTSTNKInternalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/stnk/begbal',
        component: RegistrationBASTSTNKBegbalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/bpkb/begbal',
        component: RegistrationBASTBPKBBegbalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'begbal/registration-penerimaan-stnk-upload',
        component: RegirasiBastStnkBegbalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'begbal/registration-list-refrence',
        component: RegistrationBASTSTNKlistRefrenceComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'begbal/registration-input-penerimaan-stnk',
        component: RegistrationPenerimaanSTNKInputComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/stnk/diterima',
        component: RegistrationBASTSTNKDiterimaComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/stnk/eksternal',
        component: RegistrationBASTSTNKEksternalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/bpkb/internal',
        component: RegistrationBASTBPKBInternalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/bast/bpkb/eksternal',
        component: RegistrationBASTBPKBEksternalComponent,
        resolve: {
            'pagingParams': UnitDeliverableResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/pengurusan-luar-wilayah/receive',
        component: RegistrationPengurusanLuarWilayahReceiveComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/pengurusan-luar-wilayah/request',
        component: RegistrationPengurusanLuarWilayahRequestComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration-sales',
        component: VehicleDocumentRequirementSalesComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration-sales/:id',
        component: VehicleDocumentRequirementSalesViewComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration-sales-bast/:id',
        component: VehicleDocumentRequirementSalesViewBastComponent,
        resolve: {
            'pagingParams': VehicleDocumentRequirementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const vehicleDocumentRequirementPopupRoute: Routes = [
    {
        path: 'lov',
        component: VehicleDocumentRequirementLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleDocumentRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'new',
        component: VehicleDocumentRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleDocumentRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: VehicleDocumentRequirementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleDocumentRequirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'popup-new',
        component: VehicleDocumentRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleDocumentRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: ':id/popup-edit',
        component: VehicleDocumentRequirementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehicleDocumentRequirement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'registration/isi-nama/input',
        component: RegistrationIsiNamaEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/isi-nama/edit/:id/:idOrder',
        component: RegistrationIsiNamaEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'registration/isi-nama/detail/:id/:idOrder',
        component: RegistrationIsiNamaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requirement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
