import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-registration-bast-off-the-road',
    templateUrl: './registration-bast-off-the-road.component.html'
})
export class RegistrationBASTOffTheRoadComponent implements OnInit, OnDestroy {
    currentAccount: any;
    unitDeliverable: UnitDeliverable;
    unitDeliverables: UnitDeliverable[];
    unitDeliverableUpdate: UnitDeliverable;
    dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    detailModal: boolean;

    statusTypes: StatusType[];
    salesmans: Salesman[];
    account: Account;
    selected: UnitDeliverable[];

    constructor(
        protected unitDeliverableService: UnitDeliverableService,
        protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.unitDeliverable = new UnitDeliverable();
        this.unitDeliverables = new Array<UnitDeliverable>();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.unitDeliverableService.search({
                idDeliverableType: UnitDeliverableConstant.BAST_FAKTUROFFTHEROAD,
                idStatusType: 44,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.unitDeliverableService.queryCustom({
            idDeliverableType: UnitDeliverableConstant.BAST_FAKTUROFFTHEROAD,
            idStatusType: 44,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/unit-deliverable'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/unit-deliverable', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/unit-deliverable', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInUnitDeliverables();

        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.principal.identity(true).then((account) => {
            this.account = account;
        });
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitDeliverable) {
        return item.idDeliverable;
    }

    registerChangeInUnitDeliverables() {
        this.eventSubscriber = this.eventManager.subscribe('unitDeliverableListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idDeliverable') {
            result.push('idDeliverable');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.unitDeliverables = data;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.unitDeliverableService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.unitDeliverableService.update(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.unitDeliverableService.create(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: UnitDeliverable) {
        this.toasterService.showToaster('info', 'Unit Deliverable Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitDeliverableService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'unitDeliverableListModification',
                    content: 'Deleted an unitDeliverable'
                    });
                });
            }
        });
    }

    showDetailMulti() {
        this.detailModal = true;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    doPrint() {
        this.detailModal = false;
        this.mappingDataPengambil().then(
            () => {
                this.listBAST()
            }
        )
    }

    printMulti(data: UnitDeliverable) {
        console.log('masuk untuk printtt');
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_faktur_offtheroad/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: data.identityNumber, userlogin: this.currentAccount.firstName});
    }

    mappingDataPengambil(): Promise<any> {
        return new Promise(
            (resolve) => {
                const data = this.selected;
                data.forEach (
                        (res) => {
                            res.identityNumber = this.unitDeliverable.identityNumber;
                            res.name = this.unitDeliverable.name;
                        }
                )
                resolve();
            }
        )
    }

    listBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk list update bast', this.selected);
                this.unitDeliverableService.executeListProcess(106, this.principal.getIdInternal(), this.selected)
                .subscribe(
                    (value) => {
                        this.findOne();
                        console.log('thissssss: ', value);
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.unitDeliverables = new Array<UnitDeliverable>();
                        this.unitDeliverable = new UnitDeliverable();
                    }
                )
            resolve();
            }
        )
    }

    findOne(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk fungsi find');
                const idDelType = this.selected[0].idDeliverableType;
                const idreq = this.selected[0].vehicleDocumentRequirement.idRequirement;
                this.unitDeliverableService.queryByidReqandIdDeltype({
                    idDeliverableType : idDelType,
                    idReq : idreq
                }).subscribe(
                    (data) => {
                        console.log('hasil unit deliverable', data.json);
                        this.dataUnitDeliverable = data.json;
                        this.printMulti(this.dataUnitDeliverable);
                        this.selected = new Array<UnitDeliverable>();
                    }
                )
            resolve()
            }
        )
    }
}
