import { BaseEntity } from './../../shared';
import { Vehicle } from '../vehicle/vehicle.model';
import { Person } from '../person';
import { PersonalCustomer } from '../personal-customer/personal-customer.model';
import { SaleType } from '../sale-type';
import { Organization } from '../organization';
import { PriceComponent } from '../price-component';

export class VehicleDocumentRequirement implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public note?: string,
        public statusFaktur?: string,
        public bbn?: number,
        public otherCost?: number,
        public surId?: any,
        public saleType?: SaleType,
        public personOwner?: Person,
        public vehicle?: Vehicle,
        public requestPoliceNumber?: any,
        public idVendor?: any,
        public submissionNo?: string,
        public dateSubmission?: any,
        public fakturATPM?: string,
        public fakturDate?: any,
        public invoicePrint?: any,
        public registrationNumber?: any,
        public personalCustomer?: PersonalCustomer,
        public idMachine?: any,
        public dateFillName?: any,
        public idOrderItem ?: any,
        public idOrder ?: any,
        public ivuNumber?: any,
        public salesName?: any,
        public organizationOwner?: Organization,
        public costHandling?: number,
        public idvehicle?: any,
        public idproduct?: any,
        public idcolor?: any,
        public idmachine?: any,
        public idreq?: any,
        public idframe?: any,
        public yearofass?: any,
        public idinternal?: any,
        public dateSubbmission?: any,
        public tipePembayaran?: any,
        public fname?: any,
        public dtbastsales?: string,
        public dtbastsalesback?: string,
        public idvendor?: any,
        public idDelType?: any,
        public dtdelivery?: any,
        public bastnumber?: any,
        public policenumber?: any,
        public atpmfakturdt?: any,
        public TotalData?: any,
        public idparrol?: any,
        public atpmfaktur?: any,
        public iscompleted?: any,
        public idpersonowner?: any,
        public name?: any,
        public cellphone?: any,
        public identitynumber?: any,
        public bpkbnumber?: string,
        public refnumber?: string,
        public receiptqty?: number,
        public refdt?: any,
        public receiptnominal?: number,
        public dtreceipt?: any,
        public dtreceiptPlat?: any,
        public dtreceiptStnk?: any,
        public dtreceiptNotice?: any,
        public namaLeasing?: any,
        public ktpleasing?: any,
        public biayaP?: any,
        public biayaPengurusan?: number,
        public gantiNotice?: any,
        public totalTerima?: any,
        public nominalTerima?: any,
        public biayaLain?: any,
        public noFaktur?: any,
        public namaLengkap?: any,
        public idvendor2?: any,
        // public other?: any
    ) {
        this.saleType = new SaleType();
        this.personOwner = new Person();
        this.organizationOwner = new Organization();
        this.personalCustomer = new PersonalCustomer();
        this.vehicle = new Vehicle();
    }
}

export class UploadFaktur {
    constructor(
        public noPengajuan?: any,
        public tglPengajuan?: any,
        public noka?: any,
        public nosin?: any,
        public noFaktur?: any,
        public tglFaktur?: any,
        public tglCetakFaktur?: any,
        public stsIsiNama?: any,
        public stsjadwal?: any,
        public tglPrint?: any,
    ) {
    }
}
export class BegbalComponentUpload {
    constructor(
        public idInternal?: any,
        public idFrame?: any,
        public idMachine?: any,
        public idProduct?: any,
        public FeatureCode?: any,
        public STNKKTP?: any,
        public STNKNAME?: any,
        public STNKADDRESS?: any,
        public STNKPHONE?: any,
        public STNKRELIGION?: any,
        public STNKGENDER?: any,
        public IdVendor?: any,
        public LeasingName?: any,
        public FakturNo?: any,
        public dtNoticeReceipt?: any,
        public dtNoticeDelivery?: any,
        public dtSTNKSubmit?: any,
        public dtSTNKReceipt?: any,
        public PoliceNumber?: any,
        public dtSTNKDelivery?: any,
        public dtBPKBReceipt?: any,
        public BPKBNumber?: any,
        public dtBPKBDeliveryCust?: any,
        public dtBPKBDeliveryLea?: any,
        public InvoiceNumber?: any,
        public SalesName?: any,
        public dtFakturReceipt?: any,
        public saleType?: any,
        public idInternal2?: any
    ) {
    }
}
export class TambahReference {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public note?: string,
        public statusFaktur?: string,
        public bbn?: number,
        public otherCost?: number,
        public surId?: any,
        public saleType?: SaleType,
        public personOwner?: Person,
        public vehicle?: Vehicle,
        public requestPoliceNumber?: any,
        public idVendor?: any,
        public submissionNo?: string,
        public dateSubmission?: any,
        public fakturATPM?: string,
        public fakturDate?: any,
        public registrationNumber?: any,
        public personalCustomer?: PersonalCustomer,
        public idMachine?: any,
        public dateFillName?: any,
        public idOrderItem?: any,
        public idOrder?: any,
        public ivuNumber?: any,
        public salesName?: any,
        public organizationOwner?: Organization,
        public costHandling?: number,
        public idvehicle?: any,
        public idproduct?: any,
        public idcolor?: any,
        public idmachine?: any,
        public idreq?: any,
        public idframe?: any,
        public yearofass?: any,
        public idinternal?: any,
        public dateSubbmission?: any,
        public tipePembayaran?: any,
        public fname?: any,
        public dtbastsales?: string,
        public dtbastsalesback?: string,
        public idvendor?: any,
        public idDelType?: any,
        public dtdelivery?: any,
        public bastnumber?: any,
        public policenumber?: any,
        public atpmfakturdt?: any,
        public TotalData?: any,
        public idparrol?: any,
        public atpmfaktur?: any,
        public iscompleted?: any,
        public idpersonowner?: any,
        public name?: any,
        public cellphone?: any,
        public identitynumber?: any,
        public bpkbnumber?: string,
        public refnumber?: string,
        public receiptqty?: number,
        public refdt?: any,
        public receiptnominal?: number,
        public dtreceipt?: any,
        public dtreceiptStnk?: any,
        public dtreceiptNotice?: any,
        public namaLeasing?: any,
        public ktpleasing?: any,
        public biayaP?: any,
    ) {
    }
}
export class PengurusanLuarWilayahRequest {
    constructor(
        public idreq?: any,
        public atpmfaktur?: any,
        public atpmfakturdt?: any,
        public idframe?: any,
        public idmachine?: any,
        public kota?: any,
        public namaDealer?: any,
        public namaStnk?: any,
    ) {
    }
}
export class GetBastNumber {
    constructor(
        public bastnumber?: any
    ) {
    }
}
export class GetInternal {
    constructor(
        public idinternal?: any,
        public name?: any
    ) {
    }
}
