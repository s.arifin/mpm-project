import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { VehicleDocumentRequirement, BegbalComponentUpload, TambahReference, PengurusanLuarWilayahRequest } from './vehicle-document-requirement.model';
import { CustomVehicleDocumentRequirement} from './custom-vehicle-document-requirement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';
import { stringify } from '@angular/core/src/util';
import { request } from 'http';
import { requestItemPopupRoute } from '../request-item/index';

@Injectable()
export class VehicleDocumentRequirementService {
    private itemValues: VehicleDocumentRequirement[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    private resourceUrl = SERVER_API_URL + 'api/vehicle-document-requirements';
    private resourceUrl2 = process.env.API_C_URL + '/api/vehicle-document-requirements';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vehicle-document-requirements';
    private resourceCUrl = process.env.API_C_URL + '/api/vehicle_document_requirement/';
    private resourceCGLUrl = process.env.API_C_URL + '/api/ax_generalledger/CreateJournal/';

    constructor(private http: Http) { }

    create(vehicleDocumentRequirement: VehicleDocumentRequirement): Observable<VehicleDocumentRequirement> {
        const copy = this.convert(vehicleDocumentRequirement);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createGL(idRequirement: any): Observable<any> {
        const copy = '[]';
        return this.http.post(this.resourceCGLUrl + '?TranCode=401&IdInput=' + idRequirement, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(vehicleDocumentRequirement: VehicleDocumentRequirement): Observable<VehicleDocumentRequirement> {
        const copy = this.convert(vehicleDocumentRequirement);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    queryUpdateTanggal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const copy = '[{idframe:"' + req.idframe + '",name:"' + req.name + '",identitynumber:"' + req.identitynumber + '",cellphone"' + req.cellphone + '",idTag:"' + req.idTag + '",idInternal"' + req.idInternal + '"]';
        options.params.set('idframe', req.idframe);
        options.params.set('name', req.name);
        options.params.set('identitynumber', req.identitynumber);
        options.params.set('cellphone', req.cellphone);
        options.params.set('idTag', req.idTag);
        options.params.set('idInternal', req.idInternal);
        return this.http.put(this.resourceCUrl + '/VdrCustomUpdateData', copy, options);
    }
    queryUpdateTanggalBpkbOnhand(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const copy = '[{idframe:"' + req.noka + '",iddeltype:"' + req.iddeltype + '",name:"' + req.name + '",identitynumber:"' + req.identitynumber + '",cellphone"' + req.cellphone + '", idTag"' + req.idTag + '", idInternal"' + req.idInternal + '"]';
        options.params.set('idframe', req.idframe);
        options.params.set('iddeltype', req.iddeltype);
        options.params.set('name', req.name);
        options.params.set('identitynumber', req.identitynumber);
        options.params.set('cellphone', req.cellphone);
        options.params.set('idTag', req.idTag);
        options.params.set('idInternal', req.idInternal);
        console.log('update masokkkkk SERVICE UPDATE BPKB ON HAND', req)
        return this.http.put(this.resourceCUrl + '/VdrPrintBpkbCustomer', copy, options);
    }
    queryUpdateBastLeasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const copy = '[{idframe:"' + req.noka + '",iddeltype:"' + req.iddeltype + '",name:"' + req.namaLeasing + '",identitynumber:"' + req.ktpleasing + '",idInternal:"' + req.idInternal + '"]';
        options.params.set('idframe', req.idframe);
        options.params.set('iddeltype', req.iddeltype);
        options.params.set('name', req.namaLeasing);
        options.params.set('identitynumber', req.ktpleasing);
        options.params.set('identitynumber', req.idInternal);
        console.log('update masokkkkk SERVICE UPDATE BPKB ON HAND', req)
        return this.http.put(this.resourceCUrl + '/VdrCustomUpdateLeasing', copy, options);
    }
    queryUpdateTerimaSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const copy = '[{name:"' + req.fname + '",idframe:"' + req.idframe + '",idTag:"' + req.idTag + '", idInternal:"' + req.idInternal + '" ]';
        options.params.set('name', req.fname);
        options.params.set('idframe', req.idframe);
        options.params.set('idTag', req.idTag);
        options.params.set('idInternal', req.idInternal);
        console.log('BAST STNK KE SALES', req)
        return this.http.put(this.resourceCUrl + '/VdrTerimaSales', copy, options);
    }
    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrCustomData', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryCheckByNoka(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        console.log('MASOK PAK EKO CHECK NOKA BPKB', req)
        return this.http.get(this.resourceCUrl + '/VdrCheckNoka', options).map((res: Response) => this.convertResponse(res));
    }
    queryCheckStnkByNoka(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        console.log('MASOK PAK EKO CHECK STNK BY NOKA', req)
        return this.http.get(this.resourceCUrl + '/VdrCheckStnkNoka', options).map((res: Response) => this.convertResponse(res));
    }
    searchBegbal(a: any): Observable<ResponseWrapper> {
        const options = createRequestOption(a);
        options.headers.append('Content-Type', 'application/json');
        console.log('search belakang', a);
        return this.http.get(this.resourceCUrl + 'searchBpkbStnk', options).map(
            (res: Response) =>
        this.convertResponse(res));
            // return res;
        // );
    }
    queryCheckBiaya(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        console.log('MASOK PAK EKO CHECK BIAYA PENGURUSAN', req)
        return this.http.get(this.resourceCUrl + '/VdrChecBiayaPengurusan', options).map((res: Response) => this.convertResponse(res));
    }
    queryCbpkbOnHand(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrBpkbOnHand', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryBpkbBelumJadi(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrBpkbBelumJadi', options)
            .map((res: Response) => this.convertResponse(res));
    }
    ambilSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrAmbilSales', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getInternal(): Observable<ResponseWrapper> {
        const options = createRequestOption();
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/getIdInternal', options)
            .map((res: Response) => this.convertResponse(res));
    }
    getBastNumber(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrAmbilnoBast', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryStnkBelumJadi(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrStnkBelumJadi', options)
            .map((res: Response) => this.convertResponse(res));
    }
    requestPengurusanLuarWilayah(req?: any): Observable<ResponseWrapper> {
        console.log('reqqqq nya', req);
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/vdrRequestPengurusanLuarWilayah', options)
            .map((res: Response) => this.convertResponse(res));
    }
    monitorRequester(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/monReq', options)
            .map((res: Response) => this.convertResponse(res));
    }
    inputBiayaBantuanPengurusan(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // const copy = '[{biayaPengurusan:"' + req.biayaPengurusan + '"}]';
        // options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/biayaBantuan', options).map((res: Response) => {
            return res;
        });
    }
    receivePengurusanLuarWilayah(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/vdrReceivePengurusanLuarWilayah', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryStnkSudahDiambil(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrStnkSudahDiambil', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryStnkListRefrence(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/VdrStnListRefrence', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryBpkbSudahDiambil(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'apllication/json');
        return this.http.get(this.resourceCUrl + '/VdrBpkbSudahDiambil', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryUpdate(vehicleDocumentRequirement: VehicleDocumentRequirement): Observable<VehicleDocumentRequirement> {
        const copy = this.convert(vehicleDocumentRequirement);
        return this.http.put(this.resourceUrl + '/update', copy).map((res: Response) => {
            return res.json();
        });
    }
    PrintCustomerBpkb(customerBpkb: VehicleDocumentRequirement[]): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        const copy = JSON.stringify(customerBpkb);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        console.log('update masokkkkk SERVICE UPDATE BPKB ON HAND', customerBpkb)
        return this.http.put(`${this.resourceCUrl}/VdrPrintBpkbCustomer`, copy, options).map((res: Response) => {
            return res;
        });
    }
    queryUpdateBastLeasingArray(leasingBpkb: VehicleDocumentRequirement[]): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        const copy = JSON.stringify(leasingBpkb);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        console.log('itmmm masokkkkk ', leasingBpkb)
        return this.http.put(`${this.resourceCUrl}/PrintUpdateLeasing`, copy, options).map((res: Response) => {
            return res;
        });
    }
    queryPenerimaanBpkb(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const copy = '[{idframe:"' + req.idframe + '",idmachine:"' + req.idmachine + '",policenumber:"' + req.policenumber + '",bpkbnumber:"' + req.bpkbnumber + '",idInternal:"' + req.idInternal + '"}]';
        options.params.set('idframe', req.idframe);
        options.params.set('iddeltype', req.iddeltype);
        options.params.set('policenumber', req.policenumber);
        options.params.set('bpkbnumber', req.bpkbnumber);
        options.params.set('idInternal', req.idInternal);
        console.log('MASOK PAK EKO PENERIMAAN BPKB', req)
        return this.http.put(this.resourceCUrl + '/VdrPenerimaanBpkb', copy, options)
            .map((res: Response) => this.convertResponse(res));
    }
    searchBastForPrintUlang(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/VdrCariBastPrintUlang', options).map((res: Response) => this.convertResponse(res));
    }
    queryPenerimaanStnk(itm: TambahReference[]): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        const copy = JSON.stringify(itm);
        options.headers = new Headers();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        // const copy = this.convert(veh);
        // options.params.put('itm', req.itm);

        // options.params.set('other', req.other);
        console.log('itmmm masokkkkk ', itm)
        return this.http.put(`${this.resourceCUrl}/simpanPenerimaan`, copy, options)
            .map((res: Response) => this.convertResponse(res));
    }

    find(id: any): Observable<VehicleDocumentRequirement> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }
    queryUploadReceive(pengurusanLuarWilayahs: PengurusanLuarWilayahRequest[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(pengurusanLuarWilayahs);
        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/saveUploadReceiveManual`, copy, options).map((res: Response) => {
            return res;
        });
    }
    queryUpload(begbalComponentsUpload: BegbalComponentUpload[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(begbalComponentsUpload);
        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/saveUpload`, copy, options).map((res: Response) => {
            console.log('MASUK UPLOAD NYA PAK EKO', res)
            return res;
        });
    }
    execReceiveTerimaFaktur(vehicleDocumentRequirements: VehicleDocumentRequirement[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(vehicleDocumentRequirements);
        const options = new RequestOptions();
        options.headers = new Headers();
        console.log('masok untuk kebelakan penerimaan faktur', vehicleDocumentRequirements)
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/receiveFaktur`, copy, options).map((res: Response) => {
            console.log('MASUK UPLOAD NYA PAK EKO', res)
            return res;
        });
    }
    pengurusanLuarWilayahSendRequest(a: PengurusanLuarWilayahRequest[]): Observable<ResponseWrapper> {
        const options = new RequestOptions();
        const copy = JSON.stringify(a);
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');
        console.log('update masokkkkk Send Request', a)
        return this.http.put(`${this.resourceCUrl}/sendRequestPengurusanLuarWilayah`, copy, options).map((res: Response) => {
            return res;
        });
    }
    findbyFrame(id: any): Observable<VehicleDocumentRequirement> {
        return this.http.get(`${this.resourceUrl}/getbyFrame/${id}`).map((res: Response) => {
            return res.json();
        });
    }
    findCustom(id: any): Observable<CustomVehicleDocumentRequirement> {
        return this.http.get(`${this.resourceUrl}/findCustom/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
         const options = createRequestOption(req);
         // options.params.set('idStatusType', req.idStatusType);
         // options.params.set('idSaleType', req.idSaleType);
         return this.http.get(this.resourceUrl, options)
             .map((res: Response) => this.convertResponse(res));
     }
    queryCustom(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        // options.params.set('idStatusType', req.idStatusType);
        // options.params.set('idSaleType', req.idSaleType);
        return this.http.get(this.resourceUrl + '/custom', options)
            .map((res: Response) => this.convertResponse(res));
    }

    protected setParamValue(options: BaseRequestOptions, id: string, value: any) {
        if (value !== undefined && value !== null) {
            options.params.set(id, value);
        }
    }

    queryItemsByFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        this.setParamValue(options, 'dateIVU', req.dateIVU);
        this.setParamValue(options, 'dateIsiNama', req.dateIsiNama);
        return this.http.get(this.resourceUrl + '/filter-items', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(vehicleDocumentRequirement: VehicleDocumentRequirement, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(vehicleDocumentRequirement);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    public convertListForSelectPrimeNg(lists: Array<VehicleDocumentRequirement>): Array<Object> {
        const arr: Array<Object> = new Array<Object>();

        if (lists.length > 0) {
            lists.forEach(
                (m) => {
                    const obj: object = {
                        label : m.submissionNo,
                        value : m.submissionNo
                    };
                    arr.push(obj);
                }
            )
        }

        return arr;
    }

    executeProcess(id: Number, param: String, vehicleDocumentRequirement: VehicleDocumentRequirement): Observable<VehicleDocumentRequirement> {
        const copy = this.convert(vehicleDocumentRequirement);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, vehicleDocumentRequirements: VehicleDocumentRequirement[]): Observable<VehicleDocumentRequirement[]> {
        const copy = this.convertList(vehicleDocumentRequirements);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }
    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(vehicleDocumentRequirement: VehicleDocumentRequirement): VehicleDocumentRequirement {
        if (vehicleDocumentRequirement === null || vehicleDocumentRequirement === {}) {
            return {};
        }
        // const copy: VehicleDocumentRequirement = Object.assign({}, vehicleDocumentRequirement);
        const copy: VehicleDocumentRequirement = JSON.parse(JSON.stringify(vehicleDocumentRequirement));
        return copy;
    }

    private convertList(vehicleDocumentRequirements: VehicleDocumentRequirement[]): VehicleDocumentRequirement[] {
        const copy: VehicleDocumentRequirement[] = vehicleDocumentRequirements;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: VehicleDocumentRequirement[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    // processData(idInternal , __dto: VDRIsiNamaCreateDTO ): Observable<any> {
    //     return this.http.post(`${this.resourceCUrl}isiNamaCreate?idInternal=` + idInternal , __dto).map((res: Response) => {
    //     const jsonResponse = res.json();
    //     return jsonResponse;
    //     });
    // }

    // create(vendor: Vendor): Observable<Vendor> {
    //     const copy = this.convert(vendor);
    //     console.log('create = ', copy);
    //     return this.http.post(this.resourceCUrl, copy).map((res: Response) => {
    //         const jsonResponse = res.json();
    //         this.convertItemFromServer(jsonResponse);
    //         return jsonResponse;
    //     });
    // }

    queryCustomPenerimaan(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/customPenerimaan', options)
            .map((res: Response) => this.convertResponse(res));
    }

    updateNote(vehicleDocumentRequirement: VehicleDocumentRequirement): Observable<VehicleDocumentRequirement> {
        const copy = this.convert(vehicleDocumentRequirement);
        return this.http.post(this.resourceUrl + '/updateNote', copy).map((res: Response) => {
            return res.json();
        });
    }

    querybast(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/cetakUlang', options)
            .map((res: Response) => this.convertResponse(res));
    }
    public validationSubmitToIsiNama(customVdr: CustomVehicleDocumentRequirement[]): string {
        let a: string;
        a = '';

        if (customVdr.length > 0) {
            for (let i = 0; i < customVdr.length; i++) {
                const each: CustomVehicleDocumentRequirement = customVdr[i];
                const sequence = customVdr[i].name;

                if (each.personalIdNumber === null) {
                    a += '<li>Data Stnk - Belum isi nik - atas nama ' + sequence + '</li>';
                }

                if (each.familyIdNumber === null) {
                    a += '<li>Data Stnk - Belum isi no kk - atas nama  ' + sequence + '</li>';
                }

                if (each.gender === null) {
                    a += '<li>Data Stnk - Belum isi jenis kelamin - atas nama  ' + sequence + '</li>';
                }

                if (each.religionTypeId === null) {
                    a += '<li>Data Stnk - Belum isi agama - atas nama  ' + sequence + '</li>';
                }

                if (each.personalIdNumber.length < 16) {
                    a += '<li>Data Stnk - No Ktp minimal 16 angka - atas nama ' + sequence + '</li>';
                }

                if (each.familyIdNumber.length < 16) {
                    a += '<li>Data Stnk - No KK minimal 16 angka - atas nama  ' + sequence + '</li>';
                }

            }
            if (a !== '') {
                a = '<ul>' + a + '</ul>';
            }
        } else {
            a += 'Anda belum memasukkan pesanan anda';
        }

        return a;
}
}
