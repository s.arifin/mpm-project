import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
// import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement/vehicle-document-requirement.service';
// import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';
// import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import * as BaseConstant from '../../shared/constants/base.constants';
import { VehicleDocumentRequirement, GetBastNumber } from '../vehicle-document-requirement/vehicle-document-requirement.model';
import { AutoCompleteModule } from 'primeng/primeng';

@Component({
    selector: 'jhi-registration-bast-bpkb-begbal',
    templateUrl: './registration-bast-bpkb-begbal.component.html'
})
export class RegistrationBASTBPKBBegbalComponent implements OnInit, DoCheck {
    currentAccount: any;
    // unitDeliverable: UnitDeliverable;
    // unitDeliverableParameters: UnitDeliverableParameters;
    // unitDeliverableUpdate: UnitDeliverable;
    // unitDeliverables: UnitDeliverable[];
    // dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    nomorRangka: string[] = ['KF', 'KJ', 'kuu'];
    nomorrangka: string;
    filteredNoka: any[];
    isChange: boolean;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    TotalData: VehicleDocumentRequirement;
    predicate: any;
    previousPage: any;
    resPrintUlang: any;
    reverse: any;
    a: VehicleDocumentRequirement[];
    b: VehicleDocumentRequirement[];
    noBast: GetBastNumber[]
    internal: any;
    isSaving: Boolean;
    detail: any;
    detailModal: boolean;
    printModal: boolean;
    popUpPrint: boolean;
    checkPlatNomor: Boolean;
    printUlangBpkb: Boolean;
    isCash: boolean;
    isCredit: boolean;
    pilihBast: string;
    revisiModal: boolean;
    vehicleDocumentRequirementbegbalupdate: VehicleDocumentRequirement;
    vehicleDocumentRequirementbegbal: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalCheck: VehicleDocumentRequirement[];
    vehicleDocumentRequirementSudahDiambil: VehicleDocumentRequirement[];
    vehicleDocumentRequirementBelumJadi: VehicleDocumentRequirement[];
    unitDeliverable: UnitDeliverable[];

    // statusTypes: StatusType[];
    salesmans: Salesman[];

    // bastTo: string;
    // bastKembali: any;
    // bastSalesKembali: boolean;
    selecteds: VehicleDocumentRequirement;
    selected: VehicleDocumentRequirement[];
    idframe: string;
    bastnumber: string;
    iddeltype: string;
    name: string;
    nama: string;
    ktp: string;
    noHp: string;
    potongBast: string;
    namaLeasing: string;
    ktpLeasing: string;
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    filtered: any;
    isChecking: Boolean;

    constructor(
        // protected unitDeliverableService: UnitDeliverableService,
        // protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected activatedRoute: ActivatedRoute,
        private commonUtilService: CommonUtilService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.printModal = false;
        this.isChange = false;
        this.isChecking = false;
        this.isSaving = false;
        this.isCash = false;
        this.isCredit = false;
        this.b = new Array<VehicleDocumentRequirement>();
        this.a = new Array<VehicleDocumentRequirement>();
        this.selected = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirementbegbal = [];
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirementbegbal = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirementbegbalCheck = new Array <VehicleDocumentRequirement>();
        // this.bastTo = 'salesman';
        // this.bastKembali = 0;
        // this.unitDeliverable = new UnitDeliverable();
        // this.unitDeliverableUpdate = new UnitDeliverable();
        // this.unitDeliverables = new Array<UnitDeliverable>();
        this.isFiltered = false;
    }

    loadAll() {
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.searchBegbal(
                {currentSearch: this.currentSearch, idInternal: this.principal.getIdInternal(), idDelType: 103,
                page: this.page - 1,
                size : 10000,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.vehicleDocumentRequirementService.queryCbpkbOnHand({
            query: 'idDelType:103|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 1000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers), console.log('sukses', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal', res) }
        );
        this.vehicleDocumentRequirementService.queryBpkbSudahDiambil({
            query: 'idDelType:103|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 1000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessDiambil(res.json, res.headers), console.log('sukses', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal', res) }
        );
        this.vehicleDocumentRequirementService.getBastNumber(
            {
                query: 'idDelType:103|idInternal:' + this.principal.getIdInternal()
            }
        ).subscribe(
            (res: ResponseWrapper) => { this.onSuccessGetBastNumber(res.json, res.headers)},
            (res: ResponseWrapper) => {this.onError(res.json)}
        )
        this.vehicleDocumentRequirementService.queryBpkbBelumJadi({
            query: 'idDelType:103|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 2000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessBelumJadi(res.json, res.headers), console.log('SUKSES BPKB BELUM JADI', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('GAGAL BPKB BELUM JADI', res) }
        );
    }

    private loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator: this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales', res.json);
                this.salesmans = res.json;
            }
        )
    }
    filter() {
        // this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/VEHICLE-DOCUMENT-REQUIREMENT/REGISTRATION/BAST/STNK/begbal'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'ASC' : 'DESC')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.filtered = '';
        this.currentSearch = null;
        this.isFiltered = false;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/bpkb/begbal']);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        console.log('ini keyword', this.currentSearch);
        this.page = 1;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/bpkb/begbal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadKorsal();
        });
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbal = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbal);
    }
    protected onSuccessDiambil(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementSudahDiambil = data;
        console.log('muncul bpkb sudah diambil', this.vehicleDocumentRequirementSudahDiambil);
    }
    protected onSuccessGetBastNumber(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.noBast = data;
    }
    protected onSuccessBelumJadi(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementBelumJadi = data;
        console.log('MUNCUL BPKB BELUM JADI', this.vehicleDocumentRequirementBelumJadi);
    }
    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
    }

    updateTanggalTerima(noka: any, deltipe: any) {
        this.vehicleDocumentRequirementService.queryUpdateTanggal({
            idframe: noka,
            iddeltype: deltipe
        }).subscribe(
            (res) => {
                console.log('muncul dong', res)
            })
    }
    updateTerimaBpkb(nomorRangka: any, nomorMesin: any, nomorPolisi: any, nomorBpkb: any) {
        this.vehicleDocumentRequirementService.queryPenerimaanBpkb({
            idframe: nomorRangka,
            idmachine: nomorMesin,
            policenumber: nomorPolisi,
            bpkbnumber: nomorBpkb,
            idInternal: this.principal.getIdInternal(),
        }).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'Data Tersimpan', 'Data saved..');
                console.log('PLISS MUNCUL TERIMA BPKB', res)
            },
            (err) => {
                const eror = err.json.ExceptionMessage;
                this.toasterService.showToaster('info', 'Nomor BPKB BELUM TERISI', eror);
            }
        )
    }
    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
        });
    }

    showDetail(idreq: any) {
        this.vehicleDocumentRequirementService.find(idreq).subscribe(
            (response) => {
                this.vehicleDocumentRequirementbegbalupdate = response;
                console.log('cekkk ---->', this.vehicleDocumentRequirementbegbal);
            });
        this.detailModal = true;
    }

    // showPrint(id: any) {
    //     this.printModal = true;
    // }
    masukPengambilCustomer(): Promise<any> {
        return new Promise(
            (resolve) => {
                const data = this.selected;
                data.forEach (
                    (res) => {
                        res.name = this.nama;
                        res.identitynumber = this.ktp;
                        res.cellphone = this.noHp;
                        console.log('masuk mapping nama res', res.name)
                        console.log('masuk mapping ktp res', res.identitynumber)
                        console.log('masuk mapping no hp res', res.cellphone)
                    }
                )
                resolve();
            }
        )
    }
    masukPengambilLeasing(): Promise<any> {
        this.popUpPrint = false;
        return new Promise(
            (resolve) => {
                const data = this.selected;
                data.forEach(
                    (res) => {
                        res.namaLeasing = this.vehicleDocumentRequirementbegbalupdate.namaLeasing;
                        res.ktpleasing = this.vehicleDocumentRequirementbegbalupdate.ktpleasing;
                        console.log('masuk mapping nama res', res.namaLeasing)
                        console.log('masuk mapping ktp res', res.ktpleasing)
                        console.log('masuk mapping ktp res', this.vehicleDocumentRequirementbegbalupdate.namaLeasing)
                        console.log('masuk mapping ktp res', this.vehicleDocumentRequirementbegbalupdate.ktpleasing)
                    }
                )
                resolve();
            }
        )
    }
    updatePrint(nama: any, ktp: any, noHp: any) {
        this.nama = nama;
        this.ktp = ktp;
        this.noHp = noHp;
        console.log('selected yang masuk pak eko', this.selected);
        this.popUpPrint = false;
        this.loadAll();
        this.masukPengambilCustomer()
            .then(
                (value) => {
                    this.popUpPrint = false;
                    this.vehicleDocumentRequirementService.PrintCustomerBpkb(
                        this.selected
                    ).subscribe(
                        (res) => {
                            this.popUpPrint = false;
                            this.loadAll();
                            this.b = res.json();
                            this.siapPrintCutomerArray(this.b);
                            this.selected = new Array<VehicleDocumentRequirement>();
                        })
                    // this.vehicleDocumentRequirementService.queryUpdateTanggalBpkbOnhand({
                    // }).subscribe(
                    //     (res) => {
                    //         console.log('muncul dong', res);
                    //         this.siapPrint(nama, this.idframe);
                    //     })
                }
            )
    }
    updatePrintLeasingArray() {
        this.popUpPrint = false;
        this.masukPengambilLeasing()
            .then(
                (value) => {
                    this.vehicleDocumentRequirementService.queryUpdateBastLeasingArray(
                        this.selected,
                    ).subscribe(
                        (res) => {
                            this.popUpPrint = false;
                            this.a = res.json();
                            this.siapPrintLeasingArray(this.a);
                            this.selected = new Array<VehicleDocumentRequirement>();
                        })
                }
            )
    }
    cariNomorBast() {
        this.bastnumber = this.pilihBast;
        this.vehicleDocumentRequirementService.searchBastForPrintUlang({a: this.bastnumber}).subscribe(
            (res) => {
                console.log('muncul balikan', res)
                this.resPrintUlang = res.json;
                console.log('balikan print ulang', this.resPrintUlang);
                this.PrintUlang(this.resPrintUlang, this.bastnumber);
            }
        )
    }
    showPrintModal(noka: string, deltipe: any) {
        this.printModal = true;
        console.log('pop up dialog muncul', noka)
    }
    print(noka: string, deltipe: any) {
        this.printModal = true;
        this.idframe = noka;
        this.iddeltype = deltipe;
        console.log('dapat nokaaaa', this.idframe)
        console.log('dapat deltipeeee', this.iddeltype)
        // this.updateTanggalTerima(noka, deltipe);
    }
    showPrint() {
    if (this.selected[0].tipePembayaran === 'Credit' ) {
        this.isCredit = true;
    }
    if (this.selected[0].tipePembayaran === 'Cash' ) {
        this.isCash = true;
    }
        this.popUpPrint = true;
        console.log('dapat nokaaaa dari centangggg', this.idframe)
        console.log('dapat deltipeeee', this.iddeltype)
        // this.updateTanggalTerima(noka, deltipe);
    }
    siapPrint(nama: any, noka: any) {
        this.name = nama;
        this.idframe  = noka;
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/bast_bpkb_begbal_2/pdf', { idframe: this.idframe, userlogin: this.currentAccount.firstName});
        console.log('Siapprint', this.idframe);
        console.log('DAPAT NAMA NYA', this.name);
        console.log('SIAPdeltipe', this.iddeltype);
        this.loadAll();
        this.printModal = false;
    }
    siapPrintLeasingArray(a: VehicleDocumentRequirement[]) {
        this.a = a;
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        if ( this.a.length < 3) {
        this.reportUtilService.viewFile('/api/report/bast_bpkb_begbal_Bast_Leasing_2/pdf', { bastnumber: this.a[0].bastnumber, userlogin: this.currentAccount.firstName})}else if (this.a.length > 2) {
            this.reportUtilService.viewFile('/api/report/bast_bpkb_parsial/pdf', { bastnumber: this.a[0].bastnumber, userlogin: this.currentAccount.firstName })
        };
        this.loadAll();
    }
    siapPrintCutomerArray(b: VehicleDocumentRequirement[]) {
        this.b = b;
        this.printModal = false;
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        if ( this.b.length < 3) {
        this.reportUtilService.viewFile('/api/report/bast_bpkb_begbal_2/pdf', { bastnumber: this.b[0].bastnumber, userlogin: this.currentAccount.firstName})}else if (this.b.length > 2) {
            this.reportUtilService.viewFile('/api/report/bast_bpkb_parsial/pdf', { bastnumber: this.b[0].bastnumber, userlogin: this.currentAccount.firstName })
        };
        this.loadAll();
    }
    PrintUlang(resPrintUlang: any, bastnumber: any) {
        this.bastnumber = bastnumber;
        console.log('munculll dong', this.resPrintUlang)
        // this.bastnumber = this.pilihBast;
        // this.vehicleDocumentRequirementService.searchBastForPrintUlang(this.bastnumber);
        // this.potongBast =  this.bastnumber.slice(4);
        // this.potongBast.startsWith('BDL');
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        if (this.resPrintUlang.length < 3) {
        this.reportUtilService.viewFile('/api/report/bast_bpkb_begbal_2/pdf', {bastnumber: this.bastnumber, userlogin: this.currentAccount.firstName});
        }
        if (this.resPrintUlang.length > 2) {
        this.reportUtilService.viewFile('/api/report/bast_bpkb_parsial/pdf', {bastnumber: this.bastnumber, userlogin: this.currentAccount.firstName});
        }
        // console.log('print ulang bast number', this.bastnumber);
        // console.log('SIAPdeltipe', this.iddeltype);
        // console.log('MUNCUL SELECTED', this.selected);
        this.loadAll();
    }
    simpan(nomorRangka: string, nomorMesin: string, nomorPolisi: string, nomorBpkb: string) {
        this.updateTerimaBpkb( nomorRangka, nomorMesin, nomorPolisi, nomorBpkb);
        this.loadAll();
        console.log('PLISSS MUNCULL DONGGGGG', nomorMesin);
        console.log('PLISSS MUNCULL DONGGGGG', nomorRangka);
    }
    autoComplete(event) {
        this.filteredNoka = [];
        for (let i = 0; i < this.nomorRangka.length; i++) {
            const nomorrangka = this.nomorRangka[i];
            if (nomorrangka.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                this.filteredNoka.push(nomorrangka);
            }
        }
        } ;
    ganti() {
        this.isChange = true;
        console.log('ganti true', this.isChange)
    }
    doPrint() {
        this.printModal = false;
    }

    listUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                resolve();
            }
        )
    }
    showRevisi(id: any) {
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    checkNoka(noka): void {
        const unitCheck = new VehicleDocumentRequirement();
        if (this.vehicleDocumentRequirementbegbalupdate.idframe !== '' &&
        this.vehicleDocumentRequirementbegbalupdate.idframe !== undefined &&
        this.vehicleDocumentRequirementbegbalupdate !== null) {
            unitCheck.idframe = this.vehicleDocumentRequirementbegbalupdate.idframe;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idMachine !== '' &&
        this.vehicleDocumentRequirementbegbalupdate.idmachine !== undefined &&
        this.vehicleDocumentRequirementbegbalupdate.idmachine !== null) {
            unitCheck.idmachine = this.vehicleDocumentRequirementbegbalupdate.idmachine;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.policenumber !== '' &&
        this.vehicleDocumentRequirementbegbalupdate.policenumber !== undefined &&
        this.vehicleDocumentRequirementbegbalupdate.policenumber !== null) {
            unitCheck.policenumber = this.vehicleDocumentRequirementbegbalupdate.policenumber;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idvendor !== '' &&
            this.vehicleDocumentRequirementbegbalupdate.idvendor !== undefined &&
            this.vehicleDocumentRequirementbegbalupdate.idvendor !== null) {
            unitCheck.idvendor = this.vehicleDocumentRequirementbegbalupdate.idvendor;

            console.log('MASUK DULU CEK NOKA', noka);

            console.log('MASUK DULU CEK mesin', this.vehicleDocumentRequirementbegbalupdate.idmachine);

            console.log('MASUK DULU CEK NOKA', this.vehicleDocumentRequirementbegbalupdate.policenumber);

            console.log('MASUK DULU CEK NOKA', this.vehicleDocumentRequirementbegbalupdate.idvendor);
        }
        if (this.vehicleDocumentRequirementbegbalupdate.bpkbnumber !== undefined) {
            unitCheck.bpkbnumber = this.vehicleDocumentRequirementbegbalupdate.bpkbnumber;
        }
        this.vehicleDocumentRequirementService.queryCheckByNoka({
            page: 0,
            size: 20,
            query: 'idframe:' + noka,
            idInternal: this.principal.getIdInternal(),
        }).subscribe(
            (res) => {
                if (res) {
                console.log('HASIL PENGECEKAN', res.json);
                this.vehicleDocumentRequirementbegbalCheck = res.json;
                    this.vehicleDocumentRequirementbegbalupdate.idframe = this.vehicleDocumentRequirementbegbalCheck[0].idframe;
                    this.vehicleDocumentRequirementbegbalupdate.idmachine = this.vehicleDocumentRequirementbegbalCheck[0].idmachine;
                    this.vehicleDocumentRequirementbegbalupdate.policenumber = this.vehicleDocumentRequirementbegbalCheck[0].policenumber;
                    this.vehicleDocumentRequirementbegbalupdate.idvendor =
                    this.vehicleDocumentRequirementbegbalCheck[0].idvendor;
                    this.vehicleDocumentRequirementbegbalupdate.bpkbnumber = this.vehicleDocumentRequirementbegbalCheck[0].bpkbnumber;
                this.doCheckPlatNomor(res.json);
            }
        },
        (err) => {
            this.commonUtilService.showError(err('EROR BOS'));
        }
    )
    }
    ngDoCheck(): void {
        if (this.vehicleDocumentRequirementbegbalupdate.idframe != null ||
            this.vehicleDocumentRequirementbegbalupdate.idmachine != null ||
            this.vehicleDocumentRequirementbegbalupdate.idvendor != null ||
            this.vehicleDocumentRequirementbegbalupdate.policenumber) {
            this.isChecking = false;
        } else {
            this.isChecking = true;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idframe === '' ||
            this.vehicleDocumentRequirementbegbalupdate.idframe == null ||
            this.vehicleDocumentRequirementbegbalupdate.idmachine === '' ||
            this.vehicleDocumentRequirementbegbalupdate.idmachine == null ||
            this.vehicleDocumentRequirementbegbalupdate.idvendor === '' ||
            this.vehicleDocumentRequirementbegbalupdate.idvendor  == null ||
            this.vehicleDocumentRequirementbegbalupdate.policenumber === '' ||
            this.vehicleDocumentRequirementbegbalupdate.policenumber == null) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    doCheckPlatNomor(itm: UnitDeliverable) {
        if (itm.platNomor != null) {
            this.checkPlatNomor = true;
        } else {
            this.checkPlatNomor = false;
        }

    }
    showPrintUlangBpkb() {
        this.selected = new Array<VehicleDocumentRequirement>();
        this.printUlangBpkb = true;
    }
    closeLeasing() {
        this.selected = new Array<VehicleDocumentRequirement>();
        this.popUpPrint = false;
        this.isCash = false;
        this.isCredit = false;
    }
}
