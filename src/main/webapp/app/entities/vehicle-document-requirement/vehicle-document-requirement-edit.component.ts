import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { ToasterService} from '../../shared';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-document-requirement-edit',
    templateUrl: './vehicle-document-requirement-edit.component.html'
})
export class VehicleDocumentRequirementEditComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    isSaving: boolean;

    salesunitrequirements: SalesUnitRequirement[];

    constructor(
        private alertService: JhiAlertService,
        private vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private route: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.salesUnitRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.salesunitrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.vehicleDocumentRequirementService.find(id).subscribe((vehicleDocumentRequirement) => {
            this.vehicleDocumentRequirement = vehicleDocumentRequirement;
        });
    }

    previousState() {
        this.router.navigate(['vehicle-document-requirement']);
    }

    save() {
        this.isSaving = true;
        if (this.vehicleDocumentRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.update(this.vehicleDocumentRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.create(this.vehicleDocumentRequirement));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleDocumentRequirement>) {
        result.subscribe((res: VehicleDocumentRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: VehicleDocumentRequirement) {
        this.eventManager.broadcast({ name: 'vehicleDocumentRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleDocumentRequirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.toaster.showToaster('warning', 'vehicleDocumentRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackSalesUnitRequirementById(index: number, item: SalesUnitRequirement) {
        return item.idRequirement;
    }
}
