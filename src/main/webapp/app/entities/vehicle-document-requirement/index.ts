export * from './vehicle-document-requirement.model';
export * from './custom-vehicle-document-requirement.model';
export * from './vehicle-document-requirement-popup.service';
export * from './vehicle-document-requirement.service';
export * from './vehicle-document-requirement-dialog.component';
// export * from './vehicle-document-requirement-base.component';
export * from './vehicle-document-requirement.component';
export * from './vehicle-document-requirement.route';
export * from './vehicle-document-requirement-as-list.component';
export * from './vehicle-document-requirement-as-lov.component';
export * from './vehicle-document-requirement-edit.component';
export * from './registration-data-transaksi.component';
export * from './registration-bast-stnk-begbal.component';
export * from './registration-bast-bpkb-begbal.component';
export * from './registration-bast-stnk-begbal-upload.component';
export * from './registrasi-bast-stnk-list-refrence.component';
export * from './registrasi-bast-stnk-diterima.component';
export * from './registrasi-sales/vehicle-document-requirement-sales-bast.component';
export * from './registrasi-bast-stnk-input-penerimaan-stnk.component';

export * from './registration-isi-nama.component';
export * from './registration-isi-nama-cancel.component';
export * from './registration-isi-nama-pengajuan-faktur.component';
export * from './registration-isi-nama-data-belum-lengkap.component';
export * from './registration-isi-nama-detail.component';
export * from './registration-isi-nama-edit.component';
export * from './registration-isi-nama-upload.component';

export * from './registration-penerimaan-faktur.component';
export * from './registration-penerimaan-faktur-non.component';
export * from './registration-penerimaan-faktur-revisi.component';
export * from './registration-penerimaan-faktur-upload.component';

export * from './registration-penerimaan-bpkb-stnk.component';
export * from './registration-penerimaan-bpkb-stnk-list-ref.component';
export * from './registration-penerimaan-bpkb-stnk-input.component';
export * from './registration-penerimaan-bpkb-stnk-revisi.component';

export * from './registration-pengurusan-biro-jasa.component';
export * from './registration-pengurusan-biro-jasa-pending.component';
export * from './registration-pengurusan-biro-jasa-revisi.component';

export * from './registration-bast-off-the-road.component';
export * from './registration-bast-stnk-internal.component';
export * from './registration-bast-stnk-eksternal.component';
export * from './registration-bast-bpkb-internal.component';
export * from './registration-bast-bpkb-eksternal.component';

export * from './registration-pengurusan-luar-wilayah-receive.component';
export * from './registration-pengurusan-luar-wilayah-receive-manual.component';
export * from './registration-pengurusan-luar-wilayah-receive-revisi.component';

export * from './registration-pengurusan-luar-wilayah-request.component';
export * from './registration-pengurusan-luar-wilayah-request-revisi.component';
export * from './isinama-edit-data-cdb.component';
export * from './view-person/isinama-customer-view.component';
export * from './view-person/isinama-edit-person.component';
export * from './view-person/isinama-edit-person-stnk.component';
export * from './view-person/isinama-edit-organization.component';
export * from './gc/isinama-edit-data-cdb-gc.component';
export * from './registrasi-sales/vehicle-document-requirement-sales.component';
export * from './registrasi-sales/vehicle-document-requirement-sales-view.component';
export * from './registrasi-sales/vehicle-document-requirement-sales-viewbast.component';
