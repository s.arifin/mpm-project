import { BaseEntity } from './../../shared';
import { Vehicle } from '../vehicle/vehicle.model';
import { Person } from '../person';
import { PersonalCustomer } from '../personal-customer/personal-customer.model';

export class VehicleDocumentRequirement implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public note?: string,
        public bbn?: number,
        public otherCost?: number,
        public surId?: any,
        public personOwner?: Person,
        public vehicle?: Vehicle,
        public requestPoliceNumber?: any,
        public idVendor?: any,
        public submissionNo?: string,
        public dateSubmission?: any,
        public fakturATPM?: string,
        public fakturDate?: any,
        public personalCustomer?: PersonalCustomer,
    ) {
        this.personOwner = new Person();
        this.personalCustomer = new PersonalCustomer();
        this.vehicle = new Vehicle();
    }
}
