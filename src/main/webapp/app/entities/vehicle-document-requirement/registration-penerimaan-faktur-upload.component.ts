import { Component, OnInit, OnDestroy, DoCheck, Pipe } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ConvertUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService, FileUploadModule } from 'primeng/primeng';
import { Http } from '@angular/http';
import { UploadFaktur } from '.';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
  selector: 'jhi-registration-penerimaan-faktur-upload',
  templateUrl: './registration-penerimaan-faktur-upload.component.html',
  styles: []
})
export class RegistrationPenerimaanFakturUploadComponent implements OnInit, DoCheck, OnDestroy {

    public listUploadFaktur: Array<UploadFaktur>;

  currentAccount: any;
  vehicleDocumentRequirement: VehicleDocumentRequirement;
  vehicleDocumentRequirements: VehicleDocumentRequirement[];
  tempArray: VehicleDocumentRequirement[];
  vehicleDocumentRequirementsTmp: VehicleDocumentRequirement[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: string;
  routeData: any;
  links: any;
  totalItems: any;
  queryCount: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  validTgl: boolean;
  countData: number;

  cancelModal: boolean;

  datas: any;

  note: string;

  tanggalfaktur = {
    tanggal : null,
    bulan : null,
    tahun : null
  }

  constructor(
      protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
      protected confirmationService: ConfirmationService,
      protected parseLinks: JhiParseLinks,
      protected alertService: JhiAlertService,
      protected principal: Principal,
      protected activatedRoute: ActivatedRoute,
      protected router: Router,
      protected eventManager: JhiEventManager,
      protected paginationUtil: JhiPaginationUtil,
      protected paginationConfig: PaginationConfig,
      protected toasterService: ToasterService,
      protected http: Http,
      protected convertUtilService: ConvertUtilService,
      protected loadingService: LoadingService,
  ) {
      this.itemsPerPage = ITEMS_PER_PAGE;
      this.routeData = this.activatedRoute.data.subscribe((data) => {
          this.page = data['pagingParams'].page;
          this.previousPage = data['pagingParams'].page;
          this.reverse = data['pagingParams'].ascending;
          this.predicate = data['pagingParams'].predicate;
      });
      this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
      this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
      this.vehicleDocumentRequirements = [];
      this.tempArray = [];
      this.cancelModal = false;
      this.vehicleDocumentRequirementsTmp = new Array<VehicleDocumentRequirement>();
  }

  loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
          this.vehicleDocumentRequirementService.search({
              idStatusType: 32,
              page: this.page - 1,
              query: this.currentSearch,
              size: this.itemsPerPage,
              sort: this.sort()}).subscribe(
                  (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                  (res: ResponseWrapper) => this.onError(res.json)
              );
          return;
        }
        this.vehicleDocumentRequirementService.query({
          idStatusType: 32,
          idInternal: this.principal.getIdInternal(),
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()}).subscribe(
          (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
          (res: ResponseWrapper) => this.onError(res.json)
        );
  }

  loadPage(page: number) {
      if (page !== this.previousPage) {
          this.previousPage = page;
          this.transition();
      }
  }

  transition() {
      this.router.navigate(['/vehicle-document-requirement'], {queryParams:
          {
              page: this.page,
              size: this.itemsPerPage,
              search: this.currentSearch,
              sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
          }
      });
      this.loadAll();
  }

  clear() {
      this.page = 0;
      this.currentSearch = '';
      this.router.navigate(['/vehicle-document-requirement', {
          page: this.page,
          sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }]);
      this.loadAll();
  }

  search(query) {
      if (!query) {
          return this.clear();
      }
      this.page = 0;
      this.currentSearch = query;
      this.router.navigate(['/vehicle-document-requirement', {
          search: this.currentSearch,
          page: this.page,
          sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }]);
      this.loadAll();
  }

  ngOnInit() {
      this.principal.identity(true).then((account) => {
          this.currentAccount = account;
          this.loadAll();
      });
      this.registerChangeInVehicleDocumentRequirements();

  }

  ngOnDestroy() {
      this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: VehicleDocumentRequirement) {
      return item.idRequirement;
  }

  registerChangeInVehicleDocumentRequirements() {
      this.eventSubscriber = this.eventManager.subscribe('vehicleDocumentRequirementListModification', (response) => this.loadAll());
  }

  sort() {
      const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
      if (this.predicate !== 'idRequirement') {
          result.push('idRequirement');
      }
      return result;
  }

  protected onSuccess(data, headers) {
      this.loadingService.loadingStop();
      this.links = this.parseLinks.parse(headers.get('link'));
      this.totalItems = headers.get('X-Total-Count');
      this.queryCount = this.totalItems;
      // this.page = pagingParams.page;
      // this.vehicleDocumentRequirements = data;
  }

  protected onError(error) {
      this.loadingService.loadingStop();
      this.alertService.error(error.message, null, null);
  }

  executeProcess(data) {
      this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
         (value) => console.log('this: ', value),
         (err) => console.log(err),
         () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
      );
  }

  loadDataLazy(event: LazyLoadEvent) {
      this.itemsPerPage = event.rows;
      this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

      if (event.sortField !== undefined) {
          this.predicate = event.sortField;
          this.reverse = event.sortOrder;
      }
      this.loadAll();
  }

  updateRowData(event) {
      if (event.data.id !== undefined) {
          this.vehicleDocumentRequirementService.update(event.data)
              .subscribe((res: VehicleDocumentRequirement) =>
                  this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
      } else {
          this.vehicleDocumentRequirementService.create(event.data)
              .subscribe((res: VehicleDocumentRequirement) =>
                  this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
      }
  }

  protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
      this.toasterService.showToaster('info', 'VehicleDocumentRequirement Saved', 'Data saved..');
  }

  protected onRowDataSaveError(error) {
      try {
          error.json();
      } catch (exception) {
          error.message = error.text();
      }
      this.onError(error);
  }

  refreshData(data: any, item: any, index: any) {
      if (data.vehicle.idFrame === item.vehicle.idFrame && data.vehicle.idMachine === item.vehicle.idMachine) {
          this.vehicleDocumentRequirements.splice(index, 1);

          this.tempArray = [];
          this.tempArray = this.vehicleDocumentRequirements;

          this.vehicleDocumentRequirements = [];
          this.vehicleDocumentRequirements = this.vehicleDocumentRequirements.concat(this.tempArray);
      }
  }

  doReceive(data: any) {
      this.vehicleDocumentRequirement = data;
      this.vehicleDocumentRequirementService.executeProcess(102, null, this.vehicleDocumentRequirement).subscribe(
          (value) => {
              this.vehicleDocumentRequirements.forEach((item, index) => {
                  this.refreshData(value, item, index);
              });
          },
          (err) => console.log(err),
          () => {
              this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..');
          }
      );
  }

  showCancelModal(data: any) {
      this.cancelModal = true;
      this.vehicleDocumentRequirement = data;
  }

  doCancel() {
      this.vehicleDocumentRequirement.note = this.note;
      this.vehicleDocumentRequirementService.executeProcess(104, null, this.vehicleDocumentRequirement).subscribe(
          (value) => {
              console.log(value);
              this.vehicleDocumentRequirements.forEach((item, index) => {
                  this.refreshData(value, item, index);
              });
              this.cancelModal = false;
          },
          (err) => console.log(err),
          () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
      );
  }

    ngDoCheck() {

    }

    onFileChange(evt: any) {
    const tempListUploadInvoice = new Array<UploadFaktur>();
    // var res = '';
    // res = res + isNaN(2005/12/12);
    this.listUploadFaktur = new Array<UploadFaktur>();
    this.convertUtilService.csvToArray(evt, ',').then(
        (res) => {
            console.log('res==', res);
            for (let i = 0; i < res.length; i++) {
                if (i > 0) {
                    const each = res[i];
                    if (each[3] != null) {
                        console.log('each', each[i]);
                        console.log('each0', each[0]);
                        const uploadFtkr = new UploadFaktur()
                        if (each[12] !== null) {
                            const parts = each[12].split('-');
                            this.tanggalfaktur.tanggal = parseInt(parts[0], 10);
                            this.tanggalfaktur.bulan = parseInt(parts[1], 10);
                            this.tanggalfaktur.tahun = parseInt(parts[2], 10);
                            console.log('parts', parts);
                            console.log('day',  this.tanggalfaktur.tanggal);
                            console.log('month', this.tanggalfaktur.bulan);
                            console.log('year', this.tanggalfaktur.tahun);
                            if (this.tanggalfaktur.tahun < 1000 || this.tanggalfaktur.tahun > 3000 || this.tanggalfaktur.bulan === 0 || this.tanggalfaktur.bulan > 12) {
                                uploadFtkr.stsjadwal = false;
                                uploadFtkr.tglFaktur = null;
                            }
                            const monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

                            // Adjust for leap years
                            if (this.tanggalfaktur.tahun % 400 === 0 || (this.tanggalfaktur.tahun % 100 !== 0 && this.tanggalfaktur.tahun % 4 === 0)) {
                                monthLength[1] = 29;
                            }
                        }
                        const tgl = new Date ( this.tanggalfaktur.tahun + '-' + this.tanggalfaktur.bulan + '-' + this.tanggalfaktur.tanggal );
                        if (each[12] === null || each[12] === '' || each[12] === []) {
                            uploadFtkr.stsjadwal = false;
                            uploadFtkr.tglFaktur = null;
                        }
                        // Check the range of the day
                        // return day > 0 && day <= monthLength[month - 1];

                        console.log('eachrow', each[3]);
                        if (uploadFtkr.stsjadwal === false) {  // d.valueOf() could also work
                            // date is not valid
                            console.log('null date')
                            uploadFtkr.noka = each[3]
                            uploadFtkr.nosin = each[2];
                            uploadFtkr.noFaktur = each[13];
                            uploadFtkr.tglFaktur = null;
                            uploadFtkr.stsIsiNama = each[9];
                        } else if (uploadFtkr.stsjadwal !== false) {
                            uploadFtkr.stsjadwal = true;
                            uploadFtkr.noka = each[3]
                            uploadFtkr.nosin = each[2];
                            uploadFtkr.noFaktur = each[13];
                            uploadFtkr.tglFaktur = tgl
                            uploadFtkr.stsIsiNama = each[9];
                        }

                        tempListUploadInvoice.push(uploadFtkr);
                    }
                }
            }
            console.log('doneupload', tempListUploadInvoice);
            this.listUploadFaktur = tempListUploadInvoice;
        },
        (err) => {
            this.toasterService.showToaster('error', 'Error ', err);
        }
    )

  }

  delete(id: any) {
      this.confirmationService.confirm({
          message: 'Are you sure that you want to delete?',
          header: 'Confirmation',
          icon: 'fa fa-question-circle',
          accept: () => {
              this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
              this.eventManager.broadcast({
                  name: 'vehicleDocumentRequirementListModification',
                  content: 'Deleted an vehicleDocumentRequirement'
                  });
              });
          }
      });
  }

  submit() {
    for ( const i of this.listUploadFaktur) {
        const dataupload = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirements = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement;
        if (i.tglFaktur !== null && i.noFaktur !== null) {
            this.vehicleDocumentRequirement.vehicle.idMachine = i.nosin;
            this.vehicleDocumentRequirement.vehicle.idFrame = i.noka;
            this.vehicleDocumentRequirement.fakturATPM = i.noFaktur;
            this.vehicleDocumentRequirement.fakturDate = i.tglFaktur;
            this.vehicleDocumentRequirement.note = i.stsIsiNama;
            this.vehicleDocumentRequirementsTmp.push(this.vehicleDocumentRequirement);
        }
    }
    console.log('upload all', this.vehicleDocumentRequirementsTmp);
    this.vehicleDocumentRequirementService.executeListProcess(32, null, this.vehicleDocumentRequirementsTmp).subscribe(
        (value) => {
            console.log('this: ', value);
            this.countData = 0;
            for (let i = 0; i < value.length; ++i) {
                this.countData++;
            }
            this.cancelModal = true;
            // this.loadAll();
            this.router.navigate(['/vehicle-document-requirement/registration-penerimaan-faktur']);
        },
        (err) => console.log(err),
        () => this.toasterService.showToaster('info', 'Data Proces',  this.countData + ' ' + 'Data done process in system..')
     );
    // this.vehicleDocumentRequirementService.findbyFrame
  }
}
