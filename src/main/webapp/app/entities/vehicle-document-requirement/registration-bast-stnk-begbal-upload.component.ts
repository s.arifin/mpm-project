import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

// import { PriceComponent, PriceComponentUpload } from './price-component.model';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement/vehicle-document-requirement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { VehicleDocumentRequirement, BegbalComponentUpload } from '../vehicle-document-requirement/vehicle-document-requirement.model';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { PriceTypeService, PriceType } from '../price-type';
import * as XLSX from 'xlsx';

@Component({
    selector: 'jhi-registration-bast-stnk-begbal-upload',
    templateUrl: './registration-bast-stnk-begbal-upload.component.html'
})
export class RegirasiBastStnkBegbalComponent implements OnInit, OnDestroy {

    currentAccount: any;
    // priceComponents: PriceComponent[];
    begbalComponentUpload: BegbalComponentUpload;
    begbalComponentsUpload: BegbalComponentUpload[];
    // priceComponent: PriceComponent;
    priceType: any;
    priceTypes: PriceType[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    a: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    containerData: any;
    invalidFormat: Boolean;
    tanggalharga = {
        tanggal: null,
        bulan: null,
        tahun: null
    }

    constructor(
        // protected priceComponentService: PriceComponentService,
        protected confirmationService: ConfirmationService,
        protected priceTypeService: PriceTypeService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public commonUtil: CommonUtilService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
    //     if (this.currentSearch) {
    //         this.priceComponentService.search({
    //             page: this.page - 1,
    //             query: this.currentSearch,
    //             size: this.itemsPerPage,
    //             sort: this.sort()
    //         }).subscribe(
    //             (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //             (res: ResponseWrapper) => this.onError(res.json)
    //         );
    //         return;
    //     }
    //     this.priceComponentService.query({
    //         page: this.page - 1,
    //         size: this.itemsPerPage,
    //         sort: this.sort()
    //     }).subscribe(
    //         (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
    //         (res: ResponseWrapper) => this.onError(res.json)
    //     );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/price-component'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['vehicle_document_requirement/registration/bast/stnk/begbal', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/price-component', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        // this.principal.identity(true).then((account) => {
        //     this.currentAccount = account;
        // });
        // this.priceTypeService.getAllData()
        //     .subscribe((res) => { this.priceTypes = res.json; });
        // this.registerChangeInPriceComponents();
    }

    displayPriceType(priceType) {
        console.log('ceeek', priceType);
    }

    trackByPriceTypesById(index: number, item: PriceType) {
        return item.idPriceType;
    }

     onFileChange(evt: any) {
         /* wire up file reader */
         const target: DataTransfer = <DataTransfer>(evt.target);
         if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
         const reader: FileReader = new FileReader();
         reader.onload = (e: any) => {
             const bstr: string = e.target.result;
             const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
             const wsname: string = wb.SheetNames[0];
             const ws: XLSX.WorkSheet = wb.Sheets[wsname];
             this.containerData = (XLSX.utils.sheet_to_json(ws));
             this.convertXLXStoListSuspect();
         };
         reader.readAsBinaryString(target.files[0]);
     }

     protected convertXLXStoListSuspect(): void {
         console.log('container data ==>', this.containerData);
         if (this.containerData[0]['STNKKTP']) {
             this.begbalComponentsUpload = new Array<BegbalComponentUpload>()
             this.containerData.forEach((element) => {
                 this.begbalComponentUpload = new BegbalComponentUpload();
                 this.begbalComponentUpload.idInternal2 = element['BUCODE'];
                 this.begbalComponentUpload.idFrame = element['FRAMENUMBER'];
                 this.begbalComponentUpload.idMachine = element['ENGINENUMBER'];
                 this.begbalComponentUpload.idProduct = element['PRODUCTNAME'];
                 this.begbalComponentUpload.FeatureCode = element['COLORCODE'];
                 this.begbalComponentUpload.STNKKTP = element['STNKKTP'];
                 this.begbalComponentUpload.STNKNAME = element['STNKNAME'];
                 this.begbalComponentUpload.STNKPHONE = element['NOHP'];
                 this.begbalComponentUpload.STNKRELIGION = element['AGAMA'];
                 this.begbalComponentUpload.STNKGENDER = element['JENISKELAMIN'];
                 this.begbalComponentUpload.STNKADDRESS = element['STNKADDRESS1'];
                 this.begbalComponentUpload.IdVendor = element['BIROJASA'];
                 this.begbalComponentUpload.LeasingName = element['LEASINGNAME'];
                 this.begbalComponentUpload.FakturNo = element['FAKTUR'];
                 this.begbalComponentUpload.dtNoticeReceipt = element['NOTICERECEIPTDATE'];
                 this.begbalComponentUpload.dtNoticeDelivery = element['NOTICEDELIVERDATE'];
                 this.begbalComponentUpload.dtSTNKSubmit = element['STNKSUBMITDATE'];
                 this.begbalComponentUpload.dtSTNKReceipt = element['STNKRECEIPTDATE'];
                 this.begbalComponentUpload.PoliceNumber = element['PLATENUMBER'];
                 this.begbalComponentUpload.dtSTNKDelivery = element['STNKDELIVERDATE'];
                 this.begbalComponentUpload.dtBPKBReceipt = element['BPKBRECEIPTDATE'];
                 this.begbalComponentUpload.BPKBNumber = element['BPKBNUMBER'];
                 this.begbalComponentUpload.dtBPKBDeliveryCust = element['BPKBDELIVERCUSTOMERDATE'];
                 this.begbalComponentUpload.dtBPKBDeliveryLea = element['BPKBDELIVERLEASINGDATE'];
                 this.begbalComponentUpload.idInternal = this.principal.getIdInternal();
                 this.begbalComponentUpload.InvoiceNumber = element['INVOICE'];
                 this.begbalComponentUpload.SalesName = element['SALESNAME'];
                 this.begbalComponentUpload.dtFakturReceipt = element['AFIRECEIPTDATE'];
                 this.begbalComponentUpload.saleType = element['JENISPENJUALAN'];
                 this.begbalComponentsUpload = [...this.begbalComponentsUpload, this.begbalComponentUpload];

                 if (element['STNKKTP'] === null && element['STNKKTP'] === undefined) {
                    this.invalidFormat = true;
                    this.toasterService.showToaster('Info', 'KTP Kosong Gan', 'KTP nya Kosong gan' )
                 }
             });
         } else if (this.containerData[0]['STNKKTP']) {
             this.begbalComponentsUpload = new Array<BegbalComponentUpload>()
             this.containerData.forEach((element) => {
             });
         } else {
             this.invalidFormat = true;
             this.toasterService.showToaster('Info', 'Format Upload Salah', 'Format yang di upload salah Atau Field ada yang belum lengkap ..')
         }
         this.clear();
     }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    // trackId(index: number, item: PriceComponent) {
    //     return item.idPriceComponent;
    // }

    // registerChangeInPriceComponents() {
    //     this.eventSubscriber = this.eventManager.subscribe('priceComponentListModification', (response) => this.loadAll());
    // }

    // sort() {
    //     const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    //     if (this.predicate !== 'idPriceComponent') {
    //         result.push('idPriceComponent');
    //     }
    //     return result;
    // }

     saveList(): void {
         console.log('save list: ', this.begbalComponentsUpload);
         this.vehicleDocumentRequirementService.queryUpload(this.begbalComponentsUpload).subscribe(
             (res) => {
                 this.toasterService.showToaster('info', 'Upload Data Berhasil', 'Data saved..');
                this.clear();
                 this.loadAll();
             },
             (err) => {
                    this.toasterService.showToaster('info', err.json.message, 'Nomor Ktp Belum ada atau Ktp beda internal dengan File yang akan di Upload');
                    this.commonUtil.showError(err.json);
             }
         );
        this.clear();
        this.loadAll();
     }

    // protected onSuccess(data, headers) {
    //     this.links = this.parseLinks.parse(headers.get('link'));
    //     this.totalItems = headers.get('X-Total-Count');
    //     this.queryCount = this.totalItems;
    //     // this.page = pagingParams.page;
    //     this.priceComponents = data;
    // }

    // protected onError(error) {
    //     this.alertService.error(error.message, null, null);
    // }

    // executeProcess(data) {
    //     this.priceComponentService.executeProcess(0, null, data).subscribe(
    //         (value) => console.log('this: ', value),
    //         (err) => console.log(err),
    //         () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    // loadDataLazy(event: LazyLoadEvent) {
    //     this.itemsPerPage = event.rows;
    //     this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

    //     if (event.sortField !== undefined) {
    //         this.predicate = event.sortField;
    //         this.reverse = event.sortOrder;
    //     }
    //     this.loadAll();
    // }

    // updateRowData(event) {
    //     if (event.data.id !== undefined) {
    //         this.priceComponentService.update(event.data)
    //             .subscribe((res: PriceComponent) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     } else {
    //         this.priceComponentService.create(event.data)
    //             .subscribe((res: PriceComponent) =>
    //                 this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    //     }
    // }

    // protected onRowDataSaveSuccess(result: PriceComponent) {
    //     this.toasterService.showToaster('info', 'PriceComponent Saved', 'Data saved..');
    // }

    // protected onRowDataSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.onError(error);
    // }

    // delete(id: any) {
    //     this.confirmationService.confirm({
    //         message: 'Are you sure that you want to delete?',
    //         header: 'Confirmation',
    //         icon: 'fa fa-question-circle',
    //         accept: () => {
    //             this.priceComponentService.delete(id).subscribe((response) => {
    //                 this.eventManager.broadcast({
    //                     name: 'priceComponentListModification',
    //                     content: 'Deleted an priceComponent'
    //                 });
    //             });
    //         }
    //     });
    // }
}
