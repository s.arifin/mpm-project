import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService, FileUploadModule } from 'primeng/primeng';
import { Http } from '@angular/http';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-registration-penerimaan-faktur',
    templateUrl: './registration-penerimaan-faktur.component.html'
})
export class RegistrationPenerimaanFakturComponent implements OnInit, OnDestroy {
    idInternal: string;
    currentAccount: any;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    selected: VehicleDocumentRequirement[];
    tempArray: VehicleDocumentRequirement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    cancelModal: boolean;

    datas: any;

    note: string;

    constructor(
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected http: Http,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirements = [];
        this.tempArray = [];
        this.cancelModal = false;
        this.selected = new Array<VehicleDocumentRequirement>();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                idStatusType: 43,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        // TODO: EAP
        this.vehicleDocumentRequirementService.queryFilterBy({
            filterName: 'byInternalStatus',
            idStatusType: 43,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    // } finally {
    //     this.loadingService.loadingStop();
    // }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-document-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-document-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.idInternal = this.idInternal;
            this.loadAll();
        });
        this.registerChangeInVehicleDocumentRequirements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInVehicleDocumentRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleDocumentRequirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleDocumentRequirements = data;
    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'VehicleDocumentRequirement Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    refreshData(data: any, item: any, index: any) {
        if (data.vehicle.idFrame === item.vehicle.idFrame && data.vehicle.idMachine === item.vehicle.idMachine) {
            this.vehicleDocumentRequirements.splice(index, 1);

            this.tempArray = [];
            this.tempArray = this.vehicleDocumentRequirements;

            this.vehicleDocumentRequirements = [];
            this.vehicleDocumentRequirements = this.vehicleDocumentRequirements.concat(this.tempArray);
        }
    }

    doReceive(data: any) {
        this.vehicleDocumentRequirement = data;
        this.vehicleDocumentRequirementService.executeProcess(102, null, this.vehicleDocumentRequirement).subscribe(
            (value) => {
                this.vehicleDocumentRequirements.forEach((item, index) => {
                    this.refreshData(value, item, index);
                });
            },
            (err) => console.log(err),
            () => {
                this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..');
            }
        );
    }

    showCancelModal() {
        this.cancelModal = true;
        // this.vehicleDocumentRequirement = data;
    }

    // remark ganti fungsi ke bentuk list
    // doCancel() {
    //     this.vehicleDocumentRequirement.note = this.note;
    //     this.vehicleDocumentRequirementService.executeProcess(104, null, this.vehicleDocumentRequirement).subscribe(
    //         (value) => {
    //             console.log(value);
    //             this.vehicleDocumentRequirements.forEach((item, index) => {
    //                 this.refreshData(value, item, index);
    //             });
    //             this.cancelModal = false;
    //         },
    //         (err) => console.log(err),
    //         () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
    //     );
    // }

    changeListener(files: FileList) {
        console.log(files);
        if (files && files.length > 0) {
            const file: File = files.item(0);
             console.log(file.name);
             console.log(file.size);
             console.log(file.type);
             const reader: FileReader = new FileReader();
             reader.readAsText(file);
             reader.onload = (e) => {
                const csv: string = reader.result.split('/\r\n|\n/');
                reader.result.split(',');
                console.log(csv);
                // this.vehicleDocumentRequirements= [... reader.result.split('/\r\n|\n/')];
             }
          }
      }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            this.datas = e.target.result.split('/\r\n|\n/');
            this.vehicleDocumentRequirements = [];

            this.datas.forEach((item) => {
                this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
                try {
                    item = item.split(',');
                    this.vehicleDocumentRequirement.submissionNo = item[0];
                    this.vehicleDocumentRequirement.dateSubmission = new Date(item[1].substring(0, 4).toString() + '-' +
                           item[1].substring(4, 6).toString() + '-' + item[1].substring(6, 8).toString());
                    this.vehicleDocumentRequirement.vehicle.idFrame = item[2];
                    this.vehicleDocumentRequirement.vehicle.idMachine = item[3];
                    this.vehicleDocumentRequirement.fakturATPM = item[4];
                    this.vehicleDocumentRequirement.fakturDate = new Date(item[5].substring(0, 4).toString() + '-' +
                            item[5].substring(4, 6).toString() + '-' + item[5].substring(6, 8).toString());
                    this.vehicleDocumentRequirement.note = item[6];

                    this.vehicleDocumentRequirements = [...this.vehicleDocumentRequirements, this.vehicleDocumentRequirement];
                } catch (e) {
                    this.toasterService.showToaster('error', 'Error', 'Struktur data salah');
                }

            });
        };
        reader.readAsBinaryString(target.files[0]);
    }

    saveUploadFaktur() {
        this.vehicleDocumentRequirementService.executeListProcess(108, null, this.selected)
        .subscribe(
            (value) => {
                console.log('this: ', value);
                this.selected = new Array<VehicleDocumentRequirement>();
                this.loadAll();
            },
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        )
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleDocumentRequirementListModification',
                    content: 'Deleted an vehicleDocumentRequirement'
                    });
                });
            }
        });
    }

    mappingNote(note): Promise<any> {
        return new Promise(
            (resolve) => {
            console.log('ceek note di mapping -->', note );
            const vend = this.selected;
            vend.forEach (
                (res) => {
                    res.note = note;
                }
            )
                resolve();
            }
        )
    }

    listCancelFaktur(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk list cancel faktur', this.selected);
                this.vehicleDocumentRequirementService.executeListProcess(104, null, this.selected)
                .subscribe(
                    (value) => {
                        console.log('thissssss: ', value);
                        this.selected = new Array<VehicleDocumentRequirement>();
                        this.loadAll();
                    },
                    (err) => console.log(err),
                    () => {
                        this.vehicleDocumentRequirements = new Array<VehicleDocumentRequirement>();
                        this.cancelModal = false;
                        this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..');
                    }
                )
            resolve();
            }
        )
    }

    doCancel(note) {
        console.log('ceek note --->', note);
        this.mappingNote(note)
        .then(
            (value) => this.listCancelFaktur()
        )
    }

}
