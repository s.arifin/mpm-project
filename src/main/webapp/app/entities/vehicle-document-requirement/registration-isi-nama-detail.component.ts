import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model'
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service'
import { SalesUnitRequirement} from '../sales-unit-requirement';
import { VehicleSalesOrderService} from '../vehicle-sales-order';
import { ToasterService} from '../../shared';
import { OrderItem, OrderItemService } from '../order-item';
import { PersonalCustomer, PersonalCustomerService} from '../personal-customer';
import { PaymentApplication, PaymentApplicationService } from '../payment-application';
import { Facility, FacilityService } from '../facility';
import { ResponseWrapper, Principal } from '../../shared';

@Component({
    selector: 'jhi-registration-isi-nama-detail',
    templateUrl: './registration-isi-nama-detail.component.html'
})
export class RegistrationIsiNamaDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    isSaving: boolean;

    orderitems: OrderItem[];
    personalCustomer: PersonalCustomer;
    personalCustomers: PersonalCustomer[];
    salesUnitRequirement: SalesUnitRequirement;

    paymentapplications: PaymentApplication[];

    facilities: Facility[];

    vehicleDocumentRequirements: VehicleDocumentRequirement[];

    idOrder: any;

    constructor(
        protected alertService: JhiAlertService,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected orderItemService: OrderItemService,
        protected principal: Principal,
        protected personalCustomerService: PersonalCustomerService,
        protected paymentApplicationService: PaymentApplicationService,
        protected facilityService: FacilityService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.salesUnitRequirement = new SalesUnitRequirement();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
            if (params['idOrder']) {
                this.idOrder = params['idOrder'];
            }
        });
        this.isSaving = false;
        // this.orderItemService.query()
        //     .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentApplicationService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentapplications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.vehicleDocumentRequirementService.query()
        //     .subscribe((res: ResponseWrapper) => { this.vehicleDocumentRequirement = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.vehicleDocumentRequirementService.find(id).subscribe((requirement) => {
            console.log('requirement', requirement);
            this.vehicleDocumentRequirement = requirement;
            this.vehicleSalesOrderService.findSurByOrderId(this.idOrder, this.principal.getIdInternal()).subscribe((sur) => {
                console.log('sur', sur);
                this.salesUnitRequirement = sur;
            })
            // console.log('-----> vehicleDocumentRequirement:', this.vehicleDocumentRequirement);
            // console.log('-----> salesUnitRequirement:', this.vehicleDocumentRequirement.salesUnitRequirement);
        });
    }

    previousState() {
        this.router.navigate(['/vehicle-document-requirement/registration/isi-nama']);
    }

    save() {
        this.isSaving = true;
        if (this.vehicleDocumentRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.update(this.vehicleDocumentRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.create(this.vehicleDocumentRequirement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<VehicleDocumentRequirement>) {
        result.subscribe((res: VehicleDocumentRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: VehicleDocumentRequirement) {
        this.eventManager.broadcast({ name: 'requirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requirement saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    trackPaymentApplicationById(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackRequirementById(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
