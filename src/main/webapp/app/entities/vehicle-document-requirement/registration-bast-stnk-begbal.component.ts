import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

// import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
// import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement/vehicle-document-requirement.service';
// import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';
// import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import * as BaseConstant from '../../shared/constants/base.constants';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement/vehicle-document-requirement.model';

@Component({
    selector: 'jhi-registration-bast-stnk-begbal',
    templateUrl: './registration-bast-stnk-begbal.component.html'
})
export class RegistrationBASTSTNKBegbalComponent implements OnInit {

    currentAccount: any;
    // unitDeliverable: UnitDeliverable;
    // unitDeliverableParameters: UnitDeliverableParameters;
    // unitDeliverableUpdate: UnitDeliverable;
    // unitDeliverables: UnitDeliverable[];
    // dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    TotalData: VehicleDocumentRequirement;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    selectSales: any;
    pilihSales: any;
    detailModal: boolean;
    printModal: boolean;
    revisiModal: boolean;
    vehicleDocumentRequirementbegbalupdate: VehicleDocumentRequirement;
    vehicleDocumentRequirementbegbal: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalAmbil: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalListRef: VehicleDocumentRequirement[];
    vehicleDocumentRequirementBelumJadi: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalSales: VehicleDocumentRequirement[];

    // statusTypes: StatusType[];
    salesmans: Salesman[];
    selected: VehicleDocumentRequirement[];
    bastTo: string;
    idframe: string;
    iddeltype: string;
    name: string;
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    filtered: any;

    constructor(
        // protected unitDeliverableService: UnitDeliverableService,
        // protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.printModal = false;
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.selected = new Array<VehicleDocumentRequirement>();
        // this.bastTo = 'salesman';
        // this.bastKembali = 0;
        // this.unitDeliverable = new UnitDeliverable();
        // this.unitDeliverableUpdate = new UnitDeliverable();
        // this.unitDeliverables = new Array<UnitDeliverable>();
        this.isFiltered = false;
        this.salesmans = new Array<Salesman>();
    }

    loadAll() {
        console.log('is filter load = ', this.isFiltered);
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.loadingService.loadingStart();
        this.vehicleDocumentRequirementService.queryStnkListRefrence({
            query: 'idDelType:102|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 10000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessListRefrence(res.json, res.headers), console.log('sukses', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal', res) }
        );
        console.log('vdr', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }
        this.vehicleDocumentRequirementService.queryC({
            query: 'idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 10000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers), console.log('sukses', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal', res) }
        );
        this.vehicleDocumentRequirementService.queryStnkBelumJadi({
            query: 'idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 20000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessBelumJadi(res.json, res.headers), console.log('SUKSES BPKB BELUM JADI', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('GAGAL BPKB BELUM JADI', res) }
        );
        console.log('vdr', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }
        this.vehicleDocumentRequirementService.queryStnkSudahDiambil({
            query: 'idDelType:102|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 10000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessDiambil(res.json, res.headers), console.log('sukses sudah diambil', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal sudah di ambil', res) }
        );
        this.vehicleDocumentRequirementService.ambilSales({
            query: 'idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 10000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessSales(res.json, res.headers), console.log('sukses sudah sales', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal sudah di ambil sales jnkc', res) }
        );
        console.log('vdr yang sudah di ambil', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }
        console.log('salesman', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }

        if (this.isFiltered === true) {
        }
    }

    private loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.principal.getIdInternal();
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idCoordinator: this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                this.principal.getIdInternal();
                console.log('selected Sales', res.json);
                this.salesmans = res.json;
            }
        )
    }
    filter() {
        // this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/VEHICLE-DOCUMENT-REQUIREMENT/REGISTRATION/BAST/STNK/begbal'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'ASC' : 'DESC')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.filtered = '';
        this.isFiltered = false;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal']);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadKorsal();
        });
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbal = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbal);
    }
    protected onSuccessBelumJadi(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementBelumJadi = data;
        console.log('MUNCUL BPKB BELUM JADI', this.vehicleDocumentRequirementBelumJadi);
    }
    protected onSuccessDiambil(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbalAmbil = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbalAmbil);
    }
    protected onSuccessSales(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbalSales = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbalSales);
    }
    protected onSuccessListRefrence(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbalListRef = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbalListRef);
    }
    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    trackBySales(index: number, item: Salesman) {
        return item.partyName;
    }
    executeProcess(data) {
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
    }
    // addUpdatePrint() {
    //     this.updatePrint = [...
    //     console.log('array update print ', this.updatePrint);
    //     console.log ('cobaaa notice', this.vehicleDocumentRequirementbegbalupdate.gantiNotice);
    // }
    updateTanggalTerima(nama: any, ktp: any, nomorHp: any) {
        this.printModal = false;
        this.vehicleDocumentRequirementService.queryUpdateTanggal({
            idframe: this.idframe,
            name: nama,
            identitynumber: ktp,
            cellphone: nomorHp,
            idTag: 'STD',
            idInternal: this.principal.getIdInternal()
        }).subscribe(
            (res) => {
                console.log('muncul dong', res);
                this.siapPrint(name);
            })
    }
    updateTerimaSales() {
        this.printModal = false;
        this.vehicleDocumentRequirementService.queryUpdateTerimaSales({
            fname: this.pilihSales,
            idframe: this.idframe,
            idTag: 'STD',
            idInternal: this.principal.getIdInternal()
        }).subscribe(
            (res) => {
                console.log('muncul dong BAST STNK KE SALES', res);
                this.siapPrintSales(this.pilihSales);
            })
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
        });
    }

    showDetail(idreq: any) {
        this.vehicleDocumentRequirementService.find(idreq).subscribe(
            (response) => {
                this.vehicleDocumentRequirementbegbalupdate = response;
                console.log('cekkk ---->', this.vehicleDocumentRequirementbegbal);
            });
        this.detailModal = true;
    }

    // showPrint(id: any) {
    //     this.printModal = true;
    // }
    showPrintModal(idframe: string, deltipe: any) {
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.printModal = true;
        console.log('pop up dialog muncul', idframe)
    }
    print(idframe: string, deltipe: any) {
        this.printModal = true;
        this.idframe = idframe;
        this.iddeltype = deltipe;
        console.log('dapat nokaaaa', this.idframe)
        console.log('dapat deltipeeee', this.iddeltype)
        // this.updateTanggalTerima(noka, deltipe);
    }

    siapPrint(nama: any) {
        this.name = nama;
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/bast_stnk_internal_begbal/pdf', { idframe: this.idframe, name: nama });
        console.log('Siapprint idframe', this.idframe);
        console.log('DAPAT NAMA NYA', this.name);
        console.log('SIAPdeltipe', this.iddeltype);
        this.loadAll();
    }
    siapPrintSales(nama: any) {
        this.name = nama;
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/bast_stnk__begbal_sales/pdf', { idframe: this.idframe, name: nama });
        console.log('Siapprint idframe', this.idframe);
        console.log('DAPAT NAMA NYA', this.name);
        console.log('SIAPdeltipe', this.iddeltype);
        this.loadAll();
    }
    PrintUlang(idframe: any) {
        this.idframe = idframe;
        console.log('print ulang nama', this.pilihSales);
        console.log('print ulang noka', this.idframe);
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/bast_stnk_internal_begbal/pdf', { idframe: this.idframe });
        console.log('print ulang', this.idframe);
        console.log('SIAPdeltipe', this.iddeltype);
        this.loadAll();
    }
    PrintUlangSales(idframe: any) {
        this.idframe = idframe;
        console.log('print ulang nama', this.pilihSales);
        console.log('print ulang noka', this.idframe);
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/bast_stnk__begbal_sales/pdf', { idframe: this.idframe });
        console.log('print ulang', this.idframe);
        console.log('SIAPdeltipe', this.iddeltype);
        this.loadAll();
    }

    doPrint() {
        this.printModal = false;
    }
    listUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                resolve();
            }
        )
    }
    showRevisi(id: any) {
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }
}
