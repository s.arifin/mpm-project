import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { CommunicationEventCDB } from '../../communication-event-cdb/communication-event-cdb.model';
import { CommunicationEventCDBService } from '../../communication-event-cdb/communication-event-cdb.service';
import { PersonalCustomer, PersonalCustomerService, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component/index';
import { ReligionType, ReligionTypeService } from '../../religion-type';
import { WorkType, WorkTypeService } from '../../work-type';
import { SalesUnitRequirement } from '../../sales-unit-requirement'
import { PostalAddress } from '../../postal-address';
import { Person } from '../../person';
import * as SaleTypeConstants from '../../../shared/constants/sale-type.constants';
import { LoadingService } from '../../../layouts/loading/loading.service';
import * as reqType from '../../../shared/constants/requirement-type.constants';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement.model';

@Component({
    selector: 'jhi-isinama-edit-data-cdb-gc',
    templateUrl: './isinama-edit-data-cdb-gc.component.html'
})
export class IsiNamaEditDataCdbOrganizationComponent implements OnInit, OnDestroy,  OnDestroy, OnChanges, DoCheck  {

    @Input() idCustomer: any;
    @Input() sur: SalesUnitRequirement = new SalesUnitRequirement();
    @Input() editabled: Boolean = true;
    @Input() dataStnk: VehicleDocumentRequirement = new VehicleDocumentRequirement();

    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    private subscription: Subscription;
    communicationEventCDB: CommunicationEventCDB;
    predicate: any;
    reverse: any;
    isSaving: Boolean;
    private eventManager: JhiEventManager
    religions: any;

    // untuk combo
    isDisabled: boolean;
    citizenship: any;
    hps: any;
    selectedValues: any;
    isnew: boolean;
    jenisPembayran: any;
    groupCustomer: any;
    mail: any;
    saleType: any;
    selected: any;
    idJenisPembayran: any;
    groupCust: any;
    isGroup: Boolean;
    postalCode: String = '60000';
    idGroup: any = 'G';
    groupDesc: any = 'Group Customer';
    idHobi: any = 'N';
    idHomeStatus: any = 'N';
    idExpenditur: any = 'N';
    idJob: any = 'N';
    idEducation: any = 'N';
    idInfo: any = 'N';
    idBrandOwner: any = 'N';
    idVehicleType: any = 'N';
    idVehicleUser: any = 'N';
    idBuyFor: any = 'N';
    idReligion: any = 'N';
    idHp: any = 'N';
    pic: any;
    infos: any;

    constructor(
        private alertService: JhiAlertService,
        private communicationEventCDBService: CommunicationEventCDBService,
        private toaster: ToasterService,
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService,
        private loadingService: LoadingService,
    ) {
        this.communicationEventCDB = new CommunicationEventCDB()
        this.predicate = 'idCommunicationEvent';
        this.reverse = 'asc';
        this.isSaving = false;
        this.isDisabled = false;

        this.hps = [
            {value: '1', label: 'Pra Bayar(Isi Ulang)'},
            {value: '2', label: 'Pasca Bayar'},
            {value: 'N', label: 'Tidak memiliki HP'},
        ];

        this.citizenship = [
            {value: '1', label: 'WNI'},
            {value: '2', label: 'WNA'},
        ];

        this.infos = [
            {value: 'Y', label: 'Ya'},
            {value: 'N', label: 'Tidak'},
        ];
    }

    ngOnInit() {    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.idCustomer !== undefined) {
            console.log('cek id cust di detail cdb', this.idCustomer);
            console.log('cek datastnk di detail cdb', this.dataStnk);

            this.load(this.idCustomer);
            this.mail = this.dataStnk.organizationOwner.picMail;
            this.pic = this.dataStnk.organizationOwner.picName;
            this.getSaleType();
        }
    }

    ngOnDestroy() {
    }

    load(id) {
        this.subscription = this.communicationEventCDBService.queryByIdCustomer({
            idCustomer: this.idCustomer,
            page: 0,
            size: 1,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccess(res.json, res.headers)
                },
                (res: ResponseWrapper) => {
                    console.log('WAKWAU GAGAL', res);
                    this.onError(res.json)
                }
            );
    }

    private onSuccess(data, headers) {
        console.log('on sakses data', data);
        if (data.length > 0) {
            this.communicationEventCDB = data[0];
            this.isDisabled = true;
        } else {
            this.communicationEventCDB = new CommunicationEventCDB();
            this.isDisabled = false;
        }

    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idCommunicationEvent') {
            result.push('idCommunicationEvent');
        }
        return result;
    }

    submitSurvey(): void {
            const alamat = this.dataStnk.organizationOwner.postalAddress.address1;
            const alamat2 = this.dataStnk.organizationOwner.postalAddress.address2;
            this.loadingService.loadingStart();
            const custId = this.idCustomer;
            this.communicationEventCDB.idCustomer = custId ;
            this.communicationEventCDB.email = this.mail;
            if (alamat2 != null) {
                this.communicationEventCDB.address = alamat.concat('~').concat(alamat2);
            } else {
                this.communicationEventCDB.address = alamat;
            }
            this.communicationEventCDB.idProvince = this.dataStnk.organizationOwner.postalAddress.provinceId;
            this.communicationEventCDB.idCity = this.dataStnk.organizationOwner.postalAddress.cityId;
            this.communicationEventCDB.idDistrict = this.dataStnk.organizationOwner.postalAddress.districtId;
            this.communicationEventCDB.idVillage = this.dataStnk.organizationOwner.postalAddress.villageId;
            this.communicationEventCDB.jenisPembayaran = this.idJenisPembayran;
            this.communicationEventCDB.postalCode = this.postalCode;
            this.communicationEventCDB.buyFor = this.idBuyFor;
            this.communicationEventCDB.expenditureOneMonth = this.idExpenditur;
            this.communicationEventCDB.groupCustomer = this.idGroup;
            this.communicationEventCDB.hobby = this.idHobi;
            this.communicationEventCDB.homeStatus = this.idHomeStatus;
            // this.communicationEventCDB.idReligion = this.idReligion;
            this.communicationEventCDB.job = this.idJob;
            this.communicationEventCDB.lastEducation = this.idEducation;
            this.communicationEventCDB.vehicleBrandOwner = this.idBrandOwner;
            this.communicationEventCDB.vehicleTypeOwner = this.idVehicleType;
            this.communicationEventCDB.vehicleUser = this.idVehicleUser;
            // this.communicationEventCDB.mobilePhoneOwnershipStatus = this.idHp;
            this.cekCheckBox(this.communicationEventCDB);
            if (this.communicationEventCDB.idCommunicationEvent  == null) {
                this.subscribeToSaveResponse(
                    this.communicationEventCDBService.create(this.communicationEventCDB));
                    this.loadingService.loadingStop();
            } else {
                this.subscribeToSaveResponse(
                    this.communicationEventCDBService.update(this.communicationEventCDB));
                    this.loadingService.loadingStop();
            }
    }

    ngDoCheck(): void {
            if ( this.communicationEventCDB.addressSurat == null ||
                 this.communicationEventCDB.addressSurat === '' ||
                 this.communicationEventCDB.idProvinceSurat == null ||
                 this.communicationEventCDB.idProvinceSurat === '' ||
                 this.communicationEventCDB.idCitySurat == null ||
                 this.communicationEventCDB.idCitySurat === '' ||
                 this.communicationEventCDB.idDistrictSurat == null ||
                 this.communicationEventCDB.idDistrictSurat === '' ||
                 this.communicationEventCDB.idVillageSurat == null ||
                 this.communicationEventCDB.idVillageSurat === ''
                ) {
                this.isSaving = true;
            }else {
                this.isSaving = false;
            }
        }

    private cekCheckBox(data: CommunicationEventCDB): CommunicationEventCDB {
        if (this.communicationEventCDB.isMailAddress === true  ) {
            this.communicationEventCDB.isMailAddress = 1;
        }
        if (this.communicationEventCDB.isCityzenIdCardAddress === true) {
            this.communicationEventCDB.isCityzenIdCardAddress = 1;
        }

        if (this.communicationEventCDB.isMailAddress === false  ) {
            this.communicationEventCDB.isMailAddress = 0;
        }
        if (this.communicationEventCDB.isCityzenIdCardAddress === false) {
            this.communicationEventCDB.isCityzenIdCardAddress = 0;
        }
        return data;
    }
    private subscribeToSaveResponse(result: Observable<CommunicationEventCDB>) {
        result.subscribe((res: CommunicationEventCDB) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CommunicationEventCDB) {
        this.toaster.showToaster('info', 'Save', 'Detail CD saved !');
        this.isSaving = false;
        this.communicationEventCDB = result;
        // this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    getVillage(event): void {
        this.communicationEventCDB.idVillage = event;
    }

    getDistrict(event): void {
        this.communicationEventCDB.idDistrict = event;
    }

    getProvince(event): void {
        this.communicationEventCDB.idProvince = event;
    }

    getCity(event): void {
        this.communicationEventCDB.idCity = event;
    }

    getDistrictSurat(event): void {
        this.communicationEventCDB.idDistrictSurat = event;
    }

    getProvincesurat(event): void {
        this.communicationEventCDB.idProvinceSurat = event;
    }

    getCitysurat(event): void {
        this.communicationEventCDB.idCitySurat = event;
    }

    getVillageSurat(event): void {
        this.communicationEventCDB.idVillageSurat = event;
    }

    public onChangeCheckBoxKtp() {
            this.communicationEventCDB.idDistrictSurat = null;
            this.communicationEventCDB.idProvinceSurat = null;
            this.communicationEventCDB.idCitySurat = null;
            this.communicationEventCDB.addressSurat = null;
            this.communicationEventCDB.idVillageSurat = null;

            this.communicationEventCDB.idDistrictSurat = this.dataStnk.organizationOwner.postalAddress.districtId;
            this.communicationEventCDB.idProvinceSurat = this.dataStnk.organizationOwner.postalAddress.provinceId;
            this.communicationEventCDB.idCitySurat = this.dataStnk.organizationOwner.postalAddress.cityId;
            if (this.dataStnk.organizationOwner.postalAddress.address2 != null) {
                this.communicationEventCDB.addressSurat = this.dataStnk.organizationOwner.postalAddress.address1.concat('~').concat(this.dataStnk.organizationOwner.postalAddress.address2);
            } else {
                this.communicationEventCDB.addressSurat = this.dataStnk.organizationOwner.postalAddress.address1;
            }
            this.communicationEventCDB.idVillageSurat = this.dataStnk.organizationOwner.postalAddress.villageId;
            this.communicationEventCDB.postalCodeSurat = this.postalCode;
    }

    public onChangeCheckBoxSurat() {
        this.communicationEventCDB.idDistrictSurat = null;
        this.communicationEventCDB.idProvinceSurat = null;
        this.communicationEventCDB.idCitySurat = null;
        this.communicationEventCDB.addressSurat = null;
        this.communicationEventCDB.idVillageSurat = null;
        this.communicationEventCDB.postalCodeSurat = null;
    }

    getSaleType() {
        if (this.sur.saleTypeId === SaleTypeConstants.CASH ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_CBD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_COD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD_CBD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD_COD) {
            this.jenisPembayran = [
                {value: '1', label: 'CASH'}
            ];
            this.selected = 'CASH';
            this.idJenisPembayran = 1;
        }
        if (this.sur.saleTypeId === SaleTypeConstants.CREDIT ||
            this.sur.saleTypeId === SaleTypeConstants.CREDIT_CBD ||
            this.sur.saleTypeId === SaleTypeConstants.CREDIT_COD) {
            this.jenisPembayran = [
                {value: '2', label: 'REGULER LEASING'}
            ];
            this.selected = 'REGULER LEASING';
            this.idJenisPembayran = 2;
        }
    }

    public selectGroup(event) {
        if (event === 'G') {
            this.isGroup = true;
        } else {
            this.isGroup = false;
        }
       ;
    }
}
