import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';
import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-registration-bast-stnk-internal',
    templateUrl: './registration-bast-stnk-internal.component.html'
})
export class RegistrationBASTSTNKInternalComponent implements OnInit, OnDestroy, DoCheck {

    currentAccount: any;
    unitDeliverable: UnitDeliverable;
    unitDeliverableParameters: UnitDeliverableParameters;
    unitDeliverableUpdate: UnitDeliverable;
    unitDeliverables: UnitDeliverable[];
    dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    detailModal: boolean;
    printModal: boolean;
    revisiModal: boolean;

    statusTypes: StatusType[];
    salesmans: Salesman[];

    bastTo: string;
    bastKembali: any;
    bastSalesKembali: boolean;
    selected: UnitDeliverable[];
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    filtered: any;
    chelist: boolean;

    constructor(
        protected unitDeliverableService: UnitDeliverableService,
        protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.printModal = false;
        this.bastTo = 'salesman';
        this.bastKembali = 0;
        this.unitDeliverable = new UnitDeliverable();
        this.unitDeliverableUpdate = new UnitDeliverable();
        this.unitDeliverables = new Array<UnitDeliverable>();
        this.selected = new Array<UnitDeliverable>();
        this.isFiltered = false;
        this.chelist = true;
    }

    loadAll() {
        console.log('is filter load = ', this.isFiltered );
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.unitDeliverableService.searchSTNK({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }

        if (this.isFiltered === true) {
                this.loadingService.loadingStart();
                this.unitDeliverableService.queryFilterBy({
                    unitDeliverablePTO: this.unitDeliverableParameters ,
                    idInternal: this.principal.getIdInternal(),
                    idDeliverableType: UnitDeliverableConstant.BAST_NOTICE,
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => {this.onSuccessFilter(res.json, res.headers), this.loadingService.loadingStop()},
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }

                this.unitDeliverableService.query({
                    idDeliverableType: UnitDeliverableConstant.BAST_NOTICE,
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
        }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    ngDoCheck(): void {
        if (this.bastTo === 'konsumen') {
        if (this.unitDeliverableUpdate.name == null || this.unitDeliverableUpdate.name === '') {
            this.chelist = true;
        }else {
            this.chelist = false;
        }
        if (this.unitDeliverable.identityNumber == null || this.unitDeliverable.identityNumber === '') {
            this.chelist = true;
        }else {
            this.chelist = false;
        }
        if (this.unitDeliverableUpdate.cellPhone == null || this.unitDeliverableUpdate.cellPhone === '') {
            this.chelist = true;
        }else {
            this.chelist = false;
        }
    }else {
        this.chelist = false;
    }
}

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }
    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        // this.page = 0;
        this.unitDeliverableParameters = new UnitDeliverableParameters();
        this.unitDeliverableParameters.internalId = this.principal.getIdInternal();
        this.unitDeliverableParameters.idDeliverableType = UnitDeliverableConstant.BAST_NOTICE;

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.unitDeliverableParameters.salesmanId = this.selectedSalesman;
        }

        if (this.filtered !== null && this.filtered !== undefined) {
            this.unitDeliverableParameters.dataParam = this.filtered;
        }

        this.unitDeliverableService.queryFilterBy({
            unitDeliverablePTO: this.unitDeliverableParameters,
            idInternal: this.principal.getIdInternal(),
            idDeliverableType: UnitDeliverableConstant.BAST_NOTICE,
            sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.unitDeliverableParameters.orderDateFrom);
        console.log('tanggal 2', this.unitDeliverableParameters.orderDateThru);
        console.log('salesman 2', this.unitDeliverableParameters.salesmanId);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/internal'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        // this.page = 0;
        this.filtered = '';
        this.isFiltered = false;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/internal']);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/internal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadKorsal();
        });
        this.registerChangeInUnitDeliverables();

        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitDeliverable) {
        return item.idDeliverable;
    }

    registerChangeInUnitDeliverables() {
        this.eventSubscriber = this.eventManager.subscribe('unitDeliverableListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idDeliverable') {
            result.push('idDeliverable');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDeliverables = data;
        if (data.length === 0) {
            this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        }
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDeliverables = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.unitDeliverableService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.unitDeliverableService.update(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.unitDeliverableService.create(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: UnitDeliverable) {
        this.toasterService.showToaster('info', 'Unit Deliverable Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitDeliverableService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'unitDeliverableListModification',
                    content: 'Deleted an unitDeliverable'
                    });
                });
            }
        });
    }

    showDetail(idDeliverable: any) {
        // this.detail

        this.unitDeliverableService.find(idDeliverable).subscribe(
            (response) => {
                this.unitDeliverable = response;
                console.log('cekkk ---->', this.unitDeliverable);
        });
        this.detailModal = true;
    }

    showPrint(id: any) {
        // this.detail
        this.unitDeliverableService.find(id).subscribe((response) => {
            this.unitDeliverable = response;
        })
        this.printModal = true;
    }

    showPrintModal() {
        this.unitDeliverableUpdate = new UnitDeliverable();
        this.printModal = true;
    }

    doPrint() {
        console.log('update untuk prinnttt', this.unitDeliverable);
        this.unitDeliverableService.update(this.unitDeliverable).subscribe((response) => {
            this.loadAll();
        });
        this.printModal = false;
    }

    doPrintBast() {
        console.log('cek param nama pengambil', this.unitDeliverableUpdate.name);
        console.log('cek nik', this.unitDeliverableUpdate.identityNumber);
        console.log('cek cellphone', this.unitDeliverableUpdate.cellPhone);
        console.log('cek bast ke siapa', this.bastTo);
        console.log('data pilihan', this.selected);
        console.log('halo', this.bastKembali);
        this.printModal = false;
        this.mappingDataPengambil()
        .then(
            (value) => {
                    this.listUpdateBAST()
            }
        )
    }

    onChangeBastKembali() {
        this.bastSalesKembali = true;
    }

    mappingDataPengambil(): Promise<any> {
        return new Promise(
            (resolve) => {
                const data = this.selected;
                if (this.bastTo === 'konsumen') {
                data.forEach (
                        (res) => {
                            res.identityNumber = this.unitDeliverableUpdate.identityNumber;
                            res.name = this.unitDeliverableUpdate.name;
                            res.cellPhone = this.unitDeliverableUpdate.cellPhone;
                        }
                )
            } else {
                data.forEach (
                    (res) => {
                        res.name = res.salesName;
                    }
                )
            }
                resolve();
            }
        )
    }

    listUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                // this.printModal = false;
                console.log('masuk list update bast', this.selected);
                if (this.bastTo === 'konsumen') {
                this.unitDeliverableService.executeListProcess(102, this.principal.getIdInternal(), this.selected)
                .subscribe(
                    (value) => {
                        this.findOneCus();
                        console.log('thissssss: ', value);
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.unitDeliverables = new Array<UnitDeliverable>();
                        this.unitDeliverable = new UnitDeliverable();
                    }
                )
            } else {
                this.unitDeliverableService.executeListProcess(103, this.principal.getIdInternal(), this.selected)
                .subscribe(
                    (value) => {
                        this.findOneSal();
                        console.log('thissssss: ', value);
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.unitDeliverables = new Array<UnitDeliverable>();
                        this.unitDeliverable = new UnitDeliverable();
                    }
                )
            }
            resolve();
            }
        )
    }

    findOneCus(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk fungsi find');
                console.log('masuk fungsi find CEK SELECTED', this.selected);
                const idDelType = this.selected[0].idDeliverableType;
                const idreq = this.selected[0].vehicleDocumentRequirement.idRequirement;
                this.unitDeliverableService.queryByidReqandIdDeltype({
                    idDeliverableType : idDelType,
                    idReq : idreq
                }).subscribe(
                    (data) => {
                        console.log('hasil unit deliverable', data.json);
                        this.dataUnitDeliverable = data.json;
                        if (this.selected.length <= 2) {
                            console.log('data terpilih kurang dari sama dengan 2');
                            this.printMultiCus(this.dataUnitDeliverable);
                        } else {
                            console.log('data terpilih lebih dari 2');
                            this.printMultipleCus(this.dataUnitDeliverable);
                    }
                }
            )
            resolve()
            }
        )
    }

    findOneSal(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk fungsi find');
                console.log('masuk fungsi find CEK SELECTED', this.selected);
                const idDelType = this.selected[0].idDeliverableType;
                const idreq = this.selected[0].vehicleDocumentRequirement.idRequirement;
                this.unitDeliverableService.queryByidReqandIdDeltype({
                    idDeliverableType : idDelType,
                    idReq : idreq
                }).subscribe(
                    (data) => {
                        console.log('hasil unit deliverable', data.json);
                        this.dataUnitDeliverable = data.json;
                        if (this.selected.length <= 2) {
                            console.log('data terpilih kurang dari sama dengan 2 bast ke sales');
                            this.printMultiSal(this.dataUnitDeliverable)
                        } else {
                            console.log('data terpilih lebih dari 2 bast ke sales');
                            this.printMultipleSal(this.dataUnitDeliverable);
                        }
                    }
            )
            resolve()
            }
        )
    }

    showRevisi(id: any) {
        // this.detail
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    print(idframe: string, namaPengambil: any, noktp: any, hp: any) {
        this.toasterService.showToaster('Infor', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_stnk/pdf', {noka: idframe, nama: namaPengambil, nik: noktp, nohp: hp});
    }

    printMultiCus(data: UnitDeliverable) {
        this.selected = new Array<UnitDeliverable>();
        console.log('masuk untuk print kurang dari atau sama dengan 2', data);
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_stnk/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: data.identityNumber, notlp: data.cellPhone});
    }

    printMultipleCus(data: UnitDeliverable) {
        this.selected = new Array<UnitDeliverable>();
        console.log('masuk untuk printtt jika data lebih dari 2', data);
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_stnk_2/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: data.identityNumber, notlp: data.cellPhone});
    }

    printMultiSal(data: UnitDeliverable) {
        this.selected = new Array<UnitDeliverable>();
        console.log('masuk untuk print kurang dari atau sama dengan 2', data);
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_stnk_sales/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: '-', notlp: '-'});
    }

    printMultipleSal(data: UnitDeliverable) {
        this.selected = new Array<UnitDeliverable>();
        console.log('masuk untuk printtt jika data lebih dari 2', data);
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_stnk_sales_2/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: '-', notlp: '-'});
    }

    updateTglDelivery() {
        if (this.bastSalesKembali === true) {
            this.printModal = false;
            this.unitDeliverableService.executeListProcess(107, this.principal.getIdInternal(), this.selected)
            .subscribe(
                (value) => {
                    console.log('thissssss: ', value);
                    this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                    this.loadAll();
                },
                (err) => {
                    console.log(err);
                    this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                },
                () => {
                    this.unitDeliverables = new Array<UnitDeliverable>();
                    this.selected = new Array<UnitDeliverable>();
                }
            )
        }
    }

    buildReindex() {
        this.unitDeliverableService.process({command: 'buildIndexStnk'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

}
