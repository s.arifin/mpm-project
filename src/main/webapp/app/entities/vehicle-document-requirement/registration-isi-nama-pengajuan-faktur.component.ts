import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { CustomVehicleDocumentRequirement } from './custom-vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { StatusType, StatusTypeService } from '../status-type';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Motor, MotorService } from '../motor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-registration-isi-nama-pengajuan-faktur',
    templateUrl: './registration-isi-nama-pengajuan-faktur.component.html'
})
export class RegistrationIsiNamaPengajuanFakturComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    vehicleDocumentRequirementsCancel: VehicleDocumentRequirement[];
    vehicleDocumentRequirementstmp: VehicleDocumentRequirement[];
    customVehicleDocumentRequirement: CustomVehicleDocumentRequirement[];
    error: any;
    success: any;
    selected: VehicleDocumentRequirement[];
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    cancelModal: boolean;

    downloadModal: boolean;

    statusTypes: StatusType[];
    motors: Motor[];

    udstk: string;
    cddb: string;

    constructor(
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected loadingService: LoadingService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected statusTypeService: StatusTypeService,
        protected motorService: MotorService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.downloadModal = false;
        this.udstk = '';
        this.cddb = '';
        this.selected = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirementstmp = new Array<VehicleDocumentRequirement>();
        this.customVehicleDocumentRequirement = new Array<VehicleDocumentRequirement>();
        this.cancelModal = false;
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleDocumentRequirementService.queryCustom({
            idStatusType: 31,
            statusKonfirmasi: false,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            statusPenerimaan: false,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadAllCancel() {
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccessCancel(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleDocumentRequirementService.queryCustom({
            idStatusType: 0,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccessCancel(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/registration/isi-nama', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/registration/isi-nama', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInRequirements();
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('requirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop(),
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        // this.vehicleDocumentRequirements = data;
        this.customVehicleDocumentRequirement = data;
        console.log('this.custom', this.customVehicleDocumentRequirement);
    }

    protected onSuccessCancel(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleDocumentRequirementsCancel = data;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'Requirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'requirementListModification',
                    content: 'Deleted an requirement'
                    });
                });
            }
        });
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    showDownload(data: any) {
        this.downloadModal = true;
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirement = data;
    }

    processFaktur(idRequirement: string) {
        this.vehicleDocumentRequirement.idRequirement = idRequirement;
        console.log('data', this.vehicleDocumentRequirement);

        this.vehicleDocumentRequirementService.executeProcess(102, null, this.vehicleDocumentRequirement).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    cancelFaktur() {
        this.cancelModal = true;
    }

    cancelDataIVU() {
        this.cancelModal = false;
        for (let i = 0; i < this.selected.length; i++) {
            this.vehicleDocumentRequirementstmp.push({
                idRequirement : this.selected[i].idRequirement
            });
        }
        this.vehicleDocumentRequirementService.executeListProcess(105, null, this.vehicleDocumentRequirementstmp).subscribe(
           (value) => {
               console.log('this: ', value);

               this.loadAll();
           },
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    cancelDataPengajuan() {
        this.cancelModal = false;
        const array = this.selected;
        const unique = Array.from(new Set(array.map((item) => item.registrationNumber)));
        console.log('unique', unique);
        const data = [];
        for (let i = 0; i < unique.length; i++) {
            this.vehicleDocumentRequirementstmp.push({
                registrationNumber : unique[i]
            });
        }
        console.log('this.vehicleDocumentRequirements', this.vehicleDocumentRequirements);
        // for (let i = 0; i < array.length; i++) {
        //     if ( array[i].registrationNumber !== array[i - 1 ].registrationNumber ){
        //         data.push(array[i].registrationNumber)
        //     }
        // }
        this.vehicleDocumentRequirementService.executeListProcess(106, null, this.vehicleDocumentRequirementstmp).subscribe(
            (value) => {
                console.log('this: ', value);
                this.loadAll();
            },
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
         );
    }
    buildReindex() {
        this.vehicleDocumentRequirementService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
