import { BaseEntity } from './../../shared';

export class SummaryRefferal implements BaseEntity {
    constructor(
        public id?: any,
        public refNumber?: any,
        public refDate?: any,
        public idVendor?: any,
        public receiptQty?: any,
        public inputQty?: number,
        public receiptNominal?: number,
    ) {
    }
}
