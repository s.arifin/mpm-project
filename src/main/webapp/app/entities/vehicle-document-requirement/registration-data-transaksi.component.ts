import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { SaleType, SaleTypeService } from '../sale-type';
import { StatusType, StatusTypeService } from '../status-type';
import { Person } from '../person';
import { PostalAddress } from '../postal-address/postal-address.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-registration-data-transaksi',
    templateUrl: './registration-data-transaksi.component.html'
})
export class RegistrationDataTransaksiComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    detailModal: boolean;

    vehicleDocumentRequirement: VehicleDocumentRequirement;
    personOwner: Person;
    postalAddress: PostalAddress;

    saleTypes: SaleType[];
    statusTypes: StatusType[];

    constructor(
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected saleTypeService: SaleTypeService,
        protected statusTypeService: StatusTypeService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;

        this.vehicleDocumentRequirement = new VehicleDocumentRequirement;
        // this.personOwner = new Person;
        // this.postalAddress = new PostalAddress;
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleDocumentRequirementService.query({
            page: this.page - 1,
            idInternal: this.principal.getIdInternal(),
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/requirement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInRequirements();
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saleTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('requirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleDocumentRequirements = data;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'Requirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'requirementListModification',
                    content: 'Deleted an requirement'
                    });
                });
            }
        });
    }

    showDetail(idRequirement: any) {
        // this.detail

        console.log('idRequirement', idRequirement);

        this.vehicleDocumentRequirementService.find(idRequirement).subscribe((response) => {
            console.log('response', response);
            console.log('response.personOwner', response.personOwner);
            // this.vehicleDocumentRequirement = response;
            this.vehicleDocumentRequirement = response;
            // this.personOwner = response.personOwner;
            // this.postalAddress = response.personOwner.postalAddress;
            // console.log('-----> vehicleDocumentRequirement:', this.vehicleDocumentRequirement);
            // console.log('-----> salesUnitRequirement:', this.vehicleDocumentRequirement.salesUnitRequirement);
        });

        this.detailModal = true;
    }

    trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    getSaleTypeDesc(idSaleType: number) {
        // console.log(idSaleType);

        this.saleTypes.forEach(function(saleType) {
            if (saleType.idSaleType === idSaleType) {
                return saleType.description;
            }
        })
    }
}
