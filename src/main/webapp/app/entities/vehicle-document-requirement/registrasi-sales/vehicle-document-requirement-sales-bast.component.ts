import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement.service';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { PaginationConfig } from '../../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../../layouts';
import { CustomVehicleDocumentRequirement, CustomVDR } from '../index';
import { VehicleDocumentRequirementCService } from '../../vehicle-document-requirement-c/index';

@Component({
    selector: 'jhi-vehicle-document-requirement-sales-bast',
    templateUrl: './vehicle-document-requirement-sales-bast.component.html'
})
export class VehicleDocumentRequirementSalesBastComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    customVdrs: CustomVDR[];
    error: any;
    success: any;
    // eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        private vehicleDocumentRequirementCService: VehicleDocumentRequirementCService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        // private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.vehicleDocumentRequirementCService.queryBast({
            query: 'idStatusType:36|idInternal:' + this.principal.getIdInternal() + '|idSalesman:' + this.principal.getUserLogin(),
            page: this.page - 1,
            size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json),
                () => this.loadingService.loadingStop(),
            );
    }

    search(currentSearch): void {

    }

    clear(): void {

    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            console.log('akun di bast-->', this.currentAccount);
            this.loadAll();
        });
    }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
        // this.eventSubscriber.unsubscribe();
    }

    // approveKacab(idVso) {

    // }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        if (data !== null) {
            this.totalItems = data[0].TotalData;
            this.queryCount = this.totalItems;
            console.log('hasil get bast', data);
            this.customVdrs = data;
        }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    getDescription(currentStatus: number) {
        // return this.vehicleDocumentRequirementService.getDescriptionStatus(currentStatus);
    }

}
