import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService, JhiParseLinks, JhiPaginationUtil, JhiLanguageService } from 'ng-jhipster';

import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement.service';
import { LoadingService } from '../../../layouts';
import { ToasterService, Principal } from '../../../shared';
import { SalesUnitRequirement } from '../../sales-unit-requirement';
import { VehicleSalesOrder } from '../../vehicle-sales-order';
import { UnitDeliverable } from '../../unit-deliverable/index';
import { VehicleDocumentRequirementCService } from '../../vehicle-document-requirement-c/index';
import { CustomVDR } from '../index';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-vehicle-document-requirement-sales-view',
    templateUrl: './vehicle-document-requirement-sales-view.component.html'
})
export class VehicleDocumentRequirementSalesViewComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    vehicleSalesOrderId: number;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    dataApproves: any[];
    isEdit: Boolean;
    isGanti: Boolean;
    requirementId: any;
    customVDR: CustomVDR;
    unitDeliverable: UnitDeliverable;
    checkPlatNomor: Boolean;
    checkNotice: Boolean;
    checkStnk: Boolean;
    today: Date;

    constructor(
        private vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        private vehicleDocumentRequirementCService: VehicleDocumentRequirementCService,
        private route: ActivatedRoute,
        private router: Router,
        private datePipe: DatePipe,
        private loadingService: LoadingService,
        private confirmationService: ConfirmationService,
        private toaster: ToasterService,
        private principal: Principal,
    ) {
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.today = new Date();
        this.isGanti = false;
        this.customVDR = new CustomVDR();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.dataApproves = [
            {nama: 'Admin Sales', isApprove: false},
            {nama: 'AFC', isApprove: false},
            {nama: 'KACAB', isApprove: false}
        ]
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.loadingService.loadingStart();

        this.vehicleDocumentRequirementCService.find(id).subscribe(
            (res) => {
               // this.vehicleDocumentRequirement = res;
               this.customVDR = res[0];
                console.log('isi custom vdr', this.customVDR);
                console.log('isi custom vdr notice', this.customVDR.notice);
                console.log('isi custom vdr stnk', this.customVDR.stnk);
                console.log('isi custom vdr plat nomor', this.customVDR.platNomor);
            },
            (err) => {
                console.log('errornya pas ngeload', err);
            },
            () => this.loadingService.loadingStop(),
        );
    }

    previousState() {
        this.router.navigate(['/vehicle-document-requirement/registration-sales']);
    }

    cancelVso() {
        // console.log('Cancel VSO');
        // this.confirmationService.confirm({
        //     message: 'Are you sure that you want to cancel vso ?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         this.vehicleSalesOrderService
        //         .executeProcess( 13, null, this.vehicleSalesOrder)
        //         .subscribe((res) => {
        //             this.toaster.showToaster('info', 'Data Proces', 'Cancel VSO');
        //             console.log('approve');
        //             this.router.navigate(['../../vehicle-sales-order-sales']);
        //     });
        //     },
        //     reject: () => {

        //     }
        // });
    }
    editVso() {
        console.log('Edit VSO');
        // this.confirmationService.confirm({
        //     message: 'Apakah Anda Yakin Akan Edit VSO ?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         // this.router.navigate(['../../sur-vso-sales-edit/' + this.vehicleSalesOrder.salesUnitRequirement.idRequirement + '/edit']);
        //         },
        //     reject: () => {

        //     }
        // });
    }

    gantiVso(data: SalesUnitRequirement, vso: VehicleSalesOrder) {
        // console.log('Ganti VSo');
        // this.confirmationService.confirm({
        //     message: 'Apakah Anda Yakin Akan Ganti VSO ?',
        //     header: 'Confirmation',
        //     icon: 'fa fa-question-circle',
        //     accept: () => {
        //         this.vehicleSalesOrderService.executeProcess(105, null, vso)
        //         .subscribe(
        //             (res) => {
        //                 this.isGanti = true;
        //                 console.log('ganti vsoo ', this.isGanti);
        //                 this.requirementId = data.idRequirement;
        //                 console.log('ganti vsoo idrequest', this.requirementId);
        //                 console.log('gantiiii piesoh ahh  ', res);
        //             }
        //         )
        //         },
        //     reject: () => {

        //     }
        // });
    }

        onChangePlatNomor(checked) {
        if ( checked ) {
            this.customVDR.platNomor = this.datePipe.transform(this.today, 'MM/dd/yyyy hh:mm:ss');
        } else {
            this.customVDR.platNomor = undefined;
        }
    }

    onChangeStnk(checked) {
        if ( checked ) {
            this.customVDR.stnk = this.datePipe.transform(this.today, 'MM/dd/yyyy hh:mm:ss');
        } else {
            this.customVDR.stnk = undefined;
        }
    }

    onChangeNotice(checked) {
        if ( checked ) {
            this.customVDR.notice =  this.datePipe.transform(this.today, 'MM/dd/yyyy hh:mm:ss');
        } else {
            this.customVDR.notice = undefined;
        }
    }

    bastToKonsumen() {
        console.log('cek stnk', this.customVDR.stnk);
        console.log('cek plat nomor', this.customVDR.platNomor);
        console.log('cek notice', this.customVDR.notice);
        console.log('cek custom vdr', this.customVDR);
        this.vehicleDocumentRequirementCService.bastToKonsumen(this.customVDR).subscribe(
            (res) => { console.log('hasil res', res); },
            (err) => { console.log('error', err); },
            () => {
                this.previousState();
            }
        )
    }

}
