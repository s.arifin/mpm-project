import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';

import { SalesUnitRequirement } from '../../sales-unit-requirement';
import { PersonalCustomerService, PersonalCustomer, OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';
import { Person, PersonService } from '../../person';
import { CommonUtilService, ToasterService } from '../../../shared';
import { LoadingService } from '../../../layouts';
import { Organization, OrganizationService } from '../../organization';

@Component({
    selector: 'jhi-isinama-customer-view',
    templateUrl: './isinama-customer-view.component.html'
})

export class IsiNamaCustomerViewComponent implements OnChanges {
    @Input() idCustomer: string;
    @Input() readonly: boolean;
    @Input() idReqTyp: any;
    @Input() index = 0;
    @Input() viewHiddenField: any = 0;

    person: Person;
    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    readonlyDocument: Boolean = true;
    organization: Organization;

    isEdit: Boolean;
    isEditOrganization: Boolean;

    constructor(
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService,
        private commonUtilService: CommonUtilService,
        private personService: PersonService,
        private organizationService: OrganizationService,
        private loadingService: LoadingService,
        private toaster: ToasterService
    ) {
        this.person = new Person();
        this.readonly = false;
        this.personalCustomer = new PersonalCustomer();
        this.organizationCustomer = new OrganizationCustomer();
        this.organization = new Organization();
        this.isEdit = false;
        this.isEditOrganization = false;
    }

    ngOnChanges(changes: SimpleChanges ) {
        // if (this.iidCustomer !== undefined) {
        //     this.personalCustomerService.find(this.iidCustomer).subscribe((res) => {
        //         if (res.person.dob !== null) {
        //             res.person.dob = new Date(res.person.dob);
        //         }

        //         this.person = res.person;
        //     });
        // }
            if (this.idCustomer !== undefined) {
                if (this.idReqTyp === 101 || this.idReqTyp === 103 ) {
                    this.getOrganizationCustomer(this.idCustomer);
                } else if (this.idReqTyp === 102 || this.idReqTyp === null ) {
                    this.getPersonalCustomer(this.idCustomer);
                }
            }
    }

    public updatePerson(person: Person): void {
        console.log('masuk update person customer', person);
        this.loadingService.loadingStart();
        this.personService.updatePerson(person).subscribe(
            (res: Person) => {
                console.log('update person customer', res);
                this.toaster.showToaster('info', 'Update', 'Data customer update !');
                this.loadingService.loadingStop();
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
            }
        )
    }

    public updateOrganisasi(organisasi: Organization): void {
        console.log('masuk update organisasi customer', organisasi);
        this.loadingService.loadingStart();
        this.organizationService.update(organisasi).subscribe(
            (res: Person) => {
                console.log('update organisasi customer', res);
                this.toaster.showToaster('info', 'Update', 'Data organisasi update !');
                this.loadingService.loadingStop();
            },
            (err) => {
                this.loadingService.loadingStop();
                this.commonUtilService.showError(err);
            }
        )
    }

    getPersonalCustomer(id: string): void {
        this.personalCustomerService.find(id).subscribe(
            (res) => {
                if (res.person.dob !== null) {
                    res.person.dob = new Date(res.person.dob);
                }
                this.personalCustomer = res;
                console.log('getPersonalCustomer', this.personalCustomer);
            }
        )
    }
    getOrganizationCustomer(id: string): void {
        this.organizationCustomerService.find(id).subscribe(
            (res) => {
                this.organizationCustomer = res;
                console.log('getOrganizationCustomer', this.organizationCustomer);
            }
        )
    }

    edit(event: Boolean) {
        this.isEdit = event;
    }

    editOrganization(event: Boolean) {
        this.isEditOrganization = event;
    }
};
