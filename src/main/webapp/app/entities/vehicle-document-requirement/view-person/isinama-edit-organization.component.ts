import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService  } from 'ng-jhipster';

import { PostalAddress } from '../../postal-address';
import {Observable} from 'rxjs';

import { Organization, OrganizationService } from '../../organization';
import { OrganizationCustomer, OrganizationCustomerService } from '../../shared-component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PartyService } from '../../party';
import { ToasterService } from '../../../shared';

@Component({
    selector: 'jhi-isinama-edit-organization',
    templateUrl: './isinama-edit-organization.component.html'
})
export class IsiNamaEditOrganizationComponent implements OnInit, OnDestroy, OnChanges {

    @Input() organization: Organization = new Organization();
    @Input() readonly: boolean;
    @Input() organizationCustomer: OrganizationCustomer = new OrganizationCustomer();

    constructor(
        protected organizationCustomerService: OrganizationCustomerService,
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    this.organizationCustomer = new OrganizationCustomer();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['organization']) {
            console.log('organization view global', this.organization);
        }
    }

    ngOnDestroy() {
    }

    getProvince(province: any) {
        this.organization.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.organization.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.organization.postalAddress.cityId = city;
    }

    getVillage(village: any ) {
        this.organization.postalAddress.villageId = village;
    }

    save() {
        if (this.organizationCustomer.idCustomer !== undefined) {
            this.subscribeToSaveResponse(
                this.organizationCustomerService.update(this.organizationCustomer));
        } else {
            this.subscribeToSaveResponse(
                this.organizationCustomerService.create(this.organizationCustomer));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrganizationCustomer>) {
        result.subscribe((res: OrganizationCustomer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: OrganizationCustomer) {
        this.organizationCustomerService.newCustomerRegister(result);
        this.eventManager.broadcast({ name: 'organizationCustomerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'organizationCustomer saved !');
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'organizationCustomer Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

}
