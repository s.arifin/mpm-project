import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService  } from 'ng-jhipster';

import { Person, PersonService } from '../../person';
import { ReligionType, ReligionTypeService } from '../../religion-type';
import { WorkType, WorkTypeService } from '../../work-type';
import { ResponseWrapper, CommonUtilService, ToasterService } from '../../../shared';
import { PersonalCustomerService, PersonalCustomer } from '../../shared-component';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { VehicleSalesOrder } from '../../vehicle-sales-order';
import { LoadingService } from '../../../layouts';

@Component({
    selector: 'jhi-isinama-edit-person',
    templateUrl: './isinama-edit-person.component.html'
})
export class IsiNamaEditPersonComponent implements OnInit, OnDestroy, OnChanges {

    @Input() person: Person = new Person();
    @Input() readonly = false;
    @Input() index = 0;
    @Input() viewHiddenField: any = 0;

    religiontypes: ReligionType[];
    worktypes: WorkType[];

    public yearForCalendar: string

    genders: Array<object> = [
        {label : 'Pria', value : 'P'},
        {label : 'Wanita', value : 'W'}
    ];

    bloodTypes: Array<object> = [
        {label : 'O', value : 'O'},
        {label : 'A', value : 'A'},
        {label : 'B', value : 'B'},
        {label : 'AB', value : 'AB'}
    ];

    constructor(
        private workTypeService: WorkTypeService,
        private religionTypeService: ReligionTypeService,
        private commonUtilService: CommonUtilService,
        private loadingService: LoadingService,
        private personService: PersonService
    ) {
        this.yearForCalendar = this.commonUtilService.getYearRangeForCalendar();
    }

    ngOnInit() {
        this.religionTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.religiontypes = res.json; });
        this.workTypeService.query({size: 100})
            .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['person']) {
            console.log('this.person', this.person);
            if (this.person.dob !== null) {
                const tempDate = new Date(this.person.dob).setHours(23, 59, 59, 99);
                this.person.dob = new Date(tempDate);
            }
        }
    }

    ngOnDestroy() {
    }

    getProvince(province: any) {
        this.person.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.person.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.person.postalAddress.cityId = city;
    }

    getVillage(village: any) {
        this.person.postalAddress.villageId = village;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

    trackReligionTypeById(index: number, item: ReligionType) {
        return item.idReligionType;
    }
}
