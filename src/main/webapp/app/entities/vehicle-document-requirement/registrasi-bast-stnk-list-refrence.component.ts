import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

// import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
// import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement/vehicle-document-requirement.service';
// import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';
// import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import * as BaseConstant from '../../shared/constants/base.constants';
import { VehicleDocumentRequirement } from '../vehicle-document-requirement/vehicle-document-requirement.model';

@Component({
    selector: 'jhi-registrasi-bast-stnk-list-refrence',
    templateUrl: './registrasi-bast-stnk-list-refrence.component.html'
})
export class RegistrationBASTSTNKlistRefrenceComponent implements OnInit {

    currentAccount: any;
    // unitDeliverable: UnitDeliverable;
    // unitDeliverableParameters: UnitDeliverableParameters;
    // unitDeliverableUpdate: UnitDeliverable;
    // unitDeliverables: UnitDeliverable[];
    // dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    TotalData: VehicleDocumentRequirement;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    selectSales: any;
    pilihSales: any;
    detailModal: boolean;
    printModal: boolean;
    revisiModal: boolean;
    vehicleDocumentRequirementbegbalupdate: VehicleDocumentRequirement;
    vehicleDocumentRequirementbegbal: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalAmbil: VehicleDocumentRequirement[];

    // statusTypes: StatusType[];
    salesmans: Salesman[];

    bastTo: string;
    idframe: string;
    iddeltype: string;
    name: string;
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    filtered: any;

    constructor(
        // protected unitDeliverableService: UnitDeliverableService,
        // protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.printModal = false;
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        // this.bastTo = 'salesman';
        // this.bastKembali = 0;
        // this.unitDeliverable = new UnitDeliverable();
        // this.unitDeliverableUpdate = new UnitDeliverable();
        // this.unitDeliverables = new Array<UnitDeliverable>();
        this.isFiltered = false;
        this.salesmans = new Array<Salesman>();
    }

    loadAll() {
        console.log('is filter load = ', this.isFiltered);
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.loadingService.loadingStart();
        this.vehicleDocumentRequirementService.queryC({
            query: 'idDelType:102|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 1000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccess(res.json, res.headers), console.log('sukses', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal', res) }
        );
        console.log('vdr', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }
        this.vehicleDocumentRequirementService.queryStnkSudahDiambil({
            query: 'idDelType:102|idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: 1000,
        }).subscribe(
            (res: ResponseWrapper) => { this.onSuccessDiambil(res.json, res.headers), console.log('sukses sudah diambil', res) },
            (res: ResponseWrapper) => { this.onError(res.json), console.log('gagal sudah di ambil', res) }
        );
        console.log('vdr yang sudah di ambil', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }
        console.log('salesman', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }

        if (this.isFiltered === true) {
        }
    }

    private loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.principal.getIdInternal();
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idCoordinator: this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                this.principal.getIdInternal();
                console.log('selected Sales', res.json);
                this.salesmans = res.json;
            }
        )
    }
    filter() {
        // this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/VEHICLE-DOCUMENT-REQUIREMENT/REGISTRATION/BAST/STNK/begbal'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'ASC' : 'DESC')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.filtered = '';
        this.isFiltered = false;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal']);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadKorsal();
        });
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbal = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbal);
    }
    protected onSuccessDiambil(data, headers) {
        this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbalAmbil = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbalAmbil);
    }
    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }
}
