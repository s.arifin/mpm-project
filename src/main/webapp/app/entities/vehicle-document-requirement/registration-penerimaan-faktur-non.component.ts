import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { Vehicle, VehicleService} from '../vehicle';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-registration-penerimaan-faktur-non',
    templateUrl: './registration-penerimaan-faktur-non.component.html'
})
export class RegistrationPenerimaanFakturNonComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    addVehicleDocumentRequirements: VehicleDocumentRequirement;
    selected: VehicleDocumentRequirement[];
    vehicle: Vehicle;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    newModal: boolean;
    cancelModal: boolean;
    note: any;

    constructor(
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected vehicleService: VehicleService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
    ) {
        this.addVehicleDocumentRequirements = new VehicleDocumentRequirement();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.newModal = false;
        this.selected = new Array<VehicleDocumentRequirement>();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                idStatusType: 42,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleDocumentRequirementService.queryFilterBy({
            filterName: 'byInternalStatus',
            idStatusType: 42,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-document-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-document-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInVehicleDocumentRequirements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInVehicleDocumentRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleDocumentRequirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleDocumentRequirements = data;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'VehicleDocumentRequirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    searchvehicle() {
        this.vehicleDocumentRequirementService.findbyFrame(this.addVehicleDocumentRequirements.vehicle.idFrame).subscribe((VSD) => {
            console.log('vehicle', VSD);
            this.addVehicleDocumentRequirements = VSD;
            if (VSD === null) {
                this.vehicle = null;
            }
        });
    }

    addNewData() {
        this.addVehicleDocumentRequirements = new VehicleDocumentRequirement();
        this.newModal = true;
    }
    saveUploadFaktur() {
        this.vehicleDocumentRequirementService.executeListProcess(108, null, this.selected)
        .subscribe(
            (value) => {
                console.log('this: ', value);
                this.selected = new Array<VehicleDocumentRequirement>();
                this.loadAll();
            },
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        )
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleDocumentRequirementListModification',
                    content: 'Deleted an vehicleDocumentRequirement'
                    });
                });
            }
        });
    }
    saveFaktur() {
        // this.addVehicleDocumentRequirements.vehicle.idMachine = this.vehicle.idMachine;
        this.vehicleDocumentRequirementService.executeProcess(107, null, this.addVehicleDocumentRequirements).subscribe(
            (value) => {
                this.newModal = false;
                console.log('this: ', value);
                this.loadAll();
            },
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
         );
    }

    mappingNote(note): Promise<any> {
        return new Promise(
            (resolve) => {
            console.log('ceek note di mapping -->', note );
            const vend = this.selected;
            vend.forEach (
                (res) => {
                    res.note = note;
                }
            )
                resolve();
            }
        )
    }

    listCancelFaktur(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk list cancel faktur', this.selected);
                this.vehicleDocumentRequirementService.executeListProcess(104, null, this.selected)
                .subscribe(
                    (value) => {
                        console.log('thissssss: ', value);
                        this.selected = new Array<VehicleDocumentRequirement>();
                        this.loadAll();
                    },
                    (err) => console.log(err),
                    () => {
                        this.vehicleDocumentRequirements = new Array<VehicleDocumentRequirement>();
                        this.cancelModal = false;
                        this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..');
                    }
                )
            resolve();
            }
        )
    }

    doCancel(note) {
        console.log('ceek note --->', note);
        this.mappingNote(note)
        .then(
            (value) => this.listCancelFaktur()
        )
    }

    showCancelModal() {
        this.cancelModal = true;
    }
}
