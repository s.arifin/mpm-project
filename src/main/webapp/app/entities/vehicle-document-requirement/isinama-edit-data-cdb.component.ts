import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import { CommunicationEventCDB } from '../communication-event-cdb/communication-event-cdb.model';
import { CommunicationEventCDBService } from '../communication-event-cdb/communication-event-cdb.service';
import { PersonalCustomer, PersonalCustomerService, OrganizationCustomer, OrganizationCustomerService } from '../shared-component/index';
import { ReligionType, ReligionTypeService } from '../religion-type';
import { WorkType, WorkTypeService } from '../work-type';
import { SalesUnitRequirement } from '../sales-unit-requirement'
import { PostalAddress } from '../postal-address';
import { Person } from '../person';
import * as SaleTypeConstants from '../../shared/constants/sale-type.constants';
import { LoadingService } from '../../layouts/loading/loading.service';
import * as reqType from '../../shared/constants/requirement-type.constants';
import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';

@Component({
    selector: 'jhi-isinama-edit-data-cdb',
    templateUrl: './isinama-edit-data-cdb.component.html'
})
export class IsiNamaEditDataCdbComponent implements OnInit, OnDestroy,  OnDestroy, OnChanges, DoCheck  {

    @Input() idCustomer: any;
    @Input() sur: SalesUnitRequirement = new SalesUnitRequirement();
    @Input() editabled: Boolean = true;
    @Input() dataStnk: VehicleDocumentRequirement = new VehicleDocumentRequirement();

    personalCustomer: PersonalCustomer;
    organizationCustomer: OrganizationCustomer;
    private subscription: Subscription;
    communicationEventCDB: CommunicationEventCDB;
    predicate: any;
    reverse: any;
    isSaving: Boolean;
    private eventManager: JhiEventManager
    religions: any;

    // untuk combo
    isDisabled: boolean;
    hobies: any;
    homeStatues: any;
    citizenship: any;
    expenditures: any;
    jobs: any;
    educations: any;
    hps: any;
    infos: any;
    brandOwners: any;
    vehicleTypes: any;
    vehicleUsers: any;
    buyFors: any;
    selectedValues: any;
    isnew: boolean;
    jenisPembayran: any;
    groupCustomer: any;
    religion: ReligionType[];
    religionDesc: any;
    idAgama: any;
    idWork: any;
    workDesc: any;
    mail: any;
    saleType: any;
    selected: any;
    idJenisPembayran: any;
    groupCust: any;
    isGroup: Boolean;
    isJob: Boolean;
    postalCode: String = '60000';

    constructor(
        private alertService: JhiAlertService,
        private communicationEventCDBService: CommunicationEventCDBService,
        private toaster: ToasterService,
        private personalCustomerService: PersonalCustomerService,
        private organizationCustomerService: OrganizationCustomerService,
        private religionTypeService: ReligionTypeService,
        private workTypeService: WorkTypeService,
        private loadingService: LoadingService,
    ) {
        this.religion = new Array<ReligionType>();
        this.communicationEventCDB = new CommunicationEventCDB()
        this.predicate = 'idCommunicationEvent';
        this.reverse = 'asc';
        this.isSaving = false;
        this.isDisabled = false;
        this.isGroup = false;
        this.isJob = false;
        this.hobies = [
            { value: 'A1', label: 'Adventure'},
            { value: 'A10', label: 'Makan'},
            { value: 'A11', label: 'Massage'},
            { value: 'A12',  label: 'Melukis'},
            { value: 'A13',  label: 'Memancing'},
            { value: 'A14',  label: 'Memasak'},
            { value: 'A15',  label: 'Membaca'},
            { value: 'A16',  label: 'Membaca Puisi'},
            { value: 'A17', label: 'Memelihara Binatang Peliharaan' },
            { value: 'A18', label: 'Menanam Bunga' },
            { value: 'A19', label: 'Menari' },
            { value: 'A2', label: 'Aeromodeling' },
            { value: 'A20', label: 'Mendongeng' },
            { value: 'A21', label: 'Mengaji' },
            { value: 'A22', label: 'Mengarang Cerita' },
            { value: 'A23', label: 'Menggambar' },
            { value: 'A24', label: 'Mengoleksi Barang Antik' },
            { value: 'A25', label: 'Menjahit' },
            { value: 'A26', label: 'Menulis Buku' },
            { value: 'A27', label: 'Menyanyi' },
            { value: 'A28', label: 'Origami' },
            { value: 'A29', label: 'Otomotif' },
            { value: 'A3', label: 'Bercocok Tanam' },
            { value: 'A30', label: 'Pantomim' },
            { value: 'A31', label: 'Shopping' },
            { value: 'A32', label: 'Surat Menyurat' },
            { value: 'A33', label: 'Travelling' },
            { value: 'A4', label: 'Berkaraoke' },
            { value: 'A5', label: 'Bermain Drama' },
            { value: 'A6', label: 'Bermain Sulap' },
            { value: 'A7', label: 'Fotografi' },
            { value: 'A8', label: 'Kaligrafi' },
            { value: 'A9', label: 'Koleksi Perangko (Fillateli)' },
            { value: 'B1', label: 'Badminton' },
            { value: 'B10', label: 'Senam' },
            { value: 'B11', label: 'Sepakbola' },
            { value: 'B12', label: 'Sepatu Roda' },
            { value: 'B13', label: 'Surfing' },
            { value: 'B14', label: 'Tennis' },
            { value: 'B15', label: 'Volley' },
            { value: 'B16', label: 'Yoga' },
            { value: 'B2', label: 'Basket' },
            { value: 'B3', label: 'Bersepeda' },
            { value: 'B4', label: 'Bowling' },
            { value: 'B5', label: 'Fitnes' },
            { value: 'B6', label: 'Golf' },
            { value: 'B7', label: 'Hiking' },
            { value: 'B8', label: 'Jogging' },
            { value: 'B9', label: 'Renang' },
            { value: 'C1', label: 'Bermain Game' },
            { value: 'C10', label: 'Menonton TV' },
            { value: 'C2', label: 'Bermain Komputer' },
            { value: 'C3', label: 'Bermain Musik' },
            { value: 'C4', label: 'Browsing Internet' },
            { value: 'C5', label: 'Chatting' },
            { value: 'C6', label: 'Mendengarkan Musik' },
            { value: 'C7', label: 'Mendengarkan Radio' },
            { value: 'C8', label: 'Menonton Bioskop' },
            { value: 'C9', label: 'Menonton Film' }
        ];

        this.homeStatues = [
            {value: '1', label: 'Sendiri'},
            {value: '2', label: 'Orang Tua'},
            {value: '3', label: 'Sewa'},
            {value: 'N',  label: 'Group Customer'},
        ];

        this.expenditures = [
            {value: '1', label: '< Rp 900.000,-'},
            {value: '2', label: 'Rp 900.001,- sd < Rp 1.250.000,-'},
            {value: '3', label: 'Rp 1.250.001,- sd < Rp 1.750.000,-'},
            {value: '4', label: 'Rp 1.750.001,- sd < Rp 2.500.000,-'},
            {value: '5', label: 'Rp 2.500.001,- sd < Rp 4.000.000,-'},
            {value: '6', label: 'Rp 4.000.001,- sd < Rp 6.000.000,-'},
            {value: '7', label: '> Rp 6.000.001,- '},
        ];

        this.educations = [
            {value: '1', label: 'Tidak Tamat SD'},
            {value: '2', label: 'SD'},
            {value: '3', label: 'SLTP'},
            {value: '4', label: 'SLTA'},
            {value: '5',  label: 'AKADEMI'},
            {value: '6',  label: 'UNIVERSITAS'},
            {value: '7',  label: 'PASCA SARJANA'}
        ];

        this.hps = [
            {value: '1', label: 'Pra Bayar(Isi Ulang)'},
            {value: '2', label: 'Pasca Bayar'},
            {value: 'N', label: 'Tidak memiliki HP'},
        ];

        this.infos = [
            {value: 'Y', label: 'Ya'},
            {value: 'N', label: 'Tidak'},
        ];

        this.brandOwners = [
            {value: '1', label: 'Honda'},
            {value: '2', label: 'Yamaha'},
            {value: '3', label: 'Suzuki'},
            {value: '4', label: 'Kawasaki'},
            {value: '5', label: 'Motor lain'},
            {value: '6', label: 'Belum pernah memiliki'},
        ];

        this.vehicleTypes = [
            {value: '1', label: 'Sport'},
            {value: '2', label: 'CUB / Bebek'},
            {value: '3', label: 'Automatic'},
            {value: '4', label: 'Belum pernah memiliki'},
        ];

        this.vehicleUsers = [
            {value: '1', label: 'Saya Sendiri'},
            {value: '2', label: 'Anak'},
            {value: '3', label: 'Pasangan Suami Istri'},
            {value: '3', label: 'Lain - Lain'},
        ];

        this.buyFors = [
            {value: '1', label: 'BERDAGANG'},
            {value: '2', label: 'PEMAKAIAN JARAK DEKAT'},
            {value: '3', label: 'KE SEKOLAH/KE KAMPUS'},
            {value: '4', label: 'REKREASI/OLAH RAGA'},
            {value: '5', label: 'KEBUTUHAN KELUARGA'},
            {value: '6', label: 'LAIN-LAIN'},
            {value: '7', label: 'BEKERJA'},
        ];

        this.jobs = [
            {value: '1', label: 'PEGAWAI NEGERI'},
            {value: '2a', label: 'PEGAWAI SWASTA : PERTANIAN/PERKEBUNAN/KEHUTANAN/PERIKANAN/PETERNAKAN'},
            {value: '2b', label: 'PEGAWAI SWASTA : INDUSTRI'},
            {value: '2c', label: 'PEGAWAI SWASTA : KONSTRUKSI'},
            {value: '2d', label: 'PEGAWAI SWASTA : PERTAMBANGAN'},
            {value: '2e', label: 'PEGAWAI SWASTA : JASA'},
            {value: '2f', label: 'PEGAWAI SWASTA : PERDAGANGAN (RETAIL)'},
            {value: '3', label: 'OJEK'},
            {value: '4a', label: 'WIRASWASTA : PERTANIAN/PERKEBUNAN/KEHUTANAN/PERIKANAN/PETERNAKAN'},
            {value: '4b', label: 'WIRASWASTA : INDUSTRI'},
            {value: '4c', label: 'WIRASWASTA : KONSTRUKSI'},
            {value: '4d', label: 'WIRASWASTA : PERTAMBANGAN'},
            {value: '4e', label: 'WIRASWASTA : JASA'},
            {value: '4f', label: 'WIRASWASTA : PERDAGANGAN (RETAIL)'},
            {value: '5', label: 'MAHASISWA/PELAJAR'},
            {value: '6', label: 'GURU / DOSEN'},
            {value: '7', label: 'TNI / PLORI'},
            {value: '8', label: 'IBU RUMAH TANGGA'},
            {value: '11', label: 'LAIN-LAIN'},
            {value: '12', label: 'DOKTER'},
            {value: '13', label: 'PENGACARA'},
            {value: '14', label: 'WARTAWAN'},
            {value: '15', label: 'PETANI'},
            {value: '16', label: 'NELAYAN'},
        ];

        this.groupCustomer = [
            // {value: 'G', label: 'GROUP CUSTOMER'},
            {value: 'I', label: 'Individual Customer (Regular)'},
            {value: 'J', label: 'Individual Customer (Joint Promo)'},
            {value: 'C', label: 'Individual Customer (Kolektif)'},
        ];

        this.citizenship = [
            {value: '1', label: 'WNI'},
            {value: '2', label: 'WNA'},
        ];
        this.getReligionList();
    }

    getReligionList(): void {
        this.religionTypeService.query()
            .subscribe(
                (res: ResponseWrapper) => {this.religions = res.json},
                (res: ResponseWrapper) => this.onError(res.json),
            )
    }

    ngOnInit() {    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.idCustomer !== undefined) {
            console.log('cek id cust di detail cdb', this.idCustomer);
            console.log('cek datastnk di detail cdb', this.dataStnk);

            this.load(this.idCustomer);
            this.idAgama = this.dataStnk.personOwner.religionTypeId;
            this.religionByid(this.idAgama);
            this.idWork = this.dataStnk.personOwner.workTypeId;
            this.mail = this.dataStnk.personOwner.privateMail;
            this.getSaleType();
        }
    }

    ngOnDestroy() {
    }

    load(id) {
        this.subscription = this.communicationEventCDBService.queryByIdCustomer({
            idCustomer: this.idCustomer,
            page: 0,
            size: 1,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => {
                    this.onSuccess(res.json, res.headers)
                },
                (res: ResponseWrapper) => {
                    console.log('WAKWAU GAGAL', res);
                    this.onError(res.json)
                }
            );
    }

    private onSuccess(data, headers) {
        console.log('on sakses data', data);
        if (data.length > 0) {
            this.communicationEventCDB = data[0];
            console.log('cek data cdb data ada', this.communicationEventCDB)
            this.isDisabled = true;
        } else {
            this.communicationEventCDB = new CommunicationEventCDB();
            this.isDisabled = false;
        }

    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idCommunicationEvent') {
            result.push('idCommunicationEvent');
        }
        return result;
    }

    submitSurvey(): void {
            const alamat = this.dataStnk.personOwner.postalAddress.address1;
            const alamat2 = this.dataStnk.personOwner.postalAddress.address2;
            this.loadingService.loadingStart();
            const custId = this.idCustomer;
            this.communicationEventCDB.idReligion = this.idAgama;
            this.communicationEventCDB.idCustomer = custId ;
            this.communicationEventCDB.email = this.mail;
            if (alamat2 != null) {
                this.communicationEventCDB.address = alamat.concat('~').concat(alamat2);
            } else {
                this.communicationEventCDB.address = alamat;
            }
            this.communicationEventCDB.idProvince = this.dataStnk.personOwner.postalAddress.provinceId;
            this.communicationEventCDB.idCity = this.dataStnk.personOwner.postalAddress.cityId;
            this.communicationEventCDB.idDistrict = this.dataStnk.personOwner.postalAddress.districtId;
            this.communicationEventCDB.idVillage = this.dataStnk.personOwner.postalAddress.villageId;
            this.communicationEventCDB.jenisPembayaran = this.idJenisPembayran;
            this.communicationEventCDB.postalCode = this.postalCode;
            this.cekCheckBox(this.communicationEventCDB);
            if (this.communicationEventCDB.idCommunicationEvent  == null) {
                this.subscribeToSaveResponse(
                    this.communicationEventCDBService.create(this.communicationEventCDB));
                    this.loadingService.loadingStop();
            } else {
                this.subscribeToSaveResponse(
                    this.communicationEventCDBService.update(this.communicationEventCDB));
                    this.loadingService.loadingStop();
            }
    }

    ngDoCheck(): void {
            if ( this.communicationEventCDB.addressSurat == null ||
                 this.communicationEventCDB.addressSurat === '' ||
                 this.communicationEventCDB.idProvinceSurat == null ||
                 this.communicationEventCDB.idProvinceSurat === '' ||
                 this.communicationEventCDB.idCitySurat == null ||
                 this.communicationEventCDB.idCitySurat === '' ||
                 this.communicationEventCDB.idDistrictSurat == null ||
                 this.communicationEventCDB.idDistrictSurat === '' ||
                 this.communicationEventCDB.idVillageSurat == null ||
                 this.communicationEventCDB.idVillageSurat === '' ||
                 this.communicationEventCDB.infoAboutHonda == null ||
                 this.communicationEventCDB.infoAboutHonda === '' ||
                 this.communicationEventCDB.groupCustomer == null ||
                 this.communicationEventCDB.groupCustomer === '' ||
                 this.communicationEventCDB.postalCodeSurat == null ||
                 this.communicationEventCDB.postalCodeSurat === ''
                ) {
                this.isSaving = true;
            }else {
                this.isSaving = false;
            }

            if (this.communicationEventCDB.addressSurat == null ||
                this.communicationEventCDB.addressSurat === '' ||
                this.communicationEventCDB.idProvinceSurat == null ||
                this.communicationEventCDB.idProvinceSurat === '' ||
                this.communicationEventCDB.idCitySurat == null ||
                this.communicationEventCDB.idCitySurat === '' ||
                this.communicationEventCDB.idDistrictSurat == null ||
                this.communicationEventCDB.idDistrictSurat === '' ||
                this.communicationEventCDB.idVillageSurat == null ||
                this.communicationEventCDB.idVillageSurat === '' ||
                this.communicationEventCDB.infoAboutHonda == null ||
                this.communicationEventCDB.infoAboutHonda === '' ||
                this.communicationEventCDB.job == null ||
                this.communicationEventCDB.job === '' ||
                this.communicationEventCDB.expenditureOneMonth == null ||
                this.communicationEventCDB.expenditureOneMonth === '' ||
                this.communicationEventCDB.lastEducation == null ||
                this.communicationEventCDB.lastEducation === '' ||
                this.communicationEventCDB.vehicleBrandOwner == null ||
                this.communicationEventCDB.vehicleBrandOwner === '' ||
                this.communicationEventCDB.vehicleTypeOwner == null ||
                this.communicationEventCDB.vehicleTypeOwner === '' ||
                this.communicationEventCDB.buyFor == null ||
                this.communicationEventCDB.buyFor === '' ||
                this.communicationEventCDB.vehicleUser == null ||
                this.communicationEventCDB.vehicleUser === '' ||
                this.communicationEventCDB.homeStatus == null ||
                this.communicationEventCDB.homeStatus === '' ||
                this.communicationEventCDB.hobby == null ||
                this.communicationEventCDB.hobby === '' ||
                this.communicationEventCDB.groupCustomer == null ||
                this.communicationEventCDB.groupCustomer === '' ||
                this.communicationEventCDB.postalCodeSurat == null ||
                this.communicationEventCDB.postalCodeSurat === '' ||
                this.communicationEventCDB.citizenship == null ||
                this.communicationEventCDB.citizenship === '') {
                this.isSaving = true;
            } else {
                this.isSaving = false
            }
        }

    private cekCheckBox(data: CommunicationEventCDB): CommunicationEventCDB {
        if (this.communicationEventCDB.isMailAddress === true  ) {
            this.communicationEventCDB.isMailAddress = 1;
        }
        if (this.communicationEventCDB.isCityzenIdCardAddress === true) {
            this.communicationEventCDB.isCityzenIdCardAddress = 1;
        }

        if (this.communicationEventCDB.isMailAddress === false  ) {
            this.communicationEventCDB.isMailAddress = 0;
        }
        if (this.communicationEventCDB.isCityzenIdCardAddress === false) {
            this.communicationEventCDB.isCityzenIdCardAddress = 0;
        }
        return data;
    }
    private subscribeToSaveResponse(result: Observable<CommunicationEventCDB>) {
        result.subscribe((res: CommunicationEventCDB) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CommunicationEventCDB) {
        this.toaster.showToaster('info', 'Save', 'Detail CD saved !');
        this.isSaving = false;
        this.communicationEventCDB = result;
        // this.previousState();
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    getVillage(event): void {
        this.communicationEventCDB.idVillage = event;
    }

    getDistrict(event): void {
        this.communicationEventCDB.idDistrict = event;
    }

    getProvince(event): void {
        this.communicationEventCDB.idProvince = event;
    }

    getCity(event): void {
        this.communicationEventCDB.idCity = event;
    }

    getDistrictSurat(event): void {
        this.communicationEventCDB.idDistrictSurat = event;
    }

    getProvincesurat(event): void {
        this.communicationEventCDB.idProvinceSurat = event;
    }

    getCitysurat(event): void {
        this.communicationEventCDB.idCitySurat = event;
    }

    getVillageSurat(event): void {
        this.communicationEventCDB.idVillageSurat = event;
    }

    public onChangeCheckBoxKtp() {
            this.communicationEventCDB.idDistrictSurat = null;
            this.communicationEventCDB.idProvinceSurat = null;
            this.communicationEventCDB.idCitySurat = null;
            this.communicationEventCDB.addressSurat = null;
            this.communicationEventCDB.idVillageSurat = null;

            this.communicationEventCDB.idDistrictSurat = this.dataStnk.personOwner.postalAddress.districtId;
            this.communicationEventCDB.idProvinceSurat = this.dataStnk.personOwner.postalAddress.provinceId;
            this.communicationEventCDB.idCitySurat = this.dataStnk.personOwner.postalAddress.cityId;
            if (this.dataStnk.personOwner.postalAddress.address2 != null) {
                this.communicationEventCDB.addressSurat = this.dataStnk.personOwner.postalAddress.address1.concat('~').concat(this.dataStnk.personOwner.postalAddress.address2);
            } else {
                this.communicationEventCDB.addressSurat = this.dataStnk.personOwner.postalAddress.address1;
            }
            this.communicationEventCDB.idVillageSurat = this.dataStnk.personOwner.postalAddress.villageId;
            this.communicationEventCDB.postalCodeSurat = this.postalCode;
    }

    public onChangeCheckBoxSurat() {
        this.communicationEventCDB.idDistrictSurat = null;
        this.communicationEventCDB.idProvinceSurat = null;
        this.communicationEventCDB.idCitySurat = null;
        this.communicationEventCDB.addressSurat = null;
        this.communicationEventCDB.idVillageSurat = null;
        this.communicationEventCDB.postalCodeSurat = null;
    }

    religionByid(idReligion) {
        this.religionTypeService.find(idReligion)
        .subscribe(
            (res) => {
                this.religionDesc = res.description;
            }
        )
    }

    getWorkType(idWork) {
        this.workTypeService.find(idWork)
        .subscribe(
            (res) => {
                this.workDesc = res.description;
                this.communicationEventCDB.job = idWork;
            }
        )
    }

    getSaleType() {
        if (this.sur.saleTypeId === SaleTypeConstants.CASH ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_CBD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_COD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD_CBD ||
            this.sur.saleTypeId === SaleTypeConstants.CASH_OFF_THE_ROAD_COD) {
            this.jenisPembayran = [
                {value: '1', label: 'CASH'}
            ];
            this.selected = 'CASH';
            this.idJenisPembayran = 1;
        }
        if (this.sur.saleTypeId === SaleTypeConstants.CREDIT ||
            this.sur.saleTypeId === SaleTypeConstants.CREDIT_CBD ||
            this.sur.saleTypeId === SaleTypeConstants.CREDIT_COD) {
            this.jenisPembayran = [
                {value: '2', label: 'REGULER LEASING'}
            ];
            this.selected = 'REGULER LEASING';
            this.idJenisPembayran = 2;
        }
    }

    public selectGroup(event) {
        if (event === 'G') {
            this.isGroup = true;
        } else {
            this.isGroup = false;
        }
       ;
    }

    public selectJob(event) {
        if (event === '11') {
            this.isJob = true;
        } else {
            this.isJob = false;
        }
       ;
    }
}
