import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
// import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { VehicleDocumentRequirementService } from '../vehicle-document-requirement/vehicle-document-requirement.service';
// import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { LoadingService } from '../../layouts/loading/loading.service';
// import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import * as BaseConstant from '../../shared/constants/base.constants';
import { VehicleDocumentRequirement, TambahReference } from '../vehicle-document-requirement/vehicle-document-requirement.model';
import { AutoCompleteModule } from 'primeng/primeng';
import { PriceComponentService, PriceComponent } from '../price-component';

@Component({
    selector: 'jhi-registrasi-bast-stnk-input-penerimaan-stnk',
    templateUrl: './registrasi-bast-stnk-input-penerimaan-stnk.component.html'
})
export class RegistrationPenerimaanSTNKInputComponent implements OnInit,  DoCheck {
    currentAccount: any;
    // unitDeliverable: UnitDeliverable;
    // unitDeliverableParameters: UnitDeliverableParameters;
    // unitDeliverableUpdate: UnitDeliverable;
    // unitDeliverables: UnitDeliverable[];
    // dataUnitDeliverable: UnitDeliverable;
    error: any;
    success: any;
    nomorRangka: string[] = ['KF', 'KJ', 'kuu'];
    nomorrangka: string;
    filteredNoka: any[];
    isChange: boolean;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    TotalData: VehicleDocumentRequirement;
    predicate: any;
    previousPage: any;
    reverse: any;
    isSaving: Boolean;
    detail: any;
    detailModal: boolean;
    printModal: boolean;
    checkPlatNomor: Boolean;
    revisiModal: boolean;
    vehicleDocumentRequirementbegbalupdate: VehicleDocumentRequirement;
    vehicleDocumentRequirementbegbal: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalCheck: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalCheckBiaya: VehicleDocumentRequirement[];
    vehicleDocumentRequirementbegbalupdateBiaya: VehicleDocumentRequirement;
    unitDeliverable: UnitDeliverable[];
    priceB: PriceComponent;
    price: PriceComponent[];
    additemVdr: TambahReference[];

    // statusTypes: StatusType[];
    salesmans: Salesman[];

    // bastTo: string;
    // bastKembali: any;
    // bastSalesKembali: boolean;
    // selected: VehicleDocumentRequirement[];
    tempArray: VehicleDocumentRequirement[];
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    filtered: any;
    isChecking: Boolean;
    tanggal: Date;
    bbnkb: any;
    pkb: any;
    swdkllj: any;
    stnk: any;
    tnkb: any;
    parkir: any;
    biaya: any;
    birojasa: any;
    totalBiaya: any;
    receiptNominal: any;
    receiptqty: any;

    platNomor: boolean;
    notice: boolean;
    bpkb: boolean;
    checkNotice: Boolean;
    checkStnk: Boolean;
    checkBpkb: Boolean;
    totalInput: number;
    costHandling: number;
    otherCost: number;
    refNumber: any;
    refDate: any;
    idVendor: any;
    tanggalStnk: any;
    tanggalNotice: any;
    noka: any;
    nosin: any;

    constructor(
        // protected unitDeliverableService: UnitDeliverableService,
        // protected statusTypeService: StatusTypeService,
        protected salesmanService: SalesmanService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected activatedRoute: ActivatedRoute,
        private commonUtilService: CommonUtilService,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        public reportUtilService: ReportUtilService,
        private priceComponentService: PriceComponentService,
        protected loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.printModal = false;
        this.isChange = false;
        this.additemVdr = [];
        this.isChecking = false;
        this.isSaving = false;
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirementbegbal = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirementbegbalCheck = new Array<VehicleDocumentRequirement>();
        this.vehicleDocumentRequirementbegbalupdateBiaya = new VehicleDocumentRequirement();
        this.price = new Array<PriceComponent>();
        // this.bastTo = 'salesman';
        // this.bastKembali = 0;
        // this.unitDeliverable = new UnitDeliverable();
        // this.unitDeliverableUpdate = new UnitDeliverable();
        // this.unitDeliverables = new Array<UnitDeliverable>();
        this.isFiltered = false;
        this.bbnkb = 0;
        this.pkb = 0;
        this.swdkllj = 0;
        this.stnk = 0;
        this.tnkb = 0;
        this.parkir = 0;
        this.biaya = 0;
        this.birojasa = 0;
        this.totalBiaya = 0;
    }

    loadAll() {
        console.log('is filter load = ', this.isFiltered);
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        console.log('vdr', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }

        if (this.isFiltered === true) {
        }
        console.log('vdr', this.vehicleDocumentRequirementService);
        if (this.currentSearch) {
            return;
        }

        if (this.isFiltered === true) {
        }
    }

    private loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator: this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales', res.json);
                this.salesmans = res.json;
            }
        )
    }
    filter() {
        // this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/VEHICLE-DOCUMENT-REQUIREMENT/REGISTRATION/BAST/STNK/begbal'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'ASC' : 'DESC')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.filtered = '';
        this.isFiltered = false;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal']);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement/registration/bast/stnk/begbal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadKorsal();
        });
    }

    protected onSuccess(data, headers) {
        // this.loadingService.loadingStop();
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = data.TotalData;
        this.queryCount = this.totalItems;
        this.vehicleDocumentRequirementbegbal = data;
        console.log('muncul', this.vehicleDocumentRequirementbegbal);
    }
    protected onSuccessFilter(data, headers) {
        // this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
    }

    protected onError(error) {
        // this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
    }

    updateTanggalTerima(noka: any, deltipe: any) {
        this.vehicleDocumentRequirementService.queryUpdateTanggal({
            idframe: noka,
            iddeltype: deltipe
        }).subscribe(
            (res) => {
                console.log('muncul dong', res)
            })
    }
    updateTerimaBpkb() {
        this.vehicleDocumentRequirementService.queryPenerimaanStnk(
            this.additemVdr
        ).subscribe(
            (res) => {
                console.log('PLISS MUNCUL TERIMA BPKB', res)
            }
        )
    }
    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
        });
    }

    showDetail(idreq: any) {
        this.vehicleDocumentRequirementService.find(idreq).subscribe(
            (response) => {
                this.vehicleDocumentRequirementbegbalupdate = response;
                console.log('cekkk ---->', this.vehicleDocumentRequirementbegbal);
            });
        this.detailModal = true;
    }

    showPrint(id: any) {
        this.printModal = true;
    }
    print(noka: string, deltipe: any) {
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/stnk_internal_begbal/pdf', { idframe: noka });
        console.log('print', noka)
        this.updateTanggalTerima(noka, deltipe);
        console.log('deltipe', deltipe)
        this.loadAll();
    }
    simpan() {
        this.updateTerimaBpkb();
        return this.clear();
        // this.loadAll();
        // console.log('PLISSS MUNCULL DONGGGGG', dtReceipt);
        // console.log('PLISSS MUNCULL DONGGGGG', dtReceiptNotice);
        // console.log('PLISSS MUNCULL DONGGGGG', dtReceiptStnk);
    }
    autoComplete(event) {
        this.filteredNoka = [];
        for (let i = 0; i < this.nomorRangka.length; i++) {
            const nomorrangka = this.nomorRangka[i];
            if (nomorrangka.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                this.filteredNoka.push(nomorrangka);
            }
        }
    };
    ganti() {
        this.isChange = true;
        console.log('ganti true', this.isChange)
    }
    doPrint() {
        this.printModal = false;
    }

    listUpdateBAST(): Promise<any> {
        return new Promise(
            (resolve) => {
                resolve();
            }
        )
    }
    showRevisi(id: any) {
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }
    cekBiaya(noka): void {
        const cekBiaya = new VehicleDocumentRequirement();
        if (this.vehicleDocumentRequirementbegbalupdateBiaya.idframe !== '' && this.vehicleDocumentRequirementbegbalupdateBiaya.idframe !== undefined && this.vehicleDocumentRequirementbegbalupdateBiaya.idframe !== null) {
            cekBiaya.idframe = this.vehicleDocumentRequirementbegbalupdateBiaya.idframe;
        }
        this.vehicleDocumentRequirementService.queryCheckBiaya({
            page: 0,
            size: 20,
            query: 'idframe:' + noka,
        }).subscribe(
            (res) => {
                if (res) {
                    this.vehicleDocumentRequirementbegbalCheckBiaya  = res.json;
                    this.vehicleDocumentRequirementbegbalupdateBiaya = this.vehicleDocumentRequirementbegbalCheckBiaya[0].idframe;
                    this.vehicleDocumentRequirementbegbalupdateBiaya = this.vehicleDocumentRequirementbegbalCheckBiaya[0].biayaP;
                }
            },
            (err) => {
                this.commonUtilService.showError(err('EROR BOS'));
            }
        )
    }
    checkNoka(noka): void {
        const unitCheck = new VehicleDocumentRequirement();
        if (this.vehicleDocumentRequirementbegbalupdate.idframe !== '' &&
            this.vehicleDocumentRequirementbegbalupdate.idframe !== undefined &&
            this.vehicleDocumentRequirementbegbalupdate !== null) {
            unitCheck.idframe = this.vehicleDocumentRequirementbegbalupdate.idframe;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idMachine !== '' &&
            this.vehicleDocumentRequirementbegbalupdate.idmachine !== undefined &&
            this.vehicleDocumentRequirementbegbalupdate.idmachine !== null) {
            unitCheck.idmachine = this.vehicleDocumentRequirementbegbalupdate.idmachine;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.policenumber !== '' &&
            this.vehicleDocumentRequirementbegbalupdate.policenumber !== undefined &&
            this.vehicleDocumentRequirementbegbalupdate.policenumber !== null) {
            unitCheck.policenumber = this.vehicleDocumentRequirementbegbalupdate.policenumber;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.refnumber !== '') {
            unitCheck.refnumber = this.vehicleDocumentRequirementbegbalupdate.refnumber
        }
        if (this.vehicleDocumentRequirementbegbalupdate.refdt !== '') {
            unitCheck.refdt = this.vehicleDocumentRequirementbegbalupdate.refdt
        }
        if (this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat !== '') {
            unitCheck.dtreceiptPlat = this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat
        }
        if (this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk !== '') {
            unitCheck.dtreceiptStnk = this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk
        }
        if (this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice !== '') {
            unitCheck.dtreceiptNotice = this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice
        }
        if (this.vehicleDocumentRequirementbegbalupdate.noFaktur !== '') {
            unitCheck.noFaktur = this.vehicleDocumentRequirementbegbalupdate.noFaktur
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idvendor !== '' &&
            this.vehicleDocumentRequirementbegbalupdate.idvendor !== undefined &&
            this.vehicleDocumentRequirementbegbalupdate.idvendor !== null) {
            unitCheck.idvendor = this.vehicleDocumentRequirementbegbalupdate.idvendor;

            console.log('MASUK DULU CEK NOKA', noka);

            console.log('MASUK DULU CEK mesin', this.vehicleDocumentRequirementbegbalupdate.idmachine);

            console.log('MASUK DULU CEK NOKA', this.vehicleDocumentRequirementbegbalupdate.policenumber);

            console.log('MASUK DULU CEK dtreceiptPlat', this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat);

            console.log('MASUK DULU CEK dtStnk', this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk);

            console.log('MASUK DULU CEK dtNotice', this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice);
        }

        this.vehicleDocumentRequirementService.queryCheckStnkByNoka({
            page: 0,
            size: 20,
            query: 'idframe:' + noka,
            idInternal: this.principal.getIdInternal(),
        }).subscribe(
            (res) => {
                this.checkBiaya(noka);
                if (res) {
                    console.log('HASIL PENGECEKAN DARI BELAKANG', res.json);
                    this.vehicleDocumentRequirementbegbalCheck = res.json;
                    this.vehicleDocumentRequirementbegbalupdate.idframe = this.vehicleDocumentRequirementbegbalCheck[0].idframe;
                    this.vehicleDocumentRequirementbegbalupdate.idmachine = this.vehicleDocumentRequirementbegbalCheck[0].idmachine;
                    this.vehicleDocumentRequirementbegbalupdate.policenumber = this.vehicleDocumentRequirementbegbalCheck[0].policenumber;
                    this.vehicleDocumentRequirementbegbalupdate.refnumber =
                    this.vehicleDocumentRequirementbegbalCheck[0].refnumber;
                    this.vehicleDocumentRequirementbegbalupdate.refdt =
                    this.vehicleDocumentRequirementbegbalCheck[0].refdt;
                    this.vehicleDocumentRequirementbegbalupdate.idvendor =
                    this.vehicleDocumentRequirementbegbalCheck[0].idvendor;
                    this.vehicleDocumentRequirementbegbalupdate.gantiNotice = this.vehicleDocumentRequirementbegbalCheck[1].gantiNotice;
                    this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat = this.vehicleDocumentRequirementbegbalCheck[2].dtreceiptPlat;
                    this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk = this.vehicleDocumentRequirementbegbalCheck[0].dtreceiptStnk;
                    this.vehicleDocumentRequirementbegbalupdate.noFaktur  = this.vehicleDocumentRequirementbegbalCheck[0].noFaktur;
                    this.doCheckPlatNomor(res.json);
                    console.log('balikan dtreceipt notice', this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice);
                }
            },
            (err) => {
                this.commonUtilService.showError(err('EROR BOS'));
            }
        )
    }
    ngDoCheck(): void {
        if ( this.platNomor == null ) {
            if (
                this.vehicleDocumentRequirementbegbalupdate.policenumber === '' ||
                this.vehicleDocumentRequirementbegbalupdate.policenumber == null ||
                this.receiptqty  === 0
            ) {
                this.isSaving = false;
            }else {
                this.isSaving = true;
            }
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idframe != null ||
            this.vehicleDocumentRequirementbegbalupdate.idmachine != null ||
            this.vehicleDocumentRequirementbegbalupdate.idvendor != null ||
            this.vehicleDocumentRequirementbegbalupdate.refnumber ||
            this.vehicleDocumentRequirementbegbalupdate.refdt ||
            this.vehicleDocumentRequirementbegbalupdate.gantiNotice ||
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat ||
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk ||
            this.vehicleDocumentRequirementbegbalupdate.noFaktur ||
            this.vehicleDocumentRequirementbegbalupdate.policenumber) {
            this.isChecking = false;
        } else {
            this.isChecking = true;
        }
        if (this.vehicleDocumentRequirementbegbalupdate.idframe === '' ||
            this.vehicleDocumentRequirementbegbalupdate.idframe == null ||
            this.vehicleDocumentRequirementbegbalupdate.idmachine === '' ||
            this.vehicleDocumentRequirementbegbalupdate.idmachine == null ||
            this.vehicleDocumentRequirementbegbalupdate.idvendor === '' ||
            this.vehicleDocumentRequirementbegbalupdate.idvendor == null ||
            this.vehicleDocumentRequirementbegbalupdate.refnumber === '' ||
            this.vehicleDocumentRequirementbegbalupdate.refnumber == null ||
            this.vehicleDocumentRequirementbegbalupdate.refdt === '' ||
            this.vehicleDocumentRequirementbegbalupdate.refdt == null ||
            this.vehicleDocumentRequirementbegbalupdate.gantiNotice == null ||
            this.vehicleDocumentRequirementbegbalupdate.gantiNotice === '' ||
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat == null ||
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat === '' ||
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk == null ||
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk === '' ||
            this.vehicleDocumentRequirementbegbalupdate.noFaktur == null ||
            this.vehicleDocumentRequirementbegbalupdate.noFaktur === '' ||
            this.vehicleDocumentRequirementbegbalupdate.policenumber === '' ||
            this.vehicleDocumentRequirementbegbalupdate.policenumber == null) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }
    checkBiaya(noka: any) {
        console.log('dapatt biaya', noka)
        this.priceComponentService.ambilBiayaPengurusan({
            query: 'idframe:' + noka,
        }).subscribe(
            (res) => {
                console.log('balikan harga dong', res);
                this.price = res.json;
                console.log('balikan price', this.price);
                this.price.forEach(
                    (p) => {
                        if (p.idpricetype === 210 ) {
                            this.bbnkb = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 211) {
                            this.pkb = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 212) {
                            this.swdkllj = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 213) {
                            this.stnk = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 214) {
                            this.tnkb = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 215) {
                            this.parkir = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 216) {
                            this.biaya = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        if (p.idpricetype === 217) {
                            this.birojasa = p.price;
                            console.log('coba dapat nggk ', p)
                        }
                        this.totalBiaya = this.bbnkb + this.pkb + this.swdkllj + this.stnk + this.tnkb + this.parkir + this.biaya + this.birojasa
                    }
                )
                if (this.price.length === 0 ) {
                    this.totalBiaya = 0;
                }
                console.log('total', this.totalBiaya)
            }
        )
    }
    doCheckPlatNomor(itm: VehicleDocumentRequirement) {
        if (itm.policenumber != null) {
            this.checkPlatNomor = true;
        } else {
            this.checkPlatNomor = false;
        }
    }

    onChangePlatNomor(checked) {
        if (checked) {
            this.tanggal = new Date();
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat = new Date().toLocaleDateString();
        } else {
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptPlat = undefined;
        }
    }
    // onChangeNotice(checked) {
    //     if (checked) {
    //         this.tanggal = new Date();
    //         this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice = new Date().toLocaleDateString();
    //     } else {
    //         this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice = undefined;
    //     }
    // }
    gantiNotice(checked) {
        if (checked) {
            this.tanggal = new Date();
            this.vehicleDocumentRequirementbegbalupdate.gantiNotice = new Date().toLocaleDateString();
        } else {
            this.vehicleDocumentRequirementbegbalupdate.gantiNotice = undefined;
        }
    }
    onChangeStnk(checked) {
        if (checked) {
            this.tanggal = new Date();
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk = new Date().toLocaleDateString();
        } else {
            this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk = undefined;
        }
    }
    addItem() {
        console.log('dari dpn: ', this.vehicleDocumentRequirementbegbalupdate);

        this.vehicleDocumentRequirementbegbalupdate.receiptqty = this.receiptqty;
        this.vehicleDocumentRequirementbegbalupdate.receiptnominal = this.receiptNominal;
        this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice = this.tanggalNotice;
        this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice =
        this.tanggalStnk;

        console.log('muncul receiptnya ', this.receiptqty);

        console.log('muncul receiptnya ', this.receiptNominal);

        console.log('tnggl notice ', this.tanggalNotice);
        // this.vehicleDocumentRequirementbegbalupdate.idmachine = nosin;
        this.vehicleDocumentRequirementbegbalupdate.biayaPengurusan = this.totalBiaya;

        this.additemVdr = [...this.additemVdr, this.vehicleDocumentRequirementbegbalupdate];
        this.vehicleDocumentRequirementbegbalupdate = new VehicleDocumentRequirement();
        this.totalBiaya = new VehicleDocumentRequirement();
        this.platNomor = false;
        this.notice = false;
        this.stnk = false;
        this.bpkb = false;
        this.checkPlatNomor = false;
        this.checkNotice = false;
        this.checkStnk = false;
        this.checkBpkb = false;
        console.log('iki plat nomer', this.vehicleDocumentRequirementbegbalupdate.dtreceipt);
        console.log('iki notice', this.vehicleDocumentRequirementbegbalupdate.dtreceiptNotice);
        console.log('iki stnk', this.vehicleDocumentRequirementbegbalupdate.dtreceiptStnk);
        console.log('cost noka', this.vehicleDocumentRequirementbegbalupdate.idframe);
        console.log('cost nosin', this.vehicleDocumentRequirementbegbalupdate.idmachine);
        console.log('dapat add item vdr', this.additemVdr);
    }
    removeItem(data: any) {
        console.log('deleteee', data)
        this.additemVdr.forEach((item, index) => {
            console.log('add item addvitem ', this.additemVdr)
            console.log('add item delete ', item)
            if (item.idframe === data.idframe
                && item.idmachine === data.idmachine) {
                console.log('idframe delete', data.idframe)
                this.additemVdr.splice(index, 1);

                this.tempArray = [];
                this.tempArray = this.additemVdr;

                this.additemVdr = [];
                this.additemVdr = this.additemVdr.concat(this.tempArray);
            }
        });
    }

}
