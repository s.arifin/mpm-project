import { LoadingService } from './../../layouts/loading/loading.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { StatusType, StatusTypeService } from '../status-type';
import { Salesman, SalesmanService } from '../salesman';
import { Vendor, VendorService } from '../vendor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { loadavg } from 'os';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { DatePipe } from '@angular/common';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';

@Component({
    selector: 'jhi-registration-pengurusan-biro-jasa',
    templateUrl: './registration-pengurusan-biro-jasa.component.html'
})
export class RegistrationPengurusanBiroJasaComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    account: Account;

    biroJasaModal: boolean;
    noteModal: boolean;
    reprintBiro: boolean;
    internal: Internal;
    selectedSub: any;

    statusTypes: StatusType[];
    salesmans: Salesman[];
    idVendor: any;
    vendor: Vendor;
    vendors: Vendor[];
    public vendorForSelect: Array<Object>;
    public fetchMotor: boolean;
    public selectedVendor: any;
    selected: VehicleDocumentRequirement[];
    public listbastForSelect: Array<Object>;
    public listbast: Array<Object>;

    // nuse
    axPosting: AxPosting;
    axPostingLines: AxPostingLine[];
    axPostingLine: AxPostingLine;

    constructor(
        private vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        private statusTypeService: StatusTypeService,
        private salesmanService: SalesmanService,
        private vendorService: VendorService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private reportUtilService: ReportUtilService,
        private loadingService: LoadingService,
        // nuse
        protected axPostingService: AxPostingService,
        protected datePipe: DatePipe,
        protected internalService: InternalService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.biroJasaModal = false;
        this.noteModal = false;
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirements = new Array<VehicleDocumentRequirement>();
        this.fetchMotor = true;
        this.listbastForSelect = new Array<Object>();
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                idStatusType: 34,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleDocumentRequirementService.query({
            idStatusType: 34,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-document-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-document-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.loadList();
            console.log('accountku :', this.currentAccount.firstName);
        });
        this.registerChangeInVehicleDocumentRequirements();

        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.vendorService.query({
        //     idInternal: this.principal.getIdInternal(),
        //     sort: ['idVendor', 'asc'],
        //     size : 2000})
        //     .subscribe((res: ResponseWrapper) => {
        //         this.loadVendor();
        //         },
        //         (res: ResponseWrapper) => this.onError(res.json));
        if (this.vehicleDocumentRequirement && this.vehicleDocumentRequirement.idVendor) {
            this.vendorService.find(this.vehicleDocumentRequirement.idVendor).subscribe((res) => {
                this.vendor = res;
            });
        }

        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity(true).then((account) => {
                this.account = account;
                console.log('cek account', this.account);
            });
        });
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInVehicleDocumentRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleDocumentRequirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleDocumentRequirements = data;
    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    updateNote() {
        console.log('cekk dto update note', this.vehicleDocumentRequirement);
        this.vehicleDocumentRequirementService.updateNote(this.vehicleDocumentRequirement)
        .subscribe((res: VehicleDocumentRequirement) =>
            this.onDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
    }

    private onDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.noteModal = false;
    }

    private onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'VehicleDocumentRequirement Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleDocumentRequirementListModification',
                    content: 'Deleted an vehicleDocumentRequirement'
                    });
                });
            }
        });
    }

    showNote(idRequirement: any) {
        this.noteModal = true;
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirement.idRequirement = idRequirement;
    }
    // showBiroJasaModal(param) {
    //     this.biroJasaModal = true;
    //     this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
    //     this.vehicleDocumentRequirement = param;
    // }

    showBiroJasaModal(param) {
        this.biroJasaModal = true;
        this.loadVendor();
        // this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        // this.vehicleDocumentRequirement = param;
    }

    doPengurusanBiroJasa() {
        this.selectedVendor = null;
        this.vehicleDocumentRequirement.idVendor = this.selectedVendor;
        console.log('vdr', this.vehicleDocumentRequirement);
        this.vehicleDocumentRequirementService.executeProcess(103, null, this.vehicleDocumentRequirement).subscribe(
            (value) => {
                console.log('download-this: ', value);
            },
            (err) => console.log(err),
            () => {
                setTimeout(() => {
                    this.vehicleDocumentRequirements = new Array<VehicleDocumentRequirement>();
                    this.loadAll();
                    this.biroJasaModal = false;
                    this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..');
                }, 1000);
            }

        );
    }
    private convertVendorForSelect(data: Array<Vendor>) {
        this.vendorForSelect = this.vendorService.convertVendorForSelectPrimeNg(data);
        console.log('ambil vendor', this.vendorForSelect)
    }

    private loadVendor(): Promise<Vendor[]> {
        return new Promise<Vendor[]>(
            (resolve) => {
                this.vendorService.queryFilterByBiroJasa({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['idVendor', 'asc'],
                    size : 3000}).subscribe(
                    (res: ResponseWrapper) => {
                        this.fetchMotor = false;
                        // this.convertVendorForSelect(res.json);
                        this.vendors = res.json;
                        console.log('vendor nih', this.vendors)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }

    private loadList(): Promise<VehicleDocumentRequirement[]> {
        return new Promise<VehicleDocumentRequirement[]>(
            (resolve) => {
                this.vehicleDocumentRequirementService.queryFilterBy({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['submissionNo', 'asc'],
                    size : 9000,
                    queryFrom: 'listbast'
                }).subscribe(
                    (res: ResponseWrapper) => {
                        this.listbast = res.json;
                        this.convertListForSelect(res.json);
                        console.log('bast nih nih', this.listbast)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }

    protected convertListForSelect(data: Array<VehicleDocumentRequirement>) {
        this.listbastForSelect = this.vehicleDocumentRequirementService.convertListForSelectPrimeNg(data);
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idSalesman;
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    // print(idFrame: Array<VehicleDocumentRequirement>) {
    //     idFrame.forEach(
    //         (res) => {
    //             this.toasterService.showToaster('info', 'Print Data', 'proccessing ... ');
    //             this.reportUtilService.viewFile('/api/report/bast_faktur_ke_biro/pdf', {noka: res.vehicle.idFrame, userlogin: this.currentAccount.firstName});
    //         }
    //     )
    // }

    print(bastNumber) {
        this.toasterService.showToaster('info', 'Print Data', 'proccessing ... ');
        this.reportUtilService.viewFile('/api/report/bast_faktur_ke_biro/pdf', {bastnumber: bastNumber, userlogin: this.currentAccount.firstName});
    }

    mappingIdVendor(idVendor): Promise<any> {
        return new Promise(
            (resolve) => {
            console.log('ceek vendor di mapping -->', idVendor );
            console.log('cek selected kirim biro jasa', this.selected);
            const vend = this.selected;
            vend.forEach (
                (res) => {
                    res.idVendor = idVendor;
                }
            )
                resolve();
            }
        )
    }

    listKirimBiroJasa(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk list kirim biro jasa', this.selected);
                this.vehicleDocumentRequirementService.executeListProcess(109, this.principal.getIdInternal(), this.selected)
                .subscribe(
                    (value) => {
                        this.findOne();
                        console.log('thissssss: ', value);
                        this.selected = new Array<VehicleDocumentRequirement>();
                        this.vehicleDocumentRequirementService.createGL(value[0].idRequirement).subscribe(
                            (res) => console.log('Journal Terbentuk'),
                            (res) => console.log('Journal Gagal Terbentuk')
                        );
                        // value.forEach(
                        //     (item) => {
                        //         console.log('item : ', item);
                        //         this.AXJournal(item);
                        //     }
                        // );
                        this.loadAll();
                    },
                    (err) => console.log(err),
                    () => {
                        this.vehicleDocumentRequirements = new Array<VehicleDocumentRequirement>();
                        this.biroJasaModal = false;
                        this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..');
                    }
                )
            resolve();
            }
        )
    }

    findOne(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk fungsi find');
                const idFrame = this.selected[0].vehicle.idFrame;
                this.vehicleDocumentRequirementService.findbyFrame(idFrame).subscribe(
                    (data) => {
                        console.log('hasil', data);
                        this.print(data.submissionNo);
                    }
                )
            resolve()
            }
        )
    }

    kirimBiroJasa(idVendor) {
        console.log('ceek vendor --->', idVendor);
        this.mappingIdVendor(idVendor)
        .then(
            () => this.listKirimBiroJasa()
            // .then(
                // () => this.print(this.selected)
                // () => this.findOne()
            // )
        )
    }

    searchVendor(event) {
        this.vendorService.search({ query: event.query + '*' }).subscribe((res) => {
            this.vendors = res.json;
        });
    }

    selectVendor(event) {
        console.log('Event', event);
        this.vehicleDocumentRequirement.idVendor = this.vendor.idVendor;
    }

    // protected AXJournal(inputVdr: VehicleDocumentRequirement) {
    //     const myaxPostingLine = [];
    //     const today = new Date();
    //     this.axPosting = new AxPosting();

    //     this.axPosting.AutoPosting = 'FALSE';
    //     this.axPosting.AutoSettlement = 'FALSE';
    //     this.axPosting.DataArea = this.principal.getIdInternal().substr(0, 3);
    //     this.axPosting.Guid = inputVdr.id;
    //     this.axPosting.JournalName = 'AP-InvBiroJasa';
    //     this.axPosting.Name = 'Pengurusan Biro Jasa ' + inputVdr.ivuNumber;
    //     this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
    //     this.axPosting.TransactionType = 'Unit';

    //     // Debit
    //     this.axPostingLine = new AxPostingLine();
    //     this.axPostingLine.AccountNum = 'T0777';
    //     this.axPostingLine.AccountType = 'Vend';
    //     this.axPostingLine.AmountCurCredit = '0';
    //     this.axPostingLine.AmountCurDebit = inputVdr.bbn.toString();
    //     this.axPostingLine.Company = this.principal.getIdInternal().substr(0, 3);
    //     this.axPostingLine.DMSNum = inputVdr.registrationNumber;
    //     this.axPostingLine.Description = 'Pengurusan Biro Jasa ' + inputVdr.ivuNumber;
    //     this.axPostingLine.Dimension1 = '1014';
    //     this.axPostingLine.Dimension2 = this.principal.getIdInternal().substr(0, 3);
    //     this.axPostingLine.Dimension3 = '000';
    //     this.axPostingLine.Dimension4 = '000';
    //     this.axPostingLine.Dimension5 = '00';
    //     this.axPostingLine.Dimension6 = '000';
    //     this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
    //     this.axPostingLine.Invoice = inputVdr.ivuNumber;
    //     this.axPostingLine.Payment = '';
    //     myaxPostingLine.push(this.axPostingLine);

    //     // credit
    //     this.axPostingLine = new AxPostingLine();
    //     this.axPostingLine.AccountNum = inputVdr.idVendor.substr(0, inputVdr.idVendor.length - 4);
    //     this.axPostingLine.AccountType = 'Vend';
    //     this.axPostingLine.AmountCurCredit = inputVdr.bbn.toString();
    //     this.axPostingLine.AmountCurDebit = '0';
    //     this.axPostingLine.Company = this.principal.getIdInternal().substr(0, 3);
    //     this.axPostingLine.DMSNum = inputVdr.registrationNumber;
    //     this.axPostingLine.Description = 'Pengurusan Biro Jasa ' + inputVdr.ivuNumber;
    //     this.axPostingLine.Dimension1 = '1014';
    //     this.axPostingLine.Dimension2 = this.principal.getIdInternal().substr(0, 3);
    //     this.axPostingLine.Dimension3 = '000';
    //     this.axPostingLine.Dimension4 = '000';
    //     this.axPostingLine.Dimension5 = '00';
    //     this.axPostingLine.Dimension6 = '000';
    //     this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
    //     this.axPostingLine.Invoice = inputVdr.ivuNumber;
    //     this.axPostingLine.Payment = '';

    //     myaxPostingLine.push(this.axPostingLine);

    //     this.axPosting.LedgerJournalLine = myaxPostingLine;

    //     this.axPostingService.send(this.axPosting).subscribe(
    //         (resaxPosting: ResponseWrapper) => console.log('Success : ', resaxPosting.json.Message),
    //         (resaxPosting: ResponseWrapper) => {
    //             this.onError(resaxPosting.json);
    //             console.log('error ax posting : ', resaxPosting.json);
    //         }
    //     );
    // }

    trackinternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    reprintBiro1() {
        this.reprintBiro = true;
    }

    printulang() {
           const nd = this.principal.getUserLogin();
        // const sampai = new Date (thru);
            this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
            console.log('cosole2:', this.selectedSub);
            this.reportUtilService.viewFile('/api/report/bast_faktur_ke_biro/pdf', {bastnumber: this.selectedSub, userlogin: nd});
            // this.reportUtilService.downloadFile('/api/report/Refund1/xlsx', { dtcreate1: dari, dtcreate2: sampai, internalid: nd.idInternal });

    }
}
