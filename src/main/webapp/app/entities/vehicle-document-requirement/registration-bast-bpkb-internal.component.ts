import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
import { StatusType, StatusTypeService } from '../status-type';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as UnitDeliverableConstant from '../../shared/constants/unit-deliverable.constants';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { LoadingService } from '../../layouts/loading/loading.service';
import * as BaseConstant from '../../shared/constants/base.constants';
import { Salesman, SalesmanService } from '../salesman';
import { UnitDeliverableParameters } from '../unit-deliverable/unit-deliverable-parameter.model';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';

@Component({
    selector: 'jhi-registration-bast-bpkb-internal',
    templateUrl: './registration-bast-bpkb-internal.component.html'
})
export class RegistrationBASTBPKBInternalComponent implements OnInit, OnDestroy {

    currentAccount: any;
    unitDeliverable: UnitDeliverable;
    unitDeliverables: UnitDeliverable[];
    unitDeliverableUpdate: UnitDeliverable;
    dataUnitDeliverable: UnitDeliverable;
    unitDeliverableParameters: UnitDeliverableParameters;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    detail: any;
    detailModal: boolean;
    printLeasingModal: boolean;
    printCustomerModal: boolean;
    revisiModal: boolean;
    selected: UnitDeliverable[];
    isFiltered: boolean;
    selectedSalesman: string;
    korsals: Salesman[];
    selectedKorsal: string;
    selectedLeasing: string;
    salesmans: Salesman[];
    filtered: any;
    leasingCompanies: LeasingCompany[];

    statusTypes: StatusType[];

    constructor(
        protected unitDeliverableService: UnitDeliverableService,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected statusTypeService: StatusTypeService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected loadingService: LoadingService,
        protected salesmanService: SalesmanService,
        protected leasingCompanyService: LeasingCompanyService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.detailModal = false;
        this.unitDeliverable = new UnitDeliverable();
        this.unitDeliverables = new Array<UnitDeliverable>();
        this.isFiltered = false;
    }

    loadAll() {
        console.log('is filter load = ', this.isFiltered);
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.unitDeliverableService.searchBPKB({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }

        if (this.isFiltered === true) {
            this.loadingService.loadingStart();
            this.unitDeliverableService.queryFilterBy({
                unitDeliverablePTO: this.unitDeliverableParameters,
                idInternal: this.principal.getIdInternal(),
                idDeliverableType: UnitDeliverableConstant.BAST_BPKB,
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        this.unitDeliverableService.query({
            idDeliverableType: UnitDeliverableConstant.BAST_BPKB,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

        this.leasingCompanyService.query()
        .subscribe((res: ResponseWrapper) => {this.leasingCompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/registration/bast/bpkb/internal'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        // this.page = 0;
        this.filtered = '';
        this.isFiltered = false;
        // this.currentSearch = '';
        this.router.navigate(['/registration/bast/bpkb/internal', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/registration/bast/bpkb/internal', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInUnitDeliverables();

        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.loadKorsal();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitDeliverable) {
        return item.idDeliverable;
    }

    public trackLeasingCompanyById(index: number, item: LeasingCompany): String {
        return item.idLeasingCompany;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    registerChangeInUnitDeliverables() {
        this.eventSubscriber = this.eventManager.subscribe('unitDeliverableListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idDeliverable') {
            result.push('idDeliverable');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDeliverables = data;
        if (data.length === 0) {
            this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        }
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDeliverables = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.unitDeliverableService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.unitDeliverableService.update(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.unitDeliverableService.create(event.data)
                .subscribe((res: UnitDeliverable) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: UnitDeliverable) {
        this.toasterService.showToaster('info', 'Unit Deliverable Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitDeliverableService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'unitDeliverableListModification',
                    content: 'Deleted an unitDeliverable'
                    });
                });
            }
        });
    }

    showPrintLeasingMulti() {
        this.printLeasingModal = true;
    }

    doPrintLeasing() {
        this.printLeasingModal = false;
        this.mappingDataPengambilLeasing().then(
            () => {
                this.listBASTLeasing()
            }
        )
    }

    showPrintCustomerMulti() {
        this.printCustomerModal = true;
    }

    doPrintCustomer() {
        this.printCustomerModal = false;
        this.mappingDataPengambilCustomer().then(
            () => {
                this.listBASTCustomer()
            }
        )
    }

    showRevisiMulti() {
        this.revisiModal = true;
    }

    doRevisi() {
        this.revisiModal = false;
    }

    // printMulti(data: Array<UnitDeliverable>): Promise<any> {
    //     return new Promise(
    //         (resolve) => {
    //             console.log('masuk untuk printtt');
    //             data.forEach(
    //                 (res) => {
    //                     this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
    //                     this.reportUtilService.viewFile('/api/report/bast_bpkb1/pdf', {noka: res.vehicleDocumentRequirement.vehicle.idFrame, nama: this.unitDeliverable.name, nik: this.unitDeliverable.identityNumber});
    //                 }
    //             )
    //             resolve();
    //         }
    //     )
    // }

    mappingDataPengambilLeasing(): Promise<any> {
        return new Promise(
            (resolve) => {
                const data = this.selected;
                data.forEach (
                        (res) => {
                            res.identityNumber = this.unitDeliverable.identityNumber;
                            res.name = this.unitDeliverable.name;
                            // res.cellPhone = this.unitDeliverableUpdate.cellPhone;
                        }
                )
                resolve();
            }
        )
    }

    mappingDataPengambilCustomer(): Promise<any> {
        return new Promise(
            (resolve) => {
                const data = this.selected;
                data.forEach (
                        (res) => {
                            res.identityNumber = this.unitDeliverable.identityNumber;
                            res.name = this.unitDeliverable.name;
                        }
                )
                resolve();
            }
        )
    }

    listBASTLeasing(): Promise<any> {
        return new Promise(
            (resolve) => {
                // this.printLeasingModal = false;
                console.log('masuk list update bast', this.selected);
                this.unitDeliverableService.executeListProcess(105, this.principal.getIdInternal(), this.selected)
                .subscribe(
                    (value) => {
                        this.findOne();
                        console.log('thissssss: ', value);
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.unitDeliverables = new Array<UnitDeliverable>();
                        this.unitDeliverable = new UnitDeliverable();
                    }
                )
            resolve();
            }
        )
    }

    listBASTCustomer(): Promise<any> {
        return new Promise(
            (resolve) => {
                // this.printCustomerModal = false;
                console.log('masuk list update bast', this.selected);
                this.unitDeliverableService.executeListProcess(104, this.principal.getIdInternal(), this.selected)
                .subscribe(
                    (value) => {
                        this.findOne();
                        console.log('thissssss: ', value);
                        // this.selected = new Array<UnitDeliverable>();
                        this.toasterService.showToaster('Info', 'Data Process', 'Done process in system..');
                        this.loadAll();
                    },
                    (err) => {
                        console.log(err);
                        this.toasterService.showToaster('Info', 'Data Process', 'Error Process');
                    },
                    () => {
                        this.unitDeliverables = new Array<UnitDeliverable>();
                        this.unitDeliverable = new UnitDeliverable();
                    }
                )
            resolve();
            }
        )
    }

    findOne(): Promise<any> {
        return new Promise(
            (resolve) => {
                console.log('masuk fungsi find');
                const idDelType = this.selected[0].idDeliverableType;
                const idreq = this.selected[0].vehicleDocumentRequirement.idRequirement;
                this.unitDeliverableService.queryByidReqandIdDeltype({
                    idDeliverableType : idDelType,
                    idReq : idreq
                }).subscribe(
                    (data) => {
                        console.log('hasil unit deliverable', data.json);
                        this.dataUnitDeliverable = data.json;
                        if (this.selected.length <= 2) {
                            console.log('data terpilih kurang dari sama dengan 2');
                            this.printMulti(this.dataUnitDeliverable);
                        } else {
                            console.log('data terpilih lebih dari 2');
                            this.printMultiple(this.dataUnitDeliverable);
                        }
                        this.selected = new Array<UnitDeliverable>();
                    }
                )
            resolve()
            }
        )
    }

    printMulti(data: UnitDeliverable) {
        console.log('masuk untuk print kurang dari atau sama dengan 2');
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_bpkb1/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: data.identityNumber});
    }

    printMultiple(data: UnitDeliverable) {
        console.log('masuk untuk printtt jika data lebih dari 2');
        this.toasterService.showToaster('Info', 'Print Data', 'Printing ...');
        this.reportUtilService.viewFile('/api/report/bast_bpkb_2/pdf', {bastnumber: data.bastNumber, nama: data.name, nik: data.identityNumber});
    }

    private  loadKorsal(): void {
        const obj: object = {
            idInternal: this.principal.getIdInternal(),
            idPositionType: BaseConstant.POSITION_TYPE.KOORDINATOR_SALES
        };
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('tarik data korsal', res.json);
                this.korsals = res.json;
            }
        )
    }

    public selectByLeasing() {
        console.log('ini id leasing :', this.selectedLeasing);
        this.unitDeliverableService.queryFilter({
            idDeliverableType: UnitDeliverableConstant.BAST_BPKB,
            idInternal: this.principal.getIdInternal(),
            idLeasing: this.selectedLeasing,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    public selectSalesByKorsal(): void {
        const obj: object = {
            idCoordinator : this.selectedKorsal
        }
        this.salesmanService.queryFilterBy(obj).subscribe(
            (res: ResponseWrapper) => {
                console.log('selected Sales' , res.json);
                this.salesmans = res.json;
            }
        )
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        // this.page = 0;
        this.unitDeliverableParameters = new UnitDeliverableParameters();
        this.unitDeliverableParameters.internalId = this.principal.getIdInternal();
        this.unitDeliverableParameters.idDeliverableType = UnitDeliverableConstant.BAST_BPKB;

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.unitDeliverableParameters.salesmanId = this.selectedSalesman;
        }

        if (this.filtered !== null && this.filtered !== undefined) {
            this.unitDeliverableParameters.dataParam = this.filtered;
        }

        this.unitDeliverableService.queryFilterBy({
            unitDeliverablePTO: this.unitDeliverableParameters,
            idInternal: this.principal.getIdInternal(),
            idDeliverableType: UnitDeliverableConstant.BAST_BPKB,
            sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data BPKB == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.unitDeliverableParameters.orderDateFrom);
        console.log('tanggal 2', this.unitDeliverableParameters.orderDateThru);
        // console.log('salesman 2', this.unitDeliverableParameters.salesmanId);
    }
}
