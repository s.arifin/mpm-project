import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { UnitDeliverableService, UnitDeliverable } from '../unit-deliverable';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { StatusType, StatusTypeService } from '../status-type';
import { Motor, MotorService } from '../motor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { ReportUtilService } from '../../shared/report/report-util.service';
import { CustomVehicleDocumentRequirement } from '.';
import { SummaryRefferal } from './summary-refferal.model';
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { VehicleDocumentRequirementCService, VDRAXDetail } from '../vehicle-document-requirement-c';
import { DatePipe } from '@angular/common';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-registration-penerimaan-bpkb-stnk',
    templateUrl: './registration-penerimaan-bpkb-stnk.component.html'
})
export class RegistrationPenerimaanBPKBSTNKComponent implements OnInit, OnDestroy {

    currentAccount: any;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    customVehicleDocumentRequirement: CustomVehicleDocumentRequirement[];
    error: any;
    unitDeliverable: UnitDeliverable[];
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    printBastKeHoModal: boolean;
    printUlangBastKeHoModal: boolean;
    selectedRefNumber: any;
    refference: SummaryRefferal;
    axPosting: AxPosting;
    axPostingLine: AxPostingLine;
    vdrAXDetailList: VDRAXDetail[];
    vdrAXDetail: VDRAXDetail;
    statusTypes: StatusType[];
    listReferences: SummaryRefferal[];
    listReferences1: SummaryRefferal[];
    motors: Motor[];

    constructor(
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected unitDeliverableService: UnitDeliverableService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected reportUtilService: ReportUtilService,
        protected axPostingService: AxPostingService,
        protected vehicleDocumentRequirementCService: VehicleDocumentRequirementCService,
        protected datePipe: DatePipe,
        protected loadingService: LoadingService,
    ) {
        this.printBastKeHoModal = false;
        this.refference = new SummaryRefferal();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                idStatusType: 36,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        // this.vehicleDocumentRequirementService.query({
        //     idStatusType: 36,
        //     idInternal: this.principal.getIdInternal(),
        //     page: this.page - 1,
        //     size: this.itemsPerPage,
        //     sort: this.sort()}).subscribe(
        //     (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        //     (res: ResponseWrapper) => this.onError(res.json)
        // );

        this.vehicleDocumentRequirementService.queryCustom({
            idStatusType: 36,
            statusKonfirmasi: false,
            idInternal: this.principal.getIdInternal(),
            statusPenerimaan: true,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );

        this.unitDeliverableService.listReferencesLov({
            page: this.page - 1,
            size: 500,
        }).subscribe(
            (res: ResponseWrapper) => {
                this.listReferences = res.json;
                console.log('cek list references',  this.listReferences)
            },
            (res: ResponseWrapper) => this.onError(res.json));

        this.unitDeliverableService.listReferencesLov1({
                page: this.page - 1,
                size: 500,
            }).subscribe(
                (res: ResponseWrapper) => {
                    this.listReferences1 = res.json;
                    console.log('cek list references',  this.listReferences1)
                },
                (res: ResponseWrapper) => this.onError(res.json));
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-document-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-document-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    printBast() {
        this.printBastKeHoModal = true;
    }

    printUlangBastToHO() {
        this.printUlangBastKeHoModal = false;
        const nd = this.principal.getUserLogin();
        // this.unitDeliverableService.setPrint(
        //     {refNumber: this.selectedRefNumber}
        // ).subscribe ((res) => {
        // });
        console.log('ref number', this.selectedRefNumber);
        this.toasterService.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/tagihan_ke_ho/pdf', {refnumber: this.selectedRefNumber, userlogin: nd });
        this.reportUtilService.viewFile('/api/report/tagihan_ho_2/pdf', {refnumber: this.selectedRefNumber, userlogin: nd});
        this.selectedRefNumber = '';
    }

    printBastToHO() {
        this.printBastKeHoModal = false;
        const nd = this.principal.getUserLogin();
        this.unitDeliverableService.setPrint(
            {refNumber: this.selectedRefNumber}
        ).subscribe ((res) => {
        });
        console.log('ref number', this.selectedRefNumber);
        this.vehicleDocumentRequirementCService.findbyRefNumber(this.selectedRefNumber).subscribe(
            (res) => {
                this.vdrAXDetailList = res;
                console.log('vdrAXDetail', this.vdrAXDetailList);

                const today = new Date();
                const myaxPostingLineOther4 = [];
                this.axPosting = new AxPosting();

                this.axPosting = new AxPosting();
                this.axPosting.AutoPosting = 'FALSE';
                this.axPosting.AutoSettlement = 'FALSE';
                this.axPosting.DataArea = this.vdrAXDetailList[0].idInternal.substr(0, 3) + ' ' + this.vdrAXDetailList[0].refNumber;
                this.axPosting.Guid = this.vdrAXDetailList[0].idReq;
                this.axPosting.JournalName = 'AP-PayBiroJasa';
                this.axPosting.Name = 'Payment Biro Jasa ' + this.vdrAXDetailList[0].idCustomer;
                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPosting.TransactionType = 'Unit';

                // credit
                this.vdrAXDetailList.forEach(
                    (item) => {
                        this.vdrAXDetail = item;

                        this.axPostingLine = new AxPostingLine();
                        this.axPostingLine.AccountNum = this.vdrAXDetail.idVendor;
                        this.axPostingLine.AccountType = 'Vend';
                        this.axPostingLine.AmountCurDebit = this.vdrAXDetail.bbn.toString();
                        this.axPostingLine.AmountCurCredit = '0';
                        this.axPostingLine.Company = this.vdrAXDetail.idInternal.substr(0, 3);
                        this.axPostingLine.DMSNum = this.vdrAXDetail.orderNumber;
                        this.axPostingLine.Description = 'Pembayaran BBN ' + this.vdrAXDetail.billingNumber;
                        this.axPostingLine.Dimension1 = '1014';
                        this.axPostingLine.Dimension2 = this.vdrAXDetail.idInternal.substr(0, 3);
                        this.axPostingLine.Dimension3 = '000';
                        this.axPostingLine.Dimension4 = '000';
                        this.axPostingLine.Dimension5 = '00';
                        this.axPostingLine.Dimension6 = '000';
                        this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                        this.axPostingLine.Invoice = this.vdrAXDetail.billingNumber;
                        this.axPostingLine.Payment = '';

                        myaxPostingLineOther4.push(this.axPostingLine);
                    }
                );

                if (this.vdrAXDetail.profitLoss !== 0) {
                    // debit
                    this.axPostingLine = new AxPostingLine();
                    this.axPostingLine.AccountType = 'Ledger';
                    if (this.vdrAXDetailList[0].totalProfitLoss < 0) {
                        this.axPostingLine.AccountNum = '822401';
                        this.axPostingLine.AmountCurDebit = Math.abs(this.vdrAXDetailList[0].totalProfitLoss).toString();
                        this.axPostingLine.AmountCurCredit = '0';
                    } else {
                        this.axPostingLine.AccountNum = '821004';
                        this.axPostingLine.AmountCurDebit = '0';
                        this.axPostingLine.AmountCurCredit = this.vdrAXDetailList[0].totalProfitLoss.toString();
                    }

                    this.axPostingLine.Company = this.vdrAXDetailList[0].idInternal.substr(0, 3);
                    this.axPostingLine.DMSNum = null;
                    this.axPostingLine.Description = 'Laba/rugi Pembayaran BBN ' + this.vdrAXDetailList[0].refNumber;
                    this.axPostingLine.Dimension1 = '1014';
                    this.axPostingLine.Dimension2 = this.vdrAXDetailList[0].idInternal.substr(0, 3);
                    this.axPostingLine.Dimension3 = '000';
                    this.axPostingLine.Dimension4 = '000';
                    this.axPostingLine.Dimension5 = '00';
                    this.axPostingLine.Dimension6 = '000';
                    this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                    this.axPostingLine.Invoice = this.vdrAXDetailList[0].refNumber;
                    this.axPostingLine.Payment = '';

                    myaxPostingLineOther4.push(this.axPostingLine);

                    if (this.vdrAXDetailList[0].totalProfitLoss > 0) {
                        this.axPostingLine = new AxPostingLine();
                        this.axPostingLine.AccountType = 'Ledger';
                        this.axPostingLine.AccountNum = '218904';
                        this.axPostingLine.AmountCurDebit = '0';
                        this.axPostingLine.AmountCurCredit = this.vdrAXDetailList[0].totalProfitLossPpn.toString();
                        this.axPostingLine.Company = this.vdrAXDetailList[0].idInternal.substr(0, 3);
                        this.axPostingLine.DMSNum = null;
                        this.axPostingLine.Description = 'PPN Laba/rugi Pembayaran BBN ' + this.vdrAXDetailList[0].refNumber;
                        this.axPostingLine.Dimension1 = '1014';
                        this.axPostingLine.Dimension2 = this.vdrAXDetailList[0].idInternal.substr(0, 3);
                        this.axPostingLine.Dimension3 = '000';
                        this.axPostingLine.Dimension4 = '000';
                        this.axPostingLine.Dimension5 = '00';
                        this.axPostingLine.Dimension6 = '000';
                        this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                        this.axPostingLine.Invoice = this.vdrAXDetailList[0].refNumber;
                        this.axPostingLine.Payment = '';

                        myaxPostingLineOther4.push(this.axPostingLine);
                    }
                }
                // debit
                this.axPostingLine = new AxPostingLine();
                this.axPostingLine.AccountNum = this.vdrAXDetailList[0].idAccount;
                this.axPostingLine.AccountType = 'Bank';
                this.axPostingLine.AmountCurDebit = '0';
                this.axPostingLine.AmountCurCredit = this.vdrAXDetailList[0].costHandling.toString();
                this.axPostingLine.Company = this.vdrAXDetailList[0].idInternal.substr(0, 3);
                this.axPostingLine.DMSNum = null;
                this.axPostingLine.Description = 'Pembayaran BBN ' + this.vdrAXDetailList[0].refNumber;
                this.axPostingLine.Dimension1 = '1014';
                this.axPostingLine.Dimension2 = this.vdrAXDetailList[0].idInternal.substr(0, 3);
                this.axPostingLine.Dimension3 = '000';
                this.axPostingLine.Dimension4 = '000';
                this.axPostingLine.Dimension5 = '00';
                this.axPostingLine.Dimension6 = '000';
                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                this.axPostingLine.Invoice = this.vdrAXDetailList[0].refNumber;
                this.axPostingLine.Payment = '';

                myaxPostingLineOther4.push(this.axPostingLine);

                this.axPosting.LedgerJournalLine = myaxPostingLineOther4;
                console.log('axPosting data :', this.axPosting);
                this.axPostingService.send(this.axPosting).subscribe(
                    (resaxPosting: ResponseWrapper) =>
                        console.log('Success : ', resaxPosting.json.Message),
                    (resaxPosting: ResponseWrapper) => {
                        this.onError(resaxPosting.json);
                        console.log('error ax posting : ', resaxPosting.json.Message);
                    }
                );
            }
        );

        this.toasterService.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/tagihan_ke_ho/pdf', {refnumber: this.selectedRefNumber, userlogin: nd });
        this.reportUtilService.viewFile('/api/report/tagihan_ho_2/pdf', {refnumber: this.selectedRefNumber, userlogin: nd});
        this.selectedRefNumber = '';
    }

    cancel() {
        this.printBastKeHoModal = false;
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
            this.registerChangeInVehicleDocumentRequirements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInVehicleDocumentRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleDocumentRequirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackListRefferenceByRefNumber(index: number, item: SummaryRefferal) {
        return item.refNumber;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        // this.vehicleDocumentRequirements = data;
        this.customVehicleDocumentRequirement = data;
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'VehicleDocumentRequirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleDocumentRequirementListModification',
                    content: 'Deleted an vehicleDocumentRequirement'
                    });
                });
            }
        });
    }

    setRefNumber() {
        this.refference.refNumber = this.selectedRefNumber;
        // this.unitDeliverableService.findRefference({
        //     idInternal : this.principal.getIdInternal(),
        //     refNumber : this.selectedRefNumber,
        // }).subscribe(
        //     (res: SummaryRefferal) => {
        //         this.refference = res;
        //         console.log('cek list references',  this.refference)
        //     },
        //     (res: SummaryRefferal) => this.onError(res));
        console.log('refferencesss', this.refference);
    }

    print() {
        this.toasterService.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/tagihan_ke_ho/pdf', {refnumber: this.selectedRefNumber});
    }

    showBastToHo() {
        this.printBastKeHoModal = true;
    }

    showUlangBastToHo() {
        this.printUlangBastKeHoModal = true;
    }
}
