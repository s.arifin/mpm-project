import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { UnitDeliverable, UnitDeliverableService } from '../unit-deliverable';
import { StatusType, StatusTypeService } from '../status-type';
import { Motor, MotorService } from '../motor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService, CommonUtilService } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
import { SummaryRefferal } from './summary-refferal.model';

@Component({
    selector: 'jhi-registration-penerimaan-bpkb-stnk-input',
    templateUrl: './registration-penerimaan-bpkb-stnk-input.component.html'
})
export class RegistrationPenerimaanBPKBSTNKInputComponent implements OnInit, OnDestroy, DoCheck {

    currentAccount: any;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    unitDeliverable: UnitDeliverable;
    summaryReference: SummaryRefferal;
    unitDeliverables: UnitDeliverable[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    statusTypes: StatusType[];
    motors: Motor[];

    tempArray: UnitDeliverable[];
    receiptQty: any;
    receiptNominal: number;

    platNomor: boolean;
    notice: boolean;
    stnk: boolean;
    bpkb: boolean;
    isSaving: Boolean;
    checkPlatNomor: Boolean;
    checkNotice: Boolean;
    checkStnk: Boolean;
    checkBpkb: Boolean;
    isChecking: Boolean;
    totalInput: number;
    costHandling: number;
    otherCost: number;
    refNumber: any;
    refDate: any;
    idVendor: any;
    inputQty: number;
    totalData: number;
    allowData: number;
    showPesan: boolean;
    isPlatNomor: boolean;
    isSTNK: boolean;
    isNotice: boolean;
    ceklistBpkb: boolean;
    checkNg: boolean;
    updateSTNK: boolean;
    disableQty: boolean;

    constructor(
        private vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        private unitDeliverableService: UnitDeliverableService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private commonUtilService: CommonUtilService,
        private loadingService: LoadingService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.receiptQty = 0;
        this.receiptNominal = 0;
        this.isSaving = false;
        this.isChecking = false;
        this.unitDeliverable = new UnitDeliverable();
        this.unitDeliverables = [];
        this.platNomor = false;
        this.notice = false;
        this.stnk = false;
        this.bpkb = false;
        this.totalInput = 0;
        this.costHandling = 0;
        this.otherCost = 0;
        this.refNumber = null;
        this.refDate = null;
        this.idVendor = null;
        this.inputQty = 0;
        this.totalData = 0;
        this.showPesan = false;
        this.isPlatNomor = false;
        this.isNotice = false;
        this.isSTNK = false;
        this.ceklistBpkb = false;
        this.checkNg = false;
        this.updateSTNK = false;
        this.disableQty = false;
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                idStatusType: 36,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json),
                );
            return;
        }
        this.vehicleDocumentRequirementService.query({
            idStatusType: 36,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/vehicle-document-requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vehicle-document-requirement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vehicle-document-requirement', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.unitDeliverableService.passingData.subscribe(
            (data) => {
                this.summaryReference = data;
                console.log('ini nerima subscribe', data);
            }
        );
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInVehicleDocumentRequirements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInVehicleDocumentRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('vehicleDocumentRequirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    private onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vehicleDocumentRequirements = data;
        // hasil passing dari update
        if (this.summaryReference.refNumber !== undefined || this.summaryReference.refNumber !== null) {
            this.refNumber = this.summaryReference.refNumber;
            this.refDate = this.summaryReference.refDate;
            this.idVendor = this.summaryReference.idVendor;
            this.receiptQty = this.summaryReference.receiptQty;
            this.inputQty = this.summaryReference.inputQty;
            console.log('ini input qty', this.inputQty)
            this.receiptNominal = this.summaryReference.receiptNominal;
            console.log('ini cek  receipt nominal lho', this.receiptNominal);
        }else {
            this.refNumber = null;
            this.refDate = null;
            this.idVendor = null;
            this.receiptQty = null;
            this.receiptNominal = 0;
            this.inputQty = 0;
        }
        console.log('ini cek inputqty lho', this.inputQty);
        console.log('ini cek refnumber lho', this.refNumber);
        console.log('ini cek receipt nominal lho', this.receiptNominal);
    }

    public cleardata() {
            this.refNumber = null;
            this.refDate = null;
            this.receiptQty = null;
            this.idVendor = null;
        console.log('hapus refnumber lho', this.refNumber);

    }

    private onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.vehicleDocumentRequirementService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    private onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'VehicleDocumentRequirement Saved', 'Data saved..');
    }

    private onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    onChangePlatNomor(checked) {
        if ( checked ) {
            this.unitDeliverable.platNomor = new Date();
        } else {
            this.unitDeliverable.platNomor = undefined;
        }
    }

    onChangeNotice(checked) {
        if ( checked ) {
            this.unitDeliverable.notice = new Date();
        } else {
            this.unitDeliverable.notice = undefined;
        }
    }

    onChangeSTNK(checked) {
        if ( checked ) {
            this.unitDeliverable.stnk = new Date();
        } else {
            this.unitDeliverable.stnk = undefined;
        }
    }

    onChangeBPKB(checked) {
        if ( checked ) {
            this.unitDeliverable.bpkb = new Date();
        } else {
            this.unitDeliverable.bpkb = undefined;
        }
    }

    addItem() {
        this.allowData = 0;
        this.totalData = 0;
        console.log('allowdata', this.allowData);
        console.log('totaldata', this.totalData);

        if (this.refNumber === null) {
            this.totalData = (this.unitDeliverables.length + 1);
            this.allowData = this.receiptQty;
            console.log('allowdata refkosong', this.allowData);
            console.log('totaldata refkosong', this.totalData);
        }else {
            this.totalData = (this.unitDeliverables.length + 1);
            if (this.isNotice === true || this.isPlatNomor === true || this.isSTNK === true ) {
                this.allowData = 1000;
            } else {
            // this.allowData = (this.receiptQty - this.inputQty + 1)
            this.allowData = (this.receiptQty - this.inputQty)
            }
            console.log('total yang boleh diisi', this.allowData);
            console.log('totaldata sudah terisi', this.totalData);
            console.log('totaldata input', this.inputQty);
        }
        console.log('refnumbernya', this.refNumber);

        if (this.allowData >= this.totalData ) {

        console.log('this.unitDeliverable: ', this.unitDeliverable);
        this.unitDeliverable.refNumber = this.refNumber;
        this.unitDeliverable.refDate = this.refDate;
        this.unitDeliverable.receiptQty = this.receiptQty;
        this.unitDeliverable.receiptNominal = this.receiptNominal;
        this.unitDeliverable.vehicleDocumentRequirement.costHandling = this.costHandling;
        this.unitDeliverable.vehicleDocumentRequirement.otherCost = this.otherCost;
        this.totalInput += (this.costHandling + this.otherCost);

        this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        this.unitDeliverable = new UnitDeliverable();
        this.platNomor = false;
        this.notice = false;
        this.stnk = false;
        this.bpkb = false;
        this.checkPlatNomor = false;
        this.checkNotice = false;
        this.checkStnk = false;
        this.checkBpkb = false;
        this.costHandling = 0;
        this.otherCost = 0;

        console.log('total input item', this.unitDeliverables.length);
        console.log('total input ges', this.totalInput);
        console.log('cost handling', this.costHandling, 'dan other cost ges', this.otherCost);
        console.log('cost handling vdr ges', this.unitDeliverable.vehicleDocumentRequirement.costHandling);
    } else {
        this.showPesan = true;
    }
}

    removeItem(data: any) {
        this.unitDeliverables.forEach((item, index) => {
            if (item.vehicleDocumentRequirement.vehicle.idFrame === data.vehicleDocumentRequirement.vehicle.idFrame
                && item.vehicleDocumentRequirement.vehicle.idMachine === data.vehicleDocumentRequirement.vehicle.idMachine) {

                this.unitDeliverables.splice(index, 1);

                this.tempArray = [];
                this.tempArray = this.unitDeliverables;

                this.unitDeliverables = [];
                this.unitDeliverables = this.unitDeliverables.concat(this.tempArray);
                this.totalInput -= (data.vehicleDocumentRequirement.costHandling + data.vehicleDocumentRequirement.otherCost)
            }
        });
    }

    saveList(): void {
        console.log('save list: ', this.unitDeliverables),
        this.unitDeliverableService.executeListProcess(101, null, this.unitDeliverables).subscribe(
            (res) => {
                console.log('this: ', res);
                this.router.navigate(['/vehicle-document-requirement/registration/penerimaan/bpkb-stnk']);
            },
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'vehicleDocumentRequirementListModification',
                    content: 'Deleted an vehicleDocumentRequirement'
                    });
                });
            }
        });
    }

    checkUnitOnSL(): void {
        const unitCheck = new UnitDeliverable();
        // unitCheck.shipmentPackageId = this.selectedshipmentPackageId;
        if (this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame !== '' &&
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame !== undefined &&
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame != null) {
            unitCheck.vehicleDocumentRequirement.vehicle.idFrame = this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame;
        }
        if (this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine !== '' &&
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine !== undefined &&
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine != null) {
            unitCheck.vehicleDocumentRequirement.vehicle.idMachine = this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine;
        }
        if (this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber !== '' &&
        this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber !== undefined &&
        this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber != null) {
        unitCheck.vehicleDocumentRequirement.requestPoliceNumber = this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber;
    }
        const objectBody: Object = {
            item: unitCheck
        };

        const objectHeader: Object = {
            execute: 'CheckUnitOnUnitDeliverable'
        };

        this.unitDeliverableService.execute(objectBody, objectHeader).subscribe(
            (res) => {
                if (res) {
                    console.log('result check', res);
                    this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame = res.vehicleDocumentRequirement.vehicle.idFrame;
                    this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine = res.vehicleDocumentRequirement.vehicle.idMachine;
                    this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber = res.vehicleDocumentRequirement.requestPoliceNumber;
                    this.idVendor = res.vehicleDocumentRequirement.idVendor;
                    this.unitDeliverable.stnk = res.stnk;
                    this.unitDeliverable.notice = res.notice;
                    this.unitDeliverable.platNomor = res.platNomor;
                    this.unitDeliverable.bpkb = res.bpkb;
                    this.unitDeliverable.bpkbNumber = res.bpkbNumber;
                    if (res.vehicleDocumentRequirement.costHandling != null) {
                        this.costHandling = res.vehicleDocumentRequirement.costHandling;
                    } else {
                        this.costHandling = 0;
                    }
                    if (res.vehicleDocumentRequirement.otherCost != null) {
                        this.otherCost = res.vehicleDocumentRequirement.otherCost;
                    } else {
                        this.otherCost = 0;
                    }
                    if (res.refNumber != null) {
                        this.refNumber = res.refNumber;
                        this.isNotice = true;
                        this.isSTNK = true;
                        this.isPlatNomor = true;
                        this.updateSTNK = true;
                        console.log('ini hasil cek updateSTNK', this.updateSTNK);
                        console.log('ini hasil cek notice', this.isNotice);
                        console.log('ini hasil cek stnk', this.isSTNK);
                        console.log('ini hasil cek plat', this.isPlatNomor);
                    }

                    if (res.notice != null && res.stnk != null ) {
                        this.checkNg = true;
                    }

                    if (res.notice == null ) {
                        this.checkNg = false;
                    }

                    if (res.refNumber == null) {
                        this.isNotice = false;
                        this.isSTNK = false;
                        this.isPlatNomor = false;
                        console.log('ini hasil cek notice', this.isNotice);
                        console.log('ini hasil cek stnk', this.isSTNK);
                        console.log('ini hasil cek plat', this.isPlatNomor);
                    }
                    if (res.refDate != null) {
                        this.refDate = res.refDate;
                    }
                    if (res.receiptQty != null) {
                        this.receiptQty = res.receiptQty;
                    }
                    if (res.receiptNominal != null) {
                        this.receiptNominal = res.receiptNominal;
                    }
                    this.doCheckPlatNomor(res);
                    this.doCheckNotice(res);
                    this.doCheckStnk(res);
                    this.doCheckBpkb(res);
                }
            },
            (err) => {
                this.commonUtilService.showError(err);
            }
        )
    }

    ngDoCheck(): void {
        console.log('ini stnk lho ges', this.checkNg);
        if (this.checkNg === false ) {
        if ( this.checkNotice === true || this.checkStnk === true || this.checkPlatNomor === true) {
            if (
                this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber === '' ||
                this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber == null
            ) {
                this.isSaving = true;
            } else {
                this.isSaving = false;
            }
        }

        if ( this.checkNotice === true || this.checkStnk === true || this.checkPlatNomor === true) {
            if (
                this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber !== '' ||
                this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber != null
            ) {
                this.isSaving = false;
            } else {
                this.isSaving = true;
            }
        }

        if ( this.checkBpkb === true) {
            if (
                this.unitDeliverable.bpkbNumber === '' ||
                this.unitDeliverable.bpkbNumber == null ||
                this.receiptQty === 0
            ) {
                this.isSaving = true;
            } else {
                this.isSaving = false;
            }
        }
        if ( this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame != null ||
             this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine != null ||
             this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber) {
                this.isChecking = false;
            } else {
                this.isChecking = true;
            }
        if (this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame === '' ||
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idFrame == null ||
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine === '' ||
            this.unitDeliverable.vehicleDocumentRequirement.vehicle.idMachine == null ||
            this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber === '' ||
            this.unitDeliverable.vehicleDocumentRequirement.requestPoliceNumber == null) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    if (this.checkNg === true ) {
        if ( this.checkBpkb === true) {
            if (
                this.unitDeliverable.bpkbNumber === '' ||
                this.unitDeliverable.bpkbNumber == null ||
                this.receiptQty === 0
            ) {
                this.isSaving = true;
            } else {
                this.isSaving = false;
            }
        }
    }
}

    doCheckPlatNomor(itm: UnitDeliverable) {
        if (itm.platNomor != null) {
            this.checkPlatNomor = true;
        } else {
            this.checkPlatNomor = false;
        }

    }

    doCheckNotice(itm: UnitDeliverable) {
        if (itm.notice != null) {
            this.checkNotice = true;
        } else {
            this.checkNotice = false;
        }

    }

    doCheckStnk(itm: UnitDeliverable) {
        if (itm.stnk != null) {
            this.checkStnk = true;
        } else {
            this.checkStnk = false;
        }

    }

    doCheckBpkb(itm: UnitDeliverable) {
        if (itm.bpkb != null) {
            this.checkBpkb = true;
        } else {
            this.checkBpkb = false;
        }

    }
}
// JM41E1109858
// JM21E2019683
// JFZ1E2676474
