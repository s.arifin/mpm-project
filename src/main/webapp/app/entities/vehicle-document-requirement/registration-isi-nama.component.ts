import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { VehicleDocumentRequirement } from './vehicle-document-requirement.model';
import { CustomVehicleDocumentRequirement} from './custom-vehicle-document-requirement.model';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement.service';
import { StatusType, StatusTypeService } from '../status-type';
import { Motor, MotorService } from '../motor';
import { LoadingService } from '../../layouts/loading/loading.service';
import { Internal, InternalService } from '../internal'
import { VehicleSalesOrder, VehicleSalesOrderService} from '../vehicle-sales-order';
import { CommunicationEventCDB, CommunicationEventCDBService} from '../communication-event-cdb';
import { ITEMS_PER_PAGE, ResponseWrapper, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { City, CityService} from '../city';
import { Province, ProvinceService} from '../province';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as RequirementConstrants from '../../shared/constants/vehicle-document-requirement.constants';
import { Village, VillageService } from '../village';
import { District, DistrictService } from '../district';

@Component({
    selector: 'jhi-registration-isi-nama',
    templateUrl: './registration-isi-nama.component.html'
})
export class RegistrationIsiNamaComponent implements OnInit, OnDestroy {
    idInternal: string;
    currentAccount: any;
    vehicleDocumentRequirement: VehicleDocumentRequirement;
    VehicleDocumentRequirementUpdated: VehicleDocumentRequirement[];
    vehicleDocumentRequirements: VehicleDocumentRequirement[];
    vehicleDocumentRequirementTMP: VehicleDocumentRequirement[];
    customVehicleDocumentRequirement: CustomVehicleDocumentRequirement[];
    vehicleSalesOrder: VehicleSalesOrder;
    hasilRegistrasi: VehicleDocumentRequirement;
    communicationEventCDB: CommunicationEventCDB;
    communicationEventCDBTMP: CommunicationEventCDB[];
    internal: Internal;
    error: any;
    success: any;
    // selected: VehicleDocumentRequirement[];
    selected: CustomVehicleDocumentRequirement[];
    selectedUpdate: CustomVehicleDocumentRequirement[];
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    idCustomer: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    namaFile: any;
    tglBirth: string;
    tglShippingList: string;

    downloadModal: boolean;
    cancelModal: boolean;

    statusTypes: StatusType[];
    motors: Motor[];

    udstk: string;
    cddb: string;
    filenameUDSTK: string;
    filenameCDDB: string;

    city: City;
    province: Province;
    citySurat: City;
    provinceSurat: Province;
    districtSurat: District;
    villageSurat: Village;
    displayInformation: boolean;
    information: string;

    constructor(
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected communicationEventCDBService: CommunicationEventCDBService,
        protected vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        protected cityService: CityService,
        protected provinceService: ProvinceService,
        protected districtService: DistrictService,
        protected villageService: VillageService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected internalService: InternalService,
        protected loadingService: LoadingService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected statusTypeService: StatusTypeService,
        protected motorService: MotorService,
        protected datePipe: DatePipe
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        this.vehicleDocumentRequirementTMP = new Array <VehicleDocumentRequirement>();
        this.customVehicleDocumentRequirement = new Array<CustomVehicleDocumentRequirement>();
        this.downloadModal = false;
        this.cancelModal = false;
        this.udstk = '';
        this.cddb = '';
        // this.selected = new Array<VehicleDocumentRequirement>();
        this.selected = new Array<CustomVehicleDocumentRequirement>();
        this.namaFile = {
            ahass: '',
            mainDealer: '',
            datetime: null,
            faktur: '',
            dealer: null,
            date: null,
            noUrut: null
        }
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.vehicleDocumentRequirementService.search({
                idStatusType: 10,
                statusKonfirmasi: false,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                statusPenerimaan: false,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.vehicleDocumentRequirementService.queryCustom({
            idStatusType: 10,
            statusKonfirmasi: false,
            idInternal: this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            statusPenerimaan: false,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/requirement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/registration/isi-nama', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/registration/isi-nama', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
            this.internalService.find(this.principal.getIdInternal())
                .subscribe((response) => { this.internal = response;
            });
        });
        this.registerChangeInRequirements();
        this.statusTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.statusTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VehicleDocumentRequirement) {
        return item.idRequirement;
    }

    registerChangeInRequirements() {
        this.eventSubscriber = this.eventManager.subscribe('requirementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequirement') {
            result.push('idRequirement');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop(),
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        // this.vehicleDocumentRequirements = data;
        this.customVehicleDocumentRequirement = data;
        console.log('this.custom', this.customVehicleDocumentRequirement);
    }

    protected onError(error) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.vehicleDocumentRequirementService.update(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.vehicleDocumentRequirementService.create(event.data)
                .subscribe((res: VehicleDocumentRequirement) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: VehicleDocumentRequirement) {
        this.toasterService.showToaster('info', 'Requirement Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.vehicleDocumentRequirementService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'requirementListModification',
                    content: 'Deleted an requirement'
                    });
                });
            }
        });
    }

    trackStatusTypeById(index: number, item: StatusType) {
        return item.idStatusType;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    showDownload() {
        this.downloadModal = true;
        this.vehicleDocumentRequirement = new VehicleDocumentRequirement();
        // this.vehicleDocumentRequirement = data;
        console.log('this.selected', this.selected);
    }

    protected createContentCDDB(data: Array<CustomVehicleDocumentRequirement>): string {
        console.log('cek dataaaaaa', data);
        let cddb: string;
        cddb = '';

        const picName = null;
        if (data.length > 0) {
            for ( const i of this.selected) {
                if (i.gender !== null) {
                    if (i.gender === 'P') {
                        i.gender = 1;
                    }
                    if (i.gender === 'W') {
                        i.gender = 2;
                    }
                }

                if (i.dob !== null) {
                    // i.dob = new Date(i.dob).toString;
                    this.tglBirth = '';
                    const partsBirth = i.dob.split('-');
                    const yearBirth = partsBirth[0];
                    const monthBirth = partsBirth[1];
                    const dayBirth = partsBirth[2].slice(0, 2).toString();

                    this.tglBirth = dayBirth + '' + monthBirth + '' + yearBirth;
                }
                if (i.shippingListDate !== null) {
                    this.tglShippingList = '';
                    const partsshippingListDate = i.shippingListDate.split('-');
                    const yearshippingListDate = partsshippingListDate[0];
                    const monthshippingListDate = partsshippingListDate[1];
                    const dayshippingListDate = partsshippingListDate[2].slice(0, 2).toString();

                    this.tglShippingList = dayshippingListDate + '' + monthshippingListDate + '' + yearshippingListDate;
                }
                // }
                cddb += i.idMachine.slice(0, 5) + ';'; // machine 5 pertama
                cddb += i.idMachine.slice(5) + ';'; // machine 7 terakhir
                cddb += (i.personalIdNumber !== null ) ? i.personalIdNumber + ';' : '' + ';';
                cddb += (i.groupCustomer !== null ) ? i.groupCustomer + ';' : '' + ';';
                cddb += (i.gender !== null ) ? i.gender + ';' : 'N' + ';'; // gender
                cddb += (i.dob !== null ) ? this.tglBirth + ';' : '' + ';';
                cddb += (i.addressSurat !== null ) ? i.addressSurat + ';' : '' + ';';
                cddb += (i.codeVillageSurat !== null ) ? i.codeVillageSurat + ';' : '' + ';';
                cddb += (i.codeDistrictSurat !== null ) ? i.codeDistrictSurat + ';' : '' + ';';
                cddb += (i.codeCitySurat !== null ) ? i.codeCitySurat + ';' : '' + ';';
                cddb += (i.postalCodeSurat !== null ) ? i.postalCodeSurat + ';' : '60000' + ';'; // kodesurat
                cddb += (i.codeProvinceSurat !== null ) ? i.codeProvinceSurat + ';' : '' + ';';
                cddb += (i.religionTypeId !== null ) ? i.religionTypeId + ';' : 'N' + ';';
                cddb += (i.workTypeId !== null ) ? i.workTypeId + ';' : 'N' + ';';
                cddb += (i.expenditureOneMonth !== null ) ? i.expenditureOneMonth + ';' : 'N' + ';';
                cddb += (i.lastEducation !== null ) ? i.lastEducation + ';' : 'N' + ';';
                cddb += (i.picName !== null) ? i.picName + ';' : 'N' + ';'; // nama pic
                cddb += (i.cellPhone1 !== null ) ? i.cellPhone1 + ';' : '0' + ';';
                cddb += (i.cellPhone2 !== null ) ? i.cellPhone2 + ';' : '0' + ';';
                cddb += (i.infoAboutHonda !== null ) ? i.infoAboutHonda + ';' : '' + ';';
                cddb += (i.vehicleBrandOwner !== null ) ? i.vehicleBrandOwner + ';' : '' + ';';
                cddb += (i.vehicleTypeOwner !== null ) ? i.vehicleTypeOwner + ';' : '' + ';';
                cddb += (i.buyFor !== null ) ? i.buyFor + ';' : '' + ';';
                cddb += (i.vehicleUser !== null ) ? i.vehicleUser + ';' : '' + ';';
                cddb += (i.salesCode !== null ) ? i.salesCode + ';' : '' + ';';
                cddb += (i.email !== null ) ? i.email + ';' : '' + ';';
                cddb += (i.homeStatus !== null ) ? i.homeStatus + ';' : '' + ';';
                cddb += (i.mobilePhoneOwnershipStatus !== null ) ? i.mobilePhoneOwnershipStatus + ';' : 'N' + ';';
                cddb += (i.facebook !== null ) ? i.facebook + ';' : 'N' + ';';
                cddb += (i.twitter !== null ) ? i.twitter + ';' : 'N' + ';';
                cddb += (i.instagram !== null ) ? i.instagram + ';' : 'N' + ';';
                cddb += (i.youtube !== null ) ? i.youtube + ';' : 'N' + ';';
                cddb += (i.hobby !== null ) ? i.hobby + ';' : '' + ';';
                cddb += (i.keterangan !== null ) ? i.keterangan + ';' : '' + ';'; // keterangan
                cddb += (i.citizenship !== null ) ? i.citizenship + ';' : '' + ';'; // kewarganegaan
                cddb += (i.familyIdNumber) ? i.familyIdNumber + ';' : '' + ';'; // nomor kartu keluarga
                cddb += ';'; // ref id
                cddb += ';'; // robd id
                cddb += ';'; // kode flp koordinator
                cddb += ';'; // seri nomor ro
                cddb += ';'; // nomor mesin ro
                cddb += (i.shippingListDate !== null) ? this.tglShippingList + ';' : '' + ';'; // tanggal spg
                cddb += '\r\n' ;
            }
        }

        return cddb;
    }

    protected createContentUDSTK(data: Array<CustomVehicleDocumentRequirement>): string {
        let udstk: string;
        udstk = '';

        if (data.length > 0) {
            for ( const i of data) {
                udstk += i.idFrame + ';'; //  frame
                udstk += i.idMachine.slice(0, 5) + ';'; // machine 5 pertama
                udstk += i.idMachine.slice(5) + ';'; // machine 7 terakhir
                udstk += (i.name) ? i.name + ';' : '' + ';'; // owner name
                udstk += (i.address1) ? i.address1 + ';' : '' + ';'; // owner address
                udstk += (i.village) ? i.village + ';' : '' + ';';  // kelurahan
                udstk += (i.district) ? i.district + ';' : '' + ';'; // kecamatan
                udstk += (i.city) ? i.city + ';' : '' + ';'; // kota
                udstk += (i.postalCode) ? i.postalCode + ';' : '60000' + ';'; // kodepos
                udstk += (i.province) ? i.province + ';' : '' + ';'; // propinsi
                // udstk += (i.jenisPembayaran) ? i.jenisPembayaran + ';' : '' + ';'; // jenis pembayaran
                udstk += (i.fincoy) ? '2' + ';' : '1' + ';'; // jenis pembayaran
                udstk += this.internal.idAHM + ';'; // kode dealer ambil dari idahm
                udstk += (i.fincoy) ? i.fincoy + ';' : 'TUNAI' + ';' ; // kode fincoy
                if (i.fincoy != null) {
                    udstk += (i.creditDownPayment) ? i.creditDownPayment + ';' : '1' + ';'; // DP
                } else {
                    udstk += (i.creditDownPayment) ? i.creditDownPayment + ';' : '0' + ';'; // DP
                }
                udstk += (i.creditTenor) ? i.creditTenor + ';' : '0' + ';'; // tenor
                udstk += (i.creditInstallment) ? i.creditInstallment + ';' : '0' + ';'; // cicilan
                udstk += ';';
                // this.udstk += i.productId + ';';
                // this.udstk += 'kodePos;';
                // this.udstk += 'ID POS;';
                udstk += '\r\n' ;
            }
        }

        return udstk;
    }

    downloadlist() {
        this.downloadModal = false;
        // const picName = null;
        this.vehicleDocumentRequirementTMP = new Array <VehicleDocumentRequirement>();
        this.VehicleDocumentRequirementUpdated = new Array<VehicleDocumentRequirement>();
        for (let i = 0; i < this.selected.length; i++) {
            this.vehicleDocumentRequirementTMP.push({
                idRequirement : this.selected[i].idRequirement
            });
        }

        this.vehicleDocumentRequirementService.executeListProcess(101, this.principal.getIdInternal(), this.vehicleDocumentRequirementTMP)
        .subscribe((value) => {
            this.VehicleDocumentRequirementUpdated = value;
            this.vehicleDocumentRequirementService.find(this.vehicleDocumentRequirementTMP[0].idRequirement)
            .subscribe((registrasi) => {
                this.hasilRegistrasi = registrasi;
                console.log('hasil registrasi', this.hasilRegistrasi)
            for (let i = 0; i < this.selected.length; i++) {
                this.mappingCDB(this.selected[i]);
            }
        }
        )},
        (err) => console.log('error download list', err),
        () => {
            setTimeout(() => {
                this.loadAll();
                this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
            }, 1000);
        });
    }

    mappingCDB(item): any {
                console.log('masuk proses FINDVSOANDCDBALTTTTTT');
                this.communicationEventCDBService.queryByIdCustomer({
                    idCustomer: item.idCustomer
                })
                .flatMap(
                (data) => {
                this.communicationEventCDB = data.json[0];
                console.log('cedebeeee', this.communicationEventCDB);
                // mapping dari cdb
                item.expenditureOneMonth = this.communicationEventCDB.expenditureOneMonth;
                item.lastEducation = this.communicationEventCDB.lastEducation;
                item.infoAboutHonda = this.communicationEventCDB.infoAboutHonda;
                item.vehicleBrandOwner = this.communicationEventCDB.vehicleBrandOwner;
                item.vehicleTypeOwner = this.communicationEventCDB.vehicleTypeOwner;
                item.buyFor = this.communicationEventCDB.buyFor;
                item.vehicleUser = this.communicationEventCDB.vehicleUser;
                item.email = this.communicationEventCDB.email;
                item.homeStatus = this.communicationEventCDB.homeStatus;
                item.mobilePhoneOwnershipStatus = this.communicationEventCDB.mobilePhoneOwnershipStatus;
                item.facebook = this.communicationEventCDB.facebook;
                item.instagram = this.communicationEventCDB.instagram;
                item.twitter = this.communicationEventCDB.twitter;
                item.youtube = this.communicationEventCDB.youtube;
                item.hobby = this.communicationEventCDB.hobby;
                item.keterangan = this.communicationEventCDB.keterangan;
                item.citizenship = this.communicationEventCDB.citizenship;
                item.groupCustomer = this.communicationEventCDB.groupCustomer;
                item.jenisPembayaran = this.communicationEventCDB.jenisPembayaran;
                item.idProvinceSurat = this.communicationEventCDB.idProvinceSurat;
                item.idCitySurat = this.communicationEventCDB.idCitySurat;
                item.idDistrictSurat = this.communicationEventCDB.idDistrictSurat;
                item.idVillageSurat = this.communicationEventCDB.idVillageSurat;
                item.addressSurat = this.communicationEventCDB.addressSurat;
                item.workTypeId = this.communicationEventCDB.job;
                // item.address1 = this.communicationEventCDB.address;
                item.postalCode = this.communicationEventCDB.postalCode;
                item.postalCodeSurat = this.communicationEventCDB.postalCodeSurat;
                return this.provinceService.find(this.communicationEventCDB.idProvinceSurat);
            })
            .subscribe(
                (value) => {this.provinceSurat = value; },
                // Jika error, atau tidak ada
                (error: any) => {console.log('error fungsi findvsoandcdbalt', error) },
                // Jika sudah selesai
                () => {
                    this.fungsiDownload();
                }
            );
    }

    fungsiDownload() {
        console.log('cek id internal from internal', this.internal);
        const date = new Date();
        const day = date.getDate().toString();
        const monthIndex = date.getMonth() + 1;
        const month = monthIndex.toString();
        const year = date.getFullYear().toString();
        const minutes = date.getMinutes().toString();
        const hours = date.getHours().toString();
        const seconds = date.getSeconds().toString();

        const regisNumber = this.hasilRegistrasi.registrationNumber;
        /* PREPARING ATTRIBUTE FOR DOWNLOAD */
        this.namaFile = {
            ahass: this.internal.idAHM,
            mainDealer: 'M2Z',
            datetime: year + '' + (month) + '' + day + '' + hours + '' + minutes + '' + seconds,
            faktur: 'FKTR',
            dealer: this.principal.getIdInternal().toString(),
            date: year + '_' + (month),
            noUrut: regisNumber.slice(17)
        }
        const filetype = 'text/plain';
        let filename: string;

        /* PROCESSING UDSTK */
        filename = this.namaFile.ahass + '-' + this.namaFile.mainDealer + '-' + this.namaFile.datetime + '-' + this.namaFile.faktur + '_' + this.namaFile.dealer + '_' + this.namaFile.date + '_' + this.namaFile.noUrut + '.udstk';

        const udstk = this.createContentUDSTK(this.selected);
        this.processDownload(udstk, filename, filetype);
        /* PROCESSING CDDB */
        filename = this.namaFile.ahass + '-' + this.namaFile.mainDealer + '-' + this.namaFile.datetime + '-' + this.namaFile.faktur + '-' + this.namaFile.dealer + '-' + this.namaFile.date + '-' + this.namaFile.noUrut + '.cddb';

        const cddb = this.createContentCDDB(this.selected);
        console.log('data cust yang mau dicddb ==>', this.selected);
        this.processDownload(cddb, filename, filetype);
        this.selected = new Array<CustomVehicleDocumentRequirement>();
    }

    protected processDownload(content: string, filename: string, filetype: string): void {
        if (content === null || content === '') {
            console.log('masuk ke iffff');
        } else {
            let a: any;
            a = document.createElement('a');
            let dataURI: any;
            dataURI = 'data:' + filetype + ';base64,' + btoa(content.replace(/[‘’]/g, '\'').replace(/[“”]/g, '"'));

            a.href = dataURI;
            a['download'] = filename;

            let e: any;
            e = document.createEvent('MouseEvents');

            // Use of deprecated function to satisfy TypeScript.
            e.initMouseEvent('click', true, false, document.defaultView,
                0, 0, 0, 0, 0,
                true, false, false, false,
                0, null);
            a.dispatchEvent(e);
        }
    }

    processFaktur(idRequirement: string) {
        this.vehicleDocumentRequirement.idRequirement = idRequirement;
        console.log('data', this.vehicleDocumentRequirement);

        this.vehicleDocumentRequirementService.executeProcess(102, null, this.vehicleDocumentRequirement).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    cancelDataIVU(dataParam) {
        console.log('dataParam', dataParam);
        this.vehicleDocumentRequirementService.executeProcess(RequirementConstrants.CANCEL_PENGAJUAN, null, dataParam).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
         );
    }

    cancelDataPengajuan() {
        console.log('pengajuan');
    }

    buildReindex() {
        this.vehicleDocumentRequirementService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }

    activated() {
        console.log('masuk activated');
            const filledForm: string = this.vehicleDocumentRequirementService.validationSubmitToIsiNama(this.selected);
            if (filledForm === '') {
                console.log('filled form kosong');
                this.downloadlist()
            } else {
                    this.downloadModal = false
                    console.log('filled form ada isinya', filledForm);
                    this.information = filledForm;
                    this.displayInformation = true;
            }
    }

}
