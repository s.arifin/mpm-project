import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {VehicleDocumentRequirement} from './vehicle-document-requirement.model';
import {VehicleDocumentRequirementPopupService} from './vehicle-document-requirement-popup.service';
import {VehicleDocumentRequirementService} from './vehicle-document-requirement.service';
import {ToasterService} from '../../shared';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-document-requirement-dialog',
    templateUrl: './vehicle-document-requirement-dialog.component.html'
})
export class VehicleDocumentRequirementDialogComponent implements OnInit {

    vehicleDocumentRequirement: VehicleDocumentRequirement;
    isSaving: boolean;

    salesunitrequirements: SalesUnitRequirement[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private vehicleDocumentRequirementService: VehicleDocumentRequirementService,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private eventManager: JhiEventManager,
        private toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.salesUnitRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.salesunitrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehicleDocumentRequirement.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.update(this.vehicleDocumentRequirement));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleDocumentRequirementService.create(this.vehicleDocumentRequirement));
        }
    }

    private subscribeToSaveResponse(result: Observable<VehicleDocumentRequirement>) {
        result.subscribe((res: VehicleDocumentRequirement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: VehicleDocumentRequirement) {
        this.eventManager.broadcast({ name: 'vehicleDocumentRequirementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicleDocumentRequirement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.toaster.showToaster('warning', 'vehicleDocumentRequirement Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackSalesUnitRequirementById(index: number, item: SalesUnitRequirement) {
        return item.idRequirement;
    }
}

@Component({
    selector: 'jhi-vehicle-document-requirement-popup',
    template: ''
})
export class VehicleDocumentRequirementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vehicleDocumentRequirementPopupService: VehicleDocumentRequirementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehicleDocumentRequirementPopupService
                    .open(VehicleDocumentRequirementDialogComponent as Component, params['id']);
            } else {
                this.vehicleDocumentRequirementPopupService
                    .open(VehicleDocumentRequirementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
