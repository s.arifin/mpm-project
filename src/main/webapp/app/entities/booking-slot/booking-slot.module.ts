import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    BookingSlotService,
    BookingSlotPopupService,
    BookingSlotComponent,
    BookingSlotDetailComponent,
    BookingSlotDialogComponent,
    BookingSlotSelectComponent,
    BookingSlotPopupComponent,
    BookingSlotSelectPopupComponent,
    bookingSlotRoute,
    bookingSlotPopupRoute,
    BookingSlotResolvePagingParams,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...bookingSlotRoute,
    ...bookingSlotPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        BookingSlotComponent,
        BookingSlotDetailComponent,
        BookingSlotSelectComponent,
    ],
    declarations: [
        BookingSlotComponent,
        BookingSlotDetailComponent,
        BookingSlotDialogComponent,
        BookingSlotSelectComponent,
        BookingSlotPopupComponent,
        BookingSlotSelectPopupComponent
    ],
    entryComponents: [
        BookingSlotComponent,
        BookingSlotDialogComponent,
        BookingSlotSelectComponent,
        BookingSlotPopupComponent,
        BookingSlotSelectPopupComponent
    ],
    providers: [
        BookingSlotService,
        BookingSlotPopupService,
        BookingSlotResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmBookingSlotModule {}
