import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { BookingSlot } from './booking-slot.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class BookingSlotService {

    protected itemValues: BookingSlot[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/booking-slots';
    protected resourceSearchUrl = 'api/_search/booking-slots';

    constructor(protected http: Http) { }

    pushItem(data: BookingSlot[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    create(bookingSlot: BookingSlot): Observable<BookingSlot> {
        const copy = this.convert(bookingSlot);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(bookingSlot: BookingSlot): Observable<BookingSlot> {
        const copy = this.convert(bookingSlot);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<BookingSlot> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, bookingSlot: any): Observable<BookingSlot> {
        const copy = this.convert(bookingSlot);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, bookingSlots: any): Observable<BookingSlot[]> {
        const copy = this.convert(bookingSlots);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(bookingSlot: BookingSlot): BookingSlot {
        const copy: BookingSlot = Object.assign({}, bookingSlot);
        return copy;
    }
}
