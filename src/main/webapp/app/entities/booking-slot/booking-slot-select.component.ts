import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BookingSlot} from './booking-slot.model';
import {BookingSlotPopupService} from './booking-slot-popup.service';
import {BookingSlotService} from './booking-slot.service';
import {ToasterService} from '../../shared';
import { BaseCalendar, BaseCalendarService } from '../shared-component';
import { BookingSlotStandard, BookingSlotStandardService } from '../booking-slot-standard';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-booking-slot-select',
    templateUrl: './booking-slot-select.component.html'
})
export class BookingSlotSelectComponent implements OnInit {

    bookingSlot: BookingSlot;
    isSaving: boolean;

    basecalendars: BaseCalendar[];

    bookingslotstandards: BookingSlotStandard[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected bookingSlotService: BookingSlotService,
        protected baseCalendarService: BaseCalendarService,
        protected bookingSlotStandardService: BookingSlotStandardService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
       this.bookingSlotService.query().subscribe((r) => {
            this.bookingslotstandards = r.json;
       });
    }

    selectBooking(obj) {
        const Items = new Array<BookingSlot>();
        Items.push(obj);
        this.bookingSlotService.pushItem(Items);
        this.activeModal.dismiss('close');
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'jhi-booking-slot-select-popup',
    template: ''
})
export class BookingSlotSelectPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected bookingSlotPopupService: BookingSlotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bookingSlotPopupService
                    .open(BookingSlotSelectComponent as Component, params['id']);
            } else {
                this.bookingSlotPopupService
                    .open(BookingSlotSelectComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
