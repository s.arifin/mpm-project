import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BookingSlotComponent } from './booking-slot.component';
import { BookingSlotDetailComponent } from './booking-slot-detail.component';
import { BookingSlotPopupComponent } from './booking-slot-dialog.component';
import { BookingSlotSelectPopupComponent } from './booking-slot-select.component';

@Injectable()
export class BookingSlotResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idBookSlot,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bookingSlotRoute: Routes = [
    {
        path: 'booking-slot',
        component: BookingSlotComponent,
        resolve: {
            'pagingParams': BookingSlotResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlot.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'booking-slot/:id',
        component: BookingSlotDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlot.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bookingSlotPopupRoute: Routes = [
    {
        path: 'booking-select-slot',
        component: BookingSlotSelectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Select'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-slot-new',
        component: BookingSlotPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlot.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-slot/:id/edit',
        component: BookingSlotPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.bookingSlot.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
