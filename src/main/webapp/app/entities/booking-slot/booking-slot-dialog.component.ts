import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {BookingSlot} from './booking-slot.model';
import {BookingSlotPopupService} from './booking-slot-popup.service';
import {BookingSlotService} from './booking-slot.service';
import {ToasterService} from '../../shared';
import { BaseCalendar, BaseCalendarService } from '../shared-component';
import { BookingSlotStandard, BookingSlotStandardService } from '../booking-slot-standard';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-booking-slot-dialog',
    templateUrl: './booking-slot-dialog.component.html'
})
export class BookingSlotDialogComponent implements OnInit {

    bookingSlot: BookingSlot;
    isSaving: boolean;

    basecalendars: BaseCalendar[];

    bookingslotstandards: BookingSlotStandard[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected bookingSlotService: BookingSlotService,
        protected baseCalendarService: BaseCalendarService,
        protected bookingSlotStandardService: BookingSlotStandardService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.baseCalendarService.queryByType(3, {size: 1000})
            .subscribe((res: ResponseWrapper) => { this.basecalendars = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.bookingSlotStandardService.query()
            .subscribe((res: ResponseWrapper) => { this.bookingslotstandards = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bookingSlot.idBookSlot !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingSlotService.update(this.bookingSlot));
        } else {
            this.subscribeToSaveResponse(
                this.bookingSlotService.create(this.bookingSlot));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BookingSlot>) {
        result.subscribe((res: BookingSlot) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: BookingSlot) {
        this.eventManager.broadcast({ name: 'bookingSlotListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'bookingSlot saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'bookingSlot Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBaseCalendarById(index: number, item: BaseCalendar) {
        return item.idCalendar;
    }

    trackBookingSlotStandardById(index: number, item: BookingSlotStandard) {
        return item.idBookSlotStd;
    }
}

@Component({
    selector: 'jhi-booking-slot-popup',
    template: ''
})
export class BookingSlotPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected bookingSlotPopupService: BookingSlotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bookingSlotPopupService
                    .open(BookingSlotDialogComponent as Component, params['id']);
            } else {
                this.bookingSlotPopupService
                    .open(BookingSlotDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
