import { BaseEntity } from './../../shared';

export class BookingSlot implements BaseEntity {
    constructor(
        public id?: any,
        public idBookSlot?: any,
        public slotNumber?: string,
        public calendarId?: any,
        public standardId?: any,
    ) {
    }
}
