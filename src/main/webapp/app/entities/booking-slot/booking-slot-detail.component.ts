import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {BookingSlot} from './booking-slot.model';
import {BookingSlotService} from './booking-slot.service';

@Component({
    selector: 'jhi-booking-slot-detail',
    templateUrl: './booking-slot-detail.component.html'
})
export class BookingSlotDetailComponent implements OnInit, OnDestroy {

    bookingSlot: BookingSlot;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected bookingSlotService: BookingSlotService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBookingSlots();
    }

    load(id) {
        this.bookingSlotService.find(id).subscribe((bookingSlot) => {
            this.bookingSlot = bookingSlot;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBookingSlots() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bookingSlotListModification',
            (response) => this.load(this.bookingSlot.idBookSlot)
        );
    }
}
