export * from './booking-slot.model';
export * from './booking-slot-popup.service';
export * from './booking-slot.service';
export * from './booking-slot-dialog.component';
export * from './booking-slot-detail.component';
export * from './booking-slot-select.component';
export * from './booking-slot.component';
export * from './booking-slot.route';
