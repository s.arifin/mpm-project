import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { GeoBoundaryTypeComponent } from './geo-boundary-type.component';
import { GeoBoundaryTypePopupComponent } from './geo-boundary-type-dialog.component';

@Injectable()
export class GeoBoundaryTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idGeobouType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const geoBoundaryTypeRoute: Routes = [
    {
        path: 'geo-boundary-type',
        component: GeoBoundaryTypeComponent,
        resolve: {
            'pagingParams': GeoBoundaryTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.geoBoundaryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const geoBoundaryTypePopupRoute: Routes = [
    {
        path: 'geo-boundary-type-new',
        component: GeoBoundaryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.geoBoundaryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'geo-boundary-type/:id/edit',
        component: GeoBoundaryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.geoBoundaryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
