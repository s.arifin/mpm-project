import { BaseEntity } from './../../shared';

export class ReasonType implements BaseEntity {
    constructor(
        public id?: number,
        public idReason?: number,
        public description?: string,
    ) {
    }
}
