import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ReasonType } from './reason-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ReasonTypeService {
    protected itemValues: ReasonType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/reason-types';
    protected resourceSearchUrl = 'api/_search/reason-types';

    constructor(protected http: Http) { }

    create(reasonType: ReasonType): Observable<ReasonType> {
        const copy = this.convert(reasonType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(reasonType: ReasonType): Observable<ReasonType> {
        const copy = this.convert(reasonType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ReasonType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, reasonType: ReasonType): Observable<ReasonType> {
        const copy = this.convert(reasonType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, reasonTypes: ReasonType[]): Observable<ReasonType[]> {
        const copy = this.convertList(reasonTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(reasonType: ReasonType): ReasonType {
        if (reasonType === null || reasonType === {}) {
            return {};
        }
        // const copy: ReasonType = Object.assign({}, reasonType);
        const copy: ReasonType = JSON.parse(JSON.stringify(reasonType));
        return copy;
    }

    protected convertList(reasonTypes: ReasonType[]): ReasonType[] {
        const copy: ReasonType[] = reasonTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ReasonType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
