import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReasonTypeComponent } from './reason-type.component';
import { ReasonTypePopupComponent } from './reason-type-dialog.component';

@Injectable()
export class ReasonTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReason,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const reasonTypeRoute: Routes = [
    {
        path: 'reason-type',
        component: ReasonTypeComponent,
        resolve: {
            'pagingParams': ReasonTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.reasonType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const reasonTypePopupRoute: Routes = [
    {
        path: 'reason-type-new',
        component: ReasonTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.reasonType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'reason-type/:id/edit',
        component: ReasonTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.reasonType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
