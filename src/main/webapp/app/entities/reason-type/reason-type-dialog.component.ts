import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ReasonType} from './reason-type.model';
import {ReasonTypePopupService} from './reason-type-popup.service';
import {ReasonTypeService} from './reason-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-reason-type-dialog',
    templateUrl: './reason-type-dialog.component.html'
})
export class ReasonTypeDialogComponent implements OnInit {

    reasonType: ReasonType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected reasonTypeService: ReasonTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.reasonType.idReason !== undefined) {
            this.subscribeToSaveResponse(
                this.reasonTypeService.update(this.reasonType));
        } else {
            this.subscribeToSaveResponse(
                this.reasonTypeService.create(this.reasonType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ReasonType>) {
        result.subscribe((res: ReasonType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ReasonType) {
        this.eventManager.broadcast({ name: 'reasonTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'reasonType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'reasonType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-reason-type-popup',
    template: ''
})
export class ReasonTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected reasonTypePopupService: ReasonTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.reasonTypePopupService
                    .open(ReasonTypeDialogComponent as Component, params['id']);
            } else {
                this.reasonTypePopupService
                    .open(ReasonTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
