import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MpmSharedModule } from '../shared';
import { TabsModule } from 'ngx-bootstrap/tabs';

/* jhipster-needle-add-list-import - JHipster will add entity modules imports here */

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         RadioButtonModule
} from 'primeng/primeng';

import { StockOpnameItemAsListComponent } from './stock-opname-item/';
import { OrderItemAsListComponent } from './order-item/';
import { OrderItemUnitAsListComponent } from './order-item/';
import { ProductShipmentReceiptAsListComponent } from './product-shipment-receipt';
import { UnitShipmentReceiptAsListComponent } from './unit-shipment-receipt';
import { ShipmentItemAsListComponent, ShipmentItemSpgAsListComponent } from './shipment-item';
import { ShipmentReceiptAsListComponent } from './shipment-receipt';
import { ReceiptAsListComponent, ReceiptService } from './receipt';
import { DisbursementAsListComponent } from './disbursement';
import { BillingItemAsListComponent, BillingItemInBillingDisbursementAsListComponent } from './billing-item';
import { PaymentApplicationAsListComponent } from './payment-application';
import { ContainerAsListComponent } from './container';
import { FacilityAsListComponent } from './facility';
import { UomAsListComponent } from './uom';
import { UomConversionAsListComponent } from './uom-conversion';
import { FeatureAsListComponent } from './feature';
import { ProductCategoryAsListComponent } from './product-category';
import { PartyCategoryAsListComponent } from './party-category';
import { PaymentMethodAsListComponent } from './payment-method';
import { RequestItemAsListComponent, RequestItemInMovingSlipAsListComponent } from './request-item';
import { CommunicationEventProspectAsListComponent } from './communication-event-prospect';
import { RuleIndentAsListComponent } from './rule-indent';
import { CustomerRequestItemAsListComponent } from './customer-request-item';
import { RequestRequirementAsListComponent } from './request-requirement';
import { PartyFacilityPurposeAsListComponent } from './party-facility-purpose';
import { ProductPackageReceiptAsListComponent } from './product-package-receipt';
import { OrderBillingItemAsListComponent } from './order-billing-item';
import { ShipmentBillingItemAsListComponent } from './shipment-billing-item';
import { OrderPaymentAsListComponent } from './order-payment';
import { FacilityContactMechanismAsListComponent } from './facility-contact-mechanism';
import { RequirementPaymentAsListComponent } from './requirement-payment';
import { GoodContainerAsListComponent } from './good-container';
import { UnitDocumentMessageAsListComponent, UnitDocumentMessageService, UnitDocumentMessageByRequirementComponent } from './unit-document-message';
import { OrderShipmentItemAsListComponent } from './order-shipment-item';
import { RequestUnitInternalAsListComponent } from './request-unit-internal';
import { UnitPreparationAsListComponent } from './unit-preparation';
import { GeneralUploadAsListComponent } from './general-upload';
import { UnitDeliverableResolvePagingParams } from './shared-component/services/share-paging-params';
import { FeatureApplicableService, FeatureApplicableAsListComponent } from './feature-applicable';
import { CustomerService } from './customer';
import { ProductService } from './product';
import { PartyService } from './party';
import { RoleTypeService } from './role-type';
import { StatusTypeService } from './status-type';
import { PersonService } from './person';
import { OrganizationService } from './organization';
import { InternalService } from './internal';
import { NewPersonalCustomerService, GoodService, PersonalCustomerService,
         OrganizationCustomerService, RequirementService, WorkEffortService,
         SurOrganizationViewComponent, PersonBaseViewComponent, NumberOnlyDirective,
         CommunicationEventDeliveryAsListComponent, CommunicationEventProspectAsHistoryFollowUpComponent,
         DistrictViewComponent, ProvinceViewComponent, CustomerViewComponent,
         ProvinceViewNetComponent, CityViewNetComponent, DistrictViewNetComponent, VillageViewNetComponent,
         OrganizationDetailViewComponent, OrganizationViewComponent, PersonViewComponent, OrganizationViewNetComponent,
         CityViewComponent, PartyDocumentAsListComponent, VillageViewComponent, FetchingDataComponent } from './shared-component';
import { BaseCalendarService } from './base-calendar';
import { PackageReceiptService } from './package-receipt';
import { VendorService } from './vendor';
import { VendorTypeService } from './vendor-type';
import { RemPartService } from './rem-part';
import { VehicleDocumentRequirementService } from './vehicle-document-requirement';
import { UnitDeliverableService } from './unit-deliverable';
import { PartyDocumentService } from './party-document';
import { RuleHotItemAsListComponent, RuleHotItemService } from './rule-hot-item';
import { ProspectInfoComponent } from './prospect';
import { BranchService } from './branch';
import { CommunicationEventCDB, CommunicationEventCDBService } from './communication-event-cdb';
import { WorkTypeService } from './work-type';
import { ReligionTypeService } from './religion-type';
import { VehicleRegistrationService } from './vehicle-registration';

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule,
        TabsModule.forRoot(),
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule
        /* jhipster-needle-add-list-component - JHipster will add entity modules here */
    ],
    exports: [
        // shared component
        SurOrganizationViewComponent,
        FetchingDataComponent,
        VillageViewComponent,
        PartyDocumentAsListComponent,
        CityViewComponent,
        PersonViewComponent,
        OrganizationViewComponent,
        OrganizationViewNetComponent,
        OrganizationDetailViewComponent,
        CustomerViewComponent,
        ProvinceViewComponent,
        DistrictViewComponent,
        ProvinceViewNetComponent,
        CityViewNetComponent,
        DistrictViewNetComponent,
        VillageViewNetComponent,
        CommunicationEventProspectAsHistoryFollowUpComponent,
        CommunicationEventDeliveryAsListComponent,
        NumberOnlyDirective,
        PersonBaseViewComponent,
        // shared component
        RuleIndentAsListComponent,
        RequestItemAsListComponent,
        RequestItemInMovingSlipAsListComponent,
        StockOpnameItemAsListComponent,
        OrderItemAsListComponent,
        OrderItemUnitAsListComponent,
        ProductShipmentReceiptAsListComponent,
        UnitShipmentReceiptAsListComponent,
        ShipmentItemAsListComponent,
        ShipmentItemSpgAsListComponent,
        ShipmentReceiptAsListComponent,
        ReceiptAsListComponent,
        DisbursementAsListComponent,
        BillingItemAsListComponent,
        BillingItemInBillingDisbursementAsListComponent,
        PaymentApplicationAsListComponent,
        ContainerAsListComponent,
        FacilityAsListComponent,
        UomAsListComponent,
        UomConversionAsListComponent,
        FeatureAsListComponent,
        ProductCategoryAsListComponent,
        PartyCategoryAsListComponent,
        PaymentMethodAsListComponent,
        CommunicationEventProspectAsListComponent,
        CustomerRequestItemAsListComponent,
        RequestRequirementAsListComponent,
        PartyFacilityPurposeAsListComponent,
        RequirementPaymentAsListComponent,
        ProductPackageReceiptAsListComponent,
        OrderBillingItemAsListComponent,
        ShipmentBillingItemAsListComponent,
        OrderPaymentAsListComponent,
        RequirementPaymentAsListComponent,
        FacilityContactMechanismAsListComponent,
        GoodContainerAsListComponent,
        UnitDocumentMessageAsListComponent,
        UnitDocumentMessageByRequirementComponent,
        OrderShipmentItemAsListComponent,
        RequestUnitInternalAsListComponent,
        UnitPreparationAsListComponent,
        GeneralUploadAsListComponent,
        FeatureApplicableAsListComponent,
        RuleHotItemAsListComponent,
        ProspectInfoComponent,
        ReceiptAsListComponent,
    ],
    declarations: [
        // shared component
        SurOrganizationViewComponent,
        FetchingDataComponent,
        VillageViewComponent,
        PartyDocumentAsListComponent,
        CityViewComponent,
        PersonViewComponent,
        OrganizationViewComponent,
        OrganizationViewNetComponent,
        OrganizationDetailViewComponent,
        CustomerViewComponent,
        ProvinceViewComponent,
        DistrictViewComponent,
        ProvinceViewNetComponent,
        CityViewNetComponent,
        DistrictViewNetComponent,
        VillageViewNetComponent,
        CommunicationEventProspectAsHistoryFollowUpComponent,
        CommunicationEventDeliveryAsListComponent,
        PersonBaseViewComponent,
        NumberOnlyDirective,
        // shared component
        RuleIndentAsListComponent,
        RequestItemAsListComponent,
        RequestItemInMovingSlipAsListComponent,
        StockOpnameItemAsListComponent,
        OrderItemAsListComponent,
        OrderItemUnitAsListComponent,
        ProductShipmentReceiptAsListComponent,
        UnitShipmentReceiptAsListComponent,
        ShipmentItemAsListComponent,
        ShipmentItemSpgAsListComponent,
        ShipmentReceiptAsListComponent,
        ReceiptAsListComponent,
        DisbursementAsListComponent,
        BillingItemAsListComponent,
        BillingItemInBillingDisbursementAsListComponent,
        PaymentApplicationAsListComponent,
        ContainerAsListComponent,
        FacilityAsListComponent,
        UomAsListComponent,
        UomConversionAsListComponent,
        FeatureAsListComponent,
        ProductCategoryAsListComponent,
        PartyCategoryAsListComponent,
        PaymentMethodAsListComponent,
        CommunicationEventProspectAsListComponent,
        CustomerRequestItemAsListComponent,
        RequestRequirementAsListComponent,
        PartyFacilityPurposeAsListComponent,
        RequirementPaymentAsListComponent,
        ProductPackageReceiptAsListComponent,
        OrderBillingItemAsListComponent,
        ShipmentBillingItemAsListComponent,
        OrderPaymentAsListComponent,
        RequirementPaymentAsListComponent,
        FacilityContactMechanismAsListComponent,
        GoodContainerAsListComponent,
        UnitDocumentMessageAsListComponent,
        UnitDocumentMessageByRequirementComponent,
        OrderShipmentItemAsListComponent,
        RequestUnitInternalAsListComponent,
        UnitPreparationAsListComponent,
        GeneralUploadAsListComponent,
        FeatureApplicableAsListComponent,
        RuleHotItemAsListComponent,
        ProspectInfoComponent,
        ReceiptAsListComponent,
    ],
    entryComponents: [
        // shared component
        ProvinceViewComponent,
        DistrictViewComponent,
        CityViewComponent,
        VillageViewComponent,
        ProvinceViewNetComponent,
        DistrictViewNetComponent,
        CityViewNetComponent,
        VillageViewNetComponent,
        CommunicationEventProspectAsHistoryFollowUpComponent,
        CommunicationEventDeliveryAsListComponent,
        PersonBaseViewComponent,
        SurOrganizationViewComponent,
        // shared component
        PartyDocumentAsListComponent,
        RuleIndentAsListComponent,
        RequestItemAsListComponent,
        RequestItemInMovingSlipAsListComponent,
        StockOpnameItemAsListComponent,
        OrderItemAsListComponent,
        OrderItemUnitAsListComponent,
        ProductShipmentReceiptAsListComponent,
        UnitShipmentReceiptAsListComponent,
        ShipmentItemAsListComponent,
        ShipmentItemSpgAsListComponent,
        ShipmentReceiptAsListComponent,
        ReceiptAsListComponent,
        DisbursementAsListComponent,
        BillingItemAsListComponent,
        BillingItemInBillingDisbursementAsListComponent,
        PaymentApplicationAsListComponent,
        ContainerAsListComponent,
        FacilityAsListComponent,
        UomAsListComponent,
        UomConversionAsListComponent,
        FeatureAsListComponent,
        ProductCategoryAsListComponent,
        PartyCategoryAsListComponent,
        PaymentMethodAsListComponent,
        CommunicationEventProspectAsListComponent,
        CustomerRequestItemAsListComponent,
        RequestRequirementAsListComponent,
        PartyFacilityPurposeAsListComponent,
        RequirementPaymentAsListComponent,
        ProductPackageReceiptAsListComponent,
        OrderBillingItemAsListComponent,
        ShipmentBillingItemAsListComponent,
        OrderPaymentAsListComponent,
        RequirementPaymentAsListComponent,
        FacilityContactMechanismAsListComponent,
        GoodContainerAsListComponent,
        UnitDocumentMessageAsListComponent,
        UnitDocumentMessageByRequirementComponent,
        OrderShipmentItemAsListComponent,
        RequestUnitInternalAsListComponent,
        UnitPreparationAsListComponent,
        GeneralUploadAsListComponent,
        FeatureApplicableAsListComponent,
        RuleHotItemAsListComponent,
        ProspectInfoComponent,
        ReceiptAsListComponent,
    ],
    providers: [
        CustomerService,
        ProductService,
        PartyService,
        RoleTypeService,
        StatusTypeService,
        PersonService,
        OrganizationService,
        InternalService,
        NewPersonalCustomerService,
        GoodService,
        PersonalCustomerService,
        OrganizationCustomerService,
        BaseCalendarService,
        RequirementService,
        WorkEffortService,
        PackageReceiptService,
        VendorService,
        VendorTypeService,
        RemPartService,
        UnitDeliverableResolvePagingParams,
        FeatureApplicableService,
        RuleHotItemService,
        ReceiptService,
        PartyDocumentService,
        UnitDocumentMessageService,
        BranchService,
        CommunicationEventCDBService,
        WorkTypeService,
        ReligionTypeService,
        VehicleRegistrationService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSharedEntityModule {}
