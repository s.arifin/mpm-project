import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { PackageReceipt, PackageReceiptService} from '../package-receipt';
import { UnitShipmentReceipt, UnitShipmentReceiptService} from '../unit-shipment-receipt';

import { ToasterService, CommonUtilService} from '../../shared';

import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-shipment-incoming-unit-check',
    templateUrl: './shipment-incoming-unit-check.component.html'
})
export class ShipmentIncomingUnitCheckComponent implements OnInit, OnDestroy {

    selectedSLNumber: string;
    selectedshipmentPackageId: string;
    inputIdFrame: string;
    inputIdMachine: string;

    protected subscription: Subscription;
    protected subscriptionSL: Subscription;

    isSaving: boolean;
    unitChecks: UnitShipmentReceipt[];
    unitChecksCopy: UnitShipmentReceipt[];
    tanggal: any;
    after: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    index: number;
    isOpen: Boolean = false;

    constructor(
        protected alertService: JhiAlertService,
        protected packageReceiptService: PackageReceiptService,
        protected unitShipmentReceiptService: UnitShipmentReceiptService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtilService: CommonUtilService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idShipment';
        this.reverse = 'asc';
    }

    ngOnInit() {
        // this.subscription = this.route.params.subscribe((params) => {
        //     if (params['id']) {
        //         this.load(params['id']);
        //     }
        // });
        this.unitChecks = Array<UnitShipmentReceipt>();
        this.getChosenSL();
        this.isSaving = false;
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        this.clearForm();
        this.subscriptionSL.unsubscribe();
    }

    // load(id) {
    //     this.shipmentIncomingService.find(id).subscribe((shipmentIncoming) => {
    //         this.shipmentIncoming = shipmentIncoming;
    //     });
    // }

    previousState() {
        this.router.navigate(['shipment-incoming']);
    }

    open() {
        this.isOpen = false;
    }

    save() {
        this.isSaving = true;
        const objectBody: Object = {
            items: this.unitChecks
        };

        const objectHeader: Object = {
            execute: 'UnitCheckSubmit'
        };

        this.unitShipmentReceiptService.executeListProcess(objectBody, objectHeader).subscribe(
            (res) => {
                if (res) {
                    this.onSuccess();
                }
            },
            (err) => {
                this.commonUtilService.showError(err);
            }
        )
    }

    checkUnitOnSL(): void {
        const unitCheck = new UnitShipmentReceipt();
        unitCheck.shipmentPackageId = this.selectedshipmentPackageId;
        if (this.inputIdFrame !== '' &&
            this.inputIdFrame !== undefined &&
            this.inputIdFrame != null) {
            unitCheck.idFrame = this.inputIdFrame;
        }
        if (this.inputIdMachine !== '' &&
            this.inputIdMachine !== undefined &&
            this.inputIdMachine != null) {
            unitCheck.idMachine = this.inputIdMachine;
        }
        const objectBody: Object = {
            item: unitCheck
        };

        const objectHeader: Object = {
            execute: 'CheckUnitOnShippingList'
        };

        this.unitShipmentReceiptService.executeProcess(objectBody, objectHeader).subscribe(
            (res) => {
                if (res) {
                    this.inputIdFrame = res.idFrame;
                    this.inputIdMachine = res.idMachine;
                }
            },
            (err) => {
                this.commonUtilService.showError(err);
            }
        )
    }

    clearDataInput(): void {
        this.inputIdFrame = null;
        this.inputIdMachine = null;
    }

    protected clearForm(): void {
        this.clearDataInput();
        this.selectedSLNumber = null;
        this.selectedshipmentPackageId = null;
        this.index = null;
    }

    addData(): void {
        const data = new UnitShipmentReceipt();
        data.shipmentPackageId = this.selectedshipmentPackageId;
        data.idFrame = this.inputIdFrame;
        data.idMachine = this.inputIdMachine;
        this.unitChecks = [...this.unitChecks, data];
        this.clearDataInput();
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }

    }

    addDataBtnDisable(): boolean {
        if ((this.inputIdFrame !== '' &&
            this.inputIdFrame !== undefined &&
            this.inputIdFrame != null) &&
            (this.inputIdMachine !== '' &&
            this.inputIdMachine !== undefined &&
            this.inputIdMachine != null)) {

            return false;
        } else { return true; }
    }

    removeData(index: number): void {
        this.unitChecksCopy = Object.assign([], this.unitChecks);
        this.unitChecksCopy.splice(index, 1);
        this.unitChecks = new Array<UnitShipmentReceipt>();
        this.unitChecks = this.unitChecksCopy;
        if (this.unitChecks.length > 0) {
            this.isSaving = true;
        } else {
            this.isSaving = false;
        }
    }

    // protected subscribeToSaveResponse(result: Observable<ShipmentIncoming>) {
    //     result.subscribe((res) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json));
    // }

    // protected onSaveSuccess(result: ShipmentIncoming) {
    //     console.log('WAKWAU', result);
    //     this.eventManager.broadcast({ name: 'shipmentIncomingListModification', content: 'OK'});
    //     this.toaster.showToaster('info', 'Save', 'shipmentIncoming saved !');
    //     this.isSaving = false;
    //     this.previousState();
    // }

    protected onSuccess(): void {
        this.eventManager.broadcast({ name: 'shipmentIncomingListModification', content: 'OK'});
        // this.loadingService.loadingStop();
        this.isSaving = false;
        this.previousState();
    }

    // protected onSaveError(error) {
    //     try {
    //         error.json();
    //     } catch (exception) {
    //         error.message = error.text();
    //     }
    //     this.isSaving = false;
    //     this.onError(error);
    // }

    // protected onError(error) {
    //     this.toaster.showToaster('warning', 'shipmentIncoming Changed', error.message);
    //     this.alertService.error(error.message, null, null);
    // }

    protected getChosenSL(): void {
        this.subscriptionSL = this.packageReceiptService.values.subscribe(
            (res) => {
                if (res !== undefined) {
                    this.selectedSLNumber = res.documentNumber;
                    this.selectedshipmentPackageId = res.idPackage;
                    this.isOpen = true;
                    // this.dataMovingSlipCustomSources = this.convertInventoryItemToMovingSlip(res);
                    // this.loadSelectedInventoryItems();
                }
            }
        )
    }
}
