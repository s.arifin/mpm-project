import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ShipmentIncomingComponent } from './shipment-incoming.component';
import { ShipmentIncomingEditComponent } from './shipment-incoming-edit.component';
import { ShipmentIncomingDetailComponent } from './shipment-incoming-detail.component';
import { ShipmentIncomingUnitCheckComponent } from './shipment-incoming-unit-check.component';
import { ShipmentIncomingPopupComponent } from './shipment-incoming-dialog.component';
import { PackageReceiptLovPopupComponent } from '../package-receipt/package-receipt-as-lov.component';

@Injectable()
export class ShipmentIncomingResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const shipmentIncomingRoute: Routes = [
    {
        path: 'shipment-incoming',
        component: ShipmentIncomingComponent,
        resolve: {
            'pagingParams': ShipmentIncomingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shipmentIncomingPopupRoute: Routes = [
    {
        path: 'shipment-incoming-new',
        component: ShipmentIncomingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-incoming/:id/edit',
        component: ShipmentIncomingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-incoming/:id/detail',
        component: ShipmentIncomingDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-incoming-unit-check',
        component: ShipmentIncomingUnitCheckComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'shipment-incoming-popup-new',
        component: ShipmentIncomingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-incoming/:id/popup-edit',
        component: ShipmentIncomingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentIncoming.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-incoming-unit-check-lov/:idStatusType',
        component: PackageReceiptLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
