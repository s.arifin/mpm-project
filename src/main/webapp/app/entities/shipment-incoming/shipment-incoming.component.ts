import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ShipmentIncoming } from './shipment-incoming.model';
import { ShipmentIncomingService } from './shipment-incoming.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService, MenuItem } from 'primeng/primeng';

@Component({
    selector: 'jhi-shipment-incoming',
    templateUrl: './shipment-incoming.component.html'
})
export class ShipmentIncomingComponent implements OnInit, OnDestroy {

    currentAccount: any;
    shipmentIncomings: ShipmentIncoming[];
    selected: ShipmentIncoming;
    items: MenuItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    shipmentId: any;
    first: any;
    currentInternal: string;

    constructor(
        protected shipmentIncomingService: ShipmentIncomingService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.currentInternal = principal.getIdInternal();
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        // this.itemsPerPage = ITEMS_PER_PAGE;
        // this.routeData = this.activatedRoute.data.subscribe((data) => {
        //     this.page = data['pagingParams'].page;
        //     this.previousPage = data['pagingParams'].page;
        //     this.reverse = data['pagingParams'].ascending;
        //     this.predicate = data['pagingParams'].predicate;
        });
        // this.itemsPerPage = 20;
        // this.page = 1;
        // this.predicate = 'dateSchedulle';
        // this.reverse = (this.predicate === 'dateSchedulle') ? !this.predicate : false;
        // this.first = '1990-1-1';
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            console.log('ini search masuk=');
            this.shipmentIncomingService.search({
                // idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                console.log('ini search masuk ke 2=', this.principal.getIdInternal());
            return;
        }
        this.shipmentIncomingService.queryFilterBy({
            idInternal : this.currentInternal,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/shipment-incoming'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shipment-incoming', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/shipment-incoming', {
            // idInternal: this.principal.getIdInternal(),
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.selected = new ShipmentIncoming();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInShipmentIncomings();
        this.items = [
            {label: 'Detail', routerLink: ['/shipment-incoming-detail/' + this.shipmentId]},
            {label: 'Aksesoris', routerLink: ['/pagename']}
        ]
    }

    ShipmentDetail(id) {
        this.shipmentId = id;
        this.items = [
            {label: 'Detail', routerLink: ['/shipment-incoming-detail/' + this.shipmentId]},
            {label: 'Aksesoris', routerLink: ['/pagename']}
        ]
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ShipmentIncoming) {
        return item.idShipment;
    }

    registerChangeInShipmentIncomings() {
        this.eventSubscriber = this.eventManager.subscribe('shipmentIncomingListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idShipment') {
            result.push('idShipment');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.shipmentIncomings = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.shipmentIncomingService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.shipmentIncomingService.update(event.data)
                .subscribe((res: ShipmentIncoming) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.shipmentIncomingService.create(event.data)
                .subscribe((res: ShipmentIncoming) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: ShipmentIncoming) {
        this.toasterService.showToaster('info', 'ShipmentIncoming Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.shipmentIncomingService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'shipmentIncomingListModification',
                    content: 'Deleted an shipmentIncoming'
                    });
                });
            }
        });
    }

    // public checkValue(): void {
    //     this.router.navigate(['../shipment-incoming-unit-check']);
    // }

    // public checkDisable(): Boolean {
    //     if (this.selected === null || this.selected === undefined || this.selected.currentStatus !== 10) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    public getCurrentStatusDesc(data): string {
        let result = '';

        switch (data) {
            case 10:
                result = 'DRAFT';
                break;
            case 11:
                result = 'OPEN';
                break;
            case 17:
                result = 'COMPLETED';
                break;
            default:
                result = 'NONE'
          }
        return result;
    }

    buildReindex() {
        this.shipmentIncomingService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
