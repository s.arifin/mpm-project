import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShipmentIncoming } from './shipment-incoming.model';
import { ShipmentIncomingService } from './shipment-incoming.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-incoming-edit',
    templateUrl: './shipment-incoming-edit.component.html'
})
export class ShipmentIncomingEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    shipmentIncoming: ShipmentIncoming;
    isSaving: boolean;

    constructor(
        protected alertService: JhiAlertService,
        protected shipmentIncomingService: ShipmentIncomingService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.shipmentIncoming = new ShipmentIncoming();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.shipmentIncomingService.find(id).subscribe((shipmentIncoming) => {
            this.shipmentIncoming = shipmentIncoming;
        });
    }

    previousState() {
        this.router.navigate(['shipment-incoming']);
    }

    save() {
        this.isSaving = true;
        if (this.shipmentIncoming.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentIncomingService.update(this.shipmentIncoming));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentIncomingService.create(this.shipmentIncoming));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentIncoming>) {
        result.subscribe((res: ShipmentIncoming) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ShipmentIncoming) {
        this.eventManager.broadcast({ name: 'shipmentIncomingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentIncoming saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'shipmentIncoming Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}
