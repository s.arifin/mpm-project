import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    ShipmentIncomingService,
    ShipmentIncomingPopupService,
    ShipmentIncomingComponent,
    ShipmentIncomingDialogComponent,
    ShipmentIncomingPopupComponent,
    shipmentIncomingRoute,
    shipmentIncomingPopupRoute,
    ShipmentIncomingResolvePagingParams,
    ShipmentIncomingEditComponent,
    ShipmentIncomingDetailComponent,
    ShipmentIncomingUnitCheckComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule,
         MenuModule,
         TooltipModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';
import { MpmPackageReceiptModule } from '../package-receipt/package-receipt.module';

const ENTITY_STATES = [
    ...shipmentIncomingRoute,
    ...shipmentIncomingPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        TooltipModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        MenuModule,
        MpmPackageReceiptModule
    ],
    exports: [
        ShipmentIncomingComponent,
        ShipmentIncomingEditComponent,
        ShipmentIncomingDetailComponent,
        ShipmentIncomingUnitCheckComponent,
    ],
    declarations: [
        ShipmentIncomingComponent,
        ShipmentIncomingDialogComponent,
        ShipmentIncomingPopupComponent,
        ShipmentIncomingEditComponent,
        ShipmentIncomingDetailComponent,
        ShipmentIncomingUnitCheckComponent,
    ],
    entryComponents: [
        ShipmentIncomingComponent,
        ShipmentIncomingDialogComponent,
        ShipmentIncomingPopupComponent,
        ShipmentIncomingEditComponent,
        ShipmentIncomingDetailComponent,
        ShipmentIncomingUnitCheckComponent,
    ],
    providers: [
        ShipmentIncomingService,
        ShipmentIncomingPopupService,
        ShipmentIncomingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmShipmentIncomingModule {}
