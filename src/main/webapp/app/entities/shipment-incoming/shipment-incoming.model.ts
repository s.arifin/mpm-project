import { Shipment } from '../shipment/shipment.model';

export class ShipmentIncoming extends Shipment {
    constructor() {
        super();
    }
}

export class CustomShipmentIncoming {
    constructor(
        public idPackage?: String,
        public shippingListNumber?: String,
        public shippingListDate?: Date,
        public idShipment?: String,
        public spgNumber?: String,
        public spgDate?: Date,
        public mainDealerName?: String,
        public spgLocation?: String,
        public qty?: Number
    ) {
    }
}
