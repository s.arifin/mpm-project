import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ShipmentIncoming, CustomShipmentIncoming } from './shipment-incoming.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ShipmentIncomingService {
    protected itemValues: ShipmentIncoming[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/shipment-incomings';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-incomings';

    constructor(protected http: Http) { }

    create(shipmentIncoming: ShipmentIncoming): Observable<ShipmentIncoming> {
        const copy = this.convert(shipmentIncoming);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(shipmentIncoming: ShipmentIncoming): Observable<ShipmentIncoming> {
        const copy = this.convert(shipmentIncoming);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ShipmentIncoming> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findCustom(id: any): Observable<CustomShipmentIncoming> {
        return this.http.get(`${this.resourceUrl}-spg/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(shipmentIncoming: ShipmentIncoming, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(shipmentIncoming);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, shipmentIncoming: ShipmentIncoming): Observable<ShipmentIncoming> {
        const copy = this.convert(shipmentIncoming);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, shipmentIncomings: ShipmentIncoming[]): Observable<ShipmentIncoming[]> {
        const copy = this.convertList(shipmentIncomings);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(shipmentIncoming: ShipmentIncoming): ShipmentIncoming {
        if (shipmentIncoming === null || shipmentIncoming === {}) {
            return {};
        }
        // const copy: ShipmentIncoming = Object.assign({}, shipmentIncoming);
        const copy: ShipmentIncoming = JSON.parse(JSON.stringify(shipmentIncoming));
        return copy;
    }

    protected convertList(shipmentIncomings: ShipmentIncoming[]): ShipmentIncoming[] {
        const copy: ShipmentIncoming[] = shipmentIncomings;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentIncoming[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
