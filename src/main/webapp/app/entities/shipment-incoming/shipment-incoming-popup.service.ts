import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ShipmentIncoming } from './shipment-incoming.model';
import { ShipmentIncomingService } from './shipment-incoming.service';

@Injectable()
export class ShipmentIncomingPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected shipmentIncomingService: ShipmentIncomingService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.shipmentIncomingService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.shipmentIncomingModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new ShipmentIncoming();
                    this.ngbModalRef = this.shipmentIncomingModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    shipmentIncomingModalRef(component: Component, shipmentIncoming: ShipmentIncoming): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.shipmentIncoming = shipmentIncoming;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.shipmentIncomingLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    shipmentIncomingLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
