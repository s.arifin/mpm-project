import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ShipmentIncoming} from './shipment-incoming.model';
import {ShipmentIncomingPopupService} from './shipment-incoming-popup.service';
import {ShipmentIncomingService} from './shipment-incoming.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-incoming-dialog',
    templateUrl: './shipment-incoming-dialog.component.html'
})
export class ShipmentIncomingDialogComponent implements OnInit {

    shipmentIncoming: ShipmentIncoming;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected shipmentIncomingService: ShipmentIncomingService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentIncoming.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentIncomingService.update(this.shipmentIncoming));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentIncomingService.create(this.shipmentIncoming));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentIncoming>) {
        result.subscribe((res: ShipmentIncoming) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentIncoming) {
        this.eventManager.broadcast({ name: 'shipmentIncomingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentIncoming saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentIncoming Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-shipment-incoming-popup',
    template: ''
})
export class ShipmentIncomingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentIncomingPopupService: ShipmentIncomingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentIncomingPopupService
                    .open(ShipmentIncomingDialogComponent as Component, params['id']);
            } else {
                this.shipmentIncomingPopupService
                    .open(ShipmentIncomingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
