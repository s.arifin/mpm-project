import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShipmentIncoming, CustomShipmentIncoming } from './shipment-incoming.model';
import { ShipmentIncomingService } from './shipment-incoming.service';
import { ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-incoming-detail',
    templateUrl: './shipment-incoming-detail.component.html'
})
export class ShipmentIncomingDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    idShipment: any;
    customShipmentIncoming: CustomShipmentIncoming;

    constructor(
        protected alertService: JhiAlertService,
        protected shipmentIncomingService: ShipmentIncomingService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.customShipmentIncoming = new CustomShipmentIncoming();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idShipment = params['id'];
                this.load(params['id']);
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.shipmentIncomingService.findCustom(id).subscribe((customShipmentIncoming) => {
            this.customShipmentIncoming = customShipmentIncoming;
        });
    }

    previousState() {
        this.router.navigate(['shipment-incoming']);
    }
}
