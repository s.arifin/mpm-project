import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { UnitDocumentMessageService, UnitDocumentMessage } from '.';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-unit-document-message-by-requirement',
    templateUrl: './unit-document-message-by-requirement.component.html'
})

export class UnitDocumentMessageByRequirementComponent implements OnChanges {
    @Input()
    protected idSalesUnitRequirement: string;

    public UnitDocumentMessages: Array<UnitDocumentMessage>;
    constructor(
        protected unitDocumentMessageService: UnitDocumentMessageService
    ) {

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idSalesUnitRequirement']) {
            if (this.idSalesUnitRequirement !== null) {
                this.unitDocumentMessageService.getDataByIdRequirement(this.idSalesUnitRequirement).subscribe(
                    (res) => {
                        this.UnitDocumentMessages = res.json;
                        console.log('a', this.UnitDocumentMessages);
                    }
                )
            }
        }
    }

    public getStatusName(status: number): String {
        let a: string;
        a = '';

        if (status === BaseConstant.Status.STATUS_WAITING_FOR_APPROVAL) {
            a = 'Waiting for approval';
        } else if (status === BaseConstant.Status.STATUS_APPROVED) {
            a = 'Approved';
        } else {
            a = 'Reject';
        }

        return a;
    }
}
