import { BaseEntity } from './../../shared';

export class UnitDocumentMessage implements BaseEntity {
    constructor(
        public idtemp?: any,
        public id?: any,
        public idMessage?: any,
        public idRequirement?: string,
        public idProspect?: string,
        public content?: string,
        public dateCreated?: any,
        public usernameFrom?: string,
        public usernameTo?: string,
        public unitDocumentMessageId?: any,
        public currentApprovalStatus?: number,
        public DtSalesNote?: any,
        public VSONo?: number,
        public MarketName?: string,
        public IdProduct?: string,
        public Color?: string,
        public Qty?: any,
        public SalesName?: string,
        public Korsal?: string,
        public Status?: string,
        public IdStatus?: any,
        public idFeature?: any,
        public cabang1?: any,
        public qtyCabang1?: any,
        public cabang2?: any,
        public qtyCabang2?: any,
        public cabang3?: any,
        public qtyCabang3?: any,
        public idinternalsrc?: any,
        public createdby?: any,
        public TotalData?: any,
        public reqnotnumber?: any,
        public dtcreated?: any,
        public QtyReq?: any,
        public QtyApproved?: any,
        public idreqnot?: any,
        public cabang?: any,
        public color?: string,
        public idproduct?: string,
        public yearAssmbly?: any,
        public totalQty?: any,
        public pemohon?: any,
        public idstatustype?: any,
        public IdReqnotType?: any,
        public DateDelivery?: any,
        public dtFrom?: any,
        public dtThru?: any,
        public eventName?: any,
        public QtyAvailable?: any
    ) {
    }
}

export class CustomUnitDocumentMessage implements BaseEntity {
    constructor(
        public id?: any,
        public idMessage?: number,
        public content?: string,
        public dateCreated?: Date,
        public idRequirement?: string,
        public requirementNumber?: string,
        public productName?: string,
        public idProduct?: string,
        public description?: string,
        public idFeature?: string
    ) {

    }
}

export class CustomTemp implements BaseEntity {
    static push: any;
    constructor(
        public id?: any,
        public idtemp?: any,
    ) {

    }
}

export class CustomUnitModel implements BaseEntity {
    static push: any;
    constructor(
        public id?: any,
        public idMessage?: any,
        public idRequirement?: any,
        public idProduct?: any,
        public idFeature?: any,
        public cabang1?: any,
        public qtyCabang1?: any,
        public Request1?: any,
        public cabang2?: any,
        public qtyCabang2?: any,
        public Request2?: any,
        public cabang3?: any,
        public qtyCabang3?: any,
        public Request3?: any,
        public Color?: string,
        public yearAssmbly?: number,
    ) {

    }
}
