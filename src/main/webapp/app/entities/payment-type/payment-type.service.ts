import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PaymentType } from './payment-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentTypeService {
   protected itemValues: PaymentType[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/payment-types';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/payment-types';

   constructor(protected http: Http) { }

   create(paymentType: PaymentType): Observable<PaymentType> {
       const copy = this.convert(paymentType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(paymentType: PaymentType): Observable<PaymentType> {
       const copy = this.convert(paymentType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PaymentType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PaymentType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PaymentType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PaymentType.
    */
   protected convertItemFromServer(json: any): PaymentType {
       const entity: PaymentType = Object.assign(new PaymentType(), json);
       return entity;
   }

   /**
    * Convert a PaymentType to a JSON which can be sent to the server.
    */
   protected convert(paymentType: PaymentType): PaymentType {
       if (paymentType === null || paymentType === {}) {
           return {};
       }
       // const copy: PaymentType = Object.assign({}, paymentType);
       const copy: PaymentType = JSON.parse(JSON.stringify(paymentType));
       return copy;
   }

   protected convertList(paymentTypes: PaymentType[]): PaymentType[] {
       const copy: PaymentType[] = paymentTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PaymentType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
