import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentType } from './payment-type.model';
import { PaymentTypePopupService } from './payment-type-popup.service';
import { PaymentTypeService } from './payment-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-payment-type-dialog',
    templateUrl: './payment-type-dialog.component.html'
})
export class PaymentTypeDialogComponent implements OnInit {

    paymentType: PaymentType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected paymentTypeService: PaymentTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentType.idPaymentType !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentTypeService.update(this.paymentType));
        } else {
            this.subscribeToSaveResponse(
                this.paymentTypeService.create(this.paymentType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PaymentType>) {
        result.subscribe((res: PaymentType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PaymentType) {
        this.eventManager.broadcast({ name: 'paymentTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'paymentType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-payment-type-popup',
    template: ''
})
export class PaymentTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected paymentTypePopupService: PaymentTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentTypePopupService
                    .open(PaymentTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.paymentTypePopupService.parent = params['parent'];
                this.paymentTypePopupService
                    .open(PaymentTypeDialogComponent as Component);
            } else {
                this.paymentTypePopupService
                    .open(PaymentTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
