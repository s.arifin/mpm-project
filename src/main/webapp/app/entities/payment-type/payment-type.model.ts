import { BaseEntity } from './../../shared';

export class PaymentType implements BaseEntity {
    constructor(
        public id?: number,
        public idPaymentType?: number,
        public description?: string,
    ) {
    }
}
