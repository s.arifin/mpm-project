import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    PartyRelationshipService,
} from './';

@NgModule({
    imports: [
        MpmSharedModule,
    ],
    exports: [
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        PartyRelationshipService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPartyRelationshipModule {}
