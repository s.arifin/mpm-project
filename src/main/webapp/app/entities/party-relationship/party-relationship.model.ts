import { BaseEntity } from './../../shared';

export class PartyRelationship implements BaseEntity {
    constructor(
        public id?: any,
        public idPartyRelationship?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public statusTypeId?: any,
        public relationTypeId?: any,
        public roleFromId?: any,
        public roleToId?: any,
    ) {
    }
}
