import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BillingItem } from './billing-item.model';
import { BillingItemPopupService } from './billing-item-popup.service';
import { BillingItemService } from './billing-item.service';
import { ToasterService } from '../../shared';
import { Billing, BillingService } from '../billing';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { ResponseWrapper } from '../../shared';
import { CommonUtilService } from '../../shared';
import { Feature} from '../feature';
import { Motor, MotorService } from '../motor';
import { Product, ProductService } from '../product';

import * as _ from 'lodash';

@Component({
    selector: 'jhi-billing-item-dialog',
    templateUrl: './billing-item-dialog.component.html'
})
export class BillingItemDialogComponent implements OnInit {

    billingItem: BillingItem;
    isSaving: boolean;
    idBilling: any;
    idInventoryItem: any;
    itemType: number;
    billings: Billing[];

    inventoryitems: InventoryItem[];

    public motors: Motor[];
    public selectedMotor: any;
    public features: Feature[];
    public listMotors = [{label: 'Please Select', value: null}];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected billingItemService: BillingItemService,
        protected billingService: BillingService,
        protected inventoryItemService: InventoryItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected motorService: MotorService,
        protected productService: ProductService,
        protected commontUtilService: CommonUtilService,
    ) {
        this.itemType = 101;
    }

    protected resetMe() {
        this.selectedMotor = null;
    }

    ngOnInit() {
        this.isSaving = false;
        this.billingService.query()
            .subscribe((res: ResponseWrapper) => { this.billings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.inventoryItemService.query()
            .subscribe((res: ResponseWrapper) => { this.inventoryitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.motors = res.json;
            this.motors.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.idProduct });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            });
    }

    public selectMotor(isSelect?: boolean): void {
        this.billingItem.idProduct = this.selectedMotor;

        this.productService.find(this.selectedMotor)
        .subscribe(
            (res) => {
                this.billingItem.itemDescription = res.description;
            }
        )
        this.getColorByMotor(this.billingItem.idProduct);
    }

    // protected getDescriptionByMotor(idProduct: string): void {
    //     if (idProduct !== null) {
    //         this.motorss = new Array<Feature>();
    //         const selectedProduct: Motor = _.find(this.motors, function(e) {
    //             return e.idProduct.toLowerCase() === idProduct.toLowerCase();
    //         });
    //         this.motorss = selectedProduct.description;
    //     }
    // }
    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.billingItem.idItemType = this.itemType;
        this.isSaving = true;
        if (this.billingItem.idBillingItem !== undefined) {
            this.subscribeToSaveResponse(
                this.billingItemService.update(this.billingItem));
        } else {
            this.subscribeToSaveResponse(
                this.billingItemService.create(this.billingItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingItem>) {
        result.subscribe((res: BillingItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: BillingItem) {
        this.eventManager.broadcast({ name: 'billingItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'billingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBillingById(index: number, item: Billing) {
        return item.idBilling;
    }

    trackInventoryItemById(index: number, item: InventoryItem) {
        return item.idInventoryItem;
    }
    hitungHargaInputManual() {
        console.log('masuk ngga neehh');
        this.billingItem.taxAmount = ((this.billingItem.unitPrice - this.billingItem.discount) * 0.1);
        this.billingItem.totalAmount = ((this.billingItem.unitPrice - this.billingItem.discount + this.billingItem.taxAmount) * this.billingItem.qty)
    }
}

@Component({
    selector: 'jhi-billing-item-popup',
    template: ''
})
export class BillingItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected billingItemPopupService: BillingItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.billingItemPopupService
                    .open(BillingItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.billingItemPopupService.idBilling = params['parent'];
                this.billingItemPopupService
                    .open(BillingItemDialogComponent as Component);
            } else {
                this.billingItemPopupService
                    .open(BillingItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
