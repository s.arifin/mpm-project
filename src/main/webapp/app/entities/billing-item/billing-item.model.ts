import { BaseEntity } from './../../shared';

export class BillingItem implements BaseEntity {
    constructor(
        public id?: any,
        public idBillingItem?: any,
        public idItemType?: number,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescriptionription?: number,
        public idProduct?: string,
        public itemDescription?: string,
        public qty?: number,
        public unitPrice?: number,
        public basePrice?: number,
        public discount?: number,
        public taxAmount?: number,
        public totalAmount?: number,
        public billingId?: any,
        public inventoryItemId?: any,
        public idInternal?: any,
        public sequence?: any,
    ) {
    }
}
