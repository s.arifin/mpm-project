import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { BillingItem } from './billing-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BillingItemService {
    protected itemValues: BillingItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/billing-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/billing-items';

    constructor(protected http: Http) { }

    create(billingItem: BillingItem): Observable<BillingItem> {
        const copy = this.convert(billingItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(billingItem: BillingItem): Observable<BillingItem> {
        const copy = this.convert(billingItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<BillingItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    findBillingForTaxList(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/req-faktur-pajak', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<BillingItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<BillingItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to BillingItem.
     */
    protected convertItemFromServer(json: any): BillingItem {
        const entity: BillingItem = Object.assign(new BillingItem(), json);
        return entity;
    }

    /**
     * Convert a BillingItem to a JSON which can be sent to the server.
     */
    protected convert(billingItem: BillingItem): BillingItem {
        if (billingItem === null || billingItem === {}) {
            return {};
        }
        // const copy: BillingItem = Object.assign({}, billingItem);
        const copy: BillingItem = JSON.parse(JSON.stringify(billingItem));
        return copy;
    }

    protected convertList(billingItems: BillingItem[]): BillingItem[] {
        const copy: BillingItem[] = billingItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: BillingItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

}
