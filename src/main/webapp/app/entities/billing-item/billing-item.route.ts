import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BillingItemComponent } from './billing-item.component';
import { BillingItemPopupComponent } from './billing-item-dialog.component';
import { BillingItemPopupnewComponent } from './billing-item-dialog-new.component';

@Injectable()
export class BillingItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idBillingItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const billingItemRoute: Routes = [
    {
        path: 'billing-item',
        component: BillingItemComponent,
        resolve: {
            'pagingParams': BillingItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const billingItemPopupRoute: Routes = [
    {
        path: 'billing-item-popup-new-list/:parent',
        component: BillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'billing-item-new',
        component: BillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'billing-item/:id/edit',
        component: BillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'billing-item-popup-new-list-new/:parent',
        component: BillingItemPopupnewComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
