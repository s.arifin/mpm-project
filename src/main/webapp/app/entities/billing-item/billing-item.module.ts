import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    BillingItemService,
    BillingItemPopupService,
    BillingItemComponent,
    BillingItemDialogComponent,
    BillingItemPopupComponent,
    billingItemRoute,
    billingItemPopupRoute,
    BillingItemResolvePagingParams,
    BillingItemPopupnewComponent,
    BillingItemInBillingDisbursementAsListComponent

} from './';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';
import { BillingItemDialognewComponent } from './billing-item-dialog-new.component';
import { BillingDisbursementService } from '../billing-disbursement/billing-disbursement.service';

const ENTITY_STATES = [
    ...billingItemRoute,
    ...billingItemPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        BillingItemComponent,
        BillingItemDialognewComponent,
    ],
    declarations: [
        BillingItemComponent,
        BillingItemDialogComponent,
        BillingItemPopupComponent,
        BillingItemDialognewComponent,
        BillingItemPopupnewComponent,
    ],
    entryComponents: [
        BillingItemComponent,
        BillingItemDialogComponent,
        BillingItemPopupComponent,
        BillingItemPopupnewComponent,
        BillingItemDialognewComponent,

    ],
    providers: [
        BillingDisbursementService,
        BillingItemService,
        BillingItemPopupService,
        BillingItemResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmBillingItemModule {}
