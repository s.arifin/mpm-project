import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BillingItem } from './billing-item.model';
import { BillingItemService } from './billing-item.service';

@Injectable()
export class BillingItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idBilling: any;
    idInventoryItem: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected billingItemService: BillingItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.billingItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.billingItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new BillingItem();
                    data.billingId = this.idBilling;
                    data.inventoryItemId = this.idInventoryItem;
                    this.ngbModalRef = this.billingItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    billingItemModalRef(component: Component, billingItem: BillingItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.billingItem = billingItem;
        modalRef.componentInstance.idBilling = this.idBilling;
        modalRef.componentInstance.idInventoryItem = this.idInventoryItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.billingItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    billingItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
