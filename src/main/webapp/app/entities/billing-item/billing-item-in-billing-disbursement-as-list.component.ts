import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { BillingItem } from './billing-item.model';
import { BillingItemService } from './billing-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { Billing } from '../shared-component';
import { BillingDialogComponent } from '../billing/billing-dialog.component';
import { BillingDisbursement } from '../billing-disbursement';
import { BillingDisbursementService } from '../billing-disbursement';
import { BillingDisbursementCService } from '../billing-disbursement-c';
import { LoadingService } from '../../layouts/loading/loading.service';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { BillingItemPopupService } from './billing-item-popup.service';
import { BillingService } from '../billing';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { CommonUtilService } from '../../shared';
import { Feature} from '../feature';
import { Motor, MotorService } from '../motor';
import { Product, ProductService } from '../product';
import * as _ from 'lodash';
import { InternalService } from '../internal';

@Component({
    selector: 'jhi-billing-item-in-billing-disbursement-as-list',
    templateUrl: './billing-item-in-billing-disbursement-as-list.component.html'
})
export class BillingItemInBillingDisbursementAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idBilling: any;
    @Input() idInventoryItem: any;
    @Input() idBillingItem: any;
    billingItem: BillingItem;
    isSaving: boolean;
    itemType: number;
    billings: Billing[];
    unitPrice: number;
    discount: number;

    inventoryitems: InventoryItem[];
    currentAccount: any;
    billingItems: BillingItem[];
    billingDisbursement: BillingDisbursement;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    newModal: boolean;
    isPPN: Boolean = false;
    bankMapping: any;

    public motors: Motor[];
    public selectedMotor: any;
    public features: Feature[];
    public listMotors = [{label: 'Please Select', value: null}];

    constructor(
        protected billingDisbursementService: BillingDisbursementService,
        protected billingItemService: BillingItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected loadingService: LoadingService,
        protected toasterService: ToasterService,

        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected billingService: BillingService,
        protected inventoryItemService: InventoryItemService,
        protected toaster: ToasterService,
        protected motorService: MotorService,
        protected productService: ProductService,
        protected commontUtilService: CommonUtilService,
        protected internalService: InternalService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idBillingItem';
        this.reverse = 'asc';
        this.billingDisbursement = new BillingDisbursement();
        // this.billingDisbursement.currentStatus = 0;
        this.itemType = 101;
        this.unitPrice = 0;
        this.discount = 0;
        this.newModal = false;
        this.features = new Array<Feature>();
        this.billingItem = new BillingItem();
    }
    protected resetMe() {
        this.selectedMotor = null;
    }

    loadAll() {
        this.loadBankMapping();
        this.billingItem = new BillingItem();
        this.loadingService.loadingStart();
        this.billingItemService.queryFilterBy({
            idBilling: this.idBilling,
            idBillingItem: this.idBillingItem,
            idInventoryItem: this.idInventoryItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {
                this.onSuccess(res.json, res.headers),
                this.billingDisbursementService.find(this.idBilling).subscribe((billingDisbursement) => {
                    this.billingDisbursement = billingDisbursement;
                    }
                );
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadBankMapping() {
        this.loadingService.loadingStart();
        this.internalService.queryFilterBy({
            bankMapping: 'true',
            page: this.page - 1,
            size: this.itemsPerPage}).subscribe(
            (res: ResponseWrapper) => {
                this.bankMapping = res.json;
                if (this.bankMapping.length !== 0) {
                    this.isPPN = true;
                }
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }
    clear() {
        this.page = 0;
        this.router.navigate(['/billing-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.activeModal.dismiss('cancel');
        this.loadAll();
    }
    ngOnInit() {
        this.registerChangeInBillingItems();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.isSaving = false;
        this.billingService.query()
            .subscribe((res: ResponseWrapper) => { this.billings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.inventoryItemService.query()
            .subscribe((res: ResponseWrapper) => { this.inventoryitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.motors = res.json;
            this.motors.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.idProduct });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            });
    }

    public selectMotor(isSelect?: boolean): void {
        this.billingItem.idProduct = this.selectedMotor;

        this.productService.find(this.selectedMotor)
        .subscribe(
            (res) => {
                this.billingItem.itemDescription = res.description;
            }
        )
        this.getColorByMotor(this.billingItem.idProduct);
    }

    addNewData() {
        this.billingItem = new BillingItem();
        this.newModal = true;
        this.selectedMotor = null;
        this.unitPrice = 0;
        this.discount = 0;

    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idBilling']) {
            this.loadAll();
        }
        if (changes['idBillingItem']) {
            this.loadAll();
        }
        if (changes['idInventoryItem']) {
            this.loadAll();
        }
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: BillingItem) {
        return item.idBillingItem;
    }
    // protected registerChangeInBillingItems(): void {
    //     this.eventSubscriber = this.billingItemService.values.subscribe(
    //         (res) => {
    //             this.billingItems = res;
    //         }
    //     )
    // }
    registerChangeInBillingItems() {
        this.eventSubscriber = this.eventManager.subscribe('billingItemListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idBillingItem') {
            result.push('idBillingItem');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.billingItems = data;
    }

    protected onError(error: any) {
        this.loadingService.loadingStop();
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
        this.toaster.showToaster('warning', 'billingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.billingItemService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.billingItemService.update(event.data)
                .subscribe((res: BillingItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.billingItemService.create(event.data)
                .subscribe((res: BillingItem) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: BillingItem) {
        this.toasterService.showToaster('info', 'BillingItem Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    save() {
        this.newModal = false;
        this.loadingService.loadingStart();
        this.billingItem.billingId = this.idBilling;
        this.billingItem.idItemType = this.itemType;
        this.billingItem.unitPrice = this.unitPrice;
        this.billingItem.discount = this.discount;
        this.isSaving = true;
        if (this.billingItem.idBillingItem !== undefined) {
            this.subscribeToSaveResponse(
                this.billingItemService.update(this.billingItem));
        } else {
            this.subscribeToSaveResponse(
                this.billingItemService.create(this.billingItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<BillingItem>) {
        result.subscribe((res: BillingItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: BillingItem) {
        this.loadingService.loadingStop();
        this.eventManager.broadcast({ name: 'billingItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billingItem saved !');
        this.isSaving = false;
        this.billingItem = new BillingItem();
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.loadingService.loadingStop();
        this.isSaving = false;
    }

    trackBillingById(index: number, item: Billing) {
        return item.idBilling;
    }

    trackInventoryItemById(index: number, item: InventoryItem) {
        return item.idInventoryItem;
    }
    hitungHargaInputManual() {
        if (this.isPPN === false) {
        console.log('masuk ngga neehh');
        this.billingItem.taxAmount = Math.round((this.unitPrice - this.discount) * 0.1);
        this.billingItem.totalAmount = Math.round((this.unitPrice - this.discount + this.billingItem.taxAmount) * this.billingItem.qty);
        } else {
            this.billingItem.taxAmount = 0;
        this.billingItem.totalAmount = Math.round((this.unitPrice - this.discount + this.billingItem.taxAmount) * this.billingItem.qty);
        }
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.billingItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'billingItemListModification',
                        content: 'Deleted an billingItem'
                    });
                });
            }
        });
    }
}
