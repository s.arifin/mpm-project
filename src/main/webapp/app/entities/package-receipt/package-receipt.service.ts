import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { PackageReceipt } from './package-receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { createShipmentParameterOption } from '../shipment/shipment-parameter.util';
import { AbstractEntityService } from '../../shared/base/abstract-entity.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PackageReceiptService extends AbstractEntityService<PackageReceipt> {
        protected itemValues: PackageReceipt[];
        values: Subject<any> = new Subject<any>();

    protected resourceCUrl = process.env.API_C_URL + '/api/PurchaseOrder';
    protected dummy = 'http://localhost:52374/api/PurchaseOrder';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) {
        super(http, dateUtils);
        this.resourceUrl =  SERVER_API_URL + 'api/package-receipts';
        this.resourceSearchUrl = SERVER_API_URL + 'api/_search/package-receipts';
    }

    executeToSpgC(item: any): Observable<PackageReceipt> {
        console.log('ini id ioder: ', item)
        const copy = JSON.stringify(item);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl + '/CreateSpg'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    findByDocNumber(req?: any): Observable<PackageReceipt> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-doc-number', options)
            .map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
    }

    queryFilterByPTO(req?: any): Observable<ResponseWrapper> {
        const options = createShipmentParameterOption(req);
        return this.http.get(this.resourceUrl + '/filterByPTO', options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryFilterByLov(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterByLov', options)
            .map((res: Response) => this.convertResponse(res));
    }

    protected convertItemFromServer(json: any): PackageReceipt {
        const entity: PackageReceipt = Object.assign({}, json);
        entity.dateCreated = new Date(entity.dateCreated);
        return entity;
    }
}
