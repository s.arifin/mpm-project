import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PackageReceipt } from './package-receipt.model';
import { PackageReceiptService } from './package-receipt.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ShipTo, ShipToService } from '../ship-to';

import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { BillingDisbursementC, BillingDisbursementCService } from '../billing-disbursement-c';
import { DatePipe } from '@angular/common';
import { ShipmentReceiptAX, ShipmentReceiptCService } from '../shipment-receipt-c';
import { ResponseWrapper, Principal } from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { CommonUtilService } from '../../shared';
import { VehicleSalesOrderService } from '../vehicle-sales-order/vehicle-sales-order.service';

@Component({
    selector: 'jhi-package-receipt-detail',
    templateUrl: './package-receipt-detail.component.html'
})
export class PackageReceiptDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    packageReceipt: PackageReceipt;
    isSaving: boolean;
    idPackage: any;
    paramPage: number;
    routeId: number;
    qtyAccept: number;
    qtyReject: number;
    vendors = [];
    idVendor: any;

    dataweb: any;
    shipType: {};
    selectedstatus: {};
    statusFilter = [];
    statusOne: boolean;
    statusTwo: boolean;
    statusFive: boolean;

    isInternal = false;
    isVendor = false;

    // nuse
    protected axPosting: AxPosting;
    protected axPostingLine: AxPostingLine;
    protected billingDisbursementC: BillingDisbursementC;
    protected shipmentReceiptAXList: ShipmentReceiptAX[];
    protected shipmentReceiptAX: ShipmentReceiptAX;
    protected vsoService: VehicleSalesOrderService;

    internals = [];

    shiptos: ShipTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected packageReceiptService: PackageReceiptService,
        protected internalService: InternalService,
        protected shipToService: ShipToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected confirmationService: ConfirmationService,
        protected vendorService: VendorService,
        private principal: Principal,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        // protected loadingService: LoadingService,
        // nuse
        protected axPostingService: AxPostingService,
        protected datePipe: DatePipe,
        protected billingDisbursementCService: BillingDisbursementCService,
        protected shipmentReceiptCService: ShipmentReceiptCService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
    ) {
        this.packageReceipt = new PackageReceipt();
        this.routeId = 0;

        this.statusOne = true;
        this.statusTwo = true;
        this.qtyAccept = 1;
        this.qtyReject = 2;

        this.statusFilter = [
            {statusId: 1, statusDeskripsi: 'Antar Cabang'},
            {statusId: 2, statusDeskripsi: 'Main Dealer'}
        ]
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPackage = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.internalService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

    load() {
        this.packageReceiptService.find(this.idPackage).subscribe((packageReceipt) => {
            this.packageReceipt = packageReceipt;
        });
        this.loadVendor();
        if (this.vendors.length === 0) {
            this.isVendor = false;
            this.isInternal = true;
            // this.packageReceipt.shipFromId =

        } else {
            this.isVendor = true;
            this.isInternal = false;
            // this.packageReceipt.shipFromId =
        }
    }

    save() {
        this.loadingService.loadingStart();
        this.isSaving = true;
        if (this.packageReceipt.idPackage !== undefined) {
            console.log('save di package receipt ', this.packageReceipt.idPackage)
            this.subscribeToSaveResponse(
                this.packageReceiptService.update(this.packageReceipt));
        } else {  console.log('update di package receipt ', this.packageReceipt.idPackage)
            this.subscribeToSaveResponse(
                this.packageReceiptService.create(this.packageReceipt));
        }
    }

    previousState() {
        console.log(this.routeId);
            this.router.navigate(['./', { page: this.paramPage }]);
    }

    processToSPG() {
            this.confirmationService.confirm({
                message: 'Are you sure that you want process?',
                header: 'Confirmation',
                icon: 'fa fa-question-circle',
                accept: () => {
                        this.approve(this.packageReceipt);
                    this.loadingService.loadingStart();
                }
            });
            console.log('to spg: ', this.packageReceipt.shipType);
    }

    approveTransfer(_item: PackageReceipt) {
        this.subscribeToSaveResponse(
            this.packageReceiptService.executeToSpgC(this.packageReceipt));
    }

    approve(_item: PackageReceipt) {
        // this.loadingService.loadingStart();

        const objectBody: Object = {
            item: _item
        };

        const objectHeader: Object = {
            execute: 'ApproveAndBuildShipmentIncoming',
            docnumber: this.packageReceipt.documentNumber
        };

        this.subscribeToSaveResponse(this.packageReceiptService.execute(objectBody, objectHeader)
        ).then(
            () => {
                this.vehicleSalesOrderService.process({command: 'matchingSPG'}).subscribe((r) => {
                });
                console.log('packageReceipt :', this.packageReceipt);

                this.shipmentReceiptCService.findByInvoiceId(this.packageReceipt.vendorInvoice, this.packageReceipt.idPackage).subscribe(
                    (resShipmentList) => {
                        this.shipmentReceiptAXList = resShipmentList;

                        const today = new Date();
                        const myaxPostingLine = [];

                        this.axPosting = new AxPosting();
                        this.axPosting.AutoPosting = 'FALSE';
                        this.axPosting.AutoSettlement = 'FALSE';
                        this.axPosting.DataArea = this.shipmentReceiptAXList[0].idInternal.substr(0, 3);
                        this.axPosting.Guid = this.shipmentReceiptAXList[0].uuid;
                        this.axPosting.JournalName = 'AP-PurReceipt';
                        this.axPosting.Name = 'Receive Unit ' + this.shipmentReceiptAXList[0].vendorInvoice;
                        this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                        this.axPosting.TransactionType = 'Unit';

                        this.shipmentReceiptAXList.forEach(
                            (item) => {
                                console.log('ini item : ', item);
                                this.shipmentReceiptAX = item;

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '114101';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = this.shipmentReceiptAX.totalRcvPrice.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.shipmentReceiptAX.idInternal.substr(0, 3);
                                this.axPostingLine.DMSNum = this.shipmentReceiptAX.orderNumber;
                                this.axPostingLine.Description = 'Receive Unit ' + this.shipmentReceiptAX.vendorInvoice + ' ' + this.shipmentReceiptAX.idProduct;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.shipmentReceiptAX.idInternal.substr(0, 3);
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = this.shipmentReceiptAX.categoryType;
                                this.axPostingLine.Dimension5 = this.shipmentReceiptAX.segmentType;
                                this.axPostingLine.Dimension6 = this.shipmentReceiptAX.idProduct;
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.shipmentReceiptAX.vendorInvoice;
                                this.axPostingLine.Payment = '';
                                myaxPostingLine.push(this.axPostingLine);

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '114411';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.shipmentReceiptAX.totalRcvPrice.toString();
                                this.axPostingLine.Company = this.shipmentReceiptAX.idInternal.substr(0, 3);
                                this.axPostingLine.DMSNum = this.shipmentReceiptAX.orderNumber;
                                this.axPostingLine.Description = 'Receive Unit ' + this.shipmentReceiptAX.vendorInvoice + ' ' + this.shipmentReceiptAX.idProduct;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.shipmentReceiptAX.idInternal.substr(0, 3);
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.shipmentReceiptAX.vendorInvoice;
                                this.axPostingLine.Payment = '';
                                myaxPostingLine.push(this.axPostingLine);
                            }
                        );

                        this.axPosting.LedgerJournalLine = myaxPostingLine;
                        console.log('axPosting data :', this.axPosting);
                        this.axPostingService.send(this.axPosting).subscribe(
                            (resaxPosting: ResponseWrapper) =>
                                console.log('Success : ', resaxPosting.json.Message),
                            (resaxPosting: ResponseWrapper) => {
                                console.log('error ax posting : ', resaxPosting.json.Message);
                            }
                        );
                    }
                );
                // this.loadingService.loadingStop();
            }
        );
    }

    protected subscribeToSaveResponse(result: Observable<PackageReceipt>): Promise<any>  {
        console.log('masuk');
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: PackageReceipt) => {
                    this.onSaveSuccess(res);
                    resolve();
                },
                (res: Response) => {
                    this.onSaveError(res);
                    resolve();
                });
            }
        );
    }

    protected onSaveSuccess(result: PackageReceipt) {
        this.eventManager.broadcast({ name: 'packageReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'packageReceipt saved !');
        this.isSaving = false;
        this.previousState();
        this.loadingService.loadingStop();
    }

    protected onSaveError(error) {
        try {
            error.json();
        this.onError(error);
        } catch (exception) {
            this.onError(error);
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
        this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'packageReceipt Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.organization.name;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    activate() {
        this.packageReceiptService.process(
            {command: 'doActivate', idPackage: this.idPackage})
            .subscribe((rslt) => {
                this.toaster.showToaster('info', 'Valid', 'Data Verified');
            }, (err) => {
                this.toaster.showToaster('error', 'Error', err.message);
            });
        this.previousState();
    }

    approved() {
        this.packageReceiptService.process(
            {command: 'doApproved', idPackage: this.idPackage})
            .subscribe((rslt) => {
                this.toaster.showToaster('info', 'Approved', 'Data Approved..!');
            }, (err) => {
                this.toaster.showToaster('error', 'Error', err.message);
            });
        this.previousState();
    }

    cancelData() {
        this.packageReceiptService.process(
            {command: 'doCancelData', idPackage: this.idPackage})
            .subscribe((rslt) => {
                this.toaster.showToaster('info', 'Cancel Data', 'Cancel Data Done');
            }, (err) => {
                this.toaster.showToaster('error', 'Error', err.message);
            });
        this.previousState();
    }
    trackVendorById(index: number, item: Vendor) {
        return item.organization.name;
    }

    protected buildComponentChild(): void {
        this.statusOne = true;
        this.statusTwo = true;
    }

    filterstatus() {
        console.log('selected', this.shipType);
        this.dataweb = this.shipType;
        if (this.shipType = 1) {
            this.statusOne = true;
        } else if (this.shipType = 2) {
        } else if (this.shipType = null) {
            this.statusFive = true;
        }

    }

    getLovShipfrom(id) {
        console.log(id)
        console.log(this.packageReceipt.shipFromId)
        if (id === 1) {
            this.isInternal = true;
            this.isVendor = false;
        }else if (id === 2) {
            this.isInternal = false;
            this.isVendor = true;
        }
    }

    private loadVendor(): Promise<Vendor[]> {
        return new Promise<Vendor[]>(
            (resolve) => {
                this.vendorService.queryFilterByMainDealer({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['idVendor', 'asc'],
                    size : 3000}).subscribe(
                    (res: ResponseWrapper) => {
                        // this.fetchMotor = false;
                        // this.convertVendorForSelect(res.json);
                        this.vendors = res.json;
                        console.log('vendor nih', this.vendors)
                    },
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }

}
