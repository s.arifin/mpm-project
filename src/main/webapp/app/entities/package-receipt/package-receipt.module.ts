import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule, JhiLanguageHelper } from '../../shared';
import { JhiLanguageService } from 'ng-jhipster';
import { customHttpProvider } from '../../blocks/interceptor/http.provider';

import {
    PackageReceiptPopupService,
    PackageReceiptComponent,
    PackageReceiptDialogComponent,
    PackageReceiptPopupComponent,
    PackageReceiptAsLovComponent,
    PackageReceiptLovPopupComponent,
    packageReceiptRoute,
    packageReceiptPopupRoute,
    PackageReceiptResolvePagingParams,
    PackageReceiptEditComponent,
    PackageReceiptUploadComponent,
    PackageReceiptDetailComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextareaModule,
         PaginatorModule,
         ConfirmDialogModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { TooltipModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...packageReceiptRoute,
    ...packageReceiptPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        TabsModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        SliderModule,
        RadioButtonModule,
        TooltipModule
    ],
    exports: [
        PackageReceiptComponent,
        PackageReceiptEditComponent,
        PackageReceiptUploadComponent,
        PackageReceiptDetailComponent,
        PackageReceiptAsLovComponent,
        PackageReceiptLovPopupComponent
    ],
    declarations: [
        PackageReceiptComponent,
        PackageReceiptDialogComponent,
        PackageReceiptPopupComponent,
        PackageReceiptEditComponent,
        PackageReceiptUploadComponent,
        PackageReceiptDetailComponent,
        PackageReceiptAsLovComponent,
        PackageReceiptLovPopupComponent
    ],
    entryComponents: [
        PackageReceiptComponent,
        PackageReceiptDialogComponent,
        PackageReceiptPopupComponent,
        PackageReceiptEditComponent,
        PackageReceiptUploadComponent,
        PackageReceiptDetailComponent,
        PackageReceiptAsLovComponent,
        PackageReceiptLovPopupComponent
    ],
    providers: [
        PackageReceiptPopupService,
        PackageReceiptResolvePagingParams,
        customHttpProvider(),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmPackageReceiptModule {
    constructor(protected languageService: JhiLanguageService, protected languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => this.languageService.changeLanguage(languageKey));
    }
}
