import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PackageReceipt } from './package-receipt.model';
import { PackageReceiptService } from './package-receipt.service';

@Injectable()
export class PackageReceiptPopupService {
    protected ngbModalRef: NgbModalRef;
    idInternal: any;
    idShipFrom: any;
    idStatusType: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected packageReceiptService: PackageReceiptService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.packageReceiptService.find(id).subscribe((data) => {
                    // if (data.dateCreated) {
                    //    data.dateCreated = this.datePipe
                    //        .transform(data.dateCreated, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.packageReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new PackageReceipt();
                    data.internalId = this.idInternal;
                    data.shipFromId = this.idShipFrom;
                    this.ngbModalRef = this.packageReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    packageReceiptModalRef(component: Component, packageReceipt: PackageReceipt): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.packageReceipt = packageReceipt;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idShipFrom = this.idShipFrom;
        modalRef.componentInstance.idStatusType = this.idStatusType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.packageReceiptLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    packageReceiptLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
