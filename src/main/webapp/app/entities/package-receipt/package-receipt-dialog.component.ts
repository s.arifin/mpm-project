import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PackageReceipt } from './package-receipt.model';
import { PackageReceiptPopupService } from './package-receipt-popup.service';
import { PackageReceiptService } from './package-receipt.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ShipTo, ShipToService } from '../ship-to';
import { ResponseWrapper, Principal } from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { CommonUtilService } from '../../shared';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-package-receipt-dialog',
    templateUrl: './package-receipt-dialog.component.html'
})
export class PackageReceiptDialogComponent implements OnInit {

    shipType: {};
    statusFilter = [];
    packageReceipt: PackageReceipt;
    isSaving: boolean;
    idInternal: any;
    idShipFrom: any;
    internals: Internal[];
    shiptos: ShipTo[];
    vendors: Vendor[];
    idVendor: any;
    isInternal = false;
    isVendor = false;

    dataweb: any;

    statusOne: boolean;
    statusTwo: boolean;
    statusThree: boolean;
    statusFour: boolean;
    statusFive: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected packageReceiptService: PackageReceiptService,
        protected internalService: InternalService,
        protected shipToService: ShipToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected vendorService: VendorService,
        private principal: Principal,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService
    ) {
        this.statusOne = true;
        this.statusTwo = true;

        this.statusFilter = [
            {statusId: 1, statusDeskripsi: 'Antar Cabang'},
            {statusId: 2, statusDeskripsi: 'Main Dealer'}
        ]
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.loadVendor();
    }

    protected buildComponentChild(): void {
        this.statusOne = true;
        this.statusTwo = true;
        this.statusThree = true;
        this.statusFour = true;
        this.statusFive = true;
    }

    filterstatus() {
        console.log('selected', this.shipType);
        this.dataweb = this.shipType;
        if (this.shipType = 1) {
            this.statusOne = true;
        } else if (this.shipType = 2) {
        } else if (this.shipType = null) {
            this.statusFive = true;
        }

    }
    trackStatusById(index: number, item ) {
        // console.log('index', index);
        // console.log('item', item);
        return item.statusId;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.packageReceipt.idPackage !== undefined) {
            this.subscribeToSaveResponse(
                this.packageReceiptService.update(this.packageReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.packageReceiptService.create(this.packageReceipt));
        }
    }

    // protected subscribeToSaveResponse(result: Observable<PackageReceipt>) {
    //     result.subscribe(
    //         (res: PackageReceipt) => this.onSaveSuccess(res),
    //         (res: Response) => this.toaster.showToaster('warning', 'packageReceipt Changed', res.json()));
    // }
    protected subscribeToSaveResponse(result: Observable<PackageReceipt>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: PackageReceipt) => {
                        this.onSaveSuccess(res);
                        resolve();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commonUtilService.showError(err);
                        this.isSaving = false;
                        resolve();
                    }
                );
            }
        );
    }

    protected onSaveSuccess(result: PackageReceipt) {
        this.eventManager.broadcast({ name: 'packageReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'packageReceipt saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'packageReceipt Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    getLovShipfrom(id) {
        console.log(id)
        console.log(this.packageReceipt.shipFromId)
        if (id === 1) {
            this.isInternal = true;
            this.isVendor = false;
        }else if (id === 2) {
            this.isInternal = false;
            this.isVendor = true;
        }
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }
    trackVendorById(index: number, item: Vendor) {
        return item.organization.name;
    }

    private loadVendor(): Promise<Vendor[]> {
        return new Promise<Vendor[]>(
            (resolve) => {
                this.vendorService.queryFilterByMainDealer({
                    idInternal: this.principal.getIdInternal(),
                    sort: ['idVendor', 'asc'],
                    size : 3000}).subscribe(
                    (res: ResponseWrapper) => {this.vendors = res.json},
                    (res: ResponseWrapper) => this.onError(res.json),
                    () => {
                });
                resolve();
            }
        );
    }
}

@Component({
    selector: 'jhi-package-receipt-popup',
    template: ''
})
export class PackageReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected packageReceiptPopupService: PackageReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.packageReceiptPopupService
                    .open(PackageReceiptDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.packageReceiptPopupService.parent = params['parent'];
                this.packageReceiptPopupService
                    .open(PackageReceiptDialogComponent as Component);
            } else {
                this.packageReceiptPopupService
                    .open(PackageReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
