import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PackageReceipt, UploadSL } from './package-receipt.model';
import { PackageReceiptService } from './package-receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ConvertUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { CommonUtilService } from '../../shared';
import { ConfirmationService, FileUploadModule} from 'primeng/primeng';

import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-package-receipt-upload',
    templateUrl: './package-receipt-upload.component.html'
})
export class PackageReceiptUploadComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    packageReceiptUpload: UploadSL[];
    isSaving: boolean;

    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    containerData: any;

    constructor(
        protected alertService: JhiAlertService,
        protected packageReceiptService: PackageReceiptService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected convertUtilService: ConvertUtilService
    ) {
        this.packageReceiptUpload = new Array<UploadSL>();
        this.itemsPerPage = ITEMS_PER_PAGE;
    }

    ngOnInit() {
        this.loadingService.loadingStart();
        this.loadingService.loadingStop();
        this.isSaving = false;
    }

    ngOnDestroy() {
    }

    previousState() {
        this.router.navigate(['/package-receipt']);
    }

    save() {
        this.loadingService.loadingStart();
        this.isSaving = true;

        const objectBody: Object = {
            items: this.packageReceiptUpload
        };

        const objectHeader: Object = {
            execute: 'UploadShippingList'
        };

        this.subscribeToSaveResponse(
            this.packageReceiptService.executeList(objectBody, objectHeader)
        );

        console.log(this.packageReceiptUpload);
    }

    protected subscribeToSaveResponse(result: Observable<PackageReceipt[]>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: PackageReceipt[]) => {
                        this.onSaveSuccess(res);
                        resolve();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commonUtilService.showError(err);
                        this.isSaving = false;
                        resolve();
                    }
                );
            }
        );
    }

    protected onSaveSuccess(result: PackageReceipt[]) {
        this.eventManager.broadcast({ name: 'packageReceiptListModification', content: 'OK'});
        this.isSaving = false;
        this.loadingService.loadingStop();
        this.previousState();
    }

    onFileChange(event: any): void {

        this.packageReceiptUpload = new Array<UploadSL>();

        this.convertUtilService.csvToArray(event, ';').then(
            (result) => {
                for (let i = 0; i < result.length; i++) {
                    const each = result[i];

                    const uploadSL = new UploadSL();
                    const formatedDate = each[1].slice(4, 8) + '-' + each[1].slice(2, 4) + '-' + each[1].slice(0, 2);

                    uploadSL.shipType = null;
                    uploadSL.shippingListNumber = each[0];
                    uploadSL.dateCreate = new Date(formatedDate);
                    uploadSL.dealerCode01 = each[2];
                    uploadSL.dealerCode02 = each[3];
                    uploadSL.idProduct = each[4];
                    uploadSL.idColor = each[5];
                    uploadSL.idFrame = each[6];
                    uploadSL.idMachine = each[7];
                    uploadSL.yearAssembly = each[8];
                    uploadSL.invoiceNumber = each[9];
                    uploadSL.expedition = each[10];
                    uploadSL.expeditionVehicleNumber = each[11];

                    this.packageReceiptUpload = [...this.packageReceiptUpload, uploadSL];
                }

            },
            (rej) => {
                console.log('reject');
            }
        )
    }
}
