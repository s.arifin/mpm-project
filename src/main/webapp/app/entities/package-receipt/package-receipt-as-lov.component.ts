import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService} from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';

import { PackageReceipt } from './package-receipt.model';
import { PackageReceiptService } from './package-receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

import { Response } from '@angular/http';

import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PackageReceiptPopupService } from './package-receipt-popup.service';

@Component({
    selector: 'jhi-package-receipt-as-lov',
    templateUrl: './package-receipt-as-lov.component.html'
})
export class PackageReceiptAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idStatusType: any;
    packageReceipts: PackageReceipt[];
    selected: PackageReceipt[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentInternal: string;

    constructor(
        public activeModal: NgbActiveModal,
        protected packageReceiptService: PackageReceiptService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService,
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idPackage';
        this.reverse = 'asc';
        this.currentInternal = principal.getIdInternal();
    }

    loadAll() {
        if (this.currentSearch) {
            this.packageReceiptService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.packageReceiptService.queryFilterByLov({
            idStatusType: this.idStatusType,
            idInternal: this.currentInternal,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => {this.onSuccess(res.json, res.headers),
                console.log('idInternal cuy', this.principal.getIdInternal())},
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/billing-disbursement'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['./', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['./', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInPackageReceipts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PackageReceipt) {
        return item.idPackage;
    }

    registerChangeInPackageReceipts() {
        this.eventSubscriber = this.eventManager.subscribe('packageReceiptListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPackage') {
            result.push('idPackage');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.packageReceipts = data;
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    onRowSelect(selected: any) {
        this.selected = selected.data;
    }

    pushData() {
        this.packageReceiptService.pushItems(this.selected);
        // console.log('selected push data = ', this.selected);
        this.activeModal.dismiss('close');
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'jhi-package-receipt-lov-popup',
    template: ''
})
export class PackageReceiptLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected packageReceiptPopupService: PackageReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(
            (params) => {
                if (params['idStatusType']) {
                    this.packageReceiptPopupService.idStatusType = params['idStatusType'];
                    this.packageReceiptPopupService.open(PackageReceiptAsLovComponent as Component);
                }
            }
        );
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
