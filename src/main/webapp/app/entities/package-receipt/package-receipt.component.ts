import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';

import { PackageReceipt } from './package-receipt.model';
import { ShipmentParameter } from '../shipment/shipment-parameter.model'
import { PackageReceiptService } from './package-receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService} from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PackageReceiptDialogComponent } from './package-receipt-dialog.component';

@Component({
    selector: 'jhi-package-receipt',
    templateUrl: './package-receipt.component.html'
})
export class PackageReceiptComponent implements OnInit, OnDestroy {

    currentAccount: any;
    packageReceipts: PackageReceipt[];
    packageReceiptParameter: ShipmentParameter;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    public yearForCalendar: string
    public slDateFrom: Date;
    public slDateThru: Date;
    public spgDateFrom: Date;
    public spgDateThru: Date;

    public isFiltered: boolean;

    constructor(
        protected packageReceiptService: PackageReceiptService,
        protected confirmationService: ConfirmationService,
        protected commonUtilService: CommonUtilService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService,
        // protected loadingService: LoadingService,
        protected modalService: NgbModal
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;

        this.yearForCalendar = commonUtilService.setYearRangeForCalendar(0, 70);
        this.slDateFrom = new Date();
        this.slDateThru = new Date();
        this.spgDateFrom = new Date();
        this.spgDateThru = new Date();

        this.packageReceiptParameter = new ShipmentParameter();
        this.isFiltered = false;
    }

    loadAll() {
        if (this.currentSearch) {
            this.packageReceiptService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        if (this.isFiltered) {
            this.packageReceiptService.queryFilterByPTO({
                shipmentPTO: this.packageReceiptParameter,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.packageReceiptService.queryFilterBy({
                filterName: 'byStatusInternal',
                idInternal : this.principal.getIdInternal(),
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()})
            .subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/package-receipt'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/package-receipt', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/package-receipt', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    public filter(): void {
        // this.loadingService.loadingStart();
        this.isFiltered = true;
        this.packageReceiptParameter = new ShipmentParameter();
        this.packageReceiptParameter.internalId = this.principal.getIdInternal();
        this.slDateFrom.setHours(0, 0, 0, 0);
        this.slDateThru.setHours(23, 59, 59, 99);
        this.packageReceiptParameter.packageReceiptDateFrom = this.slDateFrom.toISOString();
        this.packageReceiptParameter.packageReceiptDateThru = this.slDateThru.toISOString();

        if (this.packageReceiptParameter.packageReceiptDateFrom !== null &&
            this.packageReceiptParameter.packageReceiptDateThru !== null) {
                this.loadAll();
        }

    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPackageReceipts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPackageReceipts() {
        this.eventSubscriber = this.eventManager.subscribe('packageReceiptListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPackage') {
            result.push('idPackage');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.packageReceipts = new Array<PackageReceipt>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.packageReceipts = data;
        // this.loadingService.loadingStop();
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.packageReceiptService.execute(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    addNew() {
        const data = new PackageReceipt();
        data.internalId = this.principal.getIdInternal();
        data.dateCreated = new Date();

        const modalRef = this.modalService.open(PackageReceiptDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.packageReceipt = data;
        modalRef.componentInstance.idInternal = this.principal.getIdInternal();
        modalRef.result.then((result) => {}, (reason) => {});
    }
}
