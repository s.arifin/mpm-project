import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PackageReceiptComponent } from './package-receipt.component';
import { PackageReceiptEditComponent } from './package-receipt-edit.component';
import { PackageReceiptLovPopupComponent } from './package-receipt-as-lov.component';
import { PackageReceiptUploadComponent } from './package-receipt-upload.component';
import { PackageReceiptDetailComponent } from './package-receipt-detail.component';
import { PackageReceiptPopupComponent } from './package-receipt-dialog.component';

@Injectable()
export class PackageReceiptResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'documentNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const packageReceiptRoute: Routes = [
    {
        path: '',
        component: PackageReceiptComponent,
        resolve: {
            'pagingParams': PackageReceiptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const packageReceiptPopupRoute: Routes = [
    {
        path: 'popup-new',
        component: PackageReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'new',
        component: PackageReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'upload',
        component: PackageReceiptUploadComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PackageReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':route/:page/:id/edit',
        component: PackageReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/detail',
        component: PackageReceiptDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':route/:page/:id/detail',
        component: PackageReceiptDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/popup-edit',
        component: PackageReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.packageReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lov/:idStatusType',
        component: PackageReceiptLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
