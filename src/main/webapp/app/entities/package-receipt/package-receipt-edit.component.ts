import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PackageReceipt } from './package-receipt.model';
import { PackageReceiptService } from './package-receipt.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ShipTo, ShipToService } from '../ship-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-package-receipt-edit',
    templateUrl: './package-receipt-edit.component.html'
})
export class PackageReceiptEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    packageReceipt: PackageReceipt;
    isSaving: boolean;
    idPackage: any;
    paramPage: number;
    routeId: number;

    internals: Internal[];

    shiptos: ShipTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected packageReceiptService: PackageReceiptService,
        protected internalService: InternalService,
        protected shipToService: ShipToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.packageReceipt = new PackageReceipt();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idPackage = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.packageReceiptService.find(this.idPackage).subscribe((packageReceipt) => {
            this.packageReceipt = packageReceipt;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['./', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.packageReceipt.idPackage !== undefined) {
            this.subscribeToSaveResponse(
                this.packageReceiptService.update(this.packageReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.packageReceiptService.create(this.packageReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PackageReceipt>) {
        result.subscribe((res: PackageReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PackageReceipt) {
        this.eventManager.broadcast({ name: 'packageReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'packageReceipt saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'packageReceipt Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }
}
