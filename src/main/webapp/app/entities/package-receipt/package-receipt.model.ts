import { BaseEntity } from './../../shared';

export class PackageReceipt implements BaseEntity {
    constructor(
        public id?: any,
        public idPackage?: any,
        public dateCreated?: any,
        public documentNumber?: string,
        public vendorInvoice?: string,
        public internalId?: any,
        public shipFromId?: any,
        public shipFromName?: any,
        public currentStatus?: any,
        public dateIssued?: any,
        public shipType?: any,
    ) {
    }
}

export class UploadSL {
    constructor(
        public shipType?: any,
        public shippingListNumber?: String,
        public dateCreate?: Date,
        public dealerCode01?: String,
        public dealerCode02?: String,
        public idProduct?: String,
        public idColor?: String,
        public idFrame?: String,
        public idMachine?: String,
        public yearAssembly?: Number,
        public invoiceNumber?: String,
        public expedition?: String,
        public expeditionVehicleNumber?: String,
    ) {
    }
}
