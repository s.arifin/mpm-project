import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ShipmentType} from './shipment-type.model';
import {ShipmentTypePopupService} from './shipment-type-popup.service';
import {ShipmentTypeService} from './shipment-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-shipment-type-dialog',
    templateUrl: './shipment-type-dialog.component.html'
})
export class ShipmentTypeDialogComponent implements OnInit {

    shipmentType: ShipmentType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected shipmentTypeService: ShipmentTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentType.idShipmentType !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentTypeService.update(this.shipmentType));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentTypeService.create(this.shipmentType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentType>) {
        result.subscribe((res: ShipmentType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentType) {
        this.eventManager.broadcast({ name: 'shipmentTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-shipment-type-popup',
    template: ''
})
export class ShipmentTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentTypePopupService: ShipmentTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentTypePopupService
                    .open(ShipmentTypeDialogComponent as Component, params['id']);
            } else {
                this.shipmentTypePopupService
                    .open(ShipmentTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
