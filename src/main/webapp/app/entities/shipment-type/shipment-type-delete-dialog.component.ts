import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ShipmentType } from './shipment-type.model';
import { ShipmentTypePopupService } from './shipment-type-popup.service';
import { ShipmentTypeService } from './shipment-type.service';

@Component({
    selector: 'jhi-shipment-type-delete-dialog',
    templateUrl: './shipment-type-delete-dialog.component.html'
})
export class ShipmentTypeDeleteDialogComponent {

    shipmentType: ShipmentType;

    constructor(
        protected shipmentTypeService: ShipmentTypeService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.shipmentTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'shipmentTypeListModification',
                content: 'Deleted an shipmentType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-shipment-type-delete-popup',
    template: ''
})
export class ShipmentTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentTypePopupService: ShipmentTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.shipmentTypePopupService
                .open(ShipmentTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
