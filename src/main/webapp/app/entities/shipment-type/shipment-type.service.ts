import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { ShipmentType } from './shipment-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ShipmentTypeService {
    protected itemValues: ShipmentType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/shipment-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-types';

    constructor(protected http: Http) { }

    create(shipmentType: ShipmentType): Observable<ShipmentType> {
        const copy = this.convert(shipmentType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(shipmentType: ShipmentType): Observable<ShipmentType> {
        const copy = this.convert(shipmentType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ShipmentType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, shipmentType: ShipmentType): Observable<ShipmentType> {
        const copy = this.convert(shipmentType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, shipmentTypes: ShipmentType[]): Observable<ShipmentType[]> {
        const copy = this.convertList(shipmentTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(shipmentType: ShipmentType): ShipmentType {
        if (shipmentType === null || shipmentType === {}) {
            return {};
        }
        // const copy: ShipmentType = Object.assign({}, shipmentType);
        const copy: ShipmentType = JSON.parse(JSON.stringify(shipmentType));
        return copy;
    }

    protected convertList(shipmentTypes: ShipmentType[]): ShipmentType[] {
        const copy: ShipmentType[] = shipmentTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
