import { BaseEntity } from './../../shared';

export class ShipmentType implements BaseEntity {
    constructor(
        public id?: number,
        public idShipmentType?: number,
        public description?: string,
    ) {
    }
}
