import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ShipmentType } from './shipment-type.model';
import { ShipmentTypeService } from './shipment-type.service';

@Component({
    selector: 'jhi-shipment-type-detail',
    templateUrl: './shipment-type-detail.component.html'
})
export class ShipmentTypeDetailComponent implements OnInit, OnDestroy {

    shipmentType: ShipmentType;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected shipmentTypeService: ShipmentTypeService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInShipmentTypes();
    }

    load(id) {
        this.shipmentTypeService.find(id).subscribe((shipmentType) => {
            this.shipmentType = shipmentType;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInShipmentTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'shipmentTypeListModification',
            (response) => this.load(this.shipmentType.id)
        );
    }
}
