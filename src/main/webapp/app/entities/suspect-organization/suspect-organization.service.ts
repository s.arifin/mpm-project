import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { SuspectOrganization } from './suspect-organization.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SuspectOrganizationService {
    protected itemValues: SuspectOrganization[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/suspect-organizations';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/suspect-organizations';

    constructor(protected http: Http) { }

    create(suspectOrganization: SuspectOrganization): Observable<SuspectOrganization> {
        const copy = this.convert(suspectOrganization);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(suspectOrganization: SuspectOrganization): Observable<SuspectOrganization> {
        const copy = this.convert(suspectOrganization);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SuspectOrganization> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(suspectOrganization: SuspectOrganization, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(suspectOrganization);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, suspectOrganization: SuspectOrganization): Observable<SuspectOrganization> {
        const copy = this.convert(suspectOrganization);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, suspectOrganizations: SuspectOrganization[]): Observable<SuspectOrganization[]> {
        const copy = this.convertList(suspectOrganizations);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(suspectOrganization: SuspectOrganization): SuspectOrganization {
        if (suspectOrganization === null || suspectOrganization === {}) {
            return {};
        }
        // const copy: SuspectOrganization = Object.assign({}, suspectOrganization);
        const copy: SuspectOrganization = JSON.parse(JSON.stringify(suspectOrganization));
        return copy;
    }

    protected convertList(suspectOrganizations: SuspectOrganization[]): SuspectOrganization[] {
        const copy: SuspectOrganization[] = suspectOrganizations;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SuspectOrganization[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
