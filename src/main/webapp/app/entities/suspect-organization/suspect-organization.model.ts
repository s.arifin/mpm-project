import { BaseEntity } from './../../shared';

export class SuspectOrganization implements BaseEntity {
    constructor(
        public id?: any,
        public idSuspect?: any,
        public organizationId?: any,
    ) {
    }
}
