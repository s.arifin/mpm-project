import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SuspectOrganization } from './suspect-organization.model';
import { SuspectOrganizationService } from './suspect-organization.service';
import { ToasterService} from '../../shared';
import { Organization, OrganizationService } from '../organization';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-suspect-organization-edit',
    templateUrl: './suspect-organization-edit.component.html'
})
export class SuspectOrganizationEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    suspectOrganization: SuspectOrganization;
    isSaving: boolean;

    organizations: Organization[];

    constructor(
        protected alertService: JhiAlertService,
        protected suspectOrganizationService: SuspectOrganizationService,
        protected organizationService: OrganizationService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.suspectOrganization = new SuspectOrganization();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.organizationService.query()
            .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.suspectOrganizationService.find(id).subscribe((suspectOrganization) => {
            this.suspectOrganization = suspectOrganization;
        });
    }

    previousState() {
        this.router.navigate(['suspect-organization']);
    }

    save() {
        this.isSaving = true;
        if (this.suspectOrganization.idSuspect !== undefined) {
            this.subscribeToSaveResponse(
                this.suspectOrganizationService.update(this.suspectOrganization));
        } else {
            this.subscribeToSaveResponse(
                this.suspectOrganizationService.create(this.suspectOrganization));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SuspectOrganization>) {
        result.subscribe((res: SuspectOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: SuspectOrganization) {
        this.eventManager.broadcast({ name: 'suspectOrganizationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'suspectOrganization saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'suspectOrganization Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrganizationById(index: number, item: Organization) {
        return item.idParty;
    }
}
