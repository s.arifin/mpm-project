import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {SuspectOrganization} from './suspect-organization.model';
import {SuspectOrganizationPopupService} from './suspect-organization-popup.service';
import {SuspectOrganizationService} from './suspect-organization.service';
import {ToasterService} from '../../shared';
import { Organization, OrganizationService } from '../organization';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-suspect-organization-dialog',
    templateUrl: './suspect-organization-dialog.component.html'
})
export class SuspectOrganizationDialogComponent implements OnInit {

    suspectOrganization: SuspectOrganization;
    isSaving: boolean;

    organizations: Organization[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected suspectOrganizationService: SuspectOrganizationService,
        protected organizationService: OrganizationService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.organizationService.query()
            .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.suspectOrganization.idSuspect !== undefined) {
            this.subscribeToSaveResponse(
                this.suspectOrganizationService.update(this.suspectOrganization));
        } else {
            this.subscribeToSaveResponse(
                this.suspectOrganizationService.create(this.suspectOrganization));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SuspectOrganization>) {
        result.subscribe((res: SuspectOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: SuspectOrganization) {
        this.eventManager.broadcast({ name: 'suspectOrganizationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'suspectOrganization saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'suspectOrganization Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackOrganizationById(index: number, item: Organization) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-suspect-organization-popup',
    template: ''
})
export class SuspectOrganizationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected suspectOrganizationPopupService: SuspectOrganizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.suspectOrganizationPopupService
                    .open(SuspectOrganizationDialogComponent as Component, params['id']);
            } else {
                this.suspectOrganizationPopupService
                    .open(SuspectOrganizationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
