import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SuspectOrganizationComponent } from './suspect-organization.component';
import { SuspectOrganizationEditComponent } from './suspect-organization-edit.component';
import { SuspectOrganizationPopupComponent } from './suspect-organization-dialog.component';

@Injectable()
export class SuspectOrganizationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSuspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const suspectOrganizationRoute: Routes = [
    {
        path: 'suspect-organization',
        component: SuspectOrganizationComponent,
        resolve: {
            'pagingParams': SuspectOrganizationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const suspectOrganizationPopupRoute: Routes = [
    {
        path: 'suspect-organization-new',
        component: SuspectOrganizationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect-organization/:id/edit',
        component: SuspectOrganizationEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect-organization-popup-new',
        component: SuspectOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'suspect-organization/:id/popup-edit',
        component: SuspectOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspectOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
