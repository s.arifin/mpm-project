import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Suspect } from './suspect.model';
import { SuspectPopupService } from './suspect-popup.service';
import { SuspectService } from './suspect.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { PostalAddress } from '../postal-address';
import { Person, PersonService } from '../person';
import { Salesman, SalesmanService } from '../salesman';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-suspect-edit',
    templateUrl: './suspect-edit.component.html'
})
export class SuspectEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    suspect: Suspect;
    isSaving: boolean;

    internals: Internal[];

    postaladdresses: PostalAddress[];

    people: Person[];

    salesmen: Salesman[];

    constructor(
        protected alertService: JhiAlertService,
        protected suspectService: SuspectService,
        protected internalService: InternalService,
        protected personService: PersonService,
        protected salesmanService: SalesmanService,
        protected route: ActivatedRoute,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.suspect = new Suspect();
        this.suspect.person = new Person();
        this.suspect.postalAddress = new PostalAddress();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.personService.query()
            .subscribe((res: ResponseWrapper) => { this.people = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.salesmanService.query()
            .subscribe((res: ResponseWrapper) => { this.salesmen = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.suspectService.find(id).subscribe((suspect) => {
            this.suspect = suspect;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.suspect.idSuspect !== undefined) {
            this.subscribeToSaveResponse(
                this.suspectService.update(this.suspect));
        } else {
            this.subscribeToSaveResponse(
                this.suspectService.create(this.suspect));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Suspect>) {
        result.subscribe((res: Suspect) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Suspect) {
        this.eventManager.broadcast({ name: 'suspectListModification', content: 'OK' });
        this.toaster.showToaster('info', 'Save', 'suspect saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'suspect Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackPostalAddressById(index: number, item: PostalAddress) {
        return item.idContact;
    }

    trackPersonById(index: number, item: Person) {
        return item.idParty;
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }
}
