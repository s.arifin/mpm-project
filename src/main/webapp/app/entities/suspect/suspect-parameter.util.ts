import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createSuspectParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('status', req.suspectPTO.status);
        params.set('marketName', req.suspectPTO.marketName);
        params.set('workTypeId', req.suspectPTO.workTypeId);
        params.set('districtId', req.suspectPTO.districtId);
        params.set('coordinatorSalesId', req.suspectPTO.coordinatorSalesId);
        params.set('salesmanId', req.suspectPTO.salesmanId);
        params.set('saleTypeId', req.suspectPTO.saleTypeId);
        params.set('suspectTypeId', req.suspectPTO.suspectTypeId);
        params.set('internalId', req.suspectPTO.internalId);

        options.params = params;
    }
    return options;
}
