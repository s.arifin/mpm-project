export class SuspectParameter {
    constructor(
        public status?: Array<Number>,
        public marketNameId?: String,
        public orderDateFrom?: any,
        public orderDateThru?: any,
        public workTypeId?: Number,
        public districtId?: String,
        public coordinatorSalesId?: String,
        public salesmanId?: String,
        public saleTypeId?: Number,
        public suspectTypeId?: Number,
        public internalId?: String,
    ) {
        this.status = [10, 11];
        this.marketNameId = null;
        this.orderDateFrom = null;
        this.orderDateThru = null;
        this.workTypeId = null;
        this.districtId = null;
        this.coordinatorSalesId = null;
        this.salesmanId = null;
        this.saleTypeId = null;
        this.suspectTypeId = null;
        this.internalId = null;
    }
}
