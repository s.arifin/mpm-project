import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    SuspectService,
    SuspectPopupService,
    SuspectComponent,
    SuspectDetailComponent,
    SuspectDialogComponent,
    SuspectPopupComponent,
    suspectRoute,
    suspectPopupRoute,
    SuspectResolvePagingParams,
    SuspectEditComponent,
} from './';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

const ENTITY_STATES = [
    ...suspectRoute,
    ...suspectPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        SuspectComponent,
        SuspectDetailComponent,
        SuspectEditComponent
    ],
    declarations: [
        SuspectComponent,
        SuspectDetailComponent,
        SuspectDialogComponent,
        SuspectPopupComponent,
        SuspectEditComponent
    ],
    entryComponents: [
        SuspectComponent,
        SuspectDialogComponent,
        SuspectPopupComponent,
        SuspectEditComponent
    ],
    providers: [
        SuspectService,
        SuspectPopupService,
        SuspectResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSuspectModule {}
