import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {Suspect} from './suspect.model';
import {SuspectService} from './suspect.service';

@Component({
    selector: 'jhi-suspect-detail',
    templateUrl: './suspect-detail.component.html'
})
export class SuspectDetailComponent implements OnInit, OnDestroy {

    suspect: Suspect;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected suspectService: SuspectService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSuspects();
    }

    load(id) {
        this.suspectService.find(id).subscribe((suspect) => {
            this.suspect = suspect;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSuspects() {
        this.eventSubscriber = this.eventManager.subscribe(
            'suspectListModification',
            (response) => this.load(this.suspect.idSuspect)
        );
    }
}
