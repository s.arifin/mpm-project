import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Suspect } from './suspect.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SuspectService {

    protected resourceUrl = 'api/suspects';
    protected resourceSearchUrl = 'api/_search/suspects';

    constructor(protected http: Http) { }

    create(suspect: Suspect): Observable<Suspect> {
        const copy = this.convert(suspect);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(suspect: Suspect): Observable<Suspect> {
        const copy = this.convert(suspect);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<Suspect> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(suspect: Suspect, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(suspect);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(suspect: any): Observable<String> {
        const copy = this.convert(suspect);
        return this.http.post(this.resourceUrl + '/execute', copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(suspect: Suspect): Suspect {
        const copy: Suspect = Object.assign({}, suspect);
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
