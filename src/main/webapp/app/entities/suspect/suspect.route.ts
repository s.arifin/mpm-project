import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SuspectComponent } from './suspect.component';
import { SuspectDetailComponent } from './suspect-detail.component';
import { SuspectEditComponent } from './suspect-edit.component';
import { SuspectPopupComponent } from './suspect-dialog.component';

@Injectable()
export class SuspectResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSuspect,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const suspectRoute: Routes = [
    {
        path: 'suspect',
        component: SuspectComponent,
        resolve: {
            'pagingParams': SuspectResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'suspect/:id',
        component: SuspectDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const suspectPopupRoute: Routes = [
    {
        path: 'suspect-new',
        component: SuspectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect/:id/edit',
        component: SuspectEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspect.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'suspect-popup-new',
        component: SuspectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspect.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'suspect/:id/popup-edit',
        component: SuspectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.suspect.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
