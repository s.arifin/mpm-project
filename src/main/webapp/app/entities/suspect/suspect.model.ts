import { BaseEntity } from './../../shared';

export class Suspect implements BaseEntity {
    constructor(
        public id?: any,
        public idSuspect?: any,
        public suspectNumber?: string,
        public dealerId?: any,
        public postalAddress?: any,
        public personId?: any,
        public person?: any,
        public salesCoordinatorId?: any,
        public salesmanId?: any,
        public salesDate?: Date,
        public marketName?: string
    ) {
    }
}
