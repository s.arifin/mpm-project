export * from './suspect.model';
export * from './suspect-parameter.model';
export * from './suspect-parameter.util';
export * from './suspect-popup.service';
export * from './suspect.service';
export * from './suspect-dialog.component';
export * from './suspect-detail.component';
export * from './suspect.component';
export * from './suspect.route';
export * from './suspect-edit.component';
