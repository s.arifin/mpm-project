import { BaseEntity } from './../../shared';
import { Organization } from '../organization';
import { PartyRole } from '../party-role';

export class Internal implements BaseEntity {
    constructor(
        public id?: any,
        public idInternal?: string,
        public partyId?: any,
        public partyName?: any,
        public idMPM ?: any,
        public idAHM ?: any,
        public idRoleType?: number,
        public organization?: Organization,
    ) {
        this.organization = new Organization();
    }
}
