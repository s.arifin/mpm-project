import { BaseEntity } from './../../shared';

export class Product implements BaseEntity {
    constructor(
        public id?: any,
        public idProduct?: any,
        public name?: string,
        public description?: string,
        public dateIntroduction?: Date,
        public dateDiscontinue?: Date,
        public productTypeId?: any,
        public features?: any,
        public categories?: any,
    ) {
    }
}
