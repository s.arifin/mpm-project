import { BaseEntity } from './../../shared';
import { PostalAddress } from './../postal-address';
import { Party } from './../party';

export class Person extends Party {
    constructor(
        public firstName?: string,
        public lastName?: string,
        public pob?: string,
        public bloodType?: string,
        public gender?: string,
        public dob?: any,
        public personalIdNumber?: string,
        public familyIdNumber?: string,
        public taxIdNumber?: string,
        public phone?: string,
        public cellPhone1?: string,
        public cellPhone2?: string,
        public privateMail?: string,
        public postalAddress?: PostalAddress,
        public contacts?: BaseEntity[],
        public categories?: BaseEntity[],
        public facilities?: BaseEntity[],
        public religionTypeId?: any,
        public religionTypeDescription?: any,
        public workTypeId?: any,
        public workTypeDescription?: any,
        public userId?: any,
        public notes?: any,
    ) {
        super();
        this.postalAddress = new PostalAddress();
        this.dob = new Date('1950-01-01');
    }
}
