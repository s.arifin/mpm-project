import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Person } from './person.model';
import { createRequestOption, ResponseWrapper } from '../../shared';
import { AbstractEntityService } from '../../shared/base/abstract-entity.service';

@Injectable()
export class PersonService extends AbstractEntityService<Person> {

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) {
        super(http, dateUtils);
        this.resourceUrl =  SERVER_API_URL + 'api/people';
        this.resourceSearchUrl = SERVER_API_URL + 'api/_search/people';
    }

    findByPersonalIdNumber(req?: any): Observable<Person> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-personal-id-number', options)
            .map((res: Response) => {
                let jsonResponse: any;
                jsonResponse = null;

                if (res.text() !== '' && res.text() !== null) {
                    jsonResponse = res.json();
                }

                return this.convertItemFromServer(jsonResponse);
        });
    }

    findByPersonalIdNumberCustomer(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/by-personal-customer-id-number', options)
        .map((res: Response) => this.convertResponse(res));
    }

    protected convertItemFromServer(json: any): Person {
        const entity: Person = Object.assign(new Person(), json);
        if (entity.dob) {
            entity.dob = new Date(entity.dob);
        }
        return entity;
    }

    protected convert(person: Person): Person {
        const copy: Person = JSON.parse(JSON.stringify(person));
        return copy;
    }

    updatePerson(entity: Person): Observable<Person> {
        const copy = this.convert(entity);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    find(id: any): Observable<Person> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

}
