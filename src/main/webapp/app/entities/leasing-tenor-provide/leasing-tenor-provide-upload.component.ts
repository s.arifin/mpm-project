import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LeasingTenorProvide, LeasingTenorProvideUpload } from './leasing-tenor-provide.model';
import { LeasingTenorProvideService } from './leasing-tenor-provide.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import * as XLSX from 'xlsx';

@Component({
    selector: 'jhi-leasing-tenor-provide-upload',
    templateUrl: './leasing-tenor-provide-upload.component.html',
    styles: []
})
export class LeasingTenorProvideUploadComponent implements OnInit, OnDestroy {

    leasingComponent: LeasingTenorProvide;
    leasingComponents: LeasingTenorProvide[];
    leasingComponentUpload: LeasingTenorProvideUpload;
    leasingComponentsUpload: LeasingTenorProvideUpload[];
    eventSubscriber: Subscription;
    containerData: any;
    invalidFormat: Boolean;

    constructor(
        protected leasingComponentService: LeasingTenorProvideService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
    }

    loadAll() {
    }

    clear() {
        this.router.navigate(['/leasing-tenor-provide']);
    }

    ngOnInit() {
        this.Components();
    }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbooka */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            /* save data */
            this.containerData = (XLSX.utils.sheet_to_json(ws));

            this.convertXLXStoListSuspect();
        };
        reader.readAsBinaryString(target.files[0]);
    }

    protected convertXLXStoListSuspect(): void {
        console.log('container data ==>', this.containerData);
        if (this.containerData[0]['idleasingfincoy'] &&
            this.containerData[0]['idproduct']) {
            this.leasingComponentsUpload = new Array<LeasingTenorProvideUpload>();
            this.containerData.forEach((element) => {
                this.leasingComponentUpload = new LeasingTenorProvideUpload();
                this.leasingComponentUpload.idLeasingFincoy = element['idleasingfincoy'];
                this.leasingComponentUpload.idProduct = element['idproduct'];
                this.leasingComponentUpload.tenor = element['tenor'];
                this.leasingComponentUpload.installment = element['installment'];
                this.leasingComponentUpload.downPayment = element['downpayment'];
                this.leasingComponentUpload.dateFrom = element['datefrom'];
                this.leasingComponentUpload.dateThru = element['datethru'];
                this.leasingComponentsUpload = [...this.leasingComponentsUpload, this.leasingComponentUpload];
                console.log('upload = ', this.leasingComponentsUpload);
            });
        } else {
            this.invalidFormat = true;
            this.toasterService.showToaster('Info', 'Format Upload Salah', 'File yang di upload salah ..')
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    Components() {
        this.eventSubscriber = this.eventManager.subscribe('leasingModification', (response) => this.loadAll());
    }

    saveList(): void {
        console.log('save list: ', this.leasingComponentsUpload);
        this.leasingComponentService.uploadLeasingComponent(this.leasingComponentsUpload).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'LeasingTenorProvide Saved', 'Data saved..');
            return this.clear();
            }
        );
    }
}
