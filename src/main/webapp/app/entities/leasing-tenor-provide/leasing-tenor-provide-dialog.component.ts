import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {LeasingTenorProvide} from './leasing-tenor-provide.model';
import {LeasingTenorProvidePopupService} from './leasing-tenor-provide-popup.service';
import {LeasingTenorProvideService} from './leasing-tenor-provide.service';
import {ToasterService} from '../../shared';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';
import { ResponseWrapper } from '../../shared';
import { MotorService, Motor } from '../motor';

@Component({
    selector: 'jhi-leasing-tenor-provide-dialog',
    templateUrl: './leasing-tenor-provide-dialog.component.html'
})
export class LeasingTenorProvideDialogComponent implements OnInit {

    leasingTenorProvide: LeasingTenorProvide;
    isSaving: boolean;

    leasingcompanies: LeasingCompany[];

    motors: Motor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected leasingTenorProvideService: LeasingTenorProvideService,
        protected leasingCompanyService: LeasingCompanyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected motorService: MotorService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.leasingCompanyService.query().subscribe(
            (res: ResponseWrapper) => {
                this.leasingcompanies = res.json;
                console.log('a', this.leasingcompanies);
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );

        this.motorService.query().subscribe(
            (res) => {
                this.motors = res.json;
            },
            (res) => this.onError(res.json)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        console.log('save', this.leasingTenorProvide);
        this.isSaving = true;
        if (this.leasingTenorProvide.idLeasingProvide !== undefined) {
            this.subscribeToSaveResponse(
                this.leasingTenorProvideService.update(this.leasingTenorProvide));
        } else {
            this.subscribeToSaveResponse(
                this.leasingTenorProvideService.create(this.leasingTenorProvide));
        }
    }

    protected subscribeToSaveResponse(result: Observable<LeasingTenorProvide>) {
        result.subscribe((res: LeasingTenorProvide) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: LeasingTenorProvide) {
        this.eventManager.broadcast({ name: 'leasingTenorProvideListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'leasingTenorProvide saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'leasingTenorProvide Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackLeasingCompanyById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-leasing-tenor-provide-popup',
    template: ''
})
export class LeasingTenorProvidePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected leasingTenorProvidePopupService: LeasingTenorProvidePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.leasingTenorProvidePopupService
                    .open(LeasingTenorProvideDialogComponent as Component, params['id']);
            } else {
                this.leasingTenorProvidePopupService
                    .open(LeasingTenorProvideDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
