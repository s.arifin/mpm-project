import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { LeasingTenorProvide, LeasingTenorProvideUpload } from './leasing-tenor-provide.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class LeasingTenorProvideService {
    protected itemValues: LeasingTenorProvide[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/leasing-tenor-provides';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/leasing-tenor-provides';
    protected resourceCUrl = process.env.API_C_URL + '/api/leasing-tenor-provide';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(leasingTenorProvide: LeasingTenorProvide): Observable<LeasingTenorProvide> {
        const copy = this.convert(leasingTenorProvide);
        return this.http.post(this.resourceCUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(leasingTenorProvide: LeasingTenorProvide): Observable<LeasingTenorProvide> {
        const copy = this.convert(leasingTenorProvide);
        return this.http.put(this.resourceCUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getActiveTenorByIdProductAndIdLeasing(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idleasing', req.idleasing);
        return this.http.get(this.resourceUrl + '/by-leasing-and-product', options)
                .map((res: Response) => this.convertResponse(res));
    }

    getActiveDownPaymentByIdProductAndIdLeasingAndTenor(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idleasing', req.idleasing);
        options.params.set('tenor', req.tenor);
        return this.http.get(this.resourceUrl + '/by-leasing-and-product-tenor', options)
                .map((res: Response) => this.convertResponse(res));
    }

    getActiveDownPaymentByIdProductAndIdLeasingAndTenorAndDownPayment(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idleasing', req.idleasing);
        options.params.set('tenor', req.tenor);
        options.params.set('downpayment', req.downpayment);
        return this.http.get(this.resourceUrl + '/by-leasing-and-product-tenor-downpayment', options)
                .map((res: Response) => this.convertResponse(res));
    }

    getActiveInstallmentByIdProductAndIdLeasingAndTenorAndDownPayment(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idleasing', req.idleasing);
        options.params.set('tenor', req.tenor);
        options.params.set('installment', req.installment);
        return this.http.get(this.resourceUrl + '/by-leasing-and-product-tenor-installment', options)
                .map((res: Response) => this.convertResponse(res));
    }

    find(id: any): Observable<LeasingTenorProvide> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findById(id: any): Observable<LeasingTenorProvide> {
        return this.http.get(`${this.resourceCUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getByFilter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/filter', options)
            .map((res: any) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceCUrl}/${id}`);
    }

    uploadLeasingComponent(LeasingTenorProvidesUpload: LeasingTenorProvideUpload[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(LeasingTenorProvidesUpload);

        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/upload`, copy, options).map((res: Response) => {
            return res;
        });
    }

    executeProcess(id: Number, param: String, leasingTenorProvide: LeasingTenorProvide): Observable<LeasingTenorProvide> {
        const copy = this.convert(leasingTenorProvide);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, leasingTenorProvides: LeasingTenorProvide[]): Observable<LeasingTenorProvide[]> {
        const copy = this.convertList(leasingTenorProvides);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(leasingTenorProvide: LeasingTenorProvide): LeasingTenorProvide {
        if (leasingTenorProvide === null || leasingTenorProvide === {}) {
            return {};
        }
        // const copy: LeasingTenorProvide = Object.assign({}, leasingTenorProvide);
        const copy: LeasingTenorProvide = JSON.parse(JSON.stringify(leasingTenorProvide));

        // copy.dateFrom = this.dateUtils.toDate(leasingTenorProvide.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(leasingTenorProvide.dateThru);
        return copy;
    }

    protected convertList(leasingTenorProvides: LeasingTenorProvide[]): LeasingTenorProvide[] {
        const copy: LeasingTenorProvide[] = leasingTenorProvides;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: LeasingTenorProvide[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
