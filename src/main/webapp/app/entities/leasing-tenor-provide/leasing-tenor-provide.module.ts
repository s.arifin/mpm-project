import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    LeasingTenorProvideService,
    LeasingTenorProvidePopupService,
    LeasingTenorProvideComponent,
    LeasingTenorProvideDialogComponent,
    LeasingTenorProvidePopupComponent,
    leasingTenorProvideRoute,
    leasingTenorProvidePopupRoute,
    LeasingTenorProvideResolvePagingParams,
    LeasingTenorProvideAsLovComponent,
    LeasingTenorProvideLovPopupComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataTableModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...leasingTenorProvideRoute,
    ...leasingTenorProvidePopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        LeasingTenorProvideComponent,
        LeasingTenorProvideAsLovComponent,
        LeasingTenorProvideLovPopupComponent,
    ],
    declarations: [
        LeasingTenorProvideComponent,
        LeasingTenorProvideDialogComponent,
        LeasingTenorProvidePopupComponent,
        LeasingTenorProvideAsLovComponent,
        LeasingTenorProvideLovPopupComponent,
    ],
    entryComponents: [
        LeasingTenorProvideComponent,
        LeasingTenorProvideDialogComponent,
        LeasingTenorProvidePopupComponent,
        LeasingTenorProvideAsLovComponent,
        LeasingTenorProvideLovPopupComponent,
    ],
    providers: [
        LeasingTenorProvideService,
        LeasingTenorProvidePopupService,
        LeasingTenorProvideResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmLeasingTenorProvideModule {}
