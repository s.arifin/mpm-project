import { BaseEntity } from './../../shared';

export class LeasingTenorProvide implements BaseEntity {
    constructor(
        public id?: any,
        public idLeasingProvide?: any,
        public productId?: string,
        public description?: string,
        public productName?: string,
        public tenor?: number,
        public internalId?: String,
        public installment?: number,
        public downPayment?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public leasingId?: any,
        public leasingName?: string
    ) {
    }
}

export class LeasingTenorProvideUpload implements BaseEntity {
    constructor(
        public id?: any,
        public idLeasingFincoy?: any,
        public idProduct?: any,
        public tenor?: number,
        public installment?: number,
        public downPayment?: number,
        public dateFrom?: any,
        public dateThru?: any
    ) {
    }
}
