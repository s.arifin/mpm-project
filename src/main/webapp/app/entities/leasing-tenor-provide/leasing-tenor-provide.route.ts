import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LeasingTenorProvideComponent } from './leasing-tenor-provide.component';
import { LeasingTenorProvideLovPopupComponent } from './leasing-tenor-provide-as-lov.component';
import { LeasingTenorProvidePopupComponent } from './leasing-tenor-provide-dialog.component';

@Injectable()
export class LeasingTenorProvideResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idLeasingProvide,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const leasingTenorProvideRoute: Routes = [
    {
        path: 'leasing-tenor-provide',
        component: LeasingTenorProvideComponent,
        resolve: {
            'pagingParams': LeasingTenorProvideResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingTenorProvide.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const leasingTenorProvidePopupRoute: Routes = [
    {
        path: 'leasing-tenor-provide-lov',
        component: LeasingTenorProvideLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingTenorProvide.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'leasing-tenor-provide-new',
        component: LeasingTenorProvidePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingTenorProvide.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'leasing-tenor-provide/:id/edit',
        component: LeasingTenorProvidePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.leasingTenorProvide.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
