import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { LeasingTenorProvide } from './leasing-tenor-provide.model';
import { LeasingTenorProvideService } from './leasing-tenor-provide.service';

@Injectable()
export class LeasingTenorProvidePopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected leasingTenorProvideService: LeasingTenorProvideService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.leasingTenorProvideService.find(id).subscribe((data) => {
                    // if (data.dateFrom) {
                    //    data.dateFrom = this.datePipe
                    //        .transform(data.dateFrom, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateThru) {
                    //    data.dateThru = this.datePipe
                    //        .transform(data.dateThru, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.leasingTenorProvideModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new LeasingTenorProvide();
                    this.ngbModalRef = this.leasingTenorProvideModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    leasingTenorProvideModalRef(component: Component, leasingTenorProvide: LeasingTenorProvide): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.leasingTenorProvide = leasingTenorProvide;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.leasingTenorProvideLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    leasingTenorProvideLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
