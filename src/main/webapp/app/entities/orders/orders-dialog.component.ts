import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Orders} from './orders.model';
import {OrdersPopupService} from './orders-popup.service';
import {OrdersService} from './orders.service';
import {ToasterService} from '../../shared';
import { PaymentApplication, PaymentApplicationService } from '../payment-application';
import { OrderType, OrderTypeService } from '../order-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-orders-dialog',
    templateUrl: './orders-dialog.component.html'
})
export class OrdersDialogComponent implements OnInit {

    orders: Orders;
    isSaving: boolean;

    paymentapplications: PaymentApplication[];

    ordertypes: OrderType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected ordersService: OrdersService,
        protected paymentApplicationService: PaymentApplicationService,
        protected orderTypeService: OrderTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentApplicationService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentapplications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.ordertypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orders.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.ordersService.update(this.orders));
        } else {
            this.subscribeToSaveResponse(
                this.ordersService.create(this.orders));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Orders>) {
        result.subscribe((res: Orders) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Orders) {
        this.eventManager.broadcast({ name: 'ordersListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orders saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'orders Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPaymentApplicationById(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    trackOrderTypeById(index: number, item: OrderType) {
        return item.idOrderType;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-orders-popup',
    template: ''
})
export class OrdersPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected ordersPopupService: OrdersPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ordersPopupService
                    .open(OrdersDialogComponent as Component, params['id']);
            } else {
                this.ordersPopupService
                    .open(OrdersDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
