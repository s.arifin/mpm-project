export class OrdersParameter {
    constructor(
        public status?: Array<Number>,
        public statusNotIn?: Array<Number>,
        public orderDateFrom?: any,
        public orderDateThru?: any,
        public appDateFrom?: any,
        public appDateThru?: any,
        public leasingCompanyId?: String,
        public coordinatorSalesId?: String,
        public salesmanId?: String,
        public unitStatus?: Number,
        public internalId?: String,
        public statusFaktur?: String,
        public statusAL?: Array<Number>,
    ) {
        this.status = [10, 11, 18, 74, 61, 13, 17, 88];
        this.statusNotIn = [];
        this.orderDateFrom = null;
        this.orderDateThru = null;
        this.appDateFrom = null;
        this.appDateThru = null;
        this.leasingCompanyId = null;
        this.coordinatorSalesId = null;
        this.salesmanId = null;
        this.unitStatus = null;
        this.internalId = null;
        this.statusFaktur = null;
        this.statusAL = [];
    }
}
