import { URLSearchParams, BaseRequestOptions } from '@angular/http';

export const createOrdersParameterOption = (req?: any): BaseRequestOptions => {
    const options: BaseRequestOptions = new BaseRequestOptions();
    if (req) {
        const params: URLSearchParams = new URLSearchParams();
        params.set('page', req.page);
        params.set('size', req.size);
        if (req.sort) {
            params.paramsMap.set('sort', req.sort);
        }
        params.set('query', req.query);

        params.set('status', req.ordersPTO.status);
        params.set('statusNotIn', req.ordersPTO.statusNotIn);
        params.set('orderDateFrom', req.ordersPTO.orderDateFrom);
        params.set('orderDateThru', req.ordersPTO.orderDateThru);
        params.set('appDateFrom', req.ordersPTO.appDateFrom);
        params.set('appDateThru', req.ordersPTO.appDateThru);
        params.set('leasingCompanyId', req.ordersPTO.leasingCompanyId);
        params.set('coordinatorSalesId', req.ordersPTO.coordinatorSalesId);
        params.set('salesmanId', req.ordersPTO.salesmanId);
        params.set('internalId', req.ordersPTO.internalId);
        params.set('statusFaktur', req.ordersPTO.statusFaktur);
        params.set('statusAL', req.ordersPTO.statusAL);

        options.params = params;
    }
    return options;
}
