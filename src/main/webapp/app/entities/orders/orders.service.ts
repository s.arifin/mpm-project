import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { JhiDateUtils } from 'ng-jhipster';

import { Orders } from './orders.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class OrdersService {
    protected itemValues: Orders[];
    values: Subject<any> = new Subject();

    protected resourceUrl = 'api/orders';
    protected resourceSearchUrl = 'api/_search/orders';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(orders: Orders): Observable<Orders> {
        const copy = this.convert(orders);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(orders: Orders): Observable<Orders> {
        const copy = this.convert(orders);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Orders> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(orders: Orders, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(orders);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, orders: Orders): Observable<Orders> {
        const copy = this.convert(orders);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, orderss: Orders[]): Observable<Orders[]> {
        const copy = this.convertList(orderss);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateEntry) {
            entity.dateEntry = new Date(entity.dateEntry);
        }
        if (entity.dateOrder) {
            entity.dateOrder = new Date(entity.dateOrder);
        }
    }

    protected convert(orders: Orders): Orders {
        if (orders === null || orders === {}) {
            return {};
        }
        // const copy: Orders = Object.assign({}, orders);
        const copy: Orders = JSON.parse(JSON.stringify(orders));

        // copy.dateEntry = this.dateUtils.toDate(orders.dateEntry);

        // copy.dateOrder = this.dateUtils.toDate(orders.dateOrder);
        return copy;
    }

    protected convertList(orderss: Orders[]): Orders[] {
        const copy: Orders[] = orderss;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Orders[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
