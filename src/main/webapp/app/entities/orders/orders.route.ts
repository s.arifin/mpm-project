import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { OrdersComponent } from './orders.component';
import { OrdersEditComponent } from './orders-edit.component';
import { OrdersPopupComponent } from './orders-dialog.component';

@Injectable()
export class OrdersResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ordersRoute: Routes = [
    {
        path: 'orders',
        component: OrdersComponent,
        resolve: {
            'pagingParams': OrdersResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ordersPopupRoute: Routes = [
    {
        path: 'orders-new',
        component: OrdersEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'orders/:id/edit',
        component: OrdersEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'orders-popup-new',
        component: OrdersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'orders/:id/popup-edit',
        component: OrdersPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
