import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Orders } from './orders.model';
import { OrdersService } from './orders.service';
import { ToasterService} from '../../shared';
import { PaymentApplication, PaymentApplicationService } from '../payment-application';
import { OrderType, OrderTypeService } from '../order-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-orders-edit',
    templateUrl: './orders-edit.component.html'
})
export class OrdersEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    orders: Orders;
    isSaving: boolean;

    paymentapplications: PaymentApplication[];

    ordertypes: OrderType[];

    constructor(
        protected alertService: JhiAlertService,
        protected ordersService: OrdersService,
        protected paymentApplicationService: PaymentApplicationService,
        protected orderTypeService: OrderTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.orders = new Orders();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.paymentApplicationService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentapplications = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.ordertypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.ordersService.find(id).subscribe((orders) => {
            this.orders = orders;
        });
    }

    previousState() {
        this.router.navigate(['orders']);
    }

    save() {
        this.isSaving = true;
        if (this.orders.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.ordersService.update(this.orders));
        } else {
            this.subscribeToSaveResponse(
                this.ordersService.create(this.orders));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Orders>) {
        result.subscribe((res: Orders) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Orders) {
        this.eventManager.broadcast({ name: 'ordersListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orders saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'orders Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPaymentApplicationById(index: number, item: PaymentApplication) {
        return item.idPaymentApplication;
    }

    trackOrderTypeById(index: number, item: OrderType) {
        return item.idOrderType;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
