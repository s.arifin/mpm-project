import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {UnitReqInternal} from './unit-req-internal.model';
import {UnitReqInternalPopupService} from './unit-req-internal-popup.service';
import {UnitReqInternalService} from './unit-req-internal.service';
import {ToasterService} from '../../shared';
import { Branch, BranchService } from '../branch';
import { Motor, MotorService } from '../motor';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-req-internal-dialog',
    templateUrl: './unit-req-internal-dialog.component.html'
})
export class UnitReqInternalDialogComponent implements OnInit {

    unitReqInternal: UnitReqInternal;
    isSaving: boolean;

    branches: Branch[];

    motors: Motor[];

    orderitems: OrderItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected unitReqInternalService: UnitReqInternalService,
        protected branchService: BranchService,
        protected motorService: MotorService,
        protected orderItemService: OrderItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.branchService.query()
            .subscribe((res: ResponseWrapper) => { this.branches = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitReqInternal.idRequirement !== undefined) {
            this.subscribeToSaveResponse(
                this.unitReqInternalService.update(this.unitReqInternal));
        } else {
            this.subscribeToSaveResponse(
                this.unitReqInternalService.create(this.unitReqInternal));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitReqInternal>) {
        result.subscribe((res: UnitReqInternal) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: UnitReqInternal) {
        this.eventManager.broadcast({ name: 'unitReqInternalListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitReqInternal saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'unitReqInternal Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBranchById(index: number, item: Branch) {
        return item.idInternal;
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-unit-req-internal-popup',
    template: ''
})
export class UnitReqInternalPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitReqInternalPopupService: UnitReqInternalPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitReqInternalPopupService
                    .open(UnitReqInternalDialogComponent as Component, params['id']);
            } else {
                this.unitReqInternalPopupService
                    .open(UnitReqInternalDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
