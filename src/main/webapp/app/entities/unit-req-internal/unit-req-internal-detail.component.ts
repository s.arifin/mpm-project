import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import {UnitReqInternal} from './unit-req-internal.model';
import {UnitReqInternalService} from './unit-req-internal.service';

@Component({
    selector: 'jhi-unit-req-internal-detail',
    templateUrl: './unit-req-internal-detail.component.html'
})
export class UnitReqInternalDetailComponent implements OnInit, OnDestroy {

    unitReqInternal: UnitReqInternal;
    protected subscription: Subscription;
    protected eventSubscriber: Subscription;

    constructor(
        protected eventManager: JhiEventManager,
        protected unitReqInternalService: UnitReqInternalService,
        protected route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUnitReqInternals();
    }

    load(id) {
        this.unitReqInternalService.find(id).subscribe((unitReqInternal) => {
            this.unitReqInternal = unitReqInternal;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUnitReqInternals() {
        this.eventSubscriber = this.eventManager.subscribe(
            'unitReqInternalListModification',
            (response) => this.load(this.unitReqInternal.idRequirement)
        );
    }
}
