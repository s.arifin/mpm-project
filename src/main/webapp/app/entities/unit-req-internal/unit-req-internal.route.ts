import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnitReqInternalComponent } from './unit-req-internal.component';
import { UnitReqInternalDetailComponent } from './unit-req-internal-detail.component';
import { UnitReqInternalPopupComponent } from './unit-req-internal-dialog.component';

@Injectable()
export class UnitReqInternalResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unitReqInternalRoute: Routes = [
    {
        path: 'unit-req-internal',
        component: UnitReqInternalComponent,
        resolve: {
            'pagingParams': UnitReqInternalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitReqInternal.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'unit-req-internal/:id',
        component: UnitReqInternalDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitReqInternal.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitReqInternalPopupRoute: Routes = [
    {
        path: 'unit-req-internal-new',
        component: UnitReqInternalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitReqInternal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-req-internal/:id/edit',
        component: UnitReqInternalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitReqInternal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
