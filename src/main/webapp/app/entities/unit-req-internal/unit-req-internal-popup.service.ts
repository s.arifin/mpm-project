import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { UnitReqInternal } from './unit-req-internal.model';
import { UnitReqInternalService } from './unit-req-internal.service';

@Injectable()
export class UnitReqInternalPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected unitReqInternalService: UnitReqInternalService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unitReqInternalService.find(id).subscribe((data) => {
                    if (data.dateCreate) {
                        data.dateCreate = new Date(data.dateCreate);
                    }
                    if (data.dateRequired) {
                        data.dateRequired = new Date(data.dateRequired);
                    }
                    this.ngbModalRef = this.unitReqInternalModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new UnitReqInternal();
                    this.ngbModalRef = this.unitReqInternalModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unitReqInternalModalRef(component: Component, unitReqInternal: UnitReqInternal): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unitReqInternal = unitReqInternal;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
