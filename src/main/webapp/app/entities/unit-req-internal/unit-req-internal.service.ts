import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { UnitReqInternal } from './unit-req-internal.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class UnitReqInternalService {

    protected resourceUrl = 'api/unit-req-internals';
    protected resourceSearchUrl = 'api/_search/unit-req-internals';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(unitReqInternal: UnitReqInternal): Observable<UnitReqInternal> {
        const copy = this.convert(unitReqInternal);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(unitReqInternal: UnitReqInternal): Observable<UnitReqInternal> {
        const copy = this.convert(unitReqInternal);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<UnitReqInternal> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(unitReqInternal: any): Observable<String> {
        const copy = this.convert(unitReqInternal);
        return this.http.post(this.resourceUrl + '/execute', copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
        if (entity.dateRequired) {
            entity.dateRequired = new Date(entity.dateRequired);
        }
    }

    protected convert(unitReqInternal: UnitReqInternal): UnitReqInternal {
        const copy: UnitReqInternal = Object.assign({}, unitReqInternal);

        // copy.dateCreate = this.dateUtils.toDate(unitReqInternal.dateCreate);

        // copy.dateRequired = this.dateUtils.toDate(unitReqInternal.dateRequired);
        return copy;
    }
}
