import { BaseEntity } from './../../shared';

export class UnitReqInternal implements BaseEntity {
    constructor(
        public id?: any,
        public idRequirement?: any,
        public description?: string,
        public qty?: number,
        public budget?: number,
        public dateCreate?: any,
        public dateRequired?: any,
        public branchId?: any,
        public motorId?: any,
        public orderItems?: any,
    ) {
    }
}
