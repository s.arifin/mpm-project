import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ServiceReminder} from './service-reminder.model';
import {ServiceReminderPopupService} from './service-reminder-popup.service';
import {ServiceReminderService} from './service-reminder.service';
import {ToasterService} from '../../shared';
import { DealerClaimType, DealerClaimTypeService } from '../dealer-claim-type';
import { DealerReminderType, DealerReminderTypeService } from '../dealer-reminder-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-service-reminder-dialog',
    templateUrl: './service-reminder-dialog.component.html'
})
export class ServiceReminderDialogComponent implements OnInit {

    serviceReminder: ServiceReminder;
    isSaving: boolean;

    dealerclaimtypes: DealerClaimType[];

    dealerremindertypes: DealerReminderType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected serviceReminderService: ServiceReminderService,
        protected dealerClaimTypeService: DealerClaimTypeService,
        protected dealerReminderTypeService: DealerReminderTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dealerClaimTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.dealerclaimtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.dealerReminderTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.dealerremindertypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.serviceReminder.idReminder !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceReminderService.update(this.serviceReminder));
        } else {
            this.subscribeToSaveResponse(
                this.serviceReminderService.create(this.serviceReminder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ServiceReminder>) {
        result.subscribe((res: ServiceReminder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ServiceReminder) {
        this.eventManager.broadcast({ name: 'serviceReminderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'serviceReminder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'serviceReminder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackDealerClaimTypeById(index: number, item: DealerClaimType) {
        return item.idClaimType;
    }

    trackDealerReminderTypeById(index: number, item: DealerReminderType) {
        return item.idReminderType;
    }
}

@Component({
    selector: 'jhi-service-reminder-popup',
    template: ''
})
export class ServiceReminderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected serviceReminderPopupService: ServiceReminderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.serviceReminderPopupService
                    .open(ServiceReminderDialogComponent as Component, params['id']);
            } else {
                this.serviceReminderPopupService
                    .open(ServiceReminderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
