import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ServiceReminder } from './service-reminder.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ServiceReminderService {
    protected itemValues: ServiceReminder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/service-reminders';
    protected resourceSearchUrl = 'api/_search/service-reminders';

    constructor(protected http: Http) { }

    create(serviceReminder: ServiceReminder): Observable<ServiceReminder> {
        const copy = this.convert(serviceReminder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(serviceReminder: ServiceReminder): Observable<ServiceReminder> {
        const copy = this.convert(serviceReminder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ServiceReminder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, serviceReminder: ServiceReminder): Observable<ServiceReminder> {
        const copy = this.convert(serviceReminder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, serviceReminders: ServiceReminder[]): Observable<ServiceReminder[]> {
        const copy = this.convertList(serviceReminders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(serviceReminder: ServiceReminder): ServiceReminder {
        if (serviceReminder === null || serviceReminder === {}) {
            return {};
        }
        // const copy: ServiceReminder = Object.assign({}, serviceReminder);
        const copy: ServiceReminder = JSON.parse(JSON.stringify(serviceReminder));
        return copy;
    }

    protected convertList(serviceReminders: ServiceReminder[]): ServiceReminder[] {
        const copy: ServiceReminder[] = serviceReminders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ServiceReminder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
