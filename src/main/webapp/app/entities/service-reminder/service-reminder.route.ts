import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ServiceReminderComponent } from './service-reminder.component';
import { ServiceReminderPopupComponent } from './service-reminder-dialog.component';

@Injectable()
export class ServiceReminderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReminder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const serviceReminderRoute: Routes = [
    {
        path: 'service-reminder',
        component: ServiceReminderComponent,
        resolve: {
            'pagingParams': ServiceReminderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.serviceReminder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const serviceReminderPopupRoute: Routes = [
    {
        path: 'service-reminder-new',
        component: ServiceReminderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.serviceReminder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-reminder/:id/edit',
        component: ServiceReminderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.serviceReminder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
