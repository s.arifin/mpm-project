import { BaseEntity } from './../../shared';

export class ServiceReminder implements BaseEntity {
    constructor(
        public id?: number,
        public idReminder?: number,
        public days?: number,
        public claimTypeId?: any,
        public reminderTypeId?: any,
    ) {
    }
}
