import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { VendorRelationship } from './vendor-relationship.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class VendorRelationshipService {
   private itemValues: VendorRelationship[];
   values: Subject<any> = new Subject<any>();

   private resourceUrl =  SERVER_API_URL + 'api/vendor-relationships';
   private resourceSearchUrl = SERVER_API_URL + 'api/_search/vendor-relationships';

   constructor(private http: Http, private dateUtils: JhiDateUtils) { }

   create(vendorRelationship: VendorRelationship): Observable<VendorRelationship> {
       const copy = this.convert(vendorRelationship);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(vendorRelationship: VendorRelationship): Observable<VendorRelationship> {
       const copy = this.convert(vendorRelationship);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<VendorRelationship> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: VendorRelationship, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: VendorRelationship[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   private convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to VendorRelationship.
    */
   private convertItemFromServer(json: any): VendorRelationship {
       const entity: VendorRelationship = Object.assign(new VendorRelationship(), json);
       if (entity.dateFrom) {
           entity.dateFrom = new Date(entity.dateFrom);
       }
       if (entity.dateThru) {
           entity.dateThru = new Date(entity.dateThru);
       }
       return entity;
   }

   /**
    * Convert a VendorRelationship to a JSON which can be sent to the server.
    */
   private convert(vendorRelationship: VendorRelationship): VendorRelationship {
       if (vendorRelationship === null || vendorRelationship === {}) {
           return {};
       }
       // const copy: VendorRelationship = Object.assign({}, vendorRelationship);
       const copy: VendorRelationship = JSON.parse(JSON.stringify(vendorRelationship));

       // copy.dateFrom = this.dateUtils.toDate(vendorRelationship.dateFrom);

       // copy.dateThru = this.dateUtils.toDate(vendorRelationship.dateThru);
       return copy;
   }

   private convertList(vendorRelationships: VendorRelationship[]): VendorRelationship[] {
       const copy: VendorRelationship[] = vendorRelationships;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: VendorRelationship[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
