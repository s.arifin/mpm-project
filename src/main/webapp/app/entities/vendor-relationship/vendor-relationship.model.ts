import { BaseEntity } from './../../shared';

export class VendorRelationship implements BaseEntity {
    constructor(
        public id?: any,
        public idPartyRelationship?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public statusTypeId?: any,
        public relationTypeId?: any,
        public internalId?: any,
        public vendorId?: any,
    ) {
    }
}
