import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { OrderPayment } from './order-payment.model';
import { OrderPaymentService } from './order-payment.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-order-payment-as-list',
    templateUrl: './order-payment-as-list.component.html'
})
export class OrderPaymentAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idOrders: any;
    @Input() idPayment: any;

    currentAccount: any;
    orderPayments: OrderPayment[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected orderPaymentService: OrderPaymentService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idOrderPayment';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.orderPaymentService.queryFilterBy({
            idOrders: this.idOrders,
            idPayment: this.idPayment,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/order-payment', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInOrderPayments();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idOrders']) {
            this.loadAll();
        }
        if (changes['idPayment']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: OrderPayment) {
        return item.idOrderPayment;
    }

    registerChangeInOrderPayments() {
        this.eventSubscriber = this.eventManager.subscribe('orderPaymentListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrderPayment') {
            result.push('idOrderPayment');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.orderPayments = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.orderPaymentService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.orderPaymentService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.orderPaymentService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrderPayment>) {
        result.subscribe((res: OrderPayment) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: OrderPayment) {
        this.eventManager.broadcast({ name: 'orderPaymentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orderPayment saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.orderPaymentService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'orderPaymentListModification',
                        content: 'Deleted an orderPayment'
                    });
                });
            }
        });
    }
}
