import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OrderPayment } from './order-payment.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OrderPaymentService {
   protected itemValues: OrderPayment[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/order-payments';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/order-payments';

   constructor(protected http: Http) { }

   create(orderPayment: OrderPayment): Observable<OrderPayment> {
       const copy = this.convert(orderPayment);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(orderPayment: OrderPayment): Observable<OrderPayment> {
       const copy = this.convert(orderPayment);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<OrderPayment> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: OrderPayment, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: OrderPayment[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to OrderPayment.
    */
   protected convertItemFromServer(json: any): OrderPayment {
       const entity: OrderPayment = Object.assign(new OrderPayment(), json);
       return entity;
   }

   /**
    * Convert a OrderPayment to a JSON which can be sent to the server.
    */
   protected convert(orderPayment: OrderPayment): OrderPayment {
       if (orderPayment === null || orderPayment === {}) {
           return {};
       }
       // const copy: OrderPayment = Object.assign({}, orderPayment);
       const copy: OrderPayment = JSON.parse(JSON.stringify(orderPayment));
       return copy;
   }

   protected convertList(orderPayments: OrderPayment[]): OrderPayment[] {
       const copy: OrderPayment[] = orderPayments;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: OrderPayment[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
