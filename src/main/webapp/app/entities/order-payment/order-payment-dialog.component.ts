import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OrderPayment } from './order-payment.model';
import { OrderPaymentPopupService } from './order-payment-popup.service';
import { OrderPaymentService } from './order-payment.service';
import { ToasterService } from '../../shared';
import { Orders, OrdersService } from '../orders';
import { Payment, PaymentService } from '../payment';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-order-payment-dialog',
    templateUrl: './order-payment-dialog.component.html'
})
export class OrderPaymentDialogComponent implements OnInit {

    orderPayment: OrderPayment;
    isSaving: boolean;
    idOrders: any;
    idPayment: any;

    orders: Orders[];

    payments: Payment[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected orderPaymentService: OrderPaymentService,
        protected ordersService: OrdersService,
        protected paymentService: PaymentService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.ordersService.query()
            .subscribe((res: ResponseWrapper) => { this.orders = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentService.query()
            .subscribe((res: ResponseWrapper) => { this.payments = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orderPayment.idOrderPayment !== undefined) {
            this.subscribeToSaveResponse(
                this.orderPaymentService.update(this.orderPayment));
        } else {
            this.subscribeToSaveResponse(
                this.orderPaymentService.create(this.orderPayment));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrderPayment>) {
        result.subscribe((res: OrderPayment) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: OrderPayment) {
        this.eventManager.broadcast({ name: 'orderPaymentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orderPayment saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'orderPayment Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrdersById(index: number, item: Orders) {
        return item.idOrder;
    }

    trackPaymentById(index: number, item: Payment) {
        return item.idPayment;
    }
}

@Component({
    selector: 'jhi-order-payment-popup',
    template: ''
})
export class OrderPaymentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected orderPaymentPopupService: OrderPaymentPopupService
    ) {}

    ngOnInit() {
        this.orderPaymentPopupService.idOrders = undefined;
        this.orderPaymentPopupService.idPayment = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderPaymentPopupService
                    .open(OrderPaymentDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.orderPaymentPopupService.parent = params['parent'];
                this.orderPaymentPopupService
                    .open(OrderPaymentDialogComponent as Component);
            } else {
                this.orderPaymentPopupService
                    .open(OrderPaymentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
