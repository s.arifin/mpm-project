import { BaseEntity } from './../../shared';

export class OrderPayment implements BaseEntity {
    constructor(
        public id?: any,
        public idOrderPayment?: any,
        public amount?: number,
        public ordersId?: any,
        public paymentId?: any,
    ) {
    }
}
