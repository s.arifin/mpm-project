import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OrderPayment } from './order-payment.model';
import { OrderPaymentService } from './order-payment.service';

@Injectable()
export class OrderPaymentPopupService {
    protected ngbModalRef: NgbModalRef;
    idOrders: any;
    idPayment: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected orderPaymentService: OrderPaymentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.orderPaymentService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.orderPaymentModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new OrderPayment();
                    data.ordersId = this.idOrders;
                    data.paymentId = this.idPayment;
                    this.ngbModalRef = this.orderPaymentModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    orderPaymentModalRef(component: Component, orderPayment: OrderPayment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderPayment = orderPayment;
        modalRef.componentInstance.idOrders = this.idOrders;
        modalRef.componentInstance.idPayment = this.idPayment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.orderPaymentLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    orderPaymentLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idOrders = this.idOrders;
        modalRef.componentInstance.idPayment = this.idPayment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
