import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    OrderPaymentService,
    OrderPaymentPopupService,
    OrderPaymentComponent,
    OrderPaymentDialogComponent,
    OrderPaymentPopupComponent,
    orderPaymentRoute,
    orderPaymentPopupRoute,
    OrderPaymentResolvePagingParams,
    OrderPaymentAsLovComponent,
    OrderPaymentLovPopupComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...orderPaymentRoute,
    ...orderPaymentPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        OrderPaymentComponent,
        OrderPaymentAsLovComponent,
        OrderPaymentLovPopupComponent,
    ],
    declarations: [
        OrderPaymentComponent,
        OrderPaymentDialogComponent,
        OrderPaymentPopupComponent,
        OrderPaymentAsLovComponent,
        OrderPaymentLovPopupComponent,
    ],
    entryComponents: [
        OrderPaymentComponent,
        OrderPaymentDialogComponent,
        OrderPaymentPopupComponent,
        OrderPaymentAsLovComponent,
        OrderPaymentLovPopupComponent,
    ],
    providers: [
        OrderPaymentService,
        OrderPaymentPopupService,
        OrderPaymentResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmOrderPaymentModule {}
