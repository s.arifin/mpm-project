import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ProductShipmentReceipt } from './product-shipment-receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductShipmentReceiptService {
    protected itemValues: ProductShipmentReceipt[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/product-shipment-receipts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-shipment-receipts';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(productShipmentReceipt: ProductShipmentReceipt): Observable<ProductShipmentReceipt> {
        const copy = this.convert(productShipmentReceipt);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(productShipmentReceipt: ProductShipmentReceipt): Observable<ProductShipmentReceipt> {
        const copy = this.convert(productShipmentReceipt);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ProductShipmentReceipt> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(productShipmentReceipt: ProductShipmentReceipt, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(productShipmentReceipt);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ProductShipmentReceipt> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ProductShipmentReceipt[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ProductShipmentReceipt.
     */
    protected convertItemFromServer(json: any): ProductShipmentReceipt {
        const entity: ProductShipmentReceipt = Object.assign(new ProductShipmentReceipt(), json);
        if (entity.dateReceipt) {
            entity.dateReceipt = new Date(entity.dateReceipt);
        }
        return entity;
    }

    /**
     * Convert a ProductShipmentReceipt to a JSON which can be sent to the server.
     */
    protected convert(productShipmentReceipt: ProductShipmentReceipt): ProductShipmentReceipt {
        if (productShipmentReceipt === null || productShipmentReceipt === {}) {
            return {};
        }
        // const copy: ProductShipmentReceipt = Object.assign({}, productShipmentReceipt);
        const copy: ProductShipmentReceipt = JSON.parse(JSON.stringify(productShipmentReceipt));

        // copy.dateReceipt = this.dateUtils.toDate(productShipmentReceipt.dateReceipt);
        return copy;
    }

    protected convertList(productShipmentReceipts: ProductShipmentReceipt[]): ProductShipmentReceipt[] {
        const copy: ProductShipmentReceipt[] = productShipmentReceipts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductShipmentReceipt[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
        this.itemValues = [];
    }
}
