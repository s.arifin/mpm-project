import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { ProductShipmentReceipt } from './product-shipment-receipt.model';
import { ProductShipmentReceiptService } from './product-shipment-receipt.service';

@Injectable()
export class ProductShipmentReceiptPopupService {
    protected ngbModalRef: NgbModalRef;
    idShipmentPackage: any;
    idShipmentItem: any;
    idOrderItem: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected productShipmentReceiptService: ProductShipmentReceiptService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.productShipmentReceiptService.find(id).subscribe((data) => {
                    // if (data.dateReceipt) {
                    //    data.dateReceipt = this.datePipe
                    //        .transform(data.dateReceipt, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.productShipmentReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new ProductShipmentReceipt();
                    data.shipmentPackageId = this.idShipmentPackage;
                    data.shipmentItemId = this.idShipmentItem;
                    data.orderItemId = this.idOrderItem;
                    this.ngbModalRef = this.productShipmentReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    productShipmentReceiptModalRef(component: Component, productShipmentReceipt: ProductShipmentReceipt): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.productShipmentReceipt = productShipmentReceipt;
        modalRef.componentInstance.idShipmentPackage = this.idShipmentPackage;
        modalRef.componentInstance.idShipmentItem = this.idShipmentItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.productShipmentReceiptLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    productShipmentReceiptLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
