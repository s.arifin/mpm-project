import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductShipmentReceipt } from './product-shipment-receipt.model';
import { ProductShipmentReceiptService } from './product-shipment-receipt.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { ShipmentPackage, ShipmentPackageService } from '../shipment-package';
import { ShipmentItem, ShipmentItemService } from '../shipment-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-shipment-receipt-edit',
    templateUrl: './product-shipment-receipt-edit.component.html'
})
export class ProductShipmentReceiptEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    productShipmentReceipt: ProductShipmentReceipt;
    isSaving: boolean;
    idReceipt: any;
    paramPage: number;
    routeId: number;

    shipmentpackages: ShipmentPackage[];

    shipmentitems: ShipmentItem[];

    orderitems: OrderItem[];

    constructor(
        protected alertService: JhiAlertService,
        protected productShipmentReceiptService: ProductShipmentReceiptService,
        protected shipmentPackageService: ShipmentPackageService,
        protected shipmentItemService: ShipmentItemService,
        protected orderItemService: OrderItemService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected reportUtilService: ReportUtilService,
        protected toaster: ToasterService
    ) {
        this.productShipmentReceipt = new ProductShipmentReceipt();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idReceipt = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.shipmentPackageService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentpackages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipmentItemService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.productShipmentReceiptService.find(this.idReceipt).subscribe((productShipmentReceipt) => {
            this.productShipmentReceipt = productShipmentReceipt;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['product-shipment-receipt', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.productShipmentReceipt.idReceipt !== undefined) {
            this.subscribeToSaveResponse(
                this.productShipmentReceiptService.update(this.productShipmentReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.productShipmentReceiptService.create(this.productShipmentReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductShipmentReceipt>) {
        result.subscribe((res: ProductShipmentReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProductShipmentReceipt) {
        this.eventManager.broadcast({ name: 'productShipmentReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productShipmentReceipt saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'productShipmentReceipt Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackShipmentPackageById(index: number, item: ShipmentPackage) {
        return item.idPackage;
    }

    trackShipmentItemById(index: number, item: ShipmentItem) {
        return item.idShipmentItem;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }
    print() {
        this.toaster.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/sample/pdf', {idOrder: '440550', idUser: 'ntung'});
    }

}
