import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductShipmentReceipt } from './product-shipment-receipt.model';
import { ProductShipmentReceiptPopupService } from './product-shipment-receipt-popup.service';
import { ProductShipmentReceiptService } from './product-shipment-receipt.service';
import { ToasterService } from '../../shared';
import { ShipmentPackage, ShipmentPackageService } from '../shipment-package';
import { ShipmentItem, ShipmentItemService } from '../shipment-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-shipment-receipt-dialog',
    templateUrl: './product-shipment-receipt-dialog.component.html'
})
export class ProductShipmentReceiptDialogComponent implements OnInit {

    productShipmentReceipt: ProductShipmentReceipt;
    isSaving: boolean;
    idShipmentPackage: any;
    idShipmentItem: any;
    idOrderItem: any;

    shipmentpackages: ShipmentPackage[];

    shipmentitems: ShipmentItem[];

    orderitems: OrderItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected productShipmentReceiptService: ProductShipmentReceiptService,
        protected shipmentPackageService: ShipmentPackageService,
        protected shipmentItemService: ShipmentItemService,
        protected orderItemService: OrderItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentPackageService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentpackages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipmentItemService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productShipmentReceipt.idReceipt !== undefined) {
            this.subscribeToSaveResponse(
                this.productShipmentReceiptService.update(this.productShipmentReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.productShipmentReceiptService.create(this.productShipmentReceipt));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductShipmentReceipt>) {
        result.subscribe((res: ProductShipmentReceipt) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductShipmentReceipt) {
        this.eventManager.broadcast({ name: 'productShipmentReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productShipmentReceipt saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productShipmentReceipt Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentPackageById(index: number, item: ShipmentPackage) {
        return item.idPackage;
    }

    trackShipmentItemById(index: number, item: ShipmentItem) {
        return item.idShipmentItem;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }
}

@Component({
    selector: 'jhi-product-shipment-receipt-popup',
    template: ''
})
export class ProductShipmentReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productShipmentReceiptPopupService: ProductShipmentReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productShipmentReceiptPopupService
                    .open(ProductShipmentReceiptDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.productShipmentReceiptPopupService.idShipmentPackage = params['parent'];
                this.productShipmentReceiptPopupService
                    .open(ProductShipmentReceiptDialogComponent as Component);
            } else {
                this.productShipmentReceiptPopupService
                    .open(ProductShipmentReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
