import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProductShipmentReceiptComponent } from './product-shipment-receipt.component';
import { ProductShipmentReceiptEditComponent } from './product-shipment-receipt-edit.component';
import { ProductShipmentReceiptPopupComponent } from './product-shipment-receipt-dialog.component';

@Injectable()
export class ProductShipmentReceiptResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReceipt,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productShipmentReceiptRoute: Routes = [
    {
        path: 'product-shipment-receipt',
        component: ProductShipmentReceiptComponent,
        resolve: {
            'pagingParams': ProductShipmentReceiptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productShipmentReceiptPopupRoute: Routes = [
    {
        path: 'product-shipment-receipt-popup-new-list/:parent',
        component: ProductShipmentReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-shipment-receipt-popup-new',
        component: ProductShipmentReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-shipment-receipt-new',
        component: ProductShipmentReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-shipment-receipt/:id/edit',
        component: ProductShipmentReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-shipment-receipt/:route/:page/:id/edit',
        component: ProductShipmentReceiptEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-shipment-receipt/:id/popup-edit',
        component: ProductShipmentReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
