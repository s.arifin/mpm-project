import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ProductShipmentReceipt } from './product-shipment-receipt.model';
import { ProductShipmentReceiptService } from './product-shipment-receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import * as XLSX from 'xlsx';

@Component({
    selector: 'jhi-product-shipment-receipt',
    templateUrl: './product-shipment-receipt.component.html'
})
export class ProductShipmentReceiptComponent implements OnInit, OnDestroy {

    currentAccount: any;
    productShipmentReceipts: ProductShipmentReceipt[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    itemsToUpload: any[];
    temporaryData: any;
    first: number;

    constructor(
        protected productShipmentReceiptService: ProductShipmentReceiptService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toasterService: ToasterService
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            this.productShipmentReceiptService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.productShipmentReceiptService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/product-shipment-receipt'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/product-shipment-receipt', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/product-shipment-receipt', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProductShipmentReceipts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ProductShipmentReceipt) {
        return item.idReceipt;
    }

    registerChangeInProductShipmentReceipts() {
        this.eventSubscriber = this.eventManager.subscribe('productShipmentReceiptListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idReceipt') {
            result.push('idReceipt');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.productShipmentReceipts = data;
    }

    protected onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.productShipmentReceiptService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.productShipmentReceiptService.update(event.data)
                .subscribe((res: ProductShipmentReceipt) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.productShipmentReceiptService.create(event.data)
                .subscribe((res: ProductShipmentReceipt) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: ProductShipmentReceipt) {
        this.toasterService.showToaster('info', 'ProductShipmentReceipt Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.productShipmentReceiptService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'productShipmentReceiptListModification',
                        content: 'Deleted an productShipmentReceipt'
                    });
                });
            }
        });
    }

    process() {
        this.productShipmentReceiptService.process({}).subscribe((r) => {
            console.log('result', r);
            this.toasterService.showToaster('info', 'Data Processed', 'processed.....');
        });
    }

    uploadData() {
        if (!this.temporaryData || this.temporaryData === null) {
            return;
        }
        this.temporaryData.forEach((data) => {
            // this.itemsToUpload = [...this.itemsToUpload, column];
            console.log(data);
        });
        this.temporaryData = null;
    }

    onFileSelect(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbooka */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            /* save data */
            this.temporaryData = (XLSX.utils.sheet_to_json(ws, { header: ['column'], range: 1 }));
            this.uploadData();
        };
        reader.readAsBinaryString(target.files[0]);
    }

}
