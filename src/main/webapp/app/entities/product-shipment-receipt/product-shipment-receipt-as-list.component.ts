import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ProductShipmentReceipt } from './product-shipment-receipt.model';
import { ProductShipmentReceiptService } from './product-shipment-receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { OrderItem, OrderItemService } from '../order-item';

@Component({
    selector: 'jhi-product-shipment-receipt-as-list',
    templateUrl: './product-shipment-receipt-as-list.component.html'
})
export class ProductShipmentReceiptAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idShipmentPackage: any;
    @Input() idShipmentItem: any;
    @Input() idOrderItem: any;

    currentAccount: any;
    productShipmentReceipts: ProductShipmentReceipt[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    ordItemSubs: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected productShipmentReceiptService: ProductShipmentReceiptService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected orderItemService: OrderItemService
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idReceipt';
        this.reverse = 'asc';
    }

    loadAll() {
        this.productShipmentReceiptService.queryFilterBy({
            idShipmentPackage: this.idShipmentPackage,
            idShipmentItem: this.idShipmentItem,
            idOrderItem: this.idOrderItem,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/product-shipment-receipt', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProductShipmentReceipts();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idShipmentPackage']) {
            this.loadAll();
        }
        if (changes['idShipmentItem']) {
            this.loadAll();
        }
        if (changes['idOrderItem']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.ordItemSubs.unsubscribe();
    }

    trackId(index: number, item: ProductShipmentReceipt) {
        return item.idReceipt;
    }

    registerChangeInProductShipmentReceipts() {
        this.eventSubscriber = this.eventManager.subscribe('productShipmentReceiptListModification', (response) => this.loadAll());
        this.ordItemSubs = this.orderItemService.values.subscribe((items: OrderItem[]) => {
            if (items.length > 0 ) {
                new Promise((resolve) => {
                    for (const item of items) {
                        const data: ProductShipmentReceipt = new ProductShipmentReceipt();
                        data.shipmentPackageId = this.idShipmentPackage;
                        data.orderItemId = item.idOrderItem;
                        data.idProduct = item.idProduct;
                        data.itemDescription = item.itemDescription;
                        data.qtyAccept = item.qty - item.qtyReceipt;
                        data.qtyReject = 0;
                        this.productShipmentReceiptService.create(data).subscribe();
                    };
                    setTimeout(() => {
                        resolve('done');
                    }, 500);
                })
                .then(() => {
                    this.eventManager.broadcast({
                        name: 'productShipmentReceiptListModification', content: 'Update Product Shipment Receipt'
                    });
                });
            }
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idReceipt') {
            result.push('idReceipt');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.productShipmentReceipts = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.productShipmentReceiptService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.productShipmentReceiptService.update(event.data)
                .subscribe((res: ProductShipmentReceipt) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.productShipmentReceiptService.create(event.data)
                .subscribe((res: ProductShipmentReceipt) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: ProductShipmentReceipt) {
        this.toasterService.showToaster('info', 'ProductShipmentReceipt Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.productShipmentReceiptService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'productShipmentReceiptListModification',
                        content: 'Deleted an productShipmentReceipt'
                    });
                });
            }
        });
    }
}
