import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitOrderItem, OrderItem } from './order-item.model';
import { OrderItemService } from './order-item.service';
import { GoodService } from '../good';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts/loading/loading.service';

// nuse
import { AxPosting, AxPostingService } from '../axposting';
import { AxPostingLine } from '../axposting-line';
import { BillingDetailC, BillingDetailCService } from '../billing-detail-c';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-order-item-unit-as-list',
    templateUrl: './order-item-unit-as-list.component.html'
})
export class OrderItemUnitAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idOrders: any;

    currentAccount: any;
    orderItems: UnitOrderItem[];
    orderItem: OrderItem;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    goodSubs: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    currentSearch: string;
    // nuse
    billingDetail: BillingDetailC;
    axPosting: AxPosting;
    axPostingLine: AxPostingLine;
    VendorNum: string;
    buttonDisable: Boolean;
    // isFiltered: boolean;
    // isPart: boolean;

    constructor(
        protected orderItemService: OrderItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        protected goodService: GoodService,
        protected loadingService: LoadingService,
        // nuse
        protected billingDetailCService: BillingDetailCService,
        protected axPostingService: AxPostingService,
        protected datePipe: DatePipe,
        protected commonUtilService: CommonUtilService,
    ) {
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.itemsPerPage = 20;
        this.page = 1;
        this.predicate = 'orders.orderNumber';
        this.reverse = 'desc';
        this.first = 0;
        this.buttonDisable = false;
        this.orderItem = new OrderItem();
        // this.isPart = true;
    }
    loadAll() {
        this.loadingService.loadingStart();
        try {
            if (this.currentSearch) {
                console.log('SEARCH');
                this.orderItemService.searchUnit({
                    idInternal: this.principal.getIdInternal(),
                    page: this.page - 1,
                    query: this.currentSearch,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
                return;
            }
            this.orderItemService.queryUnitFilterBy({
                idInternal: this.principal.getIdInternal(),
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        finally {
            this.loadingService.loadingStop();
        }
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/vehicle-sales-billing', {
            page: this.page,
            // sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/unit-order-item', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInOrderItems();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idOrders']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.goodSubs.unsubscribe();
    }

    trackId(index: number, item: UnitOrderItem) {
        return item.idOrderItem;
    }

    registerChangeInOrderItems() {
        this.eventSubscriber = this.eventManager.subscribe('orderItemListModification', (response) => this.loadAll());
        this.goodSubs = this.goodService.values.subscribe((items) => {
            new Promise((resolve) => {
                const values: UnitOrderItem[] = [];
                for (const item of items) {
                    const data: UnitOrderItem = new UnitOrderItem();
                    data.ordersId = this.idOrders;
                    data.idProduct = item.idProduct;
                    data.itemDescription = item.name;
                    data.qty = 1;
                    values.push(data);
                };
                this.orderItemService.executeListProcess(values, {command: 'save'}).subscribe(() => {
                    resolve('done');
                });
            })
            .then(() => {
                this.eventManager.broadcast({
                    name: 'orderItemListModification', content: 'Update Order items'
                });
            });
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrderItem') {
            result.push('idOrderItem');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.orderItems = data;
        this.loadingService.loadingStop();
        this.buttonDisable = false;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
        this.buttonDisable = false;
    }

    executeProcess(item) {
        this.orderItemService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        console.log('load lazy dunkk from vsb-->');
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.idOrderItem !== undefined) {
            this.orderItemService.update(event.data).subscribe(() => {
                this.toaster.showToaster('info', 'Save', 'orderItem saved !');
            });
        } else {
            this.orderItemService.create(event.data).subscribe(() => {
                this.toaster.showToaster('info', 'Save', 'orderItem saved !');
            });
        }
        this.eventManager.broadcast({
            name: 'orderItemListModification', content: 'Update Order items'
        });
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.orderItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'orderItemListModification',
                        content: 'Deleted an orderItem'
                    });
                });
            }
        });
    }

    generateVsb(value: string) {
        this.loadingService.loadingStart();
        this.buttonDisable = true;
        try {
            this.orderItemService.process(
                {command: 'buildBilling', idOrderItem: value}
            ).subscribe(
                (r) => {
                    this.loadingService.loadingStart();
                    this.billingDetailCService.findbyIdOrdIte(value).subscribe(
                        (resBilDetail) => {
                            this.billingDetail = resBilDetail;
                            console.log('idOrderItem :', value);
                            console.log('billing detail :', this.billingDetail);
                            const today = new Date();
                            const myaxPostingLine = [];
                            this.axPosting = new AxPosting();

                            this.axPosting.AutoPosting = 'FALSE';
                            this.axPosting.AutoSettlement = 'FALSE';
                            this.axPosting.DataArea = this.billingDetail.idMSO;
                            this.axPosting.Guid = this.billingDetail.idOrdIte;
                            this.axPosting.JournalName = 'VSO-Inv';
                            this.axPosting.Name = 'Sales Unit ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                            this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPosting.TransactionType = 'Unit';

                            // debit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                            this.axPostingLine.AccountType = 'Cust';
                            if ( this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                                this.axPostingLine.AmountCurDebit = this.billingDetail.creditDP.toString();
                            } else {
                                this.axPostingLine.AmountCurDebit = this.billingDetail.arAmount.toString();
                            }
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'AR Cust '  + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            if (this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode != null ) {
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.finComParty;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.arAmount.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'AR Finco ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLine.push(this.axPostingLine);
                            }

                            if (this.billingDetail.discount > 0) {
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '421001';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.discount.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'DISC ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                                this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                                this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLine.push(this.axPostingLine);
                            }

                            // credit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '411001';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.AmountCurCredit = this.billingDetail.axUnitPrice.toString();
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'SALES ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                            this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                            this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '218901';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.AmountCurCredit = this.billingDetail.axPPN.toString();
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'PPN ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = this.billingDetail.vendorCode;
                            this.axPostingLine.AccountType = 'Vend';
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.AmountCurCredit = this.billingDetail.axBBN.toString();
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'BBN ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = '000';
                            this.axPostingLine.Dimension5 = '00';
                            this.axPostingLine.Dimension6 = '000';
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLine.push(this.axPostingLine);

                            this.axPosting.LedgerJournalLine = myaxPostingLine;
                            console.log('axposting : ', this.axPosting);
                            this.axPostingService.send(this.axPosting).subscribe(
                                (resaxPosting: ResponseWrapper) => {
                                    console.log('Success : ', resaxPosting.json.Message);
                                },
                                (resaxPosting: ResponseWrapper) => {
                                    this.onError(resaxPosting.json);
                                    console.log('error ax posting : ', resaxPosting.json.Message);
                                }
                            );

                            const myaxPostingLineOther = [];
                            this.axPosting = new AxPosting();

                            this.axPosting.AutoPosting = 'FALSE';
                            this.axPosting.AutoSettlement = 'FALSE';
                            this.axPosting.DataArea = this.billingDetail.idMSO;
                            this.axPosting.Guid = this.billingDetail.idOrdIte;
                            this.axPosting.JournalName = 'VSO-InvIssue';
                            this.axPosting.Name = 'Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                            this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPosting.TransactionType = 'Unit';

                            // credit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '114101';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = '0';
                            this.axPostingLine.AmountCurCredit = this.billingDetail.BasePrice.toString();
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                            this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                            this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLineOther.push(this.axPostingLine);

                            // debit
                            this.axPostingLine = new AxPostingLine();
                            this.axPostingLine.AccountNum = '511001';
                            this.axPostingLine.AccountType = 'Ledger';
                            this.axPostingLine.AmountCurDebit = this.billingDetail.BasePrice.toString();
                            this.axPostingLine.AmountCurCredit = '0';
                            this.axPostingLine.Company = this.billingDetail.idMSO;
                            this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                            this.axPostingLine.Description = 'Issue for ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.TypeCode + ' ' + this.billingDetail.noKa;
                            this.axPostingLine.Dimension1 = '1014';
                            this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                            this.axPostingLine.Dimension3 = '000';
                            this.axPostingLine.Dimension4 = this.billingDetail.CategoryType;
                            this.axPostingLine.Dimension5 = this.billingDetail.SegmentType;
                            this.axPostingLine.Dimension6 = this.billingDetail.TypeCode;
                            this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                            this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                            this.axPostingLine.Payment = '';

                            myaxPostingLineOther.push(this.axPostingLine);

                            this.axPosting.LedgerJournalLine = myaxPostingLineOther;
                            console.log('axpostingOther : ', this.axPosting);
                            this.axPostingService.send(this.axPosting).subscribe(
                                (resaxPostingNew: ResponseWrapper) => {
                                    console.log('Success : ', resaxPostingNew.json.Message);
                                },
                                (resaxPostingNew: ResponseWrapper) => {
                                    this.onError(resaxPostingNew.json);
                                    console.log('error ax posting : ', resaxPostingNew.json.Message);
                                }
                            );

                            const myaxPostingLineOther2 = [];
                            this.axPosting = new AxPosting();

                            if (this.billingDetail.arTitipan > 0) {
                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = this.billingDetail.idOrdIte;
                                this.axPosting.JournalName = 'AR-PayUnit';
                                this.axPosting.Name = 'Pelunasan ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.idCustomer;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.arTitipan.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Pelunasan ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther2.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.cashAccNum;
                                this.axPostingLine.AccountType = 'Bank';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.arTitipan.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'Pelunasan ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther2.push(this.axPostingLine);

                                this.axPosting.LedgerJournalLine = myaxPostingLineOther2;
                                console.log('axpostingOther : ', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPostingNew: ResponseWrapper) => {
                                        console.log('Success : ', resaxPostingNew.json.Message);
                                    },
                                    (resaxPostingNew: ResponseWrapper) => {
                                        this.onError(resaxPostingNew.json);
                                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                                    }
                                );
                            }

                            if (this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode !== null && this.billingDetail.totalFinCom > 0 ) {
                                const myaxPostingLineOther3 = [];
                                this.axPosting = new AxPosting();

                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = this.billingDetail.idOrdIte;
                                this.axPosting.JournalName = 'AR-InvRefLeasing';
                                this.axPosting.Name = 'AR Refund ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.finComRefCode;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.totalFinCom.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'AR Refund ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther3.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '411004';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.subsFinCom.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Rev fr Finco ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther3.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = '218905';
                                this.axPostingLine.AccountType = 'Ledger';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.ppnFinCom.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'PPN Keluaran ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther3.push(this.axPostingLine);

                                this.axPosting.LedgerJournalLine = myaxPostingLineOther3;
                                console.log('axpostingOther : ', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPostingNew: ResponseWrapper) => {
                                        console.log('Success : ', resaxPostingNew.json.Message);
                                    },
                                    (resaxPostingNew: ResponseWrapper) => {
                                        this.onError(resaxPostingNew.json);
                                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                                    }
                                );
                            }

                            if (this.billingDetail.finComRefCode !== '' && this.billingDetail.finComRefCode !== null ) {
                                const myaxPostingLineOther5 = [];
                                this.axPosting = new AxPosting();

                                this.axPosting.AutoPosting = 'FALSE';
                                this.axPosting.AutoSettlement = 'FALSE';
                                this.axPosting.DataArea = this.billingDetail.idMSO;
                                this.axPosting.Guid = this.billingDetail.idOrdIte;
                                this.axPosting.JournalName = 'AR-PayUnit';
                                this.axPosting.Name = 'Pelunasan LS ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName + ' ' + this.billingDetail.noKa;
                                this.axPosting.TransDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPosting.TransactionType = 'Unit';

                                // credit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.finComParty;
                                this.axPostingLine.AccountType = 'Cust';
                                this.axPostingLine.AmountCurDebit = '0';
                                this.axPostingLine.AmountCurCredit = this.billingDetail.arAmount.toString();
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = this.billingDetail.orderNumber;
                                this.axPostingLine.Description = 'Pelunasan LS ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther5.push(this.axPostingLine);

                                // debit
                                this.axPostingLine = new AxPostingLine();
                                this.axPostingLine.AccountNum = this.billingDetail.bankCashAccNum;
                                this.axPostingLine.AccountType = 'Bank';
                                this.axPostingLine.AmountCurDebit = this.billingDetail.arAmount.toString();
                                this.axPostingLine.AmountCurCredit = '0';
                                this.axPostingLine.Company = this.billingDetail.idMSO;
                                this.axPostingLine.DMSNum = null;
                                this.axPostingLine.Description = 'Pelunasan LS ' + this.billingDetail.orderNumber + ' ' + this.billingDetail.customerName;
                                this.axPostingLine.Dimension1 = '1014';
                                this.axPostingLine.Dimension2 = this.billingDetail.idMSO;
                                this.axPostingLine.Dimension3 = '000';
                                this.axPostingLine.Dimension4 = '000';
                                this.axPostingLine.Dimension5 = '00';
                                this.axPostingLine.Dimension6 = '000';
                                this.axPostingLine.DueDate = this.datePipe.transform(today, 'MM/dd/yyyy');
                                this.axPostingLine.Invoice = this.billingDetail.billingNumber;
                                this.axPostingLine.Payment = '';

                                myaxPostingLineOther5.push(this.axPostingLine);
                                this.axPosting.LedgerJournalLine = myaxPostingLineOther5;
                                console.log('axpostingOther : ', this.axPosting);
                                this.axPostingService.send(this.axPosting).subscribe(
                                    (resaxPostingNew: ResponseWrapper) => {
                                        console.log('Success : ', resaxPostingNew.json.Message);
                                    },
                                    (resaxPostingNew: ResponseWrapper) => {
                                        this.onError(resaxPostingNew.json);
                                        console.log('error ax posting : ', resaxPostingNew.json.Message);
                                    }
                                );
                            }
                            this.loadingService.loadingStop();
                        }
                    );
                this.toaster.showToaster('info', 'Data Processed', 'processed.....');
                this.eventManager.broadcast({
                    name: 'orderItemListModification',
                    content: 'Process Data..'
                });
            this.loadAll();
            },
        (err) => {
            let errorr: Response;
            errorr = err.json;
            this.onError(err.json),
            this.commonUtilService.showError(err);
        });
        } finally {
            this.loadingService.loadingStop();
        }
    }

    buildReindex() {
        this.orderItemService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toaster.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
