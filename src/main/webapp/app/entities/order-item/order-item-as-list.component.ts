import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { OrderItem } from './order-item.model';
import { OrderItemService } from './order-item.service';
import { Good, GoodService } from '../good';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-order-item-as-list',
    templateUrl: './order-item-as-list.component.html'
})
export class OrderItemAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idOrders: any;

    currentAccount: any;
    orderItems: OrderItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    goodSubs: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    isPart: boolean;

    constructor(
        protected orderItemService: OrderItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService,
        protected goodService: GoodService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idOrderItem';
        this.reverse = 'asc';
        this.first = 0;
        this.isPart = true;
    }

    loadAll() {
        this.orderItemService.queryFilterBy({
            idOrders: this.idOrders,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/order-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInOrderItems();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idOrders']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.goodSubs.unsubscribe();
    }

    trackId(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    registerChangeInOrderItems() {
        this.eventSubscriber = this.eventManager.subscribe('orderItemListModification', (response) => this.loadAll());
        this.goodSubs = this.goodService.values.subscribe((items) => {
            new Promise((resolve) => {
                const values: OrderItem[] = [];
                for (const item of items) {
                    const data: OrderItem = new OrderItem();
                    data.ordersId = this.idOrders;
                    data.idProduct = item.idProduct;
                    data.itemDescription = item.name;
                    data.qty = 1;
                    values.push(data);
                };
                this.orderItemService.executeListProcess(values, {command: 'save'}).subscribe(() => {
                    resolve('done');
                });
            })
            .then(() => {
                this.eventManager.broadcast({
                    name: 'orderItemListModification', content: 'Update Order items'
                });
            });
        });
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idOrderItem') {
            result.push('idOrderItem');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.orderItems = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.orderItemService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.idOrderItem !== undefined) {
            this.orderItemService.update(event.data).subscribe(() => {
                this.toaster.showToaster('info', 'Save', 'orderItem saved !');
            });
        } else {
            this.orderItemService.create(event.data).subscribe(() => {
                this.toaster.showToaster('info', 'Save', 'orderItem saved !');
            });
        }
        this.eventManager.broadcast({
            name: 'orderItemListModification', content: 'Update Order items'
        });
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.orderItemService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'orderItemListModification',
                        content: 'Deleted an orderItem'
                    });
                });
            }
        });
    }
}
