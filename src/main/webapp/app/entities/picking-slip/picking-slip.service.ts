import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { PickingSlip } from './picking-slip.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class PickingSlipService {
    protected itemValues: PickingSlip[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/picking-slips';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/picking-slips';
    protected resourceCUrl2 = process.env.API_C_URL + '/api/picking-slips';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(pickingSlip: PickingSlip): Observable<PickingSlip> {
        const copy = this.convert(pickingSlip);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(pickingSlip: PickingSlip): Observable<PickingSlip> {
        const copy = this.convert(pickingSlip);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateC(pickingSlip: PickingSlip): Observable<PickingSlip> {
        const copy = this.convert(pickingSlip);
        return this.http.put(this.resourceCUrl2, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PickingSlip> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findC(id: any): Observable<PickingSlip> {
        return this.http.get(`${this.resourceCUrl2}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        if (options) { options.params.set('idstatustype', req.idstatustype); }
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(pickingSlip: PickingSlip, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(pickingSlip);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, pickingSlip: PickingSlip): Observable<PickingSlip> {
        const copy = this.convert(pickingSlip);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, pickingSlips: PickingSlip[]): Observable<PickingSlip[]> {
        const copy = this.convertList(pickingSlips);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateCreate) {
            entity.dateCreate = new Date(entity.dateCreate);
        }
    }

    protected convert(pickingSlip: PickingSlip): PickingSlip {
        if (pickingSlip === null || pickingSlip === {}) {
            return {};
        }
        // const copy: PickingSlip = Object.assign({}, pickingSlip);
        const copy: PickingSlip = JSON.parse(JSON.stringify(pickingSlip));

        // copy.dateCreate = this.dateUtils.toDate(pickingSlip.dateCreate);
        return copy;
    }

    protected convertList(pickingSlips: PickingSlip[]): PickingSlip[] {
        const copy: PickingSlip[] = pickingSlips;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PickingSlip[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
