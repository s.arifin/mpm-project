import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PickingSlip} from './picking-slip.model';
import {PickingSlipPopupService} from './picking-slip-popup.service';
import {PickingSlipService} from './picking-slip.service';
import {ToasterService} from '../../shared';
import { ShipTo, ShipToService } from '../ship-to';
import { InventoryItem, InventoryItemService } from '../inventory-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-picking-slip-dialog',
    templateUrl: './picking-slip-dialog.component.html'
})
export class PickingSlipDialogComponent implements OnInit {

    pickingSlip: PickingSlip;
    isSaving: boolean;
    selectMatprom1: any;
    selectMatprom2: any;

    shiptos: ShipTo[];

    inventoryitems: InventoryItem[];

    orderitems: OrderItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected pickingSlipService: PickingSlipService,
        protected shipToService: ShipToService,
        protected inventoryItemService: InventoryItemService,
        protected orderItemService: OrderItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; console.log('this.shiptos', this.shiptos); }, (res: ResponseWrapper) => this.onError(res.json));
        // this.inventoryItemService.query()
        //     .subscribe((res: ResponseWrapper) => { this.inventoryitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pickingSlip.idSlip !== undefined) {
            this.subscribeToSaveResponse(
                this.pickingSlipService.update(this.pickingSlip));
        } else {
            this.subscribeToSaveResponse(
                this.pickingSlipService.create(this.pickingSlip));
        }
    }

    promat() {
        if (this.pickingSlip.promat1 === false) {
            this.pickingSlip.qty = 0;
        }
    }

    protected subscribeToSaveResponse(result: Observable<PickingSlip>) {
        result.subscribe((res: PickingSlip) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PickingSlip) {
        this.eventManager.broadcast({ name: 'pickingSlipListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'pickingSlip saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'pickingSlip Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackInventoryItemById(index: number, item: InventoryItem) {
        return item.idInventoryItem;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }
}

@Component({
    selector: 'jhi-picking-slip-popup',
    template: ''
})
export class PickingSlipPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected pickingSlipPopupService: PickingSlipPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pickingSlipPopupService
                    .open(PickingSlipDialogComponent as Component, params['id']);
            } else {
                this.pickingSlipPopupService
                    .open(PickingSlipDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
