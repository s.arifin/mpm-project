import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PickingSlipComponent } from './picking-slip.component';
import { PickingSlipPopupComponent } from './picking-slip-dialog.component';

@Injectable()
export class PickingSlipResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSlip,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const pickingSlipRoute: Routes = [
    {
        path: 'picking-slip',
        component: PickingSlipComponent,
        resolve: {
            'pagingParams': PickingSlipResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pickingSlip.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pickingSlipPopupRoute: Routes = [
    {
        path: 'picking-slip-new',
        component: PickingSlipPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pickingSlip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'picking-slip/:id/edit',
        component: PickingSlipPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.pickingSlip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
