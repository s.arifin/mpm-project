import { BaseEntity } from './../../shared';

export class PickingSlip implements BaseEntity {
    constructor(
        public id?: any,
        public idSlip?: any,
        public dateCreate?: Date,
        public shipToId?: any,
        public inventoryItemId?: any,
        public orderItemId?: any,
        public idInventoryMovementType?: number,
        public refferenceNumber?: string,
        public qty?: number,
        public acc1?: boolean,
        // public qtyAcc1?: any,
        public acc2?: boolean,
        // public qtyAcc2?: any,
        public acc3?: boolean,
        // public qtyAcc3?: any,
        public acc4?: boolean,
        // public qtyAcc4?: any,
        public acc5?: boolean,
        // public qtyAcc5?: any,
        public acc6?: boolean,
        // public qtyAcc6?: any,
        public acc7?: boolean,

        public acc8?: boolean,
        public acctambah1?: boolean,
        public acctambah2?: boolean,
        // public qtyAcc7?: any,
        public promat1?: boolean,
        // public qtyPromat1?: any,
        public promat2?: boolean,
        // public qtyPromat2?: any,
        public mechanicinternal?: any,
        public mechanicexternal?: any,
        public idproduct?: any,
        public color?: any,
        public idframe?: any,
        public idmachine?: any,
        public location?: any,
        public idfeature?: any,
        public yeaAssembly?: any,
        public idFacility?: any
    ) {
    }
}
