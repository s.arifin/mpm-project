import { BaseEntity } from './../../shared';

export class Rules implements BaseEntity {
    constructor(
        public id?: any,
        public idRule?: number,
        public idRuleType?: number,
        public idInternal?: string,
        public seqnum?: number,
        public description?: string,
        public dateFrom?: any,
        public dateThru?: any,
    ) {

    }
}
