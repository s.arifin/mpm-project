import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Disbursement } from './disbursement.model';
import { DisbursementService } from './disbursement.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { PaymentType, PaymentTypeService } from '../payment-type';
import { PaymentMethod, PaymentMethodService } from '../payment-method';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-disbursement-edit',
   templateUrl: './disbursement-edit.component.html'
})
export class DisbursementEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   disbursement: Disbursement;
   isSaving: boolean;
   idPayment: any;
   paramPage: number;
   routeId: number;

   paymenttypes: PaymentType[];

   paymentmethods: PaymentMethod[];

   internals: Internal[];

   billtos: BillTo[];

   constructor(
       protected alertService: JhiAlertService,
       protected disbursementService: DisbursementService,
       protected paymentTypeService: PaymentTypeService,
       protected paymentMethodService: PaymentMethodService,
       protected internalService: InternalService,
       protected billToService: BillToService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected reportUtilService: ReportUtilService,
       protected toaster: ToasterService
   ) {
       this.disbursement = new Disbursement();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idPayment = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.paymentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.paymenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.paymentMethodService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentmethods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.disbursementService.find(this.idPayment).subscribe((disbursement) => {
           this.disbursement = disbursement;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['disbursement', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.disbursement.idPayment !== undefined) {
           this.subscribeToSaveResponse(
               this.disbursementService.update(this.disbursement));
       } else {
           this.subscribeToSaveResponse(
               this.disbursementService.create(this.disbursement));
       }
   }

   protected subscribeToSaveResponse(result: Observable<Disbursement>) {
       result.subscribe((res: Disbursement) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: Disbursement) {
       this.eventManager.broadcast({ name: 'disbursementListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'disbursement saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'disbursement Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackPaymentTypeById(index: number, item: PaymentType) {
       return item.idPaymentType;
   }

   trackPaymentMethodById(index: number, item: PaymentMethod) {
       return item.idPaymentMethod;
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }

   trackBillToById(index: number, item: BillTo) {
       return item.idBillTo;
   }
   print() {
       this.toaster.showToaster('info', 'Print Data', 'Printing.....');
       this.reportUtilService.viewFile('/api/report/sample/pdf');
   }

}
