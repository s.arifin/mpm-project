import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Disbursement } from './disbursement.model';
import { DisbursementService } from './disbursement.service';

@Injectable()
export class DisbursementPopupService {
    protected ngbModalRef: NgbModalRef;
    idPaymentType: any;
    idMethod: any;
    idInternal: any;
    idPaidTo: any;
    idPaidFrom: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected disbursementService: DisbursementService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.disbursementService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.disbursementModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Disbursement();
                    data.paymentTypeId = this.idPaymentType;
                    data.methodId = this.idMethod;
                    data.internalId = this.idInternal;
                    data.paidToId = this.idPaidTo;
                    data.paidFromId = this.idPaidFrom;
                    this.ngbModalRef = this.disbursementModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    disbursementModalRef(component: Component, disbursement: Disbursement): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.disbursement = disbursement;
        modalRef.componentInstance.idPaymentType = this.idPaymentType;
        modalRef.componentInstance.idMethod = this.idMethod;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPaidTo = this.idPaidTo;
        modalRef.componentInstance.idPaidFrom = this.idPaidFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.disbursementLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    disbursementLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idPaymentType = this.idPaymentType;
        modalRef.componentInstance.idMethod = this.idMethod;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPaidTo = this.idPaidTo;
        modalRef.componentInstance.idPaidFrom = this.idPaidFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
