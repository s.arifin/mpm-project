import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Disbursement } from './disbursement.model';
import { DisbursementPopupService } from './disbursement-popup.service';
import { DisbursementService } from './disbursement.service';
import { ToasterService } from '../../shared';
import { PaymentType, PaymentTypeService } from '../payment-type';
import { PaymentMethod, PaymentMethodService } from '../payment-method';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-disbursement-dialog',
    templateUrl: './disbursement-dialog.component.html'
})
export class DisbursementDialogComponent implements OnInit {

    disbursement: Disbursement;
    isSaving: boolean;
    idPaymentType: any;
    idMethod: any;
    idInternal: any;
    idPaidTo: any;
    idPaidFrom: any;

    paymenttypes: PaymentType[];

    paymentmethods: PaymentMethod[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected disbursementService: DisbursementService,
        protected paymentTypeService: PaymentTypeService,
        protected paymentMethodService: PaymentMethodService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.paymenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.paymentMethodService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentmethods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.disbursement.idPayment !== undefined) {
            this.subscribeToSaveResponse(
                this.disbursementService.update(this.disbursement));
        } else {
            this.subscribeToSaveResponse(
                this.disbursementService.create(this.disbursement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Disbursement>) {
        result.subscribe((res: Disbursement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Disbursement) {
        this.eventManager.broadcast({ name: 'disbursementListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'disbursement saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'disbursement Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPaymentTypeById(index: number, item: PaymentType) {
        return item.idPaymentType;
    }

    trackPaymentMethodById(index: number, item: PaymentMethod) {
        return item.idPaymentMethod;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-disbursement-popup',
    template: ''
})
export class DisbursementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected disbursementPopupService: DisbursementPopupService
    ) {}

    ngOnInit() {
        this.disbursementPopupService.idPaymentType = undefined;
        this.disbursementPopupService.idMethod = undefined;
        this.disbursementPopupService.idInternal = undefined;
        this.disbursementPopupService.idPaidTo = undefined;
        this.disbursementPopupService.idPaidFrom = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.disbursementPopupService
                    .open(DisbursementDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.disbursementPopupService.parent = params['parent'];
                this.disbursementPopupService
                    .open(DisbursementDialogComponent as Component);
            } else {
                this.disbursementPopupService
                    .open(DisbursementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
