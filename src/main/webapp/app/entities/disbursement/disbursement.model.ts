import { BaseEntity } from './../../shared';

export class Disbursement implements BaseEntity {
    constructor(
        public id?: any,
        public idPayment?: any,
        public paymentNumber?: string,
        public refferenceNumber?: string,
        public dateCreate?: any,
        public amount?: number,
        public details?: any,
        public paymentTypeId?: any,
        public methodId?: any,
        public internalId?: any,
        public paidToId?: any,
        public paidFromId?: any,
    ) {
    }
}
