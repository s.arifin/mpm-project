import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { DisbursementComponent } from './disbursement.component';
import { DisbursementEditComponent } from './disbursement-edit.component';
import { DisbursementPopupComponent } from './disbursement-dialog.component';

@Injectable()
export class DisbursementResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPayment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const disbursementRoute: Routes = [
    {
        path: 'disbursement',
        component: DisbursementComponent,
        resolve: {
            'pagingParams': DisbursementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const disbursementPopupRoute: Routes = [
    {
        path: 'disbursement-popup-new-list/:parent',
        component: DisbursementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'disbursement-popup-new',
        component: DisbursementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'disbursement-new',
        component: DisbursementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'disbursement/:id/edit',
        component: DisbursementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'disbursement/:route/:page/:id/edit',
        component: DisbursementEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'disbursement/:id/popup-edit',
        component: DisbursementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.disbursement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
