import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Disbursement } from './disbursement.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DisbursementService {
   protected itemValues: Disbursement[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/disbursements';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/disbursements';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(disbursement: Disbursement): Observable<Disbursement> {
       const copy = this.convert(disbursement);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(disbursement: Disbursement): Observable<Disbursement> {
       const copy = this.convert(disbursement);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<Disbursement> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(disbursement: Disbursement, id: Number): Observable<ResponseWrapper> {
       const copy = this.convert(disbursement);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: Disbursement, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: Disbursement[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to Disbursement.
    */
   protected convertItemFromServer(json: any): Disbursement {
       const entity: Disbursement = Object.assign(new Disbursement(), json);
       if (entity.dateCreate) {
           entity.dateCreate = new Date(entity.dateCreate);
       }
       return entity;
   }

   /**
    * Convert a Disbursement to a JSON which can be sent to the server.
    */
   protected convert(disbursement: Disbursement): Disbursement {
       if (disbursement === null || disbursement === {}) {
           return {};
       }
       // const copy: Disbursement = Object.assign({}, disbursement);
       const copy: Disbursement = JSON.parse(JSON.stringify(disbursement));

       // copy.dateCreate = this.dateUtils.toDate(disbursement.dateCreate);
       return copy;
   }

   protected convertList(disbursements: Disbursement[]): Disbursement[] {
       const copy: Disbursement[] = disbursements;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: Disbursement[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
