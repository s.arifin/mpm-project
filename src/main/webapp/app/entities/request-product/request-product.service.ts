import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RequestProduct } from './request-product.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RequestProductService {
    protected itemValues: RequestProduct[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/request-products';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/request-products';

    constructor(protected http: Http) { }

    create(requestProduct: RequestProduct): Observable<RequestProduct> {
        const copy = this.convert(requestProduct);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(requestProduct: RequestProduct): Observable<RequestProduct> {
        const copy = this.convert(requestProduct);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<RequestProduct> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(requestProduct: RequestProduct, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(requestProduct);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<RequestProduct> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<RequestProduct[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RequestProduct.
     */
    protected convertItemFromServer(json: any): RequestProduct {
        const entity: RequestProduct = Object.assign(new RequestProduct(), json);
        return entity;
    }

    /**
     * Convert a RequestProduct to a JSON which can be sent to the server.
     */
    protected convert(requestProduct: RequestProduct): RequestProduct {
        if (requestProduct === null || requestProduct === {}) {
            return {};
        }
        // const copy: RequestProduct = Object.assign({}, requestProduct);
        const copy: RequestProduct = JSON.parse(JSON.stringify(requestProduct));
        return copy;
    }

    protected convertList(requestProducts: RequestProduct[]): RequestProduct[] {
        const copy: RequestProduct[] = requestProducts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RequestProduct[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
