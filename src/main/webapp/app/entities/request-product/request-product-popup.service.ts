import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RequestProduct } from './request-product.model';
import { RequestProductService } from './request-product.service';

@Injectable()
export class RequestProductPopupService {
    protected ngbModalRef: NgbModalRef;
    idFacilityFrom: any;
    idFacilityTo: any;
    idStatusType: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected requestProductService: RequestProductService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.requestProductService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.requestProductModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new RequestProduct();
                    data.facilityFromId = this.idFacilityFrom;
                    data.facilityToId = this.idFacilityTo;
                    this.ngbModalRef = this.requestProductModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    requestProductModalRef(component: Component, requestProduct: RequestProduct): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.requestProduct = requestProduct;
        modalRef.componentInstance.idFacilityFrom = this.idFacilityFrom;
        modalRef.componentInstance.idFacilityTo = this.idFacilityTo;
        modalRef.componentInstance.idStatusType = this.idStatusType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.requestProductLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    requestProductLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
