import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestProduct } from './request-product.model';
import { RequestProductPopupService } from './request-product-popup.service';
import { RequestProductService } from './request-product.service';
import { ToasterService } from '../../shared';
import { Facility, FacilityService } from '../facility';
import { ResponseWrapper } from '../../shared';
import { RequestType, RequestTypeService } from '../request-type';

@Component({
    selector: 'jhi-request-product-dialog',
    templateUrl: './request-product-dialog.component.html'
})
export class RequestProductDialogComponent implements OnInit {
    requestProduct: RequestProduct;
    isSaving: boolean;
    idFacilityFrom: any;
    idFacilityTo: any;

    facilities: Facility[];

    public requestTypes: Array<RequestType>;

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected requestProductService: RequestProductService,
        protected facilityService: FacilityService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected requestTypeService: RequestTypeService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.requestTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.requestTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.initData();
    }

    protected initData(): void {
        if (this.requestProduct.facilityFromId === undefined || this.requestProduct.facilityFromId === null) {
            this.requestProduct.facilityFromId = null;
        }

        if (this.requestProduct.facilityToId === undefined || this.requestProduct.facilityToId === null) {
            this.requestProduct.facilityToId = null;
        }

        if (this.requestProduct.requestTypeId === undefined || this.requestProduct.requestTypeId === null) {
            this.requestProduct.requestTypeId = null;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requestProduct.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestProductService.update(this.requestProduct));
        } else {
            this.requestProduct.dateCreate = new Date();
            this.subscribeToSaveResponse(
                this.requestProductService.create(this.requestProduct));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestProduct>) {
        result.subscribe((res: RequestProduct) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RequestProduct) {
        this.eventManager.broadcast({ name: 'requestProductListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestProduct saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'requestProduct Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackRequestTypeById(index: number, item: RequestType) {
        return item.idRequestType;
    }
}

@Component({
    selector: 'jhi-request-product-popup',
    template: ''
})
export class RequestProductPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestProductPopupService: RequestProductPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requestProductPopupService
                    .open(RequestProductDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.requestProductPopupService
                    .open(RequestProductDialogComponent as Component);
            } else {
                this.requestProductPopupService
                    .open(RequestProductDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
