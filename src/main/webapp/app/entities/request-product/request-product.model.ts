import { Request } from '../request/request.model';

export class RequestProduct extends Request {
    constructor(
        public facilityFromId?: any,
        public facilityTypeFromId?: any,
        public facilityToId?: any,
        public facilityTypeToId?: any,
        public requestTypeDescription?: any,
        public facilityFromDescription?: any,
        public facilityToDescription?: any,
    ) {
        super();
    }
}
