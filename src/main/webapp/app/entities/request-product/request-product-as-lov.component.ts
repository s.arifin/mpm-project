import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { RequestProduct } from './request-product.model';
import { RequestProductService } from './request-product.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RequestProductPopupService } from './request-product-popup.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-request-product-as-lov',
    templateUrl: './request-product-as-lov.component.html'
})
export class RequestProductAsLovComponent implements OnInit, OnDestroy {

    currentAccount: any;
    idFacilityFrom: any;
    idFacilityTo: any;
    idStatusType: any;
    requestProducts: RequestProduct[];
    selected: RequestProduct[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: Number;

    constructor(
        public activeModal: NgbActiveModal,
        protected requestProductService: RequestProductService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idRequest';
        this.reverse = 'asc';
    }

    loadAll() {
        if (this.currentSearch) {
            this.requestProductService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.requestProductService.queryFilterBy({
            idFacilityFrom: this.idFacilityFrom,
            idFacilityTo: this.idFacilityTo,
            idStatusType: this.idStatusType,

            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: RequestProduct) {
        return item.idRequest;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idRequest') {
            result.push('idRequest');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.requestProducts = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.requestProductService.pushItems(this.selected);
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

}

@Component({
    selector: 'jhi-request-product-lov-popup',
    template: ''
})
export class RequestProductLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestProductPopupService: RequestProductPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(
            (params) => {
                if (params['idFacilityFrom'] && params['idFacilityTo'] && params['idStatusType']) {
                    this.requestProductPopupService.idFacilityFrom = params['idFacilityFrom'];
                    this.requestProductPopupService.idFacilityTo = params['idFacilityTo'];
                    this.requestProductPopupService.idStatusType = params['idStatusType'];
                    this.requestProductPopupService.open(RequestProductAsLovComponent as Component);
                }
            }
        );
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
