import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RequestProductComponent } from './request-product.component';
import { RequestProductEditComponent } from './request-product-edit.component';
import { RequestProductLovPopupComponent } from './request-product-as-lov.component';
import { RequestProductPopupComponent } from './request-product-dialog.component';

@Injectable()
export class RequestProductResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequest,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requestProductRoute: Routes = [
    {
        path: 'request-product',
        component: RequestProductComponent,
        resolve: {
            'pagingParams': RequestProductResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requestProductPopupRoute: Routes = [
    {
        path: 'request-product-lov/:idStatusType/:idFacilityFrom/:idFacilityTo',
        component: RequestProductLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-product-popup-new',
        component: RequestProductPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-product-new',
        component: RequestProductEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-product/:id/edit',
        component: RequestProductEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-product/:id/edit/:page',
        component: RequestProductEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-product/:id/popup-edit',
        component: RequestProductPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
