import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestProduct } from './request-product.model';
import { RequestProductService } from './request-product.service';
import { ToasterService, CommonUtilService} from '../../shared';
import { Facility, FacilityService } from '../facility';
import { ResponseWrapper } from '../../shared';
import { RequestTypeService, RequestType } from '../request-type';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts';
import * as BaseConstant from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-request-product-edit',
    templateUrl: './request-product-edit.component.html'
})
export class RequestProductEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;

    public requestTypes: Array<RequestType>;

    requestProduct: RequestProduct;
    isSaving: boolean;
    idRequest: any;
    paramPage: number;

    facilities: Facility[];

    constructor(
        protected alertService: JhiAlertService,
        protected requestProductService: RequestProductService,
        protected facilityService: FacilityService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected requestTypeService: RequestTypeService,
        protected confirmationService: ConfirmationService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService
    ) {
        this.requestProduct = new RequestProduct();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequest = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
        });
        this.isSaving = false;
        this.requestTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.requestTypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.requestProductService.find(this.idRequest).subscribe((requestProduct) => {
            this.requestProduct = requestProduct;
        });
    }

    previousState() {
        this.router.navigate(['request-product', { page: this.paramPage }]);
    }

    save() {
        this.isSaving = true;
        if (this.requestProduct.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestProductService.update(this.requestProduct));
        } else {
            this.subscribeToSaveResponse(
                this.requestProductService.create(this.requestProduct));
        }
    }

    public process(): void {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to process to approval?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.loadingService.loadingStart();
                this.requestProductService.changeStatus(this.requestProduct, BaseConstant.Status.STATUS_WAITING_FOR_APPROVAL).subscribe(
                    (res) => {
                        this.eventManager.broadcast({ name: 'requestProductListModification', content: 'OK'});
                        this.loadingService.loadingStop();
                        this.previousState();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commonUtilService.showError(err);
                    }
                )
            }
        });
    }

    protected subscribeToSaveResponse(result: Observable<RequestProduct>) {
        result.subscribe((res: RequestProduct) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RequestProduct) {
        this.eventManager.broadcast({ name: 'requestProductListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestProduct saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requestProduct Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackRequestTypeById(index: number, item: RequestType) {
        return item.idRequestType;
    }
}
