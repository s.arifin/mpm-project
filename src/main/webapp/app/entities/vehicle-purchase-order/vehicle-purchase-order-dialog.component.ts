import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VehiclePurchaseOrder } from './vehicle-purchase-order.model';
import { VehiclePurchaseOrderPopupService } from './vehicle-purchase-order-popup.service';
import { VehiclePurchaseOrderService } from './vehicle-purchase-order.service';
import { ToasterService } from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-purchase-order-dialog',
    templateUrl: './vehicle-purchase-order-dialog.component.html'
})
export class VehiclePurchaseOrderDialogComponent implements OnInit {

    vehiclePurchaseOrder: VehiclePurchaseOrder;
    isSaving: boolean;
    idVendor: any;
    idInternal: any;
    idBillTo: any;

    vendors: Vendor[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected vehiclePurchaseOrderService: VehiclePurchaseOrderService,
        protected vendorService: VendorService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehiclePurchaseOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.vehiclePurchaseOrderService.update(this.vehiclePurchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.vehiclePurchaseOrderService.create(this.vehiclePurchaseOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<VehiclePurchaseOrder>) {
        result.subscribe((res: VehiclePurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: VehiclePurchaseOrder) {
        this.eventManager.broadcast({ name: 'vehiclePurchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehiclePurchaseOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'vehiclePurchaseOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-vehicle-purchase-order-popup',
    template: ''
})
export class VehiclePurchaseOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected vehiclePurchaseOrderPopupService: VehiclePurchaseOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehiclePurchaseOrderPopupService
                    .open(VehiclePurchaseOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.vehiclePurchaseOrderPopupService.parent = params['parent'];
                this.vehiclePurchaseOrderPopupService
                    .open(VehiclePurchaseOrderDialogComponent as Component);
            } else {
                this.vehiclePurchaseOrderPopupService
                    .open(VehiclePurchaseOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
