import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { VehiclePurchaseOrderComponent } from './vehicle-purchase-order.component';
import { VehiclePurchaseOrderEditComponent } from './vehicle-purchase-order-edit.component';
import { VehiclePurchaseOrderPopupComponent } from './vehicle-purchase-order-dialog.component';

@Injectable()
export class VehiclePurchaseOrderResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vehiclePurchaseOrderRoute: Routes = [
    {
        path: 'vehicle-purchase-order',
        component: VehiclePurchaseOrderComponent,
        resolve: {
            'pagingParams': VehiclePurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehiclePurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vehiclePurchaseOrderPopupRoute: Routes = [
    {
        path: 'vehicle-purchase-order-popup-new',
        component: VehiclePurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehiclePurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vehicle-purchase-order-new',
        component: VehiclePurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehiclePurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-purchase-order/:id/edit',
        component: VehiclePurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehiclePurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-purchase-order/:route/:page/:id/edit',
        component: VehiclePurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehiclePurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'vehicle-purchase-order/:id/popup-edit',
        component: VehiclePurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.vehiclePurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
