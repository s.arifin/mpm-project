import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { VehiclePurchaseOrder } from './vehicle-purchase-order.model';
import { VehiclePurchaseOrderService } from './vehicle-purchase-order.service';

@Injectable()
export class VehiclePurchaseOrderPopupService {
    protected ngbModalRef: NgbModalRef;
    idVendor: any;
    idInternal: any;
    idBillTo: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected vehiclePurchaseOrderService: VehiclePurchaseOrderService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.vehiclePurchaseOrderService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.vehiclePurchaseOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new VehiclePurchaseOrder();
                    data.vendorId = this.idVendor;
                    data.internalId = this.idInternal;
                    data.billToId = this.idBillTo;
                    this.ngbModalRef = this.vehiclePurchaseOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    vehiclePurchaseOrderModalRef(component: Component, vehiclePurchaseOrder: VehiclePurchaseOrder): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.vehiclePurchaseOrder = vehiclePurchaseOrder;
        modalRef.componentInstance.idVendor = this.idVendor;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.vehiclePurchaseOrderLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    vehiclePurchaseOrderLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
