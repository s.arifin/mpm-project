import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { VehiclePurchaseOrder } from './vehicle-purchase-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class VehiclePurchaseOrderService {
    private itemValues: VehiclePurchaseOrder[];
    values: Subject<any> = new Subject<any>();

    private resourceUrl =  SERVER_API_URL + 'api/vehicle-purchase-orders';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vehicle-purchase-orders';

    constructor(private http: Http) { }

    create(vehiclePurchaseOrder: VehiclePurchaseOrder): Observable<VehiclePurchaseOrder> {
        const copy = this.convert(vehiclePurchaseOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(vehiclePurchaseOrder: VehiclePurchaseOrder): Observable<VehiclePurchaseOrder> {
        const copy = this.convert(vehiclePurchaseOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<VehiclePurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<VehiclePurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<VehiclePurchaseOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to VehiclePurchaseOrder.
     */
    private convertItemFromServer(json: any): VehiclePurchaseOrder {
        const entity: VehiclePurchaseOrder = Object.assign(new VehiclePurchaseOrder(), json);
        return entity;
    }

    /**
     * Convert a VehiclePurchaseOrder to a JSON which can be sent to the server.
     */
    private convert(vehiclePurchaseOrder: VehiclePurchaseOrder): VehiclePurchaseOrder {
        if (vehiclePurchaseOrder === null || vehiclePurchaseOrder === {}) {
            return {};
        }
        // const copy: VehiclePurchaseOrder = Object.assign({}, vehiclePurchaseOrder);
        const copy: VehiclePurchaseOrder = JSON.parse(JSON.stringify(vehiclePurchaseOrder));
        return copy;
    }

    private convertList(vehiclePurchaseOrders: VehiclePurchaseOrder[]): VehiclePurchaseOrder[] {
        const copy: VehiclePurchaseOrder[] = vehiclePurchaseOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: VehiclePurchaseOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
