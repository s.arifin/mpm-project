import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    OrganizationService,
} from './';

@NgModule({
    imports: [
        MpmSharedModule,
    ],
    exports: [
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        OrganizationService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmOrganizationModule {}
