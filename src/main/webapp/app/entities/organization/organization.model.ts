import { BaseEntity } from './../../shared';
import { Party } from './../party';
import { PostalAddress } from './../postal-address';
import { Person } from '../person';

export class Organization extends Party {
    constructor(
        public officePhone?: any,
        public officeFax?: any,
        public officeMail?: any,
        public postalAddress?: any,
        public name?: any,
        public dob?: any,
        public numberTDP?: any,
        public dateOrganizationEstablish?: any,
        public idPic?: any,
        public npwp?: any,
        public picName?: any,
        public picPhone?: any,
        public picMail?: any
    ) {
        super();
        this.postalAddress = new PostalAddress();
        this.dob = new Date('1950-01-01');
        this.dateOrganizationEstablish = new Date('1950-01-01');
    }
}
