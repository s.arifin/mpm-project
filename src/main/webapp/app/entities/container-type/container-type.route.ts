import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ContainerTypeComponent } from './container-type.component';
import { ContainerTypePopupComponent } from './container-type-dialog.component';

@Injectable()
export class ContainerTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idContainerType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const containerTypeRoute: Routes = [
    {
        path: 'container-type',
        component: ContainerTypeComponent,
        resolve: {
            'pagingParams': ContainerTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.containerType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const containerTypePopupRoute: Routes = [
    {
        path: 'container-type-new',
        component: ContainerTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.containerType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'container-type/:id/edit',
        component: ContainerTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.containerType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
