import { BaseEntity } from './../../shared';

export class ContainerType implements BaseEntity {
    constructor(
        public id?: number,
        public idContainerType?: number,
        public description?: string,
    ) {
    }
}
