import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ContainerType } from './container-type.model';
import { ContainerTypePopupService } from './container-type-popup.service';
import { ContainerTypeService } from './container-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-container-type-dialog',
    templateUrl: './container-type-dialog.component.html'
})
export class ContainerTypeDialogComponent implements OnInit {

    containerType: ContainerType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected containerTypeService: ContainerTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.containerType.idContainerType !== undefined) {
            this.subscribeToSaveResponse(
                this.containerTypeService.update(this.containerType));
        } else {
            this.subscribeToSaveResponse(
                this.containerTypeService.create(this.containerType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ContainerType>) {
        result.subscribe((res: ContainerType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ContainerType) {
        this.eventManager.broadcast({ name: 'containerTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'containerType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-container-type-popup',
    template: ''
})
export class ContainerTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected containerTypePopupService: ContainerTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.containerTypePopupService
                    .open(ContainerTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.containerTypePopupService.parent = params['parent'];
                this.containerTypePopupService
                    .open(ContainerTypeDialogComponent as Component);
            } else {
                this.containerTypePopupService
                    .open(ContainerTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
