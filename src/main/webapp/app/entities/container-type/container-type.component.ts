import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ContainerType } from './container-type.model';
import { ContainerTypeService } from './container-type.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
   selector: 'jhi-container-type',
   templateUrl: './container-type.component.html'
})
export class ContainerTypeComponent implements OnInit, OnDestroy {

    currentAccount: any;
    containerTypes: ContainerType[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected containerTypeService: ContainerTypeService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            this.containerTypeService.search({
                page: this.page,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.containerTypeService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/container-type'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.router.navigate(['/container-type', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 1;
        this.currentSearch = query;
        this.router.navigate(['/container-type', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

   ngOnInit() {
       this.loadAll();
       this.principal.identity(true).then((account) => {
           this.currentAccount = account;
       });
       this.registerChangeInContainerTypes();
   }

   ngOnDestroy() {
       this.eventManager.destroy(this.eventSubscriber);
   }

   trackId(index: number, item: ContainerType) {
       return item.idContainerType;
   }

   registerChangeInContainerTypes() {
       this.eventSubscriber = this.eventManager.subscribe('containerTypeListModification', (response) => this.loadAll());
   }

   sort() {
       const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
       if (this.predicate !== 'idContainerType') {
           result.push('idContainerType');
       }
       return result;
   }

   protected onSuccess(data, headers) {
    //    this.links = this.parseLinks.parse(headers.get('link'));
       this.totalItems = headers.get('X-Total-Count');
       this.queryCount = this.totalItems;
       // this.page = pagingParams.page;
       this.containerTypes = data;
   }

   protected onError(error) {
       this.jhiAlertService.error(error.message, null, null);
   }

   loadDataLazy(event: LazyLoadEvent) {
       this.itemsPerPage = event.rows;
       this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
       this.previousPage = this.page;

       if (event.sortField !== undefined) {
           this.predicate = event.sortField;
           this.reverse = event.sortOrder;
       }
       this.loadAll();
   }

   updateRowData(event) {
       if (event.data.id !== undefined) {
           this.containerTypeService.update(event.data)
               .subscribe((res: ContainerType) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       } else {
           this.containerTypeService.create(event.data)
               .subscribe((res: ContainerType) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       }
   }

   protected onRowDataSaveSuccess(result: ContainerType) {
       this.toaster.showToaster('info', 'ContainerType Saved', 'Data saved..');
   }

   protected onRowDataSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.onError(error);
   }

   delete(id: any) {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to delete?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.containerTypeService.delete(id).subscribe((response) => {
                   this.eventManager.broadcast({
                       name: 'containerTypeListModification',
                       content: 'Deleted an containerType'
                   });
               });
           }
       });
   }

   process() {
       this.containerTypeService.process({}).subscribe((r) => {
           console.log('result', r);
           this.toaster.showToaster('info', 'Data Processed', 'processed.....');
       });
   }

   buildReindex() {
    this.containerTypeService.process({command: 'buildIndex'}).subscribe((r) => {
        this.toaster.showToaster('info', 'Build Index', 'Build Index processed.....');
    });
}

}
