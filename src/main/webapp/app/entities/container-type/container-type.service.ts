import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ContainerType } from './container-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ContainerTypeService {
    protected itemValues: ContainerType[];
    values: Subject<any> = new Subject<any>();

    //    protected resourceUrl =  process.env.API_C_URL + 'api/container-types';
    protected resourceUrl = process.env.API_C_URL + '/api/container-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/container-types';

    constructor(protected http: Http) { }

    create(containerType: ContainerType): Observable<ContainerType> {
        const copy = this.convert(containerType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(containerType: ContainerType): Observable<ContainerType> {
        const copy = this.convert(containerType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ContainerType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    getAll(req?: any): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

//    queryFilterBy(req?: any): Observable<ResponseWrapper> {
//        const options = createRequestOption(req);
//        return this.http.get(this.resourceUrl + '/filterBy', options)
//            .map((res: Response) => this.convertResponse(res));
//    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

//    executeProcess(item?: ContainerType, req?: any): Observable<Response> {
//        const options = createRequestOption(req);
//        return this.http.post(`${this.resourceUrl}/execute`, item, options);
//    }

//    executeListProcess(items?: ContainerType[], req?: any): Observable<Response> {
//        const options = createRequestOption(req);
//        return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
//    }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to ContainerType.
    */
   protected convertItemFromServer(json: any): ContainerType {
       const entity: ContainerType = Object.assign(new ContainerType(), json);
       return entity;
   }

   /**
    * Convert a ContainerType to a JSON which can be sent to the server.
    */
   protected convert(containerType: ContainerType): ContainerType {
       if (containerType === null || containerType === {}) {
           return {};
       }
       // const copy: ContainerType = Object.assign({}, containerType);
       const copy: ContainerType = JSON.parse(JSON.stringify(containerType));
       return copy;
   }

   protected convertList(containerTypes: ContainerType[]): ContainerType[] {
       const copy: ContainerType[] = containerTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: ContainerType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
