import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PaymentComponent } from './payment.component';
import { PaymentEditComponent } from './payment-edit.component';
import { PaymentPopupComponent } from './payment-dialog.component';

@Injectable()
export class PaymentResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPayment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const paymentRoute: Routes = [
    {
        path: 'payment',
        component: PaymentComponent,
        resolve: {
            'pagingParams': PaymentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentPopupRoute: Routes = [
    {
        path: 'payment-popup-new',
        component: PaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-new',
        component: PaymentEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment/:id/edit',
        component: PaymentEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment/:route/:page/:id/edit',
        component: PaymentEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment/:id/popup-edit',
        component: PaymentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
