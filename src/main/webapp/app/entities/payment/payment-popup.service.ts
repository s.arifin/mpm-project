import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Payment } from './payment.model';
import { PaymentService } from './payment.service';

@Injectable()
export class PaymentPopupService {
    protected ngbModalRef: NgbModalRef;
    idPaymentType: any;
    idMethod: any;
    idInternal: any;
    idPaidTo: any;
    idPaidFrom: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected paymentService: PaymentService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.paymentService.find(id).subscribe((data) => {
                    // if (data.dateCreate) {
                    //    data.dateCreate = this.datePipe
                    //        .transform(data.dateCreate, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.paymentModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Payment();
                    data.paymentTypeId = this.idPaymentType;
                    data.methodId = this.idMethod;
                    data.internalId = this.idInternal;
                    data.paidToId = this.idPaidTo;
                    data.paidFromId = this.idPaidFrom;
                    this.ngbModalRef = this.paymentModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    paymentModalRef(component: Component, payment: Payment): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.payment = payment;
        modalRef.componentInstance.idPaymentType = this.idPaymentType;
        modalRef.componentInstance.idMethod = this.idMethod;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPaidTo = this.idPaidTo;
        modalRef.componentInstance.idPaidFrom = this.idPaidFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.paymentLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    paymentLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idPaymentType = this.idPaymentType;
        modalRef.componentInstance.idMethod = this.idMethod;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idPaidTo = this.idPaidTo;
        modalRef.componentInstance.idPaidFrom = this.idPaidFrom;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
