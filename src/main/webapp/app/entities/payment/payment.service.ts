import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Payment } from './payment.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentService {
   protected itemValues: Payment[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/payments';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/payments';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(payment: Payment): Observable<Payment> {
       const copy = this.convert(payment);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(payment: Payment): Observable<Payment> {
       const copy = this.convert(payment);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<Payment> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(payment: Payment, id: Number): Observable<ResponseWrapper> {
       const copy = this.convert(payment);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: Payment, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: Payment[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to Payment.
    */
   protected convertItemFromServer(json: any): Payment {
       const entity: Payment = Object.assign(new Payment(), json);
       if (entity.dateCreate) {
           entity.dateCreate = new Date(entity.dateCreate);
       }
       return entity;
   }

   /**
    * Convert a Payment to a JSON which can be sent to the server.
    */
   protected convert(payment: Payment): Payment {
       if (payment === null || payment === {}) {
           return {};
       }
       // const copy: Payment = Object.assign({}, payment);
       const copy: Payment = JSON.parse(JSON.stringify(payment));

       // copy.dateCreate = this.dateUtils.toDate(payment.dateCreate);
       return copy;
   }

   protected convertList(payments: Payment[]): Payment[] {
       const copy: Payment[] = payments;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: Payment[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
