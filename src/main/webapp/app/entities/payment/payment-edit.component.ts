import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Payment } from './payment.model';
import { PaymentService } from './payment.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { PaymentType, PaymentTypeService } from '../payment-type';
import { PaymentMethod, PaymentMethodService } from '../payment-method';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-payment-edit',
   templateUrl: './payment-edit.component.html'
})
export class PaymentEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   payment: Payment;
   isSaving: boolean;
   idPayment: any;
   paramPage: number;
   routeId: number;

   paymenttypes: PaymentType[];

   paymentmethods: PaymentMethod[];

   internals: Internal[];

   billtos: BillTo[];

   constructor(
       protected alertService: JhiAlertService,
       protected paymentService: PaymentService,
       protected paymentTypeService: PaymentTypeService,
       protected paymentMethodService: PaymentMethodService,
       protected internalService: InternalService,
       protected billToService: BillToService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected reportUtilService: ReportUtilService,
       protected toaster: ToasterService
   ) {
       this.payment = new Payment();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idPayment = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.paymentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.paymenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.paymentMethodService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentmethods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.paymentService.find(this.idPayment).subscribe((payment) => {
           this.payment = payment;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['payment', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.payment.idPayment !== undefined) {
           this.subscribeToSaveResponse(
               this.paymentService.update(this.payment));
       } else {
           this.subscribeToSaveResponse(
               this.paymentService.create(this.payment));
       }
   }

   protected subscribeToSaveResponse(result: Observable<Payment>) {
       result.subscribe((res: Payment) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: Payment) {
       this.eventManager.broadcast({ name: 'paymentListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'payment saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'payment Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackPaymentTypeById(index: number, item: PaymentType) {
       return item.idPaymentType;
   }

   trackPaymentMethodById(index: number, item: PaymentMethod) {
       return item.idPaymentMethod;
   }

   trackInternalById(index: number, item: Internal) {
       return item.idInternal;
   }

   trackBillToById(index: number, item: BillTo) {
       return item.idBillTo;
   }
   print() {
       this.toaster.showToaster('info', 'Print Data', 'Printing.....');
       this.reportUtilService.viewFile('/api/report/sample/pdf');
   }

}
