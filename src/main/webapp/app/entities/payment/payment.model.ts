import { BaseEntity } from './../../shared';

export class Payment implements BaseEntity {
    constructor(
        public id?: any,
        public idPayment?: any,
        public paymentNumber?: string,
        public refferenceNumber?: string,
        public dateCreate?: any,
        public amount?: number,
        public details?: any,
        public paymentTypeId?: any,
        public paymentTypeDescription?: string,
        public methodId?: any,
        public methodDescription?: string,
        public internalId?: any,
        public paidToId?: any,
        public paidFromId?: any,
    ) {
    }
}
