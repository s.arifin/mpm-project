import { BaseEntity } from './../../shared';

export class PaymentMethod implements BaseEntity {
    constructor(
        public id?: number,
        public idPaymentMethod?: number,
        public refKey?: string,
        public description?: string,
        public accountNumber?: string,
        public methodTypeId?: any,
    ) {
    }
}
