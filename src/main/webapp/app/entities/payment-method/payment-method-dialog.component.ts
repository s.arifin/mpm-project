import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentMethod } from './payment-method.model';
import { PaymentMethodPopupService } from './payment-method-popup.service';
import { PaymentMethodService } from './payment-method.service';
import { ToasterService } from '../../shared';
import { PaymentMethodType, PaymentMethodTypeService } from '../payment-method-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-payment-method-dialog',
    templateUrl: './payment-method-dialog.component.html'
})
export class PaymentMethodDialogComponent implements OnInit {

    paymentMethod: PaymentMethod;
    isSaving: boolean;
    idMethodType: any;

    paymentmethodtypes: PaymentMethodType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected paymentMethodService: PaymentMethodService,
        protected paymentMethodTypeService: PaymentMethodTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentMethodTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.paymentmethodtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentMethod.idPaymentMethod !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentMethodService.update(this.paymentMethod));
        } else {
            this.subscribeToSaveResponse(
                this.paymentMethodService.create(this.paymentMethod));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PaymentMethod>) {
        result.subscribe((res: PaymentMethod) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PaymentMethod) {
        this.eventManager.broadcast({ name: 'paymentMethodListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'paymentMethod saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'paymentMethod Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPaymentMethodTypeById(index: number, item: PaymentMethodType) {
        return item.idPaymentMethodType;
    }
}

@Component({
    selector: 'jhi-payment-method-popup',
    template: ''
})
export class PaymentMethodPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected paymentMethodPopupService: PaymentMethodPopupService
    ) {}

    ngOnInit() {
        this.paymentMethodPopupService.idMethodType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentMethodPopupService
                    .open(PaymentMethodDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.paymentMethodPopupService.idMethodType = params['parent'];
                this.paymentMethodPopupService
                    .open(PaymentMethodDialogComponent as Component);
            } else {
                this.paymentMethodPopupService
                    .open(PaymentMethodDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
