import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PaymentMethod } from './payment-method.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentMethodService {
   protected itemValues: PaymentMethod[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/payment-methods';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/payment-methods';

   constructor(protected http: Http) { }

   create(paymentMethod: PaymentMethod): Observable<PaymentMethod> {
       const copy = this.convert(paymentMethod);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(paymentMethod: PaymentMethod): Observable<PaymentMethod> {
       const copy = this.convert(paymentMethod);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PaymentMethod> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PaymentMethod, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PaymentMethod[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PaymentMethod.
    */
   protected convertItemFromServer(json: any): PaymentMethod {
       const entity: PaymentMethod = Object.assign(new PaymentMethod(), json);
       return entity;
   }

   /**
    * Convert a PaymentMethod to a JSON which can be sent to the server.
    */
   protected convert(paymentMethod: PaymentMethod): PaymentMethod {
       if (paymentMethod === null || paymentMethod === {}) {
           return {};
       }
       // const copy: PaymentMethod = Object.assign({}, paymentMethod);
       const copy: PaymentMethod = JSON.parse(JSON.stringify(paymentMethod));
       return copy;
   }

   protected convertList(paymentMethods: PaymentMethod[]): PaymentMethod[] {
       const copy: PaymentMethod[] = paymentMethods;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PaymentMethod[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
