import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PaymentMethod } from './payment-method.model';
import { PaymentMethodService } from './payment-method.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-payment-method-as-list',
    templateUrl: './payment-method-as-list.component.html'
})
export class PaymentMethodAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idMethodType: any;

    currentAccount: any;
    paymentMethods: PaymentMethod[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected paymentMethodService: PaymentMethodService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = 10;
        this.page = 1;
        this.predicate = 'idPaymentMethod';
        this.reverse = 'asc';
        this.first = 0;
    }

    loadAll() {
        this.paymentMethodService.queryFilterBy({
            idMethodType: this.idMethodType,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/payment-method', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPaymentMethods();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idMethodType']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PaymentMethod) {
        return item.idPaymentMethod;
    }

    registerChangeInPaymentMethods() {
        this.eventSubscriber = this.eventManager.subscribe('paymentMethodListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPaymentMethod') {
            result.push('idPaymentMethod');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.paymentMethods = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.paymentMethodService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentMethodService.update(event.data));
        } else {
            this.subscribeToSaveResponse(
                this.paymentMethodService.create(event.data));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PaymentMethod>) {
        result.subscribe((res: PaymentMethod) => this.onSaveSuccess(res));
    }

    protected onSaveSuccess(result: PaymentMethod) {
        this.eventManager.broadcast({ name: 'paymentMethodListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'paymentMethod saved !');
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.paymentMethodService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'paymentMethodListModification',
                        content: 'Deleted an paymentMethod'
                    });
                });
            }
        });
    }
}
