import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ProductPurchaseOrder } from './product-purchase-order.model';
import { ProductPurchaseOrderService } from './product-purchase-order.service';

@Injectable()
export class ProductPurchaseOrderPopupService {
    protected ngbModalRef: NgbModalRef;
    idVendor: any;
    idInternal: any;
    idBillTo: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected productPurchaseOrderService: ProductPurchaseOrderService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.productPurchaseOrderService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.productPurchaseOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new ProductPurchaseOrder();
                    data.vendorId = this.idVendor;
                    data.internalId = this.idInternal;
                    data.billToId = this.idBillTo;
                    this.ngbModalRef = this.productPurchaseOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    productPurchaseOrderModalRef(component: Component, productPurchaseOrder: ProductPurchaseOrder): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.productPurchaseOrder = productPurchaseOrder;
        modalRef.componentInstance.idVendor = this.idVendor;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.productPurchaseOrderLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    productPurchaseOrderLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
