import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ProductPurchaseOrder } from './product-purchase-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductPurchaseOrderService {
    protected itemValues: ProductPurchaseOrder[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/product-purchase-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-purchase-orders';

    constructor(protected http: Http) { }

    create(productPurchaseOrder: ProductPurchaseOrder): Observable<ProductPurchaseOrder> {
        const copy = this.convert(productPurchaseOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(productPurchaseOrder: ProductPurchaseOrder): Observable<ProductPurchaseOrder> {
        const copy = this.convert(productPurchaseOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ProductPurchaseOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(productPurchaseOrder: ProductPurchaseOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(productPurchaseOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ProductPurchaseOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ProductPurchaseOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ProductPurchaseOrder.
     */
    protected convertItemFromServer(json: any): ProductPurchaseOrder {
        const entity: ProductPurchaseOrder = Object.assign(new ProductPurchaseOrder(), json);
        return entity;
    }

    /**
     * Convert a ProductPurchaseOrder to a JSON which can be sent to the server.
     */
    protected convert(productPurchaseOrder: ProductPurchaseOrder): ProductPurchaseOrder {
        if (productPurchaseOrder === null || productPurchaseOrder === {}) {
            return {};
        }
        // const copy: ProductPurchaseOrder = Object.assign({}, productPurchaseOrder);
        const copy: ProductPurchaseOrder = JSON.parse(JSON.stringify(productPurchaseOrder));
        return copy;
    }

    protected convertList(productPurchaseOrders: ProductPurchaseOrder[]): ProductPurchaseOrder[] {
        const copy: ProductPurchaseOrder[] = productPurchaseOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductPurchaseOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
