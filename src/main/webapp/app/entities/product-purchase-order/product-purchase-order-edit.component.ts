import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductPurchaseOrder } from './product-purchase-order.model';
import { ProductPurchaseOrderService } from './product-purchase-order.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { Vendor, VendorService } from '../vendor';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-purchase-order-edit',
    templateUrl: './product-purchase-order-edit.component.html'
})
export class ProductPurchaseOrderEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    productPurchaseOrder: ProductPurchaseOrder;
    isSaving: boolean;
    idOrder: any;
    paramPage: number;
    routeId: number;

    vendors: Vendor[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected productPurchaseOrderService: ProductPurchaseOrderService,
        protected vendorService: VendorService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected reportUtilService: ReportUtilService,
        protected toaster: ToasterService
    ) {
        this.productPurchaseOrder = new ProductPurchaseOrder();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idOrder = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.productPurchaseOrderService.find(this.idOrder).subscribe((productPurchaseOrder) => {
            this.productPurchaseOrder = productPurchaseOrder;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['product-purchase-order', { page: this.paramPage }], {replaceUrl: true});
        }
    }

    save() {
        this.isSaving = true;
        if (this.productPurchaseOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.productPurchaseOrderService.update(this.productPurchaseOrder));
        } else {
            this.subscribeToSaveResponse(
                this.productPurchaseOrderService.create(this.productPurchaseOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductPurchaseOrder>) {
        result.subscribe((res: ProductPurchaseOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProductPurchaseOrder) {
        this.eventManager.broadcast({ name: 'productPurchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productPurchaseOrder saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'productPurchaseOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
    print() {
        this.toaster.showToaster('info', 'Print Data', 'Printing.....');
        this.reportUtilService.viewFile('/api/report/sample/pdf');
    }

}
