import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProductPurchaseOrderComponent } from './product-purchase-order.component';
import { ProductPurchaseOrderEditComponent } from './product-purchase-order-edit.component';
import { ProductPurchaseOrderPopupComponent } from './product-purchase-order-dialog.component';

@Injectable()
export class ProductPurchaseOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'orderNumber,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productPurchaseOrderRoute: Routes = [
    {
        path: 'product-purchase-order',
        component: ProductPurchaseOrderComponent,
        resolve: {
            'pagingParams': ProductPurchaseOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productPurchaseOrderPopupRoute: Routes = [
    {
        path: 'product-purchase-order-popup-new',
        component: ProductPurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-purchase-order-new',
        component: ProductPurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-purchase-order/:id/edit',
        component: ProductPurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-purchase-order/:route/:page/:id/edit',
        component: ProductPurchaseOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-purchase-order/:id/popup-edit',
        component: ProductPurchaseOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productPurchaseOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
