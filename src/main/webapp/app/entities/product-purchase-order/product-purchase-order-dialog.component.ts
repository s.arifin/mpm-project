import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductPurchaseOrder } from './product-purchase-order.model';
import { ProductPurchaseOrderPopupService } from './product-purchase-order-popup.service';
import { ProductPurchaseOrderService } from './product-purchase-order.service';
import { ToasterService } from '../../shared';
import { Vendor, VendorService } from '../vendor';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';
import { resolve } from 'dns';

@Component({
    selector: 'jhi-product-purchase-order-dialog',
    templateUrl: './product-purchase-order-dialog.component.html'
})
export class ProductPurchaseOrderDialogComponent implements OnInit {

    productPurchaseOrder: ProductPurchaseOrder;
    isSaving: boolean;
    page: number;
    idVendor: any;
    idInternal: any;
    idBillTo: any;

    vendors: Vendor[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected productPurchaseOrderService: ProductPurchaseOrderService,
        protected vendorService: VendorService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected router: Router,
        protected actR: ActivatedRoute,
    ) {
        this.page = Number(this.actR.snapshot.params['page'] || '1');
    }

    ngOnInit() {
        this.isSaving = false;
        this.vendorService.query()
            .subscribe((res: ResponseWrapper) => { this.vendors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.queryFilterBy({idRoleType: 11})
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        new Promise((resolved, reject) => {
            this.isSaving = true;
            if (this.productPurchaseOrder.idOrder !== undefined) {
                this.productPurchaseOrderService.update(this.productPurchaseOrder).subscribe((res: ProductPurchaseOrder) => {
                    this.onSaveSuccess(this.productPurchaseOrder);
                    resolved(res);
                })
            } else {
                this.productPurchaseOrderService.create(this.productPurchaseOrder).subscribe((res: ProductPurchaseOrder) => {
                    this.onSaveSuccess(this.productPurchaseOrder);
                    resolved(res);
                });
            }
        })
        .then((val: ProductPurchaseOrder) => {
            if (val) {
                this.router.navigateByUrl('/product-purchase-order/0/' + this.page + '/' + val.idOrder + '/edit', { replaceUrl: true });
            }
        });
    }

    protected onSaveSuccess(result: ProductPurchaseOrder) {
        this.eventManager.broadcast({ name: 'productPurchaseOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productPurchaseOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productPurchaseOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackVendorById(index: number, item: Vendor) {
        return item.idVendor;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-product-purchase-order-popup',
    template: ''
})
export class ProductPurchaseOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productPurchaseOrderPopupService: ProductPurchaseOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productPurchaseOrderPopupService
                    .open(ProductPurchaseOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.productPurchaseOrderPopupService.parent = params['parent'];
                this.productPurchaseOrderPopupService
                    .open(ProductPurchaseOrderDialogComponent as Component);
            } else {
                this.productPurchaseOrderPopupService
                    .open(ProductPurchaseOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
