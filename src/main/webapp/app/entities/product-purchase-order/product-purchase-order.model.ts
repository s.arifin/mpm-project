import { BaseEntity } from './../../shared';

export class ProductPurchaseOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idOrder?: any,
        public vendorId?: any,
        public internalId?: any,
        public billToId?: any,
    ) {
    }
}
