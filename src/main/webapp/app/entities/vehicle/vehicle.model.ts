import { BaseEntity } from './../../shared';

export class Vehicle implements BaseEntity {
    constructor(
        public id?: any,
        public idVehicle?: any,
        public idMachine?: string,
        public idFrame?: string,
        public yearOfAssembly?: number,
        public productId?: any,
        public productName?: string,
        public featureId?: any,
    ) {
    }
}
