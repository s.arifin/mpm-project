import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Vehicle} from './vehicle.model';
import {VehiclePopupService} from './vehicle-popup.service';
import {VehicleService} from './vehicle.service';
import {ToasterService} from '../../shared';
import { Good, GoodService } from '../good';
import { Feature, FeatureService } from '../feature';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-vehicle-dialog',
    templateUrl: './vehicle-dialog.component.html'
})
export class VehicleDialogComponent implements OnInit {

    vehicle: Vehicle;
    isSaving: boolean;

    goods: Good[];

    features: Feature[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected vehicleService: VehicleService,
        protected goodService: GoodService,
        protected featureService: FeatureService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.goodService.query({ page: 0, size: 800, sort: ['idProduct', 'asc'] })
            .subscribe((res: ResponseWrapper) => { this.goods = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query({
            page: 0, size: 1000,
            sort: ['idFeature,asc']} )
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vehicle.idVehicle !== undefined) {
            this.subscribeToSaveResponse(
                this.vehicleService.update(this.vehicle));
        } else {
            this.subscribeToSaveResponse(
                this.vehicleService.create(this.vehicle));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Vehicle>) {
        result.subscribe((res: Vehicle) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Vehicle) {
        this.eventManager.broadcast({ name: 'vehicleListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'vehicle saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'vehicle Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackGoodById(index: number, item: Good) {
        return item.idProduct;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }
}

@Component({
    selector: 'jhi-vehicle-popup',
    template: ''
})
export class VehiclePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected vehiclePopupService: VehiclePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vehiclePopupService
                    .open(VehicleDialogComponent as Component, params['id']);
            } else {
                this.vehiclePopupService
                    .open(VehicleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
