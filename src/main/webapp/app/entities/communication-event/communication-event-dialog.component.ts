import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {CommunicationEvent} from './communication-event.model';
import {CommunicationEventPopupService} from './communication-event-popup.service';
import {CommunicationEventService} from './communication-event.service';
import {ToasterService} from '../../shared';
import { PurposeType, PurposeTypeService } from '../purpose-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-communication-event-dialog',
    templateUrl: './communication-event-dialog.component.html'
})
export class CommunicationEventDialogComponent implements OnInit {

    communicationEvent: CommunicationEvent;
    isSaving: boolean;

    purposes: PurposeType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected communicationEventService: CommunicationEventService,
        protected purposeTypeService: PurposeTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.purposeTypeService
            .query({filter: 'event-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.communicationEvent.purposeId) {
                    this.purposes = res.json;
                } else {
                    this.purposeTypeService
                        .find(this.communicationEvent.purposeId)
                        .subscribe((subRes: PurposeType) => {
                            this.purposes = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.communicationEvent.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventService.update(this.communicationEvent));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventService.create(this.communicationEvent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEvent>) {
        result.subscribe((res: CommunicationEvent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: CommunicationEvent) {
        this.eventManager.broadcast({ name: 'communicationEventListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEvent saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'communicationEvent Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPurposeTypeById(index: number, item: PurposeType) {
        return item.idPurposeType;
    }
}

@Component({
    selector: 'jhi-communication-event-popup',
    template: ''
})
export class CommunicationEventPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected communicationEventPopupService: CommunicationEventPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.communicationEventPopupService
                    .open(CommunicationEventDialogComponent as Component, params['id']);
            } else {
                this.communicationEventPopupService
                    .open(CommunicationEventDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
