import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CommunicationEvent } from './communication-event.model';
import { CommunicationEventService } from './communication-event.service';
import { ToasterService} from '../../shared';

import { PurposeType, PurposeTypeService } from '../purpose-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-communication-event-edit',
    templateUrl: './communication-event-edit.component.html'
})
export class CommunicationEventEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    communicationEvent: CommunicationEvent;
    isSaving: boolean;

    purposes: PurposeType[];

    constructor(
        protected alertService: JhiAlertService,
        protected communicationEventService: CommunicationEventService,
        protected purposeTypeService: PurposeTypeService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.communicationEvent = new CommunicationEvent();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.load(params['id']);
            }
        });
        this.isSaving = false;
        this.purposeTypeService
            .query({filter: 'event-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.communicationEvent.purposeId) {
                    this.purposes = res.json;
                } else {
                    this.purposeTypeService
                        .find(this.communicationEvent.purposeId)
                        .subscribe((subRes: PurposeType) => {
                            this.purposes = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load(id) {
        this.communicationEventService.find(id).subscribe((communicationEvent) => {
            this.communicationEvent = communicationEvent;
        });
    }

    previousState() {
        this.router.navigate(['communication-event']);
    }

    save() {
        this.isSaving = true;
        if (this.communicationEvent.idCommunicationEvent !== undefined) {
            this.subscribeToSaveResponse(
                this.communicationEventService.update(this.communicationEvent));
        } else {
            this.subscribeToSaveResponse(
                this.communicationEventService.create(this.communicationEvent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<CommunicationEvent>) {
        result.subscribe((res: CommunicationEvent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: CommunicationEvent) {
        this.eventManager.broadcast({ name: 'communicationEventListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'communicationEvent saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'communicationEvent Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPurposeTypeById(index: number, item: PurposeType) {
        return item.idPurposeType;
    }
}
