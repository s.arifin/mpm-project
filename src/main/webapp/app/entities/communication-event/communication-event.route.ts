import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CommunicationEventComponent } from './communication-event.component';
import { CommunicationEventEditComponent } from './communication-event-edit.component';
import { CommunicationEventPopupComponent } from './communication-event-dialog.component';

@Injectable()
export class CommunicationEventResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCommunicationEvent,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const communicationEventRoute: Routes = [
    {
        path: 'communication-event',
        component: CommunicationEventComponent,
        resolve: {
            'pagingParams': CommunicationEventResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEvent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const communicationEventPopupRoute: Routes = [
    {
        path: 'communication-event-new',
        component: CommunicationEventEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEvent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event/:id/edit',
        component: CommunicationEventEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEvent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'communication-event-popup-new',
        component: CommunicationEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'communication-event/:id/popup-edit',
        component: CommunicationEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.communicationEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
