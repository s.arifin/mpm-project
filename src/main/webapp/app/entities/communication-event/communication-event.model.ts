import { BaseEntity } from './../../shared';

export class CommunicationEvent implements BaseEntity {
    constructor(
        public id?: any,
        public idCommunicationEvent?: any,
        public eventTypeId?: any,
        public note?: string,
        public purposeId?: any,
        public DateFrom?: any,
        public DateThru?: any,
    ) {
    }
}

export class CommunicationEventInternal extends CommunicationEvent {
    constructor(
        public id?: any,
        public IdInternal?: any,
        public DateCreate?: any,
        public CreatedBy?: string,
        public EventName?: string,
    ) {
        super();
    }
}
