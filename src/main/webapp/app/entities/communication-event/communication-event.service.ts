import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { CommunicationEvent } from './communication-event.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class CommunicationEventService {
    protected itemValues: CommunicationEvent[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/communication-events';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/communication-events';

    constructor(protected http: Http) { }

    create(communicationEvent: CommunicationEvent): Observable<CommunicationEvent> {
        const copy = this.convert(communicationEvent);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(communicationEvent: CommunicationEvent): Observable<CommunicationEvent> {
        const copy = this.convert(communicationEvent);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<CommunicationEvent> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(communicationEvent: CommunicationEvent, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(communicationEvent);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, communicationEvent: CommunicationEvent): Observable<CommunicationEvent> {
        const copy = this.convert(communicationEvent);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, communicationEvents: CommunicationEvent[]): Observable<CommunicationEvent[]> {
        const copy = this.convertList(communicationEvents);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(communicationEvent: CommunicationEvent): CommunicationEvent {
        if (communicationEvent === null || communicationEvent === {}) {
            return {};
        }
        // const copy: CommunicationEvent = Object.assign({}, communicationEvent);
        const copy: CommunicationEvent = JSON.parse(JSON.stringify(communicationEvent));
        return copy;
    }

    protected convertList(communicationEvents: CommunicationEvent[]): CommunicationEvent[] {
        const copy: CommunicationEvent[] = communicationEvents;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: CommunicationEvent[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
