import { BaseEntity } from './../../shared';
import { PostalAddress } from './../postal-address';
import { PartyRole } from './../party-role';
import { Person } from './../person';

export class SalesBroker extends PartyRole {
    constructor(
        public idPartyRole?: any,
        public idBroker?: string,
        public brokerTypeId?: number,
        public person?: Person,
    ) {
        super();
        this.person = new Person();
    }
}
