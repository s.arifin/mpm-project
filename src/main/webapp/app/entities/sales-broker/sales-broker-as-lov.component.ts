import {Component, OnInit, OnChanges, OnDestroy} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription, Observable} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {SalesBroker} from './sales-broker.model';
import {SalesBrokerService} from './sales-broker.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {SalesBrokerPopupService} from './sales-broker-popup.service';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-sales-broker-as-lov',
    templateUrl: './sales-broker-as-lov.component.html'
})
export class SalesBrokerAsLovComponent implements OnInit, OnDestroy {

    PARAMETER: Object;

    currentAccount: any;
    isSaving: boolean;
    salesBrokers: SalesBroker[];
    salesBroker: SalesBroker;
    selected: SalesBroker[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    isLovForm = true;

    constructor(
        public activeModal: NgbActiveModal,
        protected salesBrokerService: SalesBrokerService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idPartyRole';
        this.reverse = 'asc';
        this.isSaving = false;
        this.salesBroker = new SalesBroker();
    }

    loadAll() {
        if (this.currentSearch) {
            this.salesBrokerService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.salesBrokerService.query({
            idbrokertype: this.PARAMETER['idBrokerType'],
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    ngOnDestroy() {
    }

    trackId(index: number, item: SalesBroker) {
        return item.idPartyRole;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPartyRole') {
            result.push('idPartyRole');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.salesBrokers = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    pushData() {
        this.salesBrokerService.pushItems(this.selected);
        this.activeModal.dismiss('close');
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    clearPage() {
        this.activeModal.dismiss('cancel');
    }

    // ADD DATA

    newData() {
        this.isLovForm = false;
        this.isSaving = false;
        this.salesBroker = new SalesBroker();
    }

    backToLov() {
        this.isLovForm = true;
    }

    save() {
        this.isSaving = true;
        this.salesBroker.brokerTypeId = this.PARAMETER['idBrokerType'];

        this.subscribeToSaveResponse(
                this.salesBrokerService.create(this.salesBroker)
        ).then(
            () => {
                this.loadAll();
                this.isLovForm = true;
                this.isSaving = false;
            }
        )
    }

    protected subscribeToSaveResponse(result: Observable<SalesBroker>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: SalesBroker) => {
                        resolve();
                    });
            }
        );
    }
}

@Component({
    selector: 'jhi-sales-broker-lov-popup',
    template: ''
})
export class SalesBrokerLovPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected salesBrokerPopupService: SalesBrokerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['idBrokerType']) {
                const data: Object = {
                    'idBrokerType' : params['idBrokerType']
                }
                this.salesBrokerPopupService.load(SalesBrokerAsLovComponent as Component, data);
            } else {
                this.salesBrokerPopupService.load(SalesBrokerAsLovComponent as Component, null);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
