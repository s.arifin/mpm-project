import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {SalesBroker} from './sales-broker.model';
import {SalesBrokerPopupService} from './sales-broker-popup.service';
import {SalesBrokerService} from './sales-broker.service';
import {ToasterService} from '../../shared';

import { Person } from '../person';

import { BrokerType, BrokerTypeService } from '../broker-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sales-broker-dialog',
    templateUrl: './sales-broker-dialog.component.html'
})
export class SalesBrokerDialogComponent implements OnInit {

    salesBroker: SalesBroker;
    isSaving: boolean;
    brokerTypes: BrokerType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected salesBrokerService: SalesBrokerService,
        protected brokerTypeService: BrokerTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.brokerTypeService.query().subscribe((res: ResponseWrapper) => { this.brokerTypes = res.json; },
            (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.salesBroker.idPartyRole !== undefined) {
            this.subscribeToSaveResponse(
                this.salesBrokerService.update(this.salesBroker));
        } else {
            this.subscribeToSaveResponse(
                this.salesBrokerService.create(this.salesBroker));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SalesBroker>) {
        result.subscribe((res: SalesBroker) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: SalesBroker) {
        this.eventManager.broadcast({ name: 'salesBrokerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesBroker saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'salesBroker Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBrokerTypeById(index: number, item: BrokerType) {
        return item.idBrokerType;
    }

}

@Component({
    selector: 'jhi-sales-broker-popup',
    template: ''
})
export class SalesBrokerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected salesBrokerPopupService: SalesBrokerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salesBrokerPopupService
                    .open(SalesBrokerDialogComponent as Component, params['id']);
            } else {
                this.salesBrokerPopupService
                    .open(SalesBrokerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
