import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalesBrokerComponent } from './sales-broker.component';
import { SalesBrokerLovPopupComponent } from './sales-broker-as-lov.component';
import { SalesBrokerPopupComponent } from './sales-broker-dialog.component';

@Injectable()
export class SalesBrokerResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPartyRole,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const salesBrokerRoute: Routes = [
    {
        path: 'sales-broker',
        component: SalesBrokerComponent,
        resolve: {
            'pagingParams': SalesBrokerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesBroker.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salesBrokerPopupRoute: Routes = [
    {
        path: 'sales-broker-lov/:idBrokerType',
        component: SalesBrokerLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-broker-new',
        component: SalesBrokerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesBroker.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-broker/:id/edit',
        component: SalesBrokerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesBroker.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
