import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SimulasiCredit } from './simulasi-credit.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SimulasiCreditService {
    protected itemValues: SimulasiCredit[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/simulasi-create';
    protected resourceSearchUrl = 'api/_search/simulasi-create';

    constructor(protected http: Http) { }

    create(simulasiCredit: SimulasiCredit): Observable<SimulasiCredit> {
        const copy = this.convert(simulasiCredit);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(simulasiCredit: SimulasiCredit): Observable<SimulasiCredit> {
        const copy = this.convert(simulasiCredit);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SimulasiCredit> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, simulasiCredit: SimulasiCredit): Observable<SimulasiCredit> {
        const copy = this.convert(simulasiCredit);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, simulasiCredits: SimulasiCredit[]): Observable<SimulasiCredit[]> {
        const copy = this.convertList(simulasiCredits);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(simulasiCredit: SimulasiCredit): SimulasiCredit {
        if (simulasiCredit === null || simulasiCredit === {}) {
            return {};
        }
        // const copy: simulasiCredit = Object.assign({}, simulasiCredit);
        const copy: SimulasiCredit = JSON.parse(JSON.stringify(simulasiCredit));
        return copy;
    }

    protected convertList(simulasiCredits: SimulasiCredit[]): SimulasiCredit[] {
        const copy: SimulasiCredit[] = simulasiCredits;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SimulasiCredit[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
