import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SimulasiCreditComponent } from './simulasi-credit.component';

@Injectable()
export class SimulasiCreditResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSimulasiCredit,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const simulasiCreditRoute: Routes = [
    {
        path: 'simulasi-credit',
        component: SimulasiCreditComponent,
        resolve: {
            'pagingParams': SimulasiCreditResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.simulasiCredit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const simulasiCreditPopupRoute: Routes = [
    {
        path: 'simulasi-credit-new',
        component: SimulasiCreditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.simulasiCredit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'simulasi-credit/:id/edit',
        component: SimulasiCreditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.simulasiCredit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
