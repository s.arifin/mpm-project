import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { SimulasiCredit } from './simulasi-credit.model';
import { PriceComponentService, PriceComponent } from '../price-component';
import { SimulasiCreditService } from './simulasi-credit.service';
import { Motor, MotorService } from '../motor';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../leasing-tenor-provide';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';
import { SaleType, SaleTypeService } from '../sale-type';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

import * as PriceTypeConstants from '../../shared/constants/price-type.constants';
import * as _ from 'lodash';
@Component({
    selector: 'jhi-simulasi-credit',
    templateUrl: './simulasi-credit.component.html'
})
export class SimulasiCreditComponent implements OnInit, OnDestroy {

    currentAccount: any;
    dtRequired: Date = new Date();
    simulasiCredit: SimulasiCredit;
    motors: Motor[];
    leasingCompanies: LeasingCompany[];
    leasingTenorProvides: LeasingTenorProvide[];
    tempLeasingTenorProvides: LeasingTenorProvide[];
    saletypes: SaleType[];
    tenor: LeasingTenorProvide[];
    dataMotor: any;
    dataLeasing: any;
    dataHarga: any;
    selected: string;
    tenors: Array<number>;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    installment: number;
    simulasiCreditResult: boolean;
    selectedTenor?: number = null;
    selectedLeasingTenorProvide?: LeasingTenorProvide = null;

    constructor(
        protected simulasiCreditService: SimulasiCreditService,
        protected confirmationService: ConfirmationService,
        protected motorService: MotorService,
        protected leasingTenorProvideService: LeasingTenorProvideService,
        protected leasingCompanyService: LeasingCompanyService,
        protected saleTypeService: SaleTypeService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected datePipe: DatePipe,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.simulasiCredit = new SimulasiCredit();
        this.simulasiCreditResult = true;
        this.simulasiCredit.internalId = this.principal.getIdInternal();
        this.installment = 0;
    }

    ngOnInit() {
        this.motorService.query()
        .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.leasingCompanyService.query()
        .subscribe((res: ResponseWrapper) => {this.leasingCompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query().subscribe((res: ResponseWrapper) => {
            this.saletypes = this.filterSaleType(res.json, 1);
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadAll() {
        if (this.currentSearch) {
            this.simulasiCreditService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.simulasiCreditService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/status-type'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/status-type', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    btnSimulasi() {
        this.simulasiCreditResult = false;
        const MotorData = _.find(this.motors, ['idProduct', this.simulasiCredit.productId]);

        const LeasingData = _.find(this.leasingCompanies, ['idPartyRole', this.simulasiCredit.leasingCompanyId]);

        const TenorData = _.find(this.tempLeasingTenorProvides, ['idLeasingProvide', this.simulasiCredit.leasingTenorProvideId]);

        this.dataMotor = MotorData.name;
        this.dataLeasing = LeasingData.partyName;
        const selected = TenorData.idLeasingProvide;
    }

    btnCekUnit() {
        this.simulasiCreditResult = true;
        this.simulasiCredit = new SimulasiCredit();
        this.simulasiCredit.internalId = this.principal.getIdInternal();
        this.tenors = new Array();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/status-type', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    selectMotor(isSelect?: boolean): void {
        // this.getColorByMotor(this.simulasiCredit.productId);

        if (isSelect) {
            const obj = {
                idproduct : this.simulasiCredit.productId,
                idinternal : this.simulasiCredit.internalId,
                datewhen : this.datePipe.transform(this.dtRequired.toString(), 'yyyy-MM-dd hh:mm:ss')
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    console.log(res)
                }
            )
        }
    }

    selectLeasing(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.tenors = Array();
                if (this.simulasiCredit.leasingCompanyId !== null && this.simulasiCredit.productId != null) {
                    const req = {
                        idproduct : this.simulasiCredit.productId,
                        idleasing : this.simulasiCredit.leasingCompanyId
                    };
                    this.leasingTenorProvideService.getActiveTenorByIdProductAndIdLeasing(req).subscribe(
                        (res: ResponseWrapper) => {
                            this.tempLeasingTenorProvides = res.json;
                            this.checkPastTenorProvide().then(
                                () => {
                                    this.getTenor(this.tempLeasingTenorProvides);
                                    resolve();
                                }
                            )
                        }
                    );
                } else {
                    this.simulasiCredit.leasingTenorProvideId = null;
                    this.tenors = Array();
                    this.installment = 0;
                    this.selectedTenor = null;

                    resolve();
                }
            }
        );
    }

    protected checkPastTenorProvide(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                const selectedLeasingTenorProvide: LeasingTenorProvide = _.find(this.tempLeasingTenorProvides, ['idLeasingProvide', this.simulasiCredit.leasingTenorProvideId]);
                if (selectedLeasingTenorProvide === undefined && this.selectedLeasingTenorProvide !== null) {
                    this.tempLeasingTenorProvides.push(this.selectedLeasingTenorProvide);
                }
                resolve();
            }
        )
    }

    protected getTenor(data: any): void {
        if (data.length > 0) {
            const leasingTenorOrderByTenor: LeasingTenorProvide[] = _.orderBy(data, ['tenor'], ['asc']);
            this.tenors = _.map(_.uniqBy(leasingTenorOrderByTenor, 'tenor'), function(e) {
                return e.tenor;
            });
        }
    }

    filterSaleType(data: SaleType[], parentId?: number) {
        const _arr = Array();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const obj = data[i];
                if (obj.parentId === parentId) {
                    _arr.push(obj);
                }
            }
        }
        return _arr;
    }

    // protected getColorByMotor(idProduct : string): void{
    //     if (idProduct !== null) {
    //         this.features = new Array<Feature>();
    //         let selectedProduct : Motor = _.find(this.motors, function(e) {
    //             return e.idProduct === idProduct;
    //         });
    //         this.features = selectedProduct.features;
    //     }
    // }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SimulasiCredit) {
        return item.id;
    }

    registerChangeInStatusTypes() {
        this.eventSubscriber = this.eventManager.subscribe('statusTypeListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idStatusType') {
            result.push('idStatusType');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.simulasiCredit = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.simulasiCreditService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.simulasiCreditService.update(event.data)
                .subscribe((res: SimulasiCredit) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.simulasiCreditService.create(event.data)
                .subscribe((res: SimulasiCredit) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SimulasiCredit) {
        this.toasterService.showToaster('info', 'StatusType Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.simulasiCreditService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'statusTypeListModification',
                    content: 'Deleted an statusType'
                    });
                });
            }
        });
    }

    public btnbacksimulasi(): void {
        this.router.navigate(['motor-catalog']);
    }

    public trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    public trackLeasingCompanyById(index: number, item: LeasingCompany): String {
        return item.idLeasingCompany;
    }

    public trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}
