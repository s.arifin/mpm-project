import { BaseEntity } from './../../shared';

export class SimulasiCredit implements BaseEntity {
    constructor(
        public id?: number,
        public productId?: any,
        public leasingCompanyId?: string,
        public tenor?: number,
        public tenorArray?: number,
        public leasingTenorProvideId?: string,
        public leasingDownPaymentId?: number,
        public leasingInstallmentId?: number,
        public internalId?: String,
        public saleTye?: string,
        public subsmd?: number,
        public downPayment?: number,
        public installment?: number,
        public installmentt?: number,
        public unitPrice?: number,
    ) {
    }
}
