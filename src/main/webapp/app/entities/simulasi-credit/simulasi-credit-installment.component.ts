import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { Subscription } from 'rxjs/Rx';
import { ProductDocument, ProductDocumentService } from '../product-document';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ProspectPerson } from '../prospect-person/prospect-person.model';
import { ProspectPersonService } from '../prospect-person/prospect-person.service';
import { SimulasiCredit } from './simulasi-credit.model';
import { PriceComponentService, PriceComponent } from '../price-component';
import { SimulasiCreditService } from './simulasi-credit.service';
import { Motor, MotorService } from '../motor';
import { LeasingTenorProvideService, LeasingTenorProvide } from '../leasing-tenor-provide';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';
import { SaleType, SaleTypeService } from '../sale-type';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, ToasterService, CommonUtilService} from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';

import * as PriceTypeConstants from '../../shared/constants/price-type.constants';
import * as _ from 'lodash';
@Component({
    selector: 'jhi-simulasi-credit-installment',
    templateUrl: './simulasi-credit-installment.component.html'
})
export class SimulasiCreditInstallmentComponent implements OnInit, OnDestroy {

    currentAccount: any;
    dtRequired: Date = new Date();
    simulasiCredit: SimulasiCredit;
    motors: Motor[];
    leasingCompanies: LeasingCompany[];
    leasingTenorProvides: LeasingTenorProvide[];
    tempLeasingTenorProvides: LeasingTenorProvide[];
    saletypes: SaleType[];
    leasingTenorProvide: LeasingTenorProvide;
    tenor: LeasingTenorProvide[];
    dataMotor: any;
    dataLeasing: any;
    dataTenor: any;
    dataHarga: any;
    selectedTenors: string;
    selectedDownPayments: string;
    tenors: Array<number>;
    downPayments: Array<number>;
    installments: Array<number>;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    installment: number;
    downPayment: Array<number>;
    installmentt: Array<number>;
    simulasiCreditResult: boolean;
    selectedTenor?: number = null;
    selectedDP: number = null;
    selectedLeasingTenorProvide?: LeasingTenorProvide = null;
    selectedLeasingDPProvide?: LeasingTenorProvide = null;
    name: String = null;
    cellPhone: String = null;
    nik: String = null;
    findProspect: Boolean = false;
    findPerson: Boolean = false;
    findNik: Boolean = false;
    newProspect: Boolean;
    printModal: boolean;
    prospectPerson: ProspectPerson;
    public price: number;
    public listMotors = [{label: 'Please Select', value: null}];

    constructor(
        protected simulasiCreditService: SimulasiCreditService,
        protected prospectPersonService: ProspectPersonService,
        protected commonUtilService: CommonUtilService,
        protected confirmationService: ConfirmationService,
        protected priceComponentService: PriceComponentService,
        protected motorService: MotorService,
        protected leasingTenorProvideService: LeasingTenorProvideService,
        protected leasingCompanyService: LeasingCompanyService,
        protected saleTypeService: SaleTypeService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected datePipe: DatePipe,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected principal: Principal,
        protected productDocumentService: ProductDocumentService,
        protected toasterService: ToasterService,
        private commontUtilService: CommonUtilService,
    ) {
        this.prospectPerson = new ProspectPerson();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.simulasiCredit = new SimulasiCredit();
        this.simulasiCreditResult = true;
        this.simulasiCredit.internalId = this.principal.getIdInternal();
        this.installment = 0;
    }

    ngOnInit() {
        // this.motorService.query()
        // .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.leasingCompanyService.query()
        .subscribe((res: ResponseWrapper) => {this.leasingCompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query().subscribe((res: ResponseWrapper) => {
            this.saletypes = this.filterSaleType(res.json, 1);
        }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.motors = res.json;
            this.motors.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.idProduct });
            });
        },
            (res: ResponseWrapper) => {
                this.commontUtilService.showError(res.json);
            });
    }

    loadAll() {
        if (this.currentSearch) {
            this.simulasiCreditService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.simulasiCreditService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/status-type'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/status-type', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    protected getMotorPrice(): void {
        const obj = {
            idproduct : this.simulasiCredit.productId,
            idinternal : this.simulasiCredit.internalId,
            datewhen : this.datePipe.transform(this.dtRequired.toString(), 'yyyy-MM-dd hh:mm:ss')
        };

        this.motorService.getPrice(obj).subscribe(
            (res) => {
                this.price = this.priceComponentService.countMotorPrice(res.json);
            }
        )
    }

    btnSimulasi() {
        this.simulasiCreditResult = false;
        const MotorData = _.find(this.motors, ['idProduct', this.simulasiCredit.productId]);

        const LeasingData = _.find(this.leasingCompanies, ['idPartyRole', this.simulasiCredit.leasingCompanyId]);

        const TenorData = _.find(this.tempLeasingTenorProvides, ['tenor', this.simulasiCredit.tenor]);

        const DownPaymentData = _.find(this.tempLeasingTenorProvides, ['idLeasingProvide', this.simulasiCredit.leasingDownPaymentId]);

        const InstallmentData = _.find(this.tempLeasingTenorProvides, ['idLeasingProvide', this.simulasiCredit.leasingInstallmentId]);
        this.getMotorPrice();
        this.dataMotor = MotorData.name;
        this.dataLeasing = LeasingData.partyName;
        this.dataTenor = TenorData.tenor;
        const selectedTenors = TenorData.idLeasingProvide;
        const selectedDownPayments = DownPaymentData.idLeasingProvide;
        const selectedInstallment = InstallmentData.idLeasingProvide;
    }

    btnCekUnit() {
        this.simulasiCreditResult = true;
        this.simulasiCredit = new SimulasiCredit();
        this.simulasiCredit.internalId = this.principal.getIdInternal();
        this.tenors = new Array();
        this.downPayments = new Array();
        this.installments = new Array();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/status-type', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    selectMotor(isSelect?: boolean): void {
        // this.getColorByMotor(this.simulasiCredit.productId);

        if (isSelect) {
            const obj = {
                idproduct : this.simulasiCredit.productId,
                idinternal : this.simulasiCredit.internalId,
                datewhen : this.datePipe.transform(this.dtRequired.toString(), 'yyyy-MM-dd hh:mm:ss')
            };
            this.motorService.getPrice(obj).subscribe(
                (res) => {
                    console.log(res)
                }
            )
        }
    }

    // selectTenor(): Promise<any> {
    //     return new Promise<any>(
    //         (resolve) => {
    //             this.downPayment = Array();
    //             this.installments = Array();
    //             if (this.simulasiCredit.leasingCompanyId !== null && this.simulasiCredit.productId != null && this.simulasiCredit.tenor != null) {
    //                 const req = {
    //                     idproduct : this.simulasiCredit.productId,
    //                     idleasing : this.simulasiCredit.leasingCompanyId,
    //                     Tenor : this.simulasiCredit.tenor
    //                 };
    //                 this.leasingTenorProvideService.getActiveTenorByIdProductAndIdLeasingAndTenor(req).subscribe(
    //                     (res: ResponseWrapper) => {
    //                         console.log(res)
    //                     this.tempLeasingTenorProvides = res.json;
    //                     this.checkPastTenorProvideDP().then(
    //                         () => {
    //                             this.getDownPayment(this.tempLeasingTenorProvides);
    //                             resolve();
    //                         }
    //                     )
    //                     // this.checkPastTenorProvideDP().then(
    //                     //     () => {
    //                     //         this.getInstallment(this.tempLeasingTenorProvides);
    //                     //         resolve();
    //                     //     }
    //                     // )
    //                     }
    //                 );
    //             }else {
    //                 this.simulasiCredit.leasingDownPaymentId = null;
    //                 // this.simulasiCredit.leasingInstallmentId = null;
    //                 this.downPayments = Array();
    //                 // this.installments = Array();
    //                 this.selectedDP = null;

    //                 resolve();
    //             }
    //         }
    //     );
    // }

    selectLeasing(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.tenors = Array();
                this.downPayments = Array();
                this.installments = Array();
                if (this.simulasiCredit.leasingCompanyId !== null && this.simulasiCredit.productId != null) {
                    const req = {
                        idproduct : this.simulasiCredit.productId,
                        idleasing : this.simulasiCredit.leasingCompanyId
                    };
                    this.leasingTenorProvideService.getActiveTenorByIdProductAndIdLeasing(req).subscribe(
                        (res: ResponseWrapper) => {
                            this.tempLeasingTenorProvides = res.json;
                            this.checkPastTenorProvide().then(
                                () => {
                                    this.getTenor(this.tempLeasingTenorProvides);
                                    resolve();
                                }
                            )
                            console.log('apakah data tenor masuk ?', this.getTenor)
                            this.checkPastTenorProvide().then(
                                () => {
                                    this.getDownPayment(this.tempLeasingTenorProvides);
                                    resolve();
                                }
                            )
                            this.checkPastTenorProvide().then(
                                () => {
                                    this.getInstallment(this.tempLeasingTenorProvides);
                                    resolve();
                                }
                            )
                        }
                    );
                }else {
                    this.simulasiCredit.leasingTenorProvideId = null;
                    this.simulasiCredit.leasingDownPaymentId = null;
                    this.simulasiCredit.leasingInstallmentId = null;
                    this.tenors = Array();
                    this.downPayments = Array();
                    this.installments = Array();
                    this.selectedTenor = null;

                    resolve();
                }
            }
        );
    }

    protected checkPastTenorProvide(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                const selectedLeasingTenorProvide: LeasingTenorProvide = _.find(this.tempLeasingTenorProvides, ['idLeasingProvide', this.simulasiCredit.leasingTenorProvideId]);
                if (selectedLeasingTenorProvide === undefined && this.selectedLeasingTenorProvide !== null) {
                    this.tempLeasingTenorProvides.push(this.selectedLeasingTenorProvide);
                }
                resolve();
            }
        )
    }

    protected checkPastTenorProvideDP(): Promise<void> {
        return new Promise<void>(
            (resolve) => {
                const selectedLeasingDPProvide: LeasingTenorProvide = _.find(this.tempLeasingTenorProvides, ['tenor', this.simulasiCredit.tenor]);
                if (selectedLeasingDPProvide === undefined && this.selectedLeasingDPProvide !== null) {
                    this.tempLeasingTenorProvides.push(this.selectedLeasingDPProvide);
                }
                resolve();
            }
        )
    }

    protected getTenor(data: any): void {
        if (data.length > 0) {
            const leasingTenorOrderByTenor: LeasingTenorProvide[] = _.orderBy(data, ['tenor'], ['asc']);
            this.tenors = _.map(_.uniqBy(leasingTenorOrderByTenor, 'tenor'), function(e) {
                return e.tenor;
            });
        }
    }

    protected getDownPayment(data: any): void {
        if (data.length > 0) {
            const leasingDPOrderByDP: LeasingTenorProvide[] = _.orderBy(data, ['downPayment'], ['asc']);
            this.downPayments = _.map(_.uniqBy(leasingDPOrderByDP, 'downPayment'), function(e) {
                return e.downPayment;
            });
        }
    }

    protected getInstallment(data: any): void {
        if (data.length > 0) {
            const leasingDPOrderByInstallment: LeasingTenorProvide[] = _.orderBy(data, ['installment'], ['asc']);
            this.installments = _.map(_.uniqBy(leasingDPOrderByInstallment, 'installment'), function(e) {
                return e.installment;
            });
        }
    }

    filterSaleType(data: SaleType[], parentId?: number) {
        const _arr = Array();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const obj = data[i];
                if (obj.parentId === parentId) {
                    _arr.push(obj);
                }
            }
        }
        return _arr;
    }

    // protected getColorByMotor(idProduct : string): void{
    //     if (idProduct !== null) {
    //         this.features = new Array<Feature>();
    //         let selectedProduct : Motor = _.find(this.motors, function(e) {
    //             return e.idProduct === idProduct;
    //         });
    //         this.features = selectedProduct.features;
    //     }
    // }

    ngOnDestroy() {
        // this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SimulasiCredit) {
        return item.id;
    }

    registerChangeInStatusTypes() {
        this.eventSubscriber = this.eventManager.subscribe('statusTypeListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idStatusType') {
            result.push('idStatusType');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.simulasiCredit = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.simulasiCreditService.executeProcess(0, null, data).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.simulasiCreditService.update(event.data)
                .subscribe((res: SimulasiCredit) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.simulasiCreditService.create(event.data)
                .subscribe((res: SimulasiCredit) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: SimulasiCredit) {
        this.toasterService.showToaster('info', 'StatusType Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.simulasiCreditService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'statusTypeListModification',
                    content: 'Deleted an statusType'
                    });
                });
            }
        });
    }

    public btnbacksimulasi(): void {
        this.router.navigate(['motor-catalog']);
    }

    public trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }

    public trackLeasingCompanyById(index: number, item: LeasingCompany): String {
        return item.idLeasingCompany;
    }

    public trackTenorById(index: number, item: LeasingTenorProvide): Number {
        return item.tenor;
    }

    public trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }

    submitBAST() {
        this.prospectPerson = new ProspectPerson();
        this.newProspect = true;
    }

    doPrintBAST() {
        this.newProspect = false;
        this.doCheckProspect()
    }

    doCheckProspect() {
        const checkProspect = new ProspectPerson();

        if (this.name.split(' ').length === 1) {
            checkProspect.person.firstName = this.name;
            checkProspect.person.lastName = '';
        } else {
            checkProspect.person.firstName = this.name.split(' ').slice(0, -1).join(' ');
            checkProspect.person.lastName = this.name.split(' ').slice(-1).join(' ');
        }

        checkProspect.person.cellPhone1 = this.cellPhone;

        this.prospectPersonService.executeProcess(105, this.principal.getIdInternal(), checkProspect).subscribe(
        (response) => {
            // Jika Prpspeck Ditemukan
            if (response.person.getIdInternal != null && response.person.getIdInternal !== undefined) {
                this.prospectPerson = response;
                if (this.prospectPerson.person.lastName == null) {
                    this.name = this.prospectPerson.person.firstName;
                } else {
                    this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                }
                this.cellPhone = this.prospectPerson.person.cellPhone1;
                this.findPerson = true;
                this.newProspect = false;
            // Jika Prospect Tidak Ditemukan Namun, Data Orang Ditemukan
            } else if (response.idProspect != null) {
                    this.prospectPerson = response;
                    if (this.prospectPerson.person.lastName == null) {
                        this.name = this.prospectPerson.person.firstName;
                    } else {
                        this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                    }
                    this.cellPhone = this.prospectPerson.person.cellPhone1;
                    this.findProspect = true;
                    this.newProspect = false;
                } else if (response.idProspect != null) {
                    this.prospectPerson = response;
                    if (this.prospectPerson.person.lastName == null) {
                        this.name = this.prospectPerson.person.firstName;
                    } else {
                        this.name = this.prospectPerson.person.firstName + ' ' + this.prospectPerson.person.lastName;
                    }
                    this.cellPhone = this.prospectPerson.person.cellPhone1;
                    this.findNik = true;
                    this.newProspect = false;
            // Jika Data Tidak Ditemukan
            } else {
                const newProspectData = new Array<ProspectPerson>();
                newProspectData.push(checkProspect);
                this.prospectPersonService.pushItems(newProspectData);
                this.clearOldData();
                this.router.navigate(['../prospect-person-new']);
            }
        },
        (err) => {
            this.commonUtilService.showError(err);
        });
        // () => {
            // const newProspectData = new Array<ProspectPerson>();
            // newProspectData.push(checkProspect);
            // this.prospectPersonService.pushItems(newProspectData);
            // this.clearOldData();
            // this.router.navigate(['../prospect-person-new']);
        // });
    }

    public doFollowUp(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/step']);
    }

    public doEdit(): void {
        this.clearOldData();
        this.router.navigate(['../prospect-person/' + this.prospectPerson.idProspect + '/edit']);
    }

    public backToFind(): void {
        this.clearOldData();
        this.newProspect = true;
    }

    clearOldData() {
        this.newProspect = false;
        this.findProspect = false;
        this.findPerson = false;
        this.findNik = false;
        this.name = null;
        this.cellPhone = null;
        this.nik = null;
    }
}
