import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { OrganizationCustomerComponent } from './organization-customer.component';
import { OrganizationCustomerPopupComponent } from './organization-customer-dialog.component';
import { OrganizationCostumerUploadPopupComponent } from './organization-costumer-upload-dialog.component';

@Injectable()
export class OrganizationCustomerResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCustomer,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const organizationCustomerRoute: Routes = [
    {
        path: 'organization-customer',
        component: OrganizationCustomerComponent,
        resolve: {
            'pagingParams': OrganizationCustomerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.organizationCustomer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const organizationCustomerPopupRoute: Routes = [
    {
        path: 'organization-customer-new',
        component: OrganizationCustomerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.organizationCustomer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'organization-customer/:id/edit',
        component: OrganizationCustomerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.organizationCustomer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'organization-costumer-popup-new-upload',
        component: OrganizationCostumerUploadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.organizationCustomer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
