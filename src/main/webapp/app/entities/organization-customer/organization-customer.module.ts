import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    OrganizationCustomerService,
    OrganizationCustomerPopupService,
    OrganizationCustomerComponent,
    OrganizationCustomerDialogComponent,
    OrganizationCustomerPopupComponent,
    organizationCustomerRoute,
    organizationCustomerPopupRoute,
    OrganizationCustomerResolvePagingParams,
    OrganizationCostumerUploadPopupComponent,
    OrganizationCostumerUploadDialogComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         CalendarModule,
         DropdownModule,
         ButtonModule,
         DataTableModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         DataGridModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         SelectItem,
         MenuItem,
         Header,
         Footer} from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...organizationCustomerRoute,
    ...organizationCustomerPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        OrganizationCustomerComponent
    ],
    declarations: [
        OrganizationCustomerComponent,
        OrganizationCustomerDialogComponent,
        OrganizationCustomerPopupComponent,
        OrganizationCostumerUploadPopupComponent,
        OrganizationCostumerUploadDialogComponent,
    ],
    entryComponents: [
        OrganizationCustomerComponent,
        OrganizationCustomerDialogComponent,
        OrganizationCustomerPopupComponent,
        OrganizationCostumerUploadPopupComponent,
        OrganizationCostumerUploadDialogComponent,
    ],
    providers: [
        OrganizationCustomerService,
        OrganizationCustomerPopupService,
        OrganizationCustomerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmOrganizationCustomerModule {}
