import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {OrganizationCustomer} from './organization-customer.model';
import {OrganizationCustomerPopupService} from './organization-customer-popup.service';
import {OrganizationCustomerService} from './organization-customer.service';
import {ToasterService, Principal} from '../../shared';
import { Party, PartyService } from '../party';
import { ResponseWrapper } from '../../shared';
import { PersonalCustomer, PersonalCustomerService } from '../shared-component';
import { Person, PersonService } from '../person';
import { WorkTypeService, WorkType } from '../work-type';
import { ReligionTypeService, ReligionType } from '../religion-type';

@Component({
    selector: 'jhi-organization-customer-dialog',
    templateUrl: './organization-customer-dialog.component.html'
})
export class OrganizationCustomerDialogComponent implements OnInit {

    organizationCustomer: OrganizationCustomer;
    isSaving: boolean;
    public customer: PersonalCustomer[];
    parties: Party[];
    person: Person;
    religiontypes: ReligionType[];
    worktypes: WorkType[];

    genders: Array<object> = [
        {label : 'Pria', value : 'P'},
        {label : 'Wanita', value : 'W'}
    ];

    bloodTypes: Array<object> = [
        {label : 'O', value : 'O'},
        {label : 'A', value : 'A'},
        {label : 'B', value : 'B'},
        {label : 'AB', value : 'AB'}
    ];
    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected organizationCustomerService: OrganizationCustomerService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected personalCustomerService: PersonalCustomerService,
        protected personService: PersonService,
        private workTypeService: WorkTypeService,
        private religionTypeService: ReligionTypeService,
        private principal: Principal
    ) {
        this.customer = new Array<PersonalCustomer>();
        this.person = new Person();
    }

    ngOnInit() {
        this.isSaving = false;
        // console.log('org_custdate', this.organizationCustomer);
        // if (this.organizationCustomer.idCustomer !== undefined) {
        //     this.organizationCustomer.organization.dateOrganizationEstablishdate = this.organizationCustomer.organization.dateOrganizationEstablish;
        // }
        // this.partyService.query()
        //     .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

        //     this.personalCustomerService.query({
        //         size: 99999,
        //         idInternal: this.principal.getIdInternal()
        //     })
        //     .subscribe(
        //         (res: ResponseWrapper) => { this.customer = res.json; },
        //         (res: ResponseWrapper) => this.onError(res.json)
        //     );

        //     this.religionTypeService.query({size: 100})
        //     .subscribe((res: ResponseWrapper) => { this.religiontypes = res.json; });
        // this.workTypeService.query({size: 100})
        //     .subscribe((res: ResponseWrapper) => { this.worktypes = res.json; });

            // this.loadPerson();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.organizationCustomer.organization.dateOrganizationEstablish = new Date(this.organizationCustomer.organization.dateOrganizationEstablish);
        this.isSaving = true;
        this.organizationCustomer.idInternal = this.principal.getIdInternal();
        if (this.organizationCustomer.idCustomer !== undefined) {
            console.log('ikilo', this.organizationCustomer);
            this.subscribeToSaveResponse(
                this.organizationCustomerService.updateC(this.organizationCustomer));
        } else {
            this.subscribeToSaveResponse(
                this.organizationCustomerService.createC(this.organizationCustomer));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrganizationCustomer>) {
        result.subscribe((res: OrganizationCustomer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: OrganizationCustomer) {
        this.organizationCustomerService.newCustomerRegister(result);
        this.eventManager.broadcast({ name: 'organizationCustomerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'organizationCustomer saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'organizationCustomer Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }

    // loadPerson() {
    //     this.personService.find(this.organizationCustomer.organization.idPic)
    //     .subscribe(
    //         (res) => {
    //             this.person = res;
    //             console.log('person di organization ', this.person);
    //         }
    //     )
    // }

    getProvince(province: any) {
        this.person.postalAddress.provinceId = province;
    }

    getDistrict(district: any) {
        this.person.postalAddress.districtId = district;
    }

    getCity(city: any) {
        this.person.postalAddress.cityId = city;
    }

    getVillage(village: any) {
        this.person.postalAddress.villageId = village;
    }

    trackReligionTypeById(index: number, item: ReligionType) {
        return item.idReligionType;
    }

    trackWorkTypeById(index: number, item: WorkType) {
        return item.idWorkType;
    }

}

@Component({
    selector: 'jhi-organization-customer-popup',
    template: ''
})
export class OrganizationCustomerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected organizationCustomerPopupService: OrganizationCustomerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.organizationCustomerPopupService
                    .open(OrganizationCustomerDialogComponent as Component, params['id']);
            } else {
                this.organizationCustomerPopupService
                    .open(OrganizationCustomerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
