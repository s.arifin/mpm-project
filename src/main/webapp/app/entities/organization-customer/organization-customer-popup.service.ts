import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationCustomer } from './organization-customer.model';
import { OrganizationCustomerService } from './organization-customer.service';

@Injectable()
export class OrganizationCustomerPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected organizationCustomerService: OrganizationCustomerService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: String | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.organizationCustomerService.findC(id).subscribe((data) => {
                    this.ngbModalRef = this.organizationCustomerModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new OrganizationCustomer();
                    this.ngbModalRef = this.organizationCustomerModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    organizationCustomerModalRef(component: Component, organizationCustomer: OrganizationCustomer): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.organizationCustomer = organizationCustomer;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
