import { BaseEntity } from './../shared-component';
import { PartyRole } from './../party-role';
import { Organization } from './../organization';
import { Customer } from './../customer/';

export class OrganizationCustomer extends Customer {
    constructor(
        public idCustomer?: string,
        public organization?: Organization,
        public idInternal?: string
    ) {
        super();
        this.organization = new Organization();
    }
}
