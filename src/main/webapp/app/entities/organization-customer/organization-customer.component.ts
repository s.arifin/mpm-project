import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {OrganizationCustomer} from './organization-customer.model';
import {ReportUtilService} from '../../shared/report/report-util.service';
import {OrganizationCustomerService} from './organization-customer.service';
import {ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {PaginationConfig} from '../../blocks/config/uib-pagination.config';

import {LazyLoadEvent} from 'primeng/primeng';
import {ToasterService} from '../../shared/alert/toaster.service';
import {ConfirmationService} from 'primeng/primeng';

@Component({
    selector: 'jhi-organization-customer',
    templateUrl: './organization-customer.component.html'
})
export class OrganizationCustomerComponent implements OnInit, OnDestroy {

    currentAccount: any;
    organizationCustomers: OrganizationCustomer[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    internalId: any;

    constructor(
        protected organizationCustomerService: OrganizationCustomerService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected report: ReportUtilService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.organizationCustomerService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.organizationCustomerService.queryC({
            page: this.page - 1,
            size: this.itemsPerPage,
            idinternal : this.internalId ,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/organization-customer'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/organization-customer', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/organization-customer', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.internalId = this.principal.getIdInternal();
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInOrganizationCustomers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: OrganizationCustomer) {
        return item.idCustomer;
    }

    registerChangeInOrganizationCustomers() {
        this.eventSubscriber = this.eventManager.subscribe('organizationCustomerListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idCustomer') {
            result.push('idCustomer');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        this.totalItems = data[0].TotalData;
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.organizationCustomers = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.organizationCustomerService.executeProcess(data, 0).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.organizationCustomerService.update(event.data)
                .subscribe((res: OrganizationCustomer) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.organizationCustomerService.create(event.data)
                .subscribe((res: OrganizationCustomer) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: OrganizationCustomer) {
        this.toasterService.showToaster('info', 'OrganizationCustomer Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.organizationCustomerService.delete(id).subscribe((response) => {
                this.eventManager.broadcast({
                    name: 'organizationCustomerListModification',
                    content: 'Deleted an organizationCustomer'
                    });
                });
            }
        });
    }

    print() {
        this.report.viewFile('/api/report/test/pdf');
    }

    buildReindex() {
        this.organizationCustomerService.process({command: 'buildIndex'}).subscribe((r) => {
            this.toasterService.showToaster('info', 'Build Index', 'Build Index processed.....');
        });
    }
}
