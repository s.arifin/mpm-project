import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { OrganizationCustomer } from './organization-customer.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Subject } from 'rxjs/Subject';

import * as moment from 'moment';

@Injectable()
export class OrganizationCustomerService {

    protected resourceUrl = 'api/organization-customers';
    protected resourceSearchUrl = 'api/_search/organization-customers';
    protected resourceCUrl = process.env.API_C_URL + '/api/organization_customer';

    newCustomer: Subject<any> = new Subject();

    constructor(protected http: Http) {}

    create(organizationCustomer: OrganizationCustomer): Observable<OrganizationCustomer> {
        const copy = this.convert(organizationCustomer);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    createC(organizationCustomer: OrganizationCustomer): Observable<OrganizationCustomer> {
        const copy = this.convert(organizationCustomer);
        return this.http.post(this.resourceCUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(organizationCustomer: OrganizationCustomer): Observable<OrganizationCustomer> {
        const copy = this.convert(organizationCustomer);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    updateC(organizationCustomer: OrganizationCustomer): Observable<OrganizationCustomer> {
        const copy = this.convert(organizationCustomer);
        return this.http.put(this.resourceCUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<OrganizationCustomer> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findC(id: any): Observable<OrganizationCustomer> {
        return this.http.get(this.resourceCUrl + '/getById?idCust=' + id).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl  + '/GetData', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(organizationCustomer: any, id: number): Observable<String> {
        const copy = this.convert(organizationCustomer);
        return this.http.post(`${this.resourceUrl}/execute/${id}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(organizationCustomer: OrganizationCustomer): OrganizationCustomer {
        const copy: OrganizationCustomer = Object.assign({}, organizationCustomer);
        return copy;
    }

    newCustomerRegister(customer: OrganizationCustomer) {
        this.newCustomer.next(customer);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
