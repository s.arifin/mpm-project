import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    TransferUnitService,
    TransferUnitPopupService,
    TransferUnitComponent,
    TransferUnitDetailComponent,
    TransferUnitDialogComponent,
    TransferUnitPopupComponent,
    TransferUnitDeletePopupComponent,
    TransferUnitDeleteDialogComponent,
    transferUnitRoute,
    transferUnitPopupRoute,
    TransferUnitResolvePagingParams,
    RequestNoteComponent,
    TransferUnitRequestComponent,
    RequestNoteDetailComponent
} from './';

const ENTITY_STATES = [
    ...transferUnitRoute,
    ...transferUnitPopupRoute,
];

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         CheckboxModule,
         InputTextModule,
         InputTextareaModule,
         CalendarModule,
         DropdownModule,
         EditorModule,
         ButtonModule,
         DataListModule,
         DataGridModule,
         DataScrollerModule,
         CarouselModule,
         PickListModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         ChartModule,
         DragDropModule,
         LightboxModule
        } from 'primeng/primeng';

import { FieldsetModule } from 'primeng/primeng';
import { DataTableModule } from 'primeng/primeng';
import { MpmSharedEntityModule } from '../shared-entity.module';

@NgModule({
    imports: [
        MpmSharedModule,
        MpmSharedEntityModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        EditorModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule
    ],
    exports: [
        TransferUnitComponent,
        RequestNoteComponent,
        TransferUnitRequestComponent,
        RequestNoteDetailComponent
    ],
    declarations: [
        TransferUnitComponent,
        TransferUnitDetailComponent,
        TransferUnitDialogComponent,
        TransferUnitDeleteDialogComponent,
        TransferUnitPopupComponent,
        TransferUnitDeletePopupComponent,
        RequestNoteComponent,
        TransferUnitRequestComponent,
        RequestNoteDetailComponent
    ],
    entryComponents: [
        TransferUnitComponent,
        TransferUnitDialogComponent,
        TransferUnitPopupComponent,
        TransferUnitDeleteDialogComponent,
        TransferUnitDeletePopupComponent,
        RequestNoteComponent,
        TransferUnitRequestComponent,
        // RequestNoteComponent,
        // RequestNoteInputComponent,
        RequestNoteDetailComponent
    ],
    providers: [
        TransferUnitService,
        TransferUnitPopupService,
        TransferUnitResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmTransferUnitModule {}
