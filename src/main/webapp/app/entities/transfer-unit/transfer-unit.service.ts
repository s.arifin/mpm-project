import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TransferUnit } from './transfer-unit.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { CustomRequestNote } from '../request';

@Injectable()
export class TransferUnitService {

    protected resourceCUrl = process.env.API_C_URL + '/api/PurchaseOrder';
    protected dummy = 'http://localhost:52374/api/PurchaseOrder';

    private resourceUrl = SERVER_API_URL + 'api/transfer-units';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/transfer-units';
    protected resourceAXUrl = process.env.API_C_URL + '/api/ax_generalledger';

    constructor(private http: Http) { }

    executeToSpgC(item: any): Observable<CustomRequestNote> {
        console.log('ini id ioder: ', item)
        const copy = JSON.stringify(item);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl + '/CreateSpg'}`, copy, options).map((res: Response) => {
            return res.json();
        });
    }

    create(transferUnit: TransferUnit): Observable<TransferUnit> {
        const copy = this.convert(transferUnit);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(transferUnit: TransferUnit): Observable<TransferUnit> {
        const copy = this.convert(transferUnit);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<TransferUnit> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(transferUnit: TransferUnit): TransferUnit {
        const copy: TransferUnit = Object.assign({}, transferUnit);
        return copy;
    }

    GLInventoryTransfer(idreqnot: any): Observable<any> {
        return this.http.get(this.resourceAXUrl + '/GLInventoryTransferIn?idreqnot=' + idreqnot).map((res: Response) => {
            return res.json();
        });
    }
}
