import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';

// nuse
import { DatePipe } from '@angular/common';
import { constants } from 'os';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LeasingCompanyService, LeasingCompany } from '../leasing-company';
import { LoadingService } from '../../layouts';
import { RuleHotItemService } from '../rule-hot-item';
import * as BaseConstant from '../../shared/constants/base.constants';
import { Request, RequestService, CustomRequestNote } from '../request';
import { TransferUnitService } from './transfer-unit.service';

@Component({
    selector: 'jhi-request-note-detail',
    templateUrl: './request-note-detail.component.html'
})
export class RequestNoteDetailComponent implements OnInit {

    idreqnot: any;
    requestNote: CustomRequestNote;
    items: CustomRequestNote[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    selectedKorsal: string;
    selectedSalesman: string;
    statusVsoes: SelectItem[];
    selectedStatusVso: Number;
    isLazyLoadingFirst: Boolean ;
    isMatchingMenu: Boolean ;
    isFiltered: Boolean;
    routeId: number;

    constructor(
        private transferUnitService: TransferUnitService,
        private requestService: RequestService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        protected commonUtilService: CommonUtilService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private leasingCompanyService: LeasingCompanyService,
        private loadingService: LoadingService,
        private ruleHotItemService: RuleHotItemService,
        private toaster: ToasterService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.tgl1 = new Date();
        this.tgl2 = new Date();

        this.requestNote = new CustomRequestNote();
        this.isFiltered = false;
        this.isMatchingMenu = false;

    }

    processToSPG() {
        this.confirmationService.confirm({
            message: 'Are you sure that you want process?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                    this.approveTransfer(this.requestNote);
                this.loadingService.loadingStart();
            }
        });
        console.log('to spg: ', this.requestNote);
    }

    approveTransfer(_item: CustomRequestNote) {
        this.subscribeToSaveResponse(
            this.transferUnitService.executeToSpgC(this.requestNote));
    }

    protected subscribeToSaveResponse(result: Observable<CustomRequestNote>): Promise<any>  {
        console.log('masuk');
        return new Promise<any>(
            (resolve) => {
                result.subscribe((res: CustomRequestNote) => {
                    this.onSaveSuccess(res);
                    resolve();
                },
                (res: Response) => {
                    this.onError(res);
                    resolve();
                });
            }
        );
    }

    protected onSaveSuccess(result: CustomRequestNote) {
        this.transferUnitService.GLInventoryTransfer(this.requestNote.idreqnot).subscribe(
            (resGL) => { },
            (err) => this.commonUtilService.showError(err)
        );
        this.eventManager.broadcast({ name: 'packageReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'Request Note saved !');
        this.previousState();
        this.loadingService.loadingStop();
    }

    // protected onSaveError(error) {
    //     try {
    //         error.json();
    //     this.onError(error);
    //     } catch (exception) {
    //         this.onError(error);
    //         error.message = error.text();
    //     }
    //     this.onError(error);
    //     this.loadingService.loadingStop();
    // }

    previousState() {
        console.log(this.itemsPerPage);
            this.router.navigate(['./transfer-unit', { page: this.page }]);
    }

    loadAll() {
        this.loadingService.loadingStart();
        this.requestService.detail(
            {
                query: 'idreqnotdest:' + this.idreqnot,
                page: this.page,
                size: this.itemsPerPage,
                }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)

                )
        console.log('ini internal di loadall', this.principal.getIdInternal())

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/request-note-detail'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
        console.log('is filter = ', this.isFiltered );
    }

    clear() {
        if (this.isMatchingMenu === true) {
            this.page = 0;
            this.currentSearching = '';
            this.router.navigate(['/request-note-detail']);
            this.loadAll();
        }

        if (this.isMatchingMenu === false) {
            this.page = 0;
            this.currentSearching = '';
            this.router.navigate(['/request-note-detail']);
            this.loadAll();
        }

    }

    ngOnInit() {

        this.activatedRoute.params.subscribe((params) => {
            if (params['idreqnotdest']) {
                this.isMatchingMenu = true;
                this.idreqnot = params['idreqnotdest'];
                console.log('idreqnotdest')
            } else {
                this.isMatchingMenu = false;
            }
        });

        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        // this.tglAwal = Date;
        // this.tglAkhir = Date;
        // this.loadLeasing();
        // this.loadKorsal()

    }

    trackleasingComById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }

    trackStatusVsoById(index: number, item: any) {
        return item.value;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateEntry') {
            result.push('dateEntry');
        }
        return result;
    }

    private onSuccess(data, headers) {
        if (data !== null) {
            // this.totalItems = data[0].Totaldata;
            // this.queryCount = this.totalItems;
            this.items = data;
            this.requestNote = data[0];
        }
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.toaster.showToaster('info', 'Save', 'Data Belum lengkap');
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
}
