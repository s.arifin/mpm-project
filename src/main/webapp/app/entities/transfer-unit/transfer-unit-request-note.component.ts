import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import {Observable} from 'rxjs/Rx';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem} from 'primeng/primeng';
import { Request, RequestService, CustomRequestNote } from '../request';
import { Internal } from '../internal';
import { UnitDocumentMessageService} from '../unit-document-message';

@Component({
    selector: 'jhi-transfer-unit-request-note',
    templateUrl: './transfer-unit-request-note.component.html'
})
export class TransferUnitRequestComponent implements OnInit {

    requestNotes: CustomRequestNote[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    leasingStatusPayment: any;
    newTagihan: Boolean = false;
    newPending: Boolean = false;
    isSaving: boolean;

    leasings: SelectItem[];
    selectedLeasing: string;

    korsals: SelectItem[];
    selectedKorsal: string;
    selected: CustomRequestNote[];
    selecteds: any = [];

    constructor(
        private requestService: RequestService,
        private unitDocumentMessageService: UnitDocumentMessageService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.leasings = [];
        this.korsals = [];
    }

    loadAll(idInternal?: string) {
        this.requestService.queryFilterByinternalC({
            query: 'idInternal:' + idInternal,
            page: this.page,
            size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            )
        console.log('ini internal load all', this.principal.getIdInternal());
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    saveUploadFaktur() {};

    transition() {
        this.router.navigate(['/transfer-unit-request-note'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnInit() {
        // this.loadAllSubmit();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll(this.principal.getIdInternal());
        });
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idreqnot') {
            result.push('idreqnot');
        }
        return result;
    }

    private onSuccess(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.requestNotes = data;
            // this.router.navigate(['/approval-korsal/unit-document-message'])
        }
        console.log('ini data internal', data);
    }
    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.requestNotes = data;
            this.router.navigate(['/approval-korsal/unit-document-message'])
        }
        console.log('ini data internal', data);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll(this.principal.getIdInternal());
    }

    loadDataLazySubmit(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        // this.loadAllSubmit();
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.selecteds
        }
        this.requestService.notAvailable(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }
}
