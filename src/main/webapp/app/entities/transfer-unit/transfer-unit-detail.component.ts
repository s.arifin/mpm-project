import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TransferUnit } from './transfer-unit.model';
import { TransferUnitService } from './transfer-unit.service';

@Component({
    selector: 'jhi-transfer-unit-detail',
    templateUrl: './transfer-unit-detail.component.html'
})
export class TransferUnitDetailComponent implements OnInit, OnDestroy {

    transferUnit: TransferUnit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private transferUnitService: TransferUnitService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTransferUnits();
    }

    load(id) {
        this.transferUnitService.find(id).subscribe((transferUnit) => {
            this.transferUnit = transferUnit;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTransferUnits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'transferUnitListModification',
            (response) => this.load(this.transferUnit.id)
        );
    }
}
