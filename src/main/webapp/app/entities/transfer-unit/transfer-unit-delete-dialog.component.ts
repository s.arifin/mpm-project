import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TransferUnit } from './transfer-unit.model';
import { TransferUnitPopupService } from './transfer-unit-popup.service';
import { TransferUnitService } from './transfer-unit.service';

@Component({
    selector: 'jhi-transfer-unit-delete-dialog',
    templateUrl: './transfer-unit-delete-dialog.component.html'
})
export class TransferUnitDeleteDialogComponent {

    transferUnit: TransferUnit;

    constructor(
        private transferUnitService: TransferUnitService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.transferUnitService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'transferUnitListModification',
                content: 'Deleted an transferUnit'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-transfer-unit-delete-popup',
    template: ''
})
export class TransferUnitDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private transferUnitPopupService: TransferUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.transferUnitPopupService
                .open(TransferUnitDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
