import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TransferUnit } from './transfer-unit.model';
import { TransferUnitPopupService } from './transfer-unit-popup.service';
import { TransferUnitService } from './transfer-unit.service';

@Component({
    selector: 'jhi-transfer-unit-dialog',
    templateUrl: './transfer-unit-dialog.component.html'
})
export class TransferUnitDialogComponent implements OnInit {

    transferUnit: TransferUnit;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private transferUnitService: TransferUnitService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.transferUnit.id !== undefined) {
            this.subscribeToSaveResponse(
                this.transferUnitService.update(this.transferUnit));
        } else {
            this.subscribeToSaveResponse(
                this.transferUnitService.create(this.transferUnit));
        }
    }

    private subscribeToSaveResponse(result: Observable<TransferUnit>) {
        result.subscribe((res: TransferUnit) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TransferUnit) {
        this.eventManager.broadcast({ name: 'transferUnitListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-transfer-unit-popup',
    template: ''
})
export class TransferUnitPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private transferUnitPopupService: TransferUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.transferUnitPopupService
                    .open(TransferUnitDialogComponent as Component, params['id']);
            } else {
                this.transferUnitPopupService
                    .open(TransferUnitDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
