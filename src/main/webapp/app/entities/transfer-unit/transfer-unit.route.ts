import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TransferUnitComponent } from './transfer-unit.component';
import { TransferUnitDetailComponent } from './transfer-unit-detail.component';
import { TransferUnitPopupComponent } from './transfer-unit-dialog.component';
import { TransferUnitDeletePopupComponent } from './transfer-unit-delete-dialog.component';
import { RequestNoteComponent } from './request-note-pilih-cabang.component';
import { RequestNoteDetailComponent } from './request-note-detail.component';

@Injectable()
export class TransferUnitResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const transferUnitRoute: Routes = [
    {
        path: 'transfer-unit',
        component: TransferUnitComponent,
        resolve: {
            'pagingParams': TransferUnitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnit.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-note-detail/:idreqnotdest',
        component: RequestNoteDetailComponent,
        resolve: {
            'pagingParams': TransferUnitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.movingSlip.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
    path: 'request-note-pilih-cabang',
    component: RequestNoteComponent,
    resolve: {
        'pagingParams': TransferUnitResolvePagingParams
    },
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'mpmApp.movingSlip.home.title'
    },
    canActivate: [UserRouteAccessService]
},
    {
        path: 'transfer-unit/:id',
        component: TransferUnitDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnit.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const transferUnitPopupRoute: Routes = [
    {
        path: 'transfer-unit-new',
        component: TransferUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transfer-unit/:id/edit',
        component: TransferUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transfer-unit/:id/delete',
        component: TransferUnitDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.transferUnit.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
