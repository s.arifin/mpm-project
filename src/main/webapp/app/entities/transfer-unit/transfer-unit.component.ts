import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { TransferUnit } from './transfer-unit.model';
import { TransferUnitService } from './transfer-unit.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../layouts';
import {Observable} from 'rxjs/Rx';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem} from 'primeng/primeng';
import { Request, RequestService, CustomRequestNote } from '../request';
import * as AdminHandlingConstant from '../../shared/constants/vehicle-sales-order.constants';
import { VehicleSalesOrder, VehicleSalesOrderService } from '../vehicle-sales-order';
import { UnitDocumentMessage, UnitDocumentMessageService } from '../unit-document-message';

@Component({
    selector: 'jhi-transfer-unit',
    templateUrl: './transfer-unit.component.html'
})
export class TransferUnitComponent implements OnInit {

    currentAccount: any;
    transferUnits: TransferUnit[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    unitDocumentMessage: UnitDocumentMessage[];
    isSaving: boolean;
    korsals: SelectItem[];
    selectedKorsal: string;
    selected: any = [];

    constructor(
        private requestService: RequestService,
        private unitDocumentMessageService: UnitDocumentMessageService,
        private confirmationService: ConfirmationService,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
        private transferUnitService: TransferUnitService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.unitDocumentMessageService.queryFilterByinternalC({
            // idInternal: this.principal.getIdInternal(),
            query: 'idInternal:' + this.principal.getIdInternal(),
            page: this.page - 1,
            size: this.itemsPerPage,
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                // (res: ResponseWrapper) => this.onError(res.json)
            )
    }

    createRequest() {
        this.unitDocumentMessageService.passingCustomData(this.selected);
        console.log('isi selected', this.selected);
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/transfer-unit'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/transfer-unit', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/transfer-unit', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        // this.loadAllSubmit();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
    }

    trackId(index: number, item: TransferUnit) {
        return item.id;
    }
    registerChangeInTransferUnits() {
        this.eventSubscriber = this.eventManager.subscribe('transferUnitListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    private onSuccess(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.unitDocumentMessage = data;
        }
        console.log('ini data internal', data);
    }
    private onError(error) {
        // this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
        this.toasterService.showToaster('info', 'Save', 'Error');
    }

    processToNotAvailable() {
        this.confirmationService.confirm({
            message: 'Apakah Anda Yakin?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                    this.notAvailable();
                this.loadingService.loadingStart();
            }
        });
        // console.log('to spg: ', this.requestNote);
    }

    notAvailable() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.selected
        }
        this.requestService.notAvailable(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessNotAvailable(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        console.log('isi selected', obj);
    }

    private onSuccessNotAvailable(data, headers) {
        if (data !== null) {
            this.totalItems = data[0].Totaldata;
            this.queryCount = this.totalItems;
            this.unitDocumentMessage = data;
            this.router.navigate(['/approval-korsal/unit-document-message'])
            this.loadingService.loadingStop();
        }
        console.log('ini data internal', data);
    }
}
