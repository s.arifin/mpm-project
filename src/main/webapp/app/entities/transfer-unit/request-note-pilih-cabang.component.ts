import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent } from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { SelectItem } from 'primeng/primeng';

// nuse
import { DatePipe } from '@angular/common';
import { constants } from 'os';
import { Principal, ToasterService, ITEMS_PER_PAGE, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../layouts';
import { RuleHotItemService } from '../rule-hot-item';
import * as BaseConstant from '../../shared/constants/base.constants';
import { UnitDocumentMessage, UnitDocumentMessageService, CustomUnitModel, CustomTemp } from '../unit-document-message';
import { Internal } from '../internal/internal.model';
import { InternalService } from '../internal/internal.service';
// import { TransferUnitRequestComponent } from './transfer-unit-request-note.component';
import { forEach } from '@angular/router/src/utils/collection';
import { ReportUtilService } from '../../shared/report/report-util.service';

@Component({
    selector: 'jhi-request-note-pilih-cabang',
    templateUrl: './request-note-pilih-cabang.component.html'
})
export class RequestNoteComponent implements OnInit {

    customTemp: CustomTemp;
    customUnitModel: CustomUnitModel[];
    eachCustomUnitModel: CustomUnitModel;
    unitDocumentMessages: UnitDocumentMessage[];
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    currentSearching: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tgl1: Date;
    tgl2: Date;
    internals: Internal[];
    internal1: Internal;
    internal2: Internal;
    internal3: Internal;
    leasings: SelectItem[];
    selectedLeasing: string;
    selectedKorsal: string;
    selectedSalesman: string;
    statusVsoes: SelectItem[];
    selectedStatusVso: Number;
    isLazyLoadingFirst: Boolean ;
    isMatchingMenu: Boolean ;
    isFiltered: Boolean;

    constructor(
        protected reportUtilService: ReportUtilService,
        protected internalService: InternalService,
        private unitDocumentMesageService: UnitDocumentMessageService,
        private confirmationService: ConfirmationService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        private toasterService: ToasterService,
        private loadingService: LoadingService,
        private ruleHotItemService: RuleHotItemService,
        private toaster: ToasterService,
        private datePipe: DatePipe

    ) {
        this.customUnitModel = new Array<CustomUnitModel>();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/unit-document-message'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
        console.log('is filter = ', this.isFiltered );
    }

    printSave() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.unitDocumentMessages
        }
        this.principal.getUserLogin();
        console.log('ini createrequest', this.unitDocumentMessages);
        console.log('ini createrequest', this.principal.getUserLogin);
        console.log('ini internal', this.principal.getIdInternal());
        this.unitDocumentMesageService.printSave(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessPilihsave(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        )

    }
    private onSuccessPilihsave(data, headers) {
        console.log(data);
        this.customTemp = data;
        this.print(this.customTemp);
        this.loadingService.loadingStop();
    }

    print(customTemp: Data) {
        this.toasterService.showToaster('INFO', 'Print', 'Proccess ... ');
        this.reportUtilService.viewFile('/api/report/print_list/pdf', { temp: customTemp.idtemp});
    }

    createRequestNote() {
        const obj = {
            idinternal: this.principal.getIdInternal(),
            createdBy: this.principal.getUserLogin(),
            data: this.unitDocumentMessages
        }
        this.principal.getUserLogin();
        console.log('ini createrequest', this.unitDocumentMessages);
        console.log('ini createrequest', this.principal.getUserLogin);
        console.log('ini internal', this.principal.getIdInternal());
        this.unitDocumentMesageService.createRequestnote(obj).subscribe(
            (res: ResponseWrapper) => this.onSuccessprintsave(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        )

    }
    private onSuccessprintsave(data, headers) {
        console.log(data);
        this.unitDocumentMessages = data;
        this.loadingService.loadingStop();
        this.router.navigate(['../transfer-unit'])
    }
    private onErrorprint(error) {
        this.toaster.showToaster('info', 'save', 'Qty Input Melebihi Dari Qty Request')
        this.loadingService.loadingStop();
    }

    search() {
        this.customUnitModel = new Array <CustomUnitModel>();
        this.unitDocumentMessages.forEach(
            (item) => {
                this.eachCustomUnitModel = new CustomUnitModel();
                this.eachCustomUnitModel.idMessage = item.idMessage;
                this.eachCustomUnitModel.idRequirement = item.idRequirement;
                this.eachCustomUnitModel.idProduct = item.IdProduct;
                this.eachCustomUnitModel.idFeature = item.idFeature;
                this.eachCustomUnitModel.Color = item.Color;
                this.eachCustomUnitModel.yearAssmbly = item.yearAssmbly;
                if (this.internal1 != null) {
                    this.eachCustomUnitModel.cabang1 = this.internal1;
                } else {
                    this.eachCustomUnitModel.cabang1 = '00000';
                }
                if (this.internal2 != null) {
                    this.eachCustomUnitModel.cabang2 = this.internal2;
                } else {
                    this.eachCustomUnitModel.cabang2 = '00000';
                }
                if (this.internal3 != null) {
                    this.eachCustomUnitModel.cabang3 = this.internal3;
                } else {
                    this.eachCustomUnitModel.cabang3 = '00000';
                }
                this.customUnitModel.push(this.eachCustomUnitModel);
            }
        )
        console.log('internal1', this.internal1);
        console.log('internal2', this.internal2);
        console.log('internal3', this.internal3);
        console.log('customunit', this.customUnitModel);
        this.unitDocumentMesageService.pillihCabangC(this.customUnitModel).subscribe(
            (res: ResponseWrapper) => this.onSuccesscari(res, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        )
    }

    private onSuccesscari(data, headers) {
        console.log(data);
        this.unitDocumentMessages = data;
        this.loadingService.loadingStop();
    }

    searching(query) {
        if (!query) {
            // return this.clear();
        }
        // if (this.isMatchingMenu === true) {
        //     this.page = 0;
        //     this.currentSearching = query;
        //     this.router.navigate(['/vehicle-sales-order/true', {
        //         search: this.currentSearching,
        //         page: this.page,
        //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        //     }]);
        //     this.loadAll();
        // }
        // if (this.isMatchingMenu === false) {
        //     this.page = 0;
        //     this.currentSearching = query;
        //     this.router.navigate(['/vehicle-sales-order', {
        //         search: this.currentSearching,
        //         page: this.page,
        //         sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        //     }]);
        //     this.loadAll();
        // }

    }

    ngOnInit() {
        this.internalService.query({size: 999999})
        .subscribe((res: ResponseWrapper) => {
            this.internals = res.json;
            console.log('internal :', this.internals)
        });

        this.unitDocumentMesageService.passingData.subscribe( (unitDocumentMessage) => {
            this.unitDocumentMessages = unitDocumentMessage;
            console.log('ini nerima subscribe', this.unitDocumentMessages);
            }
        );
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'dateEntry') {
            result.push('dateEntry');
        }
        return result;
    }

    private onSuccessPilihCabang(data, headers) {
        console.log(data);
        this.unitDocumentMessages = data;
        this.loadingService.loadingStop();
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
    }

    private onError(error) {
        this.toaster.showToaster('info', 'save', 'Qty Input Melebihi Dari Qty Request')
        this.loadingService.loadingStop();
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
}
