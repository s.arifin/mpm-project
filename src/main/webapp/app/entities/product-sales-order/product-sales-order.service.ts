import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ProductSalesOrder } from './product-sales-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductSalesOrderService {
    protected itemValues: ProductSalesOrder[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/product-sales-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-sales-orders';

    constructor(protected http: Http) { }

    create(productSalesOrder: ProductSalesOrder): Observable<ProductSalesOrder> {
        const copy = this.convert(productSalesOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(productSalesOrder: ProductSalesOrder): Observable<ProductSalesOrder> {
        const copy = this.convert(productSalesOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ProductSalesOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ProductSalesOrder> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ProductSalesOrder[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ProductSalesOrder.
     */
    protected convertItemFromServer(json: any): ProductSalesOrder {
        const entity: ProductSalesOrder = Object.assign(new ProductSalesOrder(), json);
        return entity;
    }

    /**
     * Convert a ProductSalesOrder to a JSON which can be sent to the server.
     */
    protected convert(productSalesOrder: ProductSalesOrder): ProductSalesOrder {
        if (productSalesOrder === null || productSalesOrder === {}) {
            return {};
        }
        // const copy: ProductSalesOrder = Object.assign({}, productSalesOrder);
        const copy: ProductSalesOrder = JSON.parse(JSON.stringify(productSalesOrder));
        return copy;
    }

    protected convertList(productSalesOrders: ProductSalesOrder[]): ProductSalesOrder[] {
        const copy: ProductSalesOrder[] = productSalesOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductSalesOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
