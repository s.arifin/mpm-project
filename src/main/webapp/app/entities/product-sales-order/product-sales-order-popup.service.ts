import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ProductSalesOrder } from './product-sales-order.model';
import { ProductSalesOrderService } from './product-sales-order.service';

@Injectable()
export class ProductSalesOrderPopupService {
    protected ngbModalRef: NgbModalRef;
    idInternal: any;
    idCustomer: any;
    idBillTo: any;
    idSaleType: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected productSalesOrderService: ProductSalesOrderService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.productSalesOrderService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.productSalesOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new ProductSalesOrder();
                    data.internalId = this.idInternal;
                    data.customerId = this.idCustomer;
                    data.billToId = this.idBillTo;
                    data.saleTypeId = this.idSaleType;
                    this.ngbModalRef = this.productSalesOrderModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    productSalesOrderModalRef(component: Component, productSalesOrder: ProductSalesOrder): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.productSalesOrder = productSalesOrder;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.componentInstance.idCustomer = this.idCustomer;
        modalRef.componentInstance.idBillTo = this.idBillTo;
        modalRef.componentInstance.idSaleType = this.idSaleType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.productSalesOrderLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    productSalesOrderLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
