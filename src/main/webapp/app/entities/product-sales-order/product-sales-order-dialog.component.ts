import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductSalesOrder } from './product-sales-order.model';
import { ProductSalesOrderPopupService } from './product-sales-order-popup.service';
import { ProductSalesOrderService } from './product-sales-order.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { SaleType, SaleTypeService } from '../sale-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-sales-order-dialog',
    templateUrl: './product-sales-order-dialog.component.html'
})
export class ProductSalesOrderDialogComponent implements OnInit {

    productSalesOrder: ProductSalesOrder;
    isSaving: boolean;
    idInternal: any;
    idCustomer: any;
    idBillTo: any;
    idSaleType: any;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    saletypes: SaleType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected productSalesOrderService: ProductSalesOrderService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected billToService: BillToService,
        protected saleTypeService: SaleTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.saleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.saletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.productSalesOrderService.update(this.productSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.productSalesOrderService.create(this.productSalesOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductSalesOrder>) {
        result.subscribe((res: ProductSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductSalesOrder) {
        this.eventManager.broadcast({ name: 'productSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productSalesOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productSalesOrder Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }

    trackSaleTypeById(index: number, item: SaleType) {
        return item.idSaleType;
    }
}

@Component({
    selector: 'jhi-product-sales-order-popup',
    template: ''
})
export class ProductSalesOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productSalesOrderPopupService: ProductSalesOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productSalesOrderPopupService
                    .open(ProductSalesOrderDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.productSalesOrderPopupService.parent = params['parent'];
                this.productSalesOrderPopupService
                    .open(ProductSalesOrderDialogComponent as Component);
            } else {
                this.productSalesOrderPopupService
                    .open(ProductSalesOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
