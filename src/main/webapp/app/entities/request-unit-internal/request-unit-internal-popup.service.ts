import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RequestUnitInternal } from './request-unit-internal.model';
import { RequestUnitInternalService } from './request-unit-internal.service';

@Injectable()
export class RequestUnitInternalPopupService {
    protected ngbModalRef: NgbModalRef;
    idRequest: any;
    idInternalFrom: any;
    idInternalTo: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected requestUnitInternalService: RequestUnitInternalService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.requestUnitInternalService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.requestUnitInternalModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new RequestUnitInternal();
                    data.requestUnitInternalId = this.idRequest;
                    data.internalFromId = this.idInternalFrom;
                    data.internalToId = this.idInternalTo;
                    this.ngbModalRef = this.requestUnitInternalModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    requestUnitInternalModalRef(component: Component, requestUnitInternal: RequestUnitInternal): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.requestUnitInternal = requestUnitInternal;
        modalRef.componentInstance.idRequest = this.idRequest;
        modalRef.componentInstance.idInternalFrom = this.idInternalFrom;
        modalRef.componentInstance.idInternalTo = this.idInternalTo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.requestUnitInternalLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    requestUnitInternalLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
