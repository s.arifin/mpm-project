import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestUnitInternal } from './request-unit-internal.model';
import { RequestUnitInternalService } from './request-unit-internal.service';
import { ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-request-unit-internal-edit',
    templateUrl: './request-unit-internal-edit.component.html'
})
export class RequestUnitInternalEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    requestUnitInternal: RequestUnitInternal;
    isSaving: boolean;
    idRequest: any;
    paramPage: number;

    requestunitinternals: RequestUnitInternal[];

    internals: Internal[];

    constructor(
        protected alertService: JhiAlertService,
        protected requestUnitInternalService: RequestUnitInternalService,
        protected internalService: InternalService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.requestUnitInternal = new RequestUnitInternal();
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idRequest = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
        });
        this.isSaving = false;
        this.requestUnitInternalService.query()
            .subscribe((res: ResponseWrapper) => { this.requestunitinternals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.requestUnitInternalService.find(this.idRequest).subscribe((requestUnitInternal) => {
            this.requestUnitInternal = requestUnitInternal;
        });
    }

    previousState() {
        this.router.navigate(['request-unit-internal', { page: this.paramPage }]);
    }

    save() {
        this.isSaving = true;
        if (this.requestUnitInternal.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestUnitInternalService.update(this.requestUnitInternal));
        } else {
            this.subscribeToSaveResponse(
                this.requestUnitInternalService.create(this.requestUnitInternal));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestUnitInternal>) {
        result.subscribe((res: RequestUnitInternal) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RequestUnitInternal) {
        this.eventManager.broadcast({ name: 'requestUnitInternalListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestUnitInternal saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'requestUnitInternal Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRequestUnitInternalById(index: number, item: RequestUnitInternal) {
        return item.idRequest;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}
