import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RequestUnitInternal } from './request-unit-internal.model';
import { RequestUnitInternalPopupService } from './request-unit-internal-popup.service';
import { RequestUnitInternalService } from './request-unit-internal.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-request-unit-internal-dialog',
    templateUrl: './request-unit-internal-dialog.component.html'
})
export class RequestUnitInternalDialogComponent implements OnInit {

    requestUnitInternal: RequestUnitInternal;
    isSaving: boolean;
    idRequest: any;
    idInternalFrom: any;
    idInternalTo: any;

    requestunitinternals: RequestUnitInternal[];

    internals: Internal[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected requestUnitInternalService: RequestUnitInternalService,
        protected internalService: InternalService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.requestUnitInternalService.query()
            .subscribe((res: ResponseWrapper) => { this.requestunitinternals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requestUnitInternal.idRequest !== undefined) {
            this.subscribeToSaveResponse(
                this.requestUnitInternalService.update(this.requestUnitInternal));
        } else {
            this.subscribeToSaveResponse(
                this.requestUnitInternalService.create(this.requestUnitInternal));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestUnitInternal>) {
        result.subscribe((res: RequestUnitInternal) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RequestUnitInternal) {
        this.eventManager.broadcast({ name: 'requestUnitInternalListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestUnitInternal saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'requestUnitInternal Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRequestUnitInternalById(index: number, item: RequestUnitInternal) {
        return item.idRequest;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}

@Component({
    selector: 'jhi-request-unit-internal-popup',
    template: ''
})
export class RequestUnitInternalPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestUnitInternalPopupService: RequestUnitInternalPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requestUnitInternalPopupService
                    .open(RequestUnitInternalDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.requestUnitInternalPopupService.parent = params['parent'];
                this.requestUnitInternalPopupService
                    .open(RequestUnitInternalDialogComponent as Component);
            } else {
                this.requestUnitInternalPopupService
                    .open(RequestUnitInternalDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
