import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RequestUnitInternalComponent } from './request-unit-internal.component';
import { RequestUnitInternalEditComponent } from './request-unit-internal-edit.component';
import { RequestUnitInternalPopupComponent } from './request-unit-internal-dialog.component';

@Injectable()
export class RequestUnitInternalResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequest,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requestUnitInternalRoute: Routes = [
    {
        path: 'request-unit-internal',
        component: RequestUnitInternalComponent,
        resolve: {
            'pagingParams': RequestUnitInternalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requestUnitInternalPopupRoute: Routes = [
    {
        path: 'request-unit-internal-popup-new-list/:parent',
        component: RequestUnitInternalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-unit-internal-popup-new',
        component: RequestUnitInternalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-unit-internal-new',
        component: RequestUnitInternalEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-unit-internal/:id/edit',
        component: RequestUnitInternalEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-unit-internal/:id/edit/:page',
        component: RequestUnitInternalEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'request-unit-internal/:id/popup-edit',
        component: RequestUnitInternalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestUnitInternal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
