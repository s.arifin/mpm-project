import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RequestUnitInternal } from './request-unit-internal.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RequestUnitInternalService {
    protected itemValues: RequestUnitInternal[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/request-unit-internals';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/request-unit-internals';

    constructor(protected http: Http) { }

    public findPreRequestUnitInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idinternal', req.idinternal);

        const url = this.resourceUrl + '/pre';
        return this.http.get(url, options).map(
            (res: Response) => this.convertResponse(res)
        );
    }

    create(requestUnitInternal: RequestUnitInternal): Observable<RequestUnitInternal> {
        const copy = this.convert(requestUnitInternal);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(requestUnitInternal: RequestUnitInternal): Observable<RequestUnitInternal> {
        const copy = this.convert(requestUnitInternal);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<RequestUnitInternal> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(requestUnitInternal: RequestUnitInternal, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(requestUnitInternal);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<RequestUnitInternal> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<RequestUnitInternal[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RequestUnitInternal.
     */
    protected convertItemFromServer(json: any): RequestUnitInternal {
        const entity: RequestUnitInternal = Object.assign(new RequestUnitInternal(), json);
        return entity;
    }

    /**
     * Convert a RequestUnitInternal to a JSON which can be sent to the server.
     */
    protected convert(requestUnitInternal: RequestUnitInternal): RequestUnitInternal {
        if (requestUnitInternal === null || requestUnitInternal === {}) {
            return {};
        }
        // const copy: RequestUnitInternal = Object.assign({}, requestUnitInternal);
        const copy: RequestUnitInternal = JSON.parse(JSON.stringify(requestUnitInternal));
        return copy;
    }

    protected convertList(requestUnitInternals: RequestUnitInternal[]): RequestUnitInternal[] {
        const copy: RequestUnitInternal[] = requestUnitInternals;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RequestUnitInternal[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
