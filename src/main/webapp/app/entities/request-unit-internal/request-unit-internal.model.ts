import { BaseEntity } from './../../shared';

export class RequestUnitInternal implements BaseEntity {
    constructor(
        public id?: any,
        public idRequest?: any,
        public requestUnitInternalId?: any,
        public internalFromId?: any,
        public internalToId?: any,
    ) {
    }
}
