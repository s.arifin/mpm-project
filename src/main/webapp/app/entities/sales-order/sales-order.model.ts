import { BaseEntity } from './../../shared';

export class SalesOrder implements BaseEntity {
    constructor(
        public id?: any,
        public idOrder?: any,
        public internalId?: any,
        public customerId?: any,
        public billToId?: any,
    ) {
    }
}
