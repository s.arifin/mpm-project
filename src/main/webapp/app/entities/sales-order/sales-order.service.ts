import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SalesOrder } from './sales-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SalesOrderService {
    protected itemValues: SalesOrder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/sales-orders';
    protected resourceSearchUrl = 'api/_search/sales-orders';

    constructor(protected http: Http) { }

    create(salesOrder: SalesOrder): Observable<SalesOrder> {
        const copy = this.convert(salesOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(salesOrder: SalesOrder): Observable<SalesOrder> {
        const copy = this.convert(salesOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SalesOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, salesOrder: SalesOrder): Observable<SalesOrder> {
        const copy = this.convert(salesOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, salesOrders: SalesOrder[]): Observable<SalesOrder[]> {
        const copy = this.convertList(salesOrders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(salesOrder: SalesOrder): SalesOrder {
        if (salesOrder === null || salesOrder === {}) {
            return {};
        }
        // const copy: SalesOrder = Object.assign({}, salesOrder);
        const copy: SalesOrder = JSON.parse(JSON.stringify(salesOrder));
        return copy;
    }

    protected convertList(salesOrders: SalesOrder[]): SalesOrder[] {
        const copy: SalesOrder[] = salesOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SalesOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
