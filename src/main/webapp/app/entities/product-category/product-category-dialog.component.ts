import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductCategory } from './product-category.model';
import { ProductCategoryPopupService } from './product-category-popup.service';
import { ProductCategoryService } from './product-category.service';
import { ToasterService } from '../../shared';
import { ProductCategoryType, ProductCategoryTypeService } from '../product-category-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-category-dialog',
    templateUrl: './product-category-dialog.component.html'
})
export class ProductCategoryDialogComponent implements OnInit {

    productCategory: ProductCategory;
    isSaving: boolean;
    idCategoryType: any;

    productcategorytypes: ProductCategoryType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected productCategoryService: ProductCategoryService,
        protected productCategoryTypeService: ProductCategoryTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productCategoryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.productcategorytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productCategory.idCategory !== undefined) {
            this.subscribeToSaveResponse(
                this.productCategoryService.update(this.productCategory));
        } else {
            this.subscribeToSaveResponse(
                this.productCategoryService.create(this.productCategory));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductCategory>) {
        result.subscribe((res: ProductCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ProductCategory) {
        this.eventManager.broadcast({ name: 'productCategoryListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productCategory saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'productCategory Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProductCategoryTypeById(index: number, item: ProductCategoryType) {
        return item.idCategoryType;
    }
}

@Component({
    selector: 'jhi-product-category-popup',
    template: ''
})
export class ProductCategoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productCategoryPopupService: ProductCategoryPopupService
    ) {}

    ngOnInit() {
        this.productCategoryPopupService.idCategoryType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productCategoryPopupService
                    .open(ProductCategoryDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.productCategoryPopupService.idCategoryType = params['parent'];
                this.productCategoryPopupService
                    .open(ProductCategoryDialogComponent as Component);
            } else {
                this.productCategoryPopupService
                    .open(ProductCategoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
