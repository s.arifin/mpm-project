import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductCategory } from './product-category.model';
import { ProductCategoryService } from './product-category.service';
import { ToasterService} from '../../shared';
import { ProductCategoryType, ProductCategoryTypeService } from '../product-category-type';
import { ResponseWrapper } from '../../shared';

@Component({
   selector: 'jhi-product-category-edit',
   templateUrl: './product-category-edit.component.html'
})
export class ProductCategoryEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   productCategory: ProductCategory;
   isSaving: boolean;
   idCategory: any;
   paramPage: number;
   routeId: number;

   productcategorytypes: ProductCategoryType[];

   constructor(
       protected alertService: JhiAlertService,
       protected productCategoryService: ProductCategoryService,
       protected productCategoryTypeService: ProductCategoryTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.productCategory = new ProductCategory();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idCategory = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.productCategoryTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.productcategorytypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.productCategoryService.find(this.idCategory).subscribe((productCategory) => {
           this.productCategory = productCategory;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['product-category', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.productCategory.idCategory !== undefined) {
           this.subscribeToSaveResponse(
               this.productCategoryService.update(this.productCategory));
       } else {
           this.subscribeToSaveResponse(
               this.productCategoryService.create(this.productCategory));
       }
   }

   protected subscribeToSaveResponse(result: Observable<ProductCategory>) {
       result.subscribe((res: ProductCategory) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: ProductCategory) {
       this.eventManager.broadcast({ name: 'productCategoryListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'productCategory saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'productCategory Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackProductCategoryTypeById(index: number, item: ProductCategoryType) {
       return item.idCategoryType;
   }
}
