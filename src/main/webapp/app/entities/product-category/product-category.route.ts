import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ProductCategoryComponent } from './product-category.component';
import { ProductCategoryEditComponent } from './product-category-edit.component';
import { ProductCategoryLovPopupComponent } from './product-category-as-lov.component';
import { ProductCategoryPopupComponent } from './product-category-dialog.component';

@Injectable()
export class ProductCategoryResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCategory,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productCategoryRoute: Routes = [
    {
        path: 'product-category',
        component: ProductCategoryComponent,
        resolve: {
            'pagingParams': ProductCategoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productCategoryPopupRoute: Routes = [
    {
        path: 'product-category-lov',
        component: ProductCategoryLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-category-popup-new-list/:parent',
        component: ProductCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-category-popup-new',
        component: ProductCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-category-new',
        component: ProductCategoryEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-category/:id/edit',
        component: ProductCategoryEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-category/:route/:page/:id/edit',
        component: ProductCategoryEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-category/:id/popup-edit',
        component: ProductCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
