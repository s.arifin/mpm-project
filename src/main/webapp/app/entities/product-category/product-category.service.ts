import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ProductCategory } from './product-category.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ProductCategoryService {
   protected itemValues: ProductCategory[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/product-categories';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/product-categories';

   constructor(protected http: Http) { }

   create(productCategory: ProductCategory): Observable<ProductCategory> {
       const copy = this.convert(productCategory);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(productCategory: ProductCategory): Observable<ProductCategory> {
       const copy = this.convert(productCategory);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<ProductCategory> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: ProductCategory, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: ProductCategory[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to ProductCategory.
    */
   protected convertItemFromServer(json: any): ProductCategory {
       const entity: ProductCategory = Object.assign(new ProductCategory(), json);
       return entity;
   }

   /**
    * Convert a ProductCategory to a JSON which can be sent to the server.
    */
   protected convert(productCategory: ProductCategory): ProductCategory {
       if (productCategory === null || productCategory === {}) {
           return {};
       }
       // const copy: ProductCategory = Object.assign({}, productCategory);
       const copy: ProductCategory = JSON.parse(JSON.stringify(productCategory));
       return copy;
   }

   protected convertList(productCategorys: ProductCategory[]): ProductCategory[] {
       const copy: ProductCategory[] = productCategorys;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: ProductCategory[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
