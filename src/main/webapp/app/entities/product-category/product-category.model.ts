import { BaseEntity } from './../../shared';

export class ProductCategory implements BaseEntity {
    constructor(
        public id?: number,
        public idCategory?: number,
        public description?: string,
        public refKey?: string,
        public categoryTypeId?: any,
    ) {
    }
}
