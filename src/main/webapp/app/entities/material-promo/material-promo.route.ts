import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MaterialPromoComponent } from './material-promo.component';
import { MaterialPromoDetailComponent } from './material-promo-detail.component';
import { MaterialPromoPopupComponent } from './material-promo-dialog.component';
import { MaterialPromoDeletePopupComponent } from './material-promo-delete-dialog.component';

@Injectable()
export class MaterialPromoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const materialPromoRoute: Routes = [
    {
        path: 'material-promo',
        component: MaterialPromoComponent,
        resolve: {
            'pagingParams': MaterialPromoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.materialPromo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'material-promo/:id',
        component: MaterialPromoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.materialPromo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const materialPromoPopupRoute: Routes = [
    {
        path: 'material-promo-new',
        component: MaterialPromoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.materialPromo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'material-promo/:id/edit',
        component: MaterialPromoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.materialPromo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'material-promo/:id/delete',
        component: MaterialPromoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.materialPromo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
