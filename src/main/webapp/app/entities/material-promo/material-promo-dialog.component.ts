import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MaterialPromo } from './material-promo.model';
import { MaterialPromoPopupService } from './material-promo-popup.service';
import { MaterialPromoService } from './material-promo.service';

@Component({
    selector: 'jhi-material-promo-dialog',
    templateUrl: './material-promo-dialog.component.html'
})
export class MaterialPromoDialogComponent implements OnInit {

    materialPromo: MaterialPromo;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private materialPromoService: MaterialPromoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.materialPromo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.materialPromoService.update(this.materialPromo));
        } else {
            this.subscribeToSaveResponse(
                this.materialPromoService.create(this.materialPromo));
        }
    }

    private subscribeToSaveResponse(result: Observable<MaterialPromo>) {
        result.subscribe((res: MaterialPromo) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: MaterialPromo) {
        this.eventManager.broadcast({ name: 'materialPromoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-material-promo-popup',
    template: ''
})
export class MaterialPromoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private materialPromoPopupService: MaterialPromoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.materialPromoPopupService
                    .open(MaterialPromoDialogComponent as Component, params['id']);
            } else {
                this.materialPromoPopupService
                    .open(MaterialPromoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
