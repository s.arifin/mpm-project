import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MaterialPromo } from './material-promo.model';
import { MaterialPromoPopupService } from './material-promo-popup.service';
import { MaterialPromoService } from './material-promo.service';

@Component({
    selector: 'jhi-material-promo-delete-dialog',
    templateUrl: './material-promo-delete-dialog.component.html'
})
export class MaterialPromoDeleteDialogComponent {

    materialPromo: MaterialPromo;

    constructor(
        private materialPromoService: MaterialPromoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.materialPromoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'materialPromoListModification',
                content: 'Deleted an materialPromo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-material-promo-delete-popup',
    template: ''
})
export class MaterialPromoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private materialPromoPopupService: MaterialPromoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.materialPromoPopupService
                .open(MaterialPromoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
