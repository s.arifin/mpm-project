import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    MaterialPromoService,
    MaterialPromoPopupService,
    MaterialPromoComponent,
    MaterialPromoDetailComponent,
    MaterialPromoDialogComponent,
    MaterialPromoPopupComponent,
    MaterialPromoDeletePopupComponent,
    MaterialPromoDeleteDialogComponent,
    materialPromoRoute,
    materialPromoPopupRoute,
    MaterialPromoResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...materialPromoRoute,
    ...materialPromoPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MaterialPromoComponent,
        MaterialPromoDetailComponent,
        MaterialPromoDialogComponent,
        MaterialPromoDeleteDialogComponent,
        MaterialPromoPopupComponent,
        MaterialPromoDeletePopupComponent,
    ],
    entryComponents: [
        MaterialPromoComponent,
        MaterialPromoDialogComponent,
        MaterialPromoPopupComponent,
        MaterialPromoDeleteDialogComponent,
        MaterialPromoDeletePopupComponent,
    ],
    providers: [
        MaterialPromoService,
        MaterialPromoPopupService,
        MaterialPromoResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMaterialPromoModule {}
