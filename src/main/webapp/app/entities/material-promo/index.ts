export * from './material-promo.model';
export * from './material-promo-popup.service';
export * from './material-promo.service';
export * from './material-promo-dialog.component';
export * from './material-promo-delete-dialog.component';
export * from './material-promo-detail.component';
export * from './material-promo.component';
export * from './material-promo.route';
