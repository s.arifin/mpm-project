import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { MaterialPromo } from './material-promo.model';
import { MaterialPromoService } from './material-promo.service';

@Component({
    selector: 'jhi-material-promo-detail',
    templateUrl: './material-promo-detail.component.html'
})
export class MaterialPromoDetailComponent implements OnInit, OnDestroy {

    materialPromo: MaterialPromo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private materialPromoService: MaterialPromoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMaterialPromos();
    }

    load(id) {
        this.materialPromoService.find(id).subscribe((materialPromo) => {
            this.materialPromo = materialPromo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMaterialPromos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'materialPromoListModification',
            (response) => this.load(this.materialPromo.id)
        );
    }
}
