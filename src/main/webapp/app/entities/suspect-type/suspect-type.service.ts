import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { SuspectType } from './suspect-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SuspectTypeService {
    protected itemValues: SuspectType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/suspect-types';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/suspect-types';

    constructor(protected http: Http) { }

    create(suspectType: SuspectType): Observable<SuspectType> {
        const copy = this.convert(suspectType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(suspectType: SuspectType): Observable<SuspectType> {
        const copy = this.convert(suspectType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<SuspectType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, suspectType: SuspectType): Observable<SuspectType> {
        const copy = this.convert(suspectType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, suspectTypes: SuspectType[]): Observable<SuspectType[]> {
        const copy = this.convertList(suspectTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(suspectType: SuspectType): SuspectType {
        if (suspectType === null || suspectType === {}) {
            return {};
        }
        // const copy: SuspectType = Object.assign({}, suspectType);
        const copy: SuspectType = JSON.parse(JSON.stringify(suspectType));
        return copy;
    }

    protected convertList(suspectTypes: SuspectType[]): SuspectType[] {
        const copy: SuspectType[] = suspectTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SuspectType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
