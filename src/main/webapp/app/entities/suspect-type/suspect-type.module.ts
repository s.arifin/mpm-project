import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {
    SuspectTypeService,
} from './';

@NgModule({
    providers: [
        SuspectTypeService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSuspectTypeModule {}
