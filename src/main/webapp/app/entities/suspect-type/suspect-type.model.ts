import { BaseEntity } from './../../shared';

export class SuspectType implements BaseEntity {
    constructor(
        public id?: number,
        public idSuspectType?: number,
        public description?: string,
    ) {
    }
}
