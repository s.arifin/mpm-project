import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { DeliveryOrder } from './delivery-order.model';
import { DeliveryOrderService } from './delivery-order.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as ShipmentOutgoingConstrants from '../../shared/constants/shipmentOutgoing.constrants'

import { VehicleSalesOrder, VehicleSalesOrderService} from './../vehicle-sales-order';
import { OrderItem, OrderItemService} from './../order-item';
import { PickingSlip, PickingSlipService} from './../picking-slip';
import { ShipmentOutgoing, CustomShipment, ShipmentOutgoingService} from './../shipment-outgoing';
import { ShipmentOutgoingParameters } from '../shipment-outgoing/shipment-outgoing-parameter.model';

import { LoadingService } from '../../layouts/loading/loading.service';
import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-delivery-order',
    templateUrl: './delivery-order.component.html'
})
export class DeliveryOrderComponent implements OnInit, OnDestroy {

    idStatusType: number;
    currentSearch: string;
    deliver: any[];

    vehicleSalesOrder: VehicleSalesOrder[];
    orderItem: OrderItem[];
    pickingSlip: PickingSlip;
    customShipment: CustomShipment[];
    shipmentOutgoing: ShipmentOutgoing;
    idInternalHere: any;
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    isLazyLoadingFirst: Boolean = false;
    selectedSalesman: string;
    filtered: any;
    isFiltered: boolean;
    shipmentOutgoingParameters: ShipmentOutgoingParameters;

    constructor(
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected orderItemService: OrderItemService,
        protected pickingSlipService: PickingSlipService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService,
        protected loadingService: LoadingService
    ) {
      this.deliver = [{}];
      this.idStatusType = ShipmentOutgoingConstrants.KIRIM_GUDANG;
      this.itemsPerPage = ITEMS_PER_PAGE;
      this.routeData = this.activatedRoute.data.subscribe((data) => {
          this.page = data['pagingParams'].page;
          this.previousPage = data['pagingParams'].page;
          this.reverse = data['pagingParams'].ascending;
          this.predicate = data['pagingParams'].predicate;
      });
      this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
      this.isFiltered = false;
    }

    loadAll() {
        this.loadingService.loadingStart();
        if (this.currentSearch) {
            this.shipmentOutgoingService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                // sort: this.sort()
            }).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.shipmentOutgoingService.queryFilterBy({
            idstatustype: 11,
            idInternal: this.principal.getIdInternal(),
            queryFor: 'picking',
            page: this.page - 1,
            size: this.itemsPerPage,
            // sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.idInternalHere = this.principal.getIdInternal();
        this.loadAll();
        this.registerChangeInShipmentOutgoings();
     }

     registerChangeInShipmentOutgoings() {
        this.eventSubscriber = this.eventManager.subscribe('shipmentOutgoingListModification', (response) => this.loadAll());
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/delivery-order', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/delivery-order', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

     ngOnDestroy() {
     }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idShipment') {
            result.push('idShipment');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.loadingService.loadingStop();
        this.customShipment = new Array<CustomShipment>();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.customShipment = data;
    }

    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }
    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    gotoDetail(rowdata)  {
       this.shipmentOutgoingService.passingCustomData(rowdata);
    }

    filter() {
        this.loadingService.loadingStart();
        this.isFiltered = true;
        console.log('is filter = ', this.isFiltered );
        // this.page = 0;
        this.shipmentOutgoingParameters = new ShipmentOutgoingParameters();
        this.shipmentOutgoingParameters.internalId = this.principal.getIdInternal();
        this.shipmentOutgoingParameters.idStatusType = 11;
        this.shipmentOutgoingParameters.queryFor = 'gudang';

        if (this.selectedSalesman !== null && this.selectedSalesman !== undefined) {
            this.shipmentOutgoingParameters.salesmanId = this.selectedSalesman;
        }

        if (this.filtered !== null && this.filtered !== undefined) {
            this.shipmentOutgoingParameters.dataParam = this.filtered;
        }

        this.shipmentOutgoingService.searchUnitPrep({
            shipmentOutgoingPTO: this.shipmentOutgoingParameters,
            idInternal: this.principal.getIdInternal(),
            // sort: this.sort()
        })
            .toPromise()
            .then((res) => {
                console.log('filter data isi nama == ', res)
                this.loadingService.loadingStop(),
                this.onSuccessFilter(res.json, res.headers)
            }
        );

        console.log('tanggal 1', this.shipmentOutgoingParameters.orderDateFrom);
        console.log('tanggal 2', this.shipmentOutgoingParameters.orderDateThru);
        console.log('salesman 2', this.shipmentOutgoingParameters.salesmanId);
    }

    protected onSuccessFilter(data, headers) {
        this.loadingService.loadingStop();
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.customShipment = data;
        // if (data.length === 0) {
        //     this.unitDeliverables = [...this.unitDeliverables, this.unitDeliverable];
        // }
    }

    // protected onSuccessOrderItem(data, headers) {
    //     this.links = this.parseLinks.parse(headers.get('link'));
    //     this.totalItems = headers.get('X-Total-Count');
    //     this.queryCount = this.totalItems;
    //     this.orderItem = data;
    // }
}
