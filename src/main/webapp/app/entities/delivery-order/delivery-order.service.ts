import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { DeliveryOrder } from './delivery-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class DeliveryOrderService {
    protected itemValues: DeliveryOrder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/delivery-orders';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/delivery-orders';

    constructor(protected http: Http) { }

    create(deliveryOrder: DeliveryOrder): Observable<DeliveryOrder> {
        const copy = this.convert(deliveryOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(deliveryOrder: DeliveryOrder): Observable<DeliveryOrder> {
        const copy = this.convert(deliveryOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<DeliveryOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        if (options) {options.params.set('idOrder', req.idOrder); }
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(deliveryOrder: DeliveryOrder, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(deliveryOrder);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, deliveryOrder: DeliveryOrder): Observable<DeliveryOrder> {
        const copy = this.convert(deliveryOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, DeliveryOrders: DeliveryOrder[]): Observable<DeliveryOrder[]> {
        const copy = this.convertList(DeliveryOrders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(deliveryOrder: DeliveryOrder): DeliveryOrder {
        if (deliveryOrder === null) {
            return {};
        }
        // const copy: DeliveryOrder = Object.assign({}, DeliveryOrder);
        const copy: DeliveryOrder = JSON.parse(JSON.stringify(DeliveryOrder));
        return copy;
    }

    protected convertList(DeliveryOrders: DeliveryOrder[]): DeliveryOrder[] {
        const copy: DeliveryOrder[] = DeliveryOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: DeliveryOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
