import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as ShipmentOutgoingConstrants from '../../shared/constants/shipmentOutgoing.constrants';

import {ShipmentOutgoingService, CustomShipment, ShipmentOutgoing} from './../shipment-outgoing';
import {PickingSlip, PickingSlipService} from './../picking-slip';
import {OrderItem, OrderItemService } from './../order-item';
import {VehicleSalesOrder, VehicleSalesOrderService } from './../vehicle-sales-order';
import {Requirement, RequirementService } from './../requirement';

import { DeliveryOrder } from './delivery-order.model';
import { DeliveryOrderService } from './delivery-order.service';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-delivery-order-detail',
    templateUrl: './delivery-order-detail.component.html'
})

export class DeliveryOrderDetailComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    deliver: any[];
    currentSearch: string;
    shipmentTMP: CustomShipment;
    shipmentOutgoing: ShipmentOutgoing;
    vehicleSalesOrder: VehicleSalesOrder;
    idOrders: string;
    idShipment: string = null;
    idSlip: string = null;
    orderItem: OrderItem[];
    pickingSlip: PickingSlip;
    currentAccount: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected orderItemService: OrderItemService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected requirementService: RequirementService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected deliveryOrderService: DeliveryOrderService,
        protected pickingSlipService: PickingSlipService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.orderItem = new Array<OrderItem>();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            // console.log('data page', data['pagingParams']);
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.deliver = [{}];
    }

    ngOnInit() {
        this.shipmentOutgoingService.passingData.subscribe((shipmentTMP) => this.shipmentTMP = shipmentTMP);
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            if (params['idOrder']) {
                this.idOrders = params['idOrder'];
                // this.loadAll(params['id']);
            }
            if (params['idShipment']) {
                this.idShipment = params['idShipment'];

            }
            if (params['idSlip']) {
                this.idSlip = params['idSlip'];

            }
            this.loadAll(this.idOrders, this.idShipment);
        });
        // this.loadAll(this.shipmentTMP.idOrder);
    }

    loadAll(idOrders, idShipment) {
        this.vehicleSalesOrderService.find(
            idOrders, this.principal.getIdInternal()).subscribe(
            (data) => {
                this.vehicleSalesOrder = data;
                console.log('vso', this.vehicleSalesOrder);
                this.orderItemService.queryFilterBy({
                    idOrders: this.vehicleSalesOrder.idOrder,
                    page: this.page - 1,
                    size: this.itemsPerPage,
                    // sort: this.sort()
                    }).subscribe(
                    (res: ResponseWrapper) => this.onSuccessOrderItem(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            }
        );
        this.shipmentOutgoingService.find(this.idShipment).subscribe(
            (data) => {
                this.shipmentOutgoing = data;
            }
        )
    }

    ngOnDestroy() {
    }

    protected onSuccessOrderItem(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        console.log('orderItem', data);
        this.orderItem = data;
    }

    gotoDetail(delivery): Promise<any> {
        console.log('delivery', delivery);
        return new Promise<any>(
            (resolve) => {
                this.pickingSlipService.find(this.shipmentTMP.idSlip).subscribe(
                    (data) => {
                        this.pickingSlip = data;
                        console.log('kirim detail',  this.pickingSlip);
                        this.shipmentOutgoingService.changeShareDataTmp( this.pickingSlip);
                        this.router.navigate(['../delivery-order/delivery-order-check']);
                    }
                )
            }
        )
    }

    selesaiGudang(): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.pickingSlipService.find(this.shipmentTMP.idSlip).subscribe(
                    (datapicking) => {
                        if (datapicking.mechanicexternal !== null || datapicking.mechanicinternal !== null ) {
                            this.shipmentOutgoingService.find(this.idShipment).subscribe(
                                (data) => {
                                    this.shipmentOutgoing = data;
                                    this.dokirimGudang(this.shipmentOutgoing).then(
                                        () => {
                                            this.router.navigate(['delivery-order']);
                                        }
                                    )
                                }
                            )
                        }else {
                            this.toasterService.showToaster('warning', 'Data Tidak Lengkap', 'Mekanik Belum Dipilih');
                        }
                    }
                )
            }
        )
    }

    dokirimGudang(datashipment): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                this.shipmentOutgoingService.executeProcess(ShipmentOutgoingConstrants.COMPLETE_GUDANG, null, datashipment ).subscribe(
                    (value) => console.log('this: ', value),
                    (err) => console.log(err),
                    () => {
                        this.eventManager.broadcast({
                            name: 'shipmentOutgoingListModification',
                            content: 'Sent From Werehouse'
                       });
                        // console.log('harus selesai kirim gudang dulu');
                        // this.loadAll();
                        resolve()
                    }
                );
            }
        )
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    public checkIdMachine(): boolean {
        let a: boolean;
        a = false;
        if (this.vehicleSalesOrder.salesUnitRequirement.idmachine === null || this.vehicleSalesOrder.salesUnitRequirement.idmachine === undefined) {
            a = true;
        }

        return a;
    }

    previousState() {
        this.shipmentTMP = {};
        this.shipmentOutgoingService.changeShareDataTmp(this.shipmentTMP);
        this.router.navigate(['delivery-order']);
    }
}
