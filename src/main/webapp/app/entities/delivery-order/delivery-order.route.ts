import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DeliveryOrderComponent } from './delivery-order.component';
import { DeliveryOrderDetailComponent } from './delivery-order-detail.component';
import { DeliveryOrderCheckComponent } from './delivery-order-check.component';

@Injectable()
export class DeliveryOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const deliveryOrderRoute: Routes = [
    {
        path: 'delivery-order',
        component: DeliveryOrderComponent,
        resolve: {
            'pagingParams': DeliveryOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-order/:idOrder/:idShipment/:idSlip/delivery-order-detail',
        component: DeliveryOrderDetailComponent,
        resolve: {
            'pagingParams': DeliveryOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const deliveryOrderPopupRoute: Routes = [
    {
        path: 'delivery-order/delivery-order-detail',
        component: DeliveryOrderDetailComponent,
        resolve: {
            'pagingParams': DeliveryOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-order/:idOrder/:idShipment/:idSlip/delivery-order-detail',
        component: DeliveryOrderDetailComponent,
        resolve: {
            'pagingParams': DeliveryOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-order/:idSlip/delivery-order-check',
        component: DeliveryOrderCheckComponent,
        resolve: {
            'pagingParams': DeliveryOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'delivery-order/delivery-order-check',
        component: DeliveryOrderCheckComponent,
        resolve: {
            'pagingParams': DeliveryOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.deliveryOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
