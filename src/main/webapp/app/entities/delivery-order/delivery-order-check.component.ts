import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as ShipmentOutgoingConstrants from '../../shared/constants/shipmentOutgoing.constrants';

import {ShipmentOutgoingService, CustomShipment, ShipmentOutgoing} from './../shipment-outgoing';
import {PickingSlip, PickingSlipService} from './../picking-slip';
import {VehicleSalesOrder, VehicleSalesOrderService } from './../vehicle-sales-order';
import {OrderItem, OrderItemService } from './../order-item';

import {Mechanic, MechanicService} from '../mechanic';
import { DeliveryOrder, DeliveryOrderService } from './';

import {
    LazyLoadEvent,
    ConfirmationService
} from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-delivery-order-check',
    templateUrl: './delivery-order-check.component.html'
})

export class DeliveryOrderCheckComponent implements OnInit {

    protected subscription: Subscription;

    pickingSlip: PickingSlip;
    // shipmentTMP: PickingSlip;
    mechanicInternal: Mechanic[];
    mechanicExternal: Mechanic[];
    vehicleSalesOrder: VehicleSalesOrder;
    customShipment: CustomShipment;
    idOrder: string;
    idSlip: any;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    stat: number;

    constructor(
        protected orderItemService: OrderItemService,
        protected shipmentOutgoingService: ShipmentOutgoingService,
        protected deliveryOrderService: DeliveryOrderService,
        protected mechanicService: MechanicService,
        protected pickingSlipService: PickingSlipService,
        protected vehicleSalesOrderService: VehicleSalesOrderService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.mechanicExternal = new Array <Mechanic>();
        this.mechanicInternal = new Array <Mechanic>();
        this.stat = 1;
        this.idOrder = null;
    }

    ngOnInit() {
        this.shipmentOutgoingService.currentData.subscribe(
            (shipmentTMP) => {
                this.pickingSlip = shipmentTMP;
            });
        this.shipmentOutgoingService.passingData.subscribe(
            (customShipment) => {
                this.customShipment = customShipment;
            });
        this.subscription = this.activatedRoute.params.subscribe((params) => {
            if (params['idSlip']) {
                // this.idSlip = params['idSlip'];
                // this.loadAll(params[this.idSlip]);
            }
        });
        this.mechanicService.queryFilterBy({
            idInternal : this.principal.getIdInternal(),
            size: 1000})
        .subscribe(
            (res: ResponseWrapper) => this.mechanicType(res.json)
            , (res: ResponseWrapper) => this.onError(res.json));
        console.log('shipmentTMP', this.pickingSlip);
        console.log('customtmp', this.customShipment);
        this.loadAll();
    }
    loadAll() {
        // this.pickingSlipService.find(idSlip).subscribe(
        //     (data) => {
        //         this.pickingSlip = data;
        //         // console.log('kirim detail',  this.pickingSlip);
        //         // this.shipmentOutgoingService.changeShareDataTmp( this.pickingSlip);
        //         // this.router.navigate(['../delivery-order/delivery-order-check']);
        //     }
        // )
        this.vehicleSalesOrderService.find(this.customShipment.idOrder, this.principal.getIdInternal()).subscribe(
            (data) => {
                this.vehicleSalesOrder = data;
                console.log('vso', this.vehicleSalesOrder);
            // if (this.orderItem.length > 0) {
            //     this.orderItem[0].idFrame = data.salesUnitRequirement.idframe;
            //     this.orderItem[0].color = data.salesUnitRequirement.colorDescription;
            // }
            }
        );
    }
    public update(): void {
        this.subscribeToSaveResponse(this.pickingSlipService.update(this.pickingSlip));
        this.router.navigate(['delivery-order/' + this.vehicleSalesOrder.idOrder + '/' + this.customShipment.idShipment + '/' + this.customShipment.idSlip + '/delivery-order-detail']);
        // this.shipmentOutgoingService.update(this.pickingSlip));
     }

     protected subscribeToSaveResponse(result: Observable<PickingSlip>): void {
         result.subscribe(
            (res: PickingSlip) => this.onSaveSuccess(),
            (res: Response) => this.onSaveError(res)
        );
     }

     protected onSaveSuccess(): void {
         this.eventManager.broadcast({ name: 'ShipmentOutgoingListModification', content: 'OK'});
         this.toasterService.showToaster('info', 'Save', 'ShipmentOutgoing saved !');
        // this.kirimGudang(this.customShipment);
     }
     protected onSaveError(error): void {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    protected onSuccess(data): void {
        console.log('datanyadelivery', data);
        // data.dateSchedulle = new Date(data.dateSchedulle)
        this.pickingSlip = data;
    }

    protected onError(error): void {
        this.toasterService.showToaster('warning', 'data changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    public previousState(): void {
        this.router.navigate(['../delivery-order']);
    }

    protected mechanicType(data): void {
        console.log('mekanik', data);
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                if ( data[i].external === 'true') {
                    this.mechanicExternal.push(data[i]);
                } else {
                    this.mechanicInternal.push(data[i]);
                }
            }
        }
    }

    public status(): void {
        if (this.stat === 1) {
            this.pickingSlip.mechanicinternal = '';
        } else {
            this.pickingSlip.mechanicexternal = '';
        }
    }

    public trackMechanicById(index: number, item: Mechanic): string {
        return item.idMechanic;
    }
}
