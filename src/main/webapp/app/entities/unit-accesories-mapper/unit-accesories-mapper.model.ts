import { BaseEntity } from './../../shared';

export class UnitAccesoriesMapper implements BaseEntity {
    constructor(
        public id?: any,
        public idUnitAccMapper?: any,
        public acc1?: string,
        public qtyAcc1?: number,
        public acc2?: string,
        public qtyAcc2?: number,
        public acc3?: string,
        public qtyAcc3?: number,
        public acc4?: string,
        public qtyAcc4?: number,
        public acc5?: string,
        public qtyAcc5?: number,
        public acc6?: string,
        public qtyAcc6?: number,
        public acc7?: string,
        public qtyAcc7?: number,
        public promat1?: string,
        public qtyPromat1?: number,
        public promat2?: string,
        public qtyPromat2?: number,
        public motorId?: any,
    ) {
    }
}
