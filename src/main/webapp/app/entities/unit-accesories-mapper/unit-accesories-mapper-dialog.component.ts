import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {UnitAccesoriesMapper} from './unit-accesories-mapper.model';
import {UnitAccesoriesMapperPopupService} from './unit-accesories-mapper-popup.service';
import {UnitAccesoriesMapperService} from './unit-accesories-mapper.service';
import {ToasterService} from '../../shared';
import { Motor, MotorService } from '../motor';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-accesories-mapper-dialog',
    templateUrl: './unit-accesories-mapper-dialog.component.html'
})
export class UnitAccesoriesMapperDialogComponent implements OnInit {

    unitAccesoriesMapper: UnitAccesoriesMapper;
    isSaving: boolean;

    motors: Motor[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected unitAccesoriesMapperService: UnitAccesoriesMapperService,
        protected motorService: MotorService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.motorService.query()
            .subscribe((res: ResponseWrapper) => { this.motors = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitAccesoriesMapper.idUnitAccMapper !== undefined) {
            this.subscribeToSaveResponse(
                this.unitAccesoriesMapperService.update(this.unitAccesoriesMapper));
        } else {
            this.subscribeToSaveResponse(
                this.unitAccesoriesMapperService.create(this.unitAccesoriesMapper));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitAccesoriesMapper>) {
        result.subscribe((res: UnitAccesoriesMapper) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UnitAccesoriesMapper) {
        this.eventManager.broadcast({ name: 'unitAccesoriesMapperListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitAccesoriesMapper saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'unitAccesoriesMapper Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackMotorById(index: number, item: Motor) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-unit-accesories-mapper-popup',
    template: ''
})
export class UnitAccesoriesMapperPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitAccesoriesMapperPopupService: UnitAccesoriesMapperPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitAccesoriesMapperPopupService
                    .open(UnitAccesoriesMapperDialogComponent as Component, params['id']);
            } else {
                this.unitAccesoriesMapperPopupService
                    .open(UnitAccesoriesMapperDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
