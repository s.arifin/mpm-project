import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnitAccesoriesMapperComponent } from './unit-accesories-mapper.component';
import { UnitAccesoriesMapperPopupComponent } from './unit-accesories-mapper-dialog.component';

@Injectable()
export class UnitAccesoriesMapperResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idUnitAccMapper,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unitAccesoriesMapperRoute: Routes = [
    {
        path: 'unit-accesories-mapper',
        component: UnitAccesoriesMapperComponent,
        resolve: {
            'pagingParams': UnitAccesoriesMapperResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitAccesoriesMapper.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitAccesoriesMapperPopupRoute: Routes = [
    {
        path: 'unit-accesories-mapper-new',
        component: UnitAccesoriesMapperPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitAccesoriesMapper.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-accesories-mapper/:id/edit',
        component: UnitAccesoriesMapperPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitAccesoriesMapper.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
