import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';

import { UnitAccesoriesMapper } from './unit-accesories-mapper.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class UnitAccesoriesMapperService {
    protected itemValues: UnitAccesoriesMapper[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/unit-accesories-mappers';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/unit-accesories-mappers';

    constructor(protected http: Http) { }

    create(unitAccesoriesMapper: UnitAccesoriesMapper): Observable<UnitAccesoriesMapper> {
        const copy = this.convert(unitAccesoriesMapper);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(unitAccesoriesMapper: UnitAccesoriesMapper): Observable<UnitAccesoriesMapper> {
        const copy = this.convert(unitAccesoriesMapper);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<UnitAccesoriesMapper> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, unitAccesoriesMapper: UnitAccesoriesMapper): Observable<UnitAccesoriesMapper> {
        const copy = this.convert(unitAccesoriesMapper);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, unitAccesoriesMappers: UnitAccesoriesMapper[]): Observable<UnitAccesoriesMapper[]> {
        const copy = this.convertList(unitAccesoriesMappers);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(unitAccesoriesMapper: UnitAccesoriesMapper): UnitAccesoriesMapper {
        if (unitAccesoriesMapper === null || unitAccesoriesMapper === {}) {
            return {};
        }
        // const copy: UnitAccesoriesMapper = Object.assign({}, unitAccesoriesMapper);
        const copy: UnitAccesoriesMapper = JSON.parse(JSON.stringify(unitAccesoriesMapper));
        return copy;
    }

    protected convertList(unitAccesoriesMappers: UnitAccesoriesMapper[]): UnitAccesoriesMapper[] {
        const copy: UnitAccesoriesMapper[] = unitAccesoriesMappers;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: UnitAccesoriesMapper[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
