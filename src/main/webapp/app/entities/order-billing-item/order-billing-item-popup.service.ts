import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OrderBillingItem } from './order-billing-item.model';
import { OrderBillingItemService } from './order-billing-item.service';

@Injectable()
export class OrderBillingItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idOrderItem: any;
    idBillingItem: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected orderBillingItemService: OrderBillingItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.orderBillingItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.orderBillingItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new OrderBillingItem();
                    data.orderItemId = this.idOrderItem;
                    data.billingItemId = this.idBillingItem;
                    this.ngbModalRef = this.orderBillingItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    orderBillingItemModalRef(component: Component, orderBillingItem: OrderBillingItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderBillingItem = orderBillingItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.componentInstance.idBillingItem = this.idBillingItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.orderBillingItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    orderBillingItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
