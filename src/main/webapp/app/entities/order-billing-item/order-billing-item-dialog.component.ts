import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OrderBillingItem } from './order-billing-item.model';
import { OrderBillingItemPopupService } from './order-billing-item-popup.service';
import { OrderBillingItemService } from './order-billing-item.service';
import { ToasterService } from '../../shared';
import { OrderItem, OrderItemService } from '../order-item';
import { BillingItem, BillingItemService } from '../billing-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-order-billing-item-dialog',
    templateUrl: './order-billing-item-dialog.component.html'
})
export class OrderBillingItemDialogComponent implements OnInit {

    orderBillingItem: OrderBillingItem;
    isSaving: boolean;
    idOrderItem: any;
    idBillingItem: any;

    orderitems: OrderItem[];

    billingitems: BillingItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected orderBillingItemService: OrderBillingItemService,
        protected orderItemService: OrderItemService,
        protected billingItemService: BillingItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billingItemService.query()
            .subscribe((res: ResponseWrapper) => { this.billingitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orderBillingItem.idOrderBillingItem !== undefined) {
            this.subscribeToSaveResponse(
                this.orderBillingItemService.update(this.orderBillingItem));
        } else {
            this.subscribeToSaveResponse(
                this.orderBillingItemService.create(this.orderBillingItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrderBillingItem>) {
        result.subscribe((res: OrderBillingItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: OrderBillingItem) {
        this.eventManager.broadcast({ name: 'orderBillingItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orderBillingItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'orderBillingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    trackBillingItemById(index: number, item: BillingItem) {
        return item.idBillingItem;
    }
}

@Component({
    selector: 'jhi-order-billing-item-popup',
    template: ''
})
export class OrderBillingItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected orderBillingItemPopupService: OrderBillingItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderBillingItemPopupService
                    .open(OrderBillingItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.orderBillingItemPopupService.parent = params['parent'];
                this.orderBillingItemPopupService
                    .open(OrderBillingItemDialogComponent as Component);
            } else {
                this.orderBillingItemPopupService
                    .open(OrderBillingItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
