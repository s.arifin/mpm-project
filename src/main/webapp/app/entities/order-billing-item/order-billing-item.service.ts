import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OrderBillingItem } from './order-billing-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OrderBillingItemService {
    protected itemValues: OrderBillingItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/order-billing-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/order-billing-items';

    constructor(protected http: Http) { }

    create(orderBillingItem: OrderBillingItem): Observable<OrderBillingItem> {
        const copy = this.convert(orderBillingItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(orderBillingItem: OrderBillingItem): Observable<OrderBillingItem> {
        const copy = this.convert(orderBillingItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<OrderBillingItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<OrderBillingItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<OrderBillingItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to OrderBillingItem.
     */
    protected convertItemFromServer(json: any): OrderBillingItem {
        const entity: OrderBillingItem = Object.assign(new OrderBillingItem(), json);
        return entity;
    }

    /**
     * Convert a OrderBillingItem to a JSON which can be sent to the server.
     */
    protected convert(orderBillingItem: OrderBillingItem): OrderBillingItem {
        if (orderBillingItem === null || orderBillingItem === {}) {
            return {};
        }
        // const copy: OrderBillingItem = Object.assign({}, orderBillingItem);
        const copy: OrderBillingItem = JSON.parse(JSON.stringify(orderBillingItem));
        return copy;
    }

    protected convertList(orderBillingItems: OrderBillingItem[]): OrderBillingItem[] {
        const copy: OrderBillingItem[] = orderBillingItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: OrderBillingItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
