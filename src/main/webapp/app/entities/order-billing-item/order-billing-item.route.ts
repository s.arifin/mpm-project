import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OrderBillingItemComponent } from './order-billing-item.component';
import { OrderBillingItemLovPopupComponent } from './order-billing-item-as-lov.component';
import { OrderBillingItemPopupComponent } from './order-billing-item-dialog.component';

@Injectable()
export class OrderBillingItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrderBillingItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const orderBillingItemRoute: Routes = [
    {
        path: 'order-billing-item',
        component: OrderBillingItemComponent,
        resolve: {
            'pagingParams': OrderBillingItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderBillingItemPopupRoute: Routes = [
    {
        path: 'order-billing-item-lov',
        component: OrderBillingItemLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-billing-item-popup-new-list/:parent',
        component: OrderBillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-billing-item-new',
        component: OrderBillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-billing-item/:id/edit',
        component: OrderBillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
