import { BaseEntity } from './../../shared';

export class OrderBillingItem implements BaseEntity {
    constructor(
        public id?: any,
        public idOrderBillingItem?: any,
        public qty?: number,
        public amount?: number,
        public orderItemId?: any,
        public billingItemId?: any,
    ) {
    }
}
