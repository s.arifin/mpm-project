import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';

import {
    ApprovalVSODraftKorsalComponent,
    ApprovalSalesNoteKorsalDetailComponent,
    approvalKorsalRoute,
    ApprovalKorsalResolvePagingParams,
    ApprovalKorsalComponent,
    ApprovalKorsalUnitDocumentMessageComponent,
    TabViewSalesNoteComponent,
    ApprovalKorsalDraftVSOComponent
 } from './';

const ENTITY_STATES = [
    ...approvalKorsalRoute
];

import { MpmSalesUnitRequirementModule } from '../sales-unit-requirement';

import {
    DataListModule,
    PanelModule,
    ButtonModule,
    InputSwitchModule,
    FieldsetModule,
    ConfirmDialogModule,
    ConfirmationService,
    DataTableModule,
    AutoCompleteModule,
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    CalendarModule,
    DropdownModule,
    EditorModule,
    DataGridModule,
    DataScrollerModule,
    CarouselModule,
    PickListModule,
    PaginatorModule,
    DialogModule,
    GrowlModule,
    SharedModule,
    AccordionModule,
    TabViewModule,
    ScheduleModule,
    ListboxModule,
    ChartModule,
    DragDropModule,
    LightboxModule,
    MessagesModule,
    RadioButtonModule
} from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { CurrencyMaskModule } from 'ng2-currency-mask';

@NgModule({
    imports : [
        MpmSalesUnitRequirementModule,
        ConfirmDialogModule,
        PanelModule,
        ButtonModule,
        DataTableModule,
        FieldsetModule,
        InputSwitchModule,
        MpmSharedModule,
        DataListModule,
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        InputSwitchModule,
        CommonModule,
        EditorModule,
        AutoCompleteModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        DataListModule,
        DataGridModule,
        DataScrollerModule,
        CarouselModule,
        PickListModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        RadioButtonModule
    ],
    exports : [
        ApprovalKorsalComponent,
        ApprovalSalesNoteKorsalDetailComponent,
        ApprovalVSODraftKorsalComponent,
        TabViewSalesNoteComponent,
        ApprovalKorsalDraftVSOComponent
    ],
    declarations : [
        ApprovalKorsalUnitDocumentMessageComponent,
        ApprovalKorsalComponent,
        ApprovalSalesNoteKorsalDetailComponent,
        ApprovalVSODraftKorsalComponent,
        TabViewSalesNoteComponent,
        ApprovalKorsalDraftVSOComponent
    ],
    entryComponents : [
        ApprovalKorsalUnitDocumentMessageComponent,
        ApprovalKorsalComponent,
        ApprovalSalesNoteKorsalDetailComponent,
        ApprovalVSODraftKorsalComponent,
        TabViewSalesNoteComponent,
        ApprovalKorsalDraftVSOComponent
    ],
    providers : [
        ApprovalKorsalResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class MpmApprovalKorsalModule {}
