import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../shared/auth/account.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';

import { ApprovalService } from '../approval';
import {
    SalesUnitRequirement,
    SalesUnitRequirementService
} from '../sales-unit-requirement';

import * as BaseConstants from '../../shared/constants/base.constants';

@Component({
    selector: 'jhi-approval-vso-draft-korsal',
    templateUrl: './approval-vso-draft-korsal.component.html'
})

export class ApprovalVSODraftKorsalComponent implements OnInit {
    salesUnitRequirements: SalesUnitRequirement[];
    totalItems: any;
    queryCount: any;
    links: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentSearch: string;
    routeData: any;

    constructor(
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected approvalService: ApprovalService,
        protected principalService: Principal,
        protected activatedRoute: ActivatedRoute,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.loadData();
    }

    loadData(): void {
        const obj: Object = {
            idinternal : this.principalService.getIdInternal()
        }
        this.approvalService.getWaitingForApprovalSURForKorsal(obj).subscribe(
            (res) => {
                console.log('a', res.json);
                this.salesUnitRequirements = res.json;
            }
        )
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }
};
