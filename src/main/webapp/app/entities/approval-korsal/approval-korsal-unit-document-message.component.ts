import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../unit-document-message';

import * as BaseConstant from '../../shared/constants/base.constants';
import * as UnitDocumentMessageConstant from '../../shared/constants/unit-document-message.constants';

import { ResponseWrapper, CommonUtilService, Principal } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-approval-korsal-unit-document-message',
    templateUrl: './approval-korsal-unit-document-message.component.html'
})
export class ApprovalKorsalUnitDocumentMessageComponent implements OnInit {
    public customUnitDocumentMessages: Array<CustomUnitDocumentMessage>;
    public customUnitDocumentMessage: CustomUnitDocumentMessage;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;

    constructor(
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.customUnitDocumentMessages = new Array<CustomUnitDocumentMessage>();
        this.customUnitDocumentMessage = new CustomUnitDocumentMessage();
    }

    ngOnInit() {
        // this.loadData();
    }

    protected loadData(): void {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.dateFrom.setHours(0, 0, 0, 0);
        this.dateThru.setHours(23, 59, 59, 99);
        this.loadingService.loadingStart();
        this.unitDocumentMessageService.getDataByApprovalStatusAndRequirement({
            idInternal: this.principalService.getIdInternal(),
            startDate: this.dateFrom.toISOString(),
            endDate: this.dateThru.toISOString(),
            page: 0
        }).subscribe(
            (res) => {
                this.customUnitDocumentMessages = res.json;
                this.customUnitDocumentMessage = res.json;
                this.loadingService.loadingStop();
            }
        );
    }

    protected reset(): void {
        this.customUnitDocumentMessages = new Array<CustomUnitDocumentMessage>();
        setTimeout(() => {
            this.loadData();
        }, 1000);
    }

    public submit(id: number, type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header : 'Confirmation',
                message : 'Do you want to approve this sales note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: UnitDocumentMessage) => {
                            const objectHeader: Object = {
                                execute: UnitDocumentMessageConstant.SALES_NOTE_APPROVED,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                item: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        } else if (type === 'reject') {
            this.confirmationService.confirm({
                header : 'Confirmation',
                message : 'Do you want to reject this sales note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: UnitDocumentMessage) => {
                            const objectHeader: Object = {
                                execute: UnitDocumentMessageConstant.SALES_NOTE_REJECTED,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                items: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody , objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    this.reset();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }

    // private onSuccess(data, headers) {
    //     // this.links = this.parseLinks.parse(headers.get('link'));
    //     // this.totalItems = headers.get('X-Total-Count');
    //     // this.queryCount = this.totalItems;
    //     this.loadingService.loadingStop();
    //     // this.page = pagingParams.page;
    //     this.customUnitDocumentMessages = data;
    //     console.log('idreq di sales note ==', this.customUnitDocumentMessages[0].idreq)
    // }
}
