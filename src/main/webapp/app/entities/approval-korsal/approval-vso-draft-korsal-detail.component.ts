import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovalService } from '../approval';
import { AccountService } from '../../shared/auth/account.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import * as UnitDocumentMessageConstant from '../../shared/constants/unit-document-message.constants';

import { ConfirmationService } from 'primeng/primeng';

import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';

import * as BaseConstants from '../../shared/constants/base.constants';
import * as SalesUnitRequirementConstant from '../../shared/constants/sales-unit-requirement.constants';
import { UnitDocumentMessageService, UnitDocumentMessage, CustomUnitDocumentMessage } from '../unit-document-message';
import { LoadingService } from '../../layouts';

@Component({
    selector: 'jhi-approval-vso-draft-korsal-detail',
    templateUrl: './approval-vso-draft-korsal-detail.component.html'
})
export class ApprovalVSODraftKorsalDetailComponent implements OnInit {

    public customUnitDocumentMessages: Array<CustomUnitDocumentMessage>;
    isApprovalSubsidi: boolean;

    salesUnitRequirementId: String;

    salesUnitRequirement: SalesUnitRequirement;

    baseConstants: any;
    idMessage: any;

    constructor(
        protected route: ActivatedRoute,
        protected alertService: JhiAlertService,
        protected router: Router,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected loadingService: LoadingService,
        protected principalService: Principal,
        protected commonUtilService: CommonUtilService,
    ) {
        this.isApprovalSubsidi = false;
        this.baseConstants = BaseConstants;
        this.salesUnitRequirement = new SalesUnitRequirement;
        this.idMessage = null;
    }

    ngOnInit() {
        this.route.params.subscribe(
            (params) => {
                if (params['id']) {
                    if (params['idMessage']) {
                        this.idMessage = params['idMessage'];
                    }
                    this.salesUnitRequirementId = params['id'];
                    this.loadSalesUnitRequirement(params['id']);
                }
            }
        )
    }

    protected loadSalesUnitRequirement(id): void {
        this.salesUnitRequirementService.find(id).subscribe(
            (data) => {
                this.salesUnitRequirement = data;
            }
        )
    }

    public confirmApprove(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to submit this <b>Approval</b>?',
            accept: () => {

                if (this.salesUnitRequirement.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
                    if (this.isApprovalSubsidi) {
                        this.salesUnitRequirement.approvalDiscount = BaseConstants.Status.STATUS_APPROVED;
                    } else {
                        this.salesUnitRequirement.approvalDiscount = BaseConstants.Status.STATUS_REJECTED;
                    }
                }

                this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.APPROVAL_KORSAL, null, this.salesUnitRequirement).subscribe(
                    (res) => {
                        this.previousState();
                    }
                );
            }
        });
    }

    public previousState(): void {
        this.router.navigate(['approval-korsal/unit-document-message']);
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    public submit(id: number, type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header : 'Konfirmasi',
                message : 'Apakah Anda Yakin Approve Note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: UnitDocumentMessage) => {
                            const objectHeader: Object = {
                                execute: UnitDocumentMessageConstant.SALES_NOTE_APPROVED,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                item: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody, objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    // this.reset();
                                    this.previousState();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }else if (type === 'reject') {
            this.confirmationService.confirm({
                header : 'Konfirmasi',
                message : 'Apakah Anda Yakin Tidak Approve Note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    this.unitDocumentMessageService.find(id).subscribe(
                        (res: UnitDocumentMessage) => {
                            const objectHeader: Object = {
                                execute: UnitDocumentMessageConstant.SALES_NOTE_REJECTED,
                                idInternal: this.principalService.getIdInternal()
                            }

                            const objectBody: Object = {
                                items: res
                            }

                            this.unitDocumentMessageService.executeProcess(objectBody , objectHeader).subscribe(
                                (res2) => {
                                    this.loadingService.loadingStop();
                                    // this.reset();
                                    this.previousState();
                                },
                                (err) => {
                                    this.loadingService.loadingStop();
                                    this.commonUtilService.showError(err);
                                }
                            )
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }

    // protected reset(): void {
    //     this.customUnitDocumentMessages = new Array<CustomUnitDocumentMessage>();
    //     setTimeout(() => {
    //         this.loadSalesUnitRequirement(thi)
    //     }, 1000);
    // }
}
