import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovalService } from '../approval';
import { AccountService } from '../../shared/auth/account.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { LazyLoadEvent } from 'primeng/primeng';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ConfirmationService } from 'primeng/primeng';

import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';

import * as BaseConstants from '../../shared/constants/base.constants';
import * as SalesUnitRequirementConstant from '../../shared/constants/sales-unit-requirement.constants';

@Component({
    selector: 'jhi-approval-korsal-draft-vso',
    templateUrl: './approval-korsal-draft-vso.component.html'
})
export class ApprovalKorsalDraftVSOComponent implements OnInit {
    isApprovalSubsidi: boolean;

    salesUnitRequirementId: String;

    salesUnitRequirement: SalesUnitRequirement;

    baseConstants: any;

    constructor(
        private route: ActivatedRoute,
        private alertService: JhiAlertService,
        private router: Router,
        private salesUnitRequirementService: SalesUnitRequirementService,
        private confirmationService: ConfirmationService
    ) {
        this.isApprovalSubsidi = false;
        this.baseConstants = BaseConstants;
        this.salesUnitRequirement = new SalesUnitRequirement;
    }

    ngOnInit() {
        this.route.params.subscribe(
            (params) => {
                if (params['id']) {
                    this.salesUnitRequirementId = params['id'];
                    this.loadSalesUnitRequirement();
                }
            }
        )
    }

    private loadSalesUnitRequirement(): void {
        this.salesUnitRequirementService.find(this.salesUnitRequirementId).subscribe(
            (data) => {
                this.salesUnitRequirement = data;
            }
        )
    }

    public confirmApprove(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to submit this <b>Approval</b>?',
            accept: () => {

                if (this.salesUnitRequirement.approvalDiscount === BaseConstants.Status.STATUS_WAITING_FOR_APPROVAL) {
                    if (this.isApprovalSubsidi) {
                        this.salesUnitRequirement.approvalDiscount = BaseConstants.Status.STATUS_APPROVED;
                    } else {
                        this.salesUnitRequirement.approvalDiscount = BaseConstants.Status.STATUS_REJECTED;
                    }
                }

                this.salesUnitRequirementService.executeProcess(SalesUnitRequirementConstant.APPROVAL_KORSAL, null, this.salesUnitRequirement).subscribe(
                    (res) => {
                        this.previousState();
                    }
                );
            }
        });
    }

    public previousState(): void {
        this.router.navigate(['approval-korsal/vso-draft']);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
