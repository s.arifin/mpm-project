import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomUnitDocumentMessage, UnitDocumentMessageService } from '../../unit-document-message';
import { ConfirmationService, LazyLoadEvent } from 'primeng/primeng';
import { LoadingService } from '../../../layouts';
import { CommonUtilService, Principal, ITEMS_PER_PAGE, ResponseWrapper } from '../../../shared';
import { SalesUnitRequirementService, SalesUnitRequirement } from '../../sales-unit-requirement';
import { JhiParseLinks, JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-tab-view-sales-note',
    templateUrl: './tab-view-sales-note.component.html'
})
export class TabViewSalesNoteComponent implements OnInit {

    @Input() idreq: any;
    @Input() idstatus: string;
    @Input() idMessage: any;
    public customUnitDocumentMessages: Array<CustomUnitDocumentMessage>;
    public customUnitDocumentMessage: CustomUnitDocumentMessage;
    salesUnitRequirement: SalesUnitRequirement;
    tanggal: Date;
    dateFrom: Date;
    dateThru: Date;
    idReq: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected confirmationService: ConfirmationService,
        protected unitDocumentMessageService: UnitDocumentMessageService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        protected principalService: Principal,
        protected salesUnitrequirementService: SalesUnitRequirementService,
        protected activatedRoute: ActivatedRoute,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService
    ) {
        this.dateFrom = new Date();
        this.dateThru = new Date();
        this.tanggal = new Date();
        this.salesUnitRequirement = new SalesUnitRequirement();

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        // this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.loadData();
    }

    protected loadData(): void {
        this.loadingService.loadingStart();
        this.unitDocumentMessageService.getDataByApprovalStatusAndRequirement({
            idInternal: this.principalService.getIdInternal(),
            idStatustype: this.idstatus,
            page: this.page - 1,
            sort: this.sort
        }).subscribe(
            (res: ResponseWrapper) => {
                // this.onSuccess(res.json, res.headers);
                this.customUnitDocumentMessages = res.json;
                this.customUnitDocumentMessage = res.json;
                this.loadingService.loadingStop();
            },
            (res: ResponseWrapper) => {
                this.onError(res.json);
            }
        );
    }
    public trackId(index: number, item: CustomUnitDocumentMessage): string {
        return item.idRequirement;
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadData();
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.loadingService.loadingStop();
        this.customUnitDocumentMessages = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
        this.loadingService.loadingStop();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'desc' : 'asc')];
        if (this.predicate !== 'idMessage') {
            result.push('idMessage');
        }
        return result;
    }

}
