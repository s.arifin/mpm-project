import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ApprovalVSODraftKorsalComponent } from './approval-vso-draft-korsal.component';
import { ApprovalSalesNoteKorsalDetailComponent } from './approval-sales-note-korsal-detail.component';
import { ApprovalKorsalComponent } from './approval-korsal.component';
import { ApprovalKorsalUnitDocumentMessageComponent } from './approval-korsal-unit-document-message.component';
import { ApprovalKorsalDraftVSOComponent } from './approval-korsal-draft-vso.component';
@Injectable()
export class ApprovalKorsalResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequirement,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const approvalKorsalRoute: Routes = [
    {
        path : 'approval-korsal',
        component: ApprovalKorsalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.kacab'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-korsal/unit-document-message',
        component: ApprovalKorsalUnitDocumentMessageComponent,
        resolve: {
            'pagingParams': ApprovalKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.korsal.approval-unit-document-message'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'approval-korsal/vso-draft',
        component: ApprovalVSODraftKorsalComponent,
        resolve: {
            'pagingParams': ApprovalKorsalResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approval.korsal-vso-draft'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path : 'approval-korsal/vso-draft/:id/:idMessage/:idStatus/detail-approval',
        component : ApprovalSalesNoteKorsalDetailComponent,
        data : {
            authorities : ['ROLE_USER'],
            pageTitle : 'mpmApp.approval.korsal-vso-draft'
        },
        canActivate : [UserRouteAccessService]
    },
    {
        path : 'approval-korsal/vso-draft/:id/detail-approval',
        component : ApprovalKorsalDraftVSOComponent,
        data : {
            authorities : ['ROLE_USER'],
            pageTitle : 'mpmApp.approval.korsal-vso-draft'
        },
        canActivate : [UserRouteAccessService]
    }
];
