import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    MotorPriceComponent,
    MotorService,
    MotorPopupService,
    MotorComponent,
    MotorDialogComponent,
    MotorPopupComponent,
    motorRoute,
    motorPopupRoute,
    MotorHotItemComponent,
    MotorResolvePagingParams,
    MotorDocumentComponent,
    MotorIndentComponent
} from './';

import { CommonModule } from '@angular/common';

import {CurrencyMaskModule} from 'ng2-currency-mask';

import {
        CheckboxModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        InputTextareaModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmationService,
        GrowlModule,
        DataGridModule,
        SharedModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        ScheduleModule,
        PanelModule,
        ListboxModule,
        SelectItem,
        MenuItem,
        Header,
        Footer} from 'primeng/primeng';

import { MpmPriceComponentModule } from '../price-component/price-component.module';
import { MpmRuleHotItemModule } from '../rule-hot-item/rule-hot-item.module';
import { MpmProductDocumentModule } from '../product-document/product-document.module';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...motorRoute,
    ...motorPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedEntityModule,
        MpmProductDocumentModule,
        InputTextareaModule,
        // MpmRuleHotItemModule,
        MpmSharedModule,
        MpmPriceComponentModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        MotorComponent,
        MotorPriceComponent
    ],
    declarations: [
        MotorIndentComponent,
        MotorDocumentComponent,
        MotorHotItemComponent,
        MotorComponent,
        MotorPriceComponent,
        MotorDialogComponent,
        MotorPopupComponent,
    ],
    entryComponents: [
        MotorIndentComponent,
        MotorDocumentComponent,
        MotorHotItemComponent,
        MotorComponent,
        MotorPriceComponent,
        MotorDialogComponent,
        MotorPopupComponent,
    ],
    providers: [
        MotorService,
        MotorPopupService,
        MotorResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmMotorModule {}
