import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    templateUrl: './motor-document.component.html'
})

export class MotorDocumentComponent implements OnInit {
    idProduct: string;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router
    ) {}

    ngOnInit() {
        this.route.params.subscribe((params) => {
            if (params['idproduct']) {
                this.idProduct = params['idproduct'];
            }
        });
    }

    previousState() {
        this.router.navigate(['motor']);
    }
}
