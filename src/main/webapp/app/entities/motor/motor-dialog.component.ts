import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import { Motor} from './motor.model';
import { MotorPopupService } from './motor-popup.service';
import { MotorService } from './motor.service';
import { ToasterService } from '../../shared';
import { Uom, UomService } from '../uom';
import { Feature, FeatureService } from '../feature';
import { ProductCategory, ProductCategoryService } from '../product-category';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-motor-dialog',
    templateUrl: './motor-dialog.component.html'
})
export class MotorDialogComponent implements OnInit {

    motor: Motor;
    isSaving: boolean;

    uoms: Uom[];

    features: Feature[];

    productcategories: ProductCategory[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected motorService: MotorService,
        protected uomService: UomService,
        protected featureService: FeatureService,
        protected productCategoryService: ProductCategoryService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomService.query()
            .subscribe((res: ResponseWrapper) => { this.uoms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query({page: 0, size: 10000, sort : ['idFeature', 'asc']})
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productCategoryService.query({page: 0, size: 100, sort : ['idCategory', 'asc']})
            .subscribe((res: ResponseWrapper) => { this.productcategories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.motor.idProduct !== undefined) {
            this.subscribeToSaveResponse(
                this.motorService.update(this.motor));
        } else {
            this.subscribeToSaveResponse(
                this.motorService.create(this.motor));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Motor>) {
        result.subscribe((res: Motor) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Motor) {
        this.eventManager.broadcast({ name: 'motorListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'motor saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'motor Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackUomById(index: number, item: Uom) {
        return item.idUom;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackProductCategoryById(index: number, item: ProductCategory) {
        return item.idCategory;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-motor-popup',
    template: ''
})
export class MotorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected motorPopupService: MotorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.motorPopupService
                    .open(MotorDialogComponent as Component, params['id']);
            } else {
                this.motorPopupService
                    .open(MotorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
