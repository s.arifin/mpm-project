import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { Subscription, Observable } from 'rxjs/Rx';

@Component({
    selector: 'jhi-motor-indent',
    templateUrl: './motor-indent.component.html'
})

export class MotorIndentComponent implements OnInit {
    protected subscription: Subscription;

    public idProduct: string;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router
    ) {

    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['idproduct']) {
                this.idProduct = params['idproduct'];
            }
        });
    }

    public previousState(): void {
        this.router.navigate(['motor']);
    }
}
