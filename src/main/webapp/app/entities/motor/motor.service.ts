import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Motor } from './motor.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class MotorService {

    protected resourceUrl = 'api/motors';
    protected resourceSearchUrl = 'api/_search/motors';
    protected resourceCUrl = process.env.API_C_URL + '/api/motors';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    getPrice(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idinternal', req.idinternal);
        options.params.set('datewhen', req.datewhen);

        return this.http.get(this.resourceUrl + '/price', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getPriceC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idinternal', req.idinternal);
        options.params.set('datewhen', req.datewhen);

        return this.http.get(this.resourceCUrl + '/price', options)
            .map((res: Response) => this.convertResponse(res));
    }

    create(motor: Motor): Observable<Motor> {
        const copy = this.convert(motor);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(motor: Motor): Observable<Motor> {
        const copy = this.convert(motor);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    updateC(motor: Motor): Observable<Motor> {
        const copy = this.convert(motor);
        return this.http.put(this.resourceCUrl + '/' + motor.idProduct, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Motor> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findC(id: any): Observable<Motor> {
        return this.http.get(`${this.resourceCUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findPromise(id: any): Promise<Motor> {
        return this.http.get(`${this.resourceUrl}/${id}`)
            .toPromise()
            .then((res: Response) => {
                const jsonResponse = res.json();
                this.convertItemFromServer(jsonResponse);
                return Promise.resolve(jsonResponse);
        });
    }

    getMotorHotItem(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/hot-item')
            .map((res: Response) => this.convertResponse(res));
    }

    getMotorHotItemc(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl + '/hot-item', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getMotorAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/all')
            .map((res: Response) => this.convertResponse(res));
    }

    getAll(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/getall')
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceCUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    deleteC(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceCUrl}/${id}`);
    }

    executeProcess(id: number, param: String, motor: any): Observable<String> {
        const copy = this.convert(motor);
        return this.http.post(`${this.resourceUrl}/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateIntroduction) {
            entity.dateIntroduction = new Date(entity.dateIntroduction);
        }
        if (entity.dateDiscontinue) {
            entity.dateDiscontinue = new Date(entity.dateDiscontinue);
        }
    }

    protected convert(motor: Motor): Motor {
        if (motor === null || motor === {}) {
            return {};
        }
        const copy: Motor = Object.assign({}, motor);

        // copy.dateIntroduction = this.dateUtils.toDate(motor.dateIntroduction);
        return copy;
    }

    public convertMotorForSelectPrimeNg(motors: Array<Motor>): Array<Object> {
        const arr: Array<Object> = new Array<Object>();

        if (motors.length > 0) {
            motors.forEach(
                (m) => {
                    const obj: object = {
                        label : m.idProduct + ' - ' + m.description + ' - ' + m.name,
                        value : m.idProduct
                    };
                    arr.push(obj);
                }
            )
        }

        return arr;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

}
