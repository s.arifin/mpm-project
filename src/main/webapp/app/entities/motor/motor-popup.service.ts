import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Motor } from './motor.model';
import { MotorService } from './motor.service';

@Injectable()
export class MotorPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected motorService: MotorService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.motorService.find(id).subscribe((data) => {
                    if (data.dateIntroduction) {
                        data.dateIntroduction = new Date(data.dateIntroduction);
                    }
                    this.ngbModalRef = this.motorModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Motor();
                    this.ngbModalRef = this.motorModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    motorModalRef(component: Component, motor: Motor): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.motor = motor;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
