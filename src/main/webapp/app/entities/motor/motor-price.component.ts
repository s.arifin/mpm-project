import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { Subscription, Observable } from 'rxjs/Rx';

@Component({
    templateUrl: './motor-price.component.html'
})

export class MotorPriceComponent implements OnInit {
    protected subscription: Subscription;

    idProduct: string;

    constructor(
        protected route: ActivatedRoute,
        protected router: Router
    ) {}

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['idproduct']) {
                this.idProduct = params['idproduct'];
            }
        });
    }

    previousState() {
        this.router.navigate(['motor']);
    }
}
