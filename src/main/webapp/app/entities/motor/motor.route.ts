import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MotorComponent } from './motor.component';
import { MotorPopupComponent } from './motor-dialog.component';
import { MotorPriceComponent } from './motor-price.component';
import { MotorHotItemComponent } from './motor-hot-item.component';
import { MotorDocumentComponent } from './motor-document.component';
import { ProductDocumentResolvePagingParams } from '../product-document';
import { MotorIndentComponent } from './motor-indent.component';

@Injectable()
export class MotorResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProduct,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const motorRoute: Routes = [
    {
        path: 'motor',
        component: MotorComponent,
        resolve: {
            'pagingParams': MotorResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'motor/:idproduct/indent',
        component: MotorIndentComponent,
        resolve: {
            'pagingParams': ProductDocumentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'motor/:idproduct/images',
        component: MotorDocumentComponent,
        resolve: {
            'pagingParams': ProductDocumentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'motor/:idproduct/hot-item',
        component: MotorHotItemComponent,
        resolve: {
            'pagingParams': MotorResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'motor/:idproduct/price',
        component: MotorPriceComponent,
        resolve: {
            'pagingParams': MotorResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const motorPopupRoute: Routes = [
    {
        path: 'motor-new',
        component: MotorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'motor/:id/edit',
        component: MotorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.motor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
