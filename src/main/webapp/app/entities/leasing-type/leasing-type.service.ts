import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { LeasingType } from './leasing-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class LeasingTypeService {

    protected resourceCUrl = process.env.API_C_URL + '/api/leasing-type';

    constructor(protected http: Http) { }

    getAllDropdown(): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/getall')
            .map((res: Response) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(leasingType: LeasingType): LeasingType {
        const copy: LeasingType = Object.assign({}, leasingType);
        return copy;
    }
}
