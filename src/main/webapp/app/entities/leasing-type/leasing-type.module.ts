import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    LeasingTypeService,
} from './';

@NgModule({
    imports: [
        MpmSharedModule,
    ],
    exports: [
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
        LeasingTypeService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmLeasingTypeModule {}
