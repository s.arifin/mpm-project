export class LeasingType {
    constructor(
        public idleatyp?: number,
        public description?: string,
    ) {
    }
}
