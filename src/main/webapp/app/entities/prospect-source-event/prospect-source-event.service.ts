import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ProspectSourceEvent } from './prospect-source-event.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProspectSourceEventService {
    protected itemValues: ProspectSourceEvent[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/prospect-source-events';
    protected resourceSearchUrl = 'api/_search/prospect-source-events';

    constructor(protected http: Http) { }

    create(prospectSourceEvent: ProspectSourceEvent): Observable<ProspectSourceEvent> {
        const copy = this.convert(prospectSourceEvent);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(prospectSourceEvent: ProspectSourceEvent): Observable<ProspectSourceEvent> {
        const copy = this.convert(prospectSourceEvent);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<ProspectSourceEvent> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, prospectSourceEvent: ProspectSourceEvent): Observable<ProspectSourceEvent> {
        const copy = this.convert(prospectSourceEvent);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, prospectSourceEvents: ProspectSourceEvent[]): Observable<ProspectSourceEvent[]> {
        const copy = this.convertList(prospectSourceEvents);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(prospectSourceEvent: ProspectSourceEvent): ProspectSourceEvent {
        if (prospectSourceEvent === null || prospectSourceEvent === {}) {
            return {};
        }
        // const copy: ProspectSourceEvent = Object.assign({}, prospectSourceEvent);
        const copy: ProspectSourceEvent = JSON.parse(JSON.stringify(prospectSourceEvent));
        return copy;
    }

    protected convertList(prospectSourceEvents: ProspectSourceEvent[]): ProspectSourceEvent[] {
        const copy: ProspectSourceEvent[] = prospectSourceEvents;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProspectSourceEvent[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
