import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProspectSourceEventComponent } from './prospect-source-event.component';
import { ProspectSourceEventPopupComponent } from './prospect-source-event-dialog.component';

@Injectable()
export class ProspectSourceEventResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProspectEventSource,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const prospectSourceEventRoute: Routes = [
    {
        path: 'prospect-source-event',
        component: ProspectSourceEventComponent,
        resolve: {
            'pagingParams': ProspectSourceEventResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectSourceEvent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const prospectSourceEventPopupRoute: Routes = [
    {
        path: 'prospect-source-event-new',
        component: ProspectSourceEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectSourceEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'prospect-source-event/:id/edit',
        component: ProspectSourceEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.prospectSourceEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
