import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ProspectSourceEvent} from './prospect-source-event.model';
import {ProspectSourceEventPopupService} from './prospect-source-event-popup.service';
import {ProspectSourceEventService} from './prospect-source-event.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-prospect-source-event-dialog',
    templateUrl: './prospect-source-event-dialog.component.html'
})
export class ProspectSourceEventDialogComponent implements OnInit {

    prospectSourceEvent: ProspectSourceEvent;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected prospectSourceEventService: ProspectSourceEventService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.prospectSourceEvent.idProspectEventSource !== undefined) {
            this.subscribeToSaveResponse(
                this.prospectSourceEventService.update(this.prospectSourceEvent));
        } else {
            this.subscribeToSaveResponse(
                this.prospectSourceEventService.create(this.prospectSourceEvent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProspectSourceEvent>) {
        result.subscribe((res: ProspectSourceEvent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProspectSourceEvent) {
        this.eventManager.broadcast({ name: 'prospectSourceEventListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'prospectSourceEvent saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'prospectSourceEvent Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-prospect-source-event-popup',
    template: ''
})
export class ProspectSourceEventPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected prospectSourceEventPopupService: ProspectSourceEventPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.prospectSourceEventPopupService
                    .open(ProspectSourceEventDialogComponent as Component, params['id']);
            } else {
                this.prospectSourceEventPopupService
                    .open(ProspectSourceEventDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
