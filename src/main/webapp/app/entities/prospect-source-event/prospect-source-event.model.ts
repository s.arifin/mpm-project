import { BaseEntity } from './../../shared';

export class ProspectSourceEvent implements BaseEntity {
    constructor(
        public id?: number,
        public idProspectEventSource?: number,
        public description?: string,
    ) {
    }
}
