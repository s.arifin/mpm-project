import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitShipmentReceipt } from './unit-shipment-receipt.model';
import { UnitShipmentReceiptPopupService } from './unit-shipment-receipt-popup.service';
import { UnitShipmentReceiptService } from './unit-shipment-receipt.service';
import { ToasterService } from '../../shared';
import { ShipmentPackage, ShipmentPackageService } from '../shipment-package';
import { ShipmentItem, ShipmentItemService } from '../shipment-item';
import { OrderItem, OrderItemService } from '../order-item';
import { ResponseWrapper } from '../../shared';
import { Feature} from '../feature';
import { Motor, MotorService } from '../motor';
import { Product, ProductService } from '../product';
import { CommonUtilService } from '../../shared';
import * as _ from 'lodash';
import { LoadingService } from '../../layouts/loading/loading.service';

@Component({
    selector: 'jhi-unit-shipment-receipt-dialog',
    templateUrl: './unit-shipment-receipt-dialog.component.html'
})
export class UnitShipmentReceiptDialogComponent implements OnInit {

    unitShipmentReceipt: UnitShipmentReceipt;
    isSaving: boolean;
    idShipmentPackage: any;
    idShipmentItem: any;
    idOrderItem: any;
    qtyAccept: number;
    qtyReject: number;
    isReceipt: boolean;

    shipmentpackages: ShipmentPackage[];

    shipmentitems: ShipmentItem[];

    orderitems: OrderItem[];
    public motors: Motor[];
    public selectedMotor: any;
    public features: Feature[];
    public listMotors = [{label: 'Please Select', value: null}];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected unitShipmentReceiptService: UnitShipmentReceiptService,
        protected shipmentPackageService: ShipmentPackageService,
        protected shipmentItemService: ShipmentItemService,
        protected orderItemService: OrderItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected motorService: MotorService,
        protected productService: ProductService,
        protected commonUtilService: CommonUtilService,
        protected loadingService: LoadingService
    ) {
        this.qtyAccept = 1;
        this.isReceipt = false;
        this.qtyReject = 0;
    }

    protected resetMe() {
        this.selectedMotor = null;
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentPackageService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentpackages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipmentItemService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        // this.orderItemService.query()
        //     .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.motorService.query({
            page: 0,
            size: 10000,
            sort: ['idProduct', 'asc']
        }).subscribe((res: ResponseWrapper) => {
            this.motors = res.json;
            this.motors.forEach((element) => {
                this.listMotors.push({
                    label: element.idProduct + ' - ' + element.description + ' - ' + element.name,
                    value: element.idProduct });
            });
        },
            (res: ResponseWrapper) => {
                this.commonUtilService.showError(res.json);
            });
    }

    public selectMotor(isSelect?: boolean): void {
        this.unitShipmentReceipt.idProduct = this.selectedMotor;

        this.productService.find(this.selectedMotor)
        .subscribe(
            (res) => {
                this.unitShipmentReceipt.itemDescription = res.description;
            }
        )
        this.getColorByMotor(this.unitShipmentReceipt.idProduct);
    }

    protected getColorByMotor(idProduct: string): void {
        if (idProduct !== null) {
            this.features = new Array<Feature>();
            const selectedProduct: Motor = _.find(this.motors, function(e) {
                return e.idProduct.toLowerCase() === idProduct.toLowerCase();
            });
            this.features = selectedProduct.features;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.unitShipmentReceipt.qtyReject = this.qtyReject;
        this.unitShipmentReceipt.receipt = this.isReceipt;
        this.unitShipmentReceipt.qtyAccept = this.qtyAccept;
        this.isSaving = true;
        if (this.unitShipmentReceipt.idReceipt !== undefined) {
            this.subscribeToSaveResponse(
                this.unitShipmentReceiptService.update(this.unitShipmentReceipt));
        } else {
            this.subscribeToSaveResponse(
                this.unitShipmentReceiptService.create(this.unitShipmentReceipt));
        }
    }

    // protected subscribeToSaveResponse(result: Observable<UnitShipmentReceipt>) {
    //     result.subscribe((res: UnitShipmentReceipt) =>
    //         this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    // }

    protected subscribeToSaveResponse(result: Observable<UnitShipmentReceipt>): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                result.subscribe(
                    (res: UnitShipmentReceipt) => {
                        this.onSaveSuccess(res);
                        resolve();
                    },
                    (err) => {
                        this.loadingService.loadingStop();
                        this.commonUtilService.showError(err);
                        this.isSaving = false;
                        resolve();
                    }
                );
            }
        );
    }

    protected onSaveSuccess(result: UnitShipmentReceipt) {
        this.eventManager.broadcast({ name: 'unitShipmentReceiptListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitShipmentReceipt saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'unitShipmentReceipt Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentPackageById(index: number, item: ShipmentPackage) {
        return item.idPackage;
    }

    trackShipmentItemById(index: number, item: ShipmentItem) {
        return item.idShipmentItem;
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }
}

@Component({
    selector: 'jhi-unit-shipment-receipt-popup',
    template: ''
})
export class UnitShipmentReceiptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitShipmentReceiptPopupService: UnitShipmentReceiptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitShipmentReceiptPopupService
                    .open(UnitShipmentReceiptDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.unitShipmentReceiptPopupService.idShipmentPackage = params['parent'];
                this.unitShipmentReceiptPopupService
                    .open(UnitShipmentReceiptDialogComponent as Component);
            } else {
                this.unitShipmentReceiptPopupService
                    .open(UnitShipmentReceiptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
