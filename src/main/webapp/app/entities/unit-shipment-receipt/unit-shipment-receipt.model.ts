import { BaseEntity } from './../../shared';

export class UnitShipmentReceipt implements BaseEntity {
    constructor(
        public id?: any,
        public idReceipt?: any,
        public idProduct?: string,
        public idFeature?: number,
        public featureRefKey?: number,
        public featureDescription?: number,
        public receiptCode?: string,
        public qtyAccept?: number,
        public qtyReject?: number,
        public itemDescription?: string,
        public dateReceipt?: any,
        public idFrame?: string,
        public idMachine?: string,
        public yearAssembly?: number,
        public unitPrice?: number,
        public acc1?: boolean,
        public acc2?: boolean,
        public acc3?: boolean,
        public acc4?: boolean,
        public acc5?: boolean,
        public acc6?: boolean,
        public acc7?: boolean,
        public shipmentPackageId?: any,
        public shipmentItemId?: any,
        public orderItemId?: any,
        public inventoryItems?: any,
        public receipt?: boolean,
    ) {
        this.acc1 = false;
        this.acc2 = false;
        this.acc3 = false;
        this.acc4 = false;
        this.acc5 = false;
        this.acc6 = false;
        this.acc7 = false;
    }
}
