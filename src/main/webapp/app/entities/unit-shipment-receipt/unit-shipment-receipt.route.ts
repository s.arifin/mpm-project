import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UnitShipmentReceiptComponent } from './unit-shipment-receipt.component';
import { UnitShipmentReceiptPopupComponent } from './unit-shipment-receipt-dialog.component';

@Injectable()
export class UnitShipmentReceiptResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReceipt,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unitShipmentReceiptRoute: Routes = [
    {
        path: 'unit-shipment-receipt',
        component: UnitShipmentReceiptComponent,
        resolve: {
            'pagingParams': UnitShipmentReceiptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitShipmentReceiptPopupRoute: Routes = [
    {
        path: 'unit-shipment-receipt-popup-new-list/:parent',
        component: UnitShipmentReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-shipment-receipt-new',
        component: UnitShipmentReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-shipment-receipt/:id/edit',
        component: UnitShipmentReceiptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentReceipt.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
