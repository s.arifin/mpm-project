import { Component, OnInit, OnChanges, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { UnitShipmentReceipt } from './unit-shipment-receipt.model';
import { UnitShipmentReceiptService } from './unit-shipment-receipt.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
    selector: 'jhi-unit-shipment-receipt-as-list',
    templateUrl: './unit-shipment-receipt-as-list.component.html'
})
export class UnitShipmentReceiptAsListComponent implements OnInit, OnDestroy, OnChanges {
    @Input() idShipmentPackage: any;
    @Input() idShipmentItem: any;
    @Input() idOrderItem: any;
    @Input() usedFor: any;
    @Input() qtyAccept: any;

    currentAccount: any;
    unitShipmentReceipts: UnitShipmentReceipt[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;
    isDetail = false;

    constructor(
        protected unitShipmentReceiptService: UnitShipmentReceiptService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.first = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.predicate = 'idReceipt';
        this.reverse = 'asc';
    }

    loadAll() {
        if (this.qtyAccept === 2) {
            this.unitShipmentReceiptService.queryFilterBy({
                idShipmentPackage: this.idShipmentPackage,
                idShipmentItem: this.idShipmentItem,
                idOrderItem: this.idOrderItem,
                qtyAccept: 2,
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
        if (this.qtyAccept === 1) {
            this.unitShipmentReceiptService.queryFilterBy({
                idShipmentPackage: this.idShipmentPackage,
                idShipmentItem: this.idShipmentItem,
                idOrderItem: this.idOrderItem,
                qtyAccept: 1,
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadAll();
        }
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/unit-shipment-receipt', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
            this.loadAll();
        });
        this.registerChangeInUnitShipmentReceipts();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['idShipmentPackage']) {
            this.loadAll();
        } else
        if (changes['idShipmentItem']) {
            this.loadAll();
        } else
        if (changes['idOrderItem']) {
            this.loadAll();
        } else
        if (changes['usedFor']) {
            if (this.usedFor === 'detail') {
                this.isDetail = true;
            }
        } else
        if (changes['qtyAccept'] && changes['idShipmentPackage']) {
            this.loadAll();
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UnitShipmentReceipt) {
        return item.idReceipt;
    }

    registerChangeInUnitShipmentReceipts() {
        this.eventSubscriber = this.eventManager.subscribe('unitShipmentReceiptListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idReceipt') {
            result.push('idReceipt');
        }
        return result;
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitShipmentReceipts = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(item) {
        this.unitShipmentReceiptService.executeProcess(item).subscribe(
           (value) => console.log('this: ', value),
           (err) => console.log(err),
           () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
        this.previousPage = this.page;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.unitShipmentReceiptService.update(event.data)
                .subscribe((res: UnitShipmentReceipt) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.unitShipmentReceiptService.create(event.data)
                .subscribe((res: UnitShipmentReceipt) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: UnitShipmentReceipt) {
        this.toasterService.showToaster('info', 'UnitShipmentReceipt Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.unitShipmentReceiptService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'unitShipmentReceiptListModification',
                        content: 'Deleted an unitShipmentReceipt'
                    });
                });
            }
        });
    }
}
