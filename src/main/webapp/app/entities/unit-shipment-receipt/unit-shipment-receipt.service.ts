import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UnitShipmentReceipt } from './unit-shipment-receipt.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnitShipmentReceiptService {
    protected itemValues: UnitShipmentReceipt[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/unit-shipment-receipts';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/unit-shipment-receipts';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(unitShipmentReceipt: UnitShipmentReceipt): Observable<UnitShipmentReceipt> {
        const copy = this.convert(unitShipmentReceipt);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(unitShipmentReceipt: UnitShipmentReceipt): Observable<UnitShipmentReceipt> {
        const copy = this.convert(unitShipmentReceipt);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<UnitShipmentReceipt> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(unitShipmentReceipt: UnitShipmentReceipt, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(unitShipmentReceipt);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<UnitShipmentReceipt> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<UnitShipmentReceipt[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UnitShipmentReceipt.
     */
    protected convertItemFromServer(json: any): UnitShipmentReceipt {
        const entity: UnitShipmentReceipt = Object.assign(new UnitShipmentReceipt(), json);
        if (entity.dateReceipt) {
            entity.dateReceipt = new Date(entity.dateReceipt);
        }
        return entity;
    }

    /**
     * Convert a UnitShipmentReceipt to a JSON which can be sent to the server.
     */
    protected convert(unitShipmentReceipt: UnitShipmentReceipt): UnitShipmentReceipt {
        if (unitShipmentReceipt === null || unitShipmentReceipt === {}) {
            return {};
        }
        // const copy: UnitShipmentReceipt = Object.assign({}, unitShipmentReceipt);
        const copy: UnitShipmentReceipt = JSON.parse(JSON.stringify(unitShipmentReceipt));

        // copy.dateReceipt = this.dateUtils.toDate(unitShipmentReceipt.dateReceipt);
        return copy;
    }

    protected convertList(unitShipmentReceipts: UnitShipmentReceipt[]): UnitShipmentReceipt[] {
        const copy: UnitShipmentReceipt[] = unitShipmentReceipts;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: UnitShipmentReceipt[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
