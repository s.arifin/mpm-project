import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { UnitShipmentReceipt } from './unit-shipment-receipt.model';
import { UnitShipmentReceiptService } from './unit-shipment-receipt.service';

@Injectable()
export class UnitShipmentReceiptPopupService {
    protected ngbModalRef: NgbModalRef;
    idShipmentPackage: any;
    idShipmentItem: any;
    idOrderItem: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected unitShipmentReceiptService: UnitShipmentReceiptService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unitShipmentReceiptService.find(id).subscribe((data) => {
                    // if (data.dateReceipt) {
                    //    data.dateReceipt = this.datePipe
                    //        .transform(data.dateReceipt, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.unitShipmentReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new UnitShipmentReceipt();
                    data.shipmentPackageId = this.idShipmentPackage;
                    data.shipmentItemId = this.idShipmentItem;
                    data.orderItemId = this.idOrderItem;
                    this.ngbModalRef = this.unitShipmentReceiptModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unitShipmentReceiptModalRef(component: Component, unitShipmentReceipt: UnitShipmentReceipt): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unitShipmentReceipt = unitShipmentReceipt;
        modalRef.componentInstance.idShipmentPackage = this.idShipmentPackage;
        modalRef.componentInstance.idShipmentItem = this.idShipmentItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.unitShipmentReceiptLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    unitShipmentReceiptLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
