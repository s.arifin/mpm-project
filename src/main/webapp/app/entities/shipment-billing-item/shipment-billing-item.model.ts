import { BaseEntity } from './../../shared';

export class ShipmentBillingItem implements BaseEntity {
    constructor(
        public id?: any,
        public idShipmentBillingItem?: any,
        public qty?: number,
        public shipmentItemId?: any,
        public billingItemId?: any,
    ) {
    }
}
