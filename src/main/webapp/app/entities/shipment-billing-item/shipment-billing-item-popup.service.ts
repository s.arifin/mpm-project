import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ShipmentBillingItem } from './shipment-billing-item.model';
import { ShipmentBillingItemService } from './shipment-billing-item.service';

@Injectable()
export class ShipmentBillingItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idShipmentItem: any;
    idBillingItem: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected shipmentBillingItemService: ShipmentBillingItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.shipmentBillingItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.shipmentBillingItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new ShipmentBillingItem();
                    data.shipmentItemId = this.idShipmentItem;
                    data.billingItemId = this.idBillingItem;
                    this.ngbModalRef = this.shipmentBillingItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    shipmentBillingItemModalRef(component: Component, shipmentBillingItem: ShipmentBillingItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.shipmentBillingItem = shipmentBillingItem;
        modalRef.componentInstance.idShipmentItem = this.idShipmentItem;
        modalRef.componentInstance.idBillingItem = this.idBillingItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.shipmentBillingItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    shipmentBillingItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
