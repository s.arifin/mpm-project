import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ShipmentBillingItemComponent } from './shipment-billing-item.component';
import { ShipmentBillingItemPopupComponent } from './shipment-billing-item-dialog.component';

@Injectable()
export class ShipmentBillingItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipmentBillingItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const shipmentBillingItemRoute: Routes = [
    {
        path: 'shipment-billing-item',
        component: ShipmentBillingItemComponent,
        resolve: {
            'pagingParams': ShipmentBillingItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shipmentBillingItemPopupRoute: Routes = [
    {
        path: 'shipment-billing-item-popup-new-list/:parent',
        component: ShipmentBillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-billing-item-new',
        component: ShipmentBillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shipment-billing-item/:id/edit',
        component: ShipmentBillingItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.shipmentBillingItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
