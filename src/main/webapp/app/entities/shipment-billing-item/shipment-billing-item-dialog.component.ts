import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShipmentBillingItem } from './shipment-billing-item.model';
import { ShipmentBillingItemPopupService } from './shipment-billing-item-popup.service';
import { ShipmentBillingItemService } from './shipment-billing-item.service';
import { ToasterService } from '../../shared';
import { ShipmentItem, ShipmentItemService } from '../shipment-item';
import { BillingItem, BillingItemService } from '../billing-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shipment-billing-item-dialog',
    templateUrl: './shipment-billing-item-dialog.component.html'
})
export class ShipmentBillingItemDialogComponent implements OnInit {

    shipmentBillingItem: ShipmentBillingItem;
    isSaving: boolean;
    idShipmentItem: any;
    idBillingItem: any;

    shipmentitems: ShipmentItem[];

    billingitems: BillingItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected shipmentBillingItemService: ShipmentBillingItemService,
        protected shipmentItemService: ShipmentItemService,
        protected billingItemService: BillingItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentItemService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmentitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billingItemService.query()
            .subscribe((res: ResponseWrapper) => { this.billingitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.shipmentBillingItem.idShipmentBillingItem !== undefined) {
            this.subscribeToSaveResponse(
                this.shipmentBillingItemService.update(this.shipmentBillingItem));
        } else {
            this.subscribeToSaveResponse(
                this.shipmentBillingItemService.create(this.shipmentBillingItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ShipmentBillingItem>) {
        result.subscribe((res: ShipmentBillingItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: ShipmentBillingItem) {
        this.eventManager.broadcast({ name: 'shipmentBillingItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'shipmentBillingItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'shipmentBillingItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentItemById(index: number, item: ShipmentItem) {
        return item.idShipmentItem;
    }

    trackBillingItemById(index: number, item: BillingItem) {
        return item.idBillingItem;
    }
}

@Component({
    selector: 'jhi-shipment-billing-item-popup',
    template: ''
})
export class ShipmentBillingItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected shipmentBillingItemPopupService: ShipmentBillingItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.shipmentBillingItemPopupService
                    .open(ShipmentBillingItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.shipmentBillingItemPopupService.parent = params['parent'];
                this.shipmentBillingItemPopupService
                    .open(ShipmentBillingItemDialogComponent as Component);
            } else {
                this.shipmentBillingItemPopupService
                    .open(ShipmentBillingItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
