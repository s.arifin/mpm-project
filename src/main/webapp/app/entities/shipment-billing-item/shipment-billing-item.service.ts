import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ShipmentBillingItem } from './shipment-billing-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ShipmentBillingItemService {
    protected itemValues: ShipmentBillingItem[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/shipment-billing-items';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/shipment-billing-items';

    constructor(protected http: Http) { }

    create(shipmentBillingItem: ShipmentBillingItem): Observable<ShipmentBillingItem> {
        const copy = this.convert(shipmentBillingItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(shipmentBillingItem: ShipmentBillingItem): Observable<ShipmentBillingItem> {
        const copy = this.convert(shipmentBillingItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<ShipmentBillingItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(params?: any, req?: any): Observable<ShipmentBillingItem> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(params?: any, req?: any): Observable<ShipmentBillingItem[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, req).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ShipmentBillingItem.
     */
    protected convertItemFromServer(json: any): ShipmentBillingItem {
        const entity: ShipmentBillingItem = Object.assign(new ShipmentBillingItem(), json);
        return entity;
    }

    /**
     * Convert a ShipmentBillingItem to a JSON which can be sent to the server.
     */
    protected convert(shipmentBillingItem: ShipmentBillingItem): ShipmentBillingItem {
        if (shipmentBillingItem === null || shipmentBillingItem === {}) {
            return {};
        }
        // const copy: ShipmentBillingItem = Object.assign({}, shipmentBillingItem);
        const copy: ShipmentBillingItem = JSON.parse(JSON.stringify(shipmentBillingItem));
        return copy;
    }

    protected convertList(shipmentBillingItems: ShipmentBillingItem[]): ShipmentBillingItem[] {
        const copy: ShipmentBillingItem[] = shipmentBillingItems;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ShipmentBillingItem[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
