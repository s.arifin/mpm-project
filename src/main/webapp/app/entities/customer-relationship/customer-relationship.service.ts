import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CustomerRelationship } from './customer-relationship.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CustomerRelationshipService {
   protected itemValues: CustomerRelationship[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/customer-relationships';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/customer-relationships';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(customerRelationship: CustomerRelationship): Observable<CustomerRelationship> {
       const copy = this.convert(customerRelationship);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(customerRelationship: CustomerRelationship): Observable<CustomerRelationship> {
       const copy = this.convert(customerRelationship);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<CustomerRelationship> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: CustomerRelationship, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: CustomerRelationship[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to CustomerRelationship.
    */
   protected convertItemFromServer(json: any): CustomerRelationship {
       const entity: CustomerRelationship = Object.assign(new CustomerRelationship(), json);
       if (entity.dateFrom) {
           entity.dateFrom = new Date(entity.dateFrom);
       }
       if (entity.dateThru) {
           entity.dateThru = new Date(entity.dateThru);
       }
       return entity;
   }

   /**
    * Convert a CustomerRelationship to a JSON which can be sent to the server.
    */
   protected convert(customerRelationship: CustomerRelationship): CustomerRelationship {
       if (customerRelationship === null || customerRelationship === {}) {
           return {};
       }
       // const copy: CustomerRelationship = Object.assign({}, customerRelationship);
       const copy: CustomerRelationship = JSON.parse(JSON.stringify(customerRelationship));

       // copy.dateFrom = this.dateUtils.toDate(customerRelationship.dateFrom);

       // copy.dateThru = this.dateUtils.toDate(customerRelationship.dateThru);
       return copy;
   }

   protected convertList(customerRelationships: CustomerRelationship[]): CustomerRelationship[] {
       const copy: CustomerRelationship[] = customerRelationships;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: CustomerRelationship[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
