import { BaseEntity } from './../../shared';

export class CustomerRelationship implements BaseEntity {
    constructor(
        public id?: any,
        public idPartyRelationship?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public statusTypeId?: any,
        public relationTypeId?: any,
        public internalId?: any,
        public customerId?: any,
    ) {
    }
}
