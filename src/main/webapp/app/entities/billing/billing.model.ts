import { BaseEntity } from './../../shared';

export class Billing implements BaseEntity {
    constructor(
        public id?: any,
        public idBilling?: any,
        public billingNumber?: string,
        public dateCreate?: Date,
        public dateDue?: Date,
        public details?: any,
        public billingTypeId?: any,
        public payments?: any,
        public internalId?: string,
        public internalName?: string,
        public billToId?: string,
        public billFromId?: string,
        public currentStatus?: number,
        public printCounter?: number,
    ) {
        this.dateCreate = new Date();
    }
}
