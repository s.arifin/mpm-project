import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Billing } from './billing.model';
import { BillingService } from './billing.service';
import { ToasterService} from '../../shared';
import { BillingType, BillingTypeService } from '../billing-type';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-billing-edit',
    templateUrl: './billing-edit.component.html'
})
export class BillingEditComponent implements OnInit, OnDestroy {

    protected subscription: Subscription;
    billing: Billing;
    isSaving: boolean;
    idBilling: any;
    paramPage: number;
    routeId: number;

    billingtypes: BillingType[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        protected alertService: JhiAlertService,
        protected billingService: BillingService,
        protected billingTypeService: BillingTypeService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.billing = new Billing();
        this.routeId = 0;
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.idBilling = params['id'];
                this.load();
            }
            if (params['page']) {
                this.paramPage = params['page'];
            }
            if (params['route']) {
                this.routeId = Number(params['route']);
            }
        });
        this.isSaving = false;
        this.billingTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.billingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    load() {
        this.billingService.find(this.idBilling).subscribe((billing) => {
            this.billing = billing;
        });
    }

    previousState() {
        if (this.routeId === 0) {
            this.router.navigate(['billing', { page: this.paramPage }]);
        }
    }

    save() {
        this.isSaving = true;
        if (this.billing.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.billingService.update(this.billing));
        } else {
            this.subscribeToSaveResponse(
                this.billingService.create(this.billing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Billing>) {
        result.subscribe((res: Billing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Billing) {
        this.eventManager.broadcast({ name: 'billingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billing saved !');
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'billing Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackBillingTypeById(index: number, item: BillingType) {
        return item.idBillingType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}
