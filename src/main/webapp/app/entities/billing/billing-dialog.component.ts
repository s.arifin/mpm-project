import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Billing } from './billing.model';
import { BillingPopupService } from './billing-popup.service';
import { BillingService } from './billing.service';
import { ToasterService } from '../../shared';
import { BillingType, BillingTypeService } from '../billing-type';
import { Internal, InternalService } from '../internal';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-billing-dialog',
    templateUrl: './billing-dialog.component.html'
})
export class BillingDialogComponent implements OnInit {

    billing: Billing;
    isSaving: boolean;
    idBillingType: any;
    idInternal: any;
    idBillTo: any;
    idBillFrom: any;

    billingtypes: BillingType[];

    internals: Internal[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected billingService: BillingService,
        protected billingTypeService: BillingTypeService,
        protected internalService: InternalService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.billingTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.billingtypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.billing.idBilling !== undefined) {
            this.subscribeToSaveResponse(
                this.billingService.update(this.billing));
        } else {
            this.subscribeToSaveResponse(
                this.billingService.create(this.billing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Billing>) {
        result.subscribe((res: Billing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Billing) {
        this.eventManager.broadcast({ name: 'billingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'billing saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'billing Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBillingTypeById(index: number, item: BillingType) {
        return item.idBillingType;
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-billing-popup',
    template: ''
})
export class BillingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected billingPopupService: BillingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.billingPopupService
                    .open(BillingDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.billingPopupService.parent = params['parent'];
                this.billingPopupService
                    .open(BillingDialogComponent as Component);
            } else {
                this.billingPopupService
                    .open(BillingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
