import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BillingComponent } from './billing.component';
import { BillingEditComponent } from './billing-edit.component';
import { BillingPopupComponent } from './billing-dialog.component';

@Injectable()
export class BillingResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idBilling,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const billingRoute: Routes = [
    {
        path: 'billing',
        component: BillingComponent,
        resolve: {
            'pagingParams': BillingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const billingPopupRoute: Routes = [
    {
        path: 'billing-popup-new',
        component: BillingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'billing-new',
        component: BillingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'billing/:id/edit',
        component: BillingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'billing/:route/:page/:id/edit',
        component: BillingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'billing/:id/popup-edit',
        component: BillingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.billing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
