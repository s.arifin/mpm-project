import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PersonalCustomerComponent } from './personal-customer.component';
import { PersonalCostumerUploadPopupComponent } from './personal-costumer-upload-dialog.component';
import { PersonalCustomerPopupComponent } from './personal-customer-dialog.component';

@Injectable()
export class PersonalCustomerResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idCustomer,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const personalCustomerRoute: Routes = [
    {
        path: 'personal-customer',
        component: PersonalCustomerComponent,
        resolve: {
            'pagingParams': PersonalCustomerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.personalCustomer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const personalCustomerPopupRoute: Routes = [
    {
        path: 'personal-customer-new',
        component: PersonalCustomerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.personalCustomer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'personal-customer/:id/edit',
        component: PersonalCustomerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.personalCustomer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'personal-costumer-popup-new-upload',
        component: PersonalCostumerUploadPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.personalCostumer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
