import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { PersonalCustomer } from './personal-customer.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { Subject } from 'rxjs/Subject';

import * as moment from 'moment';
import { SERVER_API_URL } from '../../app.constants';

@Injectable()
export class PersonalCustomerService {

    protected resourceUrl = 'api/personal-customers';
    protected resourceSearchUrl = 'api/_search/personal-customers';

    newCustomer: Subject<any> = new Subject();

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(personalCustomer: PersonalCustomer): Observable<PersonalCustomer> {
        const copy = this.convert(personalCustomer);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.newCustomerRegister(this.convert(jsonResponse));
            return jsonResponse;
        });
    }

    update(personalCustomer: PersonalCustomer): Observable<PersonalCustomer> {
        const copy = this.convert(personalCustomer);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PersonalCustomer> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    findEmail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(SERVER_API_URL + 'api/personal-customers/email', options)
        .map((res: Response) => this.convertResponse(res));
    }

    findbyPartyId(id: any): Observable<PersonalCustomer> {
        return this.http.get(`${this.resourceUrl}/byPartyId/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(personalCustomer: any): Observable<String> {
        const copy = this.convert(personalCustomer);
        return this.http.post(this.resourceUrl + '/execute', copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convert(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(json: any) {
    }

    protected convert(personalCustomer: PersonalCustomer): PersonalCustomer {
        const copy: PersonalCustomer = Object.assign({}, personalCustomer);
        return copy;
    }

    newCustomerRegister(customer: PersonalCustomer) {
        this.newCustomer.next(customer);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
