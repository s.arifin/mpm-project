import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PersonalCustomer} from './personal-customer.model';
import {PersonalCustomerPopupService} from './personal-customer-popup.service';
import {PersonalCustomerService} from './personal-customer.service';
import {ToasterService} from '../../shared';
import { RoleType, RoleTypeService } from '../role-type';
import { Party, PartyService } from '../party';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-personal-customer-dialog',
    templateUrl: './personal-customer-dialog.component.html'
})
export class PersonalCustomerDialogComponent implements OnInit {

    personalCustomer: PersonalCustomer;
    isSaving: boolean;

    roletypes: RoleType[];

    parties: Party[];
    // dobDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected personalCustomerService: PersonalCustomerService,
        protected roleTypeService: RoleTypeService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.roleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.roletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.personalCustomer.idCustomer !== undefined) {
            this.subscribeToSaveResponse(
                this.personalCustomerService.update(this.personalCustomer));
        } else {
            this.subscribeToSaveResponse(
                this.personalCustomerService.create(this.personalCustomer));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PersonalCustomer>) {
        result.subscribe((res: PersonalCustomer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PersonalCustomer) {
        this.personalCustomerService.newCustomerRegister(result);
        this.eventManager.broadcast({ name: 'personalCustomerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'personalCustomer saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'personalCustomer Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRoleTypeById(index: number, item: RoleType) {
        return item.idRoleType;
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-personal-customer-popup',
    template: ''
})
export class PersonalCustomerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected personalCustomerPopupService: PersonalCustomerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.personalCustomerPopupService
                    .open(PersonalCustomerDialogComponent as Component, params['id']);
            } else {
                this.personalCustomerPopupService
                    .open(PersonalCustomerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
