import { BaseEntity } from './../shared-component/';
import { Person } from './../person';
import { Customer } from './../customer/';
import { Organization } from '../organization';

export class PersonalCustomer extends Customer {
    constructor(
        public person?: Person
    ) {
        super();
        this.person = new Person();
    }
}
