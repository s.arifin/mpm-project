import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { GeneralUpload } from './../general-upload/general-upload.model';
import { GeneralUploadPopupService } from './../general-upload/general-upload-popup.service';
import { GeneralUploadService } from './../general-upload/general-upload.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { PurposeType, PurposeTypeService } from '../purpose-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-personal-costumer-upload-dialog',
    templateUrl: './personal-costumer-upload-dialog.component.html'
})
export class PersonalCostumerUploadDialogComponent implements OnInit {

    generalUpload: GeneralUpload;
    isSaving: boolean;
    idInternal: any;
    idPurpose: any;

    internals: Internal[];

    purposetypes: PurposeType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected generalUploadService: GeneralUploadService,
        protected internalService: InternalService,
        protected purposeTypeService: PurposeTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query({ size: 1000 })
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.purposeTypeService.query({size: 99999})
            .subscribe((res: ResponseWrapper) => { this.purposetypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        // this.isSaving = true;
        // this.subscribeToSaveResponse(
        //     this.generalUploadService.create(this.generalUpload)
        // );
        this.generalUploadService.createUploadCustomer(this.generalUpload).subscribe((res) => {});
        this.onSaveSuccess(this.generalUpload);
    }

    protected subscribeToSaveResponse(result: Observable<GeneralUpload>) {
        result.subscribe((res: GeneralUpload) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: GeneralUpload) {
        this.eventManager.broadcast({ name: 'generalUploadListModification', content: 'OK'});
        // this.toaster.showToaster('info', 'Save', 'generalUpload saved !');
        this.toaster.showToaster('INFO', 'Upload Excel', 'Processing ... ');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'generalUpload Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackPurposeTypeById(index: number, item: PurposeType) {
        return item.idPurposeType;
    }
}

@Component({
    selector: 'jhi-personal-costumer-upload-popup',
    template: ''
})
export class PersonalCostumerUploadPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected generalUploadPopupService: GeneralUploadPopupService
    ) {}

    ngOnInit() {
        this.generalUploadPopupService.idInternal = undefined;
        this.generalUploadPopupService.idPurpose = 2002;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.generalUploadPopupService
                    .open(PersonalCostumerUploadDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.generalUploadPopupService.parent = params['parent'];
                this.generalUploadPopupService
                    .open(PersonalCostumerUploadDialogComponent as Component);
            } else {
                this.generalUploadPopupService
                    .open(PersonalCostumerUploadDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
