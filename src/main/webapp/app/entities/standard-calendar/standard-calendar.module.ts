import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MpmSharedModule } from '../../shared';
import {
    StandardCalendarService,
    StandardCalendarPopupService,
    StandardCalendarComponent,
    StandardCalendarDialogComponent,
    StandardCalendarPopupComponent,
    standardCalendarRoute,
    standardCalendarPopupRoute,
    StandardCalendarResolvePagingParams,
    StandardCalendarAsLovComponent,
    StandardCalendarLovPopupComponent,
    StandardCalendarEditComponent,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { AutoCompleteModule,
         InputTextModule,
         InputTextareaModule,
         DataScrollerModule,
         PaginatorModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         SharedModule,
         FieldsetModule,
         ScheduleModule,
         ChartModule,
         DragDropModule,
         LightboxModule
} from 'primeng/primeng';

import { DataTableModule } from 'primeng/primeng';
import { DataListModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { DataGridModule } from 'primeng/primeng';
import { ButtonModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { ListboxModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...standardCalendarRoute,
    ...standardCalendarPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        InputTextareaModule,
        ButtonModule,
        DataListModule,
        DataGridModule,
        PaginatorModule,
        ConfirmDialogModule,
        ScheduleModule,
        AutoCompleteModule,
        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        ChartModule,
        DragDropModule,
        LightboxModule,
        CurrencyMaskModule,
        DataTableModule,
        SliderModule,
        RadioButtonModule
    ],
    exports: [
        StandardCalendarComponent,
        StandardCalendarAsLovComponent,
        StandardCalendarLovPopupComponent,
        StandardCalendarEditComponent
    ],
    declarations: [
        StandardCalendarComponent,
        StandardCalendarDialogComponent,
        StandardCalendarPopupComponent,
        StandardCalendarAsLovComponent,
        StandardCalendarLovPopupComponent,
        StandardCalendarEditComponent
    ],
    entryComponents: [
        StandardCalendarComponent,
        StandardCalendarDialogComponent,
        StandardCalendarPopupComponent,
        StandardCalendarAsLovComponent,
        StandardCalendarLovPopupComponent,
        StandardCalendarEditComponent
    ],
    providers: [
        StandardCalendarService,
        StandardCalendarPopupService,
        StandardCalendarResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmStandardCalendarModule {}
