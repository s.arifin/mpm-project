import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StandardCalendar } from './standard-calendar.model';
import { StandardCalendarPopupService } from './standard-calendar-popup.service';
import { StandardCalendarService } from './standard-calendar.service';
import { ToasterService } from '../../shared';
import { Internal, InternalService } from '../internal';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-standard-calendar-dialog',
    templateUrl: './standard-calendar-dialog.component.html'
})
export class StandardCalendarDialogComponent implements OnInit {

    standardCalendar: StandardCalendar;
    isSaving: boolean;
    idInternal: any;

    internals: Internal[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected standardCalendarService: StandardCalendarService,
        protected internalService: InternalService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.standardCalendar.idCalendar !== undefined) {
            this.subscribeToSaveResponse(
                this.standardCalendarService.update(this.standardCalendar));
        } else {
            this.subscribeToSaveResponse(
                this.standardCalendarService.create(this.standardCalendar));
        }
    }

    protected subscribeToSaveResponse(result: Observable<StandardCalendar>) {
        result.subscribe((res: StandardCalendar) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: StandardCalendar) {
        this.eventManager.broadcast({ name: 'standardCalendarListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'standardCalendar saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'standardCalendar Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }
}

@Component({
    selector: 'jhi-standard-calendar-popup',
    template: ''
})
export class StandardCalendarPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected standardCalendarPopupService: StandardCalendarPopupService
    ) {}

    ngOnInit() {
        this.standardCalendarPopupService.idInternal = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.standardCalendarPopupService
                    .open(StandardCalendarDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.standardCalendarPopupService.parent = params['parent'];
                this.standardCalendarPopupService
                    .open(StandardCalendarDialogComponent as Component);
            } else {
                this.standardCalendarPopupService
                    .open(StandardCalendarDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
