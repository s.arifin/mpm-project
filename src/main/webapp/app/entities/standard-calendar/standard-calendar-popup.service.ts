import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { StandardCalendar } from './standard-calendar.model';
import { StandardCalendarService } from './standard-calendar.service';

@Injectable()
export class StandardCalendarPopupService {
    protected ngbModalRef: NgbModalRef;
    idInternal: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected standardCalendarService: StandardCalendarService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.standardCalendarService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.standardCalendarModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new StandardCalendar();
                    data.internalId = this.idInternal;
                    this.ngbModalRef = this.standardCalendarModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    standardCalendarModalRef(component: Component, standardCalendar: StandardCalendar): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.standardCalendar = standardCalendar;
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.standardCalendarLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    standardCalendarLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idInternal = this.idInternal;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
