import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { OrderRequestItem } from './order-request-item.model';
import { OrderRequestItemService } from './order-request-item.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';

@Component({
   selector: 'jhi-order-request-item',
   templateUrl: './order-request-item.component.html'
})
export class OrderRequestItemComponent implements OnInit, OnDestroy {

    currentAccount: any;
    orderRequestItems: OrderRequestItem[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    first: number;

    constructor(
        protected orderRequestItemService: OrderRequestItemService,
        protected confirmationService: ConfirmationService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
        const lastPage: number = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['page'] ?
                    this.activatedRoute.snapshot.params['page'] : 0;
        this.page = lastPage > 0 ? lastPage : 1;
        this.first = (this.page - 1) * this.itemsPerPage;
    }

    loadAll() {
        if (this.currentSearch) {
            this.orderRequestItemService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                    (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                    (res: ResponseWrapper) => this.onError(res.json)
                );
            return;
        }
        this.orderRequestItemService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/order-request-item'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/order-request-item', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/order-request-item', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

   ngOnInit() {
       this.loadAll();
       this.principal.identity(true).then((account) => {
           this.currentAccount = account;
       });
       this.registerChangeInOrderRequestItems();
   }

   ngOnDestroy() {
       this.eventManager.destroy(this.eventSubscriber);
   }

   trackId(index: number, item: OrderRequestItem) {
       return item.idOrderRequestItem;
   }

   registerChangeInOrderRequestItems() {
       this.eventSubscriber = this.eventManager.subscribe('orderRequestItemListModification', (response) => this.loadAll());
   }

   sort() {
       const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
       if (this.predicate !== 'idOrderRequestItem') {
           result.push('idOrderRequestItem');
       }
       return result;
   }

   protected onSuccess(data, headers) {
       this.links = this.parseLinks.parse(headers.get('link'));
       this.totalItems = headers.get('X-Total-Count');
       this.queryCount = this.totalItems;
       // this.page = pagingParams.page;
       this.orderRequestItems = data;
   }

   protected onError(error) {
       this.jhiAlertService.error(error.message, null, null);
   }

   executeProcess(item) {
       this.orderRequestItemService.executeProcess(item).subscribe(
          (value) => console.log('this: ', value),
          (err) => console.log(err),
          () => this.toaster.showToaster('info', 'Data Proces', 'Done process in system..')
       );
   }

   loadDataLazy(event: LazyLoadEvent) {
       this.itemsPerPage = event.rows;
       this.page = Math.ceil(event.first / this.itemsPerPage) + 1;
       this.previousPage = this.page;

       if (event.sortField !== undefined) {
           this.predicate = event.sortField;
           this.reverse = event.sortOrder;
       }
       this.loadAll();
   }

   updateRowData(event) {
       if (event.data.id !== undefined) {
           this.orderRequestItemService.update(event.data)
               .subscribe((res: OrderRequestItem) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       } else {
           this.orderRequestItemService.create(event.data)
               .subscribe((res: OrderRequestItem) =>
                   this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
       }
   }

   protected onRowDataSaveSuccess(result: OrderRequestItem) {
       this.toaster.showToaster('info', 'OrderRequestItem Saved', 'Data saved..');
   }

   protected onRowDataSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.onError(error);
   }

   delete(id: any) {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to delete?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.orderRequestItemService.delete(id).subscribe((response) => {
                   this.eventManager.broadcast({
                       name: 'orderRequestItemListModification',
                       content: 'Deleted an orderRequestItem'
                   });
               });
           }
       });
   }

   process() {
       this.orderRequestItemService.process({}).subscribe((r) => {
           console.log('result', r);
           this.toaster.showToaster('info', 'Data Processed', 'processed.....');
       });
   }

}
