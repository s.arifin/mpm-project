import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OrderRequestItem } from './order-request-item.model';
import { OrderRequestItemService } from './order-request-item.service';

@Injectable()
export class OrderRequestItemPopupService {
    protected ngbModalRef: NgbModalRef;
    idOrderItem: any;
    idRequestItem: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected orderRequestItemService: OrderRequestItemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.orderRequestItemService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.orderRequestItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new OrderRequestItem();
                    data.orderItemId = this.idOrderItem;
                    data.requestItemId = this.idRequestItem;
                    this.ngbModalRef = this.orderRequestItemModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    orderRequestItemModalRef(component: Component, orderRequestItem: OrderRequestItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderRequestItem = orderRequestItem;
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.componentInstance.idRequestItem = this.idRequestItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.orderRequestItemLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    orderRequestItemLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idOrderItem = this.idOrderItem;
        modalRef.componentInstance.idRequestItem = this.idRequestItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
