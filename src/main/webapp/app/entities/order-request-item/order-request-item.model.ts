import { BaseEntity } from './../../shared';

export class OrderRequestItem implements BaseEntity {
    constructor(
        public id?: any,
        public idOrderRequestItem?: any,
        public qty?: number,
        public qtyDelivered?: number,
        public orderItemId?: any,
        public requestItemId?: any,
    ) {
    }
}
