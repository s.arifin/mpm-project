import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OrderRequestItem } from './order-request-item.model';
import { OrderRequestItemPopupService } from './order-request-item-popup.service';
import { OrderRequestItemService } from './order-request-item.service';
import { ToasterService } from '../../shared';
import { OrderItem, OrderItemService } from '../order-item';
import { RequestItem, RequestItemService } from '../request-item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-order-request-item-dialog',
    templateUrl: './order-request-item-dialog.component.html'
})
export class OrderRequestItemDialogComponent implements OnInit {

    orderRequestItem: OrderRequestItem;
    isSaving: boolean;
    idOrderItem: any;
    idRequestItem: any;

    orderitems: OrderItem[];

    requestitems: RequestItem[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected orderRequestItemService: OrderRequestItemService,
        protected orderItemService: OrderItemService,
        protected requestItemService: RequestItemService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.orderItemService.query()
            .subscribe((res: ResponseWrapper) => { this.orderitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.requestItemService.query()
            .subscribe((res: ResponseWrapper) => { this.requestitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orderRequestItem.idOrderRequestItem !== undefined) {
            this.subscribeToSaveResponse(
                this.orderRequestItemService.update(this.orderRequestItem));
        } else {
            this.subscribeToSaveResponse(
                this.orderRequestItemService.create(this.orderRequestItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<OrderRequestItem>) {
        result.subscribe((res: OrderRequestItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: OrderRequestItem) {
        this.eventManager.broadcast({ name: 'orderRequestItemListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'orderRequestItem saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'orderRequestItem Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrderItemById(index: number, item: OrderItem) {
        return item.idOrderItem;
    }

    trackRequestItemById(index: number, item: RequestItem) {
        return item.idRequestItem;
    }
}

@Component({
    selector: 'jhi-order-request-item-popup',
    template: ''
})
export class OrderRequestItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected orderRequestItemPopupService: OrderRequestItemPopupService
    ) {}

    ngOnInit() {
        this.orderRequestItemPopupService.idOrderItem = undefined;
        this.orderRequestItemPopupService.idRequestItem = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderRequestItemPopupService
                    .open(OrderRequestItemDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.orderRequestItemPopupService.parent = params['parent'];
                this.orderRequestItemPopupService
                    .open(OrderRequestItemDialogComponent as Component);
            } else {
                this.orderRequestItemPopupService
                    .open(OrderRequestItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
