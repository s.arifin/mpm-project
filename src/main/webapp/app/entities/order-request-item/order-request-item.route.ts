import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OrderRequestItemComponent } from './order-request-item.component';
import { OrderRequestItemLovPopupComponent } from './order-request-item-as-lov.component';
import { OrderRequestItemPopupComponent } from './order-request-item-dialog.component';

@Injectable()
export class OrderRequestItemResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrderRequestItem,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const orderRequestItemRoute: Routes = [
    {
        path: 'order-request-item',
        component: OrderRequestItemComponent,
        resolve: {
            'pagingParams': OrderRequestItemResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderRequestItemPopupRoute: Routes = [
    {
        path: 'order-request-item-lov',
        component: OrderRequestItemLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-request-item-new',
        component: OrderRequestItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-request-item/:id/edit',
        component: OrderRequestItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.orderRequestItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
