import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OrderRequestItem } from './order-request-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OrderRequestItemService {
   protected itemValues: OrderRequestItem[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/order-request-items';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/order-request-items';

   constructor(protected http: Http) { }

   create(orderRequestItem: OrderRequestItem): Observable<OrderRequestItem> {
       const copy = this.convert(orderRequestItem);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(orderRequestItem: OrderRequestItem): Observable<OrderRequestItem> {
       const copy = this.convert(orderRequestItem);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<OrderRequestItem> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: OrderRequestItem, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: OrderRequestItem[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to OrderRequestItem.
    */
   protected convertItemFromServer(json: any): OrderRequestItem {
       const entity: OrderRequestItem = Object.assign(new OrderRequestItem(), json);
       return entity;
   }

   /**
    * Convert a OrderRequestItem to a JSON which can be sent to the server.
    */
   protected convert(orderRequestItem: OrderRequestItem): OrderRequestItem {
       if (orderRequestItem === null || orderRequestItem === {}) {
           return {};
       }
       // const copy: OrderRequestItem = Object.assign({}, orderRequestItem);
       const copy: OrderRequestItem = JSON.parse(JSON.stringify(orderRequestItem));
       return copy;
   }

   protected convertList(orderRequestItems: OrderRequestItem[]): OrderRequestItem[] {
       const copy: OrderRequestItem[] = orderRequestItems;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: OrderRequestItem[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
