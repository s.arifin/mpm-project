import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared';
import { ShipmentReceiptAX } from './shipment-receipt-c.model';

@Injectable()
export class ShipmentReceiptCService {

    values: Subject<any> = new Subject<any>();
    protected resourceUrl =  process.env.API_C_URL  + '/api/shipment_receipt';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    findByInvoiceId(id: any, idPackage: any): Observable<ShipmentReceiptAX[]> {
        return this.http.get(this.resourceUrl + '/getByInvoiceId/?id=' + id + '&idpackage=' + idPackage).map((res: Response) => {
            const jsonResponse = res.json();
            return jsonResponse;
        });
    }
}
