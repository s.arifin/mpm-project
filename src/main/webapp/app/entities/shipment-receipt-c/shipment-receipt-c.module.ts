import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ShipmentReceiptCService } from './';

@NgModule({
    providers: [
        ShipmentReceiptCService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmShipmentReceiptCModule {}
