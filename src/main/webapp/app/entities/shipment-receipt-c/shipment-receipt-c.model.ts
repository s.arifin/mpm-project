import { BaseEntity } from './../../shared';

export class ShipmentReceiptAX implements BaseEntity {
    constructor(
        public id?: any,
        public vendorInvoice?: string,
        public idProduct?: string,
        public totalRcvPrice?: number,
        public segmentType?: string,
        public categoryType?: string,
        public idInternal?: string,
        public orderNumber?: string,
        public uuid?: string
    ) {
    }
}
