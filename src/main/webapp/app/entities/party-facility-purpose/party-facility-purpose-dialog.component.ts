import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PartyFacilityPurpose } from './party-facility-purpose.model';
import { PartyFacilityPurposePopupService } from './party-facility-purpose-popup.service';
import { PartyFacilityPurposeService } from './party-facility-purpose.service';
import { ToasterService } from '../../shared';
import { Organization, OrganizationService } from '../organization';
import { Facility, FacilityService } from '../facility';
import { PurposeType, PurposeTypeService } from '../purpose-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-party-facility-purpose-dialog',
    templateUrl: './party-facility-purpose-dialog.component.html'
})
export class PartyFacilityPurposeDialogComponent implements OnInit {

    partyFacilityPurpose: PartyFacilityPurpose;
    isSaving: boolean;
    idOrganization: any;
    idFacility: any;
    idPurposeType: any;

    organizations: Organization[];

    facilities: Facility[];

    purposetypes: PurposeType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected partyFacilityPurposeService: PartyFacilityPurposeService,
        protected organizationService: OrganizationService,
        protected facilityService: FacilityService,
        protected purposeTypeService: PurposeTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.organizationService.query()
            .subscribe((res: ResponseWrapper) => { this.organizations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.facilityService.query()
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.purposeTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.purposetypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.partyFacilityPurpose.idPartyFacility !== undefined) {
            this.subscribeToSaveResponse(
                this.partyFacilityPurposeService.update(this.partyFacilityPurpose));
        } else {
            this.subscribeToSaveResponse(
                this.partyFacilityPurposeService.create(this.partyFacilityPurpose));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PartyFacilityPurpose>) {
        result.subscribe((res: PartyFacilityPurpose) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PartyFacilityPurpose) {
        this.eventManager.broadcast({ name: 'partyFacilityPurposeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'partyFacilityPurpose saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'partyFacilityPurpose Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOrganizationById(index: number, item: Organization) {
        return item.idParty;
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackPurposeTypeById(index: number, item: PurposeType) {
        return item.idPurposeType;
    }
}

@Component({
    selector: 'jhi-party-facility-purpose-popup',
    template: ''
})
export class PartyFacilityPurposePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected partyFacilityPurposePopupService: PartyFacilityPurposePopupService
    ) {}

    ngOnInit() {
        this.partyFacilityPurposePopupService.idOrganization = undefined;
        this.partyFacilityPurposePopupService.idFacility = undefined;
        this.partyFacilityPurposePopupService.idPurposeType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partyFacilityPurposePopupService
                    .open(PartyFacilityPurposeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.partyFacilityPurposePopupService.parent = params['parent'];
                this.partyFacilityPurposePopupService
                    .open(PartyFacilityPurposeDialogComponent as Component);
            } else {
                this.partyFacilityPurposePopupService
                    .open(PartyFacilityPurposeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
