import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PartyFacilityPurpose } from './party-facility-purpose.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PartyFacilityPurposeService {
   protected itemValues: PartyFacilityPurpose[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/party-facility-purposes';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/party-facility-purposes';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(partyFacilityPurpose: PartyFacilityPurpose): Observable<PartyFacilityPurpose> {
       const copy = this.convert(partyFacilityPurpose);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(partyFacilityPurpose: PartyFacilityPurpose): Observable<PartyFacilityPurpose> {
       const copy = this.convert(partyFacilityPurpose);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PartyFacilityPurpose> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PartyFacilityPurpose, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PartyFacilityPurpose[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PartyFacilityPurpose.
    */
   protected convertItemFromServer(json: any): PartyFacilityPurpose {
       const entity: PartyFacilityPurpose = Object.assign(new PartyFacilityPurpose(), json);
       if (entity.dateFrom) {
           entity.dateFrom = new Date(entity.dateFrom);
       }
       if (entity.dateThru) {
           entity.dateThru = new Date(entity.dateThru);
       }
       return entity;
   }

   /**
    * Convert a PartyFacilityPurpose to a JSON which can be sent to the server.
    */
   protected convert(partyFacilityPurpose: PartyFacilityPurpose): PartyFacilityPurpose {
       if (partyFacilityPurpose === null || partyFacilityPurpose === {}) {
           return {};
       }
       // const copy: PartyFacilityPurpose = Object.assign({}, partyFacilityPurpose);
       const copy: PartyFacilityPurpose = JSON.parse(JSON.stringify(partyFacilityPurpose));

       // copy.dateFrom = this.dateUtils.toDate(partyFacilityPurpose.dateFrom);

       // copy.dateThru = this.dateUtils.toDate(partyFacilityPurpose.dateThru);
       return copy;
   }

   protected convertList(partyFacilityPurposes: PartyFacilityPurpose[]): PartyFacilityPurpose[] {
       const copy: PartyFacilityPurpose[] = partyFacilityPurposes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PartyFacilityPurpose[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
