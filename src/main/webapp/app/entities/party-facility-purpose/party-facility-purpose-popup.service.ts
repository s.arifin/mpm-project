import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PartyFacilityPurpose } from './party-facility-purpose.model';
import { PartyFacilityPurposeService } from './party-facility-purpose.service';

@Injectable()
export class PartyFacilityPurposePopupService {
    protected ngbModalRef: NgbModalRef;
    idOrganization: any;
    idFacility: any;
    idPurposeType: any;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected partyFacilityPurposeService: PartyFacilityPurposeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.partyFacilityPurposeService.find(id).subscribe((data) => {
                    // if (data.dateFrom) {
                    //    data.dateFrom = this.datePipe
                    //        .transform(data.dateFrom, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateThru) {
                    //    data.dateThru = this.datePipe
                    //        .transform(data.dateThru, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.partyFacilityPurposeModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new PartyFacilityPurpose();
                    data.organizationId = this.idOrganization;
                    data.facilityId = this.idFacility;
                    data.purposeTypeId = this.idPurposeType;
                    this.ngbModalRef = this.partyFacilityPurposeModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    partyFacilityPurposeModalRef(component: Component, partyFacilityPurpose: PartyFacilityPurpose): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.partyFacilityPurpose = partyFacilityPurpose;
        modalRef.componentInstance.idOrganization = this.idOrganization;
        modalRef.componentInstance.idFacility = this.idFacility;
        modalRef.componentInstance.idPurposeType = this.idPurposeType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.partyFacilityPurposeLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    partyFacilityPurposeLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idOrganization = this.idOrganization;
        modalRef.componentInstance.idFacility = this.idFacility;
        modalRef.componentInstance.idPurposeType = this.idPurposeType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
