import { BaseEntity } from './../../shared';

export class PartyFacilityPurpose implements BaseEntity {
    constructor(
        public id?: any,
        public idPartyFacility?: any,
        public dateFrom?: any,
        public dateThru?: any,
        public organizationId?: any,
        public facilityId?: any,
        public purposeTypeId?: any,
    ) {
    }
}
