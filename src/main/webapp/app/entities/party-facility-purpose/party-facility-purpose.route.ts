import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PartyFacilityPurposeComponent } from './party-facility-purpose.component';
import { PartyFacilityPurposePopupComponent } from './party-facility-purpose-dialog.component';

@Injectable()
export class PartyFacilityPurposeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPartyFacility,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const partyFacilityPurposeRoute: Routes = [
    {
        path: 'party-facility-purpose',
        component: PartyFacilityPurposeComponent,
        resolve: {
            'pagingParams': PartyFacilityPurposeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyFacilityPurpose.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const partyFacilityPurposePopupRoute: Routes = [
    {
        path: 'party-facility-purpose-popup-new-list/:parent',
        component: PartyFacilityPurposePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyFacilityPurpose.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-facility-purpose-new',
        component: PartyFacilityPurposePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyFacilityPurpose.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'party-facility-purpose/:id/edit',
        component: PartyFacilityPurposePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.partyFacilityPurpose.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
