import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { SalesUnitLeasing } from './sales-unit-leasing.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SalesUnitLeasingService {
    protected itemValues: SalesUnitLeasing[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = SERVER_API_URL + 'api/sales-unit-leasings';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/sales-unit-leasings';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(salesUnitLeasing: SalesUnitLeasing): Observable<SalesUnitLeasing> {
        const copy = this.convert(salesUnitLeasing);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(salesUnitLeasing: SalesUnitLeasing): Observable<SalesUnitLeasing> {
        const copy = this.convert(salesUnitLeasing);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<SalesUnitLeasing> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, salesUnitLeasing: SalesUnitLeasing): Observable<SalesUnitLeasing> {
        const copy = this.convert(salesUnitLeasing);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, salesUnitLeasings: SalesUnitLeasing[]): Observable<SalesUnitLeasing[]> {
        const copy = this.convertList(salesUnitLeasings);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(salesUnitLeasing: SalesUnitLeasing): SalesUnitLeasing {
        if (salesUnitLeasing === null || salesUnitLeasing === {}) {
            return {};
        }
        // const copy: SalesUnitLeasing = Object.assign({}, salesUnitLeasing);
        const copy: SalesUnitLeasing = JSON.parse(JSON.stringify(salesUnitLeasing));

        // copy.dateFrom = this.dateUtils.toDate(salesUnitLeasing.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(salesUnitLeasing.dateThru);
        return copy;
    }

    protected convertList(salesUnitLeasings: SalesUnitLeasing[]): SalesUnitLeasing[] {
        const copy: SalesUnitLeasing[] = salesUnitLeasings;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: SalesUnitLeasing[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
