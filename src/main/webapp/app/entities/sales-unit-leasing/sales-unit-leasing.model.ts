import { BaseEntity } from './../../shared';

export class SalesUnitLeasing implements BaseEntity {
    constructor(
        public id?: any,
        public idSalesLeasing?: any,
        public idStatusType?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public ownerId?: any,
        public leasingCompanyId?: any,
    ) {
    }
}
