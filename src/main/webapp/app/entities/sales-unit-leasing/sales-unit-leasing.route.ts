import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalesUnitLeasingComponent } from './sales-unit-leasing.component';
import { SalesUnitLeasingPopupComponent } from './sales-unit-leasing-dialog.component';

@Injectable()
export class SalesUnitLeasingResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSalesLeasing,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const salesUnitLeasingRoute: Routes = [
    {
        path: 'sales-unit-leasing',
        component: SalesUnitLeasingComponent,
        resolve: {
            'pagingParams': SalesUnitLeasingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitLeasing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salesUnitLeasingPopupRoute: Routes = [
    {
        path: 'sales-unit-leasing-new',
        component: SalesUnitLeasingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitLeasing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sales-unit-leasing/:id/edit',
        component: SalesUnitLeasingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesUnitLeasing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
