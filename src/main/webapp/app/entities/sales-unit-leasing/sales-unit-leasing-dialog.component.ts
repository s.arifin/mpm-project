import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {SalesUnitLeasing} from './sales-unit-leasing.model';
import {SalesUnitLeasingPopupService} from './sales-unit-leasing-popup.service';
import {SalesUnitLeasingService} from './sales-unit-leasing.service';
import {ToasterService} from '../../shared';
import { SalesUnitRequirement, SalesUnitRequirementService } from '../sales-unit-requirement';
import { LeasingCompany, LeasingCompanyService } from '../leasing-company';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sales-unit-leasing-dialog',
    templateUrl: './sales-unit-leasing-dialog.component.html'
})
export class SalesUnitLeasingDialogComponent implements OnInit {

    salesUnitLeasing: SalesUnitLeasing;
    isSaving: boolean;

    salesunitrequirements: SalesUnitRequirement[];

    leasingcompanies: LeasingCompany[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected salesUnitLeasingService: SalesUnitLeasingService,
        protected salesUnitRequirementService: SalesUnitRequirementService,
        protected leasingCompanyService: LeasingCompanyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.salesUnitRequirementService.query()
            .subscribe((res: ResponseWrapper) => { this.salesunitrequirements = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.leasingCompanyService.query()
            .subscribe((res: ResponseWrapper) => { this.leasingcompanies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.salesUnitLeasing.idSalesLeasing !== undefined) {
            this.subscribeToSaveResponse(
                this.salesUnitLeasingService.update(this.salesUnitLeasing));
        } else {
            this.subscribeToSaveResponse(
                this.salesUnitLeasingService.create(this.salesUnitLeasing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<SalesUnitLeasing>) {
        result.subscribe((res: SalesUnitLeasing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: SalesUnitLeasing) {
        this.eventManager.broadcast({ name: 'salesUnitLeasingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesUnitLeasing saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'salesUnitLeasing Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackSalesUnitRequirementById(index: number, item: SalesUnitRequirement) {
        return item.idRequirement;
    }

    trackLeasingCompanyById(index: number, item: LeasingCompany) {
        return item.idPartyRole;
    }
}

@Component({
    selector: 'jhi-sales-unit-leasing-popup',
    template: ''
})
export class SalesUnitLeasingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected salesUnitLeasingPopupService: SalesUnitLeasingPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salesUnitLeasingPopupService
                    .open(SalesUnitLeasingDialogComponent as Component, params['id']);
            } else {
                this.salesUnitLeasingPopupService
                    .open(SalesUnitLeasingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
