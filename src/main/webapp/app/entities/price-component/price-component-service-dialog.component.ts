import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PriceComponent} from './price-component.model';
import {PriceComponentPopupService} from './price-component-popup.service';
import {PriceComponentService} from './price-component.service';
import {ToasterService} from '../../shared';
import { Product, ProductService } from '../product';
import { Party, PartyService } from '../party';
import { WorkEffortType, WorkEffortTypeService } from '../work-effort-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-price-component-service-dialog',
    templateUrl: './price-component-service-dialog.component.html'
})
export class PriceComponentServiceDialogComponent implements OnInit {

    priceComponent: PriceComponent;
    isSaving: boolean;

    products: Product[];

    parties: Party[];

    workefforttypes: WorkEffortType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected priceComponentService: PriceComponentService,
        protected productService: ProductService,
        protected partyService: PartyService,
        protected workEffortTypeService: WorkEffortTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.workEffortTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.workefforttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.priceComponent.idPriceComponent !== undefined) {
            this.subscribeToSaveResponse(
                this.priceComponentService.update(this.priceComponent));
        } else {
            this.subscribeToSaveResponse(
                this.priceComponentService.create(this.priceComponent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PriceComponent>) {
        result.subscribe((res: PriceComponent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PriceComponent) {
        this.eventManager.broadcast({ name: 'priceComponentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'priceComponent saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'priceComponent Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }

    trackWorkEffortTypeById(index: number, item: WorkEffortType) {
        return item.idWeType;
    }
}

@Component({
    selector: 'jhi-price-component-service-popup',
    template: ''
})
export class PriceComponentServicePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected priceComponentPopupService: PriceComponentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.priceComponentPopupService
                    .open(PriceComponentServiceDialogComponent as Component, params['id']);
            } else {
                this.priceComponentPopupService
                    .open(PriceComponentServiceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
