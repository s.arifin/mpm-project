import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PriceComponentComponent } from './price-component.component';
import { PriceComponentPopupComponent } from './price-component-dialog.component';
import { PriceComponentUploadComponent } from './price-component-upload';

@Injectable()
export class PriceComponentResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPriceComponent,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const priceComponentRoute: Routes = [
    {
        path: 'price-component',
        component: PriceComponentComponent,
        resolve: {
            'pagingParams': PriceComponentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.priceComponent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'price-component-upload',
        component: PriceComponentUploadComponent,
        resolve: {
            'pagingParams': PriceComponentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.priceComponent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const priceComponentPopupRoute: Routes = [
    {
        path: 'price-component-new/:idproduct',
        component: PriceComponentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.priceComponent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'price-component/:id/edit',
        component: PriceComponentPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.priceComponent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
