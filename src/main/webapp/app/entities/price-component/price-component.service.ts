import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { PriceComponent, PriceComponentUpload } from './price-component.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as PriceTypeConstants from '../../shared/constants/price-type.constants';
import * as moment from 'moment';

@Injectable()
export class PriceComponentService {
    protected itemValues: PriceComponent[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/price-components';
    protected resourceSearchUrl = 'api/_search/price-components';
    protected resourceCUrl = process.env.API_C_URL + '/api/price_component';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    countMotorPrice(pricecomponents: PriceComponent[]): number {
        let price: number;
        price = 0;

        for (let i = 0; i < pricecomponents.length; i++) {
            const priceComponent: PriceComponent = pricecomponents[i];
            const idpricetype = priceComponent.idPriceType;
            if (idpricetype === PriceTypeConstants.BBN || idpricetype === PriceTypeConstants.HET) {
                price = Math.floor(price) + Math.floor(priceComponent.price);
            }
        }

        return price;
    }

    create(priceComponent: PriceComponent): Observable<PriceComponent> {
        const copy = this.convert(priceComponent);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(priceComponent: PriceComponent): Observable<PriceComponent> {
        const copy = this.convert(priceComponent);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<PriceComponent> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    getAllPriceByProductAndInternal(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idproduct);
        options.params.set('idinternal', req.idinternal);

        return this.http.get(this.resourceUrl + '/all/by-product-and-internal', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllPriceByProductAndVendor(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.params.set('idproduct', req.idProduct);
        options.params.set('idvendor', req.idVendor);

        return this.http.get(this.resourceUrl + '/all/by-product-and-vendor', options)
            .map((res: Response) => this.convertResponse(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);

        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    uploadPriceComponent(priceComponentsUpload: PriceComponentUpload[]): Observable<ResponseWrapper> {
        const copy = JSON.stringify(priceComponentsUpload);

        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/UploadComponent`, copy, options).map((res: Response) => {
            return res;
        });
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, priceComponent: PriceComponent): Observable<PriceComponent> {
        const copy = this.convert(priceComponent);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, priceComponents: PriceComponent[]): Observable<PriceComponent[]> {
        const copy = this.convertList(priceComponents);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(priceComponent: PriceComponent): PriceComponent {
        if (priceComponent === null || priceComponent === {}) {
            return {};
        }
        // const copy: PriceComponent = Object.assign({}, priceComponent);
        const copy: PriceComponent = JSON.parse(JSON.stringify(priceComponent));

        // copy.dateFrom = this.dateUtils.toDate(priceComponent.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(priceComponent.dateThru);
        return copy;
    }

    protected convertList(priceComponents: PriceComponent[]): PriceComponent[] {
        const copy: PriceComponent[] = priceComponents;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: PriceComponent[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    getAllStnkb(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);

        return this.http.get(this.resourceUrl + '/find-stnkb', options)
            .map((res: Response) => this.convertResponse(res));
    }
    ambilBiayaPengurusan(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        console.log('MASOK PAK EKO CHECK STNK BY NOKA', req)
        return this.http.get(this.resourceCUrl + '/biayaPEngurusan', options).map((res: Response) => this.convertResponse(res));
    }
}
