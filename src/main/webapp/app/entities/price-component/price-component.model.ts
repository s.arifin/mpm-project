import { BaseEntity } from './../../shared';

export class PriceComponent implements BaseEntity {
    constructor(
        public id?: any,
        public idPriceComponent?: any,
        public price?: number,
        public manfucterPrice?: number,
        public discount?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public productId?: any,
        public idPriceType?: any,
        public priceTypeDescription?: any,
        public sellerId?: any,
        public wetypeId?: any,
        public idpricetype?: any,
    ) {
    }
}

export class PriceComponentUpload implements BaseEntity {
    constructor(
        public id?: any,
        public idPriceComponent?: any,
        public bbnkb?: number,
        public pkb?: number,
        public swdkllj?: number,
        public stnk?: number,
        public tnkb?: number,
        public parkir?: number,
        public biaya?: number,
        public biroJasa?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public productId?: any,
        public het?: number,
        public bbn?: number,
        public tax?: number,
        public kodeVendor?: any,
        public kodeDealer?: any,
        public idInternal?: any,
        public priceTypeDescription?: any,
    ) {
    }
}
