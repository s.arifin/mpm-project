import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PriceComponent } from './price-component.model';
import { PriceComponentService } from './price-component.service';
import { Principal } from '../../shared';
import { Internal, InternalService } from '../internal';

@Injectable()
export class PriceComponentPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected priceComponentService: PriceComponentService,
        protected principalService: Principal,
        protected internalService: InternalService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, idproduct?: string | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.priceComponentService.find(id).subscribe((data) => {
                    // if (data.dateFrom) {
                    //    data.dateFrom = this.datePipe
                    //        .transform(data.dateFrom, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    // if (data.dateThru) {
                    //    data.dateThru = this.datePipe
                    //        .transform(data.dateThru, 'yyyy-MM-ddTHH:mm:ss');
                    // }
                    this.ngbModalRef = this.priceComponentModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else if (idproduct) {
                setTimeout(
                    () => {
                        const date = new Date();

                        const data = new PriceComponent();
                        data.productId = idproduct;

                        const dateFrom = date.setHours(0, 0, 0);
                        data.dateFrom = new Date(dateFrom);

                        const dateThru = date.setHours(23, 59, 59);
                        data.dateThru = new Date(dateThru);

                        this.internalService.find(this.principalService.getIdInternal()).subscribe(
                            (res: Internal) => {
                                data.sellerId = res.organization.idParty;
                                console.log('data', data);
                                this.ngbModalRef = this.priceComponentModalRef(component, data);
                                resolve(this.ngbModalRef);
                            }
                        )
                    }, 0
                )
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new PriceComponent();
                    this.ngbModalRef = this.priceComponentModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    priceComponentModalRef(component: Component, priceComponent: PriceComponent): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.priceComponent = priceComponent;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.priceComponentLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    priceComponentLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
