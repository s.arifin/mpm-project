import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PriceComponent, PriceComponentUpload } from './price-component.model';
import { PriceComponentService } from './price-component.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

import { LazyLoadEvent } from 'primeng/primeng';
import { ToasterService } from '../../shared/alert/toaster.service';
import { ConfirmationService } from 'primeng/primeng';
import { PriceTypeService, PriceType } from '../price-type';
import * as XLSX from 'xlsx';

@Component({
    selector: 'jhi-price-component-upload',
    templateUrl: './price-component-upload.html',
    styles: []
})
export class PriceComponentUploadComponent implements OnInit, OnDestroy {

    currentAccount: any;
    priceComponents: PriceComponent[];
    priceComponentUpload: PriceComponentUpload;
    priceComponentsUpload: PriceComponentUpload[];
    priceComponent: PriceComponent;
    priceType: any;
    priceTypes: PriceType[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    containerData: any;
    invalidFormat: Boolean;
    tanggalharga = {
        tanggal: null,
        bulan: null,
        tahun: null
    }

    constructor(
        protected priceComponentService: PriceComponentService,
        protected confirmationService: ConfirmationService,
        protected priceTypeService: PriceTypeService,
        protected parseLinks: JhiParseLinks,
        protected alertService: JhiAlertService,
        protected principal: Principal,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager,
        protected paginationUtil: JhiPaginationUtil,
        protected paginationConfig: PaginationConfig,
        protected toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.priceComponentService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.priceComponentService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/price-component'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/motor', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/price-component', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity(true).then((account) => {
            this.currentAccount = account;
        });
        this.priceTypeService.getAllData()
            .subscribe((res) => { this.priceTypes = res.json; });
        this.registerChangeInPriceComponents();
    }

    displayPriceType(priceType) {
        console.log('ceeek', priceType);
    }

    trackByPriceTypesById(index: number, item: PriceType) {
        return item.idPriceType;
    }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbooka */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];
            /* save data */
            this.containerData = (XLSX.utils.sheet_to_json(ws));

            this.convertXLXStoListSuspect();
        };
        reader.readAsBinaryString(target.files[0]);
    }

    protected convertXLXStoListSuspect(): void {
        console.log('container data ==>', this.containerData);
        if (this.containerData[0]['BBNKB'] &&
            this.containerData[0]['PKB'] &&
            this.containerData[0]['SWDKLLJ']) {
            // UPLOAD BIAYA PENGURUSAN BIRO JASA
            this.priceComponentsUpload = new Array<PriceComponentUpload>();

            this.containerData.forEach((element) => {
                this.priceComponentUpload = new PriceComponentUpload();
                this.priceComponentUpload.productId = element['KODE'];
                this.priceComponentUpload.bbnkb = element['BBNKB'];
                this.priceComponentUpload.pkb = element['PKB'];
                this.priceComponentUpload.swdkllj = element['SWDKLLJ'];
                this.priceComponentUpload.stnk = element['STNK'];
                this.priceComponentUpload.tnkb = element['TNKB'];
                this.priceComponentUpload.parkir = element['PARKIR'];
                this.priceComponentUpload.biaya = element['BIAYA'];
                this.priceComponentUpload.biroJasa = element['BIRO_JASA'];
                this.priceComponentUpload.kodeVendor = element['KODE_VENDOR'];
                this.priceComponentUpload.kodeDealer = element['KODE_DEALER'];
                this.priceComponentUpload.idInternal = this.principal.getIdInternal();
                this.priceComponentsUpload = [...this.priceComponentsUpload, this.priceComponentUpload];
                console.log('this prince components biaya biro jasa upload gan', this.priceComponentsUpload);
            });
        } else if (this.containerData[0]['KODE']) {
            // UPLOAD HARGA JUAL
            this.priceComponentsUpload = new Array<PriceComponentUpload>();

            this.containerData.forEach((element) => {
                this.priceComponentUpload = new PriceComponentUpload();
                this.priceComponentUpload.productId = element['KODE'];
                this.priceComponentUpload.het = element['Off_The_Road'];
                this.priceComponentUpload.bbn = element['BBN'];
                this.priceComponentUpload.dateFrom = element['Start_Date'];
                this.priceComponentUpload.dateThru = element['End_Date'];
                this.priceComponentUpload.idInternal = this.principal.getIdInternal();
                // this.priceComponentUpload.stnk = element['STNK'];
                // this.priceComponentUpload.tnkb = element['TNKB'];
                // this.priceComponentUpload.parkir = element['PARKIR'];
                // this.priceComponentUpload.biaya = element['BIAYA'];
                // this.priceComponentUpload.biroJasa = element['BIRO JASA'];
                this.priceComponentsUpload = [...this.priceComponentsUpload, this.priceComponentUpload];
                console.log('this price components upload harga jual gan', this.priceComponentsUpload);
            });
        } else {
            this.invalidFormat = true;
            this.toasterService.showToaster('Info', 'Format Upload Salah', 'File yang di upload salah ..')
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PriceComponent) {
        return item.idPriceComponent;
    }

    registerChangeInPriceComponents() {
        this.eventSubscriber = this.eventManager.subscribe('priceComponentListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'idPriceComponent') {
            result.push('idPriceComponent');
        }
        return result;
    }

    saveList(): void {
        console.log('save list: ', this.priceComponentsUpload);
        this.priceComponentService.uploadPriceComponent(this.priceComponentsUpload).subscribe(
            (res) => {
                this.toasterService.showToaster('info', 'PriceComponent Saved', 'Data saved..');
            return this.clear();
            }
        );
    }

    protected onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.priceComponents = data;
    }

    protected onError(error) {
        this.alertService.error(error.message, null, null);
    }

    executeProcess(data) {
        this.priceComponentService.executeProcess(0, null, data).subscribe(
            (value) => console.log('this: ', value),
            (err) => console.log(err),
            () => this.toasterService.showToaster('info', 'Data Proces', 'Done process in system..')
        );
    }

    loadDataLazy(event: LazyLoadEvent) {
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    updateRowData(event) {
        if (event.data.id !== undefined) {
            this.priceComponentService.update(event.data)
                .subscribe((res: PriceComponent) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        } else {
            this.priceComponentService.create(event.data)
                .subscribe((res: PriceComponent) =>
                    this.onRowDataSaveSuccess(res), (res: Response) => this.onRowDataSaveError(res));
        }
    }

    protected onRowDataSaveSuccess(result: PriceComponent) {
        this.toasterService.showToaster('info', 'PriceComponent Saved', 'Data saved..');
    }

    protected onRowDataSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.onError(error);
    }

    delete(id: any) {
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete?',
            header: 'Confirmation',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.priceComponentService.delete(id).subscribe((response) => {
                    this.eventManager.broadcast({
                        name: 'priceComponentListModification',
                        content: 'Deleted an priceComponent'
                    });
                });
            }
        });
    }
}
