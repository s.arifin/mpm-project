import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {PriceComponent} from './price-component.model';
import {PriceComponentPopupService} from './price-component-popup.service';
import {PriceComponentService} from './price-component.service';
import {ToasterService} from '../../shared';
import { Product, ProductService } from '../product';
import { Party, PartyService } from '../party';
import { WorkEffortType, WorkEffortTypeService } from '../work-effort-type';
import { ResponseWrapper, Principal } from '../../shared';

import { AccountService } from '../../shared/auth/account.service';
import { PriceTypeService, PriceType } from '../price-type';

@Component({
    selector: 'jhi-price-component-dialog',
    templateUrl: './price-component-dialog.component.html'
})
export class PriceComponentDialogComponent implements OnInit {

    priceTypes: PriceType[];
    priceComponent: PriceComponent;
    isSaving: boolean;

    products: Product[];

    parties: Party[];

    workefforttypes: WorkEffortType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected priceComponentService: PriceComponentService,
        protected productService: ProductService,
        protected partyService: PartyService,
        protected workEffortTypeService: WorkEffortTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected priceTypeService: PriceTypeService,
        protected accountService: AccountService,
        protected principalService: Principal
    ) {
        // this.priceComponent = new PriceComponent();
    }

    ngOnInit() {
        console.log('this.priceComponent', this.priceComponent);
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; console.log('a', this.parties); }, (res: ResponseWrapper) => this.onError(res.json));
        this.workEffortTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.workefforttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.priceTypeService.getAllData()
            .subscribe((res) => { this.priceTypes = res.json; });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.priceComponent.idPriceComponent !== undefined) {
            this.subscribeToSaveResponse(
                this.priceComponentService.update(this.priceComponent));
        } else {
            this.subscribeToSaveResponse(
                this.priceComponentService.create(this.priceComponent));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PriceComponent>) {
        result.subscribe((res: PriceComponent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: PriceComponent) {
        this.eventManager.broadcast({ name: 'priceComponentListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'priceComponent saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'priceComponent Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }

    trackWorkEffortTypeById(index: number, item: WorkEffortType) {
        return item.idWeType;
    }

    trackByPriceTypesById(index: number, item: PriceType) {
        return item.idPriceType;
    }
}

@Component({
    selector: 'jhi-price-component-popup',
    template: ''
})
export class PriceComponentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected priceComponentPopupService: PriceComponentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.priceComponentPopupService
                    .open(PriceComponentDialogComponent as Component, params['id'], null);
            } else if (params['idproduct']) {
                this.priceComponentPopupService
                    .open(PriceComponentDialogComponent as Component, null, params['idproduct']);
            } else {
                this.priceComponentPopupService
                    .open(PriceComponentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
