import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Branch} from './branch.model';
import {BranchPopupService} from './branch-popup.service';
import {BranchService} from './branch.service';
import {ToasterService} from '../../shared';
import { RoleType, RoleTypeService } from '../role-type';
import { Party, PartyService } from '../party';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-branch-dialog',
    templateUrl: './branch-dialog.component.html'
})
export class BranchDialogComponent implements OnInit {

    branch: Branch;
    isSaving: boolean;
    roletypes: RoleType[];
    parties: Party[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected branchService: BranchService,
        protected roleTypeService: RoleTypeService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.roleTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.roletypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.partyService.query()
            .subscribe((res: ResponseWrapper) => { this.parties = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.branch.idInternal !== undefined) {
            this.subscribeToSaveResponse(
                this.branchService.update(this.branch));
        } else {
            this.subscribeToSaveResponse(
                this.branchService.create(this.branch));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Branch>) {
        result.subscribe((res: Branch) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Branch) {
        this.eventManager.broadcast({ name: 'branchListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'branch saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'branch Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackRoleTypeById(index: number, item: RoleType) {
        return item.idRoleType;
    }

    trackPartyById(index: number, item: Party) {
        return item.idParty;
    }
}

@Component({
    selector: 'jhi-branch-popup',
    template: ''
})
export class BranchPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected branchPopupService: BranchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.branchPopupService
                    .open(BranchDialogComponent as Component, params['id']);
            } else {
                this.branchPopupService
                    .open(BranchDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
