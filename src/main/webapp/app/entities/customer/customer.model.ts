import { BaseEntity } from './../../shared';

export class Customer implements BaseEntity {
    constructor(
        public id?: any,
        public idCustomer?: string,
        public partyId?: any,
        public partyName?: any,
        public idMPM?: string
    ) {
    }
}

export class CustomerAX implements BaseEntity {
    constructor(
        public id?: any,
        public idCustomer?: string,
        public partyId?: any,
        public partyName?: any,
        public idMPM?: string,
        public bankAccount?: string,
    ) {
    }
}
