import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { SERVER_API_C_URL } from '../../app.constants';
import { SERVER_API_URL } from '../../app.constants';

import { Customer, CustomerAX } from './customer.model';
import { BillTo } from '../bill-to/bill-to.model';
import { ResponseWrapper, createRequestOption } from '../../shared/index';

import * as moment from 'moment';

@Injectable()
export class CustomerCService {

    // protected resourceUrl = SERVER_API_URL.substr(2, 4);
    protected resourceUrl = process.env.API_C_URL + '/api/customers';
    // protected resourceUrl = SERVER_API_URL + 'api/customers';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    find(id: any): Observable<CustomerAX> { // using idparty

        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findByIdCustomer(id: any, pymtType: any): Observable<CustomerAX> { // using idcustomer

        return this.http.get(`${this.resourceUrl}/GetCustomersByIDCustomer/?id=${id}&pymtMethod=${pymtType}`).map((res: Response) => {
            return res.json();
        });
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateRegister) {
            entity.dateRegister = new Date(entity.dateRegister);
        }
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }
}
