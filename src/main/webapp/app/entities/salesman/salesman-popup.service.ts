import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Salesman } from './salesman.model';
import { Person } from '../person';
import { SalesmanService } from './salesman.service';

@Injectable()
export class SalesmanPopupService {
    protected ngbModalRef: NgbModalRef;

    constructor(
        protected datePipe: DatePipe,
        protected modalService: NgbModal,
        protected router: Router,
        protected salesmanService: SalesmanService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.salesmanService.find(id).subscribe((data) => {
                    if (data.dateRegister) {
                        data.dateRegister = new Date(data.dateRegister);
                    }
                    if (data.dateFrom) {
                        data.dateFrom = new Date(data.dateFrom);
                    }
                    if (data.dateThru) {
                        data.dateThru = new Date(data.dateThru);
                    }
                    if (data.person === undefined || data.person === null) {
                        data.person = new Person();
                    }

                    if (data.coordinator === true) {
                        data.roleAs = 'Korsal';
                    } else if (data.teamLeader === true) {
                        data.roleAs = 'TL';
                    } else {
                        data.roleAs = 'Sales';
                    }

                    this.ngbModalRef = this.salesmanModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Salesman();
                    this.ngbModalRef = this.salesmanModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    salesmanModalRef(component: Component, salesman: Salesman): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.salesman = salesman;
        // modalRef.componentInstance.homeAddress = salesman.person.getPostalAddress(1);
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
