import { BaseEntity } from './../../shared';
import { PostalAddress } from './../postal-address';
import { Person } from './../person';
import { PartyRole } from './../party-role';

export class Salesman extends PartyRole {
    constructor(
        public idSalesman?: string,
        public coordinator?: boolean,
        public teamLeader?: boolean,
        public coordinatorSalesId?: any,
        public coordinatorSalesName?: any,
        public teamLeaderSalesId?: any,
        public teamLeaderSalesName?: any,
        public person?: Person,
        public roleAs?: string,
        public ahmCode?: any,
        public userName?: any,
        public name?: any,
    ) {
        super();
        this.coordinator = false;
        this.teamLeader = false;
        this.person = new Person();
    }

}
