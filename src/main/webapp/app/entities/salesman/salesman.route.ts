import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SalesmanComponent } from './salesman.component';
import { SalesmanPopupComponent } from './salesman-dialog.component';

@Injectable()
export class SalesmanResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idSalesman,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const salesmanRoute: Routes = [
    {
        path: 'salesman',
        component: SalesmanComponent,
        resolve: {
            'pagingParams': SalesmanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesman.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const salesmanPopupRoute: Routes = [
    {
        path: 'salesman-new',
        component: SalesmanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesman.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'salesman/:id/edit',
        component: SalesmanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.salesman.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
