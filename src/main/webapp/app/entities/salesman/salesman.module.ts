import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {MpmSharedModule} from '../../shared';
import {
    SalesmanService,
    SalesmanPopupService,
    SalesmanComponent,
    SalesmanDialogComponent,
    SalesmanPopupComponent,
    salesmanRoute,
    salesmanPopupRoute,
    SalesmanResolvePagingParams,
} from './';

import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import {
         CheckboxModule,
         InputTextModule,
         CalendarModule,
         DropdownModule,
         ButtonModule,
         DataTableModule,
         PaginatorModule,
         DialogModule,
         ConfirmDialogModule,
         ConfirmationService,
         GrowlModule,
         DataGridModule,
         SharedModule,
         AccordionModule,
         TabViewModule,
         FieldsetModule,
         ScheduleModule,
         PanelModule,
         ListboxModule,
         SelectItem,
         MenuItem,
         Header,
         Footer} from 'primeng/primeng';

import { MpmSharedEntityModule } from '../shared-entity.module';

const ENTITY_STATES = [
    ...salesmanRoute,
    ...salesmanPopupRoute,
];

@NgModule({
    imports: [
        MpmSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        MpmSharedEntityModule,
        CommonModule,
        ButtonModule,
        DataTableModule,
        PaginatorModule,
        ConfirmDialogModule,
        TabViewModule,
        DataGridModule,
        ScheduleModule,

        CheckboxModule,
        CalendarModule,
        DropdownModule,
        ListboxModule,
        FieldsetModule,
        PanelModule,
        DialogModule,
        AccordionModule,
        PanelModule,
        CurrencyMaskModule
    ],
    exports: [
        SalesmanComponent,
    ],
    declarations: [
        SalesmanComponent,
        SalesmanDialogComponent,
        SalesmanPopupComponent,
    ],
    entryComponents: [
        SalesmanComponent,
        SalesmanDialogComponent,
        SalesmanPopupComponent,
    ],
    providers: [
        SalesmanService,
        SalesmanPopupService,
        SalesmanResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmSalesmanModule {}
