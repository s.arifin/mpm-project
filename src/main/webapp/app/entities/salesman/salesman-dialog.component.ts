import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import { Salesman} from './salesman.model';
import { SalesmanPopupService} from './salesman-popup.service';
import { SalesmanService} from './salesman.service';
import { ToasterService, CommonUtilService} from '../../shared';
import { ContactMechanismPurpose } from '../shared-component';
import { PostalAddress } from './../postal-address';
import { RoleType, RoleTypeService } from '../role-type';
import { Person } from '../person';
import { ResponseWrapper } from '../../shared';
import { Party, PartyService } from '../party';

@Component({
    selector: 'jhi-salesman-dialog',
    templateUrl: './salesman-dialog.component.html'
})
export class SalesmanDialogComponent implements OnInit {

    salesman: Salesman;
    isSaving: boolean;
    roletypes: RoleType[];
    parties: Party[];
    korsals: Salesman[];
    teamLeaders: Salesman[];
    homeAddress: PostalAddress;
    // dobDp: any;

    roles: Array<object> = [
        {label : 'Salesman', value : 'Sales'},
        {label : 'TeamLeader', value : 'TL'},
        {label : 'Koordinator Sales', value: 'Korsal'}
    ];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected salesmanService: SalesmanService,
        protected roleTypeService: RoleTypeService,
        protected partyService: PartyService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService,
        protected commonUtils: CommonUtilService,
    ) {
    }

    prepare() {
    }

    ngOnInit() {
        this.isSaving = false;
        this.salesmanService.query({
            size : 9999
        }
        )
            .subscribe((res: ResponseWrapper) => {
                this.korsals = res.json.filter((d) => d.coordinator === true);
                this.teamLeaders = res.json.filter((d) => d.teamLeader === true); },
                (res: ResponseWrapper) => this.onError(res.json));
        // this.homeAddress = this.getPostalAddress( 2, this.salesman.person);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        if (this.salesman.person.privateMail == null || this.salesman.person.privateMail === '' ) {
            this.toaster.showToaster('Eror', 'Informasi', 'Email Tidak boleh kosong');
        }
        if (this.salesman.userName == null || this.salesman.userName === '') {
            this.toaster.showToaster('Eror', 'Informasi', 'UserName Tidak boleh kosong');
        } else {
                this.isSaving = true;
                this.switchRole(this.salesman.roleAs).then(
                    () => {
                        if (this.salesman.idPartyRole !== undefined) {
                            this.subscribeToSaveResponse(
                                this.salesmanService.update(this.salesman));
                        } else {
                            this.subscribeToSaveResponse(
                                this.salesmanService.create(this.salesman));
                        }
                    }
                );
            }
    }

    protected subscribeToSaveResponse(result: Observable<Salesman>) {
        result.subscribe(
            (res: Salesman) => this.onSaveSuccess(res),
            (res) => {this.isSaving = false, this.commonUtils.showError(res)}
            );
    }

    protected onSaveSuccess(result: Salesman) {
        this.eventManager.broadcast({ name: 'salesmanListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'salesman saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
        console.log('result ', this.salesman);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.isSaving = false;
        this.toaster.showToaster('warning', 'salesman Changed', error.message);
        this.commonUtils.showError(error);
        this.alertService.error(error.message, null, null);
    }

    trackSalesmanById(index: number, item: Salesman) {
        return item.idPartyRole;
    }

    switchRole(obj: string): Promise<any> {
        return new Promise<any>(
            (resolve) => {
                if (obj === 'Korsal') {
                    this.salesman.coordinator = true;
                    this.salesman.teamLeader = false;
                    this.salesman.coordinatorSalesId = null;
                    this.salesman.teamLeaderSalesId = null;
                } else if (obj === 'TL') {
                    this.salesman.coordinator = false;
                    this.salesman.teamLeader = true;
                    this.salesman.teamLeaderSalesId = null;
                } else {
                    this.salesman.coordinator = false;
                    this.salesman.teamLeader = false;
                }
                resolve();
            }
        )
    }

    // getPostalAddress(id: number, p: Person): PostalAddress {
    //     let purp: ContactMechanismPurpose =  null;
    //     purp =  p.contactPurposes.find((x) => x.idPurposeType === id);

    //     if (purp === null || purp === undefined) {
    //         purp = new ContactMechanismPurpose();
    //         purp.postal = new PostalAddress();
    //         purp.idPurposeType = id;
    //         purp.partyId = p.idParty;
    //         p.contactPurposes.push(purp);
    //     }

    //     if (purp.contact === undefined ||  purp.contact == null)  {
    //         purp.contact = new PostalAddress();
    //     }

    //     return <PostalAddress> purp.contact;
    // }

    // setPostalAddress(id: number, p: Person, a: PostalAddress): PostalAddress {
    //     let purp: ContactMechanismPurpose =  null;
    //     purp =  p.contactPurposes.find((x) => x.idPurposeType === id);

    //     if (purp === null || purp === undefined) {
    //         purp = new ContactMechanismPurpose();
    //         purp.postal = a;
    //         purp.idPurposeType = id;
    //         purp.partyId = p.idParty;
    //         p.contactPurposes.push(purp);
    //     }

    //     if (purp.contact === undefined ||  purp.contact == null)  {
    //         purp.contact = a;
    //     }

    //     return <PostalAddress> purp.contact;
    // }
}

@Component({
    selector: 'jhi-salesman-popup',
    template: ''
})
export class SalesmanPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected salesmanPopupService: SalesmanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.salesmanPopupService
                    .open(SalesmanDialogComponent as Component, params['id']);
            } else {
                this.salesmanPopupService
                    .open(SalesmanDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
