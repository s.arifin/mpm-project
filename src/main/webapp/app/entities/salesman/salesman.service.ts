import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { Salesman } from './salesman.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class SalesmanService {

    protected resourceUrl = 'api/salesmen';
    protected resourceSearchUrl = 'api/_search/salesmen';
    protected resourceCUrl = process.env.API_C_URL + '/api/salesman';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(salesman: Salesman): Observable<Salesman> {
        const copy = this.convert(salesman);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(salesman: Salesman): Observable<Salesman> {
        const copy = this.convert(salesman);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<Salesman> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    filter(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/filter', options)
            .map((res: Response) => this.convertResponse(res));
    }

    filterid(idcoordinator: String): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/filterid?idcoordinator=' + idcoordinator + '')
            .map((res: Response) => this.convertResponse(res));
    }

    filterTL(idcoordinator: String): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/filterTL?idcoordinator=' + idcoordinator + '')
            .map((res: Response) => this.convertResponse(res));
    }

    filtername(name: String): Observable<ResponseWrapper> {
        return this.http.get(this.resourceCUrl + '/filterTT?name=' + name + '')
            .map((res: Response) => this.convertResponse(res));
    }

    getTeamLeader(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getteamleader', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getSales(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getsales', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getSalesmanByTeamLeader(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl + '/getsalesman/by-teamleader', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(salesman: any): Observable<String> {
        const copy = this.convert(salesman);
        return this.http.post(this.resourceUrl + '/execute', copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateRegister) {
            entity.dateRegister = new Date(entity.dateRegister);
        }
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
        if (entity.dob) {
            entity.dob = new Date(entity.dob);
        }
    }

    protected convert(salesman: Salesman): Salesman {
        const copy: Salesman = Object.assign({}, salesman);

        // copy.dateRegister = this.dateUtils.toDate(salesman.dateRegister);

        // copy.dateFrom = this.dateUtils.toDate(salesman.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(salesman.dateThru);
        return copy;
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
