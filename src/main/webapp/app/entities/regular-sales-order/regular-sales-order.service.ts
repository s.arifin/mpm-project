import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { RegularSalesOrder } from './regular-sales-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class RegularSalesOrderService {
    protected itemValues: RegularSalesOrder[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/regular-sales-orders';
    protected resourceSearchUrl = 'api/_search/regular-sales-orders';

    constructor(protected http: Http) { }

    create(regularSalesOrder: RegularSalesOrder): Observable<RegularSalesOrder> {
        const copy = this.convert(regularSalesOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(regularSalesOrder: RegularSalesOrder): Observable<RegularSalesOrder> {
        const copy = this.convert(regularSalesOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<RegularSalesOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, regularSalesOrder: RegularSalesOrder): Observable<RegularSalesOrder> {
        const copy = this.convert(regularSalesOrder);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, regularSalesOrders: RegularSalesOrder[]): Observable<RegularSalesOrder[]> {
        const copy = this.convertList(regularSalesOrders);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(regularSalesOrder: RegularSalesOrder): RegularSalesOrder {
        if (regularSalesOrder === null || regularSalesOrder === {}) {
            return {};
        }
        // const copy: RegularSalesOrder = Object.assign({}, regularSalesOrder);
        const copy: RegularSalesOrder = JSON.parse(JSON.stringify(regularSalesOrder));
        return copy;
    }

    protected convertList(regularSalesOrders: RegularSalesOrder[]): RegularSalesOrder[] {
        const copy: RegularSalesOrder[] = regularSalesOrders;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: RegularSalesOrder[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
