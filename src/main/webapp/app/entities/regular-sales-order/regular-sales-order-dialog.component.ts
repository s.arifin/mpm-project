import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {RegularSalesOrder} from './regular-sales-order.model';
import {RegularSalesOrderPopupService} from './regular-sales-order-popup.service';
import {RegularSalesOrderService} from './regular-sales-order.service';
import {ToasterService} from '../../shared';
import { Internal, InternalService } from '../internal';
import { Customer, CustomerService } from '../customer';
import { BillTo, BillToService } from '../bill-to';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-regular-sales-order-dialog',
    templateUrl: './regular-sales-order-dialog.component.html'
})
export class RegularSalesOrderDialogComponent implements OnInit {

    regularSalesOrder: RegularSalesOrder;
    isSaving: boolean;

    internals: Internal[];

    customers: Customer[];

    billtos: BillTo[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected regularSalesOrderService: RegularSalesOrderService,
        protected internalService: InternalService,
        protected customerService: CustomerService,
        protected billToService: BillToService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.internalService.query()
            .subscribe((res: ResponseWrapper) => { this.internals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.billToService.query()
            .subscribe((res: ResponseWrapper) => { this.billtos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.regularSalesOrder.idOrder !== undefined) {
            this.subscribeToSaveResponse(
                this.regularSalesOrderService.update(this.regularSalesOrder));
        } else {
            this.subscribeToSaveResponse(
                this.regularSalesOrderService.create(this.regularSalesOrder));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RegularSalesOrder>) {
        result.subscribe((res: RegularSalesOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RegularSalesOrder) {
        this.eventManager.broadcast({ name: 'regularSalesOrderListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'regularSalesOrder saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'regularSalesOrder Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackInternalById(index: number, item: Internal) {
        return item.idInternal;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.idCustomer;
    }

    trackBillToById(index: number, item: BillTo) {
        return item.idBillTo;
    }
}

@Component({
    selector: 'jhi-regular-sales-order-popup',
    template: ''
})
export class RegularSalesOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected regularSalesOrderPopupService: RegularSalesOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.regularSalesOrderPopupService
                    .open(RegularSalesOrderDialogComponent as Component, params['id']);
            } else {
                this.regularSalesOrderPopupService
                    .open(RegularSalesOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
