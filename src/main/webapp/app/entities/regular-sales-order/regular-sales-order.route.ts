import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RegularSalesOrderComponent } from './regular-sales-order.component';
import { RegularSalesOrderEditComponent } from './regular-sales-order-edit.component';
import { RegularSalesOrderPopupComponent } from './regular-sales-order-dialog.component';

@Injectable()
export class RegularSalesOrderResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idOrder,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const regularSalesOrderRoute: Routes = [
    {
        path: 'regular-sales-order',
        component: RegularSalesOrderComponent,
        resolve: {
            'pagingParams': RegularSalesOrderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.regularSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const regularSalesOrderPopupRoute: Routes = [
    {
        path: 'regular-sales-order-new',
        component: RegularSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.regularSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'regular-sales-order/:id/edit',
        component: RegularSalesOrderEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.regularSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'regular-sales-order-popup-new',
        component: RegularSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.regularSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'regular-sales-order/:id/popup-edit',
        component: RegularSalesOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.regularSalesOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
