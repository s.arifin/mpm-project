import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {ProductAssociation} from './product-association.model';
import {ProductAssociationPopupService} from './product-association-popup.service';
import {ProductAssociationService} from './product-association.service';
import {ToasterService} from '../../shared';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-product-association-dialog',
    templateUrl: './product-association-dialog.component.html'
})
export class ProductAssociationDialogComponent implements OnInit {

    productAssociation: ProductAssociation;
    isSaving: boolean;

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected productAssociationService: ProductAssociationService,
        protected productService: ProductService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productAssociation.idProductAssociation !== undefined) {
            this.subscribeToSaveResponse(
                this.productAssociationService.update(this.productAssociation));
        } else {
            this.subscribeToSaveResponse(
                this.productAssociationService.create(this.productAssociation));
        }
    }

    protected subscribeToSaveResponse(result: Observable<ProductAssociation>) {
        result.subscribe((res: ProductAssociation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: ProductAssociation) {
        this.eventManager.broadcast({ name: 'productAssociationListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'productAssociation saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'productAssociation Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.idProduct;
    }
}

@Component({
    selector: 'jhi-product-association-popup',
    template: ''
})
export class ProductAssociationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected productAssociationPopupService: ProductAssociationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productAssociationPopupService
                    .open(ProductAssociationDialogComponent as Component, params['id']);
            } else {
                this.productAssociationPopupService
                    .open(ProductAssociationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
