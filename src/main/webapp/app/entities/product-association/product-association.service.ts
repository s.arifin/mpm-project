import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JhiDateUtils } from 'ng-jhipster';

import { ProductAssociation } from './product-association.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class ProductAssociationService {
    protected itemValues: ProductAssociation[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/product-associations';
    protected resourceSearchUrl = 'api/_search/product-associations';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(productAssociation: ProductAssociation): Observable<ProductAssociation> {
        const copy = this.convert(productAssociation);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(productAssociation: ProductAssociation): Observable<ProductAssociation> {
        const copy = this.convert(productAssociation);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: any): Observable<ProductAssociation> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, productAssociation: ProductAssociation): Observable<ProductAssociation> {
        const copy = this.convert(productAssociation);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, productAssociations: ProductAssociation[]): Observable<ProductAssociation[]> {
        const copy = this.convertList(productAssociations);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convertItemFromServer(entity: any) {
        if (entity.dateFrom) {
            entity.dateFrom = new Date(entity.dateFrom);
        }
        if (entity.dateThru) {
            entity.dateThru = new Date(entity.dateThru);
        }
    }

    protected convert(productAssociation: ProductAssociation): ProductAssociation {
        if (productAssociation === null || productAssociation === {}) {
            return {};
        }
        // const copy: ProductAssociation = Object.assign({}, productAssociation);
        const copy: ProductAssociation = JSON.parse(JSON.stringify(productAssociation));

        // copy.dateFrom = this.dateUtils.toDate(productAssociation.dateFrom);

        // copy.dateThru = this.dateUtils.toDate(productAssociation.dateThru);
        return copy;
    }

    protected convertList(productAssociations: ProductAssociation[]): ProductAssociation[] {
        const copy: ProductAssociation[] = productAssociations;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: ProductAssociation[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }
}
