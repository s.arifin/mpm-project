import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProductAssociationComponent } from './product-association.component';
import { ProductAssociationPopupComponent } from './product-association-dialog.component';

@Injectable()
export class ProductAssociationResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idProductAssociation,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const productAssociationRoute: Routes = [
    {
        path: 'product-association',
        component: ProductAssociationComponent,
        resolve: {
            'pagingParams': ProductAssociationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productAssociation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productAssociationPopupRoute: Routes = [
    {
        path: 'product-association-new',
        component: ProductAssociationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productAssociation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-association/:id/edit',
        component: ProductAssociationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.productAssociation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
