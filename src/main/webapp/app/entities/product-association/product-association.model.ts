import { BaseEntity } from './../../shared';

export class ProductAssociation implements BaseEntity {
    constructor(
        public id?: any,
        public idProductAssociation?: any,
        public qty?: number,
        public dateFrom?: any,
        public dateThru?: any,
        public productToId?: any,
        public productFromId?: any,
    ) {
    }
}
