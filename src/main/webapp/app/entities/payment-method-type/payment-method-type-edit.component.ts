import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentMethodType } from './payment-method-type.model';
import { PaymentMethodTypeService } from './payment-method-type.service';
import { ToasterService} from '../../shared';

@Component({
   selector: 'jhi-payment-method-type-edit',
   templateUrl: './payment-method-type-edit.component.html'
})
export class PaymentMethodTypeEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   paymentMethodType: PaymentMethodType;
   isSaving: boolean;
   idPaymentMethodType: any;
   paramPage: number;
   routeId: number;

   constructor(
       protected alertService: JhiAlertService,
       protected paymentMethodTypeService: PaymentMethodTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.paymentMethodType = new PaymentMethodType();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idPaymentMethodType = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.paymentMethodTypeService.find(this.idPaymentMethodType).subscribe((paymentMethodType) => {
           this.paymentMethodType = paymentMethodType;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['payment-method-type', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.paymentMethodType.idPaymentMethodType !== undefined) {
           this.subscribeToSaveResponse(
               this.paymentMethodTypeService.update(this.paymentMethodType));
       } else {
           this.subscribeToSaveResponse(
               this.paymentMethodTypeService.create(this.paymentMethodType));
       }
   }

   protected subscribeToSaveResponse(result: Observable<PaymentMethodType>) {
       result.subscribe((res: PaymentMethodType) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: PaymentMethodType) {
       this.eventManager.broadcast({ name: 'paymentMethodTypeListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'paymentMethodType saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'paymentMethodType Changed', error.message);
       this.alertService.error(error.message, null, null);
   }
}
