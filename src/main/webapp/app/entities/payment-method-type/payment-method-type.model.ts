import { BaseEntity } from './../../shared';

export class PaymentMethodType implements BaseEntity {
    constructor(
        public id?: number,
        public idPaymentMethodType?: number,
        public refKey?: string,
        public description?: string,
    ) {
    }
}
