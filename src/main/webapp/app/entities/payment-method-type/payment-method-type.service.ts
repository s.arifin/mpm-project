import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PaymentMethodType } from './payment-method-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PaymentMethodTypeService {
   protected itemValues: PaymentMethodType[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/payment-method-types';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/payment-method-types';

   constructor(protected http: Http) { }

   create(paymentMethodType: PaymentMethodType): Observable<PaymentMethodType> {
       const copy = this.convert(paymentMethodType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(paymentMethodType: PaymentMethodType): Observable<PaymentMethodType> {
       const copy = this.convert(paymentMethodType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<PaymentMethodType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: PaymentMethodType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: PaymentMethodType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to PaymentMethodType.
    */
   protected convertItemFromServer(json: any): PaymentMethodType {
       const entity: PaymentMethodType = Object.assign(new PaymentMethodType(), json);
       return entity;
   }

   /**
    * Convert a PaymentMethodType to a JSON which can be sent to the server.
    */
   protected convert(paymentMethodType: PaymentMethodType): PaymentMethodType {
       if (paymentMethodType === null || paymentMethodType === {}) {
           return {};
       }
       // const copy: PaymentMethodType = Object.assign({}, paymentMethodType);
       const copy: PaymentMethodType = JSON.parse(JSON.stringify(paymentMethodType));
       return copy;
   }

   protected convertList(paymentMethodTypes: PaymentMethodType[]): PaymentMethodType[] {
       const copy: PaymentMethodType[] = paymentMethodTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: PaymentMethodType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
