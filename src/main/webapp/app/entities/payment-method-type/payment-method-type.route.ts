import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PaymentMethodTypeComponent } from './payment-method-type.component';
import { PaymentMethodTypeEditComponent } from './payment-method-type-edit.component';
import { PaymentMethodTypePopupComponent } from './payment-method-type-dialog.component';

@Injectable()
export class PaymentMethodTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idPaymentMethodType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const paymentMethodTypeRoute: Routes = [
    {
        path: 'payment-method-type',
        component: PaymentMethodTypeComponent,
        resolve: {
            'pagingParams': PaymentMethodTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentMethodType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentMethodTypePopupRoute: Routes = [
    {
        path: 'payment-method-type-popup-new',
        component: PaymentMethodTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentMethodType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-method-type-new',
        component: PaymentMethodTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentMethodType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-method-type/:id/edit',
        component: PaymentMethodTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentMethodType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-method-type/:route/:page/:id/edit',
        component: PaymentMethodTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentMethodType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-method-type/:id/popup-edit',
        component: PaymentMethodTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.paymentMethodType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
