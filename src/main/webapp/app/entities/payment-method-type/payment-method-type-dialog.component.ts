import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentMethodType } from './payment-method-type.model';
import { PaymentMethodTypePopupService } from './payment-method-type-popup.service';
import { PaymentMethodTypeService } from './payment-method-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-payment-method-type-dialog',
    templateUrl: './payment-method-type-dialog.component.html'
})
export class PaymentMethodTypeDialogComponent implements OnInit {

    paymentMethodType: PaymentMethodType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected paymentMethodTypeService: PaymentMethodTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentMethodType.idPaymentMethodType !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentMethodTypeService.update(this.paymentMethodType));
        } else {
            this.subscribeToSaveResponse(
                this.paymentMethodTypeService.create(this.paymentMethodType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<PaymentMethodType>) {
        result.subscribe((res: PaymentMethodType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: PaymentMethodType) {
        this.eventManager.broadcast({ name: 'paymentMethodTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'paymentMethodType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-payment-method-type-popup',
    template: ''
})
export class PaymentMethodTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected paymentMethodTypePopupService: PaymentMethodTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentMethodTypePopupService
                    .open(PaymentMethodTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.paymentMethodTypePopupService.parent = params['parent'];
                this.paymentMethodTypePopupService
                    .open(PaymentMethodTypeDialogComponent as Component);
            } else {
                this.paymentMethodTypePopupService
                    .open(PaymentMethodTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
