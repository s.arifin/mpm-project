import { BaseEntity } from './../../shared';

export class Party implements BaseEntity {
    constructor(
        public id?: any,
        public idParty?: any,
        public idType?: number,
        public name?: any,
        public categories?: any,
        public facilities?: any,
        public contactPurposes?: any,
    ) {
    }
}
