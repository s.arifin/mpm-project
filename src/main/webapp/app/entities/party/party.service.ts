import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Party } from './party.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class PartyService {
    protected itemValues: Party[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/parties';
    protected resourceSearchUrl = 'api/_search/parties';

    constructor(protected http: Http) { }

    create(party: Party): Observable<Party> {
        const copy = this.convert(party);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(party: Party): Observable<Party> {
        const copy = this.convert(party);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<Party> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, party: Party): Observable<Party> {
        const copy = this.convert(party);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, partys: Party[]): Observable<Party[]> {
        const copy = this.convertList(partys);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(party: Party): Party {
        if (party === null || party === {}) {
            return {};
        }
        // const copy: Party = Object.assign({}, party);
        const copy: Party = JSON.parse(JSON.stringify(party));
        return copy;
    }

    protected convertList(partys: Party[]): Party[] {
        const copy: Party[] = partys;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Party[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
