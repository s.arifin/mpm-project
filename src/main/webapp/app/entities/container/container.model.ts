import { BaseEntity } from './../../shared';

export class Container implements BaseEntity {
    constructor(
        public id?: any,
        public idContainer?: any,
        public containerCode?: string,
        public description?: string,
        public facilityId?: any,
        public containerTypeId?: any,
        public internalId?: any,
    ) {
    }
}

export class ContainerNet implements BaseEntity {
    constructor(
        public id?: any,
        public idContainer?: string,
        public code?: string,
        public description?: string,
        public idOwner?: string,
        public name?: string,
    ) {
    }
}
