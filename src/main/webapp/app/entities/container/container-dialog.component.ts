import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Container } from './container.model';
import { ContainerPopupService } from './container-popup.service';
import { ContainerService } from './container.service';
import { ToasterService, Principal } from '../../shared';
import { Facility, FacilityService } from '../facility';
import { ContainerType, ContainerTypeService } from '../container-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-container-dialog',
    templateUrl: './container-dialog.component.html'
})
export class ContainerDialogComponent implements OnInit {

    container: Container;
    isSaving: boolean;
    idFacility: any;
    idContainerType: any;

    facilities: Facility[];

    containertypes: ContainerType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected containerService: ContainerService,
        protected facilityService: FacilityService,
        protected principal: Principal,
        protected containerTypeService: ContainerTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.facilityService.getAll(this.principal.getIdInternal())
            .subscribe((res: ResponseWrapper) => { this.facilities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.containerTypeService.getAll()
            .subscribe((res: ResponseWrapper) => { this.containertypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.container.internalId = this.principal.getIdInternal();
        if (this.container.idContainer !== undefined) {
            this.subscribeToSaveResponse(
                this.containerService.update(this.container));
        } else {
            this.subscribeToSaveResponse(
                this.containerService.create(this.container));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Container>) {
        result.subscribe((res: Container) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: Container) {
        this.eventManager.broadcast({ name: 'containerListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'container saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'container Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFacilityById(index: number, item: Facility) {
        return item.idFacility;
    }

    trackContainerTypeById(index: number, item: ContainerType) {
        return item.idContainerType;
    }
}

@Component({
    selector: 'jhi-container-popup',
    template: ''
})
export class ContainerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected containerPopupService: ContainerPopupService
    ) {}

    ngOnInit() {
        this.containerPopupService.idFacility = undefined;
        this.containerPopupService.idContainerType = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.containerPopupService
                    .open(ContainerDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                this.containerPopupService.idFacility = params['parent'];
                this.containerPopupService
                    .open(ContainerDialogComponent as Component);
            } else {
                this.containerPopupService
                    .open(ContainerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
