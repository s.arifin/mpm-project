import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Container } from './container.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ContainerService {
    protected itemValues: Container[];
    values: Subject<any> = new Subject<any>();

    //    protected resourceUrl =  process.env.API_C_URL + 'api/containers';
    protected resourceUrl = process.env.API_C_URL + '/api/containers';
    protected resourceSearchUrl = process.env.API_C_URL + '/api/_search/containers';

    constructor(protected http: Http) { }

    create(container: Container): Observable<Container> {
        const copy = this.convert(container);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(container: Container): Observable<Container> {
        const copy = this.convert(container);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<Container> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    getByFacilityDropdown(id?: any): Observable<ResponseWrapper> { // for stockopname unit
        const options = createRequestOption();
        return this.http.get(this.resourceUrl + '/by-idfacility-dropdown/' + id, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getByFacility(id?: any): Observable<ResponseWrapper> {
        const options = createRequestOption();
        return this.http.get(this.resourceUrl + '/by-idfacility/' + id, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(item?: Container, req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, item, options);
    }

    executeListProcess(items?: Container[], req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
        * Convert a returned JSON object to Container.
        */
    protected convertItemFromServer(json: any): Container {
        const entity: Container = Object.assign(new Container(), json);
        return entity;
    }

    /**
        * Convert a Container to a JSON which can be sent to the server.
        */
    protected convert(container: Container): Container {
        if (container === null || container === {}) {
            return {};
        }
        // const copy: Container = Object.assign({}, container);
        const copy: Container = JSON.parse(JSON.stringify(container));
        return copy;
    }

    protected convertList(containers: Container[]): Container[] {
        const copy: Container[] = containers;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: Container[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }

    public getContainerByFacilityId(id: string): Observable<ResponseWrapper> {
        return this.http.get(this.resourceUrl + '/get-container-by-facility-id/' + id)
            .map((res: Response) => this.convertResponse(res));
    }
}
