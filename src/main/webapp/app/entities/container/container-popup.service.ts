import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Container } from './container.model';
import { ContainerService } from './container.service';

@Injectable()
export class ContainerPopupService {
    protected ngbModalRef: NgbModalRef;
    idFacility: any;
    idContainerType: any;

    constructor(
        protected modalService: NgbModal,
        protected router: Router,
        protected containerService: ContainerService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.containerService.find(id).subscribe((data) => {
                    this.ngbModalRef = this.containerModalRef(component, data);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const data = new Container();
                    data.facilityId = this.idFacility;
                    data.containerTypeId = this.idContainerType;
                    this.ngbModalRef = this.containerModalRef(component, data);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    containerModalRef(component: Component, container: Container): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.container = container;
        modalRef.componentInstance.idFacility = this.idFacility;
        modalRef.componentInstance.idContainerType = this.idContainerType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    load(component: Component): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.containerLoadModalRef(component);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    containerLoadModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.idFacility = this.idFacility;
        modalRef.componentInstance.idContainerType = this.idContainerType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
