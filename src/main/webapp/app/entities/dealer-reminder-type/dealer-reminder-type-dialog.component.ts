import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {DealerReminderType} from './dealer-reminder-type.model';
import {DealerReminderTypePopupService} from './dealer-reminder-type-popup.service';
import {DealerReminderTypeService} from './dealer-reminder-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-dealer-reminder-type-dialog',
    templateUrl: './dealer-reminder-type-dialog.component.html'
})
export class DealerReminderTypeDialogComponent implements OnInit {

    dealerReminderType: DealerReminderType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected dealerReminderTypeService: DealerReminderTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dealerReminderType.idReminderType !== undefined) {
            this.subscribeToSaveResponse(
                this.dealerReminderTypeService.update(this.dealerReminderType));
        } else {
            this.subscribeToSaveResponse(
                this.dealerReminderTypeService.create(this.dealerReminderType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<DealerReminderType>) {
        result.subscribe((res: DealerReminderType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: DealerReminderType) {
        this.eventManager.broadcast({ name: 'dealerReminderTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'dealerReminderType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'dealerReminderType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-dealer-reminder-type-popup',
    template: ''
})
export class DealerReminderTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected dealerReminderTypePopupService: DealerReminderTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dealerReminderTypePopupService
                    .open(DealerReminderTypeDialogComponent as Component, params['id']);
            } else {
                this.dealerReminderTypePopupService
                    .open(DealerReminderTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
