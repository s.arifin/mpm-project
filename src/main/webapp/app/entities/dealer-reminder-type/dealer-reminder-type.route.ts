import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DealerReminderTypeComponent } from './dealer-reminder-type.component';
import { DealerReminderTypeLovPopupComponent } from './dealer-reminder-type-as-lov.component';
import { DealerReminderTypePopupComponent } from './dealer-reminder-type-dialog.component';

@Injectable()
export class DealerReminderTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idReminderType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const dealerReminderTypeRoute: Routes = [
    {
        path: 'dealer-reminder-type',
        component: DealerReminderTypeComponent,
        resolve: {
            'pagingParams': DealerReminderTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerReminderType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dealerReminderTypePopupRoute: Routes = [
    {
        path: 'dealer-reminder-type-lov',
        component: DealerReminderTypeLovPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerReminderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dealer-reminder-type-new',
        component: DealerReminderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerReminderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dealer-reminder-type/:id/edit',
        component: DealerReminderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.dealerReminderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
