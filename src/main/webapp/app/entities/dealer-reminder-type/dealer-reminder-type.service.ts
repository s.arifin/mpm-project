import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { DealerReminderType } from './dealer-reminder-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

import * as moment from 'moment';

@Injectable()
export class DealerReminderTypeService {
    protected itemValues: DealerReminderType[];
    values: BehaviorSubject<any> = new BehaviorSubject<any>(this.itemValues);

    protected resourceUrl = 'api/dealer-reminder-types';
    protected resourceSearchUrl = 'api/_search/dealer-reminder-types';

    constructor(protected http: Http) { }

    create(dealerReminderType: DealerReminderType): Observable<DealerReminderType> {
        const copy = this.convert(dealerReminderType);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(dealerReminderType: DealerReminderType): Observable<DealerReminderType> {
        const copy = this.convert(dealerReminderType);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: any): Observable<DealerReminderType> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    executeProcess(id: Number, param: String, dealerReminderType: DealerReminderType): Observable<DealerReminderType> {
        const copy = this.convert(dealerReminderType);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, dealerReminderTypes: DealerReminderType[]): Observable<DealerReminderType[]> {
        const copy = this.convertList(dealerReminderTypes);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    protected convert(dealerReminderType: DealerReminderType): DealerReminderType {
        if (dealerReminderType === null || dealerReminderType === {}) {
            return {};
        }
        // const copy: DealerReminderType = Object.assign({}, dealerReminderType);
        const copy: DealerReminderType = JSON.parse(JSON.stringify(dealerReminderType));
        return copy;
    }

    protected convertList(dealerReminderTypes: DealerReminderType[]): DealerReminderType[] {
        const copy: DealerReminderType[] = dealerReminderTypes;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: DealerReminderType[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
