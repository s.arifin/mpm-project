import { BaseEntity } from './../../shared';

export class DealerReminderType implements BaseEntity {
    constructor(
        public id?: number,
        public idReminderType?: number,
        public description?: string,
        public messages?: string,
    ) {
    }
}
