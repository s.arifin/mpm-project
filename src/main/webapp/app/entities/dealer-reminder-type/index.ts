export * from './dealer-reminder-type.model';
export * from './dealer-reminder-type-popup.service';
export * from './dealer-reminder-type.service';
export * from './dealer-reminder-type-dialog.component';
export * from './dealer-reminder-type.component';
export * from './dealer-reminder-type.route';
export * from './dealer-reminder-type-as-lov.component';
