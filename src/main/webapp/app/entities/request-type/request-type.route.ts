import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RequestTypeComponent } from './request-type.component';
import { RequestTypePopupComponent } from './request-type-dialog.component';

@Injectable()
export class RequestTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idRequestType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const requestTypeRoute: Routes = [
    {
        path: 'request-type',
        component: RequestTypeComponent,
        resolve: {
            'pagingParams': RequestTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const requestTypePopupRoute: Routes = [
    {
        path: 'request-type-new',
        component: RequestTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'request-type/:id/edit',
        component: RequestTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.requestType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
