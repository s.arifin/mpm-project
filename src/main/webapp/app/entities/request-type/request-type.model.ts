import { BaseEntity } from './../../shared';

export class RequestType implements BaseEntity {
    constructor(
        public id?: number,
        public idRequestType?: number,
        public description?: string,
    ) {
    }
}
