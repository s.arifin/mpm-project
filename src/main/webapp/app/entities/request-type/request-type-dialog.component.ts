import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RequestType } from './request-type.model';
import { RequestTypePopupService } from './request-type-popup.service';
import { RequestTypeService } from './request-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-request-type-dialog',
    templateUrl: './request-type-dialog.component.html'
})
export class RequestTypeDialogComponent implements OnInit {

    requestType: RequestType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected requestTypeService: RequestTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.requestType.idRequestType !== undefined) {
            this.subscribeToSaveResponse(
                this.requestTypeService.update(this.requestType));
        } else {
            this.subscribeToSaveResponse(
                this.requestTypeService.create(this.requestType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RequestType>) {
        result.subscribe((res: RequestType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: RequestType) {
        this.eventManager.broadcast({ name: 'requestTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'requestType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-request-type-popup',
    template: ''
})
export class RequestTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected requestTypePopupService: RequestTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.requestTypePopupService
                    .open(RequestTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.requestTypePopupService.parent = params['parent'];
                this.requestTypePopupService
                    .open(RequestTypeDialogComponent as Component);
            } else {
                this.requestTypePopupService
                    .open(RequestTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
