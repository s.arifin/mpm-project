
import { BaseEntity } from './../../shared';

export class BillingDetailC {
    constructor(
        public idOrdIte?: any,
        public idBilling?: any,
        public idOrder?: any,
        public orderNumber?: string,
        public arAmount?: number,
        public noKa?: string,
        public noSin?: string,
        public discount?: number,
        public idCustomer?: any,
        public axUnitPrice?: number,
        public axPPN?: number,
        public axBBN?: number,
        public idMSO?: string,
        public TypeCode?: string,
        public SegmentType?: string,
        public CategoryType?: string,
        public BasePrice?: number,
        public totalTitipan?: number,
        public billingNumber?: string,
        public vendorCode?: string,
        public subsFinCom?: number,
        public ppnFinCom?: number,
        public finComRefCode?: string,
        public totalFinCom?: number,
        public creditDP?: number,
        public arTitipan?: number,
        public bankCashAccNum?: string,
        public finComParty?: string,
        public cashAccNum?: string,
        public customerName?: string
    ) {}
}
