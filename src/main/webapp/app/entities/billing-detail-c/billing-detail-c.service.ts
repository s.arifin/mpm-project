import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { SERVER_API_C_URL } from '../../app.constants';
// import { SERVER_API_URL } from '../../app.constants';

import { BillingDetailC } from './billing-detail-c.model';
import { BillTo } from '../bill-to/bill-to.model';
import { ResponseWrapper, createRequestOption } from '../../shared/index';

import * as moment from 'moment';

@Injectable()
export class BillingDetailCService {

    protected resourceUrl = process.env.API_C_URL + '/api/billing_detail/';
    // protected resourceUrl = SERVER_API_URL + 'api/billing_detail';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    findbyIdOrdIte(id: any): Observable<BillingDetailC> {

        return this.http.get(`${this.resourceUrl}byIdOrdIte/?id=${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findbyBillingNumber(id: any): Observable<BillingDetailC> {

        return this.http.get(`${this.resourceUrl}byBillingNumber/?id=${id}`).map((res: Response) => {
            return res.json();
        });
    }
}
