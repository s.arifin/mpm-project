import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {RelationType} from './relation-type.model';
import {RelationTypePopupService} from './relation-type-popup.service';
import {RelationTypeService} from './relation-type.service';
import {ToasterService} from '../../shared';

@Component({
    selector: 'jhi-relation-type-dialog',
    templateUrl: './relation-type-dialog.component.html'
})
export class RelationTypeDialogComponent implements OnInit {

    relationType: RelationType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected relationTypeService: RelationTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.relationType.idRelationType !== undefined) {
            this.subscribeToSaveResponse(
                this.relationTypeService.update(this.relationType));
        } else {
            this.subscribeToSaveResponse(
                this.relationTypeService.create(this.relationType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<RelationType>) {
        result.subscribe((res: RelationType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: RelationType) {
        this.eventManager.broadcast({ name: 'relationTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'relationType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'relationType Changed', error.message);
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-relation-type-popup',
    template: ''
})
export class RelationTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected relationTypePopupService: RelationTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.relationTypePopupService
                    .open(RelationTypeDialogComponent as Component, params['id']);
            } else {
                this.relationTypePopupService
                    .open(RelationTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
