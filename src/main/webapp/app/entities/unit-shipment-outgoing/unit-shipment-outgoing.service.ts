import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UnitShipmentOutgoing } from './unit-shipment-outgoing.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnitShipmentOutgoingService {
   protected itemValues: UnitShipmentOutgoing[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/unit-shipment-outgoings';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/unit-shipment-outgoings';

   constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

   create(unitShipmentOutgoing: UnitShipmentOutgoing): Observable<UnitShipmentOutgoing> {
       const copy = this.convert(unitShipmentOutgoing);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(unitShipmentOutgoing: UnitShipmentOutgoing): Observable<UnitShipmentOutgoing> {
       const copy = this.convert(unitShipmentOutgoing);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<UnitShipmentOutgoing> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   changeStatus(unitShipmentOutgoing: UnitShipmentOutgoing, id: Number): Observable<UnitShipmentOutgoing> {
       const copy = this.convert(unitShipmentOutgoing);
       return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
           .map((res: Response) => this.convertItemFromServer(res.json));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: UnitShipmentOutgoing, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: UnitShipmentOutgoing[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to UnitShipmentOutgoing.
    */
   protected convertItemFromServer(json: any): UnitShipmentOutgoing {
       const entity: UnitShipmentOutgoing = Object.assign(new UnitShipmentOutgoing(), json);
       if (entity.dateSchedulle) {
           entity.dateSchedulle = new Date(entity.dateSchedulle);
       }
       if (entity.dateBAST) {
           entity.dateBAST = new Date(entity.dateBAST);
       }
       return entity;
   }

   /**
    * Convert a UnitShipmentOutgoing to a JSON which can be sent to the server.
    */
   protected convert(unitShipmentOutgoing: UnitShipmentOutgoing): UnitShipmentOutgoing {
       if (unitShipmentOutgoing === null || unitShipmentOutgoing === {}) {
           return {};
       }
       // const copy: UnitShipmentOutgoing = Object.assign({}, unitShipmentOutgoing);
       const copy: UnitShipmentOutgoing = JSON.parse(JSON.stringify(unitShipmentOutgoing));

       // copy.dateSchedulle = this.dateUtils.toDate(unitShipmentOutgoing.dateSchedulle);

       // copy.dateBAST = this.dateUtils.toDate(unitShipmentOutgoing.dateBAST);
       return copy;
   }

   protected convertList(unitShipmentOutgoings: UnitShipmentOutgoing[]): UnitShipmentOutgoing[] {
       const copy: UnitShipmentOutgoing[] = unitShipmentOutgoings;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: UnitShipmentOutgoing[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
