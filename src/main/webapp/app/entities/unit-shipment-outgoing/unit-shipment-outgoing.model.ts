import { BaseEntity } from './../../shared';

export class UnitShipmentOutgoing implements BaseEntity {
    constructor(
        public id?: any,
        public idShipment?: any,
        public currentStatus?: number,
        public shipmentNumber?: string,
        public description?: string,
        public dateSchedulle?: any,
        public reason?: string,
        public dateBAST?: any,
        public note?: string,
        public otherName?: string,
        public deliveryOption?: string,
        public deliveryAddress?: string,
        public details?: any,
        public shipmentTypeId?: any,
        public shipFromId?: any,
        public shipToId?: any,
        public addressFromId?: any,
        public addressToId?: any,
        public internalId?: any,
        public internalName?: any,
    ) {
    }
}
