import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { UnitShipmentOutgoingComponent } from './unit-shipment-outgoing.component';
import { UnitShipmentOutgoingEditComponent } from './unit-shipment-outgoing-edit.component';
import { UnitShipmentOutgoingPopupComponent } from './unit-shipment-outgoing-dialog.component';

@Injectable()
export class UnitShipmentOutgoingResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idShipment,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const unitShipmentOutgoingRoute: Routes = [
    {
        path: 'unit-shipment-outgoing',
        component: UnitShipmentOutgoingComponent,
        resolve: {
            'pagingParams': UnitShipmentOutgoingResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unitShipmentOutgoingPopupRoute: Routes = [
    {
        path: 'unit-shipment-outgoing-popup-new',
        component: UnitShipmentOutgoingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unit-shipment-outgoing-new',
        component: UnitShipmentOutgoingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'unit-shipment-outgoing/:id/edit',
        component: UnitShipmentOutgoingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'unit-shipment-outgoing/:route/:page/:id/edit',
        component: UnitShipmentOutgoingEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'unit-shipment-outgoing/:id/popup-edit',
        component: UnitShipmentOutgoingPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.unitShipmentOutgoing.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
