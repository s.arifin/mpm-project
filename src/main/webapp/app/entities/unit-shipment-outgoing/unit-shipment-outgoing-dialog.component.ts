import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitShipmentOutgoing } from './unit-shipment-outgoing.model';
import { UnitShipmentOutgoingPopupService } from './unit-shipment-outgoing-popup.service';
import { UnitShipmentOutgoingService } from './unit-shipment-outgoing.service';
import { ToasterService } from '../../shared';
import { ShipmentType, ShipmentTypeService } from '../shipment-type';
import { ShipTo, ShipToService } from '../ship-to';
import { PostalAddress, PostalAddressService } from '../postal-address';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unit-shipment-outgoing-dialog',
    templateUrl: './unit-shipment-outgoing-dialog.component.html'
})
export class UnitShipmentOutgoingDialogComponent implements OnInit {

    unitShipmentOutgoing: UnitShipmentOutgoing;
    isSaving: boolean;
    idShipmentType: any;
    idShipFrom: any;
    idShipTo: any;
    idAddressFrom: any;
    idAddressTo: any;

    shipmenttypes: ShipmentType[];

    shiptos: ShipTo[];

    postaladdresses: PostalAddress[];

    constructor(
        public activeModal: NgbActiveModal,
        protected jhiAlertService: JhiAlertService,
        protected unitShipmentOutgoingService: UnitShipmentOutgoingService,
        protected shipmentTypeService: ShipmentTypeService,
        protected shipToService: ShipToService,
        protected postalAddressService: PostalAddressService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.shipmentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.postalAddressService.query()
            .subscribe((res: ResponseWrapper) => { this.postaladdresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unitShipmentOutgoing.idShipment !== undefined) {
            this.subscribeToSaveResponse(
                this.unitShipmentOutgoingService.update(this.unitShipmentOutgoing));
        } else {
            this.subscribeToSaveResponse(
                this.unitShipmentOutgoingService.create(this.unitShipmentOutgoing));
        }
    }

    protected subscribeToSaveResponse(result: Observable<UnitShipmentOutgoing>) {
        result.subscribe((res: UnitShipmentOutgoing) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: UnitShipmentOutgoing) {
        this.eventManager.broadcast({ name: 'unitShipmentOutgoingListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'unitShipmentOutgoing saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(error: any) {
        this.toaster.showToaster('warning', 'unitShipmentOutgoing Changed', error.message);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShipmentTypeById(index: number, item: ShipmentType) {
        return item.idShipmentType;
    }

    trackShipToById(index: number, item: ShipTo) {
        return item.idShipTo;
    }

    trackPostalAddressById(index: number, item: PostalAddress) {
        return item.idContact;
    }
}

@Component({
    selector: 'jhi-unit-shipment-outgoing-popup',
    template: ''
})
export class UnitShipmentOutgoingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected unitShipmentOutgoingPopupService: UnitShipmentOutgoingPopupService
    ) {}

    ngOnInit() {
        this.unitShipmentOutgoingPopupService.idShipmentType = undefined;
        this.unitShipmentOutgoingPopupService.idShipFrom = undefined;
        this.unitShipmentOutgoingPopupService.idShipTo = undefined;
        this.unitShipmentOutgoingPopupService.idAddressFrom = undefined;
        this.unitShipmentOutgoingPopupService.idAddressTo = undefined;

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unitShipmentOutgoingPopupService
                    .open(UnitShipmentOutgoingDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.unitShipmentOutgoingPopupService.parent = params['parent'];
                this.unitShipmentOutgoingPopupService
                    .open(UnitShipmentOutgoingDialogComponent as Component);
            } else {
                this.unitShipmentOutgoingPopupService
                    .open(UnitShipmentOutgoingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
