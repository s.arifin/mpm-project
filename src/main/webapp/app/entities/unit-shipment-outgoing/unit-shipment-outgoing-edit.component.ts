import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnitShipmentOutgoing } from './unit-shipment-outgoing.model';
import { UnitShipmentOutgoingService } from './unit-shipment-outgoing.service';
import { ToasterService} from '../../shared';
import { ReportUtilService} from '../../shared/report/report-util.service';
import { ShipmentType, ShipmentTypeService } from '../shipment-type';
import { ShipTo, ShipToService } from '../ship-to';
import { PostalAddress, PostalAddressService } from '../postal-address';
import { ResponseWrapper } from '../../shared';
import { ConfirmationService } from 'primeng/primeng';

@Component({
   selector: 'jhi-unit-shipment-outgoing-edit',
   templateUrl: './unit-shipment-outgoing-edit.component.html'
})
export class UnitShipmentOutgoingEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   unitShipmentOutgoing: UnitShipmentOutgoing;
   isSaving: boolean;
   idShipment: any;
   paramPage: number;
   routeId: number;

   shipmenttypes: ShipmentType[];

   shiptos: ShipTo[];

   postaladdresses: PostalAddress[];

   constructor(
       protected alertService: JhiAlertService,
       protected unitShipmentOutgoingService: UnitShipmentOutgoingService,
       protected shipmentTypeService: ShipmentTypeService,
       protected shipToService: ShipToService,
       protected postalAddressService: PostalAddressService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected reportUtilService: ReportUtilService,
       protected toaster: ToasterService,
       protected confirmationService: ConfirmationService
   ) {
       this.unitShipmentOutgoing = new UnitShipmentOutgoing();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idShipment = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
       this.shipmentTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.shipmenttypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.shipToService.query()
            .subscribe((res: ResponseWrapper) => { this.shiptos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
       this.postalAddressService.query()
            .subscribe((res: ResponseWrapper) => { this.postaladdresses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.unitShipmentOutgoingService.find(this.idShipment).subscribe((unitShipmentOutgoing) => {
           this.unitShipmentOutgoing = unitShipmentOutgoing;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['unit-shipment-outgoing', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.unitShipmentOutgoing.idShipment !== undefined) {
           this.subscribeToSaveResponse(
               this.unitShipmentOutgoingService.update(this.unitShipmentOutgoing));
       } else {
           this.subscribeToSaveResponse(
               this.unitShipmentOutgoingService.create(this.unitShipmentOutgoing));
       }
   }

   protected subscribeToSaveResponse(result: Observable<UnitShipmentOutgoing>) {
       result.subscribe((res: UnitShipmentOutgoing) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: UnitShipmentOutgoing) {
       this.eventManager.broadcast({ name: 'unitShipmentOutgoingListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'unitShipmentOutgoing saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'unitShipmentOutgoing Changed', error.message);
       this.alertService.error(error.message, null, null);
   }

   trackShipmentTypeById(index: number, item: ShipmentType) {
       return item.idShipmentType;
   }

   trackShipToById(index: number, item: ShipTo) {
       return item.idShipTo;
   }

   trackPostalAddressById(index: number, item: PostalAddress) {
       return item.idContact;
   }
   print() {
       this.toaster.showToaster('info', 'Print Data', 'Printing.....');
       this.reportUtilService.viewFile('/api/report/sample/pdf');
   }

   activated() {
     this.unitShipmentOutgoingService.changeStatus(this.unitShipmentOutgoing, 11).delay(1000).subscribe((r) => {
        this.load();
        this.toaster.showToaster('info', 'Data Activated', 'Activated.....');
      });
   }

   approved() {
     this.unitShipmentOutgoingService.changeStatus(this.unitShipmentOutgoing, 12).delay(1000).subscribe((r) => {
        this.load();
        this.toaster.showToaster('info', 'Data Approved', 'Approved.....');
      });
   }

   completed() {
       this.unitShipmentOutgoingService.changeStatus(this.unitShipmentOutgoing, 17).delay(1000).subscribe((r) => {
           this.eventManager.broadcast({
               name: 'unitShipmentOutgoingListModification',
               content: 'Completed an unitShipmentOutgoing'
            });
            this.toaster.showToaster('info', 'Data Completed', 'Completed.....');
            this.previousState();
        });
   }

   canceled() {
       this.confirmationService.confirm({
           message: 'Are you sure that you want to cancel?',
           header: 'Confirmation',
           icon: 'fa fa-question-circle',
           accept: () => {
               this.unitShipmentOutgoingService.changeStatus(this.unitShipmentOutgoing, 13).delay(1000).subscribe((r) => {
                   this.eventManager.broadcast({
                       name: 'unitShipmentOutgoingListModification',
                       content: 'Cancel an unitShipmentOutgoing'
                    });
                    this.toaster.showToaster('info', 'Data unitShipmentOutgoing cancel', 'Cancel unitShipmentOutgoing.....');
                    this.previousState();
                });
            }
        });
   }

}
