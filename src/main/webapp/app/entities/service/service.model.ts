import { BaseEntity } from './../shared-component';
import { Product } from './../product';

export class Service extends Product {
    constructor(
        public id?: any,
        public idProduct?: any,
        public name?: string,
        public dateIntroduction?: any,
        public uomId?: any,
        public features?: any,
        public categories?: any,
        public weTypeId?: any,
    ) {
        super();
    }
}
