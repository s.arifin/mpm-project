import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService} from 'ng-jhipster';

import {Service} from './service.model';
import {ServicePopupService} from './service-popup.service';
import {ServiceService} from './service.service';
import {ToasterService} from '../../shared';
import { Uom, UomService } from '../uom';
import { Feature, FeatureService } from '../feature';
import { ProductCategory, ProductCategoryService } from '../product-category';
import { WeServiceType, WeServiceTypeService } from '../we-service-type';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-service-dialog',
    templateUrl: './service-dialog.component.html'
})
export class ServiceDialogComponent implements OnInit {

    service: Service;
    isSaving: boolean;

    uoms: Uom[];

    features: Feature[];

    productcategories: ProductCategory[];

    wetypes: WeServiceType[];

    constructor(
        public activeModal: NgbActiveModal,
        protected alertService: JhiAlertService,
        protected serviceService: ServiceService,
        protected uomService: UomService,
        protected featureService: FeatureService,
        protected productCategoryService: ProductCategoryService,
        protected weServiceTypeService: WeServiceTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.uomService.query()
            .subscribe((res: ResponseWrapper) => { this.uoms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.featureService.query()
            .subscribe((res: ResponseWrapper) => { this.features = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.productCategoryService.query()
            .subscribe((res: ResponseWrapper) => { this.productcategories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.weServiceTypeService
            .query({filter: 'service-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.service.weTypeId) {
                    this.wetypes = res.json;
                } else {
                    this.weServiceTypeService
                        .find(this.service.weTypeId)
                        .subscribe((subRes: WeServiceType) => {
                            this.wetypes = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.service.idProduct !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceService.update(this.service));
        } else {
            this.subscribeToSaveResponse(
                this.serviceService.create(this.service));
        }
    }

    protected subscribeToSaveResponse(result: Observable<Service>) {
        result.subscribe((res: Service) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    protected onSaveSuccess(result: Service) {
        this.eventManager.broadcast({ name: 'serviceListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'service saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    protected onError(error) {
        this.toaster.showToaster('warning', 'service Changed', error.message);
        this.alertService.error(error.message, null, null);
    }

    trackUomById(index: number, item: Uom) {
        return item.idUom;
    }

    trackFeatureById(index: number, item: Feature) {
        return item.idFeature;
    }

    trackProductCategoryById(index: number, item: ProductCategory) {
        return item.idCategory;
    }

    trackWeServiceTypeById(index: number, item: WeServiceType) {
        return item.idWeType;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-service-popup',
    template: ''
})
export class ServicePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected servicePopupService: ServicePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.servicePopupService
                    .open(ServiceDialogComponent as Component, params['id']);
            } else {
                this.servicePopupService
                    .open(ServiceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
