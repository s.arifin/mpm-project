import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { FeatureType } from './feature-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FeatureTypeService {
   protected itemValues: FeatureType[];
   values: Subject<any> = new Subject<any>();

   protected resourceUrl =  SERVER_API_URL + 'api/feature-types';
   protected resourceSearchUrl = SERVER_API_URL + 'api/_search/feature-types';

   constructor(protected http: Http) { }

   create(featureType: FeatureType): Observable<FeatureType> {
       const copy = this.convert(featureType);
       return this.http.post(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   update(featureType: FeatureType): Observable<FeatureType> {
       const copy = this.convert(featureType);
       return this.http.put(this.resourceUrl, copy).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   find(id: any): Observable<FeatureType> {
       return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
           const jsonResponse = res.json();
           return this.convertItemFromServer(jsonResponse);
       });
   }

   query(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl, options)
           .map((res: Response) => this.convertResponse(res));
   }

   queryFilterBy(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceUrl + '/filterBy', options)
           .map((res: Response) => this.convertResponse(res));
   }

   delete(id?: any): Observable<Response> {
       return this.http.delete(`${this.resourceUrl}/${id}`);
   }

   process(params?: any, req?: any): Observable<any> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
           return res.json();
       });
   }

   executeProcess(item?: FeatureType, req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute`, item, options);
   }

   executeListProcess(items?: FeatureType[], req?: any): Observable<Response> {
       const options = createRequestOption(req);
       return this.http.post(`${this.resourceUrl}/execute-list`, items, options);
   }

   search(req?: any): Observable<ResponseWrapper> {
       const options = createRequestOption(req);
       return this.http.get(this.resourceSearchUrl, options)
           .map((res: any) => this.convertResponse(res));
   }

   protected convertResponse(res: Response): ResponseWrapper {
       const jsonResponse = res.json();
       const result = [];
       for (let i = 0; i < jsonResponse.length; i++) {
           result.push(this.convertItemFromServer(jsonResponse[i]));
       }
       return new ResponseWrapper(res.headers, result, res.status);
   }

   /**
    * Convert a returned JSON object to FeatureType.
    */
   protected convertItemFromServer(json: any): FeatureType {
       const entity: FeatureType = Object.assign(new FeatureType(), json);
       return entity;
   }

   /**
    * Convert a FeatureType to a JSON which can be sent to the server.
    */
   protected convert(featureType: FeatureType): FeatureType {
       if (featureType === null || featureType === {}) {
           return {};
       }
       // const copy: FeatureType = Object.assign({}, featureType);
       const copy: FeatureType = JSON.parse(JSON.stringify(featureType));
       return copy;
   }

   protected convertList(featureTypes: FeatureType[]): FeatureType[] {
       const copy: FeatureType[] = featureTypes;
       copy.forEach((item) => {
           item = this.convert(item);
       });
       return copy;
   }

   pushItems(data: FeatureType[]) {
       this.itemValues = data;
       this.values.next(this.itemValues);
   }
}
