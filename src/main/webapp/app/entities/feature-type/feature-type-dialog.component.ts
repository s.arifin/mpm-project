import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { FeatureType } from './feature-type.model';
import { FeatureTypePopupService } from './feature-type-popup.service';
import { FeatureTypeService } from './feature-type.service';
import { ToasterService } from '../../shared';

@Component({
    selector: 'jhi-feature-type-dialog',
    templateUrl: './feature-type-dialog.component.html'
})
export class FeatureTypeDialogComponent implements OnInit {

    featureType: FeatureType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected featureTypeService: FeatureTypeService,
        protected eventManager: JhiEventManager,
        protected toaster: ToasterService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.featureType.idFeatureType !== undefined) {
            this.subscribeToSaveResponse(
                this.featureTypeService.update(this.featureType));
        } else {
            this.subscribeToSaveResponse(
                this.featureTypeService.create(this.featureType));
        }
    }

    protected subscribeToSaveResponse(result: Observable<FeatureType>) {
        result.subscribe((res: FeatureType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    protected onSaveSuccess(result: FeatureType) {
        this.eventManager.broadcast({ name: 'featureTypeListModification', content: 'OK'});
        this.toaster.showToaster('info', 'Save', 'featureType saved !');
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-feature-type-popup',
    template: ''
})
export class FeatureTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        protected route: ActivatedRoute,
        protected featureTypePopupService: FeatureTypePopupService
    ) {}

    ngOnInit() {

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.featureTypePopupService
                    .open(FeatureTypeDialogComponent as Component, params['id']);
            } else if ( params['parent'] ) {
                // this.featureTypePopupService.parent = params['parent'];
                this.featureTypePopupService
                    .open(FeatureTypeDialogComponent as Component);
            } else {
                this.featureTypePopupService
                    .open(FeatureTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
