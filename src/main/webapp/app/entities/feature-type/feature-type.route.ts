import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { FeatureTypeComponent } from './feature-type.component';
import { FeatureTypeEditComponent } from './feature-type-edit.component';
import { FeatureTypePopupComponent } from './feature-type-dialog.component';

@Injectable()
export class FeatureTypeResolvePagingParams implements Resolve<any> {

    constructor(protected paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'idFeatureType,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const featureTypeRoute: Routes = [
    {
        path: 'feature-type',
        component: FeatureTypeComponent,
        resolve: {
            'pagingParams': FeatureTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const featureTypePopupRoute: Routes = [
    {
        path: 'feature-type-popup-new',
        component: FeatureTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'feature-type-new',
        component: FeatureTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'feature-type/:id/edit',
        component: FeatureTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'feature-type/:route/:page/:id/edit',
        component: FeatureTypeEditComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'feature-type/:id/popup-edit',
        component: FeatureTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.featureType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
