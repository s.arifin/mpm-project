import { BaseEntity } from './../../shared';

export class FeatureType implements BaseEntity {
    constructor(
        public id?: number,
        public idFeatureType?: number,
        public description?: string,
        public refKey?: string,
    ) {
    }
}
