import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FeatureType } from './feature-type.model';
import { FeatureTypeService } from './feature-type.service';
import { ToasterService} from '../../shared';

@Component({
   selector: 'jhi-feature-type-edit',
   templateUrl: './feature-type-edit.component.html'
})
export class FeatureTypeEditComponent implements OnInit, OnDestroy {

   protected subscription: Subscription;
   featureType: FeatureType;
   isSaving: boolean;
   idFeatureType: any;
   paramPage: number;
   routeId: number;

   constructor(
       protected alertService: JhiAlertService,
       protected featureTypeService: FeatureTypeService,
       protected route: ActivatedRoute,
       protected router: Router,
       protected eventManager: JhiEventManager,
       protected toaster: ToasterService
   ) {
       this.featureType = new FeatureType();
       this.routeId = 0;
   }

   ngOnInit() {
       this.subscription = this.route.params.subscribe((params) => {
           if (params['id']) {
               this.idFeatureType = params['id'];
               this.load();
           }
           if (params['page']) {
               this.paramPage = params['page'];
           }
           if (params['route']) {
               this.routeId = Number(params['route']);
           }
       });
       this.isSaving = false;
   }

   ngOnDestroy() {
       this.subscription.unsubscribe();
   }

   load() {
       this.featureTypeService.find(this.idFeatureType).subscribe((featureType) => {
           this.featureType = featureType;
       });
   }

   previousState() {
       if (this.routeId === 0) {
           this.router.navigate(['feature-type', { page: this.paramPage }]);
       }
   }

   save() {
       this.isSaving = true;
       if (this.featureType.idFeatureType !== undefined) {
           this.subscribeToSaveResponse(
               this.featureTypeService.update(this.featureType));
       } else {
           this.subscribeToSaveResponse(
               this.featureTypeService.create(this.featureType));
       }
   }

   protected subscribeToSaveResponse(result: Observable<FeatureType>) {
       result.subscribe((res: FeatureType) =>
           this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
   }

   protected onSaveSuccess(result: FeatureType) {
       this.eventManager.broadcast({ name: 'featureTypeListModification', content: 'OK'});
       this.toaster.showToaster('info', 'Save', 'featureType saved !');
       this.isSaving = false;
       this.previousState();
   }

   protected onSaveError(error) {
       try {
           error.json();
       } catch (exception) {
           error.message = error.text();
       }
       this.isSaving = false;
       this.onError(error);
   }

   protected onError(error) {
       this.toaster.showToaster('warning', 'featureType Changed', error.message);
       this.alertService.error(error.message, null, null);
   }
}
