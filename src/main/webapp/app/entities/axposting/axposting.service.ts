import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import { RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { AxPosting } from './axposting.model';
import { ResponseWrapper } from '../../shared';

import { SERVER_API_C_URL } from '../../app.constants';
// import { SERVER_API_URL } from '../../app.constants';
@Injectable()
export class AxPostingService {

    // protected resourceUrl =  'http://10.10.104.127:8084/api/ax_generalledger';
    protected resourceUrl =  process.env.API_C_URL + '/api/ax_generalledger';
    // protected resourceUrl =  SERVER_API_URL + 'api/ax_generalledger';
    constructor(
        protected http: Http
    ) { }

    protected convert(axposting: AxPosting): string {
        if (axposting === null || axposting === {}) {
            return '';
        }
        return JSON.stringify(axposting);
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json(); // 8084
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    send(axposting: AxPosting): Observable<ResponseWrapper> {
        const copy = this.convert(axposting);

        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        console.log('axpostingdata : ', copy);
        return this.http.post(this.resourceUrl, copy, options).map((res: Response) => this.convertResponse(res));
    }
}
