import { BaseEntity } from './../../shared';

export class AxPosting {
    constructor(
        public AutoPosting?: string,
        public AutoSettlement?: string,
        public DataArea?: string,
        public Guid?: string,
        public JournalName?: string,
        public Name?: string,
        public TransDate?: string,
        public TransactionType?: string,
        public LedgerJournalLine?: any
    ) {
    }
}
