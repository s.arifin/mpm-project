import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { AxPostingService } from './';

@NgModule({
    providers: [
        AxPostingService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmAxPostingModule {}
