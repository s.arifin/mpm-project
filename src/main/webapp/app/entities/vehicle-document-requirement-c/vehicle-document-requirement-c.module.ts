import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { VehicleDocumentRequirementCService } from './';

@NgModule({
    providers: [
        VehicleDocumentRequirementCService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MpmVehicleDocumentRequirementCModule {}
