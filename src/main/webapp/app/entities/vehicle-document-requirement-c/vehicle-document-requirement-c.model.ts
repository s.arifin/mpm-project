import { BaseEntity } from './../../shared';

export class VDRAXDetail implements BaseEntity {
    constructor(
        public id?: any,
        public idReq?: string,
        public bbn?: number,
        public costHandling?: number,
        public profitLoss?: number,
        public idVendor?: string,
        public idInternal?: string,
        public orderNumber?: string,
        public billingNumber?: string,
        public idCustomer?: string,
        public idAccount?: string,
        public totalProfitLoss?: number,
        public totalCostHandling?: number,
        public totalProfitLossPpn?: number,
        public refNumber?: string,
    ) {}
}
