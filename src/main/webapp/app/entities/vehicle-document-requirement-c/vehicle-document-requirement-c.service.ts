import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from '../../shared/index';

import * as moment from 'moment';
import {VDRAXDetail} from './vehicle-document-requirement-c.model';
import { CustomVehicleDocumentRequirement, CustomVDR} from '../vehicle-document-requirement/custom-vehicle-document-requirement.model';

@Injectable()
export class VehicleDocumentRequirementCService {

    protected resourceUrl = process.env.API_C_URL + '/api/vehicle_document_requirement/';
    protected resourceUrlReport = process.env.API_C_URL + '/api/report';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    findbyIdReq(id: any): Observable<VDRAXDetail> {

        return this.http.get(`${this.resourceUrl}axdetail/?id=${id}`).map((res: Response) => {
            return res.json();
        });
    }

    findbyRefNumber(id: any): Observable<VDRAXDetail[]> {

        return this.http.get(`${this.resourceUrl}AxDetailListbyRefNumber/?id=${id}`).map((res: Response) => {
            return res.json();
        });
    }

    queryC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceUrl + '/VdrCustomDataBySales', options)
            .map((res: Response) => this.convertResponse(res));
    }

    reportType(): Observable<ResponseWrapper> {
        const options = createRequestOption();
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceUrlReport + '/type', options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    find(id: any): Observable<CustomVDR> {
        return this.http.get(`${this.resourceUrl}findCustom/?id=${id}`).map((res: Response) => {
            return res.json();
        });
    }

    bastToKonsumen(customVDR: CustomVDR): Observable<ResponseWrapper> {
        const copy = JSON.stringify(customVDR);

        const options = new RequestOptions();
        options.headers = new Headers();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceUrl}VdrVdrBastSTNKSales`, copy, options).map((res: Response) => {
            return res;
        });
    }

    queryBast(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.headers.append('Content-Type', 'application/json');
        return this.http.get(this.resourceUrl + '/VdrDataBastBySales', options)
            .map((res: Response) => this.convertResponse(res));
    }
}
