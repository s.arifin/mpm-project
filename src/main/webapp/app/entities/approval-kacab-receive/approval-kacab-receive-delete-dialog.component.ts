import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ApprovalKacabReceive } from './approval-kacab-receive.model';
import { ApprovalKacabReceivePopupService } from './approval-kacab-receive-popup.service';
import { ApprovalKacabReceiveService } from './approval-kacab-receive.service';

@Component({
    selector: 'jhi-approval-kacab-receive-delete-dialog',
    templateUrl: './approval-kacab-receive-delete-dialog.component.html'
})
export class ApprovalKacabReceiveDeleteDialogComponent {

    approvalKacabReceive: ApprovalKacabReceive;

    constructor(
        private approvalKacabReceiveService: ApprovalKacabReceiveService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.approvalKacabReceiveService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'approvalKacabReceiveListModification',
                content: 'Deleted an approvalKacabReceive'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-approval-kacab-receive-delete-popup',
    template: ''
})
export class ApprovalKacabReceiveDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private approvalKacabReceivePopupService: ApprovalKacabReceivePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.approvalKacabReceivePopupService
                .open(ApprovalKacabReceiveDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
