import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ApprovalKacabReceive } from './approval-kacab-receive.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { UnitDocumentMessage } from '../unit-document-message/unit-document-message.model';

@Injectable()
export class ApprovalKacabReceiveService {

    private resourceUrl = SERVER_API_URL + 'api/approval-kacab-receives';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/approval-kacab-receives';
    protected resourceCUrl = process.env.API_C_URL + '/api/requestNote';
    protected dummy = 'http://localhost:52374/api/requestNote';
    protected resourceAXUrl = process.env.API_C_URL + '/api/ax_generalledger';

    valueUnitDocumentMessage = {};
    protected enheritancedata = new BehaviorSubject<any>(this.valueUnitDocumentMessage);
    passingData = this.enheritancedata.asObservable();
    constructor(private http: Http) { }

    create(approvalKacabReceive: ApprovalKacabReceive): Observable<ApprovalKacabReceive> {
        const copy = this.convert(approvalKacabReceive);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(approvalKacabReceive: ApprovalKacabReceive): Observable<ApprovalKacabReceive> {
        const copy = this.convert(approvalKacabReceive);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ApprovalKacabReceive> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryFilterByinternalC(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/Recive`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryDetail(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/receiptDetail`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryDetailPos(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/receiptDetailPos`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryDetailEvent(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/receiptDetailEvent`, options)
            .map((res: Response) => this.convertResponse(res));
    }
    queryApprove(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/acceptReceive`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryReject(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        console.log('logg', options);
        // options.headers.append('Content-Type', 'application/json');
        return this.http.get(`${this.resourceCUrl}/rejectReceive`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceCUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(approvalKacabReceive: ApprovalKacabReceive): ApprovalKacabReceive {
        const copy: ApprovalKacabReceive = Object.assign({}, approvalKacabReceive);
        return copy;
    }

    passingCustomData(custom) {
        this.enheritancedata.next(custom);
    }
    // createRequestnote(unitDocumentMessages: any): Observable<ResponseWrapper> {
    //     const copy = JSON.stringify(unitDocumentMessages);
    //     const options: BaseRequestOptions = new BaseRequestOptions();
    //     // const params: URLSearchParams = new URLSearchParams();
    //     // params.set('query', copy);
    //     options.headers = new Headers;
    //     options.headers.append('Content-Type', 'application/json');

    //     return this.http.post(`${this.resourceCUrl + '/createRequestNote'}`, copy, options).map((res: Response) => {
    //         return res.json();
    //     });
    // }

    executeListProcessAccept(unitDocumentMessage: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessage);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/acceptRequest`, copy, options)
        .map((res: Response) => this.convertResponse(res));
    }
    executeListProcessReject(unitDocumentMessage: any): Observable<ResponseWrapper> {
        const copy = JSON.stringify(unitDocumentMessage);
        const options: BaseRequestOptions = new BaseRequestOptions();
        options.headers.append('Content-Type', 'application/json');

        return this.http.post(`${this.resourceCUrl}/rejectRequest`, copy, options).map((res: Response) => {
            return res.json();
        });
    }
    protected convertList(unitDocumentMessage: UnitDocumentMessage[]): UnitDocumentMessage[] {
        const copy: UnitDocumentMessage[] = unitDocumentMessage;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    GLInventoryTransfer(idreqnot: string): Observable<any> {
        return this.http.get(this.resourceAXUrl + '/ax_generalledger/GLInventoryTransfer?idreqnot=' + idreqnot).map((res: Response) => {
            return res.json();
        });
    }
}
