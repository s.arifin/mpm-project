import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ApprovalKacabReceive } from './approval-kacab-receive.model';
import { ApprovalKacabReceiveService } from './approval-kacab-receive.service';

@Injectable()
export class ApprovalKacabReceivePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private approvalKacabReceiveService: ApprovalKacabReceiveService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.approvalKacabReceiveService.find(id).subscribe((approvalKacabReceive) => {
                    this.ngbModalRef = this.approvalKacabReceiveModalRef(component, approvalKacabReceive);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.approvalKacabReceiveModalRef(component, new ApprovalKacabReceive());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    approvalKacabReceiveModalRef(component: Component, approvalKacabReceive: ApprovalKacabReceive): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.approvalKacabReceive = approvalKacabReceive;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
