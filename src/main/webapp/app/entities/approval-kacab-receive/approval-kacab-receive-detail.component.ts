import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiPaginationUtil, JhiAlertService } from 'ng-jhipster';

import { LazyLoadEvent, ConfirmationService, SpinnerModule } from 'primeng/primeng';
import { ApprovalKacabReceive } from './approval-kacab-receive.model';
import { ApprovalKacabReceiveNotif } from './approval-kacab-receive-notif.model';
import { ApprovalKacabReceiveService } from './approval-kacab-receive.service';
import { TransferUnitRequestService } from '../transfer-unit-request/transfer-unit-request.service';
import { UnitDocumentMessage } from '../unit-document-message/unit-document-message.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, CommonUtilService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { LoadingService } from '../../layouts';
import { not } from '@angular/compiler/src/output/output_ast';
import { ToasterService } from '../../shared/alert/toaster.service';

@Component({
    selector: 'jhi-approval-kacab-receive-detail',
    templateUrl: './approval-kacab-receive-detail.component.html'
})
export class ApprovalKacabReceiveDetailComponent implements OnInit, OnDestroy {

    approvalKacabReceive: ApprovalKacabReceive;
    notif: ApprovalKacabReceiveNotif;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public unitDocumentMessage: UnitDocumentMessage[];
    unitDocumentMessagePassing: UnitDocumentMessage;
    predicate: any;
    previousPage: any;
    reverse: any;
    page: any;
    itemsPerPage: any;
    routeData: any;
    currentAccount: any;
    currentSearch: string;
    totalItems: any;
    queryCount: any;
    isLazyLoadingFirst: Boolean = false;
    idReqNot: any;

    constructor(
        private eventManager: JhiEventManager,
        private approvalKacabReceiveService: ApprovalKacabReceiveService,
        private transferUnitRequestService: TransferUnitRequestService,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private paginationUtil: JhiPaginationUtil,
        private paginationConfig: PaginationConfig,
        protected confirmationService: ConfirmationService,
        protected loadingService: LoadingService,
        protected commonUtilService: CommonUtilService,
        private alertService: JhiAlertService,
        private router: Router,
        private toasterService: ToasterService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        this.unitDocumentMessagePassing = new UnitDocumentMessage;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.approvalKacabReceiveService.passingData.subscribe( (dataPassing) => {
                console.log('data passing:', dataPassing);
                this.unitDocumentMessagePassing = dataPassing;
                this.idReqNot = dataPassing.idreqnot;
            });
            this.loadAll();
        });
        this.registerChangeInApprovalKacabReceives();
    }

    loadAll() {
        if ( this.unitDocumentMessagePassing.IdReqnotType === 31 ) {
            this.approvalKacabReceiveService.queryDetail({
                page: this.page - 1,
                query: 'idReqNot:' + this.idReqNot,
                internals: 'idInternal:' + this.principal.getIdInternal(),
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else if ( this.unitDocumentMessagePassing.IdReqnotType === 32 ) {
            this.approvalKacabReceiveService.queryDetailPos({
                page: this.page - 1,
                query: 'idReqNot:' + this.idReqNot,
                internals: 'idInternal:' + this.principal.getIdInternal(),
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        } else if ( this.unitDocumentMessagePassing.IdReqnotType === 33 ) {
            this.approvalKacabReceiveService.queryDetailEvent({
                page: this.page - 1,
                query: 'idReqNot:' + this.idReqNot,
                internals: 'idInternal:' + this.principal.getIdInternal(),
                size: this.itemsPerPage,
                sort: this.sort()}).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
        }
    }
    valuechange($event) {

    }
    transition() {
        this.router.navigate(['/approval-kacab-receive'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/approval-kacab-receive', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/approval-kacab-receive', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.unitDocumentMessage = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    previousState() {
        this.router.navigate(['approval-kacab-receive']);
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
        // this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInApprovalKacabReceives() {
        this.eventSubscriber = this.eventManager.subscribe('approvalKacabReceiveListModification', (response) => this.loadAll());
    }
    loadDataLazy(event: LazyLoadEvent) {
        if (this.isLazyLoadingFirst === false) {
            this.isLazyLoadingFirst = true;
            return ;
        }
        this.itemsPerPage = event.rows;
        this.page = Math.ceil(event.first / this.itemsPerPage) + 1;

        if (event.sortField !== undefined) {
            this.predicate = event.sortField;
            this.reverse = event.sortOrder;
        }
        this.loadAll();
    }

    gotoDetail(rowData)  {
        console.log('datadriverdetail', rowData);
        this.approvalKacabReceiveService.passingCustomData(rowData);
    }

    public confirmApprove(): void {
        this.confirmationService.confirm({
            header : 'Confirmation',
            message : 'Do you want to submit this <b>Approval</b>?',
            accept: () => {
                this.approvalKacabReceiveService.queryApprove({
                    query: 'idReqNot:' + this.idReqNot,
                }).subscribe(
                    (res) => {
                        this.previousState();
                    }
                );
            }
        });
    }

    public submit(type: string): void {
        if (type === 'approve') {
            this.confirmationService.confirm({
                header : 'Konfirmasi',
                message : 'Apakah Anda Yakin Approve Note?',
                accept: () => {
                    this.loadingService.loadingStart();
                    const obj = {
                        Items: this.unitDocumentMessage,
                        createdby: this.principal.getUserLogin()
                    }
                    if (this.unitDocumentMessagePassing.IdReqnotType === 31) {
                        this.approvalKacabReceiveService.executeListProcessAccept(obj).subscribe(
                            (res: ResponseWrapper) => {
                                this.notif = res.json;
                                this.unitDocumentMessage = null;
                                this.unitDocumentMessage = this.notif.Items;
                                console.log('notif :' + res.json )
                                if (this.notif.Sukses === true) {
                                    // this.approvalKacabReceiveService.GLInventoryTransfer(this.idReqNot).subscribe(
                                    //     (resGL) => { },
                                    //     (err) => this.commonUtilService.showError(err)
                                    // );
                                    this.toasterService.showToaster('success', 'Sucess', this.notif.Message);
                                    this.previousState();
                                } else {
                                    this.toasterService.showToaster('error', 'Failed', this.notif.Message);
                                }
                                this.loadingService.loadingStop();
                            },
                            (err) => {
                                this.loadingService.loadingStop();
                                this.commonUtilService.showError(err);
                            }
                        )
                    } else if (this.unitDocumentMessagePassing.IdReqnotType === 32) {
                        this.transferUnitRequestService.executeListProcessAcceptPOS(obj).subscribe(
                            (res: ResponseWrapper) => {
                                this.notif = res.json;
                                this.unitDocumentMessage = null;
                                this.unitDocumentMessage = this.notif.Items;
                                console.log('notif :' + res.json )
                                if (this.notif.Sukses === true) {
                                    // this.approvalKacabReceiveService.GLInventoryTransfer(this.idReqNot).subscribe(
                                    //     (resGL) => { },
                                    //     (err) => this.commonUtilService.showError(err)
                                    // );
                                    this.toasterService.showToaster('success', 'Sucess', this.notif.Message);
                                    this.previousState();
                                } else {
                                    this.toasterService.showToaster('error', 'Failed', this.notif.Message);
                                }
                                this.loadingService.loadingStop();
                            },
                            (err) => {
                                this.loadingService.loadingStop();
                                this.commonUtilService.showError(err);
                            }
                        )
                    } else if (this.unitDocumentMessagePassing.IdReqnotType === 33) {
                        this.transferUnitRequestService.executeListProcessAcceptEvent(obj).subscribe(
                            (res: ResponseWrapper) => {
                                this.notif = res.json;
                                this.unitDocumentMessage = null;
                                this.unitDocumentMessage = this.notif.Items;
                                console.log('notif :' + res.json )
                                if (this.notif.Sukses === true) {
                                    // this.approvalKacabReceiveService.GLInventoryTransfer(this.idReqNot).subscribe(
                                    //     (resGL) => { },
                                    //     (err) => this.commonUtilService.showError(err)
                                    // );
                                    this.toasterService.showToaster('success', 'Sucess', this.notif.Message);
                                    this.previousState();
                                } else {
                                    this.toasterService.showToaster('error', 'Failed', this.notif.Message);
                                }
                                this.loadingService.loadingStop();
                            },
                            (err) => {
                                this.loadingService.loadingStop();
                                this.commonUtilService.showError(err);
                            }
                        )
                    }
                }
            })
        }else if (type === 'reject') {
            this.confirmationService.confirm({
                header : 'Konfirmasi',
                message : 'Apakah Anda Yakin Tidak Approve Note?',
                accept: () => {
                    const obj = {
                        Items: this.unitDocumentMessage,
                        createdby: this.principal.getUserLogin()
                    }
                    this.loadingService.loadingStart();
                    this.approvalKacabReceiveService.executeListProcessReject(obj).subscribe(
                        (res) => {
                            this.loadingService.loadingStop();
                            this.previousState();
                        },
                        (err) => {
                            this.loadingService.loadingStop();
                            this.commonUtilService.showError(err);
                        }
                    )
                }
            })
        }
    }
}
