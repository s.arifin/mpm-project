import { BaseEntity } from './../../shared';

export class ApprovalKacabReceive implements BaseEntity {
    constructor(
        public id?: any,
        public idreqnot?: any,
        public reqnotenumber?: any,
        public dtcreated?: any,
        public createdby?: any,
        public idstatustype?: any
    ) {
    }
}
