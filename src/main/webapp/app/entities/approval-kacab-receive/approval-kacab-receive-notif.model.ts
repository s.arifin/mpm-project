import { BaseEntity } from './../../shared';
import { UnitDocumentMessage } from '../unit-document-message/unit-document-message.model';

export class ApprovalKacabReceiveNotif implements BaseEntity {
    constructor(
        public id?: any,
        public Sukses?: any,
        public Message?: any,
        public idProduct?: string[],
        public Items?: UnitDocumentMessage[]
    ) {
    }
}
