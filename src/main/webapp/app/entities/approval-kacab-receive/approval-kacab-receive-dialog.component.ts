import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ApprovalKacabReceive } from './approval-kacab-receive.model';
import { ApprovalKacabReceivePopupService } from './approval-kacab-receive-popup.service';
import { ApprovalKacabReceiveService } from './approval-kacab-receive.service';

@Component({
    selector: 'jhi-approval-kacab-receive-dialog',
    templateUrl: './approval-kacab-receive-dialog.component.html'
})
export class ApprovalKacabReceiveDialogComponent implements OnInit {

    approvalKacabReceive: ApprovalKacabReceive;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private approvalKacabReceiveService: ApprovalKacabReceiveService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.approvalKacabReceive.id !== undefined) {
            this.subscribeToSaveResponse(
                this.approvalKacabReceiveService.update(this.approvalKacabReceive));
        } else {
            this.subscribeToSaveResponse(
                this.approvalKacabReceiveService.create(this.approvalKacabReceive));
        }
    }

    private subscribeToSaveResponse(result: Observable<ApprovalKacabReceive>) {
        result.subscribe((res: ApprovalKacabReceive) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ApprovalKacabReceive) {
        this.eventManager.broadcast({ name: 'approvalKacabReceiveListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-approval-kacab-receive-popup',
    template: ''
})
export class ApprovalKacabReceivePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private approvalKacabReceivePopupService: ApprovalKacabReceivePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.approvalKacabReceivePopupService
                    .open(ApprovalKacabReceiveDialogComponent as Component, params['id']);
            } else {
                this.approvalKacabReceivePopupService
                    .open(ApprovalKacabReceiveDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
