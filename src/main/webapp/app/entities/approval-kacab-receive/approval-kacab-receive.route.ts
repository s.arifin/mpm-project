import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ApprovalKacabReceiveComponent } from './approval-kacab-receive.component';
import { ApprovalKacabReceiveDetailComponent } from './approval-kacab-receive-detail.component';
import { ApprovalKacabReceivePopupComponent } from './approval-kacab-receive-dialog.component';
import { ApprovalKacabReceiveDeletePopupComponent } from './approval-kacab-receive-delete-dialog.component';

@Injectable()
export class ApprovalKacabReceiveResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const approvalKacabReceiveRoute: Routes = [
    {
        path: 'approval-kacab-receive',
        component: ApprovalKacabReceiveComponent,
        resolve: {
            'pagingParams': ApprovalKacabReceiveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabReceive.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'approval-kacab-receive/:idReqNot',
        component: ApprovalKacabReceiveDetailComponent,
        resolve: {
            'pagingParams': ApprovalKacabReceiveResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabReceive.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const approvalKacabReceivePopupRoute: Routes = [
    {
        path: 'approval-kacab-receive-new',
        component: ApprovalKacabReceivePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabReceive.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'approval-kacab-receive/:id/edit',
        component: ApprovalKacabReceivePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabReceive.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'approval-kacab-receive/:id/delete',
        component: ApprovalKacabReceiveDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mpmApp.approvalKacabReceive.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
