import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    MpmSharedLibsModule,
    MpmSharedCommonModule,
    ToasterService,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    Principal,
    JhiTrackerService,
    HasAnyAuthorityDirective,
    JhiLoginModalComponent,
    ValidationUtilsService,
    ConvertUtilService,
    CommonUtilService
} from './';

import { KeysPipe, UpperFirstCasePipe } from './pipe/function.pipe';
import { MessagePipe, MessageApprovalCategoryPipe, MessageIfNullPipe, ByteSizePipe } from './pipe/message.pipe';

@NgModule({
    imports: [
        MpmSharedLibsModule,
        MpmSharedCommonModule
    ],
    declarations: [
        ByteSizePipe,
        UpperFirstCasePipe,
        MessagePipe,
        KeysPipe,
        MessageIfNullPipe,
        MessageApprovalCategoryPipe,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        CommonUtilService,
        ConvertUtilService,
        ValidationUtilsService,
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        CSRFService,
        JhiTrackerService,
        AuthServerProvider,
        UserService,
        DatePipe
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        ByteSizePipe,
        UpperFirstCasePipe,
        KeysPipe,
        MessageIfNullPipe,
        MessagePipe,
        MessageApprovalCategoryPipe,
        MpmSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class MpmSharedModule {}
