export const SUBMIT_VSO = 101;
// export const APPROVE_SUBSIDI_OR_TERRITORIAL_VIOLATION = 102;
export const BUILD_VSO = 103;
export const RESET_VSO_DRAFT = 104;
export const FIND_OR_CREATE_VSO_NUMBER = 105;
export const APPROVAL_KACAB = 107;
export const APPROVAL_KORSAL = 108;
export const CANCEL_VSO = 109;
export const PROSES_BACK_ORDER = 117;
export const PROSES_UPDATE_HARGA_PEMBAYARAN = 119;
export const PROSES_BACK_ORDER_FROM_INDENT = 120;
