import { Pipe, PipeTransform } from '@angular/core';
import * as BaseConstants from '../../shared/constants/base.constants';

@Pipe({
    name : 'ApprovalStatus'
})
export class ApprovalStatusPipe implements PipeTransform {
    transform(value: number): string {
        let status: string;
        if (value === BaseConstants.Status.STATUS_NOT_REQUIRED) {
            status = 'Not Required';
        } else {
            status = 'Unknown Status';
        }
        return status;
    }
}
