import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args: string[]): any {
    const keys = [];
    for (const _key in value) {
        if (value.hasOwnProperty(_key)) {
            const obj: Object = {
                key: _key,
                value : value[_key]
            };

            keys.push(obj);
        }
    }
    return keys;
  }
}

@Pipe({
    name : 'UpperFirstCase'
})
export class UpperFirstCasePipe implements PipeTransform {
    transform(value: string): string {
        return value.replace(/\w\S*/g, function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }
}
