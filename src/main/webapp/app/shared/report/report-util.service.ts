import { Injectable } from '@angular/core';
import { Http, Headers, Response, ResponseContentType, BaseRequestOptions, URLSearchParams } from '@angular/http';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import * as FileSaver from 'file-saver';

@Injectable()
export class ReportUtilService {

    constructor(
        private http: Http,
        private dataUtils: JhiDataUtils,
    ) {
    }

    private getFileName(response: any): string {
        const contentDispositionHeader: string = response.headers.get('content-disposition');
        const parts: string[] = contentDispositionHeader.split(';');
        let fileNamePos = 2; // default pos value ...

        for (let index = 0; index < parts.length; index++) {
            if ( parts[index].trim().toLowerCase().indexOf('filename') === 0 ) {
                fileNamePos = index;
                break;
            }
        }

        const fileName = parts[fileNamePos].split('"')[1];
        return fileName;
    }

    downloadFile(api: string, req?: any) {
        const options = this.createReportRequestOption(req);
        this.http.get(api, options).subscribe((response: any) => {
            const pecahapi: string[] = api.split('/');
            const fileName = pecahapi[ pecahapi.length - 2 ] + '.' + pecahapi[ pecahapi.length - 1 ];
            FileSaver.saveAs(response.blob(), fileName );

            // FileSaver.saveAs(response.blob(), this.getFileName(response));
        });
    }

    viewFile(api: string, req?: any) {
        const options = this.createReportRequestOption(req);
        this.http.get(api, options).subscribe((response: any) => {
            const reader = new FileReader();
            const style = 'border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;';
            reader.readAsDataURL(response.blob());
            reader.onloadend = function() {
                const win = window.open();
                win.document.write('<iframe src="' + reader.result + '" frameborder="0" style="' + style + '" allowfullscreen></iframe>');
            }
        });
    }

    private createReportRequestOption = (req?: any): BaseRequestOptions => {
        const options: BaseRequestOptions = new BaseRequestOptions();
        const params: URLSearchParams = new URLSearchParams();
        options.responseType = ResponseContentType.Blob;
        if (req) {
            for (const key in req) {
                if (req[key]) {
                    const value = req[key];
                    params.set(key, value);
                }
            }
            options.params = params;
        }
        return options;
    };
}
