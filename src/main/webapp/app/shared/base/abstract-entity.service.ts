import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';
import { ResponseWrapper, createRequestOption } from '../../shared';

export class AbstractEntityService<T> {
    protected itemValues: T[];
    values: Subject<any> = new Subject<any>();

    protected resourceUrl =  SERVER_API_URL + 'api/billing-disbursements';
    protected resourceSearchUrl = SERVER_API_URL + 'api/_search/billing-disbursements';

    constructor(protected http: Http, protected dateUtils: JhiDateUtils) { }

    create(entity: T): Observable<T> {
        const copy = this.convert(entity);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(entity: T): Observable<T> {
        const copy = this.convert(entity);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: any): Observable<T> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    changeStatus(entity: T, id: Number): Observable<ResponseWrapper> {
        const copy = this.convert(entity);
        return this.http.post(`${this.resourceUrl}/set-status/${id}`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    queryFilterBy(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl + '/filterBy', options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id?: any): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    process(params?: any, req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/process`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    execute(params?: any, req?: any): Observable<T> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeList(params?: any, req?: any): Observable<T[]> {
        const options = createRequestOption(req);
        return this.http.post(`${this.resourceUrl}/execute-list`, params, options).map((res: Response) => {
            return res.json();
        });
    }

    executeProcess(id: Number, param: String, entity: T): Observable<T> {
        const copy = this.convert(entity);
        return this.http.post(`${this.resourceUrl}/execute/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    executeListProcess(id: Number, param: String, items: T[]): Observable<T[]> {
        const copy = this.convertList(items);
        return this.http.post(`${this.resourceUrl}/execute-list/${id}/${param}`, copy).map((res: Response) => {
            return res.json();
        });
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    protected convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    protected convertItemFromServer(json: any): T {
        const entity: T = Object.assign({}, json);
        return entity;
    }

    protected convert(entity: T): T {
        // if (entity === null || entity === {}) {
        //     return JSON.parse('{}');
        // }
        // const copy: T = JSON.parse(JSON.stringify(entity));
        return entity;
    }

    protected convertList(entities: T[]): T[] {
        const copy: T[] = entities;
        copy.forEach((item) => {
            item = this.convert(item);
        });
        return copy;
    }

    pushItems(data: T[]) {
        this.itemValues = data;
        this.values.next(this.itemValues);
    }
}
