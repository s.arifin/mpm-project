import { Routes } from '@angular/router';

import { NavbarComponent } from './layouts';

export const navbarRoute: Routes = [
  {
    path: '',
    component: NavbarComponent,
    outlet: 'navbar',
    data: {
      pageTitle: 'global.menu.home'
    }
  },
  {
    path: 'billing-disbursement',
    loadChildren: './entities/billing-disbursement/billing-disbursement.module#MpmBillingDisbursementModule'
  },
  {
    path: 'purchase-order',
    loadChildren: './entities/purchase-order/purchase-order.module#MpmPurchaseOrderModule'
  },
  {
    path: 'package-receipt',
    loadChildren: './entities/package-receipt/package-receipt.module#MpmPackageReceiptModule'
  },
  {
    path: 'vendor',
    loadChildren: './entities/vendor/vendor.module#MpmVendorModule'
  },
  {
    path: 'vendor-type',
    loadChildren: './entities/vendor-type/vendor-type.module#MpmVendorTypeModule'
  },
  {
    path: 'rem-part',
    loadChildren: './entities/rem-part/rem-part.module#MpmRemPartModule'
  },
  {
    path: 'unit-deliverable',
    loadChildren: './entities/unit-deliverable/unit-deliverable.module#MpmUnitDeliverableModule'
  },
  {
    path: 'vehicle-document-requirement',
    loadChildren: './entities/vehicle-document-requirement/vehicle-document-requirement.module#MpmVehicleDocumentRequirementModule'
  },
  {
    path: 'branch',
    loadChildren: './entities/branch/branch.module#MpmBranchModule'
  },
  {
    path: 'feature-applicable',
    loadChildren: './entities/feature-applicable/feature-applicable.module#MpmFeatureApplicableModule'
  },
  {
    path: 'vehicle-registration',
    loadChildren: './entities/vehicle-registration/vehicle-registration.module#MpmVehicleRegistrationModule'
  }
];
