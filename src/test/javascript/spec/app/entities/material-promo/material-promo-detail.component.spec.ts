/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { MaterialPromoDetailComponent } from '../../../../../../main/webapp/app/entities/material-promo/material-promo-detail.component';
import { MaterialPromoService } from '../../../../../../main/webapp/app/entities/material-promo/material-promo.service';
import { MaterialPromo } from '../../../../../../main/webapp/app/entities/material-promo/material-promo.model';

describe('Component Tests', () => {

    describe('MaterialPromo Management Detail Component', () => {
        let comp: MaterialPromoDetailComponent;
        let fixture: ComponentFixture<MaterialPromoDetailComponent>;
        let service: MaterialPromoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [MaterialPromoDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    MaterialPromoService,
                    JhiEventManager
                ]
            }).overrideTemplate(MaterialPromoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MaterialPromoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MaterialPromoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new MaterialPromo(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.materialPromo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
