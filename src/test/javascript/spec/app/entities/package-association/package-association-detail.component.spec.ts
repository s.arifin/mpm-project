/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PackageAssociationDetailComponent } from '../../../../../../main/webapp/app/entities/package-association/package-association-detail.component';
import { PackageAssociationService } from '../../../../../../main/webapp/app/entities/package-association/package-association.service';
import { PackageAssociation } from '../../../../../../main/webapp/app/entities/package-association/package-association.model';

describe('Component Tests', () => {

    describe('PackageAssociation Management Detail Component', () => {
        let comp: PackageAssociationDetailComponent;
        let fixture: ComponentFixture<PackageAssociationDetailComponent>;
        let service: PackageAssociationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [PackageAssociationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PackageAssociationService,
                    JhiEventManager
                ]
            }).overrideTemplate(PackageAssociationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PackageAssociationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PackageAssociationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PackageAssociation(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.packageAssociation).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
