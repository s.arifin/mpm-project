/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PelanggaranLuarWilayahDetailComponent } from '../../../../../../main/webapp/app/entities/pelanggaran-luar-wilayah/pelanggaran-luar-wilayah-detail.component';
import { PelanggaranLuarWilayahService } from '../../../../../../main/webapp/app/entities/pelanggaran-luar-wilayah/pelanggaran-luar-wilayah.service';
import { PelanggaranLuarWilayah } from '../../../../../../main/webapp/app/entities/pelanggaran-luar-wilayah/pelanggaran-luar-wilayah.model';

describe('Component Tests', () => {

    describe('PelanggaranLuarWilayah Management Detail Component', () => {
        let comp: PelanggaranLuarWilayahDetailComponent;
        let fixture: ComponentFixture<PelanggaranLuarWilayahDetailComponent>;
        let service: PelanggaranLuarWilayahService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [PelanggaranLuarWilayahDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PelanggaranLuarWilayahService,
                    JhiEventManager
                ]
            }).overrideTemplate(PelanggaranLuarWilayahDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PelanggaranLuarWilayahDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PelanggaranLuarWilayahService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PelanggaranLuarWilayah(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.pelanggaranLuarWilayah).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
