/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TransferUnitRequestDetailComponent } from '../../../../../../main/webapp/app/entities/transfer-unit-request/transfer-unit-request-detail.component';
import { TransferUnitRequestService } from '../../../../../../main/webapp/app/entities/transfer-unit-request/transfer-unit-request.service';
import { TransferUnitRequest } from '../../../../../../main/webapp/app/entities/transfer-unit-request/transfer-unit-request.model';

describe('Component Tests', () => {

    describe('TransferUnitRequest Management Detail Component', () => {
        let comp: TransferUnitRequestDetailComponent;
        let fixture: ComponentFixture<TransferUnitRequestDetailComponent>;
        let service: TransferUnitRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [TransferUnitRequestDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TransferUnitRequestService,
                    JhiEventManager
                ]
            }).overrideTemplate(TransferUnitRequestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TransferUnitRequestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TransferUnitRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TransferUnitRequest(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.transferUnitRequest).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
