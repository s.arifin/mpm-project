/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ApprovalKacabRequestDetailComponent } from '../../../../../../main/webapp/app/entities/approval-kacab-request/approval-kacab-request-detail.component';
import { ApprovalKacabRequestService } from '../../../../../../main/webapp/app/entities/approval-kacab-request/approval-kacab-request.service';
import { ApprovalKacabRequest } from '../../../../../../main/webapp/app/entities/approval-kacab-request/approval-kacab-request.model';

describe('Component Tests', () => {

    describe('ApprovalKacabRequest Management Detail Component', () => {
        let comp: ApprovalKacabRequestDetailComponent;
        let fixture: ComponentFixture<ApprovalKacabRequestDetailComponent>;
        let service: ApprovalKacabRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [ApprovalKacabRequestDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ApprovalKacabRequestService,
                    JhiEventManager
                ]
            }).overrideTemplate(ApprovalKacabRequestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ApprovalKacabRequestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ApprovalKacabRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ApprovalKacabRequest(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.approvalKacabRequest).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
