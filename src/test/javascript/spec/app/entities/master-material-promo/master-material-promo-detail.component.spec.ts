/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { MasterMaterialPromoDetailComponent } from '../../../../../../main/webapp/app/entities/master-material-promo/master-material-promo-detail.component';
import { MasterMaterialPromoService } from '../../../../../../main/webapp/app/entities/master-material-promo/master-material-promo.service';
import { MasterMaterialPromo } from '../../../../../../main/webapp/app/entities/master-material-promo/master-material-promo.model';

describe('Component Tests', () => {

    describe('MasterMaterialPromo Management Detail Component', () => {
        let comp: MasterMaterialPromoDetailComponent;
        let fixture: ComponentFixture<MasterMaterialPromoDetailComponent>;
        let service: MasterMaterialPromoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [MasterMaterialPromoDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    MasterMaterialPromoService,
                    JhiEventManager
                ]
            }).overrideTemplate(MasterMaterialPromoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MasterMaterialPromoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MasterMaterialPromoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new MasterMaterialPromo(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.masterMaterialPromo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
