/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ApprovalKacabReceiveDetailComponent } from '../../../../../../main/webapp/app/entities/approval-kacab-receive/approval-kacab-receive-detail.component';
import { ApprovalKacabReceiveService } from '../../../../../../main/webapp/app/entities/approval-kacab-receive/approval-kacab-receive.service';
import { ApprovalKacabReceive } from '../../../../../../main/webapp/app/entities/approval-kacab-receive/approval-kacab-receive.model';

describe('Component Tests', () => {

    describe('ApprovalKacabReceive Management Detail Component', () => {
        let comp: ApprovalKacabReceiveDetailComponent;
        let fixture: ComponentFixture<ApprovalKacabReceiveDetailComponent>;
        let service: ApprovalKacabReceiveService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [ApprovalKacabReceiveDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ApprovalKacabReceiveService,
                    JhiEventManager
                ]
            }).overrideTemplate(ApprovalKacabReceiveDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ApprovalKacabReceiveDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ApprovalKacabReceiveService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ApprovalKacabReceive(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.approvalKacabReceive).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
