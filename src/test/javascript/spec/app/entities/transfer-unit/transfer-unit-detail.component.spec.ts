/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { MpmTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TransferUnitDetailComponent } from '../../../../../../main/webapp/app/entities/transfer-unit/transfer-unit-detail.component';
import { TransferUnitService } from '../../../../../../main/webapp/app/entities/transfer-unit/transfer-unit.service';
import { TransferUnit } from '../../../../../../main/webapp/app/entities/transfer-unit/transfer-unit.model';

describe('Component Tests', () => {

    describe('TransferUnit Management Detail Component', () => {
        let comp: TransferUnitDetailComponent;
        let fixture: ComponentFixture<TransferUnitDetailComponent>;
        let service: TransferUnitService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MpmTestModule],
                declarations: [TransferUnitDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TransferUnitService,
                    JhiEventManager
                ]
            }).overrideTemplate(TransferUnitDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TransferUnitDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TransferUnitService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TransferUnit(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.transferUnit).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
