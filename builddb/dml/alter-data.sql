-- drop table organization_details;

-- alter table organization add column idpic BINARY(16);

-- alter table organization add column idposaddtdp BINARY(16);

-- alter table organization add column numbertdp VARCHAR(30);

-- alter table organization add column dtorgest DATETIME;

-- alter table communication_event drop column idprospect;

-- alter table communication_event drop column idevetyp;

-- alter table communication_event drop column interest;

-- untuk jumlah cetak

-- alter table billing add column printcounter int;

-- alter table communication_event_prospect add column purchaseplan VARCHAR(10);


-- alter table unit_deliverable modify column iddeliverable BINARY(16);
-- ALTER TABLE unit_deliverable ADD COLUMN refnumber VARCHAR(30) DEFAULT NULL;
-- ALTER TABLE unit_deliverable ADD COLUMN refdt DATETIME DEFAULT NULL;
-- ALTER TABLE unit_deliverable ADD COLUMN receiptqty INT DEFAULT NULL;
-- ALTER TABLE unit_deliverable ADD COLUMN receiptnominal DOUBLE(18,4) DEFAULT NULL;

-- idorder 

-- 
-- TABLE: communication_event_prospect 
--

-- CREATE TABLE communication_event_cdb(
--     idcomevt        				BINARY(16)        NOT NULL,
--     idorder      					BINARY(16),
--     hobby							VARCHAR(255),
-- 	home_status						VARCHAR(255),
-- 	expenditure_one_month			VARCHAR(255),
-- 	job								VARCHAR(255),
-- 	last_education					VARCHAR(255),
-- 	mobile_phone_ownership_status 	VARCHAR(255),
-- 	info_about_honda				VARCHAR(255),
-- 	vehicle_brand_owner				VARCHAR(255),
-- 	vehicle_type_owner				VARCHAR(255),
-- 	buy_for							VARCHAR(255), 
-- 	vehicle_user					VARCHAR(255),
 --    PRIMARY KEY (idcomevt)
-- )ENGINE=INNODB
-- ;

-- ALTER TABLE prospect ADD COLUMN idsalescoordinator BINARY(16);

-- CREATE TABLE price_component_info (
-- 	idpricompinfo BINARY(16) NOT NULL,
-- 	idproduct VARCHAR(30) NULL DEFAULT NULL,
-- 	number VARCHAR(30) NULL DEFAULT NULL,
-- 	dtfrom DATE NULL DEFAULT NULL,
-- 	dtthru DATE NULL DEFAULT NULL,
-- 	PRIMARY KEY (idpricompinfo)
-- )
-- ENGINE=InnoDB
-- ;


-- ALTER TABLE price_component ADD COLUMN idpricompinfo BINARY(16);

-- ALTER TABLE price_component ADD 
--     FOREIGN KEY (idpricompinfo)
--     REFERENCES price_component_info(idpricompinfo)
-- ;

-- ALTER TABLE price_component_info ADD
-- 	FOREIGN KEY (idproduct)
-- 	REFERENCES product(idproduct)
-- ;

-- ALTER TABLE price_component_info ADD COLUMN idparty BINARY(16);

-- ALTER TABLE price_component_info ADD
-- 	FOREIGN KEY (idparty)
-- 	REFERENCES party(idparty)
-- ;

-- ALTER TABLE price_component
-- 	DROP FOREIGN KEY price_component_ibfk_8
-- ;

-- ALTER TABLE price_component
-- 	DROP COLUMN idpricompinfo
-- ; 

-- DROP TABLE price_component_info;

-- ALTER TABLE shipment_receipt ADD COLUMN idfacility BINARY(16);

-- ALTER TABLE sales_unit_requirement ADD COLUMN makelarfee DECIMAL(-- 18,4); // diganti brokerfee

-- ALTER TABLE vehicle_sales_order ADD COLUMN dtadmslsverif DATE default null; // diganti dtadmslsverify


-- ALTER TABLE vehicle_sales_order ADD COLUMN dtafcverif DATE default null; // dtafcverify

-- ALTER TABLE sales_unit_requirement ADD COLUMN bbnprice DECIMAL(18,4) NULL DEFAULT NULL;
-- ALTER TABLE sales_unit_requirement ADD COLUMN hetprice DECIMAL(18,4) NULL DEFAULT NULL;

-- ALTER TABLE prospect ADD COLUMN dtfollowup DATETIME default null;

-- ALTER TABLE communication_event_prospect ADD COLUMN connect VARCHAR(5);

-- RICKY
-- UPDATE status_type SET description = 'Not Required' WHERE idstatustype=1; 
-- UPDATE status_type SET description = 'Waiting For Approval' WHERE idstatustype=60;
-- UPDATE status_type SET description = 'Approval' WHERE idstatustype=61;
-- UPDATE status_type SET description = 'Rejected' WHERE idstatustype=62;
-- UPDATE status_type SET description = 'Territorial Violation' WHERE idstatustype=63;

-- CREATE TABLE broker_type(
--     idbrotyp       INT            AUTO_INCREMENT,
--     description    VARCHAR(50),
--     PRIMARY KEY (idbrotyp)
-- )ENGINE=INNODB
-- ;

-- CREATE TABLE suspect_type(
--     idsuspecttyp       INT            AUTO_INCREMENT,
--     description    VARCHAR(50),
--     PRIMARY KEY (idsuspecttyp)
-- )ENGINE=INNODB
-- ;

-- ALTER TABLE sales_broker ADD COLUMN idbrotyp INT DEFAULT NULL;

-- ALTER TABLE suspect ADD COLUMN idsuspecttyp INT DEFAULT NULL;

-- ALTER TABLE prospect ADD COLUMN eveloc VARCHAR(255);

-- IRFAN
-- ALTER TABLE picking_slip ADD COLUMN acc1 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN acc2 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN acc3 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN acc4 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN acc5 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN acc6 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN acc7 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN promat1 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN promat2 VARCHAR(5) DEFAULT NULL;

-- ALTER TABLE shipment_receipt ADD COLUMN acc1 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN acc2 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN acc3 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN acc4 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN acc5 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN acc6 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN acc7 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN promat1 VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE shipment_receipt ADD COLUMN promat2 VARCHAR(5) DEFAULT NULL;

-- CREATE TABLE driver(
--     idparrol    BINARY(16)     NOT NULL,
--     iddriver    VARCHAR(30),
--     PRIMARY KEY (idparrol)
-- )ENGINE=INNODB
-- ;

--

-- ALTER TABLE vehicle_document_requirement ADD COLUMN atpmfakturdt DATETIME DEFAULT NULL;
-- ALTER TABLE vehicle_document_requirement ADD COLUMN submissionno VARCHAR(30) DEFAULT NULL;
-- ALTER TABLE vehicle_document_requirement ADD COLUMN submissiondt DATETIME DEFAULT NULL;
-- ALTER TABLE vehicle_document_requirement ADD COLUMN crossarea INT DEFAULT NULL;

-- IRFAN
-- ALTER TABLE driver ADD COLUMN idvendor BINARY(16) DEFAULT NULL;
-- ALTER TABLE shipment_outgoing ADD COLUMN iddriver BINARY(16) DEFAULT NULL;
-- ALTER TABLE shipment_outgoing ADD COLUMN othername VARCHAR(255) DEFAULT NULL;
-- ALTER TABLE mechanic ADD COLUMN idvendor BINARY(16) DEFAULT NULL;
-- ALTER TABLE mechanic ADD COLUMN external VARCHAR(5) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN idinternalmecha BINARY(16) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN idexternalmecha BINARY(16) DEFAULT NULL;
-- ALTER TABLE picking_slip ADD COLUMN promatdirectgift VARCHAR(30) DEFAULT NULL;


-- NAUFAL
-- ALTER TABLE salesman ADD COLUMN idteamleader BINARY(16) DEFAULT NULL;
-- ALTER TABLE salesman ADD COLUMN teamleader VARCHAR(5) DEFAULT NULL;

-- RICKY
-- ALTER TABLE sales_unit_requirement ADD COLUMN idprosou INT DEFAULT NULL;

-- CREATE TABLE rule_hot_item (
-- 	idrule INT(11) NULL DEFAULT NULL,
-- 	idproduct VARCHAR(50) NULL DEFAULT NULL,
-- 	mindownpayment DOUBLE NULL DEFAULT NULL,
-- 	PRIMARY KEY (idrule)
-- )ENGINE=INNODB;

-- ALTER TABLE product ADD COLUMN description VARCHAR(200) DEFAULT NULL;

-- CREATE TABLE communication_event_delivery(
-- 	idcomevt        			BINARY(16)  NOT NULL,
-- 	idcustomer      			BINARY(16)	DEFAULT NULL,
-- 	idinternal					BINARY(16)	DEFAULT NULL,
-- 	bussinesscode				VARCHAR(5) DEFAULT NULL,
	
-- 	b1							VARCHAR(5) DEFAULT NULL,
-- 	b2							VARCHAR(5) DEFAULT NULL,	
-- 	b3							VARCHAR(5) DEFAULT NULL,
-- 	b4							VARCHAR(5) DEFAULT NULL,
-- 	c11rate						INT DEFAULT NULL,
-- 	c11reason					VARCHAR(50) DEFAULT NULL,
-- 	c11hope						VARCHAR(50) DEFAULT NULL,
-- 	c12rate						INT DEFAULT NULL,
-- 	c12reason					VARCHAR(50) DEFAULT NULL,
-- 	c13hope						VARCHAR(50) DEFAULT NULL,
-- 	c14rate						INT DEFAULT NULL,
-- 	c14reason					VARCHAR(50) DEFAULT NULL,
-- 	c14hope						VARCHAR(50) DEFAULT NULL,
-- 	c15rate						INT DEFAULT NULL,
-- 	c15reason					VARCHAR(50) DEFAULT NULL,
-- 	c15hope						VARCHAR(50) DEFAULT NULL,
-- 	c16rate						INT DEFAULT NULL,
-- 	c16reason					VARCHAR(50) DEFAULT NULL,
-- 	c16hope						VARCHAR(50) DEFAULT NULL,
-- 	c17rate						INT DEFAULT NULL,
-- 	c17reason					VARCHAR(50) DEFAULT NULL,
-- 	c17hope						VARCHAR(50) DEFAULT NULL,
-- 	c18rate						INT DEFAULT NULL,
-- 	c18reason					VARCHAR(50) DEFAULT NULL,
-- 	c18hope						VARCHAR(50) DEFAULT NULL,
-- 	c19rate						INT DEFAULT NULL,
-- 	c19reason					VARCHAR(50) DEFAULT NULL,
-- 	c19hope						VARCHAR(50) DEFAULT NULL,
-- 	c110rate					INT DEFAULT NULL,
-- 	c110reason					VARCHAR(50) DEFAULT NULL,
-- 	c110hope					VARCHAR(50) DEFAULT NULL,
-- 	c2rate						INT DEFAULT NULL,
-- 	c3option					VARCHAR(50) DEFAULT NULL,
-- 	d1name						VARCHAR(50) DEFAULT NULL,
-- 	d2gender					VARCHAR(50) DEFAULT NULL,
-- 	d3age						INT DEFAULT NULL,
-- 	d4phone						VARCHAR(50) DEFAULT NULL,
-- 	d5vehicle					VARCHAR(50) DEFAULT NULL,
-- 	d6education	       			VARCHAR(50) DEFAULT NULL,
  --   PRIMARY KEY (idcomevt)
--  )ENGINE=INNODB
--  ;
 
-- ALTER TABLE suspect ADD COLUMN salesdt 			DATE DEFAULT NULL;
-- ALTER TABLE suspect ADD COLUMN marketname		VARCHAR(100) DEFAULT NULL;
-- ALTER TABLE suspect ADD COLUMN repurchaseprob 	FLOAT(8, 2) DEFAULT NULL;
-- ALTER TABLE suspect ADD COLUMN priority		  	FLOAT(8, 2) DEFAULT NULL;
-- ALTER TABLE suspect ADD COLUMN idsaletype  		INT DEFAULT NULL;
-- ALTER TABLE suspect ADD COLUMN idlsgpro  		BINARY(16) DEFAULT NULL;

-- alter table vehicle_sales_order add column admslsverifyval int DEFAULT 0;


-- alter table vehicle_sales_order add column afcverifyval int DEFAULT 0;


-- alter table vehicle_sales_order add column admslsnotapprovnote VARCHAR(50) DEFAULT NULL;


-- ALTER TABLE payment	ADD COLUMN refno VARCHAR(30) NULL;

-- alter table vehicle_sales_order add column admslsnotapprovnote VARCHAR(50) DEFAULT NULL;

-- alter table vehicle_sales_order add column afcnotapprovnote VARCHAR(50) DEFAULT NULL;




-- alter table vehicle_sales_order add column vrnama1 VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrnama2 VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrnamamarket VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrwarna VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrtahunproduksi VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrangsuran DOUBLE DEFAULT 0;
-- alter table vehicle_sales_order add column vrtenor DOUBLE DEFAULT 0;
-- alter table vehicle_sales_order add column vrleasing VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrtglpengiriman DATE DEFAULT NULL;
-- alter table vehicle_sales_order add column vrjampengiriman VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrtipepenjualan VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrdpmurni DOUBLE DEFAULT 0;
-- alter table vehicle_sales_order add column vrtandajadi DOUBLE DEFAULT 0;
-- alter table vehicle_sales_order add column vrtglpembayaran DATE DEFAULT NULL;
-- alter table vehicle_sales_order add column vrsisa DOUBLE DEFAULT 0;
-- alter table vehicle_sales_order add column vriscod INTEGER DEFAULT 0;
-- alter table vehicle_sales_order add column vrcatatantambahan VARCHAR(50) DEFAULT NULL;
-- alter table vehicle_sales_order add column vrisverified INTEGER DEFAULT 0;
-- alter table vehicle_sales_order add column vrcatatan VARCHAR(50) DEFAULT NULL;

-- RICKY
-- CREATE TABLE rule_indent (
-- 	idrule INT(11) NULL DEFAULT NULL,
-- 	idproduct VARCHAR(50) NULL DEFAULT NULL,
-- 	idInternal BINARY(16) NULL DEFAULT NULL,
-- 	PRIMARY KEY (idrule)
-- )ENGINE=INNODB;



-- alter table communication_event_cdb add column facebook VARCHAR(100) DEFAULT NULL;
-- alter table communication_event_cdb add column instagram VARCHAR(100) DEFAULT NULL;
-- alter table communication_event_cdb add column twitter VARCHAR(100) DEFAULT NULL;
-- alter table communication_event_cdb add column youtube VARCHAR(100) DEFAULT NULL;
-- alter table communication_event_cdb add column email VARCHAR(100) DEFAULT NULL;

-- alter table communication_event_delivery add column dealername VARCHAR(50) DEFAULT NULL;
-- alter table communication_event_delivery add column	dealeraddress VARCHAR(50) DEFAULT NULL;

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN prodyear SMALLINT NULL;

-- ALTER TABLE communication_event_delivery MODIFY COLUMN c11rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c12rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c14rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c15rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c16rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c17rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c18rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c19rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c110rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery MODIFY COLUMN c2rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery ADD COLUMN c13rate VARCHAR(25) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery ADD COLUMN c13reason VARCHAR(50) DEFAULT NULL;
-- ALTER TABLE communication_event_delivery ADD COLUMN c12hope	VARCHAR(50) DEFAULT NULL;

-- ALTER TABLE shipment_outgoing ADD COLUMN deliveryopt VARCHAR(50) DEFAULT NULL;
-- ALTER TABLE shipment_outgoing ADD COLUMN deliveryaddress VARCHAR(50) DEFAULT NULL;

-- ALTER TABLE billing ADD COLUMN duedate DATETIME NULL ;

-- ALTER TABLE vehicle_document_requirement add column dtfillname DATETIME NULL;

	
-- ALTER TABLE sales_unit_requirement
--	ADD COLUMN prodyear SMALLINT NULL;

-- ALTER TABLE vehicle_sales_order ADD COLUMN kacabNote VARCHAR(300) DEFAULT NULL;

-- alter table vehicle_sales_order change column vrtglpengiriman vrtglpengiriman char(10) default null;

-- alter table vehicle_sales_order change column vrtglpembayaran vrtglpembayaran char(10) default null;

-- RICKY
-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN requestidmachine VARCHAR(90) NULL,
-- 	ADD COLUMN requestidframe VARCHAR(90) NULL;

-- NAUFAL // REPLACE BY EAP, di ganti dengan ada catatan qty
-- CREATE TABLE shipment_billing_item (
--	idbilite BINARY(50) NOT NULL,
--	idshiite BINARY(50) NOT NULL,
--	PRIMARY KEY (idbilite, idshiite)
-- )ENGINE=InnoDB;

-- alter table memo add column currbillnumb varchar(50) default null; 

-- alter table communication_event_cdb add column idreligion int default null; 

-- alter table communication_event_cdb add column iscityidadd int default null; 

-- alter table communication_event_cdb add column ismailadd int default null; 

-- ALTER TABLE package_receipt ADD COLUMN vendorinvoice VARCHAR(30) DEFAULT NULL; 

-- ALTER TABLE inventory_movement ADD COLUMN slipnum VARCHAR(50) NULL;

-- ALTER TABLE vehicle_sales_order CHANGE COLUMN vrtglpembayaran vrtglpembayaran DATE NULL DEFAULT NULL;

-- ALTER TABLE vehicle_sales_order CHANGE COLUMN vrtglpengiriman vrtglpengiriman DATE NULL DEFAULT NULL;

-- ALTER TABLE suspect CHANGE COLUMN salesdt salesdt DATE NULL DEFAULT NULL;

-- ALTER TABLE communication_event_cdb ADD COLUMN idprovince BINARY(16) DEFAULT NULL;
-- ALTER TABLE communication_event_cdb ADD COLUMN idcity BINARY(16) DEFAULT NULL;
-- ALTER TABLE communication_event_cdb ADD COLUMN iddistrict BINARY(16) DEFAULT NULL;
-- ALTER TABLE communication_event_cdb ADD COLUMN address BINARY(16) DEFAULT NULL;

-- ALTER TABLE memo ADD COLUMN reqidfeature int default null;

-- ALTER TABLE postal_address
-- CHANGE COLUMN district iddistrict VARCHAR(36) NULL DEFAULT NULL,
-- CHANGE COLUMN village idvillage VARCHAR(36) NULL DEFAULT NULL,
-- CHANGE COLUMN city idcity VARCHAR(36) NULL DEFAULT NULL,
-- CHANGE COLUMN province idprovince VARCHAR(36) NULL DEFAULT NULL;

-- CREATE TABLE request (
--	idreq BINARY(16) NOT NULL,
--	idreqtype INT(11) NULL DEFAULT NULL,
--	reqnum VARCHAR(50) NULL DEFAULT NULL,
--	dtcreate DATETIME NULL DEFAULT NULL,
--	description VARCHAR(50) NULL DEFAULT NULL,
--	PRIMARY KEY (idreq)
-- )ENGINE=InnoDB;

-- CREATE TABLE request_item (
--	idreqitem BINARY(16) NOT NULL,
--	idreq BINARY(16) NULL DEFAULT NULL,
--	idproduct VARCHAR(30) NULL DEFAULT NULL,
--	idfeature INT NULL DEFAULT NULL,
--	seq INT(11) NULL DEFAULT NULL,
--	qtyreq DOUBLE NULL DEFAULT NULL,
--	qtytransfer DOUBLE NULL DEFAULT NULL,
--	description VARCHAR(50) NULL DEFAULT NULL,
--	PRIMARY KEY (idreqitem)
-- )ENGINE=InnoDB;


-- CREATE TABLE request_type (
--	idreqtype INT(11) NOT NULL,
--	description VARCHAR(50) NULL DEFAULT NULL,
--	PRIMARY KEY (idreqtype)
-- )ENGINE=InnoDB;

-- CREATE TABLE request_product (
--	idreq BINARY(16) NOT NULL,
--	idfacilityfrom BINARY(16) NOT NULL,
--	idfacilityto BINARY(16) NOT NULL,
--	PRIMARY KEY (idreq)
-- )ENGINE=InnoDB;

-- CREATE TABLE request_status (
--	idstatus BINARY(16) NOT NULL,
--	idreq BINARY(16) NULL DEFAULT NULL,
--	idstatustype INT(11) NULL DEFAULT NULL,
--	idreason INT(11) NULL DEFAULT NULL,
--	reason VARCHAR(300) NULL DEFAULT NULL,
--	dtfrom DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
--	dtthru DATETIME NULL DEFAULT '9999-12-31 23:59:59',
--	PRIMARY KEY (idstatus)
-- )ENGINE=InnoDB;

-- CREATE TABLE request_role (
--	idrole BINARY(16) NOT NULL,
--	idreq BINARY(16) NULL DEFAULT NULL,
--	idroletype INT(11) NULL DEFAULT NULL,
--	idparty BINARY(16) NULL DEFAULT NULL,
--	username VARCHAR(30) NULL DEFAULT NULL,
--	dtfrom DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
--	dtthru DATETIME NULL DEFAULT '9999-12-31 23:59:59',
--	PRIMARY KEY (idrole)
-- )ENGINE=InnoDB;

-- INSERT INTO request_type (idreqtype, description) VALUES (1, 'Moving Item');
-- COMMIT;

-- ALTER TABLE sales_booking
-- 	ADD COLUMN note VARCHAR(200) NULL;

-- ALTER TABLE sales_booking
-- 	ADD COLUMN yearassembly INT NULL;

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN requestidmachineandframe VARCHAR(100) NULL;

-- ALTER TABLE approval
-- 	ADD COLUMN idmessage INT NULL;

-- ALTER TABLE vehicle_document_requirement
-- 	ADD COLUMN registrationnumber VARCHAR(30) DEFAULT NULL;

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN minpayment DOUBLE DEFAULT NULL;

-- CREATE TABLE request_requirement (
-- 	idreq BINARY(16) NOT NULL,
-- 	idrequirement BINARY(16) NULL DEFAULT NULL,
-- 	qty DOUBLE NULL DEFAULT NULL,
-- 	PRIMARY KEY (idreq)
-- )
-- ENGINE=InnoDB
-- ;

-- ALTER TABLE rules
-- 	ADD COLUMN idinternal VARCHAR(30) NULL;

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN isunithotitem VARCHAR(5) NULL DEFAULT NULL;

-- ALTER TABLE sales_unit_requirement
--   ADD COLUMN isunitindent VARCHAR(5) NULL DEFAULT NULL;


-- ALTER TABLE vehicle_sales_order
-- 	ADD COLUMN isreqtaxinv INTEGER DEFAULT 0;

-- Deddy
-- cek dulu ada ga 2 baris ini? soal nya ALTER di comment, tapi ga ada di CREATE table
-- alter table memo add column currbillnumb varchar(50) default null; 

-- ALTER TABLE memo ADD COLUMN reqidfeature int default null;


-- untuk Memo koreksi ganti komposisi
-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axunitprice DOUBLE NULL DEFAULT 0;
      
-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axbbn DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axsalesamount DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axdisc DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axprice DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axppn DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axaramt DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axadd1 DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axadd2 DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  vehicle_sales_billing
--       add COLUMN axadd3 DOUBLE NULL DEFAULT 0;

-- ALTER TABLE  order_item
--       add COLUMN discountatpm DOUBLE NULL DEFAULT 0;

-- ALTER TABLE sales_booking
-- 	ADD COLUMN onhand VARCHAR(5),
-- 	ADD COLUMN intransit VARCHAR(5);

-- ALTER TABLE unit_shipment_receipt ADD COLUMN isreceipt VARCHAR(5);

-- ALTER TABLE unit_deliverable
-- 	MODIFY COLUMN dtreceipt DATETIME,
-- 	MODIFY COLUMN dtdelivery DATETIME;

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN waitstnk VARCHAR(5);

-- ALTER TABLE sales_unit_requirement
--	ADD COLUMN idleasing BINARY(16);

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN creditinstallment DECIMAL(18,4);

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN creditdownpayment DECIMAL(18,4);

-- ALTER TABLE sales_unit_requirement
-- 	ADD COLUMN credittenor INTEGER;
	
-- ALTER TABLE vehicle_document_requirement
-- 	ADD COLUMN statusfaktur VARCHAR(20);

-- ALTER TABLE vehicle_sales_order
-- 	ADD COLUMN isdonematching VARCHAR(5);


-- alter table vehicle_sales_order
-- modify vrtglpengiriman varchar(25);

-- alter table vehicle_sales_order
-- modify vrtglpembayaran varchar(25);


-- CREATE TABLE request_unit_internal (
-- 	idrequest BINARY(16) NOT NULL,
-- 	idparent BINARY(16),
-- 	idinternalto VARCHAR(50),
-- 	PRIMARY KEY (idrequest)
-- );

-- ALTER TABLE rule_indent
-- 	ADD COLUMN minpayment DOUBLE;

-- ALTER TABLE vehicle_sales_order 
-- 	ADD COLUMN vridsaletype  INTEGER;
-- add foreign key (vridsaletype) references sale_type(idsaletype);

-- add lagi... ilang terus

-- alter table communication_event_cdb add column idreligion int; 

-- alter table communication_event_cdb add column iscityidadd int; 

-- alter table communication_event_cdb add column ismailadd int; 

-- ALTER TABLE picking_slip ADD COLUMN acc8 VARCHAR(5);

-- ALTER TABLE picking_slip ADD COLUMN acctambah1 VARCHAR(5);

-- ALTER TABLE picking_slip ADD COLUMN acctambah2 VARCHAR(5);

-- ALTER TABLE shipment_outgoing ADD COLUMN jumlahprint INTEGER DEFAULT 0;

-- ALTER TABLE approval
-- 	ADD COLUMN idleacom BINARY(16);


-- ALTER TABLE postal_address CHANGE COLUMN address1 address1 VARCHAR(255) NULL DEFAULT NULL;

-- ALTER TABLE organization CHANGE COLUMN officephone officephone VARCHAR(255) NULL DEFAULT NULL;

-- ALTER TABLE organization CHANGE COLUMN name name VARCHAR(255) NULL DEFAULT NULL;

-- ALTER TABLE customer
-- 	ADD COLUMN idmpm VARCHAR(30) NULL;


-- alter table sales_unit_requirement add column podate DATETIME;

-- alter table sales_unit_requirement add column ponumber VARCHAR(50);

-- ALTER TABLE vehicle_sales_order 
--	ADD COLUMN idsalesman BINARY(16);
	
-- ALTER TABLE vehicle_sales_order
--	ADD CONSTRAINT 
--	FOREIGN KEY (idsalesman) REFERENCES salesman (idparrol);


-- Alter table vehicle_sales_order add column idbirojasa varchar(30);

-- ALTER TABLE internal ADD COLUMN iddealercode VARCHAR(30) NULL DEFAULT NULL;

-- ALTER TABLE internal ADD COLUMN idmpm VARCHAR(30) NULL DEFAULT NULL;

-- ALTER TABLE internal ADD COLUMN idahm VARCHAR(30) NULL DEFAULT NULL;


-- ALTER TABLE vehicle_sales_order ADD COLUMN idsales BINARY(16);

------- dean person_prospect---------------
-- ALTER TABLE   person_prospect ADD   fname DOM_NAME;
-- ALTER TABLE   person_prospect ADD   lname DOM_NAME;
-- ALTER TABLE   person_prospect ADD   cellphone DOM_DESCRIPTION;
-- ALTER TABLE   person_prospect ADD   facilityname DOM_DESCRIPTION
-- ALTER TABLE   person_prospect ADD   internname DOM_NAME

---------HANNY--------------------
--kolom bpkbnumber di tabel unit_deliverable typedata string
-- ALTER TABLE unit_deliverable ADD bpkbnumber DOM_DESCRIPTION

---------ASNAN---------------------
----kolom isbirojasa di tabel vendor typedata int
----kolom isdealer di tabel vendor typedata int
-- ALTER TABLE vendor ADD isbirojasa DOM_INTEGER

-- ALTER TABLE vendor ADD ismaindealer DOM_INTEGER

-------saeful ---------------------
-----sudah ditambah di prod baru di daftarkan di alter table-----
-- table memo
-- ALTER TABLE memo ADD ppn DOM_MONEY

-- ALTER TABLE memo ADD price DOM_MONEY   -> dpp

-- ALTER TABLE memo ADD salesamount DOM_MONEY

-- ALTER TABLE memo ADD aramount DOM_MONEY

-- ALTER TABLE vehicle_sales_order ADD issubsidifincoasdp DOM_BOOLEAN
	
--table communicationeventcdb
-- ALTER TABLE communication_event_cdb
--      ADD idprovincesurat DOM_UUID

-- ALTER TABLE communication_event_cdb
--      ADD idcitysurat DOM_UUID

-- ALTER TABLE communication_event_cdb
--      ADD iddistrictsurat DOM_UUID

-- ALTER TABLE communication_event_cdb
--      ADD idvillagesurat DOM_UUID

-- ALTER TABLE communication_event_cdb
--      ADD addresssurat DOM_LONGDESC

-- ALTER TABLE communication_event_cdb
--      ADD jenispembayaran DOM_LONGDESC

-- ALTER TABLE communication_event_cdb
--      ADD groupcustomer DOM_LONGDESC

-- ALTER TABLE communication_event_cdb
--      ADD keterangan DOM_LONGDESC

	 ---------motor------
-- ALTER TABLE motor
-- 	ADD dtdiscontinued DOM_DATEFROM
	
-- ALTER TABLE vehicle_customer_request
-- 	ADD idsalesman DOM_UUID

	-- hanny
	-- penambahan kolom untuk biaya pengurusan
-- ALTER TABLE vehicle_document_requirement
-- 	ADD costhandling DOM_MONEY

-- tabel inventory_movement
-- ALTER TABLE inventory_movement ADD idivtmvttyp DOM_INTEGER
-- go
-- ALTER TABLE inventory_movement ADD reffnum DOM_ID
-- go
-- ALTER TABLE person_prospect ADD cellphone1 DOM_DESCRIPTION
-- go
-- ALTER TABLE person_prospect ADD cellphone2 DOM_DESCRIPTION
-- go
-- 
-- ALTER TABLE vehicle_sales_order ADD refundfinco DOM_MONEY
-- go
-- ALTER TABLE communication_event_cdb ADD citizenship DOM_LONGDESC
-- go
-- -- hanny
-- -- untuk keperluan report bast ke ho
-- ALTER TABLE vehicle_document_requirement
--  	ADD bbnkb DOM_MONEY
-- go
-- ALTER TABLE vehicle_document_requirement
--  	ADD pkb DOM_MONEY
-- go
-- ALTER TABLE vehicle_document_requirement
--  	ADD swdkllj DOM_MONEY
-- go
-- ALTER TABLE vehicle_document_requirement
--  	ADD tnkb DOM_MONEY
-- go
-- ALTER TABLE vehicle_document_requirement
--  	ADD jasa DOM_MONEY
-- go
-- ALTER TABLE vehicle_document_requirement
--  	ADD stnkbpkb DOM_MONEY
-- go
-- ALTER TABLE vehicle_document_requirement
--  	ADD totalcosthandling DOM_MONEY	 
-- go
-- 
-- -- flaging untuk tampilan bast akan hilang jika dokumen sudah diserahkan ke cust
-- ALTER TABLE unit_deliverable ADD iscompleted DOM_INTEGER
-- go
-- ALTER TABLE unit_deliverable ADD isprinted DOM_INTEGER
-- go
-- ALTER TABLE sales_unit_requirement
--  	ADD salescommissions DOM_MONEY	 
-- go
-- 	-----irfan-------
-- Alter table communication_event_delivery
-- alter column c11rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c12rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c13rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c14rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c15rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c16rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c17rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c18rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c19rate dom_shortid
-- go
-- Alter table communication_event_delivery
-- alter column c110rate dom_shortid;
-- go
-- 
-- -- hanny nambahin buat ngisi postal code di detail cdb untuk keperluan download cddb
-- ALTER table communication_event_cdb add postalcodesurat DOM_DESCRIPTION
-- go
-- ALTER table communication_event_cdb add postalcode DOM_DESCRIPTION
-- go
-- 
-- ALTER TABLE shipment
-- ADD dtcreatenumber DOM_DATEFROM
-- go
-- 
-- alter table vehicle_sales_order add iscanclosedp DOM_BOOLEAN
-- go
-- alter table billing add refkey DOM_DESCRIPTION
-- go
-- -- hanny tambah relasi sur ke cdb
-- ALTER TABLE communication_event_cdb 
-- ADD idslsreq DOM_UUID
-- go
-- 
-- -- hanny tambah relasi cdb ke vdr
-- ALTER TABLE vehicle_document_requirement 
-- ADD idcomevt DOM_UUID
-- go
-- 
-- ALTER TABLE communication_event_cdb 
--     add foreign key (idslsreq) references sales_unit_requirement(idreq)
-- go
-- 
-- ALTER TABLE vehicle_document_requirement_begbal
-- ADD idcomevt DOM_UUID
-- go
-- 
-- ALTER TABLE communication_event_prospect
-- ADD nextfollowup DOM_DATEFROM
-- go
-- 
-- ALTER TABLE communication_event_prospect
-- ADD note DOM_DESCRIPTION
-- go
-- 
-- ALTER TABLE package_receipt
-- ADD dtissued DOM_DATEFROM
-- go
-- 
-- 
-- ALTER TABLE billing_disbursement
-- ADD dtissued DOM_DATEFROM
-- go
-- 
-- 
-- ALTER TABLE communication_event_prospect
-- ADD walkintype DOM_DESCRIPTION
-- go
-- 
-- ALTER TABLE billing ADD createdby DOM_NAME, dtcreated DOM_DATEFROM, modifiedby DOM_NAME, dtmodified DOM_DATEFROM
-- go
-- ALTER TABLE orders ADD createdby DOM_NAME, dtcreated DOM_DATEFROM, modifiedby DOM_NAME, dtmodified DOM_DATEFROM
-- go
-- ALTER TABLE shipment ADD createdby DOM_NAME, dtcreated DOM_DATEFROM, modifiedby DOM_NAME, dtmodified DOM_DATEFROM
-- go
-- ALTER TABLE payment ADD createdby DOM_NAME, dtcreated DOM_DATEFROM, modifiedby DOM_NAME, dtmodified DOM_DATEFROM
-- go
-- ALTER TABLE requirement ADD createdby DOM_NAME, dtcreated DOM_DATEFROM, modifiedby DOM_NAME, dtmodified DOM_DATEFROM
-- go
-- ALTER TABLE prospect ADD createdby DOM_NAME, dtcreated DOM_DATEFROM, modifiedby DOM_NAME, dtmodified DOM_DATEFROM
-- go
-- 
-- alter table driver 
-- add username varchar(30)
-- go
-- 
-- ALTER TABLE vehicle_sales_billing
-- ADD irunumber varchar(100)
-- go
-- 
-- ALTER TABLE vehicle_document_requirement
-- ADD biayaparkir DOM_MONEY
-- go
-- 
-- ALTER TABLE vehicle_document_requirement
-- ADD biaya DOM_MONEY
-- go
-- 
-- ALTER TABLE unit_deliverable
-- ADD dtbastsales DOM_DATEFROM
-- go
-- 
-- ALTER TABLE unit_deliverable
-- ADD dtbastsalesback DOM_DATEFROM
-- go
-- 
-- ALTER TABLE billing_disbursement
-- ADD discountinvoice DOM_MONEY
-- go
-- 
-- ALTER TABLE billing_disbursement
-- ADD freightcost DOM_MONEY
-- go
-- 
-- ALTER TABLE organization
-- ADD picname DOM_DESCRIPTION, picphone DOM_DESCRIPTION, picmail DOM_DESCRIPTION
-- go
-- 
-- 
ALTER TABLE geo_boundary
ADD ahmgeocode DOM_ID

ALTER TABLE mechanic
ADD ahmcode DOM_ID

create table dbo.unit_document_message_status
(
	idstatus		uniqueidentifier,
	idmessage		int, 
	idstatustype	int,
	dtfrom			datetime, 
	dtthru			datetime,
	primary key		(idstatus, idmessage, idstatustype)
)