/*
 * ER/Studio Data Architect SQL Code Generation
 * Project :      mpm-model.DM1
 *
 * Date Created : Thursday, July 19, 2018 20:15:03
 * Target DBMS : Microsoft SQL Server 2016
 */

/* 
 * TABLE: accounting_calendar 
 */

CREATE TABLE accounting_calendar(
    idinternal    DOM_ID          NULL,
    idcalendar    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK__accounti__18BA6F8C616FF299 PRIMARY KEY CLUSTERED (idcalendar)
)
go



IF OBJECT_ID('accounting_calendar') IS NOT NULL
    PRINT '<<< CREATED TABLE accounting_calendar >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE accounting_calendar >>>'
go

/* 
 * TABLE: acctg_trans_detail 
 */

CREATE TABLE acctg_trans_detail(
    idtradet      DOM_IDENTITY    IDENTITY(1,1),
    idacctra      DOM_IDENTITY    NULL,
    idglacc       DOM_UUID        NULL,
    idinternal    DOM_ID          NULL,
    idcalendar    DOM_IDENTITY    NULL,
    amount        DOM_MONEY       NULL,
    dbcrflag      DOM_FLAG        NULL,
    CONSTRAINT PK__acctg_tr__25E0C083C167B3D5 PRIMARY KEY CLUSTERED (idtradet)
)
go



IF OBJECT_ID('acctg_trans_detail') IS NOT NULL
    PRINT '<<< CREATED TABLE acctg_trans_detail >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE acctg_trans_detail >>>'
go

/* 
 * TABLE: acctg_trans_type 
 */

CREATE TABLE acctg_trans_type(
    idacctratyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__acctg_tr__13BFC8C3863F8A8C PRIMARY KEY CLUSTERED (idacctratyp)
)
go



IF OBJECT_ID('acctg_trans_type') IS NOT NULL
    PRINT '<<< CREATED TABLE acctg_trans_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE acctg_trans_type >>>'
go

/* 
 * TABLE: acctg_transaction 
 */

CREATE TABLE acctg_transaction(
    idacctra         DOM_IDENTITY       IDENTITY(1,1),
    idacctratyp      DOM_INTEGER        NULL,
    dttransaction    DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtentry          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description      DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__acctg_tr__F099E8A0C0E3F365 PRIMARY KEY CLUSTERED (idacctra)
)
go



IF OBJECT_ID('acctg_transaction') IS NOT NULL
    PRINT '<<< CREATED TABLE acctg_transaction >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE acctg_transaction >>>'
go

/* 
 * TABLE: acctg_transaction_status 
 */

CREATE TABLE acctg_transaction_status(
    idstatus        DOM_UUID        NOT NULL,
    idacctra        DOM_IDENTITY    NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__acctg_tr__EBF77C2DF3B0E284 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('acctg_transaction_status') IS NOT NULL
    PRINT '<<< CREATED TABLE acctg_transaction_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE acctg_transaction_status >>>'
go

/* 
 * TABLE: agreement 
 */

CREATE TABLE agreement(
    idagreement    DOM_UUID           NOT NULL,
    idagrtyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__agreemen__43F2607E889F2BDE PRIMARY KEY NONCLUSTERED (idagreement)
)
go



IF OBJECT_ID('agreement') IS NOT NULL
    PRINT '<<< CREATED TABLE agreement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE agreement >>>'
go

/* 
 * TABLE: agreement_item 
 */

CREATE TABLE agreement_item(
    idagrite       DOM_UUID    NOT NULL,
    idagreement    DOM_UUID    NULL,
    CONSTRAINT PK__agreemen__242750098125B8DA PRIMARY KEY NONCLUSTERED (idagrite)
)
go



IF OBJECT_ID('agreement_item') IS NOT NULL
    PRINT '<<< CREATED TABLE agreement_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE agreement_item >>>'
go

/* 
 * TABLE: agreement_motor_item 
 */

CREATE TABLE agreement_motor_item(
    idagrite     DOM_UUID    NOT NULL,
    idvehicle    DOM_UUID    NULL,
    CONSTRAINT PK__agreemen__24275008687D7F1B PRIMARY KEY CLUSTERED (idagrite)
)
go



IF OBJECT_ID('agreement_motor_item') IS NOT NULL
    PRINT '<<< CREATED TABLE agreement_motor_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE agreement_motor_item >>>'
go

/* 
 * TABLE: agreement_role 
 */

CREATE TABLE agreement_role(
    idrole         DOM_UUID        NOT NULL,
    idagreement    DOM_UUID        NULL,
    idroletype     DOM_IDENTITY    NULL,
    idparty        DOM_UUID        NULL,
    username       DOM_NAME        NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__agreemen__F324A06CC17AA87E PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('agreement_role') IS NOT NULL
    PRINT '<<< CREATED TABLE agreement_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE agreement_role >>>'
go

/* 
 * TABLE: agreement_status 
 */

CREATE TABLE agreement_status(
    idstatus        DOM_UUID        NOT NULL,
    idagreement     DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__agreemen__EBF77C2CCFDED468 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('agreement_status') IS NOT NULL
    PRINT '<<< CREATED TABLE agreement_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE agreement_status >>>'
go

/* 
 * TABLE: agreement_type 
 */

CREATE TABLE agreement_type(
    idagrtyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__agreemen__233F41FB7F8F7902 PRIMARY KEY NONCLUSTERED (idagrtyp)
)
go



IF OBJECT_ID('agreement_type') IS NOT NULL
    PRINT '<<< CREATED TABLE agreement_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE agreement_type >>>'
go

/* 
 * TABLE: approval 
 */

CREATE TABLE approval(
    idapproval      DOM_UUID        NOT NULL,
    idapptyp        DOM_INTEGER     NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreq           DOM_UUID        NULL,
    idordite        DOM_UUID        NULL,
    idmessage       DOM_IDENTITY    NULL,
    idleacom        DOM_UUID        NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__approval__3D7FE41CB1081C97 PRIMARY KEY CLUSTERED (idapproval)
)
go



IF OBJECT_ID('approval') IS NOT NULL
    PRINT '<<< CREATED TABLE approval >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE approval >>>'
go

/* 
 * TABLE: approval_stockopname_participant_tm 
 */

CREATE TABLE approval_stockopname_participant_tm(
    idapprovalstockopnameparticipant    DOM_IDENTITY    IDENTITY(1,1),
    participantuserid                   int             NULL,
    lastmodifieduserid                  int             NULL,
    lastmodifieddate                    datetime        NULL,
    CONSTRAINT PK__approval__AD2095157527EEE6 PRIMARY KEY CLUSTERED (idapprovalstockopnameparticipant)
)
go



IF OBJECT_ID('approval_stockopname_participant_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE approval_stockopname_participant_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE approval_stockopname_participant_tm >>>'
go

/* 
 * TABLE: approval_stockopname_status_tp 
 */

CREATE TABLE approval_stockopname_status_tp(
    idapprovalstockopnamestatus    int             NOT NULL,
    description                    nvarchar(50)    NULL,
    lastmodifieddate               datetime        NULL,
    lastmodifieduserid             int             NULL,
    CONSTRAINT PK__approval__443C26A3E92006AC PRIMARY KEY CLUSTERED (idapprovalstockopnamestatus)
)
go



IF OBJECT_ID('approval_stockopname_status_tp') IS NOT NULL
    PRINT '<<< CREATED TABLE approval_stockopname_status_tp >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE approval_stockopname_status_tp >>>'
go

/* 
 * TABLE: approval_stockopname_tm 
 */

CREATE TABLE approval_stockopname_tm(
    idapprovalstockopname          int              IDENTITY(1,1),
    requestnumber                  nvarchar(50)     NULL,
    idstockopname                  int              NULL,
    activityname                   nvarchar(50)     NULL,
    approvaldate                   datetime         NULL,
    idapprovaluser                 int              NULL,
    requestdate                    datetime         NULL,
    requestuserid                  int              NULL,
    idapprovalstockopnamestatus    int              NULL,
    notes                          nvarchar(200)    NULL,
    lastmodifieddate               datetime         NULL,
    lastmodifieduserid             int              NULL,
    CONSTRAINT PK__approval__E300628D54662DF7 PRIMARY KEY CLUSTERED (idapprovalstockopname)
)
go



IF OBJECT_ID('approval_stockopname_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE approval_stockopname_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE approval_stockopname_tm >>>'
go

/* 
 * TABLE: approval_type 
 */

CREATE TABLE approval_type(
    idapptyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__approval__84F7592D939A0370 PRIMARY KEY CLUSTERED (idapptyp)
)
go



IF OBJECT_ID('approval_type') IS NOT NULL
    PRINT '<<< CREATED TABLE approval_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE approval_type >>>'
go

/* 
 * TABLE: association_type 
 */

CREATE TABLE association_type(
    idasstyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__associat__7008109A1CBFD44B PRIMARY KEY NONCLUSTERED (idasstyp)
)
go



IF OBJECT_ID('association_type') IS NOT NULL
    PRINT '<<< CREATED TABLE association_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE association_type >>>'
go

/* 
 * TABLE: attendance 
 */

CREATE TABLE attendance(
    idattendance          DOM_IDENTITY    IDENTITY(1,1),
    idparrol              DOM_UUID        NULL,
    dateattendance        datetime        NULL,
    attendance            bit             NULL,
    attendinfo            varchar(20)     NULL,
    availability          bit             NULL,
    availinfo             varchar(20)     NULL,
    pdi                   bit             NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__attendan__4AF47B20D1E0E4DB PRIMARY KEY NONCLUSTERED (idattendance)
)
go



IF OBJECT_ID('attendance') IS NOT NULL
    PRINT '<<< CREATED TABLE attendance >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE attendance >>>'
go

/* 
 * TABLE: ax_purchaseorder_item 
 */

CREATE TABLE ax_purchaseorder_item(
    idpurchaseorderitem    DOM_INTEGER       NOT NULL,
    ponumber               nvarchar(30)      NULL,
    podate                 datetime          NULL,
    itemname               nvarchar(50)      NULL,
    merk                   nvarchar(30)      NULL,
    type                   nvarchar(30)      NULL,
    branchcode             nvarchar(20)      NULL,
    typecode               nvarchar(10)      NULL,
    price                  decimal(18, 2)    NULL,
    quantity               int               NULL,
    description            nvarchar(100)     NULL,
    CONSTRAINT PK__ax_purch__97AE33BCCB4F2CE8 PRIMARY KEY CLUSTERED (idpurchaseorderitem)
)
go



IF OBJECT_ID('ax_purchaseorder_item') IS NOT NULL
    PRINT '<<< CREATED TABLE ax_purchaseorder_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE ax_purchaseorder_item >>>'
go

/* 
 * TABLE: base_calendar 
 */

CREATE TABLE base_calendar(
    idcalendar     DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    idcaltyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    dtkey          DOM_DATE           NULL,
    workday        DOM_BOOLEAN        DEFAULT 0 NULL,
    seqnum         DOM_INTEGER        NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__base_cal__18BA6F8D3C053ABF PRIMARY KEY NONCLUSTERED (idcalendar)
)
go



IF OBJECT_ID('base_calendar') IS NOT NULL
    PRINT '<<< CREATED TABLE base_calendar >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE base_calendar >>>'
go

/* 
 * TABLE: bill_to 
 */

CREATE TABLE bill_to(
    idbillto      DOM_ID          NOT NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    CONSTRAINT PK__bill_to__495F961AB49FF05B PRIMARY KEY NONCLUSTERED (idbillto)
)
go



IF OBJECT_ID('bill_to') IS NOT NULL
    PRINT '<<< CREATED TABLE bill_to >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE bill_to >>>'
go

/* 
 * TABLE: billing 
 */

CREATE TABLE billing(
    idbilling        DOM_UUID           NOT NULL,
    idbiltyp         DOM_IDENTITY       NULL,
    idinternal       DOM_ID             NULL,
    idbilto          DOM_ID             NULL,
    idbilfro         DOM_ID             NULL,
    billingnumber    DOM_ID             NULL,
    dtcreate         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtdue            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    printcounter     DOM_INTEGER        NULL,
    description      DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__billing__7496EE8783F07F1B PRIMARY KEY NONCLUSTERED (idbilling)
)
go



IF OBJECT_ID('billing') IS NOT NULL
    PRINT '<<< CREATED TABLE billing >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing >>>'
go

/* 
 * TABLE: billing_account 
 */

CREATE TABLE billing_account(
    idbilacc       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__billing___4D91AD7C85D8A38E PRIMARY KEY NONCLUSTERED (idbilacc)
)
go



IF OBJECT_ID('billing_account') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_account >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_account >>>'
go

/* 
 * TABLE: billing_acctg_trans 
 */

CREATE TABLE billing_acctg_trans(
    idbilling    DOM_UUID        NULL,
    idacctra     DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK__billing___F099E8A09A65EF53 PRIMARY KEY CLUSTERED (idacctra)
)
go



IF OBJECT_ID('billing_acctg_trans') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_acctg_trans >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_acctg_trans >>>'
go

/* 
 * TABLE: billing_disbursement 
 */

CREATE TABLE billing_disbursement(
    idbilling        DOM_UUID        NOT NULL,
    idbilacc         DOM_IDENTITY    NULL,
    idvendor         DOM_ID          NULL,
    vendorinvoice    DOM_ID          NULL,
    CONSTRAINT PK__billing___7496EE86A87845BB PRIMARY KEY CLUSTERED (idbilling)
)
go



IF OBJECT_ID('billing_disbursement') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_disbursement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_disbursement >>>'
go

/* 
 * TABLE: billing_issuance 
 */

CREATE TABLE billing_issuance(
    idslip      DOM_UUID    NOT NULL,
    idbilite    DOM_UUID    NULL,
    idinvite    DOM_UUID    NULL,
    idshipto    DOM_ID      NULL,
    qty         DOM_QTY     NULL,
    CONSTRAINT PK__billing___D730145CE9473B13 PRIMARY KEY CLUSTERED (idslip)
)
go



IF OBJECT_ID('billing_issuance') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_issuance >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_issuance >>>'
go

/* 
 * TABLE: billing_item 
 */

CREATE TABLE billing_item(
    idbilite           DOM_UUID           NOT NULL,
    idbilling          DOM_UUID           NULL,
    idinvite           DOM_UUID           NULL,
    idproduct          DOM_ID             NULL,
    idwetyp            DOM_IDENTITY       NULL,
    idbilitetyp        DOM_IDENTITY       NULL,
    idfeature          DOM_INTEGER        NULL,
    seq                DOM_INTEGER        NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    qty                DOM_QTY            NULL,
    baseprice          DOM_MONEY          NULL,
    unitprice          DOM_MONEY          NULL,
    discount           DOM_MONEY          NULL,
    taxamount          DOM_MONEY          NULL,
    totalamount        DOM_MONEY          NULL,
    CONSTRAINT PK__billing___4791E44BF0F1EDE9 PRIMARY KEY NONCLUSTERED (idbilite)
)
go



IF OBJECT_ID('billing_item') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_item >>>'
go

/* 
 * TABLE: billing_item_type 
 */

CREATE TABLE billing_item_type(
    idbilitetyp    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__billing___4009D902CDD3EAA9 PRIMARY KEY CLUSTERED (idbilitetyp)
)
go



IF OBJECT_ID('billing_item_type') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_item_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_item_type >>>'
go

/* 
 * TABLE: billing_njb 
 */

CREATE TABLE billing_njb(
    idbilling             DOM_UUID          NOT NULL,
    nonjb                 varchar(30)       NOT NULL,
    totalbaseprice        decimal(18, 2)    NULL,
    totaltaxamount        decimal(18, 2)    NULL,
    mischarge             decimal(18, 2)    NULL,
    totalunitprice        decimal(18, 2)    NULL,
    totaldiscount         decimal(18, 2)    NULL,
    grandtotalamount      decimal(18, 2)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    CONSTRAINT PK__billing___7496EE87220D4D11 PRIMARY KEY NONCLUSTERED (idbilling),
    UNIQUE (nonjb)
)
go



IF OBJECT_ID('billing_njb') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_njb >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_njb >>>'
go

/* 
 * TABLE: billing_nsc 
 */

CREATE TABLE billing_nsc(
    idbilling             DOM_UUID          NOT NULL,
    nonsc                 varchar(30)       NOT NULL,
    totalbaseprice        decimal(18, 2)    NULL,
    totaltaxamount        decimal(18, 2)    NULL,
    mischarge             decimal(18, 2)    NULL,
    totalunitprice        decimal(18, 2)    NULL,
    totaldiscount         decimal(18, 2)    NULL,
    grandtotalamount      decimal(18, 2)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    CONSTRAINT PK__billing___7496EE870206F991 PRIMARY KEY NONCLUSTERED (idbilling),
    UNIQUE (nonsc)
)
go



IF OBJECT_ID('billing_nsc') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_nsc >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_nsc >>>'
go

/* 
 * TABLE: billing_receipt 
 */

CREATE TABLE billing_receipt(
    idbilling         DOM_UUID        NOT NULL,
    idbilacc          DOM_IDENTITY    NULL,
    proformanumber    DOM_ID          NULL,
    CONSTRAINT PK__billing___7496EE8656D34312 PRIMARY KEY CLUSTERED (idbilling)
)
go



IF OBJECT_ID('billing_receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_receipt >>>'
go

/* 
 * TABLE: billing_role 
 */

CREATE TABLE billing_role(
    idrole        DOM_UUID        NOT NULL,
    idbilling     DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__billing___F324A06C70565643 PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('billing_role') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_role >>>'
go

/* 
 * TABLE: billing_status 
 */

CREATE TABLE billing_status(
    idstatus        DOM_UUID        NOT NULL,
    idbilling       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__billing___EBF77C2C1CC9C442 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('billing_status') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_status >>>'
go

/* 
 * TABLE: billing_type 
 */

CREATE TABLE billing_type(
    idbiltyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__billing___A9563D4D39307547 PRIMARY KEY NONCLUSTERED (idbiltyp)
)
go



IF OBJECT_ID('billing_type') IS NOT NULL
    PRINT '<<< CREATED TABLE billing_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE billing_type >>>'
go

/* 
 * TABLE: booking 
 */

CREATE TABLE booking(
    idbooking             DOM_IDENTITY    IDENTITY(1,1),
    idbookingstatus       DOM_INTEGER     NULL,
    nobooking             varchar(4)      NOT NULL,
    customername          varchar(100)    NULL,
    vehiclenumber         varchar(20)     NULL,
    datebook              datetime        NULL,
    bookingtime           datetime        NULL,
    waitinglimit          datetime        NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__booking__DBB298C04B6C12D4 PRIMARY KEY NONCLUSTERED (idbooking),
    UNIQUE (nobooking)
)
go



IF OBJECT_ID('booking') IS NOT NULL
    PRINT '<<< CREATED TABLE booking >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE booking >>>'
go

/* 
 * TABLE: booking_slot 
 */

CREATE TABLE booking_slot(
    idbooslo       DOM_UUID        NOT NULL,
    idcalendar     DOM_IDENTITY    NULL,
    idbooslostd    DOM_UUID        NULL,
    slotnumber     DOM_ID          NULL,
    CONSTRAINT PK__booking___23A4939876BD9B61 PRIMARY KEY NONCLUSTERED (idbooslo)
)
go



IF OBJECT_ID('booking_slot') IS NOT NULL
    PRINT '<<< CREATED TABLE booking_slot >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE booking_slot >>>'
go

/* 
 * TABLE: booking_slot_standard 
 */

CREATE TABLE booking_slot_standard(
    idbooslostd    DOM_UUID           NOT NULL,
    idboktyp       DOM_IDENTITY       NULL,
    idinternal     DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    capacity       DOM_INTEGER        NULL,
    starthour      DOM_INTEGER        NULL,
    startminute    DOM_INTEGER        NULL,
    endhour        DOM_INTEGER        NULL,
    endminute      DOM_INTEGER        NULL,
    CONSTRAINT PK__booking___40DDB7505E7BA0CF PRIMARY KEY NONCLUSTERED (idbooslostd)
)
go



IF OBJECT_ID('booking_slot_standard') IS NOT NULL
    PRINT '<<< CREATED TABLE booking_slot_standard >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE booking_slot_standard >>>'
go

/* 
 * TABLE: booking_status 
 */

CREATE TABLE booking_status(
    idbookingstatus    DOM_INTEGER        NOT NULL,
    description        DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__booking___AE92624299DC3829 PRIMARY KEY CLUSTERED (idbookingstatus)
)
go



IF OBJECT_ID('booking_status') IS NOT NULL
    PRINT '<<< CREATED TABLE booking_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE booking_status >>>'
go

/* 
 * TABLE: booking_type 
 */

CREATE TABLE booking_type(
    idboktyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__booking___4E3A828D533F1184 PRIMARY KEY NONCLUSTERED (idboktyp)
)
go



IF OBJECT_ID('booking_type') IS NOT NULL
    PRINT '<<< CREATED TABLE booking_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE booking_type >>>'
go

/* 
 * TABLE: branch 
 */

CREATE TABLE branch(
    idbranchcategory    DOM_IDENTITY    NULL,
    idinternal          DOM_ID          NOT NULL,
    CONSTRAINT PK__branch__2BD308915FE547BF PRIMARY KEY NONCLUSTERED (idinternal)
)
go



IF OBJECT_ID('branch') IS NOT NULL
    PRINT '<<< CREATED TABLE branch >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE branch >>>'
go

/* 
 * TABLE: broker_type 
 */

CREATE TABLE broker_type(
    idbrotyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__broker_t__019748686B12B25D PRIMARY KEY CLUSTERED (idbrotyp)
)
go



IF OBJECT_ID('broker_type') IS NOT NULL
    PRINT '<<< CREATED TABLE broker_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE broker_type >>>'
go

/* 
 * TABLE: calendar_type 
 */

CREATE TABLE calendar_type(
    idcaltyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__calendar__FC1377906648B550 PRIMARY KEY NONCLUSTERED (idcaltyp)
)
go



IF OBJECT_ID('calendar_type') IS NOT NULL
    PRINT '<<< CREATED TABLE calendar_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE calendar_type >>>'
go

/* 
 * TABLE: carrier 
 */

CREATE TABLE carrier(
    idcarrier    DOM_INTEGER     NOT NULL,
    idvehicle    DOM_UUID        NULL,
    idparty      DOM_UUID        NULL,
    idreason     DOM_INTEGER     NULL,
    idreltyp     DOM_IDENTITY    NULL,
    CONSTRAINT PK__carrier__B400F334B5A13CF2 PRIMARY KEY CLUSTERED (idcarrier)
)
go



IF OBJECT_ID('carrier') IS NOT NULL
    PRINT '<<< CREATED TABLE carrier >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE carrier >>>'
go

/* 
 * TABLE: category_asset_tp 
 */

CREATE TABLE category_asset_tp(
    idcategoryasset       int             IDENTITY(1,1),
    description           nvarchar(50)    NULL,
    statuscode            int             NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    CONSTRAINT PK__category__447874BE0A7E42B0 PRIMARY KEY CLUSTERED (idcategoryasset)
)
go



IF OBJECT_ID('category_asset_tp') IS NOT NULL
    PRINT '<<< CREATED TABLE category_asset_tp >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE category_asset_tp >>>'
go

/* 
 * TABLE: charge_to_type 
 */

CREATE TABLE charge_to_type(
    idchargeto     int            NOT NULL,
    description    varchar(30)    NULL,
    CONSTRAINT PK__charge_t__1576B9F2EC507458 PRIMARY KEY CLUSTERED (idchargeto)
)
go



IF OBJECT_ID('charge_to_type') IS NOT NULL
    PRINT '<<< CREATED TABLE charge_to_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE charge_to_type >>>'
go

/* 
 * TABLE: city 
 */

CREATE TABLE city(
    idgeobou    DOM_UUID    NOT NULL,
    CONSTRAINT PK__city__96A5A05E866FEF39 PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



IF OBJECT_ID('city') IS NOT NULL
    PRINT '<<< CREATED TABLE city >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE city >>>'
go

/* 
 * TABLE: claim_detail 
 */

CREATE TABLE claim_detail(
    idclaim               DOM_IDENTITY      IDENTITY(1,1),
    idcustomer            DOM_ID            NULL,
    idsympthomdatam       DOM_IDENTITY      NULL,
    iddatadamage          int               NULL,
    noclaim               int               NOT NULL,
    claimtype             varchar(5)        NULL,
    statusclaim           varchar(20)       NULL,
    noticket              varchar(20)       NULL,
    submissiongroup_      varchar(20)       NULL,
    nolkh                 varchar(30)       NULL,
    datelkh               datetime          NULL,
    noho                  datetime          NULL,
    dateho                date              NULL,
    notype                varchar(5)        NULL,
    marketname            varchar(30)       NULL,
    datebuy               datetime          NULL,
    noproduction          varchar(15)       NULL,
    brokendate            datetime          NULL,
    kmbroken              int               NULL,
    servicedate           datetime          NULL,
    rank                  char(1)           NULL,
    finishdate            datetime          NULL,
    analysisresult        varchar(100)      NULL,
    amount                decimal(18, 2)    NULL,
    mainpart              bit               NULL,
    dsubtotaljob          decimal(18, 2)    NULL,
    transferdate          datetime          NULL,
    nominaltransfer       decimal(18, 2)    NULL,
    nodocs                varchar(15)       NULL,
    replace_              varchar(10)       NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    CONSTRAINT PK__claim_de__ED88E5AD7F6BFF85 PRIMARY KEY NONCLUSTERED (idclaim),
    UNIQUE (noclaim)
)
go



IF OBJECT_ID('claim_detail') IS NOT NULL
    PRINT '<<< CREATED TABLE claim_detail >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE claim_detail >>>'
go

/* 
 * TABLE: communication_event 
 */

CREATE TABLE communication_event(
    idcomevt    DOM_UUID        NOT NULL,
    idparrel    DOM_UUID        NULL,
    idevetyp    DOM_IDENTITY    NULL,
    note        DOM_LONGDESC    NULL,
    dtfrom      DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru      DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__communic__EB114D63C73D3E3F PRIMARY KEY CLUSTERED (idcomevt)
)
go



IF OBJECT_ID('communication_event') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event >>>'
go

/* 
 * TABLE: communication_event_cdb 
 */

CREATE TABLE communication_event_cdb(
    idcomevt           DOM_UUID           NOT NULL,
    idcustomer         DOM_ID             NULL,
    idprovince         DOM_UUID           NULL,
    idcity             DOM_UUID           NULL,
    iddistrict         DOM_UUID           NULL,
    idreligion         DOM_IDENTITY       NULL,
    address            DOM_LONGDESC       NULL,
    addresssurat       DOM_ADDRESS        NULL,
    hobby              DOM_DESCRIPTION    NULL,
    homestatus         DOM_DESCRIPTION    NULL,
    exponemonth        DOM_DESCRIPTION    NULL,
    job                DOM_DESCRIPTION    NULL,
    lasteducation      DOM_DESCRIPTION    NULL,
    owncellphone       DOM_DESCRIPTION    NULL,
    hondarefference    DOM_DESCRIPTION    NULL,
    ownvehiclebrand    DOM_DESCRIPTION    NULL,
    ownvehicletype     DOM_DESCRIPTION    NULL,
    buyfor             DOM_DESCRIPTION    NULL,
    vehicleuser        DOM_DESCRIPTION    NULL,
    facebook           DOM_DESCRIPTION    NULL,
    instagram          DOM_DESCRIPTION    NULL,
    twitter            DOM_DESCRIPTION    NULL,
    youtube            DOM_DESCRIPTION    NULL,
    email              DOM_DESCRIPTION    NULL,
    iscityidadd        DOM_BOOLEAN        DEFAULT 0 NULL,
    ismailadd          DOM_BOOLEAN        DEFAULT 0 NULL,
    idprovincesurat    DOM_UUID           NULL,
    idcitysurat        DOM_UUID           NULL,
    iddistrictsurat    DOM_UUID           NULL,
    idvillagesurat     DOM_UUID           NULL,
    jenispembayaran    DOM_DESCRIPTION    NULL,
    groupcustomer      DOM_DESCRIPTION    NULL,
    keterangan         DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__communic__EB114D6347B4DC25 PRIMARY KEY CLUSTERED (idcomevt)
)
go



IF OBJECT_ID('communication_event_cdb') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event_cdb >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event_cdb >>>'
go

/* 
 * TABLE: communication_event_delivery 
 */

CREATE TABLE communication_event_delivery(
    idcomevt         DOM_UUID        NOT NULL,
    idinternal       DOM_ID          NULL,
    idcustomer       DOM_ID          NULL,
    bussinesscode    DOM_ID          NULL,
    b1               DOM_ID          NULL,
    b2               DOM_ID          NULL,
    b3               DOM_ID          NULL,
    b4               DOM_ID          NULL,
    c11rate          DOM_INTEGER     NULL,
    c11reason        DOM_LONGDESC    NULL,
    c11hope          DOM_LONGDESC    NULL,
    c12rate          DOM_INTEGER     NULL,
    c12reason        DOM_LONGDESC    NULL,
    c13hope          DOM_LONGDESC    NULL,
    c14rate          DOM_INTEGER     NULL,
    c14reason        DOM_LONGDESC    NULL,
    c14hope          DOM_LONGDESC    NULL,
    c15rate          DOM_INTEGER     NULL,
    c15reason        DOM_LONGDESC    NULL,
    c15hope          DOM_LONGDESC    NULL,
    c16rate          DOM_INTEGER     NULL,
    c16reason        DOM_LONGDESC    NULL,
    c16hope          DOM_LONGDESC    NULL,
    c17rate          DOM_INTEGER     NULL,
    c17reason        DOM_LONGDESC    NULL,
    c17hope          DOM_LONGDESC    NULL,
    c18rate          DOM_INTEGER     NULL,
    c18reason        DOM_LONGDESC    NULL,
    c18hope          DOM_LONGDESC    NULL,
    c19rate          DOM_INTEGER     NULL,
    c19reason        DOM_LONGDESC    NULL,
    c19hope          DOM_LONGDESC    NULL,
    c110rate         DOM_INTEGER     NULL,
    c110reason       DOM_LONGDESC    NULL,
    c110hope         DOM_LONGDESC    NULL,
    c2rate           DOM_INTEGER     NULL,
    c3option         DOM_LONGDESC    NULL,
    d1name           DOM_LONGDESC    NULL,
    d2gender         DOM_LONGDESC    NULL,
    d3age            DOM_INTEGER     NULL,
    d4phone          DOM_LONGDESC    NULL,
    d5vehicle        DOM_LONGDESC    NULL,
    d6education      DOM_LONGDESC    NULL,
    dealername       DOM_LONGDESC    NULL,
    dealeraddress    DOM_LONGDESC    NULL,
    c13rate          DOM_LONGDESC    NULL,
    c13reason        DOM_LONGDESC    NULL,
    c12hope          DOM_LONGDESC    NULL,
    CONSTRAINT PK__communic__EB114D63E66EAF54 PRIMARY KEY CLUSTERED (idcomevt)
)
go



IF OBJECT_ID('communication_event_delivery') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event_delivery >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event_delivery >>>'
go

/* 
 * TABLE: communication_event_prospect 
 */

CREATE TABLE communication_event_prospect(
    idcomevt        DOM_UUID       NOT NULL,
    idprospect      DOM_UUID       NULL,
    idsaletype      DOM_INTEGER    NULL,
    idproduct       DOM_ID         NULL,
    idcolor         DOM_INTEGER    NULL,
    interest        DOM_BOOLEAN    DEFAULT 0 NULL,
    qty             DOM_QTY        NULL,
    purchaseplan    DOM_ID         NULL,
    connect         DOM_BOOLEAN    DEFAULT 0 NULL,
    CONSTRAINT PK__communic__EB114D636BB33A72 PRIMARY KEY CLUSTERED (idcomevt)
)
go



IF OBJECT_ID('communication_event_prospect') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event_prospect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event_prospect >>>'
go

/* 
 * TABLE: communication_event_purpose 
 */

CREATE TABLE communication_event_purpose(
    idcomevt         DOM_UUID        NOT NULL,
    idpurposetype    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK__communic__6A88F3D2926D6D2A PRIMARY KEY CLUSTERED (idcomevt, idpurposetype)
)
go



IF OBJECT_ID('communication_event_purpose') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event_purpose >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event_purpose >>>'
go

/* 
 * TABLE: communication_event_role 
 */

CREATE TABLE communication_event_role(
    idrole        DOM_UUID        NOT NULL,
    idcomevt      DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__communic__F324A06D78EDD9C1 PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('communication_event_role') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event_role >>>'
go

/* 
 * TABLE: communication_event_status 
 */

CREATE TABLE communication_event_status(
    idstatus        DOM_UUID        NOT NULL,
    idcomevt        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__communic__EBF77C2D0CB8F596 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('communication_event_status') IS NOT NULL
    PRINT '<<< CREATED TABLE communication_event_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE communication_event_status >>>'
go

/* 
 * TABLE: contact_mechanism 
 */

CREATE TABLE contact_mechanism(
    idcontact        DOM_UUID           NOT NULL,
    idcontacttype    DOM_INTEGER        NULL,
    description      DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__contact___A36DC10716C8767E PRIMARY KEY NONCLUSTERED (idcontact)
)
go



IF OBJECT_ID('contact_mechanism') IS NOT NULL
    PRINT '<<< CREATED TABLE contact_mechanism >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE contact_mechanism >>>'
go

/* 
 * TABLE: contact_purpose 
 */

CREATE TABLE contact_purpose(
    idcontact        DOM_UUID        NOT NULL,
    idpurposetype    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK__contact___22F47FB79F1F57DB PRIMARY KEY CLUSTERED (idcontact, idpurposetype)
)
go



IF OBJECT_ID('contact_purpose') IS NOT NULL
    PRINT '<<< CREATED TABLE contact_purpose >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE contact_purpose >>>'
go

/* 
 * TABLE: container 
 */

CREATE TABLE container(
    idcontainer    DOM_UUID           NOT NULL,
    idfacility     DOM_UUID           NULL,
    idcontyp       DOM_IDENTITY       NULL,
    code           DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__containe__D040497518CB53E3 PRIMARY KEY NONCLUSTERED (idcontainer)
)
go



IF OBJECT_ID('container') IS NOT NULL
    PRINT '<<< CREATED TABLE container >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE container >>>'
go

/* 
 * TABLE: container_type 
 */

CREATE TABLE container_type(
    idcontyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__containe__40EAA9B3148F03D6 PRIMARY KEY NONCLUSTERED (idcontyp)
)
go



IF OBJECT_ID('container_type') IS NOT NULL
    PRINT '<<< CREATED TABLE container_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE container_type >>>'
go

/* 
 * TABLE: correction_note 
 */

CREATE TABLE correction_note(
    idcorrectnote       DOM_INTEGER        NOT NULL,
    idproduct           DOM_ID             NULL,
    idspgclaim          int                NULL,
    idissue             int                NULL,
    correctionnumber    DOM_DESCRIPTION    NULL,
    qty                 DOM_QTY            NULL,
    dtcorrection        datetime           NULL,
    CONSTRAINT PK__correcti__E75DBDD9A3A722E7 PRIMARY KEY CLUSTERED (idcorrectnote)
)
go



IF OBJECT_ID('correction_note') IS NOT NULL
    PRINT '<<< CREATED TABLE correction_note >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE correction_note >>>'
go

/* 
 * TABLE: customer 
 */

CREATE TABLE customer(
    idcustomer            DOM_ID          NOT NULL,
    idmarketcategory      DOM_IDENTITY    NULL,
    idparty               DOM_UUID        NULL,
    idroletype            DOM_IDENTITY    NULL,
    idmpm                 DOM_ID          NULL,
    idsegmen              DOM_INTEGER     NULL,
    idcustomertype        DOM_INTEGER     NULL,
    useshipmentaddress    DOM_BOOLEAN     DEFAULT 0 NULL,
    CONSTRAINT PK__customer__53EFD0A731118153 PRIMARY KEY NONCLUSTERED (idcustomer)
)
go



IF OBJECT_ID('customer') IS NOT NULL
    PRINT '<<< CREATED TABLE customer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE customer >>>'
go

/* 
 * TABLE: customer_order 
 */

CREATE TABLE customer_order(
    idorder       DOM_UUID    NOT NULL,
    idvehicle     DOM_UUID    NULL,
    idcustomer    DOM_ID      NULL,
    idinternal    DOM_ID      NULL,
    idbillto      DOM_ID      NULL,
    CONSTRAINT PK__customer__44DCCDAB2B574250 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('customer_order') IS NOT NULL
    PRINT '<<< CREATED TABLE customer_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE customer_order >>>'
go

/* 
 * TABLE: customer_quotation 
 */

CREATE TABLE customer_quotation(
    idquote            DOM_UUID    NOT NULL,
    idinternal         DOM_ID      NULL,
    idcustomer         DOM_ID      NULL,
    quotationnumber    DOM_ID      NULL,
    CONSTRAINT PK__customer__4F00C6E76AF37B93 PRIMARY KEY CLUSTERED (idquote)
)
go



IF OBJECT_ID('customer_quotation') IS NOT NULL
    PRINT '<<< CREATED TABLE customer_quotation >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE customer_quotation >>>'
go

/* 
 * TABLE: customer_relationship 
 */

CREATE TABLE customer_relationship(
    idparrel      DOM_UUID    NOT NULL,
    idinternal    DOM_ID      NULL,
    idcustomer    DOM_ID      NULL,
    CONSTRAINT PK__customer__86F7AE0ADF4EDB23 PRIMARY KEY CLUSTERED (idparrel)
)
go



IF OBJECT_ID('customer_relationship') IS NOT NULL
    PRINT '<<< CREATED TABLE customer_relationship >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE customer_relationship >>>'
go

/* 
 * TABLE: customer_request_item 
 */

CREATE TABLE customer_request_item(
    idreqitem     DOM_UUID     NOT NULL,
    idcustomer    DOM_ID       NULL,
    qty           DOM_QTY      NULL,
    unitprice     DOM_MONEY    NULL,
    bbn           DOM_MONEY    NULL,
    idframe       DOM_ID       NULL,
    idmachine     DOM_ID       NOT NULL,
    CONSTRAINT PK__customer__D27E3AA60B647F4B PRIMARY KEY CLUSTERED (idreqitem)
)
go



IF OBJECT_ID('customer_request_item') IS NOT NULL
    PRINT '<<< CREATED TABLE customer_request_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE customer_request_item >>>'
go

/* 
 * TABLE: customer_type 
 */

CREATE TABLE customer_type(
    idcostumertype    DOM_INTEGER        NOT NULL,
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__customer__70DC1EA531D62D75 PRIMARY KEY CLUSTERED (idcostumertype)
)
go



IF OBJECT_ID('customer_type') IS NOT NULL
    PRINT '<<< CREATED TABLE customer_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE customer_type >>>'
go

/* 
 * TABLE: data_damage 
 */

CREATE TABLE data_damage(
    iddatadamage    int            IDENTITY(1,1),
    descdamage      varchar(30)    NULL,
    CONSTRAINT PK__data_dam__5A516AC96FAC8C4F PRIMARY KEY CLUSTERED (iddatadamage)
)
go



IF OBJECT_ID('data_damage') IS NOT NULL
    PRINT '<<< CREATED TABLE data_damage >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE data_damage >>>'
go

/* 
 * TABLE: dealer_claim_type 
 */

CREATE TABLE dealer_claim_type(
    idclaimtype    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__dealer_c__E4290365E704A48A PRIMARY KEY NONCLUSTERED (idclaimtype)
)
go



IF OBJECT_ID('dealer_claim_type') IS NOT NULL
    PRINT '<<< CREATED TABLE dealer_claim_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE dealer_claim_type >>>'
go

/* 
 * TABLE: dealer_reminder_type 
 */

CREATE TABLE dealer_reminder_type(
    idremindertype    DOM_IDENTITY       IDENTITY(1,1),
    description       DOM_DESCRIPTION    NULL,
    messages          DOM_LONGDESC       NULL,
    CONSTRAINT PK__dealer_r__8950BC5BFF764351 PRIMARY KEY NONCLUSTERED (idremindertype)
)
go



IF OBJECT_ID('dealer_reminder_type') IS NOT NULL
    PRINT '<<< CREATED TABLE dealer_reminder_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE dealer_reminder_type >>>'
go

/* 
 * TABLE: deliverable_type 
 */

CREATE TABLE deliverable_type(
    iddeltype      DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__delivera__0A4A3F9C22ACBD84 PRIMARY KEY CLUSTERED (iddeltype)
)
go



IF OBJECT_ID('deliverable_type') IS NOT NULL
    PRINT '<<< CREATED TABLE deliverable_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE deliverable_type >>>'
go

/* 
 * TABLE: delivery_order 
 */

CREATE TABLE delivery_order(
    invoicevendornumber    varchar(30)    NULL,
    idbilling              DOM_UUID       NOT NULL,
    CONSTRAINT PK__delivery__7496EE8667D7A0D8 PRIMARY KEY CLUSTERED (idbilling)
)
go



IF OBJECT_ID('delivery_order') IS NOT NULL
    PRINT '<<< CREATED TABLE delivery_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE delivery_order >>>'
go

/* 
 * TABLE: delivery_order_status 
 */

CREATE TABLE delivery_order_status(
    iddeliveryorderstatus    int            NOT NULL,
    description              varchar(50)    NULL,
    CONSTRAINT PK__delivery__C529B9621C6E4B9E PRIMARY KEY CLUSTERED (iddeliveryorderstatus)
)
go



IF OBJECT_ID('delivery_order_status') IS NOT NULL
    PRINT '<<< CREATED TABLE delivery_order_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE delivery_order_status >>>'
go

/* 
 * TABLE: dimension 
 */

CREATE TABLE dimension(
    iddimension    DOM_ID             NOT NULL,
    iddimtyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__dimensio__9E9D4CC28A7AB855 PRIMARY KEY CLUSTERED (iddimension)
)
go



IF OBJECT_ID('dimension') IS NOT NULL
    PRINT '<<< CREATED TABLE dimension >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE dimension >>>'
go

/* 
 * TABLE: dimension_type 
 */

CREATE TABLE dimension_type(
    iddimtyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__dimensio__2164C0FEDA085531 PRIMARY KEY CLUSTERED (iddimtyp)
)
go



IF OBJECT_ID('dimension_type') IS NOT NULL
    PRINT '<<< CREATED TABLE dimension_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE dimension_type >>>'
go

/* 
 * TABLE: disbursement 
 */

CREATE TABLE disbursement(
    idpayment    DOM_UUID    NOT NULL,
    CONSTRAINT PK__disburse__64452FA4796CEFBF PRIMARY KEY NONCLUSTERED (idpayment)
)
go



IF OBJECT_ID('disbursement') IS NOT NULL
    PRINT '<<< CREATED TABLE disbursement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE disbursement >>>'
go

/* 
 * TABLE: dispatch_status 
 */

CREATE TABLE dispatch_status(
    iddispatchstatus    DOM_INTEGER    NOT NULL,
    description         varchar(30)    NULL,
    CONSTRAINT PK__dispatch__B7C439D07D06A40F PRIMARY KEY CLUSTERED (iddispatchstatus)
)
go



IF OBJECT_ID('dispatch_status') IS NOT NULL
    PRINT '<<< CREATED TABLE dispatch_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE dispatch_status >>>'
go

/* 
 * TABLE: district 
 */

CREATE TABLE district(
    idgeobou    DOM_UUID    NOT NULL,
    CONSTRAINT PK__district__96A5A05F2C13BD94 PRIMARY KEY CLUSTERED (idgeobou)
)
go



IF OBJECT_ID('district') IS NOT NULL
    PRINT '<<< CREATED TABLE district >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE district >>>'
go

/* 
 * TABLE: document_type 
 */

CREATE TABLE document_type(
    iddoctype      DOM_INTEGER        NOT NULL,
    idparent       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__document__C880892C563CCC13 PRIMARY KEY CLUSTERED (iddoctype)
)
go



IF OBJECT_ID('document_type') IS NOT NULL
    PRINT '<<< CREATED TABLE document_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE document_type >>>'
go

/* 
 * TABLE: documents 
 */

CREATE TABLE documents(
    iddocument    DOM_UUID           NOT NULL,
    iddoctype     DOM_INTEGER        NULL,
    note          DOM_DESCRIPTION    NULL,
    dtcreate      DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__document__2B52541309029529 PRIMARY KEY CLUSTERED (iddocument)
)
go



IF OBJECT_ID('documents') IS NOT NULL
    PRINT '<<< CREATED TABLE documents >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE documents >>>'
go

/* 
 * TABLE: driver 
 */

CREATE TABLE driver(
    idparrol      DOM_UUID       NOT NULL,
    idvendor      DOM_ID         NULL,
    iddriver      varchar(90)    NULL,
    isexternal    char(5)        NULL,
    CONSTRAINT PK__driver__86F77EC2305DFADD PRIMARY KEY CLUSTERED (idparrol)
)
go



IF OBJECT_ID('driver') IS NOT NULL
    PRINT '<<< CREATED TABLE driver >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE driver >>>'
go

/* 
 * TABLE: electronic_address 
 */

CREATE TABLE electronic_address(
    idcontact    DOM_UUID       NOT NULL,
    address      DOM_ADDRESS    NULL,
    CONSTRAINT PK__electron__A36DC1072D461A7B PRIMARY KEY NONCLUSTERED (idcontact)
)
go



IF OBJECT_ID('electronic_address') IS NOT NULL
    PRINT '<<< CREATED TABLE electronic_address >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE electronic_address >>>'
go

/* 
 * TABLE: employee 
 */

CREATE TABLE employee(
    employeenumber    DOM_ID      NOT NULL,
    idparty           DOM_UUID    NULL,
    idparrol          DOM_UUID    NULL,
    CONSTRAINT PK__employee__93736337988C42A4 PRIMARY KEY NONCLUSTERED (employeenumber)
)
go



IF OBJECT_ID('employee') IS NOT NULL
    PRINT '<<< CREATED TABLE employee >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE employee >>>'
go

/* 
 * TABLE: employee_customer_relationship 
 */

CREATE TABLE employee_customer_relationship(
    idparrel          DOM_UUID    NOT NULL,
    employeenumber    DOM_ID      NULL,
    idcustomer        DOM_ID      NULL,
    CONSTRAINT PK__employee__86F7AE0A08651CE0 PRIMARY KEY CLUSTERED (idparrel)
)
go



IF OBJECT_ID('employee_customer_relationship') IS NOT NULL
    PRINT '<<< CREATED TABLE employee_customer_relationship >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE employee_customer_relationship >>>'
go

/* 
 * TABLE: event_type 
 */

CREATE TABLE event_type(
    idevetyp       DOM_IDENTITY       IDENTITY(1,1),
    idprneve       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__event_ty__CD02FF5B26B0EA6F PRIMARY KEY NONCLUSTERED (idevetyp)
)
go



IF OBJECT_ID('event_type') IS NOT NULL
    PRINT '<<< CREATED TABLE event_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE event_type >>>'
go

/* 
 * TABLE: exchange_part 
 */

CREATE TABLE exchange_part(
    idspgclaimaction    int               NULL,
    idproduct           DOM_ID            NULL,
    idspgclaimdetail    DOM_IDENTITY      NOT NULL,
    qty                 decimal(14, 2)    NULL,
    dtexchangepart      datetime          NULL,
    CONSTRAINT PK__exchange__E9C600350C9DD7C6 PRIMARY KEY CLUSTERED (idspgclaimdetail)
)
go



IF OBJECT_ID('exchange_part') IS NOT NULL
    PRINT '<<< CREATED TABLE exchange_part >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE exchange_part >>>'
go

/* 
 * TABLE: external_acctg_trans 
 */

CREATE TABLE external_acctg_trans(
    idacctra    DOM_IDENTITY    NOT NULL,
    idparfro    DOM_UUID        NULL,
    idparto     DOM_UUID        NULL,
    CONSTRAINT PK__external__F099E8A004F617ED PRIMARY KEY CLUSTERED (idacctra)
)
go



IF OBJECT_ID('external_acctg_trans') IS NOT NULL
    PRINT '<<< CREATED TABLE external_acctg_trans >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE external_acctg_trans >>>'
go

/* 
 * TABLE: facility 
 */

CREATE TABLE facility(
    idfacility        DOM_UUID           NOT NULL,
    idfacilitytype    DOM_IDENTITY       NULL,
    idpartof          DOM_UUID           NULL,
    idfa              DOM_IDENTITY       NULL,
    code              DOM_ID             NULL,
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__facility__6469C7C0310B5BE9 PRIMARY KEY NONCLUSTERED (idfacility)
)
go



IF OBJECT_ID('facility') IS NOT NULL
    PRINT '<<< CREATED TABLE facility >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility >>>'
go

/* 
 * TABLE: facility_contact_mechanism 
 */

CREATE TABLE facility_contact_mechanism(
    idconmecpur      DOM_UUID        NOT NULL,
    idfacility       DOM_UUID        NULL,
    idcontact        DOM_UUID        NULL,
    idpurposetype    DOM_IDENTITY    NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__facility__3319DA5FD0123862 PRIMARY KEY NONCLUSTERED (idconmecpur)
)
go



IF OBJECT_ID('facility_contact_mechanism') IS NOT NULL
    PRINT '<<< CREATED TABLE facility_contact_mechanism >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility_contact_mechanism >>>'
go

/* 
 * TABLE: facility_status 
 */

CREATE TABLE facility_status(
    idstatus        DOM_UUID        NOT NULL,
    idfacility      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__facility__EBF77C2CE24D648A PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('facility_status') IS NOT NULL
    PRINT '<<< CREATED TABLE facility_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility_status >>>'
go

/* 
 * TABLE: facility_type 
 */

CREATE TABLE facility_type(
    idfacilitytype    DOM_IDENTITY       IDENTITY(1,1),
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__facility__AB669AF96C381B32 PRIMARY KEY NONCLUSTERED (idfacilitytype)
)
go



IF OBJECT_ID('facility_type') IS NOT NULL
    PRINT '<<< CREATED TABLE facility_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility_type >>>'
go

/* 
 * TABLE: feature 
 */

CREATE TABLE feature(
    idfeature      DOM_INTEGER        NOT NULL,
    idfeatyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__feature__FD52071E61A7F7F1 PRIMARY KEY NONCLUSTERED (idfeature)
)
go



IF OBJECT_ID('feature') IS NOT NULL
    PRINT '<<< CREATED TABLE feature >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE feature >>>'
go

/* 
 * TABLE: feature_applicable 
 */

CREATE TABLE feature_applicable(
    idapplicability    DOM_UUID        NOT NULL,
    idfeature          DOM_INTEGER     NULL,
    idproduct          DOM_ID          NULL,
    idfeatyp           DOM_IDENTITY    NULL,
    idowner            DOM_UUID        NULL,
    boolvalue          DOM_BOOLEAN     DEFAULT 0 NULL,
    dtfrom             DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru             DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__feature___B99BE8A254FD15B4 PRIMARY KEY CLUSTERED (idapplicability)
)
go



IF OBJECT_ID('feature_applicable') IS NOT NULL
    PRINT '<<< CREATED TABLE feature_applicable >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE feature_applicable >>>'
go

/* 
 * TABLE: feature_type 
 */

CREATE TABLE feature_type(
    idfeatyp       DOM_IDENTITY       IDENTITY(1,1),
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__feature___085C0BD2C2CE7935 PRIMARY KEY NONCLUSTERED (idfeatyp)
)
go



IF OBJECT_ID('feature_type') IS NOT NULL
    PRINT '<<< CREATED TABLE feature_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE feature_type >>>'
go

/* 
 * TABLE: fixed_asset 
 */

CREATE TABLE fixed_asset(
    idfa           DOM_IDENTITY       IDENTITY(1,1),
    idfatype       DOM_INTEGER        NULL,
    iduom          DOM_ID             NULL,
    name           DOM_NAME           NULL,
    dtacquired     DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__fixed_as__9DB7BA49DEE67035 PRIMARY KEY CLUSTERED (idfa)
)
go



IF OBJECT_ID('fixed_asset') IS NOT NULL
    PRINT '<<< CREATED TABLE fixed_asset >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fixed_asset >>>'
go

/* 
 * TABLE: fixed_asset_type 
 */

CREATE TABLE fixed_asset_type(
    idfatype       DOM_INTEGER        NOT NULL,
    idparent       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__fixed_as__6552B96A68FEB392 PRIMARY KEY CLUSTERED (idfatype)
)
go



IF OBJECT_ID('fixed_asset_type') IS NOT NULL
    PRINT '<<< CREATED TABLE fixed_asset_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fixed_asset_type >>>'
go

/* 
 * TABLE: fixedasset_adjustment_th 
 */

CREATE TABLE fixedasset_adjustment_th(
    idfixedassetadjustment    int             IDENTITY(1,1),
    idfixedasset              DOM_IDENTITY    NULL,
    adjusmentdate             datetime        NULL,
    adjustmentquantity        int             NULL,
    newquantity               int             NULL,
    lastmodifieddate          datetime        NULL,
    lastmodifieduserid        int             NULL,
    CONSTRAINT PK__fixedass__BC28C7EF8F887BB3 PRIMARY KEY CLUSTERED (idfixedassetadjustment)
)
go



IF OBJECT_ID('fixedasset_adjustment_th') IS NOT NULL
    PRINT '<<< CREATED TABLE fixedasset_adjustment_th >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fixedasset_adjustment_th >>>'
go

/* 
 * TABLE: fixedasset_th 
 */

CREATE TABLE fixedasset_th(
    idfixedassetth        int             IDENTITY(1,1),
    idfixedasset          DOM_IDENTITY    NULL,
    idlocationroom        int             NULL,
    idlocationfloor       int             NULL,
    idpicuser             int             NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    CONSTRAINT PK__fixedass__D613A7A955EBF978 PRIMARY KEY CLUSTERED (idfixedassetth)
)
go



IF OBJECT_ID('fixedasset_th') IS NOT NULL
    PRINT '<<< CREATED TABLE fixedasset_th >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fixedasset_th >>>'
go

/* 
 * TABLE: fixedasset_tm 
 */

CREATE TABLE fixedasset_tm(
    idfixedasset                DOM_IDENTITY    IDENTITY(1,1),
    idpurchaseorderreception    int             NULL,
    idpurchaseorder             int             NULL,
    idfloor                     int             NULL,
    idroom                      int             NULL,
    idcategoryasset             int             NULL,
    batchno                     int             NULL,
    assetid                     DOM_ID          NULL,
    idpicuser                   int             NULL,
    name                        DOM_NAME        NULL,
    price                       DOM_MONEY       NULL,
    orderquantity               DOM_INTEGER     NULL,
    acceptedquantity            DOM_INTEGER     NULL,
    currentquantity             int             NULL,
    unitmeasure                 DOM_ID          NULL,
    picname                     DOM_USER        NULL,
    lastmodifieddate            datetime        NULL,
    lastmodifieduserid          int             NULL,
    statuscode                  int             NULL,
    CONSTRAINT PK__fixedass__EF8005B5B05AAFC6 PRIMARY KEY CLUSTERED (idfixedasset)
)
go



IF OBJECT_ID('fixedasset_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE fixedasset_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fixedasset_tm >>>'
go

/* 
 * TABLE: floor_tm 
 */

CREATE TABLE floor_tm(
    idfloor               int             IDENTITY(1,1),
    floorname             nvarchar(50)    NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__floor_tm__0D2AEF81BB060A8C PRIMARY KEY CLUSTERED (idfloor)
)
go



IF OBJECT_ID('floor_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE floor_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE floor_tm >>>'
go

/* 
 * TABLE: gender 
 */

CREATE TABLE gender(
    idgender       DOM_FLAG           NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__gender__8E466278A5FAC702 PRIMARY KEY CLUSTERED (idgender)
)
go



IF OBJECT_ID('gender') IS NOT NULL
    PRINT '<<< CREATED TABLE gender >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE gender >>>'
go

/* 
 * TABLE: general_upload 
 */

CREATE TABLE general_upload(
    idgenupl            DOM_UUID           NOT NULL,
    idinternal          DOM_ID             NULL,
    idpurposetype       DOM_IDENTITY       NULL,
    dtcreate            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description         DOM_DESCRIPTION    NULL,
    filename            varchar(255)       NULL,
    value               DOM_BLOB           NULL,
    valuecontenttype    DOM_LONGDESC       NULL,
    CONSTRAINT PK__general___8CD99C38317066D0 PRIMARY KEY CLUSTERED (idgenupl)
)
go



IF OBJECT_ID('general_upload') IS NOT NULL
    PRINT '<<< CREATED TABLE general_upload >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE general_upload >>>'
go

/* 
 * TABLE: general_upload_status 
 */

CREATE TABLE general_upload_status(
    idstatus        DOM_UUID        NOT NULL,
    idgenupl        DOM_UUID        NULL,
    idreason        DOM_INTEGER     NULL,
    idstatustype    DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__general___EBF77C2D3DB831E6 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('general_upload_status') IS NOT NULL
    PRINT '<<< CREATED TABLE general_upload_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE general_upload_status >>>'
go

/* 
 * TABLE: geo_boundary 
 */

CREATE TABLE geo_boundary(
    idgeobou        DOM_UUID           NOT NULL,
    idparent        DOM_UUID           NULL,
    idgeoboutype    DOM_IDENTITY       NULL,
    geocode         DOM_ID             NULL,
    description     DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__geo_boun__96A5A05EA30CC842 PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



IF OBJECT_ID('geo_boundary') IS NOT NULL
    PRINT '<<< CREATED TABLE geo_boundary >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE geo_boundary >>>'
go

/* 
 * TABLE: geo_boundary_type 
 */

CREATE TABLE geo_boundary_type(
    idgeoboutype    DOM_IDENTITY       IDENTITY(1,1),
    description     DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__geo_boun__CE7D96A24AA890F2 PRIMARY KEY NONCLUSTERED (idgeoboutype)
)
go



IF OBJECT_ID('geo_boundary_type') IS NOT NULL
    PRINT '<<< CREATED TABLE geo_boundary_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE geo_boundary_type >>>'
go

/* 
 * TABLE: gl_account 
 */

CREATE TABLE gl_account(
    idglacc        DOM_UUID           NOT NULL,
    idglacctyp     DOM_INTEGER        NULL,
    name           DOM_NAME           NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__gl_accou__081869CDB22A2F05 PRIMARY KEY CLUSTERED (idglacc)
)
go



IF OBJECT_ID('gl_account') IS NOT NULL
    PRINT '<<< CREATED TABLE gl_account >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE gl_account >>>'
go

/* 
 * TABLE: gl_account_type 
 */

CREATE TABLE gl_account_type(
    idglacctyp     DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__gl_accou__201FAB431B68CD23 PRIMARY KEY CLUSTERED (idglacctyp)
)
go



IF OBJECT_ID('gl_account_type') IS NOT NULL
    PRINT '<<< CREATED TABLE gl_account_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE gl_account_type >>>'
go

/* 
 * TABLE: gl_dimension 
 */

CREATE TABLE gl_dimension(
    idgldim       DOM_UUID        NOT NULL,
    idbillto      DOM_ID          NULL,
    idvendor      DOM_ID          NULL,
    idinternal    DOM_ID          NULL,
    idprocat      DOM_IDENTITY    NULL,
    idparcat      DOM_IDENTITY    NULL,
    dimension1    DOM_ID          NULL,
    dimension2    DOM_ID          NULL,
    dimension3    DOM_ID          NULL,
    dimension4    DOM_ID          NULL,
    dimension5    DOM_ID          NULL,
    dimension6    DOM_ID          NULL,
    CONSTRAINT PK__gl_dimen__0B4623993C87697D PRIMARY KEY CLUSTERED (idgldim)
)
go



IF OBJECT_ID('gl_dimension') IS NOT NULL
    PRINT '<<< CREATED TABLE gl_dimension >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE gl_dimension >>>'
go

/* 
 * TABLE: good 
 */

CREATE TABLE good(
    idproduct     DOM_ID         NOT NULL,
    iduom         DOM_ID         NULL,
    serialized    DOM_BOOLEAN    DEFAULT 0 NULL,
    CONSTRAINT PK__good__8D5074256B631A59 PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('good') IS NOT NULL
    PRINT '<<< CREATED TABLE good >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE good >>>'
go

/* 
 * TABLE: good_container 
 */

CREATE TABLE good_container(
    idgoocon       DOM_UUID        NOT NULL,
    idproduct      DOM_ID          NULL,
    idcontainer    DOM_UUID        NULL,
    idparty        DOM_UUID        NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__good_con__F10983624B872025 PRIMARY KEY NONCLUSTERED (idgoocon)
)
go



IF OBJECT_ID('good_container') IS NOT NULL
    PRINT '<<< CREATED TABLE good_container >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE good_container >>>'
go

/* 
 * TABLE: good_identification 
 */

CREATE TABLE good_identification(
    idgooide                DOM_UUID       NOT NULL,
    idproduct               DOM_ID         NULL,
    ididentificationtype    DOM_INTEGER    NULL,
    idvalue                 DOM_ID         NULL,
    CONSTRAINT PK764 PRIMARY KEY CLUSTERED (idgooide)
)
go



IF OBJECT_ID('good_identification') IS NOT NULL
    PRINT '<<< CREATED TABLE good_identification >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE good_identification >>>'
go

/* 
 * TABLE: good_promotion 
 */

CREATE TABLE good_promotion(
    idproduct    DOM_ID      NOT NULL,
    ispromo      bit         NULL,
    dtcreate     datetime    NULL,
    CONSTRAINT PK__good_pro__8D507424F5EEBCAF PRIMARY KEY CLUSTERED (idproduct)
)
go



IF OBJECT_ID('good_promotion') IS NOT NULL
    PRINT '<<< CREATED TABLE good_promotion >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE good_promotion >>>'
go

/* 
 * TABLE: history_store 
 */

CREATE TABLE history_store(
    timemark        datetime         NOT NULL,
    table_name      nvarchar(50)     NOT NULL,
    pk_date_src     nvarchar(400)    NOT NULL,
    pk_date_dest    nvarchar(400)    NOT NULL,
    record_state    smallint         NOT NULL,
    CONSTRAINT PK__history___915183C97782EBF1 PRIMARY KEY CLUSTERED (table_name, pk_date_dest)
)
go



IF OBJECT_ID('history_store') IS NOT NULL
    PRINT '<<< CREATED TABLE history_store >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE history_store >>>'
go

/* 
 * TABLE: identification_type 
 */

CREATE TABLE identification_type(
    ididentificationtype    DOM_INTEGER        NOT NULL,
    description             DOM_DESCRIPTION    NULL,
    CONSTRAINT PK765 PRIMARY KEY CLUSTERED (ididentificationtype)
)
go



IF OBJECT_ID('identification_type') IS NOT NULL
    PRINT '<<< CREATED TABLE identification_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE identification_type >>>'
go

/* 
 * TABLE: intern 
 */

CREATE TABLE intern(
    idintern       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__intern__3A3F2748B99C333C PRIMARY KEY NONCLUSTERED (idintern)
)
go



IF OBJECT_ID('intern') IS NOT NULL
    PRINT '<<< CREATED TABLE intern >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE intern >>>'
go

/* 
 * TABLE: internal 
 */

CREATE TABLE internal(
    idinternal      DOM_ID          NOT NULL,
    idroot          DOM_ID          NULL,
    idparent        DOM_ID          NULL,
    idroletype      DOM_IDENTITY    NULL,
    idparty         DOM_UUID        NULL,
    selfsender      DOM_BOOLEAN     DEFAULT 0 NULL,
    idmpm           DOM_ID          NULL,
    idahm           DOM_ID          NULL,
    iddealercode    DOM_ID          NULL,
    CONSTRAINT PK__internal__2BD3089135B8FC06 PRIMARY KEY NONCLUSTERED (idinternal)
)
go



IF OBJECT_ID('internal') IS NOT NULL
    PRINT '<<< CREATED TABLE internal >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE internal >>>'
go

/* 
 * TABLE: internal_acctg_trans 
 */

CREATE TABLE internal_acctg_trans(
    idacctra      DOM_IDENTITY    NOT NULL,
    idinternal    DOM_ID          NULL,
    CONSTRAINT PK__internal__F099E8A0D0205531 PRIMARY KEY CLUSTERED (idacctra)
)
go



IF OBJECT_ID('internal_acctg_trans') IS NOT NULL
    PRINT '<<< CREATED TABLE internal_acctg_trans >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE internal_acctg_trans >>>'
go

/* 
 * TABLE: internal_facility_purpose 
 */

CREATE TABLE internal_facility_purpose(
    idintfacpur      DOM_UUID        NOT NULL,
    idpurposetype    DOM_IDENTITY    NULL,
    idfacility       DOM_UUID        NULL,
    idinternal       DOM_ID          NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__internal__4057C3E6BE8DFBAB PRIMARY KEY CLUSTERED (idintfacpur)
)
go



IF OBJECT_ID('internal_facility_purpose') IS NOT NULL
    PRINT '<<< CREATED TABLE internal_facility_purpose >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE internal_facility_purpose >>>'
go

/* 
 * TABLE: internal_order 
 */

CREATE TABLE internal_order(
    idorder     DOM_UUID    NOT NULL,
    idintto     DOM_ID      NULL,
    idintfro    DOM_ID      NULL,
    CONSTRAINT PK__internal__44DCCDAB60DB9590 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('internal_order') IS NOT NULL
    PRINT '<<< CREATED TABLE internal_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE internal_order >>>'
go

/* 
 * TABLE: inventory_adjustment 
 */

CREATE TABLE inventory_adjustment(
    idinvadj      DOM_UUID    NOT NULL,
    idinvite      DOM_UUID    NULL,
    idstopnite    DOM_UUID    NULL,
    qty           DOM_QTY     NULL,
    CONSTRAINT PK__inventor__7D749694F15F541B PRIMARY KEY CLUSTERED (idinvadj)
)
go



IF OBJECT_ID('inventory_adjustment') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_adjustment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_adjustment >>>'
go

/* 
 * TABLE: inventory_item 
 */

CREATE TABLE inventory_item(
    idinvite        DOM_UUID           NOT NULL,
    idowner         DOM_UUID           NULL,
    idproduct       DOM_ID             NULL,
    idfacility      DOM_UUID           NULL,
    idcontainer     DOM_UUID           NULL,
    idreceipt       DOM_UUID           NULL,
    idfeature       DOM_INTEGER        NULL,
    idfa            DOM_IDENTITY       NULL,
    dtcreated       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    qty             DOM_QTY            NULL,
    qtybooking      DOM_QTY            NULL,
    idmachine       DOM_DESCRIPTION    NULL,
    idframe         DOM_DESCRIPTION    NULL,
    yearassembly    DOM_INTEGER        NULL,
    CONSTRAINT PK__inventor__6755DA8D565456C8 PRIMARY KEY NONCLUSTERED (idinvite)
)
go



IF OBJECT_ID('inventory_item') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_item >>>'
go

/* 
 * TABLE: inventory_item_status 
 */

CREATE TABLE inventory_item_status(
    idstatus        DOM_UUID        NOT NULL,
    idinvite        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__inventor__EBF77C2CEE4BB804 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('inventory_item_status') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_item_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_item_status >>>'
go

/* 
 * TABLE: inventory_movement 
 */

CREATE TABLE inventory_movement(
    idslip      DOM_UUID        NOT NULL,
    slipnum     DOM_ID          NULL,
    dtcreate    DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__inventor__D730145DE0B4CE32 PRIMARY KEY NONCLUSTERED (idslip)
)
go



IF OBJECT_ID('inventory_movement') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_movement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_movement >>>'
go

/* 
 * TABLE: inventory_movement_status 
 */

CREATE TABLE inventory_movement_status(
    idstatus        DOM_UUID        NOT NULL,
    idslip          DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__inventor__EBF77C2C9777F790 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('inventory_movement_status') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_movement_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_movement_status >>>'
go

/* 
 * TABLE: inventory_request_item 
 */

CREATE TABLE inventory_request_item(
    idinvite     DOM_UUID    NOT NULL,
    idreqitem    DOM_UUID    NOT NULL,
    qty          DOM_QTY     NULL,
    CONSTRAINT PK__inventor__1A723926955320AF PRIMARY KEY CLUSTERED (idinvite, idreqitem)
)
go



IF OBJECT_ID('inventory_request_item') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_request_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_request_item >>>'
go

/* 
 * TABLE: item_issuance 
 */

CREATE TABLE item_issuance(
    iditeiss    DOM_UUID    NOT NULL,
    idshiite    DOM_UUID    NULL,
    idinvite    DOM_UUID    NULL,
    idslip      DOM_UUID    NULL,
    qty         DOM_QTY     NULL,
    CONSTRAINT PK__item_iss__A1E268A45E3D2872 PRIMARY KEY NONCLUSTERED (iditeiss)
)
go



IF OBJECT_ID('item_issuance') IS NOT NULL
    PRINT '<<< CREATED TABLE item_issuance >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE item_issuance >>>'
go

/* 
 * TABLE: job 
 */

CREATE TABLE job(
    idpkb                 DOM_IDENTITY    NOT NULL,
    idparrol              DOM_UUID        NULL,
    idjobstatus           int             NULL,
    starttime             datetime        NULL,
    complaint             varchar(255)    NULL,
    jobsuggest            varchar(255)    NULL,
    duration              int             NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__job__04B793EED0E8AA57 PRIMARY KEY NONCLUSTERED (idpkb)
)
go



IF OBJECT_ID('job') IS NOT NULL
    PRINT '<<< CREATED TABLE job >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job >>>'
go

/* 
 * TABLE: job_dispatch 
 */

CREATE TABLE job_dispatch(
    idjobdispatch       DOM_IDENTITY    IDENTITY(1,1),
    iddispatchstatus    DOM_INTEGER     NULL,
    idpkb               DOM_IDENTITY    NULL,
    idparrol            DOM_UUID        NULL,
    idjobpriority       DOM_INTEGER     NULL,
    dispatchdate        datetime        NULL,
    CONSTRAINT PK__job_disp__1EFF84D3047E1576 PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



IF OBJECT_ID('job_dispatch') IS NOT NULL
    PRINT '<<< CREATED TABLE job_dispatch >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_dispatch >>>'
go

/* 
 * TABLE: job_dispatch_customer_type 
 */

CREATE TABLE job_dispatch_customer_type(
    idjobdispatch     DOM_IDENTITY    NOT NULL,
    idcostumertype    DOM_INTEGER     NULL,
    CONSTRAINT PK__job_disp__1EFF84D37764C09C PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



IF OBJECT_ID('job_dispatch_customer_type') IS NOT NULL
    PRINT '<<< CREATED TABLE job_dispatch_customer_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_dispatch_customer_type >>>'
go

/* 
 * TABLE: job_dispatch_group_service 
 */

CREATE TABLE job_dispatch_group_service(
    idjobdispatch    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK__job_disp__1EFF84D32750A6F9 PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



IF OBJECT_ID('job_dispatch_group_service') IS NOT NULL
    PRINT '<<< CREATED TABLE job_dispatch_group_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_dispatch_group_service >>>'
go

/* 
 * TABLE: job_dispatch_pkb_type 
 */

CREATE TABLE job_dispatch_pkb_type(
    idjobdispatch    DOM_IDENTITY    NOT NULL,
    idtypepkb        DOM_INTEGER     NULL,
    CONSTRAINT PK__job_disp__1EFF84D3F6CC95DA PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



IF OBJECT_ID('job_dispatch_pkb_type') IS NOT NULL
    PRINT '<<< CREATED TABLE job_dispatch_pkb_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_dispatch_pkb_type >>>'
go

/* 
 * TABLE: job_dispatch_service 
 */

CREATE TABLE job_dispatch_service(
    idjobdispatch    DOM_IDENTITY    NOT NULL,
    idproduct        DOM_ID          NULL,
    CONSTRAINT PK__job_disp__1EFF84D34FCA4AD7 PRIMARY KEY CLUSTERED (idjobdispatch)
)
go



IF OBJECT_ID('job_dispatch_service') IS NOT NULL
    PRINT '<<< CREATED TABLE job_dispatch_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_dispatch_service >>>'
go

/* 
 * TABLE: job_history 
 */

CREATE TABLE job_history(
    idjobhistory       int             IDENTITY(1,1),
    idpkb              DOM_IDENTITY    NULL,
    idjobstatus        int             NULL,
    idpendingreason    int             NULL,
    idparrol           DOM_UUID        NULL,
    time               datetime        NULL,
    duration           int             NULL,
    CONSTRAINT PK__job_hist__BA940B76AC5ABA09 PRIMARY KEY CLUSTERED (idjobhistory)
)
go



IF OBJECT_ID('job_history') IS NOT NULL
    PRINT '<<< CREATED TABLE job_history >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_history >>>'
go

/* 
 * TABLE: job_priority 
 */

CREATE TABLE job_priority(
    idjobpriority        DOM_INTEGER    NOT NULL,
    idjobprioritytype    DOM_INTEGER    NULL,
    priorityname         varchar(30)    NULL,
    priority             int            NULL,
    dtcreate             datetime       NULL,
    CONSTRAINT PK__job_prio__621708C45C4151CD PRIMARY KEY CLUSTERED (idjobpriority)
)
go



IF OBJECT_ID('job_priority') IS NOT NULL
    PRINT '<<< CREATED TABLE job_priority >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_priority >>>'
go

/* 
 * TABLE: job_priority_customer_type 
 */

CREATE TABLE job_priority_customer_type(
    idjobpriority     DOM_INTEGER    NOT NULL,
    idcostumertype    DOM_INTEGER    NULL,
    CONSTRAINT PK__job_prio__621708C4A05F326C PRIMARY KEY CLUSTERED (idjobpriority)
)
go



IF OBJECT_ID('job_priority_customer_type') IS NOT NULL
    PRINT '<<< CREATED TABLE job_priority_customer_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_priority_customer_type >>>'
go

/* 
 * TABLE: job_priority_group_service 
 */

CREATE TABLE job_priority_group_service(
    idjobpriority    DOM_INTEGER    NOT NULL,
    CONSTRAINT PK__job_prio__621708C47FD278CB PRIMARY KEY CLUSTERED (idjobpriority)
)
go



IF OBJECT_ID('job_priority_group_service') IS NOT NULL
    PRINT '<<< CREATED TABLE job_priority_group_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_priority_group_service >>>'
go

/* 
 * TABLE: job_priority_pkb_type 
 */

CREATE TABLE job_priority_pkb_type(
    idjobpriority    DOM_INTEGER    NOT NULL,
    idtypepkb        DOM_INTEGER    NULL,
    CONSTRAINT PK__job_prio__621708C4D0A5415E PRIMARY KEY CLUSTERED (idjobpriority)
)
go



IF OBJECT_ID('job_priority_pkb_type') IS NOT NULL
    PRINT '<<< CREATED TABLE job_priority_pkb_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_priority_pkb_type >>>'
go

/* 
 * TABLE: job_priority_service 
 */

CREATE TABLE job_priority_service(
    idjobpriority    DOM_INTEGER    NOT NULL,
    idproduct        DOM_ID         NULL,
    CONSTRAINT PK__job_prio__621708C4B072DA5D PRIMARY KEY CLUSTERED (idjobpriority)
)
go



IF OBJECT_ID('job_priority_service') IS NOT NULL
    PRINT '<<< CREATED TABLE job_priority_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_priority_service >>>'
go

/* 
 * TABLE: job_priority_type 
 */

CREATE TABLE job_priority_type(
    idjobprioritytype    DOM_INTEGER    NOT NULL,
    jobtypename          varchar(30)    NULL,
    dtcreate             datetime       NULL,
    CONSTRAINT PK__job_prio__F62E9B92586B1467 PRIMARY KEY CLUSTERED (idjobprioritytype)
)
go



IF OBJECT_ID('job_priority_type') IS NOT NULL
    PRINT '<<< CREATED TABLE job_priority_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_priority_type >>>'
go

/* 
 * TABLE: job_service 
 */

CREATE TABLE job_service(
    idjobservice    int             NOT NULL,
    idpkb           DOM_IDENTITY    NULL,
    idproduct       DOM_ID          NULL,
    CONSTRAINT PK__job_serv__2DF4A79BEA39C5B0 PRIMARY KEY CLUSTERED (idjobservice)
)
go



IF OBJECT_ID('job_service') IS NOT NULL
    PRINT '<<< CREATED TABLE job_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_service >>>'
go

/* 
 * TABLE: job_status 
 */

CREATE TABLE job_status(
    idjobstatus     DOM_INTEGER        NOT NULL,
    idstatustype    DOM_INTEGER        NULL,
    desc_           DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__job_stat__A0FC51353C5013B7 PRIMARY KEY CLUSTERED (idjobstatus)
)
go



IF OBJECT_ID('job_status') IS NOT NULL
    PRINT '<<< CREATED TABLE job_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_status >>>'
go

/* 
 * TABLE: job_suggest 
 */

CREATE TABLE job_suggest(
    idjobsuggest    int         IDENTITY(1,1),
    idproduct       DOM_ID      NULL,
    firstkm         int         NULL,
    lastkm          int         NULL,
    dtfrom          datetime    NULL,
    dtthru          datetime    NULL,
    CONSTRAINT PK__job_sugg__349CF27C21DA0D70 PRIMARY KEY CLUSTERED (idjobsuggest)
)
go



IF OBJECT_ID('job_suggest') IS NOT NULL
    PRINT '<<< CREATED TABLE job_suggest >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_suggest >>>'
go

/* 
 * TABLE: job_suggest_service 
 */

CREATE TABLE job_suggest_service(
    idjobsuggestservice    int         IDENTITY(1,1),
    idjobsuggest           int         NULL,
    idproduct              DOM_ID      NULL,
    dtfrom                 datetime    NULL,
    dtthru                 datetime    NULL,
    CONSTRAINT PK__job_sugg__FDACB671B4F60A5F PRIMARY KEY CLUSTERED (idjobsuggestservice)
)
go



IF OBJECT_ID('job_suggest_service') IS NOT NULL
    PRINT '<<< CREATED TABLE job_suggest_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE job_suggest_service >>>'
go

/* 
 * TABLE: leasing_company 
 */

CREATE TABLE leasing_company(
    idparrol        DOM_UUID    NOT NULL,
    idleacom        DOM_ID      NULL,
    idleasingahm    DOM_ID      NULL,
    CONSTRAINT PK__leasing___86F77EC2748963BA PRIMARY KEY CLUSTERED (idparrol)
)
go



IF OBJECT_ID('leasing_company') IS NOT NULL
    PRINT '<<< CREATED TABLE leasing_company >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE leasing_company >>>'
go

/* 
 * TABLE: leasing_tenor_provide 
 */

CREATE TABLE leasing_tenor_provide(
    idlsgpro       DOM_UUID        NOT NULL,
    idproduct      DOM_ID          NULL,
    idleacom       DOM_UUID        NULL,
    tenor          DOM_INTEGER     NULL,
    installment    DOM_MONEY       NULL,
    downpayment    DOM_MONEY       NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__leasing___52E9E4EB97B288B6 PRIMARY KEY CLUSTERED (idlsgpro)
)
go



IF OBJECT_ID('leasing_tenor_provide') IS NOT NULL
    PRINT '<<< CREATED TABLE leasing_tenor_provide >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE leasing_tenor_provide >>>'
go

/* 
 * TABLE: market_treatment 
 */

CREATE TABLE market_treatment(
    idmarkettreatment    int             IDENTITY(1,1),
    idvendor             DOM_ID          NULL,
    idbilling            DOM_UUID        NULL,
    name                 varchar(90)     NULL,
    description          varchar(300)    NULL,
    juklaknumber         varchar(30)     NULL,
    dtfirstperiod        datetime        NULL,
    dtlastperiod         datetime        NULL,
    CONSTRAINT PK__market_t__FF25213EA399F3D0 PRIMARY KEY CLUSTERED (idmarkettreatment)
)
go



IF OBJECT_ID('market_treatment') IS NOT NULL
    PRINT '<<< CREATED TABLE market_treatment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE market_treatment >>>'
go

/* 
 * TABLE: market_treatment_status 
 */

CREATE TABLE market_treatment_status(
    idmarkettreatmentstatus    DOM_IDENTITY    IDENTITY(1,1),
    idstatustype               DOM_INTEGER     NULL,
    idmarkettreatment          int             NULL,
    dtfrom                     datetime        NULL,
    dtthru                     datetime        NULL,
    CONSTRAINT PK__market_t__4EDE75A255073E22 PRIMARY KEY CLUSTERED (idmarkettreatmentstatus)
)
go



IF OBJECT_ID('market_treatment_status') IS NOT NULL
    PRINT '<<< CREATED TABLE market_treatment_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE market_treatment_status >>>'
go

/* 
 * TABLE: master_numbering 
 */

CREATE TABLE master_numbering(
    idnumbering    DOM_UUID           NOT NULL,
    idtag          DOM_DESCRIPTION    NULL,
    idvalue        DOM_DESCRIPTION    NULL,
    nextvalue      DOM_INTEGER        NULL,
    valuenumber    DOM_MONEY          NULL,
    valuestring    DOM_LONGDESC       NULL,
    CONSTRAINT PK__master_n__3A89C164CE46F744 PRIMARY KEY NONCLUSTERED (idnumbering)
)
go



IF OBJECT_ID('master_numbering') IS NOT NULL
    PRINT '<<< CREATED TABLE master_numbering >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE master_numbering >>>'
go

/* 
 * TABLE: mechanic 
 */

CREATE TABLE mechanic(
    idparrol            DOM_UUID           NOT NULL,
    idvendor            DOM_ID             NULL,
    idmechanic          DOM_ID             NULL,
    isexternal          DOM_BOOLEAN        DEFAULT 0 NULL,
    passwordmechanic    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__mechanic__86F77EC3EC584B0E PRIMARY KEY NONCLUSTERED (idparrol)
)
go



IF OBJECT_ID('mechanic') IS NOT NULL
    PRINT '<<< CREATED TABLE mechanic >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE mechanic >>>'
go

/* 
 * TABLE: memo 
 */

CREATE TABLE memo(
    idmemo          DOM_UUID        NOT NULL,
    idbilling       DOM_UUID        NULL,
    idmemotype      DOM_INTEGER     NULL,
    idinvite        DOM_UUID        NULL,
    idoldinvite     DOM_UUID        NULL,
    idinternal      DOM_ID          NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    memonumber      DOM_ID          NULL,
    bbn             DOM_MONEY       NULL,
    discount        DOM_MONEY       NULL,
    unitprice       DOM_MONEY       NULL,
    currbillnumb    DOM_ID          NULL,
    reqidfeature    DOM_INTEGER     NULL,
    ppn             DOM_MONEY       NULL,
    dpp             DOM_MONEY       NULL,
    salesamount     DOM_MONEY       NULL,
    aramount        DOM_MONEY       NULL,
    CONSTRAINT PK__memo__753CC04BB1B86764 PRIMARY KEY CLUSTERED (idmemo)
)
go



IF OBJECT_ID('memo') IS NOT NULL
    PRINT '<<< CREATED TABLE memo >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE memo >>>'
go

/* 
 * TABLE: memo_status 
 */

CREATE TABLE memo_status(
    idstatus        DOM_UUID        NOT NULL,
    idmemo          DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__memo_sta__EBF77C2D27BF0810 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('memo_status') IS NOT NULL
    PRINT '<<< CREATED TABLE memo_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE memo_status >>>'
go

/* 
 * TABLE: memo_type 
 */

CREATE TABLE memo_type(
    idmemotype     DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__memo_typ__73FE00C77BA2FCE0 PRIMARY KEY CLUSTERED (idmemotype)
)
go



IF OBJECT_ID('memo_type') IS NOT NULL
    PRINT '<<< CREATED TABLE memo_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE memo_type >>>'
go

/* 
 * TABLE: motor 
 */

CREATE TABLE motor(
    idproduct         DOM_ID             NOT NULL,
    marketname        DOM_DESCRIPTION    NULL,
    dtdiscontinued    DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__motor__8D50742524155311 PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('motor') IS NOT NULL
    PRINT '<<< CREATED TABLE motor >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE motor >>>'
go

/* 
 * TABLE: motor_due_reminder 
 */

CREATE TABLE motor_due_reminder(
    idreminder    DOM_IDENTITY    IDENTITY(1,1),
    idmotor       DOM_ID          NULL,
    nservice1     DOM_INTEGER     NULL,
    nservice2     DOM_INTEGER     NULL,
    nservice3     DOM_INTEGER     NULL,
    nservice4     DOM_INTEGER     NULL,
    nservice5     DOM_INTEGER     NULL,
    km            DOM_INTEGER     NULL,
    ndays         DOM_INTEGER     NULL,
    CONSTRAINT PK__motor_du__84BBA9C3C95B2E13 PRIMARY KEY NONCLUSTERED (idreminder)
)
go



IF OBJECT_ID('motor_due_reminder') IS NOT NULL
    PRINT '<<< CREATED TABLE motor_due_reminder >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE motor_due_reminder >>>'
go

/* 
 * TABLE: moving_slip 
 */

CREATE TABLE moving_slip(
    idslip             DOM_UUID    NOT NULL,
    idinviteto         DOM_UUID    NULL,
    idcontainerfrom    DOM_UUID    NULL,
    idcontainerto      DOM_UUID    NULL,
    idproduct          DOM_ID      NULL,
    idfacilityto       DOM_UUID    NULL,
    idfacilityfrom     DOM_UUID    NULL,
    idinvite           DOM_UUID    NULL,
    idreqitem          DOM_UUID    NULL,
    qty                DOM_QTY     NULL,
    CONSTRAINT PK__moving_s__D730145D9DCF53A2 PRIMARY KEY NONCLUSTERED (idslip)
)
go



IF OBJECT_ID('moving_slip') IS NOT NULL
    PRINT '<<< CREATED TABLE moving_slip >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE moving_slip >>>'
go

/* 
 * TABLE: operation_code 
 */

CREATE TABLE operation_code(
    idoperationcode    DOM_ID             NOT NULL,
    description        DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__operatio__479E24EED6A43272 PRIMARY KEY CLUSTERED (idoperationcode)
)
go



IF OBJECT_ID('operation_code') IS NOT NULL
    PRINT '<<< CREATED TABLE operation_code >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE operation_code >>>'
go

/* 
 * TABLE: order_billing_item 
 */

CREATE TABLE order_billing_item(
    idordbilite    DOM_UUID     NOT NULL,
    idordite       DOM_UUID     NULL,
    idbilite       DOM_UUID     NULL,
    amount         DOM_MONEY    NULL,
    qty            DOM_QTY      NULL,
    CONSTRAINT PK__order_bi__52B1AA39EA9186BA PRIMARY KEY CLUSTERED (idordbilite)
)
go



IF OBJECT_ID('order_billing_item') IS NOT NULL
    PRINT '<<< CREATED TABLE order_billing_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_billing_item >>>'
go

/* 
 * TABLE: order_demand 
 */

CREATE TABLE order_demand(
    idorderdemand    char(10)          NOT NULL,
    qty              decimal(18, 4)    NULL,
    idproduct        DOM_ID            NULL,
    idorder          DOM_UUID          NULL,
    dtorderdemand    datetime          NULL,
    CONSTRAINT PK__order_de__123E474AC00C4AB1 PRIMARY KEY CLUSTERED (idorderdemand)
)
go



IF OBJECT_ID('order_demand') IS NOT NULL
    PRINT '<<< CREATED TABLE order_demand >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_demand >>>'
go

/* 
 * TABLE: order_item 
 */

CREATE TABLE order_item(
    idordite           DOM_UUID           NOT NULL,
    idparordite        DOM_UUID           NULL,
    idorder            DOM_UUID           NULL,
    idproduct          DOM_ID             NULL,
    idfeature          DOM_INTEGER        NULL,
    idshipto           DOM_ID             NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    qty                DOM_QTY            NULL,
    unitprice          DOM_MONEY          NULL,
    discount           DOM_MONEY          NULL,
    discountmd         DOM_MONEY          NULL,
    discountfin        DOM_MONEY          NULL,
    bbn                DOM_MONEY          NULL,
    discountatpm       DOM_MONEY          NULL,
    idframe            DOM_ID             NULL,
    idmachine          DOM_ID             NULL,
    taxamount          DOM_MONEY          NULL,
    totalamount        DOM_MONEY          NULL,
    CONSTRAINT PK__order_it__D0E2AC1C456DDA36 PRIMARY KEY NONCLUSTERED (idordite)
)
go



IF OBJECT_ID('order_item') IS NOT NULL
    PRINT '<<< CREATED TABLE order_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_item >>>'
go

/* 
 * TABLE: order_payment 
 */

CREATE TABLE order_payment(
    idordpayite    DOM_UUID     NOT NULL,
    idorder        DOM_UUID     NULL,
    idpayment      DOM_UUID     NULL,
    amount         DOM_MONEY    NULL,
    CONSTRAINT PK__order_pa__2878BCF49746E45C PRIMARY KEY NONCLUSTERED (idordpayite)
)
go



IF OBJECT_ID('order_payment') IS NOT NULL
    PRINT '<<< CREATED TABLE order_payment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_payment >>>'
go

/* 
 * TABLE: order_request_item 
 */

CREATE TABLE order_request_item(
    idordreqite     DOM_UUID    NOT NULL,
    idordite        DOM_UUID    NULL,
    idreqitem       DOM_UUID    NULL,
    qty             DOM_QTY     NULL,
    qtydelivered    DOM_QTY     NULL,
    CONSTRAINT PK__order_re__7A99F5B2180157FE PRIMARY KEY CLUSTERED (idordreqite)
)
go



IF OBJECT_ID('order_request_item') IS NOT NULL
    PRINT '<<< CREATED TABLE order_request_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_request_item >>>'
go

/* 
 * TABLE: order_shipment_item 
 */

CREATE TABLE order_shipment_item(
    idordshiite    DOM_UUID    NOT NULL,
    idordite       DOM_UUID    NULL,
    idshiite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    CONSTRAINT PK__order_sh__C080AAE744CC66BC PRIMARY KEY CLUSTERED (idordshiite)
)
go



IF OBJECT_ID('order_shipment_item') IS NOT NULL
    PRINT '<<< CREATED TABLE order_shipment_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_shipment_item >>>'
go

/* 
 * TABLE: order_type 
 */

CREATE TABLE order_type(
    idordtyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__order_ty__CA02175F2C4F4C6E PRIMARY KEY NONCLUSTERED (idordtyp)
)
go



IF OBJECT_ID('order_type') IS NOT NULL
    PRINT '<<< CREATED TABLE order_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE order_type >>>'
go

/* 
 * TABLE: orders 
 */

CREATE TABLE orders(
    idorder        DOM_UUID        NOT NULL,
    idordtyp       DOM_IDENTITY    NULL,
    ordernumber    DOM_ID          NULL,
    dtentry        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtorder        DOM_DATE        NULL,
    CONSTRAINT PK__orders__44DCCDAB75109433 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('orders') IS NOT NULL
    PRINT '<<< CREATED TABLE orders >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE orders >>>'
go

/* 
 * TABLE: orders_role 
 */

CREATE TABLE orders_role(
    idrole        DOM_UUID        NOT NULL,
    idorder       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__orders_r__F324A06CA349C18A PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('orders_role') IS NOT NULL
    PRINT '<<< CREATED TABLE orders_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE orders_role >>>'
go

/* 
 * TABLE: orders_status 
 */

CREATE TABLE orders_status(
    idstatus        DOM_UUID           NOT NULL,
    idorder         DOM_UUID           NULL,
    idstatustype    DOM_INTEGER        NULL,
    idreason        DOM_INTEGER        NULL,
    idordite        DOM_UUID           NULL,
    description     DOM_DESCRIPTION    NULL,
    reason          varchar(255)       NULL,
    dtfrom          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__orders_s__EBF77C2C3C4E499E PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('orders_status') IS NOT NULL
    PRINT '<<< CREATED TABLE orders_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE orders_status >>>'
go

/* 
 * TABLE: organization 
 */

CREATE TABLE organization(
    idparty         DOM_UUID           NOT NULL,
    idpic           DOM_UUID           NULL,
    idposaddtdp     DOM_UUID           NULL,
    name            DOM_NAME           NULL,
    numbertdp       DOM_ID             NULL,
    dtestablised    DOM_DATE           NULL,
    officephone     DOM_ID             NULL,
    officefax       DOM_ID             NULL,
    taxidnumber     DOM_ID             NULL,
    officemail      DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__organiza__9E44201FFEC8568B PRIMARY KEY NONCLUSTERED (idparty)
)
go



IF OBJECT_ID('organization') IS NOT NULL
    PRINT '<<< CREATED TABLE organization >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE organization >>>'
go

/* 
 * TABLE: organization_customer 
 */

CREATE TABLE organization_customer(
    idcustomer    DOM_ID    NOT NULL,
    CONSTRAINT PK__organiza__53EFD0A712EA5395 PRIMARY KEY NONCLUSTERED (idcustomer)
)
go



IF OBJECT_ID('organization_customer') IS NOT NULL
    PRINT '<<< CREATED TABLE organization_customer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE organization_customer >>>'
go

/* 
 * TABLE: organization_gl_account 
 */

CREATE TABLE organization_gl_account(
    idorgglacc    DOM_UUID        NOT NULL,
    idparent      DOM_UUID        NULL,
    idglacc       DOM_UUID        NULL,
    idinternal    DOM_ID          NULL,
    idvendor      DOM_ID          NULL,
    idbillto      DOM_ID          NULL,
    idproduct     DOM_ID          NULL,
    idcategory    DOM_IDENTITY    NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__organiza__2D39404F913F7D9C PRIMARY KEY CLUSTERED (idorgglacc)
)
go



IF OBJECT_ID('organization_gl_account') IS NOT NULL
    PRINT '<<< CREATED TABLE organization_gl_account >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE organization_gl_account >>>'
go

/* 
 * TABLE: organization_prospect 
 */

CREATE TABLE organization_prospect(
    idprospect     DOM_UUID    NOT NULL,
    idparty        DOM_UUID    NULL,
    idpic          DOM_UUID    NULL,
    idposaddtdp    DOM_UUID    NULL,
    CONSTRAINT PK__organiza__580AA288A1191607 PRIMARY KEY NONCLUSTERED (idprospect)
)
go



IF OBJECT_ID('organization_prospect') IS NOT NULL
    PRINT '<<< CREATED TABLE organization_prospect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE organization_prospect >>>'
go

/* 
 * TABLE: organization_prospect_detail 
 */

CREATE TABLE organization_prospect_detail(
    idetail          DOM_UUID       NOT NULL,
    idprospect       DOM_UUID       NULL,
    idpersonowner    DOM_UUID       NULL,
    idproduct        DOM_ID         NULL,
    idfeature        DOM_INTEGER    NULL,
    unitprice        DOM_MONEY      NULL,
    discount         DOM_MONEY      NULL,
    CONSTRAINT PK__organiza__8A5CC1C9DAA19A17 PRIMARY KEY CLUSTERED (idetail)
)
go



IF OBJECT_ID('organization_prospect_detail') IS NOT NULL
    PRINT '<<< CREATED TABLE organization_prospect_detail >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE organization_prospect_detail >>>'
go

/* 
 * TABLE: organization_suspect 
 */

CREATE TABLE organization_suspect(
    idsuspect    DOM_UUID    NOT NULL,
    idparty      DOM_UUID    NULL,
    CONSTRAINT PK__organiza__2F025AB227464AF3 PRIMARY KEY CLUSTERED (idsuspect)
)
go



IF OBJECT_ID('organization_suspect') IS NOT NULL
    PRINT '<<< CREATED TABLE organization_suspect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE organization_suspect >>>'
go

/* 
 * TABLE: package_receipt 
 */

CREATE TABLE package_receipt(
    idpackage         DOM_UUID           NOT NULL,
    idshifro          DOM_ID             NULL,
    documentnumber    DOM_DESCRIPTION    NULL,
    vendorinvoice     DOM_ID             NULL,
    CONSTRAINT PK__package___C3EF12B654C47E3B PRIMARY KEY CLUSTERED (idpackage)
)
go



IF OBJECT_ID('package_receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE package_receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE package_receipt >>>'
go

/* 
 * TABLE: packaging_content 
 */

CREATE TABLE packaging_content(
    idpaccon     DOM_UUID    NOT NULL,
    idpackage    DOM_UUID    NULL,
    idshiite     DOM_UUID    NULL,
    qty          DOM_QTY     NULL,
    CONSTRAINT PK__packagin__8A883688EC10EFCF PRIMARY KEY CLUSTERED (idpaccon)
)
go



IF OBJECT_ID('packaging_content') IS NOT NULL
    PRINT '<<< CREATED TABLE packaging_content >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE packaging_content >>>'
go

/* 
 * TABLE: parent_organization 
 */

CREATE TABLE parent_organization(
    idinternal    DOM_ID    NOT NULL,
    CONSTRAINT PK__parent_o__2BD308910D6AABF9 PRIMARY KEY NONCLUSTERED (idinternal)
)
go



IF OBJECT_ID('parent_organization') IS NOT NULL
    PRINT '<<< CREATED TABLE parent_organization >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE parent_organization >>>'
go

/* 
 * TABLE: part_history 
 */

CREATE TABLE part_history(
    idparthistory              int               IDENTITY(1,1),
    idservicehistory           int               NULL,
    idvehicleservicehistory    DOM_IDENTITY      NULL,
    nopart                     varchar(30)       NULL,
    descpart                   varchar(30)       NULL,
    qty                        decimal(18, 4)    NULL,
    CONSTRAINT PK__part_his__0382A4A20F25F595 PRIMARY KEY CLUSTERED (idparthistory)
)
go



IF OBJECT_ID('part_history') IS NOT NULL
    PRINT '<<< CREATED TABLE part_history >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_history >>>'
go

/* 
 * TABLE: part_issue 
 */

CREATE TABLE part_issue(
    idissue            int            IDENTITY(1,1),
    idpartissuetype    DOM_INTEGER    NULL,
    filingnumber       varchar(20)    NULL,
    filingdate         date           NULL,
    CONSTRAINT PK__part_iss__4DD3F0DC32DAF4C1 PRIMARY KEY CLUSTERED (idissue)
)
go



IF OBJECT_ID('part_issue') IS NOT NULL
    PRINT '<<< CREATED TABLE part_issue >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_issue >>>'
go

/* 
 * TABLE: part_issue_action 
 */

CREATE TABLE part_issue_action(
    idpartissueact    DOM_UUID       NOT NULL,
    idissue           int            NULL,
    claimto           varchar(90)    NULL,
    nobast            varchar(20)    NULL,
    receivedby        varchar(90)    NULL,
    releasedby        varchar(90)    NULL,
    approvedby        varchar(90)    NULL,
    dtcreate          date           NULL,
    CONSTRAINT PK__part_iss__1D17CDB1BFB64620 PRIMARY KEY CLUSTERED (idpartissueact)
)
go



IF OBJECT_ID('part_issue_action') IS NOT NULL
    PRINT '<<< CREATED TABLE part_issue_action >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_issue_action >>>'
go

/* 
 * TABLE: part_issue_item 
 */

CREATE TABLE part_issue_item(
    idissueitem     DOM_IDENTITY    IDENTITY(1,1),
    idissue         int             NULL,
    idproduct       DOM_ID          NULL,
    idstatustype    DOM_INTEGER     NULL,
    description     varchar(50)     NULL,
    qty             int             NULL,
    CONSTRAINT PK__part_iss__2345D68E11094DF6 PRIMARY KEY CLUSTERED (idissueitem)
)
go



IF OBJECT_ID('part_issue_item') IS NOT NULL
    PRINT '<<< CREATED TABLE part_issue_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_issue_item >>>'
go

/* 
 * TABLE: part_issue_status 
 */

CREATE TABLE part_issue_status(
    idstatus        int            IDENTITY(1,1),
    idissue         int            NULL,
    idstatustype    DOM_INTEGER    NULL,
    dtfrom          date           NULL,
    dtthru          date           NULL,
    CONSTRAINT PK__part_iss__EBF77C2DDB6D914F PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('part_issue_status') IS NOT NULL
    PRINT '<<< CREATED TABLE part_issue_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_issue_status >>>'
go

/* 
 * TABLE: part_issue_type 
 */

CREATE TABLE part_issue_type(
    idpartissuetype    DOM_INTEGER     NOT NULL,
    description        varchar(255)    NULL,
    CONSTRAINT PK__part_iss__E729E1D6CC141B1C PRIMARY KEY CLUSTERED (idpartissuetype)
)
go



IF OBJECT_ID('part_issue_type') IS NOT NULL
    PRINT '<<< CREATED TABLE part_issue_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_issue_type >>>'
go

/* 
 * TABLE: part_sales_order 
 */

CREATE TABLE part_sales_order(
    idorder    DOM_UUID     NOT NULL,
    qty        DOM_QTY      NULL,
    total      DOM_MONEY    NULL,
    CONSTRAINT PK__part_sal__44DCCDABBF72E941 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('part_sales_order') IS NOT NULL
    PRINT '<<< CREATED TABLE part_sales_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE part_sales_order >>>'
go

/* 
 * TABLE: party 
 */

CREATE TABLE party(
    idparty    DOM_UUID       NOT NULL,
    idtype     DOM_INTEGER    NULL,
    CONSTRAINT PK__party__9E44201F4B001C70 PRIMARY KEY NONCLUSTERED (idparty)
)
go



IF OBJECT_ID('party') IS NOT NULL
    PRINT '<<< CREATED TABLE party >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party >>>'
go

/* 
 * TABLE: party_category 
 */

CREATE TABLE party_category(
    idcategory     DOM_IDENTITY       IDENTITY(1,1),
    idcattyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__party_ca__78C6B76251E01D45 PRIMARY KEY CLUSTERED (idcategory)
)
go



IF OBJECT_ID('party_category') IS NOT NULL
    PRINT '<<< CREATED TABLE party_category >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_category >>>'
go

/* 
 * TABLE: party_category_impl 
 */

CREATE TABLE party_category_impl(
    idparty       DOM_UUID        NOT NULL,
    idcategory    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK1019 PRIMARY KEY CLUSTERED (idparty, idcategory)
)
go



IF OBJECT_ID('party_category_impl') IS NOT NULL
    PRINT '<<< CREATED TABLE party_category_impl >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_category_impl >>>'
go

/* 
 * TABLE: party_category_type 
 */

CREATE TABLE party_category_type(
    idcattyp       DOM_INTEGER        NOT NULL,
    idparent       DOM_INTEGER        NULL,
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__party_ca__4574011B0241D92A PRIMARY KEY CLUSTERED (idcattyp)
)
go



IF OBJECT_ID('party_category_type') IS NOT NULL
    PRINT '<<< CREATED TABLE party_category_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_category_type >>>'
go

/* 
 * TABLE: party_classification 
 */

CREATE TABLE party_classification(
    idclassification    DOM_UUID        NOT NULL,
    idparty             DOM_UUID        NULL,
    idowner             DOM_UUID        NULL,
    idcategory          DOM_IDENTITY    NULL,
    idcattyp            DOM_INTEGER     NULL,
    dtfrom              DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru              DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__party_cl__677F70841966D6A5 PRIMARY KEY CLUSTERED (idclassification)
)
go



IF OBJECT_ID('party_classification') IS NOT NULL
    PRINT '<<< CREATED TABLE party_classification >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_classification >>>'
go

/* 
 * TABLE: party_contact 
 */

CREATE TABLE party_contact(
    idparty      DOM_UUID    NOT NULL,
    idcontact    DOM_UUID    NOT NULL,
    CONSTRAINT PK__party_co__E472FC0E3B6FFECF PRIMARY KEY CLUSTERED (idparty, idcontact)
)
go



IF OBJECT_ID('party_contact') IS NOT NULL
    PRINT '<<< CREATED TABLE party_contact >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_contact >>>'
go

/* 
 * TABLE: party_contact_mechanism 
 */

CREATE TABLE party_contact_mechanism(
    idparcon         DOM_IDENTITY    IDENTITY(1,1),
    idparty          DOM_UUID        NULL,
    idcontact        DOM_UUID        NULL,
    idpurposetype    DOM_IDENTITY    DEFAULT 1 NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__party_co__82353190A15270A3 PRIMARY KEY CLUSTERED (idparcon)
)
go



IF OBJECT_ID('party_contact_mechanism') IS NOT NULL
    PRINT '<<< CREATED TABLE party_contact_mechanism >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_contact_mechanism >>>'
go

/* 
 * TABLE: party_document 
 */

CREATE TABLE party_document(
    iddocument     DOM_UUID           NOT NULL,
    idparty        DOM_UUID           NULL,
    contenttype    DOM_DESCRIPTION    NULL,
    content        DOM_BLOB           NULL,
    CONSTRAINT PK__party_do__2B5254138C933AE5 PRIMARY KEY CLUSTERED (iddocument)
)
go



IF OBJECT_ID('party_document') IS NOT NULL
    PRINT '<<< CREATED TABLE party_document >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_document >>>'
go

/* 
 * TABLE: party_facility_purpose 
 */

CREATE TABLE party_facility_purpose(
    idparfacpur      DOM_UUID        NOT NULL,
    idparty          DOM_UUID        NULL,
    idpurposetype    DOM_IDENTITY    NULL,
    idfacility       DOM_UUID        NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__party_fa__6958712590D46336 PRIMARY KEY CLUSTERED (idparfacpur)
)
go



IF OBJECT_ID('party_facility_purpose') IS NOT NULL
    PRINT '<<< CREATED TABLE party_facility_purpose >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_facility_purpose >>>'
go

/* 
 * TABLE: party_relationship 
 */

CREATE TABLE party_relationship(
    idparrel        DOM_UUID        NOT NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreltyp        DOM_IDENTITY    NULL,
    idparrolfro     DOM_UUID        NULL,
    idparrolto      DOM_UUID        NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__party_re__86F7AE0B6536C5DE PRIMARY KEY NONCLUSTERED (idparrel)
)
go



IF OBJECT_ID('party_relationship') IS NOT NULL
    PRINT '<<< CREATED TABLE party_relationship >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_relationship >>>'
go

/* 
 * TABLE: party_role 
 */

CREATE TABLE party_role(
    idparrol      DOM_UUID        NOT NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    dtregister    DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__party_ro__86F77EC3B9808953 PRIMARY KEY NONCLUSTERED (idparrol)
)
go



IF OBJECT_ID('party_role') IS NOT NULL
    PRINT '<<< CREATED TABLE party_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_role >>>'
go

/* 
 * TABLE: party_role_status 
 */

CREATE TABLE party_role_status(
    idstatus        DOM_UUID        NOT NULL,
    idparrol        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__party_ro__EBF77C2C21AB89EF PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('party_role_status') IS NOT NULL
    PRINT '<<< CREATED TABLE party_role_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_role_status >>>'
go

/* 
 * TABLE: party_role_type 
 */

CREATE TABLE party_role_type(
    idparty       DOM_UUID        NOT NULL,
    idroletype    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK__party_ro__19936A144651FCBF PRIMARY KEY CLUSTERED (idparty, idroletype)
)
go



IF OBJECT_ID('party_role_type') IS NOT NULL
    PRINT '<<< CREATED TABLE party_role_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_role_type >>>'
go

/* 
 * TABLE: payment 
 */

CREATE TABLE payment(
    idpayment        DOM_UUID           NOT NULL,
    idpaytyp         DOM_IDENTITY       NULL,
    idpaymet         DOM_IDENTITY       NULL,
    idinternal       DOM_ID             NULL,
    idpaito          DOM_ID             NULL,
    idpaifro         DOM_ID             NULL,
    paymentnumber    DOM_ID             NULL,
    dtcreate         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    amount           DOM_MONEY          NULL,
    reffnum          DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__payment__64452FA4EE95D51B PRIMARY KEY NONCLUSTERED (idpayment)
)
go



IF OBJECT_ID('payment') IS NOT NULL
    PRINT '<<< CREATED TABLE payment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment >>>'
go

/* 
 * TABLE: payment_acctg_trans 
 */

CREATE TABLE payment_acctg_trans(
    idacctra     DOM_IDENTITY    NOT NULL,
    idpayment    DOM_UUID        NULL,
    CONSTRAINT PK__payment___F099E8A082D29B9A PRIMARY KEY CLUSTERED (idacctra)
)
go



IF OBJECT_ID('payment_acctg_trans') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_acctg_trans >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_acctg_trans >>>'
go

/* 
 * TABLE: payment_application 
 */

CREATE TABLE payment_application(
    idpayapp         DOM_UUID        NOT NULL,
    idpayment        DOM_UUID        NULL,
    idbilacc         DOM_IDENTITY    NULL,
    idbilling        DOM_UUID        NULL,
    amountapplied    DOM_MONEY       NULL,
    CONSTRAINT PK__payment___29D8D22B5ACCAFC4 PRIMARY KEY NONCLUSTERED (idpayapp)
)
go



IF OBJECT_ID('payment_application') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_application >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_application >>>'
go

/* 
 * TABLE: payment_method 
 */

CREATE TABLE payment_method(
    idpaymet         DOM_IDENTITY       IDENTITY(1,1),
    idpaymettyp      DOM_IDENTITY       NULL,
    refkey           DOM_ID             NULL,
    description      DOM_DESCRIPTION    NULL,
    accountnumber    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__payment___2CF3DFCB6B11AF52 PRIMARY KEY CLUSTERED (idpaymet)
)
go



IF OBJECT_ID('payment_method') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_method >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_method >>>'
go

/* 
 * TABLE: payment_method_type 
 */

CREATE TABLE payment_method_type(
    idpaymettyp    DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__payment___C86CB996EDDA62E4 PRIMARY KEY NONCLUSTERED (idpaymettyp)
)
go



IF OBJECT_ID('payment_method_type') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_method_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_method_type >>>'
go

/* 
 * TABLE: payment_role 
 */

CREATE TABLE payment_role(
    idrole        DOM_UUID        NOT NULL,
    idpayment     DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__payment___F324A06C18AF9880 PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('payment_role') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_role >>>'
go

/* 
 * TABLE: payment_status 
 */

CREATE TABLE payment_status(
    idstatus        DOM_UUID        NOT NULL,
    idpayment       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__payment___EBF77C2C891F0E25 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('payment_status') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_status >>>'
go

/* 
 * TABLE: payment_type 
 */

CREATE TABLE payment_type(
    idpaytyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__payment___541D3B6FB8512F8D PRIMARY KEY NONCLUSTERED (idpaytyp)
)
go



IF OBJECT_ID('payment_type') IS NOT NULL
    PRINT '<<< CREATED TABLE payment_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE payment_type >>>'
go

/* 
 * TABLE: pendingreason 
 */

CREATE TABLE pendingreason(
    idpendingreason      int             NOT NULL,
    descpendingreason    varchar(250)    NULL,
    lastmodifieddate     datetime        NULL,
    statuscode           int             NULL,
    CONSTRAINT PK__pendingr__E97A7D093F1DB9D8 PRIMARY KEY CLUSTERED (idpendingreason)
)
go



IF OBJECT_ID('pendingreason') IS NOT NULL
    PRINT '<<< CREATED TABLE pendingreason >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pendingreason >>>'
go

/* 
 * TABLE: person 
 */

CREATE TABLE person(
    idparty             DOM_UUID           NOT NULL,
    fname               DOM_NAME           NULL,
    lname               DOM_NAME           NULL,
    pob                 DOM_NAME           NULL,
    dtob                DOM_DATE           NULL,
    bloodtype           DOM_SHORTID        NULL,
    gender              DOM_FLAG           NULL,
    personalidnumber    DOM_ID             NULL,
    familyidnumber      DOM_ID             NULL,
    taxidnumber         DOM_ID             NULL,
    idreligiontype      DOM_INTEGER        NULL,
    idworktype          DOM_INTEGER        NULL,
    cellphone1          DOM_DESCRIPTION    NULL,
    cellphone2          DOM_DESCRIPTION    NULL,
    privatemail         DOM_DESCRIPTION    NULL,
    phone               DOM_DESCRIPTION    NULL,
    username            DOM_NAME           NULL,
    CONSTRAINT PK__person__9E44201F4D9715AA PRIMARY KEY NONCLUSTERED (idparty)
)
go



IF OBJECT_ID('person') IS NOT NULL
    PRINT '<<< CREATED TABLE person >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE person >>>'
go

/* 
 * TABLE: person_prospect 
 */

CREATE TABLE person_prospect(
    idprospect      DOM_UUID    NOT NULL,
    idparty         DOM_UUID    NULL,
    fname           DOM_NAME    NULL,
    lname           DOM_NAME    NULL,
    cellphone       DOM_NAME    NULL,
    facilityname    DOM_NAME    NULL,
    internname      DOM_NAME    NULL,
    CONSTRAINT PK__person_p__580AA288F29BCB4F PRIMARY KEY NONCLUSTERED (idprospect)
)
go



IF OBJECT_ID('person_prospect') IS NOT NULL
    PRINT '<<< CREATED TABLE person_prospect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE person_prospect >>>'
go

/* 
 * TABLE: person_suspect 
 */

CREATE TABLE person_suspect(
    idsuspect    DOM_UUID    NOT NULL,
    idparty      DOM_UUID    NULL,
    CONSTRAINT PK__person_s__2F025AB2C285DCB5 PRIMARY KEY CLUSTERED (idsuspect)
)
go



IF OBJECT_ID('person_suspect') IS NOT NULL
    PRINT '<<< CREATED TABLE person_suspect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE person_suspect >>>'
go

/* 
 * TABLE: person_tm 
 */

CREATE TABLE person_tm(
    idperson              int             NOT NULL,
    name                  nvarchar(50)    NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    CONSTRAINT PK__person_t__03BCEA8CEC9CCD5B PRIMARY KEY CLUSTERED (idperson)
)
go



IF OBJECT_ID('person_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE person_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE person_tm >>>'
go

/* 
 * TABLE: personal_customer 
 */

CREATE TABLE personal_customer(
    idcustomer    DOM_ID    NOT NULL,
    CONSTRAINT PK__personal__53EFD0A7BD941016 PRIMARY KEY NONCLUSTERED (idcustomer)
)
go



IF OBJECT_ID('personal_customer') IS NOT NULL
    PRINT '<<< CREATED TABLE personal_customer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE personal_customer >>>'
go

/* 
 * TABLE: personsosmed 
 */

CREATE TABLE personsosmed(
    idsosmed        int            IDENTITY(1,1),
    idsosmedtype    int            NULL,
    idparty         DOM_UUID       NULL,
    username        varchar(30)    NULL,
    CONSTRAINT PK__personso__C5146E04493520B6 PRIMARY KEY CLUSTERED (idsosmed)
)
go



IF OBJECT_ID('personsosmed') IS NOT NULL
    PRINT '<<< CREATED TABLE personsosmed >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE personsosmed >>>'
go

/* 
 * TABLE: picking_slip 
 */

CREATE TABLE picking_slip(
    idslip              DOM_UUID       NOT NULL,
    idinvite            DOM_UUID       NULL,
    idordite            DOM_UUID       NULL,
    idshipto            DOM_ID         NULL,
    qty                 DOM_QTY        NULL,
    acc1                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc2                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc3                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc4                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc5                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc6                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc7                DOM_BOOLEAN    DEFAULT 0 NULL,
    acc8                DOM_BOOLEAN    DEFAULT 0 NULL,
    acctambah1          DOM_BOOLEAN    DEFAULT 0 NULL,
    acctambah2          DOM_BOOLEAN    DEFAULT 0 NULL,
    promat1             DOM_BOOLEAN    DEFAULT 0 NULL,
    promat2             DOM_BOOLEAN    DEFAULT 0 NULL,
    promatdirectgift    DOM_BOOLEAN    DEFAULT 0 NULL,
    idinternalmecha     DOM_UUID       NULL,
    idexternalmecha     DOM_UUID       NULL,
    CONSTRAINT PK__picking___D730145D263E57A2 PRIMARY KEY NONCLUSTERED (idslip)
)
go



IF OBJECT_ID('picking_slip') IS NOT NULL
    PRINT '<<< CREATED TABLE picking_slip >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE picking_slip >>>'
go

/* 
 * TABLE: pkb 
 */

CREATE TABLE pkb(
    idpkb                  DOM_IDENTITY      IDENTITY(1,1),
    idinternal             DOM_ID            NULL,
    idstatuspkbinternal    DOM_INTEGER       NULL,
    idstatuspkbcust        DOM_INTEGER       NULL,
    idcustomer             DOM_ID            NULL,
    idqueue                DOM_IDENTITY      NULL,
    idvehicle              DOM_UUID          NULL,
    idpaymettyp            DOM_IDENTITY      NULL,
    idtypepkb              DOM_INTEGER       NULL,
    idvehide               DOM_UUID          NULL,
    idpkbjobreturn         DOM_IDENTITY      NULL,
    nopkb                  varchar(15)       NOT NULL,
    pkbdate                datetime          NULL,
    lamentation            varchar(50)       NULL,
    lastmodifieddate       date              NULL,
    lastmodifieduserid     int               NULL,
    statuscode             int               NULL,
    subtotalservice        decimal(18, 2)    NULL,
    subtotalpart           decimal(18, 2)    NULL,
    reason                 varchar(255)      NULL,
    kilometer              decimal(14, 2)    NULL,
    nextservicedate        datetime          NULL,
    jobsuggest             varchar(50)       NULL,
    CONSTRAINT PK__pkb__04B793EE5FA5AA68 PRIMARY KEY NONCLUSTERED (idpkb),
    UNIQUE (nopkb)
)
go



IF OBJECT_ID('pkb') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb >>>'
go

/* 
 * TABLE: pkb_estimation 
 */

CREATE TABLE pkb_estimation(
    idpkb                DOM_IDENTITY      NOT NULL,
    estimatedprice       decimal(18, 0)    NULL,
    depositcust          decimal(18, 0)    NULL,
    dp                   decimal(18, 0)    NULL,
    beawaited            bit               NULL,
    estimatedduration    int               NULL,
    hoursregist          int               NULL,
    estimatedstart       int               NULL,
    estimatedfinish      int               NULL,
    CONSTRAINT PK__pkb_esti__04B793EF921AEC00 PRIMARY KEY CLUSTERED (idpkb)
)
go



IF OBJECT_ID('pkb_estimation') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_estimation >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_estimation >>>'
go

/* 
 * TABLE: pkb_inspection 
 */

CREATE TABLE pkb_inspection(
    idinspection    int            IDENTITY(1,1),
    description     varchar(60)    NULL,
    dtcreate        datetime       NULL,
    CONSTRAINT PK__pkb_insp__19F13E122999FF12 PRIMARY KEY CLUSTERED (idinspection)
)
go



IF OBJECT_ID('pkb_inspection') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_inspection >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_inspection >>>'
go

/* 
 * TABLE: pkb_part 
 */

CREATE TABLE pkb_part(
    IdPkbPart             DOM_INTEGER       NOT NULL,
    idpkbservice          DOM_INTEGER       NULL,
    idproduct             varchar(90)       NULL,
    idpkb                 DOM_IDENTITY      NULL,
    idchargeto            int               NULL,
    nopart                varchar(300)      NULL,
    qty                   decimal(18, 4)    NULL,
    het                   decimal(18, 4)    NULL,
    discount              decimal(18, 4)    NULL,
    unitprice             decimal(18, 4)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    descpart              varchar(270)      NULL,
    totalamount           decimal(18, 4)    NULL,
    isrequest             tinyint           NULL,
    isclaimhotline        tinyint           NULL,
    taxamount             decimal(18, 4)    NULL,
    idcategory            int               NULL,
    sequence              int               NULL,
    qtyrequest            decimal(18, 4)    NULL,
    idinvite              varchar(192)      NULL,
    isdefaultpart         tinyint           NULL,
    CONSTRAINT PK__pkb_part__8FEA1D86080DA6D6 PRIMARY KEY CLUSTERED (IdPkbPart)
)
go



IF OBJECT_ID('pkb_part') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_part >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_part >>>'
go

/* 
 * TABLE: pkb_part_billing_item 
 */

CREATE TABLE pkb_part_billing_item(
    idbilite     DOM_UUID    NOT NULL,
    idpkbpart    int         NULL,
    idparrol     DOM_UUID    NULL,
    CONSTRAINT PK__pkb_part__4791E44ADE53BDBB PRIMARY KEY CLUSTERED (idbilite)
)
go



IF OBJECT_ID('pkb_part_billing_item') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_part_billing_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_part_billing_item >>>'
go

/* 
 * TABLE: pkb_part_request_item 
 */

CREATE TABLE pkb_part_request_item(
    idreqitem    DOM_UUID    NOT NULL,
    idpkbpart    int         NULL,
    CONSTRAINT PK__pkb_part__D27E3AA60BD7CD6A PRIMARY KEY CLUSTERED (idreqitem)
)
go



IF OBJECT_ID('pkb_part_request_item') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_part_request_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_part_request_item >>>'
go

/* 
 * TABLE: pkb_service 
 */

CREATE TABLE pkb_service(
    IdPkbService             DOM_INTEGER       NOT NULL,
    idpkb                    DOM_IDENTITY      NULL,
    idproduct                DOM_ID            NULL,
    idchargeto               int               NULL,
    codeservice              varchar(90)       NULL,
    descservice              varchar(270)      NULL,
    unitprice                decimal(18, 4)    NULL,
    discount                 decimal(18, 4)    NULL,
    flatrate                 decimal(5, 2)     NULL,
    chargeto                 varchar(60)       NULL,
    issuggestedbymechanic    tinyint           NULL,
    lastmodifieddate         date              NULL,
    lastmodifieduserid       int               NULL,
    statuscode               int               NULL,
    totalamount              decimal(18, 4)    NULL,
    isrequest                tinyint           NULL,
    taxamount                decimal(18, 4)    NULL,
    idcategory               int               NULL,
    sequence                 int               NULL,
    standardrate             decimal(18, 4)    NULL,
    isjobreturn              tinyint           NULL,
    baseprice                decimal(18, 4)    NULL,
    CONSTRAINT PK__pkb_serv__E2AA1C2740A4D61B PRIMARY KEY CLUSTERED (IdPkbService)
)
go



IF OBJECT_ID('pkb_service') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_service >>>'
go

/* 
 * TABLE: pkb_service_billing_item 
 */

CREATE TABLE pkb_service_billing_item(
    idbilite        DOM_UUID    NOT NULL,
    idparrol        DOM_UUID    NULL,
    idpkbservice    int         NULL,
    CONSTRAINT PK__pkb_serv__4791E44A5D77F3AC PRIMARY KEY CLUSTERED (idbilite)
)
go



IF OBJECT_ID('pkb_service_billing_item') IS NOT NULL
    PRINT '<<< CREATED TABLE pkb_service_billing_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE pkb_service_billing_item >>>'
go

/* 
 * TABLE: po_detail_part 
 */

CREATE TABLE po_detail_part(
    idordite              DOM_UUID    NOT NULL,
    idpkbpart             int         NULL,
    lastmodifieddate      date        NULL,
    lastmodefieduserid    int         NULL,
    statuscode            int         NULL,
    CONSTRAINT PK__po_detai__D0E2AC1C644C86DD PRIMARY KEY NONCLUSTERED (idordite)
)
go



IF OBJECT_ID('po_detail_part') IS NOT NULL
    PRINT '<<< CREATED TABLE po_detail_part >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE po_detail_part >>>'
go

/* 
 * TABLE: po_detail_service 
 */

CREATE TABLE po_detail_service(
    idordite              DOM_UUID    NOT NULL,
    idpkbservice          int         NULL,
    lastmodifieddate      date        NULL,
    lastmodifieduserid    int         NULL,
    statuscode            int         NULL,
    CONSTRAINT PK__po_detai__D0E2AC1C5647BCFC PRIMARY KEY NONCLUSTERED (idordite)
)
go



IF OBJECT_ID('po_detail_service') IS NOT NULL
    PRINT '<<< CREATED TABLE po_detail_service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE po_detail_service >>>'
go

/* 
 * TABLE: po_status 
 */

CREATE TABLE po_status(
    idpostatus      DOM_INTEGER    NOT NULL,
    idstatustype    DOM_INTEGER    NULL,
    description     varchar(30)    NULL,
    CONSTRAINT PK__po_statu__B35CEE7287027BFD PRIMARY KEY CLUSTERED (idpostatus)
)
go



IF OBJECT_ID('po_status') IS NOT NULL
    PRINT '<<< CREATED TABLE po_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE po_status >>>'
go

/* 
 * TABLE: position_authority 
 */

CREATE TABLE position_authority(
    idpostyp    DOM_IDENTITY    NOT NULL,
    name        DOM_NAME        NOT NULL,
    CONSTRAINT PK__position__2D0A62971144CA30 PRIMARY KEY CLUSTERED (idpostyp, name)
)
go



IF OBJECT_ID('position_authority') IS NOT NULL
    PRINT '<<< CREATED TABLE position_authority >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE position_authority >>>'
go

/* 
 * TABLE: position_fullfillment 
 */

CREATE TABLE position_fullfillment(
    idposfulfil    DOM_IDENTITY    IDENTITY(1,1),
    idposition     DOM_UUID        NULL,
    idperson       DOM_UUID        NULL,
    username       DOM_NAME        NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__position__BC5A6548C815DD2D PRIMARY KEY NONCLUSTERED (idposfulfil)
)
go



IF OBJECT_ID('position_fullfillment') IS NOT NULL
    PRINT '<<< CREATED TABLE position_fullfillment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE position_fullfillment >>>'
go

/* 
 * TABLE: position_reporting_structure 
 */

CREATE TABLE position_reporting_structure(
    idposrepstru    DOM_IDENTITY    IDENTITY(1,1),
    idposfro        DOM_UUID        NULL,
    idposto         DOM_UUID        NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__position__FBE5691D573AD344 PRIMARY KEY NONCLUSTERED (idposrepstru)
)
go



IF OBJECT_ID('position_reporting_structure') IS NOT NULL
    PRINT '<<< CREATED TABLE position_reporting_structure >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE position_reporting_structure >>>'
go

/* 
 * TABLE: position_type 
 */

CREATE TABLE position_type(
    idpostyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    title          DOM_NAME           NULL,
    CONSTRAINT PK__position__9A247067FE045012 PRIMARY KEY NONCLUSTERED (idpostyp)
)
go



IF OBJECT_ID('position_type') IS NOT NULL
    PRINT '<<< CREATED TABLE position_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE position_type >>>'
go

/* 
 * TABLE: positions 
 */

CREATE TABLE positions(
    idposition        DOM_UUID           NOT NULL,
    idpostyp          DOM_IDENTITY       NULL,
    idorganization    DOM_UUID           NULL,
    idusrmed          DOM_UUID           NULL,
    idinternal        DOM_ID             NULL,
    seqnum            DOM_INTEGER        NULL,
    username          DOM_NAME           NULL,
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__position__5148129A43784D58 PRIMARY KEY NONCLUSTERED (idposition)
)
go



IF OBJECT_ID('positions') IS NOT NULL
    PRINT '<<< CREATED TABLE positions >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE positions >>>'
go

/* 
 * TABLE: positions_status 
 */

CREATE TABLE positions_status(
    idstatus        DOM_UUID        NOT NULL,
    idposition      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__position__EBF77C2C77269E23 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('positions_status') IS NOT NULL
    PRINT '<<< CREATED TABLE positions_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE positions_status >>>'
go

/* 
 * TABLE: postal_address 
 */

CREATE TABLE postal_address(
    idcontact     DOM_UUID       NOT NULL,
    idprovince    DOM_UUID       NULL,
    idcity        DOM_UUID       NULL,
    iddistrict    DOM_UUID       NULL,
    idvillage     DOM_UUID       NULL,
    idpostal      DOM_UUID       NULL,
    address       DOM_ADDRESS    NULL,
    address1      DOM_ADDRESS    NULL,
    address2      DOM_ADDRESS    NULL,
    CONSTRAINT PK__postal_a__A36DC1074B74883A PRIMARY KEY NONCLUSTERED (idcontact)
)
go



IF OBJECT_ID('postal_address') IS NOT NULL
    PRINT '<<< CREATED TABLE postal_address >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE postal_address >>>'
go

/* 
 * TABLE: price_agreement_item 
 */

CREATE TABLE price_agreement_item(
    idagrite     DOM_UUID    NOT NULL,
    idproduct    DOM_ID      NULL,
    CONSTRAINT PK__price_ag__24275008249A42F4 PRIMARY KEY CLUSTERED (idagrite)
)
go



IF OBJECT_ID('price_agreement_item') IS NOT NULL
    PRINT '<<< CREATED TABLE price_agreement_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE price_agreement_item >>>'
go

/* 
 * TABLE: price_component 
 */

CREATE TABLE price_component(
    idpricom          DOM_UUID        NOT NULL,
    idparty           DOM_UUID        NULL,
    idproduct         DOM_ID          NULL,
    idpricetype       DOM_INTEGER     NULL,
    idparcat          DOM_IDENTITY    NULL,
    idprocat          DOM_IDENTITY    NULL,
    idgeobou          DOM_UUID        NULL,
    idagrite          DOM_UUID        NULL,
    price             DOM_MONEY       NULL,
    manfucterprice    DOM_MONEY       NULL,
    percentvalue      DOM_PERCENT     DEFAULT 0 NULL,
    standardrate      DOM_MONEY       NULL,
    dtfrom            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru            DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__price_co__1476EE33DB94F96B PRIMARY KEY NONCLUSTERED (idpricom)
)
go



IF OBJECT_ID('price_component') IS NOT NULL
    PRINT '<<< CREATED TABLE price_component >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE price_component >>>'
go

/* 
 * TABLE: price_type 
 */

CREATE TABLE price_type(
    idpricetype    DOM_INTEGER        NOT NULL,
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__price_ty__54D8EA8DD7F26A69 PRIMARY KEY NONCLUSTERED (idpricetype)
)
go



IF OBJECT_ID('price_type') IS NOT NULL
    PRINT '<<< CREATED TABLE price_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE price_type >>>'
go

/* 
 * TABLE: product 
 */

CREATE TABLE product(
    idproduct         DOM_ID             NOT NULL,
    idprotyp          DOM_IDENTITY       NULL,
    name              DOM_NAME           NULL,
    description       DOM_DESCRIPTION    NULL,
    dtintroduction    DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtdiscontinue     DOM_DATE           NULL,
    istaxable         DOM_BOOLEAN        DEFAULT 0 NULL,
    pricetype         DOM_INTEGER        NULL,
    CONSTRAINT PK__product__8D50742542C20CCE PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('product') IS NOT NULL
    PRINT '<<< CREATED TABLE product >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product >>>'
go

/* 
 * TABLE: product_association 
 */

CREATE TABLE product_association(
    idproass         DOM_UUID        NOT NULL,
    idasstyp         DOM_IDENTITY    NULL,
    idproductto      DOM_ID          NULL,
    idproductfrom    DOM_ID          NULL,
    qty              DOM_QTY         NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__product___472A2BE3B40EF4D4 PRIMARY KEY NONCLUSTERED (idproass)
)
go



IF OBJECT_ID('product_association') IS NOT NULL
    PRINT '<<< CREATED TABLE product_association >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_association >>>'
go

/* 
 * TABLE: product_category 
 */

CREATE TABLE product_category(
    idcategory     DOM_IDENTITY       IDENTITY(1,1),
    idcattyp       DOM_IDENTITY       NULL,
    refkey         DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__product___78C6B76292D477FF PRIMARY KEY CLUSTERED (idcategory)
)
go



IF OBJECT_ID('product_category') IS NOT NULL
    PRINT '<<< CREATED TABLE product_category >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_category >>>'
go

/* 
 * TABLE: product_category_impl 
 */

CREATE TABLE product_category_impl(
    idproduct     DOM_ID          NOT NULL,
    idcategory    DOM_IDENTITY    NOT NULL,
    CONSTRAINT PK1016 PRIMARY KEY CLUSTERED (idproduct, idcategory)
)
go



IF OBJECT_ID('product_category_impl') IS NOT NULL
    PRINT '<<< CREATED TABLE product_category_impl >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_category_impl >>>'
go

/* 
 * TABLE: product_category_type 
 */

CREATE TABLE product_category_type(
    idcattyp       DOM_IDENTITY       IDENTITY(1,1),
    idparent       DOM_IDENTITY       NULL,
    idrultyp       DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    refkey         DOM_ID             NULL,
    CONSTRAINT PK__product___4574011B522B4BC9 PRIMARY KEY CLUSTERED (idcattyp)
)
go



IF OBJECT_ID('product_category_type') IS NOT NULL
    PRINT '<<< CREATED TABLE product_category_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_category_type >>>'
go

/* 
 * TABLE: product_classification 
 */

CREATE TABLE product_classification(
    idclassification    DOM_UUID        NOT NULL,
    idproduct           DOM_ID          NULL,
    idcategory          DOM_IDENTITY    NULL,
    idcattyp            DOM_IDENTITY    NULL,
    idowner             DOM_UUID        NULL,
    dtfrom              DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru              DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__product___677F7084AB9B9B48 PRIMARY KEY CLUSTERED (idclassification)
)
go



IF OBJECT_ID('product_classification') IS NOT NULL
    PRINT '<<< CREATED TABLE product_classification >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_classification >>>'
go

/* 
 * TABLE: product_document 
 */

CREATE TABLE product_document(
    iddocument     DOM_UUID           NOT NULL,
    idproduct      DOM_ID             NULL,
    contenttype    DOM_DESCRIPTION    NULL,
    content        DOM_BLOB           NULL,
    CONSTRAINT PK__product___2B5254139DF2DD27 PRIMARY KEY CLUSTERED (iddocument)
)
go



IF OBJECT_ID('product_document') IS NOT NULL
    PRINT '<<< CREATED TABLE product_document >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_document >>>'
go

/* 
 * TABLE: product_feature_impl 
 */

CREATE TABLE product_feature_impl(
    products_id    DOM_ID         NOT NULL,
    features_id    DOM_INTEGER    NOT NULL,
    CONSTRAINT PK1015 PRIMARY KEY CLUSTERED (products_id, features_id)
)
go



IF OBJECT_ID('product_feature_impl') IS NOT NULL
    PRINT '<<< CREATED TABLE product_feature_impl >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_feature_impl >>>'
go

/* 
 * TABLE: product_package_receipt 
 */

CREATE TABLE product_package_receipt(
    idpackage         DOM_UUID    NOT NULL,
    idshifro          DOM_ID      NULL,
    documentnumber    DOM_ID      NULL,
    CONSTRAINT PK__product___C3EF12B62A70EDD0 PRIMARY KEY CLUSTERED (idpackage)
)
go



IF OBJECT_ID('product_package_receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE product_package_receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_package_receipt >>>'
go

/* 
 * TABLE: product_purchase_order 
 */

CREATE TABLE product_purchase_order(
    idorder    DOM_UUID    NOT NULL,
    CONSTRAINT PK__product___44DCCDAB2BACD6A7 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('product_purchase_order') IS NOT NULL
    PRINT '<<< CREATED TABLE product_purchase_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_purchase_order >>>'
go

/* 
 * TABLE: product_shipment_incoming 
 */

CREATE TABLE product_shipment_incoming(
    idshipment    DOM_UUID    NOT NULL,
    CONSTRAINT PK__product___84926834A08858F0 PRIMARY KEY CLUSTERED (idshipment)
)
go



IF OBJECT_ID('product_shipment_incoming') IS NOT NULL
    PRINT '<<< CREATED TABLE product_shipment_incoming >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_shipment_incoming >>>'
go

/* 
 * TABLE: product_shipment_receipt 
 */

CREATE TABLE product_shipment_receipt(
    idreceipt    DOM_UUID    NOT NULL,
    CONSTRAINT PK__product___889F9A67917257A2 PRIMARY KEY CLUSTERED (idreceipt)
)
go



IF OBJECT_ID('product_shipment_receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE product_shipment_receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_shipment_receipt >>>'
go

/* 
 * TABLE: product_type 
 */

CREATE TABLE product_type(
    idprotyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__product___4A700A461A5C23E4 PRIMARY KEY NONCLUSTERED (idprotyp)
)
go



IF OBJECT_ID('product_type') IS NOT NULL
    PRINT '<<< CREATED TABLE product_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE product_type >>>'
go

/* 
 * TABLE: prospect 
 */

CREATE TABLE prospect(
    idprospect            DOM_UUID        NOT NULL,
    idbroker              DOM_UUID        NULL,
    idprosou              DOM_INTEGER     NULL,
    idevetyp              DOM_IDENTITY    NULL,
    idfacility            DOM_UUID        NULL,
    idsalesman            DOM_UUID        NULL,
    idsuspect             DOM_UUID        NULL,
    idsalescoordinator    DOM_UUID        NULL,
    idinternal            DOM_ID          NULL,
    prospectnumber        DOM_ID          NULL,
    prospectcount         DOM_INTEGER     NULL,
    eveloc                DOM_LONGDESC    NULL,
    dtfollowup            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__prospect__580AA2886C5DDD39 PRIMARY KEY NONCLUSTERED (idprospect)
)
go



IF OBJECT_ID('prospect') IS NOT NULL
    PRINT '<<< CREATED TABLE prospect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE prospect >>>'
go

/* 
 * TABLE: prospect_role 
 */

CREATE TABLE prospect_role(
    idrole        DOM_UUID        NOT NULL,
    idprospect    DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__prospect__F324A06C0CE7C110 PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('prospect_role') IS NOT NULL
    PRINT '<<< CREATED TABLE prospect_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE prospect_role >>>'
go

/* 
 * TABLE: prospect_source 
 */

CREATE TABLE prospect_source(
    idprosou       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__prospect__4B89860F745C1917 PRIMARY KEY NONCLUSTERED (idprosou)
)
go



IF OBJECT_ID('prospect_source') IS NOT NULL
    PRINT '<<< CREATED TABLE prospect_source >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE prospect_source >>>'
go

/* 
 * TABLE: prospect_status 
 */

CREATE TABLE prospect_status(
    idstatus        DOM_UUID        NOT NULL,
    idprospect      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__prospect__EBF77C2C07D47230 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('prospect_status') IS NOT NULL
    PRINT '<<< CREATED TABLE prospect_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE prospect_status >>>'
go

/* 
 * TABLE: province 
 */

CREATE TABLE province(
    idgeobou    DOM_UUID    NOT NULL,
    CONSTRAINT PK__province__96A5A05ECA61D5DF PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



IF OBJECT_ID('province') IS NOT NULL
    PRINT '<<< CREATED TABLE province >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE province >>>'
go

/* 
 * TABLE: purchase_order 
 */

CREATE TABLE purchase_order(
    idorder    DOM_UUID        NOT NULL,
    reason     DOM_LONGDESC    NULL,
    CONSTRAINT PK__purchase__44DCCDABC4FEAE8D PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('purchase_order') IS NOT NULL
    PRINT '<<< CREATED TABLE purchase_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE purchase_order >>>'
go

/* 
 * TABLE: purchase_request 
 */

CREATE TABLE purchase_request(
    idreq         DOM_UUID    NOT NULL,
    idproduct     DOM_ID      NULL,
    idinternal    DOM_ID      NULL,
    CONSTRAINT PK__purchase__24C5146D5838FBE5 PRIMARY KEY CLUSTERED (idreq)
)
go



IF OBJECT_ID('purchase_request') IS NOT NULL
    PRINT '<<< CREATED TABLE purchase_request >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE purchase_request >>>'
go

/* 
 * TABLE: purchaseorder_reception_tr 
 */

CREATE TABLE purchaseorder_reception_tr(
    idpurchaseorderreception    int               NOT NULL,
    idpurchaseorder             int               NULL,
    idcategoryasset             int               NULL,
    quantitypurchase            int               NULL,
    quantityaccepted            int               NULL,
    lastmodifieddate            datetime          NULL,
    lastmodifieduserid          int               NULL,
    itemname                    nvarchar(30)      NULL,
    price                       decimal(18, 2)    NULL,
    branchcode                  nvarchar(30)      NULL,
    typecode                    nvarchar(30)      NULL,
    merk                        nvarchar(30)      NULL,
    type                        nvarchar(30)      NULL,
    CONSTRAINT PK__purchase__C31649ABD77ADFF8 PRIMARY KEY CLUSTERED (idpurchaseorderreception)
)
go



IF OBJECT_ID('purchaseorder_reception_tr') IS NOT NULL
    PRINT '<<< CREATED TABLE purchaseorder_reception_tr >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE purchaseorder_reception_tr >>>'
go

/* 
 * TABLE: purchaseorder_status_tp 
 */

CREATE TABLE purchaseorder_status_tp(
    idpurchaseorderstatus    int             NOT NULL,
    description              nvarchar(50)    NULL,
    lastmodifieddate         datetime        NULL,
    lastmodifieduserid       int             NULL,
    CONSTRAINT PK__purchase__94C3330959BED053 PRIMARY KEY CLUSTERED (idpurchaseorderstatus)
)
go



IF OBJECT_ID('purchaseorder_status_tp') IS NOT NULL
    PRINT '<<< CREATED TABLE purchaseorder_status_tp >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE purchaseorder_status_tp >>>'
go

/* 
 * TABLE: purchaseorder_tm 
 */

CREATE TABLE purchaseorder_tm(
    idpurchaseorder       int              IDENTITY(1,1),
    ponumber              nvarchar(15)     NULL,
    podate                datetime         NULL,
    spgdate               datetime         NULL,
    spgnumber             nvarchar(20)     NULL,
    vendorname            nvarchar(500)    NULL,
    status                int              NULL,
    lastmodifieddate      datetime         NULL,
    lastmodifieduserid    int              NULL,
    CONSTRAINT PK__purchase__22CF4E527E33C298 PRIMARY KEY CLUSTERED (idpurchaseorder)
)
go



IF OBJECT_ID('purchaseorder_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE purchaseorder_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE purchaseorder_tm >>>'
go

/* 
 * TABLE: purpose_type 
 */

CREATE TABLE purpose_type(
    idpurposetype    DOM_IDENTITY       IDENTITY(1,1),
    description      DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__purpose___199BEB18F9A19FD5 PRIMARY KEY NONCLUSTERED (idpurposetype)
)
go



IF OBJECT_ID('purpose_type') IS NOT NULL
    PRINT '<<< CREATED TABLE purpose_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE purpose_type >>>'
go

/* 
 * TABLE: queue 
 */

CREATE TABLE queue(
    idqueue               DOM_IDENTITY    IDENTITY(1,1),
    idqueuestatus         DOM_INTEGER     NULL,
    idqueuetype           DOM_INTEGER     NULL,
    idbooking             DOM_IDENTITY    NULL,
    noqueue               varchar(10)     NOT NULL,
    customername          varchar(100)    NULL,
    vehiclenumber         varchar(20)     NULL,
    datequeue             datetime        NULL,
    waitingduration       int             NULL,
    ismissed              bit             NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__queue__4A81A85CD99235CF PRIMARY KEY NONCLUSTERED (idqueue),
    UNIQUE (noqueue)
)
go



IF OBJECT_ID('queue') IS NOT NULL
    PRINT '<<< CREATED TABLE queue >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE queue >>>'
go

/* 
 * TABLE: queue_status 
 */

CREATE TABLE queue_status(
    idqueuestatus    DOM_INTEGER    NOT NULL,
    description      varchar(30)    NULL,
    CONSTRAINT PK__queue_st__D598ABA0AC152084 PRIMARY KEY CLUSTERED (idqueuestatus)
)
go



IF OBJECT_ID('queue_status') IS NOT NULL
    PRINT '<<< CREATED TABLE queue_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE queue_status >>>'
go

/* 
 * TABLE: queue_type 
 */

CREATE TABLE queue_type(
    idqueuetype    DOM_INTEGER    NOT NULL,
    type           varchar(30)    NULL,
    CONSTRAINT PK__queue_ty__85B4B3C0706D0839 PRIMARY KEY CLUSTERED (idqueuetype)
)
go



IF OBJECT_ID('queue_type') IS NOT NULL
    PRINT '<<< CREATED TABLE queue_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE queue_type >>>'
go

/* 
 * TABLE: quote 
 */

CREATE TABLE quote(
    idquote        DOM_UUID           NOT NULL,
    idquotyp       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    dtissued       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    dteffective    DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__quote__4F00C6E73B92CBC1 PRIMARY KEY CLUSTERED (idquote)
)
go



IF OBJECT_ID('quote') IS NOT NULL
    PRINT '<<< CREATED TABLE quote >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE quote >>>'
go

/* 
 * TABLE: quote_item 
 */

CREATE TABLE quote_item(
    idquoteitem    DOM_UUID       NOT NULL,
    idquote        DOM_UUID       NULL,
    idproduct      DOM_ID         NULL,
    qty            DOM_QTY        NULL,
    unitprice      DOM_MONEY      NULL,
    pricetype      DOM_INTEGER    NULL,
    serialized     DOM_BOOLEAN    DEFAULT 0 NULL,
    istaxable      DOM_BOOLEAN    DEFAULT 0 NULL,
    CONSTRAINT PK__quote_it__A69DE2A84904BAEF PRIMARY KEY CLUSTERED (idquoteitem)
)
go



IF OBJECT_ID('quote_item') IS NOT NULL
    PRINT '<<< CREATED TABLE quote_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE quote_item >>>'
go

/* 
 * TABLE: quote_role 
 */

CREATE TABLE quote_role(
    idrole        DOM_UUID        NOT NULL,
    idquote       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__quote_ro__F324A06D06A658C4 PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('quote_role') IS NOT NULL
    PRINT '<<< CREATED TABLE quote_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE quote_role >>>'
go

/* 
 * TABLE: quote_status 
 */

CREATE TABLE quote_status(
    idstatus        DOM_UUID        NOT NULL,
    idquote         DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__quote_st__EBF77C2D3EE534B8 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('quote_status') IS NOT NULL
    PRINT '<<< CREATED TABLE quote_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE quote_status >>>'
go

/* 
 * TABLE: quote_type 
 */

CREATE TABLE quote_type(
    idquotyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK763 PRIMARY KEY CLUSTERED (idquotyp)
)
go



IF OBJECT_ID('quote_type') IS NOT NULL
    PRINT '<<< CREATED TABLE quote_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE quote_type >>>'
go

/* 
 * TABLE: reason_type 
 */

CREATE TABLE reason_type(
    idreason       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__reason_t__08353103839CACF5 PRIMARY KEY NONCLUSTERED (idreason)
)
go



IF OBJECT_ID('reason_type') IS NOT NULL
    PRINT '<<< CREATED TABLE reason_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE reason_type >>>'
go

/* 
 * TABLE: receipt 
 */

CREATE TABLE receipt(
    idpayment    DOM_UUID    NOT NULL,
    idreq        DOM_UUID    NULL,
    CONSTRAINT PK__receipt__64452FA46B0BF258 PRIMARY KEY NONCLUSTERED (idpayment)
)
go



IF OBJECT_ID('receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE receipt >>>'
go

/* 
 * TABLE: regular_sales_order 
 */

CREATE TABLE regular_sales_order(
    idorder    DOM_UUID    NOT NULL,
    CONSTRAINT PK__regular___44DCCDABC4D4FC6E PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('regular_sales_order') IS NOT NULL
    PRINT '<<< CREATED TABLE regular_sales_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE regular_sales_order >>>'
go

/* 
 * TABLE: reimbursement_part 
 */

CREATE TABLE reimbursement_part(
    idspgclaimaction    int               NULL,
    idproduct           DOM_ID            NULL,
    idspgclaimdetail    DOM_IDENTITY      NOT NULL,
    qty                 decimal(14, 2)    NULL,
    dtreimpart          datetime          NULL,
    CONSTRAINT PK__reimburs__E9C60035A399C5A6 PRIMARY KEY CLUSTERED (idspgclaimdetail)
)
go



IF OBJECT_ID('reimbursement_part') IS NOT NULL
    PRINT '<<< CREATED TABLE reimbursement_part >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE reimbursement_part >>>'
go

/* 
 * TABLE: relation_type 
 */

CREATE TABLE relation_type(
    idreltyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__relation__2E3112CE651E038F PRIMARY KEY NONCLUSTERED (idreltyp)
)
go



IF OBJECT_ID('relation_type') IS NOT NULL
    PRINT '<<< CREATED TABLE relation_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE relation_type >>>'
go

/* 
 * TABLE: religion_type 
 */

CREATE TABLE religion_type(
    idreligiontype    DOM_IDENTITY       IDENTITY(1,1),
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__religion__0BFF7E7A38EC7942 PRIMARY KEY NONCLUSTERED (idreligiontype)
)
go



IF OBJECT_ID('religion_type') IS NOT NULL
    PRINT '<<< CREATED TABLE religion_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE religion_type >>>'
go

/* 
 * TABLE: rem_part 
 */

CREATE TABLE rem_part(
    idproduct       DOM_ID     NOT NULL,
    minimumstock    DOM_QTY    NULL,
    qtysuggest      DOM_QTY    NULL,
    safetystock     DOM_QTY    NULL,
    CONSTRAINT PK__rem_part__8D50742532DC7CD8 PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('rem_part') IS NOT NULL
    PRINT '<<< CREATED TABLE rem_part >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE rem_part >>>'
go

/* 
 * TABLE: reminder_sent_status 
 */

CREATE TABLE reminder_sent_status(
    idsensta        DOM_UUID        NOT NULL,
    idreminder      DOM_IDENTITY    NULL,
    cellphone       DOM_ID          NULL,
    currentstate    DOM_INTEGER     NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__reminder__BB36EE28807AFD78 PRIMARY KEY CLUSTERED (idsensta)
)
go



IF OBJECT_ID('reminder_sent_status') IS NOT NULL
    PRINT '<<< CREATED TABLE reminder_sent_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE reminder_sent_status >>>'
go

/* 
 * TABLE: reorder_guideline 
 */

CREATE TABLE reorder_guideline(
    idreogui      DOM_IDENTITY    IDENTITY(1,1),
    idproduct     DOM_ID          NULL,
    idinternal    DOM_ID          NULL,
    qtyreorder    DOM_QTY         NULL,
    qtylevel      DOM_QTY         NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK766 PRIMARY KEY CLUSTERED (idreogui)
)
go



IF OBJECT_ID('reorder_guideline') IS NOT NULL
    PRINT '<<< CREATED TABLE reorder_guideline >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE reorder_guideline >>>'
go

/* 
 * TABLE: request 
 */

CREATE TABLE request(
    idrequest      DOM_UUID           NOT NULL,
    idreqtype      DOM_INTEGER        NULL,
    idinternal     DOM_ID             NULL,
    reqnum         DOM_ID             NULL,
    dtcreate       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtrequest      DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__request__00907462FA85D507 PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request') IS NOT NULL
    PRINT '<<< CREATED TABLE request >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request >>>'
go

/* 
 * TABLE: request_item 
 */

CREATE TABLE request_item(
    idreqitem      DOM_UUID           NOT NULL,
    idrequest      DOM_UUID           NULL,
    idproduct      DOM_ID             NULL,
    idfeature      DOM_INTEGER        NULL,
    seq            DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    qtyreq         DOM_QTY            NULL,
    qtytransfer    DOM_QTY            NULL,
    qtyorder       DOM_QTY            NULL,
    CONSTRAINT PK__request___D27E3AA6162B9F60 PRIMARY KEY CLUSTERED (idreqitem)
)
go



IF OBJECT_ID('request_item') IS NOT NULL
    PRINT '<<< CREATED TABLE request_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_item >>>'
go

/* 
 * TABLE: request_product 
 */

CREATE TABLE request_product(
    idrequest         DOM_UUID    NOT NULL,
    idfacilityfrom    DOM_UUID    NULL,
    idfacilityto      DOM_UUID    NULL,
    CONSTRAINT PK__request___0090746263D4DF7E PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request_product') IS NOT NULL
    PRINT '<<< CREATED TABLE request_product >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_product >>>'
go

/* 
 * TABLE: request_requirement 
 */

CREATE TABLE request_requirement(
    idrequest        DOM_UUID    NOT NULL,
    idrequirement    DOM_UUID    NULL,
    qty              DOM_QTY     NULL,
    CONSTRAINT PK__request___00907462330E04BE PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE request_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_requirement >>>'
go

/* 
 * TABLE: request_role 
 */

CREATE TABLE request_role(
    idrole        DOM_UUID        NOT NULL,
    idrequest     DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__request___F324A06D8E26B67E PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('request_role') IS NOT NULL
    PRINT '<<< CREATED TABLE request_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_role >>>'
go

/* 
 * TABLE: request_status 
 */

CREATE TABLE request_status(
    idstatus        DOM_UUID        NOT NULL,
    idrequest       DOM_UUID        NULL,
    idreason        DOM_INTEGER     NULL,
    idstatustype    DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__request___EBF77C2D41FEF3E7 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('request_status') IS NOT NULL
    PRINT '<<< CREATED TABLE request_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_status >>>'
go

/* 
 * TABLE: request_type 
 */

CREATE TABLE request_type(
    idreqtype      DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__request___FCFD1A95890CD1D2 PRIMARY KEY CLUSTERED (idreqtype)
)
go



IF OBJECT_ID('request_type') IS NOT NULL
    PRINT '<<< CREATED TABLE request_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_type >>>'
go

/* 
 * TABLE: request_unit_internal 
 */

CREATE TABLE request_unit_internal(
    idrequest       DOM_UUID    NOT NULL,
    idparent        DOM_UUID    NULL,
    idinternalto    DOM_ID      NULL,
    CONSTRAINT PK__request___00907462DF1F00FA PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request_unit_internal') IS NOT NULL
    PRINT '<<< CREATED TABLE request_unit_internal >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_unit_internal >>>'
go

/* 
 * TABLE: requirement 
 */

CREATE TABLE requirement(
    idreq                DOM_UUID           NOT NULL,
    idfacility           DOM_UUID           NULL,
    idparentreq          DOM_UUID           NULL,
    idreqtyp             DOM_IDENTITY       NULL,
    requirementnumber    DOM_ID             NULL,
    description          DOM_DESCRIPTION    NULL,
    dtcreate             DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtrequired           DOM_DATE           NULL,
    budget               DOM_MONEY          NULL,
    qty                  DOM_QTY            NULL,
    CONSTRAINT PK__requirem__24C5146C14F69206 PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement >>>'
go

/* 
 * TABLE: requirement_order_item 
 */

CREATE TABLE requirement_order_item(
    idreqordite    DOM_UUID    NOT NULL,
    idreq          DOM_UUID    NULL,
    idordite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    CONSTRAINT PK__requirem__734EDA4D1D87A2AC PRIMARY KEY CLUSTERED (idreqordite)
)
go



IF OBJECT_ID('requirement_order_item') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement_order_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement_order_item >>>'
go

/* 
 * TABLE: requirement_payment 
 */

CREATE TABLE requirement_payment(
    idreqpayite        DOM_UUID     NOT NULL,
    requirements_id    DOM_UUID     NULL,
    idpayment          DOM_UUID     NULL,
    amount             DOM_MONEY    NULL,
    CONSTRAINT PK__requirem__EDD56CDE495F4469 PRIMARY KEY NONCLUSTERED (idreqpayite)
)
go



IF OBJECT_ID('requirement_payment') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement_payment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement_payment >>>'
go

/* 
 * TABLE: requirement_role 
 */

CREATE TABLE requirement_role(
    idrole        DOM_UUID        NOT NULL,
    idreq         DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idparty       DOM_UUID        NOT NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__requirem__F324A06CEB45C5B6 PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('requirement_role') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement_role >>>'
go

/* 
 * TABLE: requirement_status 
 */

CREATE TABLE requirement_status(
    idstatus        DOM_UUID        NOT NULL,
    idreq           DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__requirem__EBF77C2C5E5ABB6F PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('requirement_status') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement_status >>>'
go

/* 
 * TABLE: requirement_type 
 */

CREATE TABLE requirement_type(
    idreqtyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__requirem__F115DCF6D3144913 PRIMARY KEY NONCLUSTERED (idreqtyp)
)
go



IF OBJECT_ID('requirement_type') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement_type >>>'
go

/* 
 * TABLE: return_purchase_order 
 */

CREATE TABLE return_purchase_order(
    idorder    DOM_UUID       NOT NULL,
    idissue    DOM_INTEGER    NULL,
    CONSTRAINT PK__return_p__44DCCDABC5B8400A PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('return_purchase_order') IS NOT NULL
    PRINT '<<< CREATED TABLE return_purchase_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE return_purchase_order >>>'
go

/* 
 * TABLE: return_sales_order 
 */

CREATE TABLE return_sales_order(
    idorder    DOM_UUID    NOT NULL,
    CONSTRAINT PK__return_s__44DCCDAB3500F0AC PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('return_sales_order') IS NOT NULL
    PRINT '<<< CREATED TABLE return_sales_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE return_sales_order >>>'
go

/* 
 * TABLE: role_type 
 */

CREATE TABLE role_type(
    idroletype     DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__role_typ__7D74A0A8A43A7A92 PRIMARY KEY NONCLUSTERED (idroletype)
)
go



IF OBJECT_ID('role_type') IS NOT NULL
    PRINT '<<< CREATED TABLE role_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE role_type >>>'
go

/* 
 * TABLE: room_tm 
 */

CREATE TABLE room_tm(
    idroom                int             IDENTITY(1,1),
    idfloor               int             NULL,
    roomname              nvarchar(50)    NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__room_tm__F32498567438727D PRIMARY KEY CLUSTERED (idroom)
)
go



IF OBJECT_ID('room_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE room_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE room_tm >>>'
go

/* 
 * TABLE: rule_hot_item 
 */

CREATE TABLE rule_hot_item(
    idrule            DOM_IDENTITY    NOT NULL,
    idproduct         DOM_ID          NULL,
    mindownpayment    DOM_MONEY       NULL,
    CONSTRAINT PK__rule_hot__FDA4C5FA6CB3021A PRIMARY KEY CLUSTERED (idrule)
)
go



IF OBJECT_ID('rule_hot_item') IS NOT NULL
    PRINT '<<< CREATED TABLE rule_hot_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE rule_hot_item >>>'
go

/* 
 * TABLE: rule_indent 
 */

CREATE TABLE rule_indent(
    idrule        DOM_IDENTITY    NOT NULL,
    idproduct     DOM_ID          NULL,
    minpayment    DOM_MONEY       NULL,
    CONSTRAINT PK__rule_ind__FDA4C5FA78A6DAB6 PRIMARY KEY CLUSTERED (idrule)
)
go



IF OBJECT_ID('rule_indent') IS NOT NULL
    PRINT '<<< CREATED TABLE rule_indent >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE rule_indent >>>'
go

/* 
 * TABLE: rule_sales_discount 
 */

CREATE TABLE rule_sales_discount(
    idrule       DOM_IDENTITY    NOT NULL,
    maxamount    DOM_MONEY       NULL,
    name         DOM_NAME        NULL,
    CONSTRAINT PK__rule_sal__FDA4C5FA301C2A96 PRIMARY KEY CLUSTERED (idrule)
)
go



IF OBJECT_ID('rule_sales_discount') IS NOT NULL
    PRINT '<<< CREATED TABLE rule_sales_discount >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE rule_sales_discount >>>'
go

/* 
 * TABLE: rule_type 
 */

CREATE TABLE rule_type(
    idrultyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__rule_typ__3468D7BB329A5A17 PRIMARY KEY NONCLUSTERED (idrultyp)
)
go



IF OBJECT_ID('rule_type') IS NOT NULL
    PRINT '<<< CREATED TABLE rule_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE rule_type >>>'
go

/* 
 * TABLE: rules 
 */

CREATE TABLE rules(
    idrule         DOM_IDENTITY       IDENTITY(1,1),
    idrultyp       DOM_IDENTITY       NULL,
    idinternal     DOM_ID             NULL,
    seqnum         DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    dtfrom         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__rules__FDA4C5FAD683D932 PRIMARY KEY CLUSTERED (idrule)
)
go



IF OBJECT_ID('rules') IS NOT NULL
    PRINT '<<< CREATED TABLE rules >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE rules >>>'
go

/* 
 * TABLE: sale_type 
 */

CREATE TABLE sale_type(
    idsaletype     DOM_INTEGER        NOT NULL,
    idparent       DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__sale_typ__F329BDDD099712C6 PRIMARY KEY CLUSTERED (idsaletype)
)
go



IF OBJECT_ID('sale_type') IS NOT NULL
    PRINT '<<< CREATED TABLE sale_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sale_type >>>'
go

/* 
 * TABLE: sales_agreement 
 */

CREATE TABLE sales_agreement(
    idagreement    DOM_UUID    NOT NULL,
    idinternal     DOM_ID      NULL,
    idcustomer     DOM_ID      NULL,
    CONSTRAINT PK__sales_ag__43F2607F96B73058 PRIMARY KEY CLUSTERED (idagreement)
)
go



IF OBJECT_ID('sales_agreement') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_agreement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_agreement >>>'
go

/* 
 * TABLE: sales_booking 
 */

CREATE TABLE sales_booking(
    idslsboo        DOM_UUID        NOT NULL,
    idinternal      DOM_ID          NULL,
    idreq           DOM_UUID        NULL,
    idproduct       DOM_ID          NULL,
    idfeature       DOM_INTEGER     NULL,
    note            DOM_LONGDESC    NULL,
    yearassembly    DOM_INTEGER     NULL,
    onhand          DOM_BOOLEAN     DEFAULT 0 NULL,
    intransit       DOM_BOOLEAN     DEFAULT 0 NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__sales_bo__65135F54210F6ED6 PRIMARY KEY CLUSTERED (idslsboo)
)
go



IF OBJECT_ID('sales_booking') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_booking >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_booking >>>'
go

/* 
 * TABLE: sales_broker 
 */

CREATE TABLE sales_broker(
    idparrol    DOM_UUID       NOT NULL,
    idbrotyp    DOM_INTEGER    NULL,
    idbroker    DOM_ID         NULL,
    CONSTRAINT PK__sales_br__86F77EC38EA5FF14 PRIMARY KEY NONCLUSTERED (idparrol)
)
go



IF OBJECT_ID('sales_broker') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_broker >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_broker >>>'
go

/* 
 * TABLE: sales_order 
 */

CREATE TABLE sales_order(
    idorder       DOM_UUID       NOT NULL,
    idsaletype    DOM_INTEGER    NULL,
    CONSTRAINT PK__sales_or__44DCCDABA59A352B PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('sales_order') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_order >>>'
go

/* 
 * TABLE: sales_point 
 */

CREATE TABLE sales_point(
    idinternal    DOM_ID    NOT NULL,
    CONSTRAINT PK__sales_po__2BD3089005A3DB4F PRIMARY KEY CLUSTERED (idinternal)
)
go



IF OBJECT_ID('sales_point') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_point >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_point >>>'
go

/* 
 * TABLE: sales_unit_leasing 
 */

CREATE TABLE sales_unit_leasing(
    idsallea        DOM_UUID        NOT NULL,
    idreq           DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idorder         DOM_UUID        NULL,
    idlsgpro        DOM_UUID        NULL,
    idleacom        DOM_UUID        NULL,
    ponumber        DOM_ID          NULL,
    dtpo            DOM_DATE        NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__sales_un__FD0B36E5EA1F818E PRIMARY KEY CLUSTERED (idsallea)
)
go



IF OBJECT_ID('sales_unit_leasing') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_unit_leasing >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_unit_leasing >>>'
go

/* 
 * TABLE: sales_unit_requirement 
 */

CREATE TABLE sales_unit_requirement(
    idreq                       DOM_UUID        NOT NULL,
    idprospect                  DOM_UUID        NULL,
    idsaletype                  DOM_INTEGER     NULL,
    idsalesman                  DOM_UUID        NULL,
    idproduct                   DOM_ID          NULL,
    idcolor                     DOM_INTEGER     NULL,
    idsalesbroker               DOM_UUID        NULL,
    idowner                     DOM_UUID        NULL,
    idlsgpro                    DOM_UUID        NULL,
    idinternal                  DOM_ID          NULL,
    idbillto                    DOM_ID          NULL,
    idcustomer                  DOM_ID          NULL,
    idrequest                   DOM_UUID        NULL,
    idorganizationowner         DOM_UUID        NULL,
    downpayment                 DOM_MONEY       NULL,
    idframe                     DOM_ID          NULL,
    idmachine                   DOM_ID          NULL,
    subsfincomp                 DOM_MONEY       NULL,
    subsmd                      DOM_MONEY       NULL,
    subsown                     DOM_MONEY       NULL,
    subsahm                     DOM_MONEY       NULL,
    bbnprice                    DOM_MONEY       NULL,
    hetprice                    DOM_MONEY       NULL,
    note                        DOM_LONGDESC    NULL,
    unitprice                   DOM_MONEY       NULL,
    notepartner                 DOM_LONGDESC    NULL,
    shipmentnote                DOM_LONGDESC    NULL,
    requestpoliceid             DOM_ID          NULL,
    leasingpo                   DOM_ID          NULL,
    dtpoleasing                 DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    brokerfee                   DOM_MONEY       NULL,
    idprosou                    DOM_INTEGER     NULL,
    prodyear                    DOM_INTEGER     NULL,
    requestidmachine            DOM_ID          NULL,
    requestidframe              DOM_ID          NULL,
    requestidmachineandframe    DOM_ID          NULL,
    minpayment                  DOM_MONEY       NULL,
    isunithotitem               DOM_BOOLEAN     DEFAULT 0 NULL,
    isunitindent                DOM_BOOLEAN     DEFAULT 0 NULL,
    waitstnk                    DOM_BOOLEAN     DEFAULT 0 NULL,
    creditinstallment           DOM_MONEY       NULL,
    creditdownpayment           DOM_MONEY       NULL,
    credittenor                 DOM_INTEGER     NULL,
    idleasing                   DOM_ID          NULL,
    podate                      DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    ponumber                    DOM_ID          NULL,
    CONSTRAINT PK__sales_un__24C5146DD932CC4D PRIMARY KEY CLUSTERED (idreq)
)
go



IF OBJECT_ID('sales_unit_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE sales_unit_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sales_unit_requirement >>>'
go

/* 
 * TABLE: salesman 
 */

CREATE TABLE salesman(
    idparrol         DOM_UUID       NOT NULL,
    idcoordinator    DOM_UUID       NULL,
    idsalesman       DOM_ID         NULL,
    coordinator      DOM_BOOLEAN    DEFAULT 0 NULL,
    idteamleader     DOM_UUID       NULL,
    teamleader       DOM_BOOLEAN    DEFAULT 0 NULL,
    ahmcode          DOM_ID         NULL,
    CONSTRAINT PK__salesman__86F77EC3E214B278 PRIMARY KEY NONCLUSTERED (idparrol)
)
go



IF OBJECT_ID('salesman') IS NOT NULL
    PRINT '<<< CREATED TABLE salesman >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE salesman >>>'
go

/* 
 * TABLE: service 
 */

CREATE TABLE service(
    idproduct            DOM_ID         NOT NULL,
    iduom                DOM_ID         NULL,
    frt                  DOM_INTEGER    NULL,
    idchargetopart       DOM_INTEGER    NULL,
    idchargetoservice    DOM_INTEGER    NULL,
    CONSTRAINT PK__service__8D507425B3D4C280 PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('service') IS NOT NULL
    PRINT '<<< CREATED TABLE service >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service >>>'
go

/* 
 * TABLE: service_agreement 
 */

CREATE TABLE service_agreement(
    idagreement    DOM_UUID    NOT NULL,
    idcustomer     DOM_ID      NULL,
    idinternal     DOM_ID      NULL,
    CONSTRAINT PK__service___43F2607EB174AA92 PRIMARY KEY NONCLUSTERED (idagreement)
)
go



IF OBJECT_ID('service_agreement') IS NOT NULL
    PRINT '<<< CREATED TABLE service_agreement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service_agreement >>>'
go

/* 
 * TABLE: service_history 
 */

CREATE TABLE service_history(
    idservicehistory           int             IDENTITY(1,1),
    idvehicleservicehistory    DOM_IDENTITY    NULL,
    codeservice                varchar(30)     NULL,
    descservice                varchar(30)     NULL,
    CONSTRAINT PK__service___66A540DF87EE1D15 PRIMARY KEY CLUSTERED (idservicehistory)
)
go



IF OBJECT_ID('service_history') IS NOT NULL
    PRINT '<<< CREATED TABLE service_history >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service_history >>>'
go

/* 
 * TABLE: service_kpb 
 */

CREATE TABLE service_kpb(
    idproduct            DOM_ID            NOT NULL,
    km                   decimal(12, 2)    NULL,
    expireddate          datetime          NULL,
    range_               decimal(12, 2)    NULL,
    daylimit             numeric(18, 0)    NULL,
    tolerancekm          decimal(12, 2)    NULL,
    tolerancedaylimit    numeric(18, 0)    NULL,
    CONSTRAINT PK__service___8D507425C348FD06 PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('service_kpb') IS NOT NULL
    PRINT '<<< CREATED TABLE service_kpb >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service_kpb >>>'
go

/* 
 * TABLE: service_per_motor 
 */

CREATE TABLE service_per_motor(
    idproduct    DOM_ID    NOT NULL,
    CONSTRAINT PK__service___8D50742588A3E96B PRIMARY KEY NONCLUSTERED (idproduct)
)
go



IF OBJECT_ID('service_per_motor') IS NOT NULL
    PRINT '<<< CREATED TABLE service_per_motor >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service_per_motor >>>'
go

/* 
 * TABLE: service_reminder 
 */

CREATE TABLE service_reminder(
    idreminder        DOM_IDENTITY    IDENTITY(1,1),
    idclaimtype       DOM_IDENTITY    NULL,
    idremindertype    DOM_IDENTITY    NULL,
    idvehide          DOM_UUID        NULL,
    days              DOM_INTEGER     NULL,
    CONSTRAINT PK__service___84BBA9C38D7528D0 PRIMARY KEY NONCLUSTERED (idreminder)
)
go



IF OBJECT_ID('service_reminder') IS NOT NULL
    PRINT '<<< CREATED TABLE service_reminder >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service_reminder >>>'
go

/* 
 * TABLE: service_reminder_event 
 */

CREATE TABLE service_reminder_event(
    idcomevt       DOM_UUID           NOT NULL,
    idreminder     DOM_IDENTITY       NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__service___EB114D63C74153AA PRIMARY KEY CLUSTERED (idcomevt)
)
go



IF OBJECT_ID('service_reminder_event') IS NOT NULL
    PRINT '<<< CREATED TABLE service_reminder_event >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE service_reminder_event >>>'
go

/* 
 * TABLE: ship_to 
 */

CREATE TABLE ship_to(
    idshipto      DOM_ID          NOT NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    CONSTRAINT PK__ship_to__E1F6A30ADA8EA346 PRIMARY KEY NONCLUSTERED (idshipto)
)
go



IF OBJECT_ID('ship_to') IS NOT NULL
    PRINT '<<< CREATED TABLE ship_to >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE ship_to >>>'
go

/* 
 * TABLE: shipment 
 */

CREATE TABLE shipment(
    idshipment        DOM_UUID           NOT NULL,
    idinternal        DOM_ID             NULL,
    idshito           DOM_ID             NULL,
    idshifro          DOM_ID             NULL,
    idshityp          DOM_IDENTITY       NULL,
    idfacility        DOM_UUID           NULL,
    idposaddfro       DOM_UUID           NULL,
    idposaddto        DOM_UUID           NULL,
    shipmentnumber    DOM_ID             NULL,
    description       DOM_DESCRIPTION    NULL,
    dtschedulle       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    reason            varchar(255)       NULL,
    CONSTRAINT PK__shipment__84926835185D943B PRIMARY KEY NONCLUSTERED (idshipment)
)
go



IF OBJECT_ID('shipment') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment >>>'
go

/* 
 * TABLE: shipment_billing_item 
 */

CREATE TABLE shipment_billing_item(
    idshibilite    DOM_UUID    NOT NULL,
    idshiite       DOM_UUID    NULL,
    idbilite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    CONSTRAINT PK__shipment__496A3E7FFDDB03D5 PRIMARY KEY CLUSTERED (idshibilite)
)
go



IF OBJECT_ID('shipment_billing_item') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_billing_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_billing_item >>>'
go

/* 
 * TABLE: shipment_incoming 
 */

CREATE TABLE shipment_incoming(
    idshipment    DOM_UUID    NOT NULL,
    CONSTRAINT PK__shipment__84926834D07A11A1 PRIMARY KEY CLUSTERED (idshipment)
)
go



IF OBJECT_ID('shipment_incoming') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_incoming >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_incoming >>>'
go

/* 
 * TABLE: shipment_item 
 */

CREATE TABLE shipment_item(
    idshiite              DOM_UUID           NOT NULL,
    idshipment            DOM_UUID           NULL,
    idfeature             DOM_INTEGER        NULL,
    idproduct             DOM_ID             NULL,
    itemdescription       DOM_DESCRIPTION    NULL,
    qty                   DOM_QTY            NULL,
    contentdescription    DOM_DESCRIPTION    NULL,
    idframe               DOM_DESCRIPTION    NULL,
    idmachine             DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__shipment__E234C4ED4FD1D985 PRIMARY KEY NONCLUSTERED (idshiite)
)
go



IF OBJECT_ID('shipment_item') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_item >>>'
go

/* 
 * TABLE: shipment_outgoing 
 */

CREATE TABLE shipment_outgoing(
    idshipment         DOM_UUID        NOT NULL,
    dtbast             DOM_DATE        NULL,
    note               DOM_LONGDESC    NULL,
    othername          DOM_NAME        NULL,
    deliveryopt        DOM_LONGDESC    NULL,
    deliveryaddress    DOM_LONGDESC    NULL,
    printcount         DOM_INTEGER     NULL,
    idparrol           DOM_UUID        NULL,
    CONSTRAINT PK__shipment__8492683431AFA53A PRIMARY KEY CLUSTERED (idshipment)
)
go



IF OBJECT_ID('shipment_outgoing') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_outgoing >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_outgoing >>>'
go

/* 
 * TABLE: shipment_package 
 */

CREATE TABLE shipment_package(
    idpackage      DOM_UUID        NOT NULL,
    idshipactyp    DOM_INTEGER     NULL,
    idinternal     DOM_ID          NULL,
    dtcreated      DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__shipment__C3EF12B79B4E6AAB PRIMARY KEY NONCLUSTERED (idpackage)
)
go



IF OBJECT_ID('shipment_package') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_package >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_package >>>'
go

/* 
 * TABLE: shipment_package_role 
 */

CREATE TABLE shipment_package_role(
    idrole        DOM_UUID        NOT NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    idpackage     DOM_UUID        NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__shipment__F324A06DA353D686 PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('shipment_package_role') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_package_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_package_role >>>'
go

/* 
 * TABLE: shipment_package_status 
 */

CREATE TABLE shipment_package_status(
    idstatus        DOM_UUID        NOT NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    idpackage       DOM_UUID        NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__shipment__EBF77C2DA6E2C9E1 PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('shipment_package_status') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_package_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_package_status >>>'
go

/* 
 * TABLE: shipment_package_type 
 */

CREATE TABLE shipment_package_type(
    idshipactyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__shipment__22F0310D4DF93EA4 PRIMARY KEY CLUSTERED (idshipactyp)
)
go



IF OBJECT_ID('shipment_package_type') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_package_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_package_type >>>'
go

/* 
 * TABLE: shipment_receipt 
 */

CREATE TABLE shipment_receipt(
    idreceipt          DOM_UUID           NOT NULL,
    idproduct          DOM_ID             NULL,
    idpackage          DOM_UUID           NULL,
    idfeature          DOM_INTEGER        NULL,
    idordite           DOM_UUID           NULL,
    idshiite           DOM_UUID           NULL,
    code               DOM_ID             NULL,
    qtyaccept          DOM_QTY            NULL,
    qtyreject          DOM_QTY            NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    dtreceipt          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__shipment__889F9A661FCD024E PRIMARY KEY NONCLUSTERED (idreceipt)
)
go



IF OBJECT_ID('shipment_receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_receipt >>>'
go

/* 
 * TABLE: shipment_receipt_role 
 */

CREATE TABLE shipment_receipt_role(
    idrole        DOM_UUID        NOT NULL,
    idreceipt     DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__shipment__F324A06D4918D960 PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('shipment_receipt_role') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_receipt_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_receipt_role >>>'
go

/* 
 * TABLE: shipment_receipt_status 
 */

CREATE TABLE shipment_receipt_status(
    idstatus        DOM_UUID        NOT NULL,
    idreceipt       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__shipment__EBF77C2DFF5134EF PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('shipment_receipt_status') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_receipt_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_receipt_status >>>'
go

/* 
 * TABLE: shipment_status 
 */

CREATE TABLE shipment_status(
    idstatus        DOM_UUID        NOT NULL,
    idshipment      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__shipment__EBF77C2CD0A36BFE PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('shipment_status') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_status >>>'
go

/* 
 * TABLE: shipment_type 
 */

CREATE TABLE shipment_type(
    idshityp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__shipment__E6FE7AF7F3D349AF PRIMARY KEY NONCLUSTERED (idshityp)
)
go



IF OBJECT_ID('shipment_type') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment_type >>>'
go

/* 
 * TABLE: sosmedtype 
 */

CREATE TABLE sosmedtype(
    idsosmedtype    int            NOT NULL,
    description     varchar(30)    NULL,
    CONSTRAINT PK__sosmedty__003F9F3BC0A90688 PRIMARY KEY CLUSTERED (idsosmedtype)
)
go



IF OBJECT_ID('sosmedtype') IS NOT NULL
    PRINT '<<< CREATED TABLE sosmedtype >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sosmedtype >>>'
go

/* 
 * TABLE: spg_claim 
 */

CREATE TABLE spg_claim(
    idspgclaim    int     IDENTITY(1,1),
    dtclaim       date    NULL,
    CONSTRAINT PK__spg_clai__CB828E8F2E7DB12B PRIMARY KEY CLUSTERED (idspgclaim)
)
go



IF OBJECT_ID('spg_claim') IS NOT NULL
    PRINT '<<< CREATED TABLE spg_claim >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE spg_claim >>>'
go

/* 
 * TABLE: spg_claim_action 
 */

CREATE TABLE spg_claim_action(
    idspgclaimaction    int            IDENTITY(1,1),
    claimto             varchar(90)    NULL,
    receivedby          varchar(90)    NULL,
    releasedby          varchar(90)    NULL,
    approvedby          varchar(90)    NULL,
    bastnumber          varchar(30)    NULL,
    CONSTRAINT PK__spg_clai__4654926C661B3E22 PRIMARY KEY CLUSTERED (idspgclaimaction)
)
go



IF OBJECT_ID('spg_claim_action') IS NOT NULL
    PRINT '<<< CREATED TABLE spg_claim_action >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE spg_claim_action >>>'
go

/* 
 * TABLE: spg_claim_detail 
 */

CREATE TABLE spg_claim_detail(
    idspgclaimdetail        DOM_IDENTITY      IDENTITY(1,1),
    idspgclaim              int               NULL,
    idproduct               DOM_ID            NULL,
    idreceipt               DOM_UUID          NULL,
    idspgclaimtype          int               NULL,
    idstatustype            DOM_INTEGER       NULL,
    idbilite                DOM_UUID          NULL,
    qty                     decimal(14, 2)    NULL,
    qtyreject               decimal(14, 2)    NULL,
    isnonactiveqty          bit               NULL,
    description             varchar(150)      NULL,
    isnonactiveqtyreject    bit               NULL,
    dtnonactivepart         datetime          NULL,
    CONSTRAINT PK__spg_clai__E9C6003500260879 PRIMARY KEY CLUSTERED (idspgclaimdetail)
)
go



IF OBJECT_ID('spg_claim_detail') IS NOT NULL
    PRINT '<<< CREATED TABLE spg_claim_detail >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE spg_claim_detail >>>'
go

/* 
 * TABLE: spg_claim_type 
 */

CREATE TABLE spg_claim_type(
    idspgclaimtype    int            IDENTITY(1,1),
    description       varchar(50)    NULL,
    CONSTRAINT PK__spg_clai__6946265DDC0A6022 PRIMARY KEY CLUSTERED (idspgclaimtype)
)
go



IF OBJECT_ID('spg_claim_type') IS NOT NULL
    PRINT '<<< CREATED TABLE spg_claim_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE spg_claim_type >>>'
go

/* 
 * TABLE: standard_calendar 
 */

CREATE TABLE standard_calendar(
    idcalendar    DOM_IDENTITY    NOT NULL,
    idinternal    DOM_ID          NULL,
    CONSTRAINT PK__standard__18BA6F8DEF966B02 PRIMARY KEY NONCLUSTERED (idcalendar)
)
go



IF OBJECT_ID('standard_calendar') IS NOT NULL
    PRINT '<<< CREATED TABLE standard_calendar >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE standard_calendar >>>'
go

/* 
 * TABLE: status_pkb 
 */

CREATE TABLE status_pkb(
    idstatuspkb           DOM_INTEGER    NOT NULL,
    idstatustype          DOM_INTEGER    NULL,
    descstatuspkb         varchar(30)    NULL,
    lastmodifieddate      date           NULL,
    lastmodifieduserid    int            NULL,
    statuscode            int            NULL,
    CONSTRAINT PK__status_p__2D8298C07F78BCA8 PRIMARY KEY NONCLUSTERED (idstatuspkb)
)
go



IF OBJECT_ID('status_pkb') IS NOT NULL
    PRINT '<<< CREATED TABLE status_pkb >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE status_pkb >>>'
go

/* 
 * TABLE: status_type 
 */

CREATE TABLE status_type(
    idstatustype    DOM_INTEGER        NOT NULL,
    description     DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__status_t__81D9B11A8B40F4E6 PRIMARY KEY NONCLUSTERED (idstatustype)
)
go



IF OBJECT_ID('status_type') IS NOT NULL
    PRINT '<<< CREATED TABLE status_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE status_type >>>'
go

/* 
 * TABLE: stock_opname 
 */

CREATE TABLE stock_opname(
    idstkop              DOM_UUID        NOT NULL,
    idstkopntyp          DOM_INTEGER     NULL,
    idinternal           DOM_ID          NULL,
    idfacility           DOM_UUID        NULL,
    idcalendar           DOM_IDENTITY    NULL,
    dtcreated            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    stockopnamenumber    DOM_ID          NULL,
    CONSTRAINT PK__stock_op__38E7CC5131D317C8 PRIMARY KEY CLUSTERED (idstkop)
)
go



IF OBJECT_ID('stock_opname') IS NOT NULL
    PRINT '<<< CREATED TABLE stock_opname >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stock_opname >>>'
go

/* 
 * TABLE: stock_opname_inventory 
 */

CREATE TABLE stock_opname_inventory(
    idstkopninv    DOM_UUID    NOT NULL,
    idinvite       DOM_UUID    NULL,
    idstopnite     DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    CONSTRAINT PK__stock_op__681AD6260A0E0E8F PRIMARY KEY CLUSTERED (idstkopninv)
)
go



IF OBJECT_ID('stock_opname_inventory') IS NOT NULL
    PRINT '<<< CREATED TABLE stock_opname_inventory >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stock_opname_inventory >>>'
go

/* 
 * TABLE: stock_opname_item 
 */

CREATE TABLE stock_opname_item(
    idstopnite         DOM_UUID           NOT NULL,
    idstkop            DOM_UUID           NULL,
    idproduct          DOM_ID             NULL,
    idcontainer        DOM_UUID           NULL,
    itemdescription    DOM_DESCRIPTION    NULL,
    qty                DOM_QTY            NULL,
    qtycount           DOM_QTY            NULL,
    tagnumber          DOM_ID             NULL,
    checked            DOM_BOOLEAN        DEFAULT 0 NULL,
    het                DOM_MONEY          NULL,
    CONSTRAINT PK__stock_op__5599D222A73D8891 PRIMARY KEY CLUSTERED (idstopnite)
)
go



IF OBJECT_ID('stock_opname_item') IS NOT NULL
    PRINT '<<< CREATED TABLE stock_opname_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stock_opname_item >>>'
go

/* 
 * TABLE: stock_opname_status 
 */

CREATE TABLE stock_opname_status(
    idstatus        DOM_UUID        NOT NULL,
    idstkop         DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__stock_op__EBF77C2D156C52BF PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('stock_opname_status') IS NOT NULL
    PRINT '<<< CREATED TABLE stock_opname_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stock_opname_status >>>'
go

/* 
 * TABLE: stock_opname_type 
 */

CREATE TABLE stock_opname_type(
    idstkopntyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__stock_op__677EFA7FFF767DCE PRIMARY KEY CLUSTERED (idstkopntyp)
)
go



IF OBJECT_ID('stock_opname_type') IS NOT NULL
    PRINT '<<< CREATED TABLE stock_opname_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stock_opname_type >>>'
go

/* 
 * TABLE: stockopname_item_tm 
 */

CREATE TABLE stockopname_item_tm(
    idstockopnameitem     int             IDENTITY(1,1),
    idstockopname         int             NULL,
    idfixedasset          DOM_IDENTITY    NULL,
    idroom                int             NULL,
    idfloor               int             NULL,
    quantitysystem        int             NULL,
    quantityfound         int             NULL,
    bitfound              bit             NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    CONSTRAINT PK__stockopn__2766FC0C97230EC2 PRIMARY KEY CLUSTERED (idstockopnameitem)
)
go



IF OBJECT_ID('stockopname_item_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE stockopname_item_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stockopname_item_tm >>>'
go

/* 
 * TABLE: stockopname_status_tp 
 */

CREATE TABLE stockopname_status_tp(
    idstockopnamestatus    int             NOT NULL,
    description            nvarchar(10)    NULL,
    lastmodifieddate       datetime        NULL,
    lastmodifieduserid     int             NULL,
    CONSTRAINT PK__stockopn__E6B6560CE598D0A9 PRIMARY KEY CLUSTERED (idstockopnamestatus)
)
go



IF OBJECT_ID('stockopname_status_tp') IS NOT NULL
    PRINT '<<< CREATED TABLE stockopname_status_tp >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stockopname_status_tp >>>'
go

/* 
 * TABLE: stockopname_tm 
 */

CREATE TABLE stockopname_tm(
    idstockopname         int             IDENTITY(1,1),
    stockopnamenumber     nvarchar(20)    NULL,
    stockopnamedate       datetime        NULL,
    status                int             NULL,
    lastmodifieddate      datetime        NULL,
    lastmodifieduserid    int             NULL,
    CONSTRAINT PK__stockopn__E7424C610576E52C PRIMARY KEY CLUSTERED (idstockopname)
)
go



IF OBJECT_ID('stockopname_tm') IS NOT NULL
    PRINT '<<< CREATED TABLE stockopname_tm >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stockopname_tm >>>'
go

/* 
 * TABLE: suggest_part 
 */

CREATE TABLE suggest_part(
    idsuggestpart     int               IDENTITY(1,1),
    idproductmotor    DOM_ID            NULL,
    idproductpart     DOM_ID            NULL,
    km                decimal(12, 2)    NULL,
    ismultiply        bit               NULL,
    range_            decimal(12, 2)    NULL,
    CONSTRAINT PK__suggest___D180CB1EAEB068FC PRIMARY KEY CLUSTERED (idsuggestpart)
)
go



IF OBJECT_ID('suggest_part') IS NOT NULL
    PRINT '<<< CREATED TABLE suggest_part >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE suggest_part >>>'
go

/* 
 * TABLE: summary 
 */

CREATE TABLE summary(
    idsummary             int               IDENTITY(1,1),
    idpkb                 DOM_IDENTITY      NULL,
    totalunit             int               NULL,
    serviceqty            int               NULL,
    njbmechanic           decimal(18, 2)    NULL,
    nscmechanic           decimal(18, 2)    NULL,
    totalnjbmec           decimal(18, 2)    NULL,
    totalnscmec           decimal(18, 2)    NULL,
    lastmodifieddate      date              NULL,
    lastmodifieduserid    int               NULL,
    statuscode            int               NULL,
    CONSTRAINT PK__summary__81CF623EE865CC91 PRIMARY KEY NONCLUSTERED (idsummary)
)
go



IF OBJECT_ID('summary') IS NOT NULL
    PRINT '<<< CREATED TABLE summary >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE summary >>>'
go

/* 
 * TABLE: suspect 
 */

CREATE TABLE suspect(
    idsuspect             DOM_UUID        NOT NULL,
    suspectnumber         DOM_ID          NULL,
    idparty               DOM_UUID        NULL,
    idsalescoordinator    DOM_UUID        NULL,
    idsalesman            DOM_UUID        NULL,
    idaddress             DOM_UUID        NULL,
    idsuspecttyp          DOM_INTEGER     NULL,
    iddealer              DOM_ID          NULL,
    salesdt               DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    marketname            DOM_NAME        NULL,
    repurchaseprob        DOM_PERCENT     DEFAULT 0 NULL,
    priority              DOM_PERCENT     DEFAULT 0 NULL,
    idsaletype            DOM_INTEGER     NULL,
    idlsgpro              DOM_UUID        NULL,
    CONSTRAINT PK__suspect__2F025AB308E408E2 PRIMARY KEY NONCLUSTERED (idsuspect)
)
go



IF OBJECT_ID('suspect') IS NOT NULL
    PRINT '<<< CREATED TABLE suspect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE suspect >>>'
go

/* 
 * TABLE: suspect_role 
 */

CREATE TABLE suspect_role(
    idrole        DOM_UUID        NOT NULL,
    idsuspect     DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__suspect___F324A06C176DE973 PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('suspect_role') IS NOT NULL
    PRINT '<<< CREATED TABLE suspect_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE suspect_role >>>'
go

/* 
 * TABLE: suspect_status 
 */

CREATE TABLE suspect_status(
    idstatus        DOM_UUID        NOT NULL,
    idsuspect       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__suspect___EBF77C2C91600466 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('suspect_status') IS NOT NULL
    PRINT '<<< CREATED TABLE suspect_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE suspect_status >>>'
go

/* 
 * TABLE: suspect_type 
 */

CREATE TABLE suspect_type(
    idsuspecttyp    DOM_INTEGER        NOT NULL,
    description     DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__suspect___1888E40203AC738B PRIMARY KEY CLUSTERED (idsuspecttyp)
)
go



IF OBJECT_ID('suspect_type') IS NOT NULL
    PRINT '<<< CREATED TABLE suspect_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE suspect_type >>>'
go

/* 
 * TABLE: sympthom_data_m 
 */

CREATE TABLE sympthom_data_m(
    idsympthomdatam       DOM_IDENTITY    IDENTITY(1,1),
    descsymptom           varchar(150)    NULL,
    lastmodifieddate      date            NULL,
    lastmodifieduserid    int             NULL,
    statuscode            int             NULL,
    CONSTRAINT PK__sympthom__7CB6CDD16849ED1D PRIMARY KEY NONCLUSTERED (idsympthomdatam)
)
go



IF OBJECT_ID('sympthom_data_m') IS NOT NULL
    PRINT '<<< CREATED TABLE sympthom_data_m >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sympthom_data_m >>>'
go

/* 
 * TABLE: sysdiagrams 
 */

CREATE TABLE sysdiagrams(
    diagram_id      int               IDENTITY(1,1),
    name            DOM_NAME          NULL,
    principal_id    int               NOT NULL,
    version         int               NULL,
    definition      varbinary(max)    NULL,
    CONSTRAINT PK__sysdiagr__C2B05B6130853138 PRIMARY KEY CLUSTERED (diagram_id),
    UNIQUE (principal_id, name)
)
go



IF OBJECT_ID('sysdiagrams') IS NOT NULL
    PRINT '<<< CREATED TABLE sysdiagrams >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE sysdiagrams >>>'
go

/* 
 * TABLE: telecomunication_number 
 */

CREATE TABLE telecomunication_number(
    idcontact    DOM_UUID    NOT NULL,
    number       DOM_ID      NULL,
    CONSTRAINT PK__telecomu__A36DC107E712831A PRIMARY KEY NONCLUSTERED (idcontact)
)
go



IF OBJECT_ID('telecomunication_number') IS NOT NULL
    PRINT '<<< CREATED TABLE telecomunication_number >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE telecomunication_number >>>'
go

/* 
 * TABLE: type_pkb 
 */

CREATE TABLE type_pkb(
    idtypepkb    DOM_INTEGER    NOT NULL,
    desctype     varchar(30)    NULL,
    CONSTRAINT PK__type_pkb__1BF1ED17C844C073 PRIMARY KEY CLUSTERED (idtypepkb)
)
go



IF OBJECT_ID('type_pkb') IS NOT NULL
    PRINT '<<< CREATED TABLE type_pkb >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE type_pkb >>>'
go

/* 
 * TABLE: unit_accesories_mapper 
 */

CREATE TABLE unit_accesories_mapper(
    iduntaccmap    DOM_UUID        NOT NULL,
    idmotor        DOM_ID          NULL,
    acc1           DOM_ID          NULL,
    qtyacc1        DOM_QTY         NULL,
    acc2           DOM_ID          NULL,
    qtyacc2        DOM_QTY         NULL,
    acc3           DOM_ID          NULL,
    qtyacc3        DOM_QTY         NULL,
    acc4           DOM_ID          NULL,
    qtyacc4        DOM_QTY         NULL,
    acc5           DOM_ID          NULL,
    qtyacc5        DOM_QTY         NULL,
    acc6           DOM_ID          NULL,
    qtyacc6        DOM_QTY         NULL,
    acc7           DOM_ID          NULL,
    qtyacc7        DOM_QTY         NULL,
    promat1        DOM_ID          NULL,
    qtypromat1     DOM_QTY         NULL,
    promat2        DOM_ID          NULL,
    qtypromat2     DOM_QTY         NULL,
    dtfrom         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru         DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__unit_acc__4B9284ACC9EF4A73 PRIMARY KEY CLUSTERED (iduntaccmap)
)
go



IF OBJECT_ID('unit_accesories_mapper') IS NOT NULL
    PRINT '<<< CREATED TABLE unit_accesories_mapper >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE unit_accesories_mapper >>>'
go

/* 
 * TABLE: unit_deliverable 
 */

CREATE TABLE unit_deliverable(
    iddeliverable     DOM_UUID           NOT NULL,
    idreq             DOM_UUID           NULL,
    iddeltype         DOM_INTEGER        NULL,
    idvndstatyp       DOM_INTEGER        NULL,
    idconstatyp       DOM_INTEGER        NULL,
    description       DOM_DESCRIPTION    NULL,
    dtreceipt         DOM_DATE           NULL,
    dtdelivery        DOM_DATE           NULL,
    bastnumber        DOM_ID             NULL,
    name              DOM_NAME           NULL,
    identitynumber    DOM_ID             NULL,
    cellphone         DOM_ID             NULL,
    refnumber         DOM_DESCRIPTION    NULL,
    refdt             DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    receiptqty        DOM_INTEGER        NULL,
    receiptnominal    DOM_MONEY          NULL,
    bpkbnumber        DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__unit_del__FBC359D7FE60D137 PRIMARY KEY CLUSTERED (iddeliverable)
)
go



IF OBJECT_ID('unit_deliverable') IS NOT NULL
    PRINT '<<< CREATED TABLE unit_deliverable >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE unit_deliverable >>>'
go

/* 
 * TABLE: unit_document_message 
 */

CREATE TABLE unit_document_message(
    idmessage       DOM_IDENTITY    IDENTITY(1,1),
    idparent        DOM_IDENTITY    NULL,
    idreq           DOM_UUID        NULL,
    idprospect      DOM_UUID        NULL,
    content         DOM_LONGDESC    NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    usernamefrom    DOM_USER        NULL,
    usernameto      DOM_USER        NULL,
    CONSTRAINT PK__unit_doc__A10EF59EE64B3925 PRIMARY KEY CLUSTERED (idmessage)
)
go



IF OBJECT_ID('unit_document_message') IS NOT NULL
    PRINT '<<< CREATED TABLE unit_document_message >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE unit_document_message >>>'
go

/* 
 * TABLE: unit_preparation 
 */

CREATE TABLE unit_preparation(
    idslip         DOM_UUID    NOT NULL,
    idinternal     DOM_ID      NULL,
    idfacility     DOM_UUID    NULL,
    usermekanik    DOM_USER    NULL,
    CONSTRAINT PK__unit_pre__D730145C6FDB5008 PRIMARY KEY CLUSTERED (idslip)
)
go



IF OBJECT_ID('unit_preparation') IS NOT NULL
    PRINT '<<< CREATED TABLE unit_preparation >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE unit_preparation >>>'
go

/* 
 * TABLE: unit_requirement 
 */

CREATE TABLE unit_requirement(
    idreq        DOM_UUID    NOT NULL,
    idproduct    DOM_ID      NULL,
    idbranch     DOM_ID      NULL,
    CONSTRAINT PK__unit_req__24C5146C89014EDA PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('unit_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE unit_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE unit_requirement >>>'
go

/* 
 * TABLE: unit_shipment_receipt 
 */

CREATE TABLE unit_shipment_receipt(
    idreceipt       DOM_UUID       NOT NULL,
    idframe         DOM_ID         NULL,
    idmachine       DOM_ID         NULL,
    yearassembly    DOM_INTEGER    NULL,
    unitprice       DOM_MONEY      NULL,
    acc1            DOM_BOOLEAN    DEFAULT 0 NULL,
    acc2            DOM_BOOLEAN    DEFAULT 0 NULL,
    acc3            DOM_BOOLEAN    DEFAULT 0 NULL,
    acc4            DOM_BOOLEAN    DEFAULT 0 NULL,
    acc5            DOM_BOOLEAN    DEFAULT 0 NULL,
    acc6            DOM_BOOLEAN    DEFAULT 0 NULL,
    acc7            DOM_BOOLEAN    DEFAULT 0 NULL,
    isreceipt       DOM_BOOLEAN    DEFAULT 0 NULL,
    CONSTRAINT PK__unit_shi__889F9A67A584E98B PRIMARY KEY CLUSTERED (idreceipt)
)
go



IF OBJECT_ID('unit_shipment_receipt') IS NOT NULL
    PRINT '<<< CREATED TABLE unit_shipment_receipt >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE unit_shipment_receipt >>>'
go

/* 
 * TABLE: uom 
 */

CREATE TABLE uom(
    iduom           DOM_ID             NOT NULL,
    iduomtyp        DOM_IDENTITY       NULL,
    description     DOM_DESCRIPTION    NULL,
    abbreviation    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__uom__2503326D196DE533 PRIMARY KEY NONCLUSTERED (iduom)
)
go



IF OBJECT_ID('uom') IS NOT NULL
    PRINT '<<< CREATED TABLE uom >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE uom >>>'
go

/* 
 * TABLE: uom_conversion 
 */

CREATE TABLE uom_conversion(
    iduomconversion    DOM_IDENTITY    IDENTITY(1,1),
    iduomto            DOM_ID          NULL,
    iduomfro           DOM_ID          NULL,
    factor             DOM_QTY         NULL,
    CONSTRAINT PK__uom_conv__153E651333AA8B6B PRIMARY KEY NONCLUSTERED (iduomconversion)
)
go



IF OBJECT_ID('uom_conversion') IS NOT NULL
    PRINT '<<< CREATED TABLE uom_conversion >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE uom_conversion >>>'
go

/* 
 * TABLE: uom_type 
 */

CREATE TABLE uom_type(
    iduomtyp       DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__uom_type__99D182F1DDDECAA1 PRIMARY KEY NONCLUSTERED (iduomtyp)
)
go



IF OBJECT_ID('uom_type') IS NOT NULL
    PRINT '<<< CREATED TABLE uom_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE uom_type >>>'
go

/* 
 * TABLE: user_mediator 
 */

CREATE TABLE user_mediator(
    idusrmed      DOM_UUID           NOT NULL,
    idperson      DOM_UUID           NULL,
    idinternal    DOM_ID             NULL,
    id            bigint             NULL,
    email         DOM_DESCRIPTION    NULL,
    username      DOM_NAME           NULL,
    firstname     DOM_NAME           NULL,
    lastname      DOM_NAME           NULL,
    CONSTRAINT PK__user_med__A0765872C5023A92 PRIMARY KEY CLUSTERED (idusrmed)
)
go



IF OBJECT_ID('user_mediator') IS NOT NULL
    PRINT '<<< CREATED TABLE user_mediator >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE user_mediator >>>'
go

/* 
 * TABLE: user_mediator_role 
 */

CREATE TABLE user_mediator_role(
    idrole        DOM_UUID        NOT NULL,
    idusrmed      DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    username      DOM_USER        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__user_med__F324A06D865722A5 PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('user_mediator_role') IS NOT NULL
    PRINT '<<< CREATED TABLE user_mediator_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE user_mediator_role >>>'
go

/* 
 * TABLE: user_mediator_status 
 */

CREATE TABLE user_mediator_status(
    idstatus        DOM_UUID        NOT NULL,
    idusrmed        DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__user_med__EBF77C2DCD17C79F PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('user_mediator_status') IS NOT NULL
    PRINT '<<< CREATED TABLE user_mediator_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE user_mediator_status >>>'
go

/* 
 * TABLE: vehicle 
 */

CREATE TABLE vehicle(
    idvehicle            DOM_UUID       NOT NULL,
    idproduct            DOM_ID         NULL,
    idcolor              DOM_INTEGER    NULL,
    idinternal           DOM_ID         NULL,
    idmachine            DOM_ID         NULL,
    idframe              DOM_ID         NULL,
    servicebooknumber    DOM_ID         NULL,
    yearofass            DOM_INTEGER    NULL,
    fuel                 DOM_ID         NULL,
    merkoil              DOM_ID         NULL,
    CONSTRAINT PK__vehicle__D4F33B58FCC49C49 PRIMARY KEY NONCLUSTERED (idvehicle)
)
go



IF OBJECT_ID('vehicle') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle >>>'
go

/* 
 * TABLE: vehicle_carrier 
 */

CREATE TABLE vehicle_carrier(
    idvehide    DOM_UUID        NOT NULL,
    idparty     DOM_UUID        NULL,
    idreason    DOM_INTEGER     NULL,
    idreltyp    DOM_IDENTITY    NULL,
    CONSTRAINT PK__vehicle___C7BC32F4A9D3A4DB PRIMARY KEY CLUSTERED (idvehide)
)
go



IF OBJECT_ID('vehicle_carrier') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_carrier >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_carrier >>>'
go

/* 
 * TABLE: vehicle_customer_request 
 */

CREATE TABLE vehicle_customer_request(
    idrequest        DOM_UUID    NOT NULL,
    idcustomer       DOM_ID      NULL,
    idsalesbroker    DOM_UUID    NULL,
    idsalesman       DOM_UUID    NULL,
    customerorder    DOM_ID      NULL,
    CONSTRAINT PK__vehicle___009074627BE47252 PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('vehicle_customer_request') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_customer_request >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_customer_request >>>'
go

/* 
 * TABLE: vehicle_document_requirement 
 */

CREATE TABLE vehicle_document_requirement(
    idreq                  DOM_UUID           NOT NULL,
    idslsreq               DOM_UUID           NULL,
    idordite               DOM_UUID           NULL,
    idorganizationowner    DOM_UUID           NULL,
    idpersonowner          DOM_UUID           NULL,
    idvehregtyp            DOM_INTEGER        NULL,
    idsaletype             DOM_INTEGER        NULL,
    idvehicle              DOM_UUID           NULL,
    idvendor               DOM_ID             NULL,
    idinternal             DOM_ID             NULL,
    idbillto               DOM_ID             NULL,
    idshipto               DOM_ID             NULL,
    atpmfaktur             DOM_ID             NULL,
    note                   DOM_LONGDESC       NULL,
    bbn                    DOM_MONEY          NULL,
    othercost              DOM_MONEY          NULL,
    policenumber           DOM_ID             NULL,
    atpmfakturdt           DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    submissionno           DOM_DESCRIPTION    NULL,
    submissiondt           DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    crossarea              DOM_INTEGER        NULL,
    dtfillname             DOM_DATE           NULL,
    registrationnumber     DOM_ID             NULL,
    statusfaktur           DOM_ID             NULL,
    waitstnk               DOM_BOOLEAN        DEFAULT 0 NULL,
    reffnumber             DOM_ID             NULL,
    dtreff                 DOM_DATE           NULL,
    fname                  DOM_NAME           NULL,
    lname                  DOM_NAME           NULL,
    cellphone              DOM_ID             NULL,
    facilityname           DOM_NAME           NULL,
    internname             DOM_NAME           NULL,
    costhandling           DOM_MONEY          NULL,
    CONSTRAINT PK__vehicle___24C5146D8ADB047C PRIMARY KEY CLUSTERED (idreq)
)
go



IF OBJECT_ID('vehicle_document_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_document_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_document_requirement >>>'
go

/* 
 * TABLE: vehicle_identification 
 */

CREATE TABLE vehicle_identification(
    idvehide          DOM_UUID           NOT NULL,
    idvehicle         DOM_UUID           NULL,
    idcustomer        DOM_ID             NULL,
    vehiclenumber     DOM_SHORTID        NULL,
    dealerpurchase    DOM_DESCRIPTION    NULL,
    dtpurchase        DOM_DATE           NULL,
    dtfrom            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru            DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__vehicle___C7BC32F56D17A7AC PRIMARY KEY NONCLUSTERED (idvehide)
)
go



IF OBJECT_ID('vehicle_identification') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_identification >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_identification >>>'
go

/* 
 * TABLE: vehicle_purchase_order 
 */

CREATE TABLE vehicle_purchase_order(
    idorder    DOM_UUID    NOT NULL,
    CONSTRAINT PK__vehicle___44DCCDAB3A165F59 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('vehicle_purchase_order') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_purchase_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_purchase_order >>>'
go

/* 
 * TABLE: vehicle_registration_type 
 */

CREATE TABLE vehicle_registration_type(
    idvehregtyp    DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__vehicle___7736B941B35FC00F PRIMARY KEY CLUSTERED (idvehregtyp)
)
go



IF OBJECT_ID('vehicle_registration_type') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_registration_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_registration_type >>>'
go

/* 
 * TABLE: vehicle_sales_billing 
 */

CREATE TABLE vehicle_sales_billing(
    idbilling        DOM_UUID       NOT NULL,
    idsaletype       DOM_INTEGER    NULL,
    idcustomer       DOM_ID         NULL,
    axunitprice      DOM_MONEY      NULL,
    axbbn            DOM_MONEY      NULL,
    axsalesamount    DOM_MONEY      NULL,
    axdisc           DOM_MONEY      NULL,
    axprice          DOM_MONEY      NULL,
    axppn            DOM_MONEY      NULL,
    axaramt          DOM_MONEY      NULL,
    axadd1           DOM_MONEY      NULL,
    axadd2           DOM_MONEY      NULL,
    axadd3           DOM_MONEY      NULL,
    CONSTRAINT PK__vehicle___7496EE86BCC281B2 PRIMARY KEY CLUSTERED (idbilling)
)
go



IF OBJECT_ID('vehicle_sales_billing') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_sales_billing >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_sales_billing >>>'
go

/* 
 * TABLE: vehicle_sales_order 
 */

CREATE TABLE vehicle_sales_order(
    idorder                DOM_UUID           NOT NULL,
    idunitreq              DOM_UUID           NULL,
    idsaletype             DOM_INTEGER        NULL,
    idsalesman             DOM_UUID           NULL,
    idsales                DOM_UUID           NULL,
    idbirojasa             DOM_ID             NULL,
    issubsidifincoasdp     DOM_BOOLEAN        DEFAULT 0 NULL,
    shiptoowner            DOM_BOOLEAN        DEFAULT 0 NULL,
    dtbastunit             DOM_DATE           NULL,
    dtcustreceipt          DOM_DATE           NULL,
    dtmachineswipe         DOM_DATE           NULL,
    dttakepicture          DOM_DATE           NULL,
    dtcovernote            DOM_DATE           NULL,
    idlsgpaystatus         DOM_INTEGER        NULL,
    dtadmslsverify         DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtafcverify            DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    admslsverifyval        DOM_INTEGER        NULL,
    afcverifyval           DOM_INTEGER        NULL,
    admslsnotapprovnote    DOM_LONGDESC       NULL,
    afcnotapprovnote       DOM_LONGDESC       NULL,
    vrnama1                DOM_LONGDESC       NULL,
    vrnama2                DOM_LONGDESC       NULL,
    vrnamamarket           DOM_LONGDESC       NULL,
    vrwarna                DOM_LONGDESC       NULL,
    vrtahunproduksi        DOM_LONG           NULL,
    vrangsuran             DOM_MONEY          NULL,
    vrtenor                DOM_INTEGER        NULL,
    vrleasing              DOM_LONGDESC       NULL,
    vrtglpengiriman        DOM_ID             NULL,
    vrjampengiriman        DOM_DESCRIPTION    NULL,
    vrtipepenjualan        DOM_DESCRIPTION    NULL,
    vrdpmurni              DOM_MONEY          NULL,
    vrtandajadi            DOM_MONEY          NULL,
    vrtglpembayaran        DOM_ID             NULL,
    vrsisa                 DOM_MONEY          NULL,
    vriscod                DOM_BOOLEAN        DEFAULT 0 NULL,
    vrcatatantambahan      DOM_LONGDESC       NULL,
    vrisverified           DOM_BOOLEAN        DEFAULT 0 NULL,
    vrcatatan              DOM_LONGDESC       NULL,
    kacabnote              DOM_LONGDESC       NULL,
    isreqtaxinv            DOM_BOOLEAN        DEFAULT 0 NULL,
    isdonematching         DOM_BOOLEAN        DEFAULT 0 NULL,
    vridsaletype           DOM_INTEGER        NULL,
    CONSTRAINT PK__vehicle___44DCCDABD06C7DD9 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('vehicle_sales_order') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_sales_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_sales_order >>>'
go

/* 
 * TABLE: vehicle_service_history 
 */

CREATE TABLE vehicle_service_history(
    idvehicleservicehistory    DOM_IDENTITY      IDENTITY(1,1),
    idvehicle                  DOM_UUID          NULL,
    location                   varchar(100)      NULL,
    km                         decimal(10, 2)    NULL,
    jobsuggest                 varchar(100)      NULL,
    servicedate                datetime          NULL,
    mechanicname               varchar(30)       NULL,
    nextservicedate            date              NULL,
    vehiclenumber              DOM_ID            NULL,
    idframe                    DOM_ID            NULL,
    idmachine                  DOM_ID            NULL,
    CONSTRAINT PK__vehicle___BC262B6C448D302F PRIMARY KEY CLUSTERED (idvehicleservicehistory)
)
go



IF OBJECT_ID('vehicle_service_history') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_service_history >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_service_history >>>'
go

/* 
 * TABLE: vehicle_work_requirement 
 */

CREATE TABLE vehicle_work_requirement(
    idreq            DOM_UUID    NOT NULL,
    idmechanic       DOM_UUID    NULL,
    idvehicle        DOM_UUID    NULL,
    idvehide         DOM_UUID    NULL,
    idcustomer       DOM_ID      NULL,
    vehiclenumber    DOM_ID      NULL,
    CONSTRAINT PK__vehicle___24C5146CC1855E38 PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('vehicle_work_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_work_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_work_requirement >>>'
go

/* 
 * TABLE: vendor 
 */

CREATE TABLE vendor(
    idvendor        DOM_ID          NOT NULL,
    idvndtyp        DOM_INTEGER     NULL,
    idroletype      DOM_IDENTITY    NULL,
    idparty         DOM_UUID        NULL,
    isbirojasa      DOM_BOOLEAN     DEFAULT 0 NULL,
    ismaindealer    DOM_BOOLEAN     DEFAULT 0 NULL,
    CONSTRAINT PK__vendor__AE65260A82DE06C1 PRIMARY KEY NONCLUSTERED (idvendor)
)
go



IF OBJECT_ID('vendor') IS NOT NULL
    PRINT '<<< CREATED TABLE vendor >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vendor >>>'
go

/* 
 * TABLE: vendor_order 
 */

CREATE TABLE vendor_order(
    idorder       DOM_UUID    NOT NULL,
    idinternal    DOM_ID      NULL,
    idvendor      DOM_ID      NULL,
    idbillto      DOM_ID      NULL,
    CONSTRAINT PK__vendor_o__44DCCDAB1130D1A7 PRIMARY KEY NONCLUSTERED (idorder)
)
go



IF OBJECT_ID('vendor_order') IS NOT NULL
    PRINT '<<< CREATED TABLE vendor_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vendor_order >>>'
go

/* 
 * TABLE: vendor_product 
 */

CREATE TABLE vendor_product(
    idvenpro      DOM_UUID        NOT NULL,
    idproduct     DOM_ID          NULL,
    idvendor      DOM_ID          NULL,
    idinternal    DOM_ID          NULL,
    orderratio    DOM_INTEGER     NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__vendor_p__5551E10E6CB69D4F PRIMARY KEY NONCLUSTERED (idvenpro)
)
go



IF OBJECT_ID('vendor_product') IS NOT NULL
    PRINT '<<< CREATED TABLE vendor_product >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vendor_product >>>'
go

/* 
 * TABLE: vendor_quotation 
 */

CREATE TABLE vendor_quotation(
    idquote        DOM_UUID    NOT NULL,
    idinternal     DOM_ID      NULL,
    idvendor       DOM_ID      NULL,
    vendorquote    DOM_ID      NULL,
    CONSTRAINT PK762 PRIMARY KEY CLUSTERED (idquote)
)
go



IF OBJECT_ID('vendor_quotation') IS NOT NULL
    PRINT '<<< CREATED TABLE vendor_quotation >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vendor_quotation >>>'
go

/* 
 * TABLE: vendor_relationship 
 */

CREATE TABLE vendor_relationship(
    idparrel      DOM_UUID    NOT NULL,
    idvendor      DOM_ID      NULL,
    idinternal    DOM_ID      NULL,
    CONSTRAINT PK__vendor_r__86F7AE0AE94ABF3D PRIMARY KEY CLUSTERED (idparrel)
)
go



IF OBJECT_ID('vendor_relationship') IS NOT NULL
    PRINT '<<< CREATED TABLE vendor_relationship >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vendor_relationship >>>'
go

/* 
 * TABLE: vendor_type 
 */

CREATE TABLE vendor_type(
    idvndtyp       DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__vendor_t__53B81E08E9DB7D38 PRIMARY KEY CLUSTERED (idvndtyp)
)
go



IF OBJECT_ID('vendor_type') IS NOT NULL
    PRINT '<<< CREATED TABLE vendor_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vendor_type >>>'
go

/* 
 * TABLE: village 
 */

CREATE TABLE village(
    idgeobou    DOM_UUID    NOT NULL,
    CONSTRAINT PK__village__96A5A05ECF491310 PRIMARY KEY NONCLUSTERED (idgeobou)
)
go



IF OBJECT_ID('village') IS NOT NULL
    PRINT '<<< CREATED TABLE village >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE village >>>'
go

/* 
 * TABLE: we_service_type 
 */

CREATE TABLE we_service_type(
    idwetyp      DOM_IDENTITY    NOT NULL,
    idproduct    DOM_ID          NULL,
    frt          DOM_INTEGER     NULL,
    CONSTRAINT PK__we_servi__3B40FDAE580C5B4E PRIMARY KEY NONCLUSTERED (idwetyp)
)
go



IF OBJECT_ID('we_service_type') IS NOT NULL
    PRINT '<<< CREATED TABLE we_service_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE we_service_type >>>'
go

/* 
 * TABLE: we_type 
 */

CREATE TABLE we_type(
    idwetyp        DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__we_type__3B40FDAEE4594A1C PRIMARY KEY NONCLUSTERED (idwetyp)
)
go



IF OBJECT_ID('we_type') IS NOT NULL
    PRINT '<<< CREATED TABLE we_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE we_type >>>'
go

/* 
 * TABLE: we_type_good_standard 
 */

CREATE TABLE we_type_good_standard(
    idwegoostd    DOM_UUID        NOT NULL,
    idwetyp       DOM_IDENTITY    NULL,
    idproduct     DOM_ID          NULL,
    qty           DOM_QTY         NULL,
    CONSTRAINT PK__we_type___3F894AEE267EDE17 PRIMARY KEY NONCLUSTERED (idwegoostd)
)
go



IF OBJECT_ID('we_type_good_standard') IS NOT NULL
    PRINT '<<< CREATED TABLE we_type_good_standard >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE we_type_good_standard >>>'
go

/* 
 * TABLE: work_effort 
 */

CREATE TABLE work_effort(
    idwe           DOM_UUID           NOT NULL,
    idfacility     DOM_UUID           NULL,
    idwetyp        DOM_IDENTITY       NULL,
    name           DOM_NAME           NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__work_eff__9DBB0C7B4CE02AF7 PRIMARY KEY NONCLUSTERED (idwe)
)
go



IF OBJECT_ID('work_effort') IS NOT NULL
    PRINT '<<< CREATED TABLE work_effort >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_effort >>>'
go

/* 
 * TABLE: work_effort_payment 
 */

CREATE TABLE work_effort_payment(
    payments_id        DOM_UUID    NOT NULL,
    work_efforts_id    DOM_UUID    NOT NULL,
    CONSTRAINT PK__work_eff__D6BB04347C97ADA3 PRIMARY KEY NONCLUSTERED (payments_id, work_efforts_id)
)
go



IF OBJECT_ID('work_effort_payment') IS NOT NULL
    PRINT '<<< CREATED TABLE work_effort_payment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_effort_payment >>>'
go

/* 
 * TABLE: work_effort_role 
 */

CREATE TABLE work_effort_role(
    idrole        DOM_UUID        NOT NULL,
    idwe          DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__work_eff__F324A06CF8E6F376 PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('work_effort_role') IS NOT NULL
    PRINT '<<< CREATED TABLE work_effort_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_effort_role >>>'
go

/* 
 * TABLE: work_effort_status 
 */

CREATE TABLE work_effort_status(
    idstatus        DOM_UUID        NOT NULL,
    idwe            DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__work_eff__EBF77C2C91243AE4 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('work_effort_status') IS NOT NULL
    PRINT '<<< CREATED TABLE work_effort_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_effort_status >>>'
go

/* 
 * TABLE: work_order 
 */

CREATE TABLE work_order(
    idreq    DOM_UUID    NOT NULL,
    CONSTRAINT PK__work_ord__24C5146C9322F3A6 PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('work_order') IS NOT NULL
    PRINT '<<< CREATED TABLE work_order >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_order >>>'
go

/* 
 * TABLE: work_order_booking 
 */

CREATE TABLE work_order_booking(
    idbooking        DOM_UUID        NOT NULL,
    idvehicle        DOM_UUID        NULL,
    idbooslo         DOM_UUID        NULL,
    idboktyp         DOM_IDENTITY    NULL,
    idevetyp         DOM_IDENTITY    NULL,
    idcustomer       DOM_ID          NULL,
    idinternal       DOM_ID          NULL,
    bookingnumber    DOM_ID          NULL,
    vehiclenumber    DOM_SHORTID     NULL,
    dtcreate         DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PK__work_ord__DBB298C0D1BF25B6 PRIMARY KEY NONCLUSTERED (idbooking)
)
go



IF OBJECT_ID('work_order_booking') IS NOT NULL
    PRINT '<<< CREATED TABLE work_order_booking >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_order_booking >>>'
go

/* 
 * TABLE: work_order_booking_role 
 */

CREATE TABLE work_order_booking_role(
    idrole        DOM_UUID        NOT NULL,
    idbooking     DOM_UUID        NULL,
    idparty       DOM_UUID        NULL,
    idroletype    DOM_IDENTITY    NULL,
    username      DOM_NAME        NULL,
    dtfrom        DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__work_ord__F324A06C202C5BEE PRIMARY KEY NONCLUSTERED (idrole)
)
go



IF OBJECT_ID('work_order_booking_role') IS NOT NULL
    PRINT '<<< CREATED TABLE work_order_booking_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_order_booking_role >>>'
go

/* 
 * TABLE: work_order_booking_status 
 */

CREATE TABLE work_order_booking_status(
    idstatus        DOM_UUID        NOT NULL,
    idbooking       DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          varchar(255)    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK__work_ord__EBF77C2CA65F1593 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('work_order_booking_status') IS NOT NULL
    PRINT '<<< CREATED TABLE work_order_booking_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_order_booking_status >>>'
go

/* 
 * TABLE: work_product_req 
 */

CREATE TABLE work_product_req(
    idreq    DOM_UUID    NOT NULL,
    CONSTRAINT PK__work_pro__24C5146C49115793 PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('work_product_req') IS NOT NULL
    PRINT '<<< CREATED TABLE work_product_req >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_product_req >>>'
go

/* 
 * TABLE: work_requirement 
 */

CREATE TABLE work_requirement(
    idreq    DOM_UUID    NOT NULL,
    CONSTRAINT PK__work_req__24C5146CFEE1087D PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('work_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE work_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_requirement >>>'
go

/* 
 * TABLE: work_requirement_services 
 */

CREATE TABLE work_requirement_services(
    idwe     DOM_UUID    NOT NULL,
    idreq    DOM_UUID    NOT NULL,
    CONSTRAINT PK__work_req__5FF75D3D1CD58157 PRIMARY KEY NONCLUSTERED (idwe, idreq)
)
go



IF OBJECT_ID('work_requirement_services') IS NOT NULL
    PRINT '<<< CREATED TABLE work_requirement_services >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_requirement_services >>>'
go

/* 
 * TABLE: work_service_requirement 
 */

CREATE TABLE work_service_requirement(
    idwe    DOM_UUID    NOT NULL,
    CONSTRAINT PK__work_ser__9DBB0C7BCBB6165A PRIMARY KEY NONCLUSTERED (idwe)
)
go



IF OBJECT_ID('work_service_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE work_service_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_service_requirement >>>'
go

/* 
 * TABLE: work_type 
 */

CREATE TABLE work_type(
    idworktype     DOM_IDENTITY       IDENTITY(1,1),
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK__work_typ__1792F7D030734901 PRIMARY KEY NONCLUSTERED (idworktype)
)
go



IF OBJECT_ID('work_type') IS NOT NULL
    PRINT '<<< CREATED TABLE work_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_type >>>'
go

/* 
 * INDEX: billing_item_type_refkey 
 */

CREATE UNIQUE INDEX billing_item_type_refkey ON billing_item_type(refkey)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('billing_item_type') AND name='billing_item_type_refkey')
    PRINT '<<< CREATED INDEX billing_item_type.billing_item_type_refkey >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX billing_item_type.billing_item_type_refkey >>>'
go

/* 
 * INDEX: customer_id_mpm 
 */

CREATE UNIQUE INDEX customer_id_mpm ON customer(idmpm)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('customer') AND name='customer_id_mpm')
    PRINT '<<< CREATED INDEX customer.customer_id_mpm >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX customer.customer_id_mpm >>>'
go

/* 
 * INDEX: geo_boundary_code 
 */

CREATE UNIQUE INDEX geo_boundary_code ON geo_boundary(geocode)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('geo_boundary') AND name='geo_boundary_code')
    PRINT '<<< CREATED INDEX geo_boundary.geo_boundary_code >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX geo_boundary.geo_boundary_code >>>'
go

/* 
 * INDEX: good_identification_value 
 */

CREATE UNIQUE INDEX good_identification_value ON good_identification(idvalue)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('good_identification') AND name='good_identification_value')
    PRINT '<<< CREATED INDEX good_identification.good_identification_value >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX good_identification.good_identification_value >>>'
go

/* 
 * INDEX: FK_JOB_STAT_REFERENCE_STATUS_T 
 */

CREATE INDEX FK_JOB_STAT_REFERENCE_STATUS_T ON job_status(idstatustype)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('job_status') AND name='FK_JOB_STAT_REFERENCE_STATUS_T')
    PRINT '<<< CREATED INDEX job_status.FK_JOB_STAT_REFERENCE_STATUS_T >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX job_status.FK_JOB_STAT_REFERENCE_STATUS_T >>>'
go

/* 
 * INDEX: leasing_company_id 
 */

CREATE UNIQUE INDEX leasing_company_id ON leasing_company(idleacom)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('leasing_company') AND name='leasing_company_id')
    PRINT '<<< CREATED INDEX leasing_company.leasing_company_id >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX leasing_company.leasing_company_id >>>'
go

/* 
 * INDEX: mem_number 
 */

CREATE UNIQUE INDEX mem_number ON memo(memonumber)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('memo') AND name='mem_number')
    PRINT '<<< CREATED INDEX memo.mem_number >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX memo.mem_number >>>'
go

/* 
 * INDEX: [nonclusteredindex-20180401-183606] 
 */

CREATE INDEX [nonclusteredindex-20180401-183606] ON package_receipt(documentnumber)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('package_receipt') AND name='nonclusteredindex-20180401-183606')
    PRINT '<<< CREATED INDEX package_receipt.nonclusteredindex-20180401-183606 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX package_receipt.nonclusteredindex-20180401-183606 >>>'
go

/* 
 * INDEX: party_category_refkey 
 */

CREATE INDEX party_category_refkey ON party_category(idcattyp, refkey)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('party_category') AND name='party_category_refkey')
    PRINT '<<< CREATED INDEX party_category.party_category_refkey >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX party_category.party_category_refkey >>>'
go

/* 
 * INDEX: idpaytyp 
 */

CREATE INDEX idpaytyp ON payment(idpaytyp)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('payment') AND name='idpaytyp')
    PRINT '<<< CREATED INDEX payment.idpaytyp >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX payment.idpaytyp >>>'
go

/* 
 * INDEX: person_id_user 
 */

CREATE INDEX person_id_user ON person(username)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('person') AND name='person_id_user')
    PRINT '<<< CREATED INDEX person.person_id_user >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX person.person_id_user >>>'
go

/* 
 * INDEX: idordite 
 */

CREATE INDEX idordite ON picking_slip(idordite)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('picking_slip') AND name='idordite')
    PRINT '<<< CREATED INDEX picking_slip.idordite >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX picking_slip.idordite >>>'
go

/* 
 * INDEX: idinvite 
 */

CREATE INDEX idinvite ON picking_slip(idinvite)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('picking_slip') AND name='idinvite')
    PRINT '<<< CREATED INDEX picking_slip.idinvite >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX picking_slip.idinvite >>>'
go

/* 
 * INDEX: FK_PKB_PART_REFERENCE_PKB 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_PKB ON pkb_part(idpkb)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_part') AND name='FK_PKB_PART_REFERENCE_PKB')
    PRINT '<<< CREATED INDEX pkb_part.FK_PKB_PART_REFERENCE_PKB >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_part.FK_PKB_PART_REFERENCE_PKB >>>'
go

/* 
 * INDEX: idinvite 
 */

CREATE UNIQUE INDEX idinvite ON pkb_part(idinvite)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_part') AND name='idinvite')
    PRINT '<<< CREATED INDEX pkb_part.idinvite >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_part.idinvite >>>'
go

/* 
 * INDEX: FK_PKB_PART_REFERENCE_CHARGE_T 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_CHARGE_T ON pkb_part(idchargeto)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_part') AND name='FK_PKB_PART_REFERENCE_CHARGE_T')
    PRINT '<<< CREATED INDEX pkb_part.FK_PKB_PART_REFERENCE_CHARGE_T >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_part.FK_PKB_PART_REFERENCE_CHARGE_T >>>'
go

/* 
 * INDEX: FK_PKB_PART_REFERENCE_PKB_SERV 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_PKB_SERV ON pkb_part(idpkbservice)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_part') AND name='FK_PKB_PART_REFERENCE_PKB_SERV')
    PRINT '<<< CREATED INDEX pkb_part.FK_PKB_PART_REFERENCE_PKB_SERV >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_part.FK_PKB_PART_REFERENCE_PKB_SERV >>>'
go

/* 
 * INDEX: FK_PKB_PART_REFERENCE_CATEGORY 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_CATEGORY ON pkb_part(idcategory)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_part') AND name='FK_PKB_PART_REFERENCE_CATEGORY')
    PRINT '<<< CREATED INDEX pkb_part.FK_PKB_PART_REFERENCE_CATEGORY >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_part.FK_PKB_PART_REFERENCE_CATEGORY >>>'
go

/* 
 * INDEX: FK_PKB_PART_REFERENCE_GOOD 
 */

CREATE INDEX FK_PKB_PART_REFERENCE_GOOD ON pkb_part(idproduct)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_part') AND name='FK_PKB_PART_REFERENCE_GOOD')
    PRINT '<<< CREATED INDEX pkb_part.FK_PKB_PART_REFERENCE_GOOD >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_part.FK_PKB_PART_REFERENCE_GOOD >>>'
go

/* 
 * INDEX: FK_PKB_SERV_REFERENCE_CATEGORY 
 */

CREATE INDEX FK_PKB_SERV_REFERENCE_CATEGORY ON pkb_service(idcategory)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_service') AND name='FK_PKB_SERV_REFERENCE_CATEGORY')
    PRINT '<<< CREATED INDEX pkb_service.FK_PKB_SERV_REFERENCE_CATEGORY >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_service.FK_PKB_SERV_REFERENCE_CATEGORY >>>'
go

/* 
 * INDEX: FK_PKB_SERV_REFERENCE_SERVICE 
 */

CREATE INDEX FK_PKB_SERV_REFERENCE_SERVICE ON pkb_service(idproduct)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_service') AND name='FK_PKB_SERV_REFERENCE_SERVICE')
    PRINT '<<< CREATED INDEX pkb_service.FK_PKB_SERV_REFERENCE_SERVICE >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_service.FK_PKB_SERV_REFERENCE_SERVICE >>>'
go

/* 
 * INDEX: FK_PKB_SERV_REFERENCE_CHARGE_T 
 */

CREATE INDEX FK_PKB_SERV_REFERENCE_CHARGE_T ON pkb_service(idchargeto)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_service') AND name='FK_PKB_SERV_REFERENCE_CHARGE_T')
    PRINT '<<< CREATED INDEX pkb_service.FK_PKB_SERV_REFERENCE_CHARGE_T >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_service.FK_PKB_SERV_REFERENCE_CHARGE_T >>>'
go

/* 
 * INDEX: FK_PKB_SERV_RELATIONS_PKB 
 */

CREATE INDEX FK_PKB_SERV_RELATIONS_PKB ON pkb_service(idpkb)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('pkb_service') AND name='FK_PKB_SERV_RELATIONS_PKB')
    PRINT '<<< CREATED INDEX pkb_service.FK_PKB_SERV_RELATIONS_PKB >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX pkb_service.FK_PKB_SERV_RELATIONS_PKB >>>'
go

/* 
 * INDEX: price_component_period 
 */

CREATE INDEX price_component_period ON price_component(dtfrom, dtthru)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('price_component') AND name='price_component_period')
    PRINT '<<< CREATED INDEX price_component.price_component_period >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX price_component.price_component_period >>>'
go

/* 
 * INDEX: idprotyp 
 */

CREATE INDEX idprotyp ON product(idprotyp)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('product') AND name='idprotyp')
    PRINT '<<< CREATED INDEX product.idprotyp >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX product.idprotyp >>>'
go

/* 
 * INDEX: product_category_refkey 
 */

CREATE INDEX product_category_refkey ON product_category(idcattyp, refkey)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('product_category') AND name='product_category_refkey')
    PRINT '<<< CREATED INDEX product_category.product_category_refkey >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX product_category.product_category_refkey >>>'
go

/* 
 * INDEX: idproduct 
 */

CREATE INDEX idproduct ON sales_unit_requirement(idproduct)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idproduct')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idproduct >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idproduct >>>'
go

/* 
 * INDEX: idowner 
 */

CREATE INDEX idowner ON sales_unit_requirement(idowner)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idowner')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idowner >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idowner >>>'
go

/* 
 * INDEX: idprospect 
 */

CREATE INDEX idprospect ON sales_unit_requirement(idprospect)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idprospect')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idprospect >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idprospect >>>'
go

/* 
 * INDEX: idsalesman 
 */

CREATE INDEX idsalesman ON sales_unit_requirement(idsalesman)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idsalesman')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idsalesman >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idsalesman >>>'
go

/* 
 * INDEX: idcolor 
 */

CREATE INDEX idcolor ON sales_unit_requirement(idcolor)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idcolor')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idcolor >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idcolor >>>'
go

/* 
 * INDEX: idsalesbroker 
 */

CREATE INDEX idsalesbroker ON sales_unit_requirement(idsalesbroker)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idsalesbroker')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idsalesbroker >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idsalesbroker >>>'
go

/* 
 * INDEX: idlsgpro 
 */

CREATE INDEX idlsgpro ON sales_unit_requirement(idlsgpro)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idlsgpro')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idlsgpro >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idlsgpro >>>'
go

/* 
 * INDEX: idsaletype 
 */

CREATE INDEX idsaletype ON sales_unit_requirement(idsaletype)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('sales_unit_requirement') AND name='idsaletype')
    PRINT '<<< CREATED INDEX sales_unit_requirement.idsaletype >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX sales_unit_requirement.idsaletype >>>'
go

/* 
 * INDEX: idcoordinator 
 */

CREATE INDEX idcoordinator ON salesman(idcoordinator)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('salesman') AND name='idcoordinator')
    PRINT '<<< CREATED INDEX salesman.idcoordinator >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX salesman.idcoordinator >>>'
go

/* 
 * INDEX: stk_op_item_tag 
 */

CREATE UNIQUE INDEX stk_op_item_tag ON stock_opname_item(tagnumber)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('stock_opname_item') AND name='stk_op_item_tag')
    PRINT '<<< CREATED INDEX stock_opname_item.stk_op_item_tag >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX stock_opname_item.stk_op_item_tag >>>'
go

/* 
 * INDEX: idsalesman 
 */

CREATE INDEX idsalesman ON suspect(idsalesman)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('suspect') AND name='idsalesman')
    PRINT '<<< CREATED INDEX suspect.idsalesman >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX suspect.idsalesman >>>'
go

/* 
 * INDEX: idsuspecttyp 
 */

CREATE INDEX idsuspecttyp ON suspect(idsuspecttyp)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('suspect') AND name='idsuspecttyp')
    PRINT '<<< CREATED INDEX suspect.idsuspecttyp >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX suspect.idsuspecttyp >>>'
go

/* 
 * INDEX: idparty 
 */

CREATE INDEX idparty ON suspect(idparty)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('suspect') AND name='idparty')
    PRINT '<<< CREATED INDEX suspect.idparty >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX suspect.idparty >>>'
go

/* 
 * INDEX: idsalescoordinator 
 */

CREATE INDEX idsalescoordinator ON suspect(idsalescoordinator)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('suspect') AND name='idsalescoordinator')
    PRINT '<<< CREATED INDEX suspect.idsalescoordinator >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX suspect.idsalescoordinator >>>'
go

/* 
 * INDEX: idaddress 
 */

CREATE INDEX idaddress ON suspect(idaddress)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('suspect') AND name='idaddress')
    PRINT '<<< CREATED INDEX suspect.idaddress >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX suspect.idaddress >>>'
go

/* 
 * INDEX: iddeltype 
 */

CREATE INDEX iddeltype ON unit_deliverable(iddeltype)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('unit_deliverable') AND name='iddeltype')
    PRINT '<<< CREATED INDEX unit_deliverable.iddeltype >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX unit_deliverable.iddeltype >>>'
go

/* 
 * INDEX: idvndstatyp 
 */

CREATE INDEX idvndstatyp ON unit_deliverable(idvndstatyp)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('unit_deliverable') AND name='idvndstatyp')
    PRINT '<<< CREATED INDEX unit_deliverable.idvndstatyp >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX unit_deliverable.idvndstatyp >>>'
go

/* 
 * INDEX: idconstatyp 
 */

CREATE INDEX idconstatyp ON unit_deliverable(idconstatyp)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('unit_deliverable') AND name='idconstatyp')
    PRINT '<<< CREATED INDEX unit_deliverable.idconstatyp >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX unit_deliverable.idconstatyp >>>'
go

/* 
 * INDEX: idreq 
 */

CREATE INDEX idreq ON unit_deliverable(idreq)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('unit_deliverable') AND name='idreq')
    PRINT '<<< CREATED INDEX unit_deliverable.idreq >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX unit_deliverable.idreq >>>'
go

/* 
 * INDEX: user_mediator_username 
 */

CREATE UNIQUE INDEX user_mediator_username ON user_mediator(username)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('user_mediator') AND name='user_mediator_username')
    PRINT '<<< CREATED INDEX user_mediator.user_mediator_username >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX user_mediator.user_mediator_username >>>'
go

/* 
 * INDEX: idsaletype 
 */

CREATE INDEX idsaletype ON vehicle_document_requirement(idsaletype)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_document_requirement') AND name='idsaletype')
    PRINT '<<< CREATED INDEX vehicle_document_requirement.idsaletype >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_document_requirement.idsaletype >>>'
go

/* 
 * INDEX: idordite 
 */

CREATE INDEX idordite ON vehicle_document_requirement(idordite)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_document_requirement') AND name='idordite')
    PRINT '<<< CREATED INDEX vehicle_document_requirement.idordite >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_document_requirement.idordite >>>'
go

/* 
 * INDEX: idpersonowner 
 */

CREATE INDEX idpersonowner ON vehicle_document_requirement(idpersonowner)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_document_requirement') AND name='idpersonowner')
    PRINT '<<< CREATED INDEX vehicle_document_requirement.idpersonowner >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_document_requirement.idpersonowner >>>'
go

/* 
 * INDEX: idvehregtyp 
 */

CREATE INDEX idvehregtyp ON vehicle_document_requirement(idvehregtyp)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_document_requirement') AND name='idvehregtyp')
    PRINT '<<< CREATED INDEX vehicle_document_requirement.idvehregtyp >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_document_requirement.idvehregtyp >>>'
go

/* 
 * INDEX: idvehicle 
 */

CREATE INDEX idvehicle ON vehicle_document_requirement(idvehicle)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_document_requirement') AND name='idvehicle')
    PRINT '<<< CREATED INDEX vehicle_document_requirement.idvehicle >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_document_requirement.idvehicle >>>'
go

/* 
 * INDEX: idslsreq 
 */

CREATE INDEX idslsreq ON vehicle_document_requirement(idslsreq)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_document_requirement') AND name='idslsreq')
    PRINT '<<< CREATED INDEX vehicle_document_requirement.idslsreq >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_document_requirement.idslsreq >>>'
go

/* 
 * INDEX: idsaletype 
 */

CREATE INDEX idsaletype ON vehicle_sales_order(idsaletype)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_sales_order') AND name='idsaletype')
    PRINT '<<< CREATED INDEX vehicle_sales_order.idsaletype >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_sales_order.idsaletype >>>'
go

/* 
 * INDEX: idunitreq 
 */

CREATE INDEX idunitreq ON vehicle_sales_order(idunitreq)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('vehicle_sales_order') AND name='idunitreq')
    PRINT '<<< CREATED INDEX vehicle_sales_order.idunitreq >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX vehicle_sales_order.idunitreq >>>'
go

/* 
 * INDEX: we_service_type_id_product 
 */

CREATE UNIQUE INDEX we_service_type_id_product ON we_service_type(idproduct)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('we_service_type') AND name='we_service_type_id_product')
    PRINT '<<< CREATED INDEX we_service_type.we_service_type_id_product >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX we_service_type.we_service_type_id_product >>>'
go

/* 
 * TABLE: accounting_calendar 
 */

ALTER TABLE accounting_calendar ADD CONSTRAINT Refbase_calendar2386 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
go

ALTER TABLE accounting_calendar ADD CONSTRAINT FK__accountin__idint__3FBB6990 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: acctg_trans_detail 
 */

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idacc__438BFA74 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idcal__4297D63B 
    FOREIGN KEY (idcalendar)
    REFERENCES accounting_calendar(idcalendar)
go

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idgla__40AF8DC9 
    FOREIGN KEY (idglacc)
    REFERENCES gl_account(idglacc)
go

ALTER TABLE acctg_trans_detail ADD CONSTRAINT FK__acctg_tra__idint__41A3B202 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: acctg_transaction 
 */

ALTER TABLE acctg_transaction ADD CONSTRAINT FK__acctg_tra__idacc__44801EAD 
    FOREIGN KEY (idacctratyp)
    REFERENCES acctg_trans_type(idacctratyp)
go


/* 
 * TABLE: acctg_transaction_status 
 */

ALTER TABLE acctg_transaction_status ADD CONSTRAINT FK__acctg_tra__idacc__475C8B58 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go

ALTER TABLE acctg_transaction_status ADD CONSTRAINT FK__acctg_tra__idrea__4668671F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE acctg_transaction_status ADD CONSTRAINT FK__acctg_tra__idsta__457442E6 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: agreement 
 */

ALTER TABLE agreement ADD CONSTRAINT FK__agreement__idagr__4850AF91 
    FOREIGN KEY (idagrtyp)
    REFERENCES agreement_type(idagrtyp)
go


/* 
 * TABLE: agreement_item 
 */

ALTER TABLE agreement_item ADD CONSTRAINT FK__agreement__idagr__4944D3CA 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go


/* 
 * TABLE: agreement_motor_item 
 */

ALTER TABLE agreement_motor_item ADD CONSTRAINT FK__agreement__idagr__4B2D1C3C 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
go

ALTER TABLE agreement_motor_item ADD CONSTRAINT FK__agreement__idveh__4A38F803 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: agreement_role 
 */

ALTER TABLE agreement_role ADD CONSTRAINT FK__agreement__idagr__4C214075 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE agreement_role ADD CONSTRAINT FK__agreement__idpar__4E0988E7 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE agreement_role ADD CONSTRAINT FK__agreement__idrol__4D1564AE 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: agreement_status 
 */

ALTER TABLE agreement_status ADD CONSTRAINT FK__agreement__idagr__4EFDAD20 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE agreement_status ADD CONSTRAINT FK__agreement__idrea__50E5F592 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE agreement_status ADD CONSTRAINT FK__agreement__idsta__4FF1D159 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: approval 
 */

ALTER TABLE approval ADD CONSTRAINT FK__approval__idappt__54B68676 
    FOREIGN KEY (idapptyp)
    REFERENCES approval_type(idapptyp)
go

ALTER TABLE approval ADD CONSTRAINT FK__approval__idmess__55AAAAAF 
    FOREIGN KEY (idmessage)
    REFERENCES unit_document_message(idmessage)
go

ALTER TABLE approval ADD CONSTRAINT FK__approval__idordi__53C2623D 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE approval ADD CONSTRAINT FK__approval__idreq__52CE3E04 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE approval ADD CONSTRAINT FK__approval__idstat__51DA19CB 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: approval_stockopname_tm 
 */

ALTER TABLE approval_stockopname_tm ADD CONSTRAINT FK__approval___idapp__5792F321 
    FOREIGN KEY (idapprovalstockopnamestatus)
    REFERENCES approval_stockopname_status_tp(idapprovalstockopnamestatus)
go

ALTER TABLE approval_stockopname_tm ADD CONSTRAINT FK__approval___idsto__569ECEE8 
    FOREIGN KEY (idstockopname)
    REFERENCES stockopname_tm(idstockopname)
go


/* 
 * TABLE: attendance 
 */

ALTER TABLE attendance ADD CONSTRAINT FK__attendanc__idpar__5887175A 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go


/* 
 * TABLE: base_calendar 
 */

ALTER TABLE base_calendar ADD CONSTRAINT FK__base_cale__idcal__597B3B93 
    FOREIGN KEY (idcaltyp)
    REFERENCES calendar_type(idcaltyp)
go

ALTER TABLE base_calendar ADD CONSTRAINT FK__base_cale__idpar__5A6F5FCC 
    FOREIGN KEY (idparent)
    REFERENCES base_calendar(idcalendar)
go


/* 
 * TABLE: bill_to 
 */

ALTER TABLE bill_to ADD CONSTRAINT FK__bill_to__5B638405 
    FOREIGN KEY (idparty, idroletype)
    REFERENCES party_role_type(idparty, idroletype)
go


/* 
 * TABLE: billing 
 */

ALTER TABLE billing ADD CONSTRAINT FK__billing__idbilfr__5F3414E9 
    FOREIGN KEY (idbilfro)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE billing ADD CONSTRAINT FK__billing__idbilto__5E3FF0B0 
    FOREIGN KEY (idbilto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE billing ADD CONSTRAINT FK__billing__idbilty__5C57A83E 
    FOREIGN KEY (idbiltyp)
    REFERENCES billing_type(idbiltyp)
go

ALTER TABLE billing ADD CONSTRAINT FK__billing__idinter__5D4BCC77 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: billing_acctg_trans 
 */

ALTER TABLE billing_acctg_trans ADD CONSTRAINT FK__billing_a__idacc__611C5D5B 
    FOREIGN KEY (idacctra)
    REFERENCES external_acctg_trans(idacctra)
go

ALTER TABLE billing_acctg_trans ADD CONSTRAINT FK__billing_a__idbil__60283922 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: billing_disbursement 
 */

ALTER TABLE billing_disbursement ADD CONSTRAINT Refbilling2387 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_disbursement ADD CONSTRAINT FK__billing_d__idbil__63F8CA06 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
go

ALTER TABLE billing_disbursement ADD CONSTRAINT FK__billing_d__idven__6304A5CD 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: billing_issuance 
 */

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idbil__67C95AEA 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idinv__64ECEE3F 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idshi__66D536B1 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE billing_issuance ADD CONSTRAINT FK__billing_i__idsli__65E11278 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go


/* 
 * TABLE: billing_item 
 */

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idbil__68BD7F23 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idbil__6C8E1007 
    FOREIGN KEY (idbilitetyp)
    REFERENCES billing_item_type(idbilitetyp)
go

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idfea__6D823440 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idinv__69B1A35C 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idpro__6AA5C795 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE billing_item ADD CONSTRAINT FK__billing_i__idwet__6B99EBCE 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go


/* 
 * TABLE: billing_njb 
 */

ALTER TABLE billing_njb ADD CONSTRAINT FK__billing_n__idbil__6E765879 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: billing_nsc 
 */

ALTER TABLE billing_nsc ADD CONSTRAINT FK__billing_n__idbil__6F6A7CB2 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go


/* 
 * TABLE: billing_receipt 
 */

ALTER TABLE billing_receipt ADD CONSTRAINT Refbilling2388 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_receipt ADD CONSTRAINT FK__billing_r__idbil__7152C524 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
go


/* 
 * TABLE: billing_role 
 */

ALTER TABLE billing_role ADD CONSTRAINT FK__billing_r__idbil__7246E95D 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_role ADD CONSTRAINT FK__billing_r__idpar__742F31CF 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE billing_role ADD CONSTRAINT FK__billing_r__idrol__733B0D96 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: billing_status 
 */

ALTER TABLE billing_status ADD CONSTRAINT FK__billing_s__idbil__75235608 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE billing_status ADD CONSTRAINT FK__billing_s__idrea__770B9E7A 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE billing_status ADD CONSTRAINT FK__billing_s__idsta__76177A41 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: booking 
 */

ALTER TABLE booking ADD CONSTRAINT FK__booking__idbooki__77FFC2B3 
    FOREIGN KEY (idbookingstatus)
    REFERENCES booking_status(idbookingstatus)
go


/* 
 * TABLE: booking_slot 
 */

ALTER TABLE booking_slot ADD CONSTRAINT FK__booking_s__idboo__79E80B25 
    FOREIGN KEY (idbooslostd)
    REFERENCES booking_slot_standard(idbooslostd)
go

ALTER TABLE booking_slot ADD CONSTRAINT FK__booking_s__idcal__78F3E6EC 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
go


/* 
 * TABLE: booking_slot_standard 
 */

ALTER TABLE booking_slot_standard ADD CONSTRAINT FK__booking_s__idbok__7BD05397 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
go

ALTER TABLE booking_slot_standard ADD CONSTRAINT FK__booking_s__idint__7ADC2F5E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: branch 
 */

ALTER TABLE branch ADD CONSTRAINT FK__branch__idbranch__7DB89C09 
    FOREIGN KEY (idbranchcategory)
    REFERENCES party_category(idcategory)
go

ALTER TABLE branch ADD CONSTRAINT FK__branch__idintern__7CC477D0 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: carrier 
 */

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idparty__01892CED 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idreaso__7EACC042 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idrelty__7FA0E47B 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
go

ALTER TABLE carrier ADD CONSTRAINT FK__carrier__idvehic__009508B4 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: city 
 */

ALTER TABLE city ADD CONSTRAINT Refgeo_boundary2389 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: claim_detail 
 */

ALTER TABLE claim_detail ADD CONSTRAINT FK__claim_det__idcus__08362A7C 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE claim_detail ADD CONSTRAINT FK__claim_det__iddat__064DE20A 
    FOREIGN KEY (iddatadamage)
    REFERENCES data_damage(iddatadamage)
go

ALTER TABLE claim_detail ADD CONSTRAINT FK__claim_det__idsym__07420643 
    FOREIGN KEY (idsympthomdatam)
    REFERENCES sympthom_data_m(idsympthomdatam)
go


/* 
 * TABLE: communication_event 
 */

ALTER TABLE communication_event ADD CONSTRAINT FK__communica__ideve__0A1E72EE 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
go

ALTER TABLE communication_event ADD CONSTRAINT FK__communica__idpar__092A4EB5 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go


/* 
 * TABLE: communication_event_cdb 
 */

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refreligion_type3048 
    FOREIGN KEY (idreligion)
    REFERENCES religion_type(idreligiontype)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refprovince3049 
    FOREIGN KEY (idprovincesurat)
    REFERENCES province(idgeobou)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refdistrict3050 
    FOREIGN KEY (idcitysurat)
    REFERENCES district(idgeobou)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refdistrict3051 
    FOREIGN KEY (iddistrictsurat)
    REFERENCES district(idgeobou)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT Refvillage3052 
    FOREIGN KEY (idvillagesurat)
    REFERENCES village(idgeobou)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idcit__0DEF03D2 
    FOREIGN KEY (idcity)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idcom__0C06BB60 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idcus__0B129727 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__iddis__0EE3280B 
    FOREIGN KEY (iddistrict)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE communication_event_cdb ADD CONSTRAINT FK__communica__idpro__0CFADF99 
    FOREIGN KEY (idprovince)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: communication_event_delivery 
 */

ALTER TABLE communication_event_delivery ADD CONSTRAINT FK__communica__idcus__10CB707D 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE communication_event_delivery ADD CONSTRAINT FK__communica__idint__0FD74C44 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: communication_event_prospect 
 */

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idcol__149C0161 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
go

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idcom__1590259A 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idpro__11BF94B6 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idpro__13A7DD28 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE communication_event_prospect ADD CONSTRAINT FK__communica__idsal__12B3B8EF 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: communication_event_purpose 
 */

ALTER TABLE communication_event_purpose ADD CONSTRAINT FK__communica__idcom__17786E0C 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_purpose ADD CONSTRAINT FK__communica__idpur__168449D3 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: communication_event_role 
 */

ALTER TABLE communication_event_role ADD CONSTRAINT FK__communica__idcom__186C9245 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_role ADD CONSTRAINT FK__communica__idpar__1A54DAB7 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE communication_event_role ADD CONSTRAINT FK__communica__idrol__1960B67E 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: communication_event_status 
 */

ALTER TABLE communication_event_status ADD CONSTRAINT FK__communica__idcom__1B48FEF0 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE communication_event_status ADD CONSTRAINT FK__communica__idrea__1D314762 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE communication_event_status ADD CONSTRAINT FK__communica__idsta__1C3D2329 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: contact_purpose 
 */

ALTER TABLE contact_purpose ADD CONSTRAINT FK__contact_p__idcon__1F198FD4 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE contact_purpose ADD CONSTRAINT FK__contact_p__idpur__1E256B9B 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: container 
 */

ALTER TABLE container ADD CONSTRAINT FK__container__idcon__200DB40D 
    FOREIGN KEY (idcontyp)
    REFERENCES container_type(idcontyp)
go

ALTER TABLE container ADD CONSTRAINT FK__container__idfac__2101D846 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: correction_note 
 */

ALTER TABLE correction_note ADD CONSTRAINT FK__correctio__idiss__22EA20B8 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go

ALTER TABLE correction_note ADD CONSTRAINT FK__correctio__idpro__23DE44F1 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE correction_note ADD CONSTRAINT FK__correctio__idspg__21F5FC7F 
    FOREIGN KEY (idspgclaim)
    REFERENCES spg_claim(idspgclaim)
go


/* 
 * TABLE: customer 
 */

ALTER TABLE customer ADD CONSTRAINT FK__customer__idmark__26BAB19C 
    FOREIGN KEY (idmarketcategory)
    REFERENCES party_category(idcategory)
go

ALTER TABLE customer ADD CONSTRAINT FK__customer__idpart__24D2692A 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE customer ADD CONSTRAINT FK__customer__idrole__25C68D63 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: customer_order 
 */

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idbil__2A8B4280 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idcus__27AED5D5 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idint__28A2FA0E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idord__29971E47 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE customer_order ADD CONSTRAINT FK__customer___idveh__2B7F66B9 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: customer_quotation 
 */

ALTER TABLE customer_quotation ADD CONSTRAINT Refquote2390 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go

ALTER TABLE customer_quotation ADD CONSTRAINT FK__customer___idcus__2C738AF2 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_quotation ADD CONSTRAINT FK__customer___idint__2D67AF2B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: customer_relationship 
 */

ALTER TABLE customer_relationship ADD CONSTRAINT FK__customer___idcus__30441BD6 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_relationship ADD CONSTRAINT FK__customer___idint__2F4FF79D 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE customer_relationship ADD CONSTRAINT FK__customer___idpar__3138400F 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go


/* 
 * TABLE: customer_request_item 
 */

ALTER TABLE customer_request_item ADD CONSTRAINT FK__customer___idcus__322C6448 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE customer_request_item ADD CONSTRAINT FK__customer___idreq__33208881 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: delivery_order 
 */

ALTER TABLE delivery_order ADD CONSTRAINT FK__delivery___idbil__3414ACBA 
    FOREIGN KEY (idbilling)
    REFERENCES billing_disbursement(idbilling)
go


/* 
 * TABLE: dimension 
 */

ALTER TABLE dimension ADD CONSTRAINT FK__dimension__iddim__3508D0F3 
    FOREIGN KEY (iddimtyp)
    REFERENCES dimension_type(iddimtyp)
go


/* 
 * TABLE: disbursement 
 */

ALTER TABLE disbursement ADD CONSTRAINT FK__disbursem__idpay__35FCF52C 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: district 
 */

ALTER TABLE district ADD CONSTRAINT Refgeo_boundary2391 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: document_type 
 */

ALTER TABLE document_type ADD CONSTRAINT FK__document___idpar__37E53D9E 
    FOREIGN KEY (idparent)
    REFERENCES document_type(iddoctype)
go


/* 
 * TABLE: documents 
 */

ALTER TABLE documents ADD CONSTRAINT FK__documents__iddoc__38D961D7 
    FOREIGN KEY (iddoctype)
    REFERENCES document_type(iddoctype)
go


/* 
 * TABLE: driver 
 */

ALTER TABLE driver ADD CONSTRAINT Refparty_role2392 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE driver ADD CONSTRAINT FK__driver__idvendor__3AC1AA49 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: electronic_address 
 */

ALTER TABLE electronic_address ADD CONSTRAINT FK__electroni__idcon__3BB5CE82 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go


/* 
 * TABLE: employee 
 */

ALTER TABLE employee ADD CONSTRAINT FK__employee__idparr__3CA9F2BB 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE employee ADD CONSTRAINT FK__employee__idpart__3D9E16F4 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go


/* 
 * TABLE: employee_customer_relationship 
 */

ALTER TABLE employee_customer_relationship ADD CONSTRAINT FK__employee___emplo__3E923B2D 
    FOREIGN KEY (employeenumber)
    REFERENCES employee(employeenumber)
go

ALTER TABLE employee_customer_relationship ADD CONSTRAINT FK__employee___idcus__3F865F66 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE employee_customer_relationship ADD CONSTRAINT FK__employee___idpar__407A839F 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go


/* 
 * TABLE: event_type 
 */

ALTER TABLE event_type ADD CONSTRAINT FK__event_typ__idprn__416EA7D8 
    FOREIGN KEY (idprneve)
    REFERENCES event_type(idevetyp)
go


/* 
 * TABLE: exchange_part 
 */

ALTER TABLE exchange_part ADD CONSTRAINT FK__exchange___idpro__444B1483 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE exchange_part ADD CONSTRAINT FK__exchange___idspg__4262CC11 
    FOREIGN KEY (idspgclaimdetail)
    REFERENCES spg_claim_detail(idspgclaimdetail)
go

ALTER TABLE exchange_part ADD CONSTRAINT FK__exchange___idspg__4356F04A 
    FOREIGN KEY (idspgclaimaction)
    REFERENCES spg_claim_action(idspgclaimaction)
go


/* 
 * TABLE: external_acctg_trans 
 */

ALTER TABLE external_acctg_trans ADD CONSTRAINT Refacctg_transaction2393 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go

ALTER TABLE external_acctg_trans ADD CONSTRAINT FK__external___idpar__453F38BC 
    FOREIGN KEY (idparfro)
    REFERENCES party(idparty)
go

ALTER TABLE external_acctg_trans ADD CONSTRAINT FK__external___idpar__46335CF5 
    FOREIGN KEY (idparto)
    REFERENCES party(idparty)
go


/* 
 * TABLE: facility 
 */

ALTER TABLE facility ADD CONSTRAINT FK__facility__idfa__4A03EDD9 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
go

ALTER TABLE facility ADD CONSTRAINT FK__facility__idfaci__490FC9A0 
    FOREIGN KEY (idfacilitytype)
    REFERENCES facility_type(idfacilitytype)
go

ALTER TABLE facility ADD CONSTRAINT FK__facility__idpart__481BA567 
    FOREIGN KEY (idpartof)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: facility_contact_mechanism 
 */

ALTER TABLE facility_contact_mechanism ADD CONSTRAINT FK__facility___idcon__4AF81212 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE facility_contact_mechanism ADD CONSTRAINT FK__facility___idfac__4BEC364B 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE facility_contact_mechanism ADD CONSTRAINT FK__facility___idpur__4CE05A84 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: facility_status 
 */

ALTER TABLE facility_status ADD CONSTRAINT FK__facility___idfac__4DD47EBD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE facility_status ADD CONSTRAINT FK__facility___idrea__4FBCC72F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE facility_status ADD CONSTRAINT FK__facility___idsta__4EC8A2F6 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: feature 
 */

ALTER TABLE feature ADD CONSTRAINT FK__feature__idfeaty__50B0EB68 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
go


/* 
 * TABLE: feature_applicable 
 */

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idfea__538D5813 
    FOREIGN KEY (idfeatyp)
    REFERENCES feature_type(idfeatyp)
go

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idfea__54817C4C 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idown__529933DA 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE feature_applicable ADD CONSTRAINT FK__feature_a__idpro__51A50FA1 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: feature_type 
 */

ALTER TABLE feature_type ADD CONSTRAINT FK__feature_t__idrul__5575A085 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: fixed_asset 
 */

ALTER TABLE fixed_asset ADD CONSTRAINT FK__fixed_ass__idfat__5669C4BE 
    FOREIGN KEY (idfatype)
    REFERENCES fixed_asset_type(idfatype)
go

ALTER TABLE fixed_asset ADD CONSTRAINT FK__fixed_ass__iduom__575DE8F7 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
go


/* 
 * TABLE: fixed_asset_type 
 */

ALTER TABLE fixed_asset_type ADD CONSTRAINT FK__fixed_ass__idpar__58520D30 
    FOREIGN KEY (idparent)
    REFERENCES fixed_asset_type(idfatype)
go


/* 
 * TABLE: fixedasset_adjustment_th 
 */

ALTER TABLE fixedasset_adjustment_th ADD CONSTRAINT FK__fixedasse__idfix__59463169 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
go


/* 
 * TABLE: fixedasset_th 
 */

ALTER TABLE fixedasset_th ADD CONSTRAINT FK__fixedasse__idfix__5C229E14 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
go

ALTER TABLE fixedasset_th ADD CONSTRAINT FK__fixedasse__idloc__5A3A55A2 
    FOREIGN KEY (idlocationfloor)
    REFERENCES floor_tm(idfloor)
go

ALTER TABLE fixedasset_th ADD CONSTRAINT FK__fixedasse__idloc__5B2E79DB 
    FOREIGN KEY (idlocationroom)
    REFERENCES room_tm(idroom)
go


/* 
 * TABLE: fixedasset_tm 
 */

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idcat__60E75331 
    FOREIGN KEY (idcategoryasset)
    REFERENCES category_asset_tp(idcategoryasset)
go

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idflo__5D16C24D 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
go

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idpur__5E0AE686 
    FOREIGN KEY (idpurchaseorderreception)
    REFERENCES purchaseorder_reception_tr(idpurchaseorderreception)
go

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idpur__5EFF0ABF 
    FOREIGN KEY (idpurchaseorder)
    REFERENCES purchaseorder_tm(idpurchaseorder)
go

ALTER TABLE fixedasset_tm ADD CONSTRAINT FK__fixedasse__idroo__5FF32EF8 
    FOREIGN KEY (idroom)
    REFERENCES room_tm(idroom)
go


/* 
 * TABLE: general_upload 
 */

ALTER TABLE general_upload ADD CONSTRAINT FK__general_u__idint__62CF9BA3 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE general_upload ADD CONSTRAINT FK__general_u__idpur__61DB776A 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: general_upload_status 
 */

ALTER TABLE general_upload_status ADD CONSTRAINT Refreason_type2398 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE general_upload_status ADD CONSTRAINT Refstatus_type2399 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE general_upload_status ADD CONSTRAINT FK__general_u__idgen__63C3BFDC 
    FOREIGN KEY (idgenupl)
    REFERENCES general_upload(idgenupl)
go


/* 
 * TABLE: geo_boundary 
 */

ALTER TABLE geo_boundary ADD CONSTRAINT FK__geo_bound__idgeo__64B7E415 
    FOREIGN KEY (idgeoboutype)
    REFERENCES geo_boundary_type(idgeoboutype)
go

ALTER TABLE geo_boundary ADD CONSTRAINT FK__geo_bound__idpar__65AC084E 
    FOREIGN KEY (idparent)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: gl_account 
 */

ALTER TABLE gl_account ADD CONSTRAINT FK__gl_accoun__idgla__66A02C87 
    FOREIGN KEY (idglacctyp)
    REFERENCES gl_account_type(idglacctyp)
go


/* 
 * TABLE: gl_dimension 
 */

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__679450C0 
    FOREIGN KEY (dimension1)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__688874F9 
    FOREIGN KEY (dimension5)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__697C9932 
    FOREIGN KEY (dimension4)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__6A70BD6B 
    FOREIGN KEY (dimension3)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__6B64E1A4 
    FOREIGN KEY (dimension2)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__dimen__6C5905DD 
    FOREIGN KEY (dimension6)
    REFERENCES dimension(iddimension)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idbil__6D4D2A16 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idint__711DBAFA 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idpar__702996C1 
    FOREIGN KEY (idparcat)
    REFERENCES party_category(idcategory)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idpro__6F357288 
    FOREIGN KEY (idprocat)
    REFERENCES product_category(idcategory)
go

ALTER TABLE gl_dimension ADD CONSTRAINT FK__gl_dimens__idven__6E414E4F 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: good 
 */

ALTER TABLE good ADD CONSTRAINT FK__good__idproduct__7306036C 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE good ADD CONSTRAINT FK__good__iduom__7211DF33 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
go


/* 
 * TABLE: good_container 
 */

ALTER TABLE good_container ADD CONSTRAINT FK__good_cont__idcon__74EE4BDE 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE good_container ADD CONSTRAINT FK__good_cont__idpar__75E27017 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE good_container ADD CONSTRAINT FK__good_cont__idpro__73FA27A5 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: good_identification 
 */

ALTER TABLE good_identification ADD CONSTRAINT Refidentification_type2400 
    FOREIGN KEY (ididentificationtype)
    REFERENCES identification_type(ididentificationtype)
go

ALTER TABLE good_identification ADD CONSTRAINT Refproduct2401 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: good_promotion 
 */

ALTER TABLE good_promotion ADD CONSTRAINT FK__good_prom__idpro__76D69450 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: internal 
 */

ALTER TABLE internal ADD CONSTRAINT FK__internal__idpare__79B300FB 
    FOREIGN KEY (idparent)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal ADD CONSTRAINT FK__internal__idpart__77CAB889 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE internal ADD CONSTRAINT FK__internal__idrole__78BEDCC2 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE internal ADD CONSTRAINT FK__internal__idroot__7AA72534 
    FOREIGN KEY (idroot)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: internal_acctg_trans 
 */

ALTER TABLE internal_acctg_trans ADD CONSTRAINT Refacctg_transaction2402 
    FOREIGN KEY (idacctra)
    REFERENCES acctg_transaction(idacctra)
go

ALTER TABLE internal_acctg_trans ADD CONSTRAINT FK__internal___idint__7B9B496D 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: internal_facility_purpose 
 */

ALTER TABLE internal_facility_purpose ADD CONSTRAINT FK__internal___idfac__7D8391DF 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE internal_facility_purpose ADD CONSTRAINT FK__internal___idint__7E77B618 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal_facility_purpose ADD CONSTRAINT FK__internal___idpur__7F6BDA51 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: internal_order 
 */

ALTER TABLE internal_order ADD CONSTRAINT FK__internal___idint__015422C3 
    FOREIGN KEY (idintfro)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal_order ADD CONSTRAINT FK__internal___idint__024846FC 
    FOREIGN KEY (idintto)
    REFERENCES internal(idinternal)
go

ALTER TABLE internal_order ADD CONSTRAINT FK__internal___idord__005FFE8A 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go


/* 
 * TABLE: inventory_adjustment 
 */

ALTER TABLE inventory_adjustment ADD CONSTRAINT FK__inventory__idinv__033C6B35 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE inventory_adjustment ADD CONSTRAINT FK__inventory__idsto__04308F6E 
    FOREIGN KEY (idstopnite)
    REFERENCES stock_opname_item(idstopnite)
go


/* 
 * TABLE: inventory_item 
 */

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory___idfa__0ADD8CFD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
go

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idcon__08012052 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idfac__070CFC19 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idfea__09E968C4 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idown__0524B3A7 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idpro__0618D7E0 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE inventory_item ADD CONSTRAINT FK__inventory__idrec__08F5448B 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: inventory_item_status 
 */

ALTER TABLE inventory_item_status ADD CONSTRAINT FK__inventory__idinv__0BD1B136 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE inventory_item_status ADD CONSTRAINT FK__inventory__idrea__0DB9F9A8 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE inventory_item_status ADD CONSTRAINT FK__inventory__idsta__0CC5D56F 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: inventory_movement_status 
 */

ALTER TABLE inventory_movement_status ADD CONSTRAINT FK__inventory__idrea__10966653 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE inventory_movement_status ADD CONSTRAINT FK__inventory__idsli__0EAE1DE1 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

ALTER TABLE inventory_movement_status ADD CONSTRAINT FK__inventory__idsta__0FA2421A 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: inventory_request_item 
 */

ALTER TABLE inventory_request_item ADD CONSTRAINT FK__inventory__idinv__1372D2FE 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE inventory_request_item ADD CONSTRAINT FK__inventory__idreq__118A8A8C 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go

ALTER TABLE inventory_request_item ADD CONSTRAINT FK__inventory__idreq__127EAEC5 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: item_issuance 
 */

ALTER TABLE item_issuance ADD CONSTRAINT FK__item_issu__idinv__155B1B70 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE item_issuance ADD CONSTRAINT FK__item_issu__idshi__1466F737 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go

ALTER TABLE item_issuance ADD CONSTRAINT FK__item_issu__idsli__164F3FA9 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
go


/* 
 * TABLE: job 
 */

ALTER TABLE job ADD CONSTRAINT FK__job__idparrol__1837881B 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE job ADD CONSTRAINT FK__job__idpkb__174363E2 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go


/* 
 * TABLE: job_dispatch 
 */

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__iddis__1C0818FF 
    FOREIGN KEY (iddispatchstatus)
    REFERENCES dispatch_status(iddispatchstatus)
go

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__idjob__192BAC54 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__idpar__1A1FD08D 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE job_dispatch ADD CONSTRAINT FK__job_dispa__idpkb__1B13F4C6 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go


/* 
 * TABLE: job_dispatch_customer_type 
 */

ALTER TABLE job_dispatch_customer_type ADD CONSTRAINT FK__job_dispa__idcos__1DF06171 
    FOREIGN KEY (idcostumertype)
    REFERENCES customer_type(idcostumertype)
go

ALTER TABLE job_dispatch_customer_type ADD CONSTRAINT FK__job_dispa__idjob__1CFC3D38 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go


/* 
 * TABLE: job_dispatch_group_service 
 */

ALTER TABLE job_dispatch_group_service ADD CONSTRAINT FK__job_dispa__idjob__1EE485AA 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go


/* 
 * TABLE: job_dispatch_pkb_type 
 */

ALTER TABLE job_dispatch_pkb_type ADD CONSTRAINT FK__job_dispa__idjob__20CCCE1C 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go

ALTER TABLE job_dispatch_pkb_type ADD CONSTRAINT FK__job_dispa__idtyp__1FD8A9E3 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
go


/* 
 * TABLE: job_dispatch_service 
 */

ALTER TABLE job_dispatch_service ADD CONSTRAINT FK__job_dispa__idjob__22B5168E 
    FOREIGN KEY (idjobdispatch)
    REFERENCES job_dispatch(idjobdispatch)
go

ALTER TABLE job_dispatch_service ADD CONSTRAINT FK__job_dispa__idpro__21C0F255 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: job_history 
 */

ALTER TABLE job_history ADD CONSTRAINT FK__job_histo__idpar__23A93AC7 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE job_history ADD CONSTRAINT FK__job_histo__idpen__249D5F00 
    FOREIGN KEY (idpendingreason)
    REFERENCES pendingreason(idpendingreason)
go

ALTER TABLE job_history ADD CONSTRAINT FK__job_histo__idpkb__25918339 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
go


/* 
 * TABLE: job_priority 
 */

ALTER TABLE job_priority ADD CONSTRAINT FK__job_prior__idjob__2685A772 
    FOREIGN KEY (idjobprioritytype)
    REFERENCES job_priority_type(idjobprioritytype)
go


/* 
 * TABLE: job_priority_customer_type 
 */

ALTER TABLE job_priority_customer_type ADD CONSTRAINT FK__job_prior__idcos__286DEFE4 
    FOREIGN KEY (idcostumertype)
    REFERENCES customer_type(idcostumertype)
go

ALTER TABLE job_priority_customer_type ADD CONSTRAINT FK__job_prior__idjob__2779CBAB 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go


/* 
 * TABLE: job_priority_group_service 
 */

ALTER TABLE job_priority_group_service ADD CONSTRAINT FK__job_prior__idjob__2962141D 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go


/* 
 * TABLE: job_priority_pkb_type 
 */

ALTER TABLE job_priority_pkb_type ADD CONSTRAINT FK__job_prior__idjob__2B4A5C8F 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go

ALTER TABLE job_priority_pkb_type ADD CONSTRAINT FK__job_prior__idtyp__2A563856 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
go


/* 
 * TABLE: job_priority_service 
 */

ALTER TABLE job_priority_service ADD CONSTRAINT FK__job_prior__idjob__2D32A501 
    FOREIGN KEY (idjobpriority)
    REFERENCES job_priority(idjobpriority)
go

ALTER TABLE job_priority_service ADD CONSTRAINT FK__job_prior__idpro__2C3E80C8 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: job_service 
 */

ALTER TABLE job_service ADD CONSTRAINT FK__job_servi__idpkb__2F1AED73 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
go

ALTER TABLE job_service ADD CONSTRAINT FK__job_servi__idpro__2E26C93A 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: job_suggest 
 */

ALTER TABLE job_suggest ADD CONSTRAINT FK__job_sugge__idpro__300F11AC 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: job_suggest_service 
 */

ALTER TABLE job_suggest_service ADD CONSTRAINT FK__job_sugge__idjob__310335E5 
    FOREIGN KEY (idjobsuggest)
    REFERENCES job_suggest(idjobsuggest)
go

ALTER TABLE job_suggest_service ADD CONSTRAINT FK__job_sugge__idpro__31F75A1E 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: leasing_company 
 */

ALTER TABLE leasing_company ADD CONSTRAINT Refparty_role2404 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go


/* 
 * TABLE: leasing_tenor_provide 
 */

ALTER TABLE leasing_tenor_provide ADD CONSTRAINT FK__leasing_t__idlea__34D3C6C9 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
go

ALTER TABLE leasing_tenor_provide ADD CONSTRAINT FK__leasing_t__idpro__33DFA290 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go


/* 
 * TABLE: market_treatment 
 */

ALTER TABLE market_treatment ADD CONSTRAINT FK__market_tr__idbil__36BC0F3B 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE market_treatment ADD CONSTRAINT FK__market_tr__idven__35C7EB02 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: market_treatment_status 
 */

ALTER TABLE market_treatment_status ADD CONSTRAINT FK__market_tr__idmar__38A457AD 
    FOREIGN KEY (idmarkettreatment)
    REFERENCES market_treatment(idmarkettreatment)
go

ALTER TABLE market_treatment_status ADD CONSTRAINT FK__market_tr__idsta__37B03374 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: mechanic 
 */

ALTER TABLE mechanic ADD CONSTRAINT FK__mechanic__idparr__39987BE6 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE mechanic ADD CONSTRAINT FK__mechanic__idvend__3A8CA01F 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: memo 
 */

ALTER TABLE memo ADD CONSTRAINT FK__memo__idbilling__3B80C458 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE memo ADD CONSTRAINT FK__memo__idinternal__3F51553C 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE memo ADD CONSTRAINT FK__memo__idinvite__3D690CCA 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE memo ADD CONSTRAINT FK__memo__idmemotype__3C74E891 
    FOREIGN KEY (idmemotype)
    REFERENCES memo_type(idmemotype)
go

ALTER TABLE memo ADD CONSTRAINT FK__memo__idoldinvit__3E5D3103 
    FOREIGN KEY (idoldinvite)
    REFERENCES inventory_item(idinvite)
go


/* 
 * TABLE: memo_status 
 */

ALTER TABLE memo_status ADD CONSTRAINT FK__memo_stat__idmem__422DC1E7 
    FOREIGN KEY (idmemo)
    REFERENCES memo(idmemo)
go

ALTER TABLE memo_status ADD CONSTRAINT FK__memo_stat__idrea__41399DAE 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE memo_status ADD CONSTRAINT FK__memo_stat__idsta__40457975 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: motor 
 */

ALTER TABLE motor ADD CONSTRAINT FK__motor__idproduct__4321E620 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: motor_due_reminder 
 */

ALTER TABLE motor_due_reminder ADD CONSTRAINT FK__motor_due__idmot__44160A59 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
go


/* 
 * TABLE: moving_slip 
 */

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idcon__46F27704 
    FOREIGN KEY (idcontainerfrom)
    REFERENCES container(idcontainer)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idcon__47E69B3D 
    FOREIGN KEY (idcontainerto)
    REFERENCES container(idcontainer)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idfac__49CEE3AF 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idfac__4AC307E8 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idinv__45FE52CB 
    FOREIGN KEY (idinviteto)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idinv__4BB72C21 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idpro__48DABF76 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idreq__4CAB505A 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go

ALTER TABLE moving_slip ADD CONSTRAINT FK__moving_sl__idsli__450A2E92 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go


/* 
 * TABLE: order_billing_item 
 */

ALTER TABLE order_billing_item ADD CONSTRAINT FK__order_bil__idbil__4E9398CC 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE order_billing_item ADD CONSTRAINT FK__order_bil__idord__4D9F7493 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: order_demand 
 */

ALTER TABLE order_demand ADD CONSTRAINT FK__order_dem__idord__507BE13E 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
go

ALTER TABLE order_demand ADD CONSTRAINT FK__order_dem__idpro__4F87BD05 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: order_item 
 */

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idfea__544C7222 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idord__51700577 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idpar__5540965B 
    FOREIGN KEY (idparordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idpro__526429B0 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE order_item ADD CONSTRAINT FK__order_ite__idshi__53584DE9 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go


/* 
 * TABLE: order_payment 
 */

ALTER TABLE order_payment ADD CONSTRAINT FK__order_pay__idord__5634BA94 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE order_payment ADD CONSTRAINT FK__order_pay__idpay__5728DECD 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: order_request_item 
 */

ALTER TABLE order_request_item ADD CONSTRAINT FK__order_req__idord__581D0306 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE order_request_item ADD CONSTRAINT FK__order_req__idreq__5911273F 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: order_shipment_item 
 */

ALTER TABLE order_shipment_item ADD CONSTRAINT FK__order_shi__idord__5CE1B823 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE order_shipment_item ADD CONSTRAINT FK__order_shi__idshi__5BED93EA 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go


/* 
 * TABLE: orders 
 */

ALTER TABLE orders ADD CONSTRAINT FK__orders__idordtyp__5DD5DC5C 
    FOREIGN KEY (idordtyp)
    REFERENCES order_type(idordtyp)
go


/* 
 * TABLE: orders_role 
 */

ALTER TABLE orders_role ADD CONSTRAINT FK__orders_ro__idord__5ECA0095 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE orders_role ADD CONSTRAINT FK__orders_ro__idpar__60B24907 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE orders_role ADD CONSTRAINT FK__orders_ro__idrol__5FBE24CE 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: orders_status 
 */

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idord__61A66D40 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idord__6482D9EB 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idrea__638EB5B2 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE orders_status ADD CONSTRAINT FK__orders_st__idsta__629A9179 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: organization 
 */

ALTER TABLE organization ADD CONSTRAINT FK__organizat__idpar__6576FE24 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE organization ADD CONSTRAINT FK__organizat__idpic__666B225D 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
go

ALTER TABLE organization ADD CONSTRAINT FK__organizat__idpos__675F4696 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
go


/* 
 * TABLE: organization_customer 
 */

ALTER TABLE organization_customer ADD CONSTRAINT FK__organizat__idcus__68536ACF 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: organization_gl_account 
 */

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idbil__6C23FBB3 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idcat__6E0C4425 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
go

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idgla__6F00685E 
    FOREIGN KEY (idglacc)
    REFERENCES gl_account(idglacc)
go

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idint__6A3BB341 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idpar__69478F08 
    FOREIGN KEY (idparent)
    REFERENCES organization_gl_account(idorgglacc)
go

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idpro__6D181FEC 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE organization_gl_account ADD CONSTRAINT FK__organizat__idven__6B2FD77A 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: organization_prospect 
 */

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpar__70E8B0D0 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpic__71DCD509 
    FOREIGN KEY (idpic)
    REFERENCES person(idparty)
go

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpos__72D0F942 
    FOREIGN KEY (idposaddtdp)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE organization_prospect ADD CONSTRAINT FK__organizat__idpro__6FF48C97 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go


/* 
 * TABLE: organization_prospect_detail 
 */

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idfea__75AD65ED 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idper__73C51D7B 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
go

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idpro__74B941B4 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE organization_prospect_detail ADD CONSTRAINT FK__organizat__idpro__76A18A26 
    FOREIGN KEY (idprospect)
    REFERENCES organization_prospect(idprospect)
go


/* 
 * TABLE: organization_suspect 
 */

ALTER TABLE organization_suspect ADD CONSTRAINT FK__organizat__idpar__7795AE5F 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE organization_suspect ADD CONSTRAINT FK__organizat__idsus__7889D298 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: package_receipt 
 */

ALTER TABLE package_receipt ADD CONSTRAINT FK__package_r__idpac__7A721B0A 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE package_receipt ADD CONSTRAINT FK__package_r__idshi__797DF6D1 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
go


/* 
 * TABLE: packaging_content 
 */

ALTER TABLE packaging_content ADD CONSTRAINT FK__packaging__idpac__7C5A637C 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE packaging_content ADD CONSTRAINT FK__packaging__idshi__7B663F43 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go


/* 
 * TABLE: parent_organization 
 */

ALTER TABLE parent_organization ADD CONSTRAINT FK__parent_or__idint__7D4E87B5 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: part_history 
 */

ALTER TABLE part_history ADD CONSTRAINT FK__part_hist__idser__7F36D027 
    FOREIGN KEY (idservicehistory)
    REFERENCES service_history(idservicehistory)
go

ALTER TABLE part_history ADD CONSTRAINT FK__part_hist__idveh__7E42ABEE 
    FOREIGN KEY (idvehicleservicehistory)
    REFERENCES vehicle_service_history(idvehicleservicehistory)
go


/* 
 * TABLE: part_issue 
 */

ALTER TABLE part_issue ADD CONSTRAINT FK__part_issu__idpar__002AF460 
    FOREIGN KEY (idpartissuetype)
    REFERENCES part_issue_type(idpartissuetype)
go


/* 
 * TABLE: part_issue_action 
 */

ALTER TABLE part_issue_action ADD CONSTRAINT FK__part_issu__idiss__011F1899 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go


/* 
 * TABLE: part_issue_item 
 */

ALTER TABLE part_issue_item ADD CONSTRAINT FK__part_issu__idiss__03FB8544 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go

ALTER TABLE part_issue_item ADD CONSTRAINT FK__part_issu__idpro__02133CD2 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE part_issue_item ADD CONSTRAINT FK__part_issu__idsta__0307610B 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: part_issue_status 
 */

ALTER TABLE part_issue_status ADD CONSTRAINT FK__part_issu__idiss__05E3CDB6 
    FOREIGN KEY (idissue)
    REFERENCES part_issue(idissue)
go

ALTER TABLE part_issue_status ADD CONSTRAINT FK__part_issu__idsta__04EFA97D 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: part_sales_order 
 */

ALTER TABLE part_sales_order ADD CONSTRAINT FK__part_sale__idord__06D7F1EF 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
go


/* 
 * TABLE: party_category 
 */

ALTER TABLE party_category ADD CONSTRAINT FK__party_cat__idcat__07CC1628 
    FOREIGN KEY (idcattyp)
    REFERENCES party_category_type(idcattyp)
go


/* 
 * TABLE: party_category_impl 
 */

ALTER TABLE party_category_impl ADD CONSTRAINT Refparty_category3067 
    FOREIGN KEY (idcategory)
    REFERENCES party_category(idcategory)
go

ALTER TABLE party_category_impl ADD CONSTRAINT Refparty3068 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_category_type 
 */

ALTER TABLE party_category_type ADD CONSTRAINT FK__party_cat__idpar__0B9CA70C 
    FOREIGN KEY (idparent)
    REFERENCES party_category_type(idcattyp)
go

ALTER TABLE party_category_type ADD CONSTRAINT FK__party_cat__idrul__0AA882D3 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: party_classification 
 */

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idcat__0C90CB45 
    FOREIGN KEY (idcategory)
    REFERENCES party_category(idcategory)
go

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idcat__0D84EF7E 
    FOREIGN KEY (idcattyp)
    REFERENCES party_category_type(idcattyp)
go

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idown__0E7913B7 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE party_classification ADD CONSTRAINT FK__party_cla__idpar__0F6D37F0 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_contact 
 */

ALTER TABLE party_contact ADD CONSTRAINT FK__party_con__idcon__10615C29 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE party_contact ADD CONSTRAINT FK__party_con__idpar__11558062 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_contact_mechanism 
 */

ALTER TABLE party_contact_mechanism ADD CONSTRAINT FK__party_con__idcon__1249A49B 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE party_contact_mechanism ADD CONSTRAINT FK__party_con__idpar__133DC8D4 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_contact_mechanism ADD CONSTRAINT FK__party_con__idpur__1431ED0D 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: party_document 
 */

ALTER TABLE party_document ADD CONSTRAINT FK__party_doc__iddoc__15261146 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
go

ALTER TABLE party_document ADD CONSTRAINT FK__party_doc__idpar__161A357F 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: party_facility_purpose 
 */

ALTER TABLE party_facility_purpose ADD CONSTRAINT FK__party_fac__idfac__18027DF1 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE party_facility_purpose ADD CONSTRAINT FK__party_fac__idpar__18F6A22A 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_facility_purpose ADD CONSTRAINT FK__party_fac__idpur__170E59B8 
    FOREIGN KEY (idpurposetype)
    REFERENCES purpose_type(idpurposetype)
go


/* 
 * TABLE: party_relationship 
 */

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idpar__1BD30ED5 
    FOREIGN KEY (idparrolto)
    REFERENCES party_role(idparrol)
go

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idpar__1CC7330E 
    FOREIGN KEY (idparrolfro)
    REFERENCES party_role(idparrol)
go

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idrel__19EAC663 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
go

ALTER TABLE party_relationship ADD CONSTRAINT FK__party_rel__idsta__1ADEEA9C 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: party_role 
 */

ALTER TABLE party_role ADD CONSTRAINT FK__party_rol__idpar__1DBB5747 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_role ADD CONSTRAINT FK__party_rol__idrol__1EAF7B80 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: party_role_status 
 */

ALTER TABLE party_role_status ADD CONSTRAINT FK__party_rol__idpar__1FA39FB9 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go

ALTER TABLE party_role_status ADD CONSTRAINT FK__party_rol__idrea__218BE82B 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE party_role_status ADD CONSTRAINT FK__party_rol__idsta__2097C3F2 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: party_role_type 
 */

ALTER TABLE party_role_type ADD CONSTRAINT FK__party_rol__idpar__2374309D 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE party_role_type ADD CONSTRAINT FK__party_rol__idrol__22800C64 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: payment 
 */

ALTER TABLE payment ADD CONSTRAINT FK__payment__idinter__26509D48 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpaifr__2838E5BA 
    FOREIGN KEY (idpaifro)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpaito__2744C181 
    FOREIGN KEY (idpaito)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpayme__255C790F 
    FOREIGN KEY (idpaymet)
    REFERENCES payment_method(idpaymet)
go

ALTER TABLE payment ADD CONSTRAINT FK__payment__idpayty__246854D6 
    FOREIGN KEY (idpaytyp)
    REFERENCES payment_type(idpaytyp)
go


/* 
 * TABLE: payment_acctg_trans 
 */

ALTER TABLE payment_acctg_trans ADD CONSTRAINT FK__payment_a__idacc__2A212E2C 
    FOREIGN KEY (idacctra)
    REFERENCES external_acctg_trans(idacctra)
go

ALTER TABLE payment_acctg_trans ADD CONSTRAINT FK__payment_a__idpay__292D09F3 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: payment_application 
 */

ALTER TABLE payment_application ADD CONSTRAINT FK__payment_a__idbil__2C09769E 
    FOREIGN KEY (idbilacc)
    REFERENCES billing_account(idbilacc)
go

ALTER TABLE payment_application ADD CONSTRAINT FK__payment_a__idbil__2CFD9AD7 
    FOREIGN KEY (idbilling)
    REFERENCES billing(idbilling)
go

ALTER TABLE payment_application ADD CONSTRAINT FK__payment_a__idpay__2B155265 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go


/* 
 * TABLE: payment_method 
 */

ALTER TABLE payment_method ADD CONSTRAINT FK__payment_m__idpay__2DF1BF10 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
go


/* 
 * TABLE: payment_role 
 */

ALTER TABLE payment_role ADD CONSTRAINT FK__payment_r__idpar__30CE2BBB 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE payment_role ADD CONSTRAINT FK__payment_r__idpay__2EE5E349 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE payment_role ADD CONSTRAINT FK__payment_r__idrol__2FDA0782 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: payment_status 
 */

ALTER TABLE payment_status ADD CONSTRAINT FK__payment_s__idpay__31C24FF4 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE payment_status ADD CONSTRAINT FK__payment_s__idrea__33AA9866 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE payment_status ADD CONSTRAINT FK__payment_s__idsta__32B6742D 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: person 
 */

ALTER TABLE person ADD CONSTRAINT FK__person__idparty__349EBC9F 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go


/* 
 * TABLE: person_prospect 
 */

ALTER TABLE person_prospect ADD CONSTRAINT FK__person_pr__idpar__36870511 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE person_prospect ADD CONSTRAINT FK__person_pr__idpro__3592E0D8 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go


/* 
 * TABLE: person_suspect 
 */

ALTER TABLE person_suspect ADD CONSTRAINT FK__person_su__idpar__377B294A 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE person_suspect ADD CONSTRAINT FK__person_su__idsus__386F4D83 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: personal_customer 
 */

ALTER TABLE personal_customer ADD CONSTRAINT FK__personal___idcus__396371BC 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go


/* 
 * TABLE: personsosmed 
 */

ALTER TABLE personsosmed ADD CONSTRAINT FK__personsos__idpar__3B4BBA2E 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE personsosmed ADD CONSTRAINT FK__personsos__idsos__3A5795F5 
    FOREIGN KEY (idsosmedtype)
    REFERENCES sosmedtype(idsosmedtype)
go


/* 
 * TABLE: picking_slip 
 */

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idinv__3D3402A0 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idord__3F1C4B12 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idshi__3C3FDE67 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE picking_slip ADD CONSTRAINT FK__picking_s__idsli__3E2826D9 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go


/* 
 * TABLE: pkb 
 */

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idcustomer__41F8B7BD 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idinternal__40106F4B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idpaymettyp__42ECDBF6 
    FOREIGN KEY (idpaymettyp)
    REFERENCES payment_method_type(idpaymettyp)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idpkbjobret__48A5B54C 
    FOREIGN KEY (idpkbjobreturn)
    REFERENCES pkb(idpkb)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idqueue__43E1002F 
    FOREIGN KEY (idqueue)
    REFERENCES queue(idqueue)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idstatuspkb__44D52468 
    FOREIGN KEY (idstatuspkbcust)
    REFERENCES status_pkb(idstatuspkb)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idstatuspkb__47B19113 
    FOREIGN KEY (idstatuspkbinternal)
    REFERENCES status_pkb(idstatuspkb)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idtypepkb__45C948A1 
    FOREIGN KEY (idtypepkb)
    REFERENCES type_pkb(idtypepkb)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idvehicle__46BD6CDA 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE pkb ADD CONSTRAINT FK__pkb__idvehide__41049384 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go


/* 
 * TABLE: pkb_estimation 
 */

ALTER TABLE pkb_estimation ADD CONSTRAINT FK__pkb_estim__idpkb__4999D985 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go


/* 
 * TABLE: pkb_part 
 */

ALTER TABLE pkb_part ADD CONSTRAINT FK__pkb_part__idchar__4A8DFDBE 
    FOREIGN KEY (idchargeto)
    REFERENCES charge_to_type(idchargeto)
go

ALTER TABLE pkb_part ADD CONSTRAINT FK__pkb_part__idpkb__4B8221F7 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go

ALTER TABLE pkb_part ADD CONSTRAINT FK__pkb_part__idpkbs__4C764630 
    FOREIGN KEY (idpkbservice)
    REFERENCES pkb_service(IdPkbService)
go


/* 
 * TABLE: pkb_part_billing_item 
 */

ALTER TABLE pkb_part_billing_item ADD CONSTRAINT FK__pkb_part___idbil__4E5E8EA2 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE pkb_part_billing_item ADD CONSTRAINT FK__pkb_part___idpar__4D6A6A69 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go


/* 
 * TABLE: pkb_part_request_item 
 */

ALTER TABLE pkb_part_request_item ADD CONSTRAINT FK__pkb_part___idreq__4F52B2DB 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go


/* 
 * TABLE: pkb_service 
 */

ALTER TABLE pkb_service ADD CONSTRAINT FK__pkb_servi__idcha__5046D714 
    FOREIGN KEY (idchargeto)
    REFERENCES charge_to_type(idchargeto)
go

ALTER TABLE pkb_service ADD CONSTRAINT FK__pkb_servi__idpkb__522F1F86 
    FOREIGN KEY (idpkb)
    REFERENCES pkb(idpkb)
go

ALTER TABLE pkb_service ADD CONSTRAINT FK__pkb_servi__idpro__513AFB4D 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: pkb_service_billing_item 
 */

ALTER TABLE pkb_service_billing_item ADD CONSTRAINT FK__pkb_servi__idbil__541767F8 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE pkb_service_billing_item ADD CONSTRAINT FK__pkb_servi__idpar__532343BF 
    FOREIGN KEY (idparrol)
    REFERENCES mechanic(idparrol)
go


/* 
 * TABLE: po_detail_part 
 */

ALTER TABLE po_detail_part ADD CONSTRAINT FK__po_detail__idord__550B8C31 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: po_detail_service 
 */

ALTER TABLE po_detail_service ADD CONSTRAINT FK__po_detail__idord__55FFB06A 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go


/* 
 * TABLE: po_status 
 */

ALTER TABLE po_status ADD CONSTRAINT FK__po_status__idsta__56F3D4A3 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: position_authority 
 */

ALTER TABLE position_authority ADD CONSTRAINT FK__position___idpos__57E7F8DC 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
go


/* 
 * TABLE: position_fullfillment 
 */

ALTER TABLE position_fullfillment ADD CONSTRAINT FK__position___idper__59D0414E 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
go

ALTER TABLE position_fullfillment ADD CONSTRAINT FK__position___idpos__58DC1D15 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
go


/* 
 * TABLE: position_reporting_structure 
 */

ALTER TABLE position_reporting_structure ADD CONSTRAINT FK__position___idpos__5AC46587 
    FOREIGN KEY (idposfro)
    REFERENCES positions(idposition)
go

ALTER TABLE position_reporting_structure ADD CONSTRAINT FK__position___idpos__5BB889C0 
    FOREIGN KEY (idposto)
    REFERENCES positions(idposition)
go


/* 
 * TABLE: positions 
 */

ALTER TABLE positions ADD CONSTRAINT FK__positions__idint__5DA0D232 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE positions ADD CONSTRAINT FK__positions__idorg__5E94F66B 
    FOREIGN KEY (idorganization)
    REFERENCES organization(idparty)
go

ALTER TABLE positions ADD CONSTRAINT FK__positions__idpos__5CACADF9 
    FOREIGN KEY (idpostyp)
    REFERENCES position_type(idpostyp)
go

ALTER TABLE positions ADD CONSTRAINT FK__positions__idusr__5F891AA4 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
go


/* 
 * TABLE: positions_status 
 */

ALTER TABLE positions_status ADD CONSTRAINT FK__positions__idpos__607D3EDD 
    FOREIGN KEY (idposition)
    REFERENCES positions(idposition)
go

ALTER TABLE positions_status ADD CONSTRAINT FK__positions__idrea__6265874F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE positions_status ADD CONSTRAINT FK__positions__idsta__61716316 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: postal_address 
 */

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idcit__6541F3FA 
    FOREIGN KEY (idcity)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idcon__6359AB88 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__iddis__66361833 
    FOREIGN KEY (iddistrict)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idpos__672A3C6C 
    FOREIGN KEY (idpostal)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idpro__644DCFC1 
    FOREIGN KEY (idprovince)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE postal_address ADD CONSTRAINT FK__postal_ad__idvil__681E60A5 
    FOREIGN KEY (idvillage)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: price_agreement_item 
 */

ALTER TABLE price_agreement_item ADD CONSTRAINT FK__price_agr__idagr__6A06A917 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
go

ALTER TABLE price_agreement_item ADD CONSTRAINT FK__price_agr__idpro__691284DE 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: price_component 
 */

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idagr__6ECB5E34 
    FOREIGN KEY (idagrite)
    REFERENCES agreement_item(idagrite)
go

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idgeo__6DD739FB 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpar__6AFACD50 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpar__6FBF826D 
    FOREIGN KEY (idparcat)
    REFERENCES party_category(idcategory)
go

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpri__6CE315C2 
    FOREIGN KEY (idpricetype)
    REFERENCES price_type(idpricetype)
go

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpro__6BEEF189 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE price_component ADD CONSTRAINT FK__price_com__idpro__70B3A6A6 
    FOREIGN KEY (idprocat)
    REFERENCES product_category(idcategory)
go


/* 
 * TABLE: price_type 
 */

ALTER TABLE price_type ADD CONSTRAINT FK__price_typ__idrul__71A7CADF 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: product 
 */

ALTER TABLE product ADD CONSTRAINT FK__product__idproty__729BEF18 
    FOREIGN KEY (idprotyp)
    REFERENCES product_type(idprotyp)
go


/* 
 * TABLE: product_association 
 */

ALTER TABLE product_association ADD CONSTRAINT FK__product_a__idass__75785BC3 
    FOREIGN KEY (idasstyp)
    REFERENCES association_type(idasstyp)
go

ALTER TABLE product_association ADD CONSTRAINT FK__product_a__idpro__73901351 
    FOREIGN KEY (idproductfrom)
    REFERENCES product(idproduct)
go

ALTER TABLE product_association ADD CONSTRAINT FK__product_a__idpro__7484378A 
    FOREIGN KEY (idproductto)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_category 
 */

ALTER TABLE product_category ADD CONSTRAINT FK__product_c__idcat__766C7FFC 
    FOREIGN KEY (idcattyp)
    REFERENCES product_category_type(idcattyp)
go


/* 
 * TABLE: product_category_impl 
 */

ALTER TABLE product_category_impl ADD CONSTRAINT Refproduct_category3069 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
go

ALTER TABLE product_category_impl ADD CONSTRAINT Refproduct3070 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_category_type 
 */

ALTER TABLE product_category_type ADD CONSTRAINT FK__product_c__idpar__7A3D10E0 
    FOREIGN KEY (idparent)
    REFERENCES product_category_type(idcattyp)
go

ALTER TABLE product_category_type ADD CONSTRAINT FK__product_c__idrul__7948ECA7 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: product_classification 
 */

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idcat__7B313519 
    FOREIGN KEY (idcategory)
    REFERENCES product_category(idcategory)
go

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idcat__7C255952 
    FOREIGN KEY (idcattyp)
    REFERENCES product_category_type(idcattyp)
go

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idown__7D197D8B 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE product_classification ADD CONSTRAINT FK__product_c__idpro__7E0DA1C4 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_document 
 */

ALTER TABLE product_document ADD CONSTRAINT FK__product_d__iddoc__7F01C5FD 
    FOREIGN KEY (iddocument)
    REFERENCES documents(iddocument)
go

ALTER TABLE product_document ADD CONSTRAINT FK__product_d__idpro__7FF5EA36 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_feature_impl 
 */

ALTER TABLE product_feature_impl ADD CONSTRAINT Reffeature3071 
    FOREIGN KEY (features_id)
    REFERENCES feature(idfeature)
go

ALTER TABLE product_feature_impl ADD CONSTRAINT Refproduct3072 
    FOREIGN KEY (products_id)
    REFERENCES product(idproduct)
go


/* 
 * TABLE: product_package_receipt 
 */

ALTER TABLE product_package_receipt ADD CONSTRAINT FK__product_p__idpac__03C67B1A 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE product_package_receipt ADD CONSTRAINT FK__product_p__idshi__02D256E1 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
go


/* 
 * TABLE: product_purchase_order 
 */

ALTER TABLE product_purchase_order ADD CONSTRAINT FK__product_p__idord__04BA9F53 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
go


/* 
 * TABLE: product_shipment_incoming 
 */

ALTER TABLE product_shipment_incoming ADD CONSTRAINT FK__product_s__idshi__05AEC38C 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go


/* 
 * TABLE: product_shipment_receipt 
 */

ALTER TABLE product_shipment_receipt ADD CONSTRAINT FK__product_s__idrec__06A2E7C5 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: prospect 
 */

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idbrok__097F5470 
    FOREIGN KEY (idbroker)
    REFERENCES sales_broker(idparrol)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idevet__0A7378A9 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idfaci__0B679CE2 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idinte__07970BFE 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idpros__088B3037 
    FOREIGN KEY (idprosou)
    REFERENCES prospect_source(idprosou)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idsale__0C5BC11B 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idsale__0E44098D 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
go

ALTER TABLE prospect ADD CONSTRAINT FK__prospect__idsusp__0D4FE554 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: prospect_role 
 */

ALTER TABLE prospect_role ADD CONSTRAINT FK__prospect___idpar__11207638 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE prospect_role ADD CONSTRAINT FK__prospect___idpro__0F382DC6 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE prospect_role ADD CONSTRAINT FK__prospect___idrol__102C51FF 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: prospect_status 
 */

ALTER TABLE prospect_status ADD CONSTRAINT FK__prospect___idpro__12149A71 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE prospect_status ADD CONSTRAINT FK__prospect___idrea__13FCE2E3 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE prospect_status ADD CONSTRAINT FK__prospect___idsta__1308BEAA 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: province 
 */

ALTER TABLE province ADD CONSTRAINT Refgeo_boundary2408 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: purchase_order 
 */

ALTER TABLE purchase_order ADD CONSTRAINT FK__purchase___idord__15E52B55 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
go


/* 
 * TABLE: purchase_request 
 */

ALTER TABLE purchase_request ADD CONSTRAINT FK__purchase___idint__17CD73C7 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE purchase_request ADD CONSTRAINT FK__purchase___idpro__16D94F8E 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE purchase_request ADD CONSTRAINT FK__purchase___idreq__18C19800 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: purchaseorder_reception_tr 
 */

ALTER TABLE purchaseorder_reception_tr ADD CONSTRAINT FK__purchaseo__idcat__1AA9E072 
    FOREIGN KEY (idcategoryasset)
    REFERENCES category_asset_tp(idcategoryasset)
go

ALTER TABLE purchaseorder_reception_tr ADD CONSTRAINT FK__purchaseo__idpur__19B5BC39 
    FOREIGN KEY (idpurchaseorder)
    REFERENCES purchaseorder_tm(idpurchaseorder)
go


/* 
 * TABLE: purchaseorder_tm 
 */

ALTER TABLE purchaseorder_tm ADD CONSTRAINT FK__purchaseo__statu__1B9E04AB 
    FOREIGN KEY (status)
    REFERENCES purchaseorder_status_tp(idpurchaseorderstatus)
go


/* 
 * TABLE: queue 
 */

ALTER TABLE queue ADD CONSTRAINT FK__queue__idbooking__1E7A7156 
    FOREIGN KEY (idbooking)
    REFERENCES booking(idbooking)
go

ALTER TABLE queue ADD CONSTRAINT FK__queue__idqueuest__1C9228E4 
    FOREIGN KEY (idqueuestatus)
    REFERENCES queue_status(idqueuestatus)
go

ALTER TABLE queue ADD CONSTRAINT FK__queue__idqueuety__1D864D1D 
    FOREIGN KEY (idqueuetype)
    REFERENCES queue_type(idqueuetype)
go


/* 
 * TABLE: quote 
 */

ALTER TABLE quote ADD CONSTRAINT Refquote_type2409 
    FOREIGN KEY (idquotyp)
    REFERENCES quote_type(idquotyp)
go


/* 
 * TABLE: quote_item 
 */

ALTER TABLE quote_item ADD CONSTRAINT FK__quote_ite__idpro__1F6E958F 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE quote_item ADD CONSTRAINT FK__quote_ite__idquo__2062B9C8 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go


/* 
 * TABLE: quote_role 
 */

ALTER TABLE quote_role ADD CONSTRAINT FK__quote_rol__idpar__233F2673 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE quote_role ADD CONSTRAINT FK__quote_rol__idquo__224B023A 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go

ALTER TABLE quote_role ADD CONSTRAINT FK__quote_rol__idrol__2156DE01 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: quote_status 
 */

ALTER TABLE quote_status ADD CONSTRAINT FK__quote_sta__idquo__261B931E 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go

ALTER TABLE quote_status ADD CONSTRAINT FK__quote_sta__idrea__25276EE5 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE quote_status ADD CONSTRAINT FK__quote_sta__idsta__24334AAC 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: receipt 
 */

ALTER TABLE receipt ADD CONSTRAINT FK__receipt__idpayme__270FB757 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE receipt ADD CONSTRAINT FK__receipt__idreq__2803DB90 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: regular_sales_order 
 */

ALTER TABLE regular_sales_order ADD CONSTRAINT FK__regular_s__idord__28F7FFC9 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
go


/* 
 * TABLE: reimbursement_part 
 */

ALTER TABLE reimbursement_part ADD CONSTRAINT FK__reimburse__idpro__2BD46C74 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE reimbursement_part ADD CONSTRAINT FK__reimburse__idspg__29EC2402 
    FOREIGN KEY (idspgclaimdetail)
    REFERENCES spg_claim_detail(idspgclaimdetail)
go

ALTER TABLE reimbursement_part ADD CONSTRAINT FK__reimburse__idspg__2AE0483B 
    FOREIGN KEY (idspgclaimaction)
    REFERENCES spg_claim_action(idspgclaimaction)
go


/* 
 * TABLE: rem_part 
 */

ALTER TABLE rem_part ADD CONSTRAINT FK__rem_part__idprod__2CC890AD 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: reminder_sent_status 
 */

ALTER TABLE reminder_sent_status ADD CONSTRAINT FK__reminder___idrem__2DBCB4E6 
    FOREIGN KEY (idreminder)
    REFERENCES service_reminder(idreminder)
go


/* 
 * TABLE: reorder_guideline 
 */

ALTER TABLE reorder_guideline ADD CONSTRAINT Refinternal2410 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE reorder_guideline ADD CONSTRAINT Refgood2411 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: request 
 */

ALTER TABLE request ADD CONSTRAINT FK__request__idinter__2EB0D91F 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE request ADD CONSTRAINT FK__request__idreqty__2FA4FD58 
    FOREIGN KEY (idreqtype)
    REFERENCES request_type(idreqtype)
go


/* 
 * TABLE: request_item 
 */

ALTER TABLE request_item ADD CONSTRAINT FK__request_i__idfea__32816A03 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE request_item ADD CONSTRAINT FK__request_i__idpro__30992191 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE request_item ADD CONSTRAINT FK__request_i__idreq__318D45CA 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_product 
 */

ALTER TABLE request_product ADD CONSTRAINT FK__request_p__idfac__33758E3C 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
go

ALTER TABLE request_product ADD CONSTRAINT FK__request_p__idfac__3469B275 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
go

ALTER TABLE request_product ADD CONSTRAINT FK__request_p__idreq__355DD6AE 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_requirement 
 */

ALTER TABLE request_requirement ADD CONSTRAINT FK__request_r__idreq__3651FAE7 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE request_requirement ADD CONSTRAINT FK__request_r__idreq__37461F20 
    FOREIGN KEY (idrequirement)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: request_role 
 */

ALTER TABLE request_role ADD CONSTRAINT FK__request_r__idpar__392E6792 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE request_role ADD CONSTRAINT FK__request_r__idreq__3A228BCB 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE request_role ADD CONSTRAINT FK__request_r__idrol__383A4359 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: request_status 
 */

ALTER TABLE request_status ADD CONSTRAINT FK__request_s__idrea__3B16B004 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE request_status ADD CONSTRAINT FK__request_s__idreq__3CFEF876 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE request_status ADD CONSTRAINT FK__request_s__idsta__3C0AD43D 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: request_unit_internal 
 */

ALTER TABLE request_unit_internal ADD CONSTRAINT FK__request_u__idreq__3DF31CAF 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: requirement 
 */

ALTER TABLE requirement ADD CONSTRAINT FK__requireme__idfac__3EE740E8 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE requirement ADD CONSTRAINT FK__requireme__idpar__3FDB6521 
    FOREIGN KEY (idparentreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement ADD CONSTRAINT FK__requireme__idreq__40CF895A 
    FOREIGN KEY (idreqtyp)
    REFERENCES requirement_type(idreqtyp)
go


/* 
 * TABLE: requirement_order_item 
 */

ALTER TABLE requirement_order_item ADD CONSTRAINT FK__requireme__idord__41C3AD93 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE requirement_order_item ADD CONSTRAINT FK__requireme__idreq__42B7D1CC 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: requirement_payment 
 */

ALTER TABLE requirement_payment ADD CONSTRAINT FK__requireme__idpay__44A01A3E 
    FOREIGN KEY (idpayment)
    REFERENCES payment(idpayment)
go

ALTER TABLE requirement_payment ADD CONSTRAINT FK__requireme__idreq__43ABF605 
    FOREIGN KEY (requirements_id)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: requirement_role 
 */

ALTER TABLE requirement_role ADD CONSTRAINT FK__requireme__idpar__477C86E9 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE requirement_role ADD CONSTRAINT FK__requireme__idreq__45943E77 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement_role ADD CONSTRAINT FK__requireme__idrol__468862B0 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: requirement_status 
 */

ALTER TABLE requirement_status ADD CONSTRAINT FK__requireme__idrea__4A58F394 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE requirement_status ADD CONSTRAINT FK__requireme__idreq__4870AB22 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE requirement_status ADD CONSTRAINT FK__requireme__idsta__4964CF5B 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: return_purchase_order 
 */

ALTER TABLE return_purchase_order ADD CONSTRAINT Refvendor_order2412 
    FOREIGN KEY (idorder)
    REFERENCES vendor_order(idorder)
go


/* 
 * TABLE: return_sales_order 
 */

ALTER TABLE return_sales_order ADD CONSTRAINT Refcustomer_order2413 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
go


/* 
 * TABLE: room_tm 
 */

ALTER TABLE room_tm ADD CONSTRAINT FK__room_tm__idfloor__4D35603F 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
go


/* 
 * TABLE: rule_hot_item 
 */

ALTER TABLE rule_hot_item ADD CONSTRAINT FK__rule_hot___idpro__4F1DA8B1 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE rule_hot_item ADD CONSTRAINT FK__rule_hot___idrul__4E298478 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
go


/* 
 * TABLE: rule_indent 
 */

ALTER TABLE rule_indent ADD CONSTRAINT FK__rule_inde__idpro__5105F123 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE rule_indent ADD CONSTRAINT FK__rule_inde__idrul__5011CCEA 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
go


/* 
 * TABLE: rule_sales_discount 
 */

ALTER TABLE rule_sales_discount ADD CONSTRAINT FK__rule_sale__idrul__51FA155C 
    FOREIGN KEY (idrule)
    REFERENCES rules(idrule)
go


/* 
 * TABLE: rules 
 */

ALTER TABLE rules ADD CONSTRAINT FK__rules__idinterna__53E25DCE 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE rules ADD CONSTRAINT FK__rules__idrultyp__52EE3995 
    FOREIGN KEY (idrultyp)
    REFERENCES rule_type(idrultyp)
go


/* 
 * TABLE: sale_type 
 */

ALTER TABLE sale_type ADD CONSTRAINT FK__sale_type__idpar__54D68207 
    FOREIGN KEY (idparent)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: sales_agreement 
 */

ALTER TABLE sales_agreement ADD CONSTRAINT FK__sales_agr__idagr__57B2EEB2 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE sales_agreement ADD CONSTRAINT FK__sales_agr__idcus__56BECA79 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idcustomer)
go

ALTER TABLE sales_agreement ADD CONSTRAINT FK__sales_agr__idint__55CAA640 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: sales_booking 
 */

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idfea__5A8F5B5D 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idint__5B837F96 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idpro__599B3724 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE sales_booking ADD CONSTRAINT FK__sales_boo__idreq__58A712EB 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
go


/* 
 * TABLE: sales_broker 
 */

ALTER TABLE sales_broker ADD CONSTRAINT FK__sales_bro__idbro__5D6BC808 
    FOREIGN KEY (idbrotyp)
    REFERENCES broker_type(idbrotyp)
go

ALTER TABLE sales_broker ADD CONSTRAINT FK__sales_bro__idpar__5C77A3CF 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go


/* 
 * TABLE: sales_order 
 */

ALTER TABLE sales_order ADD CONSTRAINT FK__sales_ord__idord__5E5FEC41 
    FOREIGN KEY (idorder)
    REFERENCES customer_order(idorder)
go

ALTER TABLE sales_order ADD CONSTRAINT FK__sales_ord__idsal__5F54107A 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: sales_point 
 */

ALTER TABLE sales_point ADD CONSTRAINT FK__sales_poi__idint__604834B3 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: sales_unit_leasing 
 */

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idlea__613C58EC 
    FOREIGN KEY (idleacom)
    REFERENCES leasing_company(idparrol)
go

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idlsg__650CE9D0 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
go

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idord__6418C597 
    FOREIGN KEY (idorder)
    REFERENCES vehicle_sales_order(idorder)
go

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idreq__6324A15E 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
go

ALTER TABLE sales_unit_leasing ADD CONSTRAINT FK__sales_uni__idsta__62307D25 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: sales_unit_requirement 
 */

ALTER TABLE sales_unit_requirement ADD CONSTRAINT Reforganization3032 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idbil__69D19EED 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idcol__6CAE0B98 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idcus__67E9567B 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idint__6DA22FD1 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idlsg__707E9C7C 
    FOREIGN KEY (idlsgpro)
    REFERENCES leasing_tenor_provide(idlsgpro)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idown__6F8A7843 
    FOREIGN KEY (idowner)
    REFERENCES party(idparty)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idpro__68DD7AB4 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idpro__6BB9E75F 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idreq__66010E09 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idreq__7172C0B5 
    FOREIGN KEY (idrequest)
    REFERENCES vehicle_customer_request(idrequest)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idsal__66F53242 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idsal__6AC5C326 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE sales_unit_requirement ADD CONSTRAINT FK__sales_uni__idsal__6E96540A 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
go


/* 
 * TABLE: salesman 
 */

ALTER TABLE salesman ADD CONSTRAINT FK__salesman__idcoor__735B0927 
    FOREIGN KEY (idcoordinator)
    REFERENCES salesman(idparrol)
go

ALTER TABLE salesman ADD CONSTRAINT FK__salesman__idparr__7266E4EE 
    FOREIGN KEY (idparrol)
    REFERENCES party_role(idparrol)
go


/* 
 * TABLE: service 
 */

ALTER TABLE service ADD CONSTRAINT FK__service__idprodu__75435199 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE service ADD CONSTRAINT FK__service__iduom__744F2D60 
    FOREIGN KEY (iduom)
    REFERENCES uom(iduom)
go


/* 
 * TABLE: service_agreement 
 */

ALTER TABLE service_agreement ADD CONSTRAINT FK__service_a__idagr__763775D2 
    FOREIGN KEY (idagreement)
    REFERENCES agreement(idagreement)
go

ALTER TABLE service_agreement ADD CONSTRAINT FK__service_a__idcus__781FBE44 
    FOREIGN KEY (idcustomer)
    REFERENCES organization_customer(idcustomer)
go

ALTER TABLE service_agreement ADD CONSTRAINT FK__service_a__idint__772B9A0B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: service_history 
 */

ALTER TABLE service_history ADD CONSTRAINT FK__service_h__idveh__7913E27D 
    FOREIGN KEY (idvehicleservicehistory)
    REFERENCES vehicle_service_history(idvehicleservicehistory)
go


/* 
 * TABLE: service_kpb 
 */

ALTER TABLE service_kpb ADD CONSTRAINT FK__service_k__idpro__7A0806B6 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: service_per_motor 
 */

ALTER TABLE service_per_motor ADD CONSTRAINT FK__service_p__idpro__7AFC2AEF 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go


/* 
 * TABLE: service_reminder 
 */

ALTER TABLE service_reminder ADD CONSTRAINT FK__service_r__idcla__7BF04F28 
    FOREIGN KEY (idclaimtype)
    REFERENCES dealer_claim_type(idclaimtype)
go

ALTER TABLE service_reminder ADD CONSTRAINT FK__service_r__idrem__7CE47361 
    FOREIGN KEY (idremindertype)
    REFERENCES dealer_reminder_type(idremindertype)
go

ALTER TABLE service_reminder ADD CONSTRAINT FK__service_r__idveh__7DD8979A 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go


/* 
 * TABLE: service_reminder_event 
 */

ALTER TABLE service_reminder_event ADD CONSTRAINT FK__service_r__idcom__7FC0E00C 
    FOREIGN KEY (idcomevt)
    REFERENCES communication_event(idcomevt)
go

ALTER TABLE service_reminder_event ADD CONSTRAINT FK__service_r__idrem__7ECCBBD3 
    FOREIGN KEY (idreminder)
    REFERENCES service_reminder(idreminder)
go


/* 
 * TABLE: ship_to 
 */

ALTER TABLE ship_to ADD CONSTRAINT FK__ship_to__00B50445 
    FOREIGN KEY (idparty, idroletype)
    REFERENCES party_role_type(idparty, idroletype)
go


/* 
 * TABLE: shipment 
 */

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idfaci__029D4CB7 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idinte__076201D4 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idposa__0579B962 
    FOREIGN KEY (idposaddfro)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idposa__066DDD9B 
    FOREIGN KEY (idposaddto)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idshif__039170F0 
    FOREIGN KEY (idshifro)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idshit__01A9287E 
    FOREIGN KEY (idshityp)
    REFERENCES shipment_type(idshityp)
go

ALTER TABLE shipment ADD CONSTRAINT FK__shipment__idshit__04859529 
    FOREIGN KEY (idshito)
    REFERENCES ship_to(idshipto)
go


/* 
 * TABLE: shipment_billing_item 
 */

ALTER TABLE shipment_billing_item ADD CONSTRAINT FK__shipment___idbil__0856260D 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE shipment_billing_item ADD CONSTRAINT FK__shipment___idshi__094A4A46 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go


/* 
 * TABLE: shipment_incoming 
 */

ALTER TABLE shipment_incoming ADD CONSTRAINT FK__shipment___idshi__0A3E6E7F 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go


/* 
 * TABLE: shipment_item 
 */

ALTER TABLE shipment_item ADD CONSTRAINT FK__shipment___idfea__0C26B6F1 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE shipment_item ADD CONSTRAINT FK__shipment___idpro__0D1ADB2A 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE shipment_item ADD CONSTRAINT FK__shipment___idshi__0B3292B8 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go


/* 
 * TABLE: shipment_outgoing 
 */

ALTER TABLE shipment_outgoing ADD CONSTRAINT FK__shipment___iddri__0F03239C 
    FOREIGN KEY (idparrol)
    REFERENCES driver(idparrol)
go

ALTER TABLE shipment_outgoing ADD CONSTRAINT FK__shipment___idshi__0E0EFF63 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go


/* 
 * TABLE: shipment_package 
 */

ALTER TABLE shipment_package ADD CONSTRAINT FK__shipment___idint__10EB6C0E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE shipment_package ADD CONSTRAINT FK__shipment___idshi__0FF747D5 
    FOREIGN KEY (idshipactyp)
    REFERENCES shipment_package_type(idshipactyp)
go


/* 
 * TABLE: shipment_package_role 
 */

ALTER TABLE shipment_package_role ADD CONSTRAINT FK__shipment___idpac__12D3B480 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE shipment_package_role ADD CONSTRAINT FK__shipment___idpar__13C7D8B9 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE shipment_package_role ADD CONSTRAINT FK__shipment___idrol__11DF9047 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: shipment_package_status 
 */

ALTER TABLE shipment_package_status ADD CONSTRAINT FK__shipment___idpac__15B0212B 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE shipment_package_status ADD CONSTRAINT FK__shipment___idrea__14BBFCF2 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE shipment_package_status ADD CONSTRAINT FK__shipment___idsta__16A44564 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: shipment_receipt 
 */

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idfea__1980B20F 
    FOREIGN KEY (idfeature)
    REFERENCES feature(idfeature)
go

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idord__1A74D648 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idpac__188C8DD6 
    FOREIGN KEY (idpackage)
    REFERENCES shipment_package(idpackage)
go

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idpro__1798699D 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE shipment_receipt ADD CONSTRAINT FK__shipment___idshi__1B68FA81 
    FOREIGN KEY (idshiite)
    REFERENCES shipment_item(idshiite)
go


/* 
 * TABLE: shipment_receipt_role 
 */

ALTER TABLE shipment_receipt_role ADD CONSTRAINT FK__shipment___idpar__1C5D1EBA 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE shipment_receipt_role ADD CONSTRAINT FK__shipment___idrec__1E45672C 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go

ALTER TABLE shipment_receipt_role ADD CONSTRAINT FK__shipment___idrol__1D5142F3 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: shipment_receipt_status 
 */

ALTER TABLE shipment_receipt_status ADD CONSTRAINT FK__shipment___idrea__202DAF9E 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE shipment_receipt_status ADD CONSTRAINT FK__shipment___idrec__2121D3D7 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go

ALTER TABLE shipment_receipt_status ADD CONSTRAINT FK__shipment___idsta__1F398B65 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: shipment_status 
 */

ALTER TABLE shipment_status ADD CONSTRAINT FK__shipment___idrea__23FE4082 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE shipment_status ADD CONSTRAINT FK__shipment___idshi__2215F810 
    FOREIGN KEY (idshipment)
    REFERENCES shipment(idshipment)
go

ALTER TABLE shipment_status ADD CONSTRAINT FK__shipment___idsta__230A1C49 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: spg_claim_detail 
 */

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idbil__29B719D8 
    FOREIGN KEY (idbilite)
    REFERENCES billing_item(idbilite)
go

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idpro__24F264BB 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idrec__25E688F4 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idspg__26DAAD2D 
    FOREIGN KEY (idspgclaim)
    REFERENCES spg_claim(idspgclaim)
go

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idspg__27CED166 
    FOREIGN KEY (idspgclaimtype)
    REFERENCES spg_claim_type(idspgclaimtype)
go

ALTER TABLE spg_claim_detail ADD CONSTRAINT FK__spg_claim__idsta__28C2F59F 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: standard_calendar 
 */

ALTER TABLE standard_calendar ADD CONSTRAINT FK__standard___idcal__2B9F624A 
    FOREIGN KEY (idcalendar)
    REFERENCES base_calendar(idcalendar)
go

ALTER TABLE standard_calendar ADD CONSTRAINT FK__standard___idint__2AAB3E11 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go


/* 
 * TABLE: status_pkb 
 */

ALTER TABLE status_pkb ADD CONSTRAINT FK__status_pk__idsta__2C938683 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: stock_opname 
 */

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idcal__2E7BCEF5 
    FOREIGN KEY (idcalendar)
    REFERENCES standard_calendar(idcalendar)
go

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idfac__2D87AABC 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idint__2F6FF32E 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE stock_opname ADD CONSTRAINT FK__stock_opn__idstk__30641767 
    FOREIGN KEY (idstkopntyp)
    REFERENCES stock_opname_type(idstkopntyp)
go


/* 
 * TABLE: stock_opname_inventory 
 */

ALTER TABLE stock_opname_inventory ADD CONSTRAINT FK__stock_opn__idinv__324C5FD9 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE stock_opname_inventory ADD CONSTRAINT FK__stock_opn__idsto__31583BA0 
    FOREIGN KEY (idstopnite)
    REFERENCES stock_opname_item(idstopnite)
go


/* 
 * TABLE: stock_opname_item 
 */

ALTER TABLE stock_opname_item ADD CONSTRAINT FK__stock_opn__idcon__3434A84B 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE stock_opname_item ADD CONSTRAINT FK__stock_opn__idpro__33408412 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE stock_opname_item ADD CONSTRAINT FK__stock_opn__idstk__3528CC84 
    FOREIGN KEY (idstkop)
    REFERENCES stock_opname(idstkop)
go


/* 
 * TABLE: stock_opname_status 
 */

ALTER TABLE stock_opname_status ADD CONSTRAINT Refstatus_type2424 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE stock_opname_status ADD CONSTRAINT Refreason_type2425 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE stock_opname_status ADD CONSTRAINT FK__stock_opn__idstk__361CF0BD 
    FOREIGN KEY (idstkop)
    REFERENCES stock_opname(idstkop)
go


/* 
 * TABLE: stockopname_item_tm 
 */

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idfix__39ED81A1 
    FOREIGN KEY (idfixedasset)
    REFERENCES fixedasset_tm(idfixedasset)
go

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idflo__371114F6 
    FOREIGN KEY (idfloor)
    REFERENCES floor_tm(idfloor)
go

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idroo__3805392F 
    FOREIGN KEY (idroom)
    REFERENCES room_tm(idroom)
go

ALTER TABLE stockopname_item_tm ADD CONSTRAINT FK__stockopna__idsto__38F95D68 
    FOREIGN KEY (idstockopname)
    REFERENCES stockopname_tm(idstockopname)
go


/* 
 * TABLE: stockopname_tm 
 */

ALTER TABLE stockopname_tm ADD CONSTRAINT FK__stockopna__statu__3AE1A5DA 
    FOREIGN KEY (status)
    REFERENCES stockopname_status_tp(idstockopnamestatus)
go


/* 
 * TABLE: suggest_part 
 */

ALTER TABLE suggest_part ADD CONSTRAINT FK__suggest_p__idpro__3BD5CA13 
    FOREIGN KEY (idproductmotor)
    REFERENCES motor(idproduct)
go

ALTER TABLE suggest_part ADD CONSTRAINT FK__suggest_p__idpro__3CC9EE4C 
    FOREIGN KEY (idproductpart)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: summary 
 */

ALTER TABLE summary ADD CONSTRAINT FK__summary__idpkb__3DBE1285 
    FOREIGN KEY (idpkb)
    REFERENCES job(idpkb)
go


/* 
 * TABLE: suspect 
 */

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idaddre__4282C7A2 
    FOREIGN KEY (idaddress)
    REFERENCES postal_address(idcontact)
go

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__iddeale__3EB236BE 
    FOREIGN KEY (iddealer)
    REFERENCES internal(idinternal)
go

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idparty__3FA65AF7 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idsales__409A7F30 
    FOREIGN KEY (idsalescoordinator)
    REFERENCES salesman(idparrol)
go

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idsales__418EA369 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE suspect ADD CONSTRAINT FK__suspect__idsuspe__4376EBDB 
    FOREIGN KEY (idsuspecttyp)
    REFERENCES suspect_type(idsuspecttyp)
go


/* 
 * TABLE: suspect_role 
 */

ALTER TABLE suspect_role ADD CONSTRAINT FK__suspect_r__idpar__455F344D 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE suspect_role ADD CONSTRAINT FK__suspect_r__idrol__46535886 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE suspect_role ADD CONSTRAINT FK__suspect_r__idsus__446B1014 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: suspect_status 
 */

ALTER TABLE suspect_status ADD CONSTRAINT FK__suspect_s__idrea__492FC531 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE suspect_status ADD CONSTRAINT FK__suspect_s__idsta__483BA0F8 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE suspect_status ADD CONSTRAINT FK__suspect_s__idsus__47477CBF 
    FOREIGN KEY (idsuspect)
    REFERENCES suspect(idsuspect)
go


/* 
 * TABLE: telecomunication_number 
 */

ALTER TABLE telecomunication_number ADD CONSTRAINT FK__telecomun__idcon__4A23E96A 
    FOREIGN KEY (idcontact)
    REFERENCES contact_mechanism(idcontact)
go


/* 
 * TABLE: unit_accesories_mapper 
 */

ALTER TABLE unit_accesories_mapper ADD CONSTRAINT FK__unit_acce__idmot__4B180DA3 
    FOREIGN KEY (idmotor)
    REFERENCES motor(idproduct)
go


/* 
 * TABLE: unit_deliverable 
 */

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__idcon__4DF47A4E 
    FOREIGN KEY (idconstatyp)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__iddel__4EE89E87 
    FOREIGN KEY (iddeltype)
    REFERENCES deliverable_type(iddeltype)
go

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__idreq__4C0C31DC 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_document_requirement(idreq)
go

ALTER TABLE unit_deliverable ADD CONSTRAINT FK__unit_deli__idvnd__4D005615 
    FOREIGN KEY (idvndstatyp)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: unit_document_message 
 */

ALTER TABLE unit_document_message ADD CONSTRAINT FK__unit_docu__idpar__50D0E6F9 
    FOREIGN KEY (idparent)
    REFERENCES unit_document_message(idmessage)
go

ALTER TABLE unit_document_message ADD CONSTRAINT FK__unit_docu__idpro__4FDCC2C0 
    FOREIGN KEY (idprospect)
    REFERENCES prospect(idprospect)
go

ALTER TABLE unit_document_message ADD CONSTRAINT FK__unit_docu__idreq__51C50B32 
    FOREIGN KEY (idreq)
    REFERENCES sales_unit_requirement(idreq)
go


/* 
 * TABLE: unit_preparation 
 */

ALTER TABLE unit_preparation ADD CONSTRAINT FK__unit_prep__idfac__52B92F6B 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE unit_preparation ADD CONSTRAINT FK__unit_prep__idint__53AD53A4 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE unit_preparation ADD CONSTRAINT FK__unit_prep__idsli__54A177DD 
    FOREIGN KEY (idslip)
    REFERENCES picking_slip(idslip)
go


/* 
 * TABLE: unit_requirement 
 */

ALTER TABLE unit_requirement ADD CONSTRAINT FK__unit_requ__idbra__55959C16 
    FOREIGN KEY (idbranch)
    REFERENCES branch(idinternal)
go

ALTER TABLE unit_requirement ADD CONSTRAINT FK__unit_requ__idpro__5689C04F 
    FOREIGN KEY (idproduct)
    REFERENCES motor(idproduct)
go

ALTER TABLE unit_requirement ADD CONSTRAINT FK__unit_requ__idreq__577DE488 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: unit_shipment_receipt 
 */

ALTER TABLE unit_shipment_receipt ADD CONSTRAINT FK__unit_ship__idrec__587208C1 
    FOREIGN KEY (idreceipt)
    REFERENCES shipment_receipt(idreceipt)
go


/* 
 * TABLE: uom 
 */

ALTER TABLE uom ADD CONSTRAINT FK__uom__iduomtyp__59662CFA 
    FOREIGN KEY (iduomtyp)
    REFERENCES uom_type(iduomtyp)
go


/* 
 * TABLE: uom_conversion 
 */

ALTER TABLE uom_conversion ADD CONSTRAINT FK__uom_conve__iduom__5A5A5133 
    FOREIGN KEY (iduomto)
    REFERENCES uom(iduom)
go

ALTER TABLE uom_conversion ADD CONSTRAINT FK__uom_conve__iduom__5B4E756C 
    FOREIGN KEY (iduomfro)
    REFERENCES uom(iduom)
go


/* 
 * TABLE: user_mediator 
 */

ALTER TABLE user_mediator ADD CONSTRAINT FK__user_medi__idint__5D36BDDE 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE user_mediator ADD CONSTRAINT FK__user_medi__idper__5C4299A5 
    FOREIGN KEY (idperson)
    REFERENCES person(idparty)
go


/* 
 * TABLE: user_mediator_role 
 */

ALTER TABLE user_mediator_role ADD CONSTRAINT FK__user_medi__idpar__5E2AE217 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE user_mediator_role ADD CONSTRAINT FK__user_medi__idrol__60132A89 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE user_mediator_role ADD CONSTRAINT FK__user_medi__idusr__5F1F0650 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
go


/* 
 * TABLE: user_mediator_status 
 */

ALTER TABLE user_mediator_status ADD CONSTRAINT FK__user_medi__idrea__62EF9734 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE user_mediator_status ADD CONSTRAINT FK__user_medi__idsta__61FB72FB 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE user_mediator_status ADD CONSTRAINT FK__user_medi__idusr__61074EC2 
    FOREIGN KEY (idusrmed)
    REFERENCES user_mediator(idusrmed)
go


/* 
 * TABLE: vehicle 
 */

ALTER TABLE vehicle ADD CONSTRAINT FK__vehicle__idcolor__64D7DFA6 
    FOREIGN KEY (idcolor)
    REFERENCES feature(idfeature)
go

ALTER TABLE vehicle ADD CONSTRAINT FK__vehicle__idinter__65CC03DF 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vehicle ADD CONSTRAINT FK__vehicle__idprodu__63E3BB6D 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go


/* 
 * TABLE: vehicle_carrier 
 */

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idpar__699C94C3 
    FOREIGN KEY (idparty)
    REFERENCES person(idparty)
go

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idrea__66C02818 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idrel__67B44C51 
    FOREIGN KEY (idreltyp)
    REFERENCES relation_type(idreltyp)
go

ALTER TABLE vehicle_carrier ADD CONSTRAINT FK__vehicle_c__idveh__68A8708A 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go


/* 
 * TABLE: vehicle_customer_request 
 */

ALTER TABLE vehicle_customer_request ADD CONSTRAINT Refsalesman3053 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE vehicle_customer_request ADD CONSTRAINT FK__vehicle_c__idcus__6A90B8FC 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_customer_request ADD CONSTRAINT FK__vehicle_c__idreq__6B84DD35 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE vehicle_customer_request ADD CONSTRAINT FK__vehicle_c__idsal__6C79016E 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
go


/* 
 * TABLE: vehicle_document_requirement 
 */

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idbil__750E476F 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idint__713DB68B 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idord__6F556E19 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idorg__77EAB41A 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idper__70499252 
    FOREIGN KEY (idpersonowner)
    REFERENCES person(idparty)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idreq__6E6149E0 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idsal__7325FEFD 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idshi__76F68FE1 
    FOREIGN KEY (idshipto)
    REFERENCES ship_to(idshipto)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idsls__6D6D25A7 
    FOREIGN KEY (idslsreq)
    REFERENCES sales_unit_requirement(idreq)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idveh__7231DAC4 
    FOREIGN KEY (idvehregtyp)
    REFERENCES vehicle_registration_type(idvehregtyp)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idveh__741A2336 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE vehicle_document_requirement ADD CONSTRAINT FK__vehicle_d__idven__76026BA8 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: vehicle_identification 
 */

ALTER TABLE vehicle_identification ADD CONSTRAINT FK__vehicle_i__idcus__78DED853 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_identification ADD CONSTRAINT FK__vehicle_i__idveh__79D2FC8C 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: vehicle_purchase_order 
 */

ALTER TABLE vehicle_purchase_order ADD CONSTRAINT FK__vehicle_p__idord__7AC720C5 
    FOREIGN KEY (idorder)
    REFERENCES purchase_order(idorder)
go


/* 
 * TABLE: vehicle_sales_billing 
 */

ALTER TABLE vehicle_sales_billing ADD CONSTRAINT FK__vehicle_s__idbil__7CAF6937 
    FOREIGN KEY (idbilling)
    REFERENCES billing_receipt(idbilling)
go

ALTER TABLE vehicle_sales_billing ADD CONSTRAINT FK__vehicle_s__idcus__7BBB44FE 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_sales_billing ADD CONSTRAINT FK__vehicle_s__idsal__7DA38D70 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go


/* 
 * TABLE: vehicle_sales_order 
 */

ALTER TABLE vehicle_sales_order ADD CONSTRAINT Refvendor3054 
    FOREIGN KEY (idbirojasa)
    REFERENCES vendor(idvendor)
go

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idlea__01741E54 
    FOREIGN KEY (idsales)
    REFERENCES leasing_company(idparrol)
go

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idord__7E97B1A9 
    FOREIGN KEY (idorder)
    REFERENCES sales_order(idorder)
go

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idsal__007FFA1B 
    FOREIGN KEY (idsaletype)
    REFERENCES sale_type(idsaletype)
go

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__idsal__0268428D 
    FOREIGN KEY (idsalesman)
    REFERENCES salesman(idparrol)
go

ALTER TABLE vehicle_sales_order ADD CONSTRAINT FK__vehicle_s__iduni__7F8BD5E2 
    FOREIGN KEY (idunitreq)
    REFERENCES sales_unit_requirement(idreq)
go


/* 
 * TABLE: vehicle_service_history 
 */

ALTER TABLE vehicle_service_history ADD CONSTRAINT FK__vehicle_s__idveh__035C66C6 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: vehicle_work_requirement 
 */

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idcus__08211BE3 
    FOREIGN KEY (idcustomer)
    REFERENCES customer(idcustomer)
go

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idmec__0544AF38 
    FOREIGN KEY (idmechanic)
    REFERENCES mechanic(idparrol)
go

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idreq__04508AFF 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
go

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idveh__0638D371 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go

ALTER TABLE vehicle_work_requirement ADD CONSTRAINT FK__vehicle_w__idveh__072CF7AA 
    FOREIGN KEY (idvehide)
    REFERENCES vehicle_identification(idvehide)
go


/* 
 * TABLE: vendor 
 */

ALTER TABLE vendor ADD CONSTRAINT FK__vendor__idparty__0AFD888E 
    FOREIGN KEY (idparty)
    REFERENCES organization(idparty)
go

ALTER TABLE vendor ADD CONSTRAINT FK__vendor__idrolety__0A096455 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE vendor ADD CONSTRAINT FK__vendor__idvndtyp__0915401C 
    FOREIGN KEY (idvndtyp)
    REFERENCES vendor_type(idvndtyp)
go


/* 
 * TABLE: vendor_order 
 */

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idbil__0DD9F539 
    FOREIGN KEY (idbillto)
    REFERENCES bill_to(idbillto)
go

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idint__0BF1ACC7 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idord__0ECE1972 
    FOREIGN KEY (idorder)
    REFERENCES orders(idorder)
go

ALTER TABLE vendor_order ADD CONSTRAINT FK__vendor_or__idven__0CE5D100 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: vendor_product 
 */

ALTER TABLE vendor_product ADD CONSTRAINT FK__vendor_pr__idint__0FC23DAB 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_product ADD CONSTRAINT FK__vendor_pr__idpro__10B661E4 
    FOREIGN KEY (idproduct)
    REFERENCES product(idproduct)
go

ALTER TABLE vendor_product ADD CONSTRAINT FK__vendor_pr__idven__11AA861D 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: vendor_quotation 
 */

ALTER TABLE vendor_quotation ADD CONSTRAINT Refinternal2415 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_quotation ADD CONSTRAINT Refvendor2416 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go

ALTER TABLE vendor_quotation ADD CONSTRAINT Refquote2417 
    FOREIGN KEY (idquote)
    REFERENCES quote(idquote)
go


/* 
 * TABLE: vendor_relationship 
 */

ALTER TABLE vendor_relationship ADD CONSTRAINT FK__vendor_re__idint__129EAA56 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE vendor_relationship ADD CONSTRAINT FK__vendor_re__idpar__1486F2C8 
    FOREIGN KEY (idparrel)
    REFERENCES party_relationship(idparrel)
go

ALTER TABLE vendor_relationship ADD CONSTRAINT FK__vendor_re__idven__1392CE8F 
    FOREIGN KEY (idvendor)
    REFERENCES vendor(idvendor)
go


/* 
 * TABLE: village 
 */

ALTER TABLE village ADD CONSTRAINT Refgeo_boundary2418 
    FOREIGN KEY (idgeobou)
    REFERENCES geo_boundary(idgeobou)
go


/* 
 * TABLE: we_service_type 
 */

ALTER TABLE we_service_type ADD CONSTRAINT FK__we_servic__idpro__17635F73 
    FOREIGN KEY (idproduct)
    REFERENCES service(idproduct)
go

ALTER TABLE we_service_type ADD CONSTRAINT FK__we_servic__idwet__166F3B3A 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go


/* 
 * TABLE: we_type_good_standard 
 */

ALTER TABLE we_type_good_standard ADD CONSTRAINT FK__we_type_g__idpro__194BA7E5 
    FOREIGN KEY (idproduct)
    REFERENCES good(idproduct)
go

ALTER TABLE we_type_good_standard ADD CONSTRAINT FK__we_type_g__idwet__185783AC 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go


/* 
 * TABLE: work_effort 
 */

ALTER TABLE work_effort ADD CONSTRAINT FK__work_effo__idfac__1A3FCC1E 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE work_effort ADD CONSTRAINT FK__work_effo__idwet__1B33F057 
    FOREIGN KEY (idwetyp)
    REFERENCES we_type(idwetyp)
go


/* 
 * TABLE: work_effort_payment 
 */

ALTER TABLE work_effort_payment ADD CONSTRAINT FK__work_effo__payme__1D1C38C9 
    FOREIGN KEY (payments_id)
    REFERENCES payment_application(idpayapp)
go

ALTER TABLE work_effort_payment ADD CONSTRAINT FK__work_effo__work___1C281490 
    FOREIGN KEY (work_efforts_id)
    REFERENCES work_effort(idwe)
go


/* 
 * TABLE: work_effort_role 
 */

ALTER TABLE work_effort_role ADD CONSTRAINT FK__work_effo__idpar__1F04813B 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE work_effort_role ADD CONSTRAINT FK__work_effo__idrol__1FF8A574 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go

ALTER TABLE work_effort_role ADD CONSTRAINT FK__work_effor__idwe__1E105D02 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
go


/* 
 * TABLE: work_effort_status 
 */

ALTER TABLE work_effort_status ADD CONSTRAINT FK__work_effo__idrea__22D5121F 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE work_effort_status ADD CONSTRAINT FK__work_effo__idsta__21E0EDE6 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go

ALTER TABLE work_effort_status ADD CONSTRAINT FK__work_effor__idwe__20ECC9AD 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
go


/* 
 * TABLE: work_order 
 */

ALTER TABLE work_order ADD CONSTRAINT FK__work_orde__idreq__23C93658 
    FOREIGN KEY (idreq)
    REFERENCES vehicle_work_requirement(idreq)
go


/* 
 * TABLE: work_order_booking 
 */

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idbok__25B17ECA 
    FOREIGN KEY (idboktyp)
    REFERENCES booking_type(idboktyp)
go

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idboo__24BD5A91 
    FOREIGN KEY (idbooslo)
    REFERENCES booking_slot(idbooslo)
go

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idcus__288DEB75 
    FOREIGN KEY (idcustomer)
    REFERENCES personal_customer(idcustomer)
go

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__ideve__26A5A303 
    FOREIGN KEY (idevetyp)
    REFERENCES event_type(idevetyp)
go

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idint__2799C73C 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE work_order_booking ADD CONSTRAINT FK__work_orde__idveh__29820FAE 
    FOREIGN KEY (idvehicle)
    REFERENCES vehicle(idvehicle)
go


/* 
 * TABLE: work_order_booking_role 
 */

ALTER TABLE work_order_booking_role ADD CONSTRAINT FK__work_orde__idboo__2A7633E7 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
go

ALTER TABLE work_order_booking_role ADD CONSTRAINT FK__work_orde__idpar__2B6A5820 
    FOREIGN KEY (idparty)
    REFERENCES party(idparty)
go

ALTER TABLE work_order_booking_role ADD CONSTRAINT FK__work_orde__idrol__2C5E7C59 
    FOREIGN KEY (idroletype)
    REFERENCES role_type(idroletype)
go


/* 
 * TABLE: work_order_booking_status 
 */

ALTER TABLE work_order_booking_status ADD CONSTRAINT FK__work_orde__idboo__2D52A092 
    FOREIGN KEY (idbooking)
    REFERENCES work_order_booking(idbooking)
go

ALTER TABLE work_order_booking_status ADD CONSTRAINT FK__work_orde__idrea__2F3AE904 
    FOREIGN KEY (idreason)
    REFERENCES reason_type(idreason)
go

ALTER TABLE work_order_booking_status ADD CONSTRAINT FK__work_orde__idsta__2E46C4CB 
    FOREIGN KEY (idstatustype)
    REFERENCES status_type(idstatustype)
go


/* 
 * TABLE: work_product_req 
 */

ALTER TABLE work_product_req ADD CONSTRAINT FK__work_prod__idreq__302F0D3D 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: work_requirement 
 */

ALTER TABLE work_requirement ADD CONSTRAINT FK__work_requ__idreq__31233176 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: work_requirement_services 
 */

ALTER TABLE work_requirement_services ADD CONSTRAINT FK__work_requ__idreq__321755AF 
    FOREIGN KEY (idreq)
    REFERENCES work_requirement(idreq)
go

ALTER TABLE work_requirement_services ADD CONSTRAINT FK__work_requi__idwe__330B79E8 
    FOREIGN KEY (idwe)
    REFERENCES work_service_requirement(idwe)
go


/* 
 * TABLE: work_service_requirement 
 */

ALTER TABLE work_service_requirement ADD CONSTRAINT FK__work_servi__idwe__33FF9E21 
    FOREIGN KEY (idwe)
    REFERENCES work_effort(idwe)
go


