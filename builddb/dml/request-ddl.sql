/*
 * ER/Studio Data Architect SQL Code Generation
 * Project :      mpm-model-tuning.DM1
 *
 * Date Created : Thursday, March 08, 2018 08:12:26
 * Target DBMS : Microsoft SQL Server 2012
 */

CREATE TYPE DOM_ADDRESS FROM varchar(60) NOT NULL
go

CREATE TYPE DOM_BLOB FROM image NULL
go

CREATE TYPE DOM_BOOLEAN FROM varchar(5) NULL
go

CREATE TYPE DOM_CLOB FROM ntext NULL
go

CREATE TYPE DOM_DATE FROM date NULL
go

CREATE TYPE DOM_DATEFROM FROM datetime NULL
go

CREATE TYPE DOM_DATETHRU FROM datetime NULL
go

CREATE TYPE DOM_DATETIME FROM datetime NULL
go

CREATE TYPE DOM_DESCRIPTION FROM varchar(50) NULL
go

CREATE TYPE DOM_FLAG FROM varchar(1) NULL
go

CREATE TYPE DOM_ID FROM varchar(30) NULL
go

CREATE TYPE DOM_IDENTITY FROM int NOT NULL
go

CREATE TYPE DOM_INTEGER FROM int NULL
go

CREATE TYPE DOM_LONG FROM bigint NULL
go

CREATE TYPE DOM_LONGDESC FROM varchar(300) NULL
go

CREATE TYPE DOM_MONEY FROM decimal(18,4) NULL
go

CREATE TYPE DOM_NAME FROM varchar(30) NULL
go

CREATE TYPE DOM_QTY FROM decimal(14,4) NULL
go

CREATE TYPE DOM_SHORTID FROM varchar(10) NULL
go

CREATE TYPE DOM_UUID FROM UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID()
go

/* 
 * TABLE: container 
 */

CREATE TABLE container(
    idcontainer    DOM_UUID           NOT NULL,
    idfacility     DOM_UUID           NULL,
    idcontyp       DOM_IDENTITY       NULL,
    code           DOM_ID             NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idcontainer)
)
go



IF OBJECT_ID('container') IS NOT NULL
    PRINT '<<< CREATED TABLE container >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE container >>>'
go

/* 
 * TABLE: facility 
 */

CREATE TABLE facility(
    idfacility        DOM_UUID           NOT NULL,
    idfacilitytype    DOM_IDENTITY       NULL,
    idpartof          DOM_UUID           NULL,
    idfa              DOM_IDENTITY       NULL,
    code              DOM_ID             NULL,
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idfacility)
)
go



IF OBJECT_ID('facility') IS NOT NULL
    PRINT '<<< CREATED TABLE facility >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility >>>'
go

/* 
 * TABLE: facility_contact_mechanism 
 */

CREATE TABLE facility_contact_mechanism(
    idconmecpur      DOM_UUID        NOT NULL,
    idfacility       DOM_UUID        NULL,
    idcontact        DOM_UUID        NULL,
    idpurposetype    DOM_IDENTITY    NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idconmecpur)
)
go



IF OBJECT_ID('facility_contact_mechanism') IS NOT NULL
    PRINT '<<< CREATED TABLE facility_contact_mechanism >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility_contact_mechanism >>>'
go

/* 
 * TABLE: facility_status 
 */

CREATE TABLE facility_status(
    idstatus        DOM_UUID        NOT NULL,
    idfacility      DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('facility_status') IS NOT NULL
    PRINT '<<< CREATED TABLE facility_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility_status >>>'
go

/* 
 * TABLE: facility_type 
 */

CREATE TABLE facility_type(
    idfacilitytype    DOM_IDENTITY       IDENTITY(1,1),
    description       DOM_DESCRIPTION    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idfacilitytype)
)
go



IF OBJECT_ID('facility_type') IS NOT NULL
    PRINT '<<< CREATED TABLE facility_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE facility_type >>>'
go

/* 
 * TABLE: fixed_asset 
 */

CREATE TABLE fixed_asset(
    idfa           DOM_IDENTITY       IDENTITY(1,1),
    idfatype       DOM_IDENTITY       NULL,
    iduom          DOM_ID             NULL,
    name           DOM_NAME           NULL,
    dtacquired     DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PK400 PRIMARY KEY CLUSTERED (idfa)
)
go



IF OBJECT_ID('fixed_asset') IS NOT NULL
    PRINT '<<< CREATED TABLE fixed_asset >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE fixed_asset >>>'
go

/* 
 * TABLE: internal 
 */

CREATE TABLE internal(
    idinternal    DOM_ID         NOT NULL,
    idroletype    DOM_INTEGER    NULL,
    idparty       DOM_UUID       NULL,
    selfsender    DOM_BOOLEAN    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idinternal)
)
go



IF OBJECT_ID('internal') IS NOT NULL
    PRINT '<<< CREATED TABLE internal >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE internal >>>'
go

/* 
 * TABLE: inventory_item 
 */

CREATE TABLE inventory_item(
    idinvite        DOM_UUID        NOT NULL,
    idowner         DOM_UUID        NULL,
    idproduct       DOM_ID          NULL,
    idfacility      DOM_UUID        NULL,
    idcontainer     DOM_UUID        NULL,
    idreceipt       DOM_UUID        NULL,
    idfeature       DOM_IDENTITY    NULL,
    idfa            DOM_IDENTITY    NULL,
    dtcreated       DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    qty             DOM_QTY         NULL,
    qtybooking      DOM_QTY         NULL,
    idmachine       DOM_ID          NULL,
    idframe         DOM_ID          NULL,
    yearassembly    DOM_INTEGER     NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idinvite)
)
go



IF OBJECT_ID('inventory_item') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_item >>>'
go

/* 
 * TABLE: inventory_movement 
 */

CREATE TABLE inventory_movement(
    idslip      DOM_UUID        NOT NULL,
    dtcreate    DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    slipnum     DOM_ID          NULL,
    CONSTRAINT PK183 PRIMARY KEY NONCLUSTERED (idslip)
)
go



IF OBJECT_ID('inventory_movement') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_movement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_movement >>>'
go

/* 
 * TABLE: inventory_movement_status 
 */

CREATE TABLE inventory_movement_status(
    idstatus        DOM_UUID        NOT NULL,
    idslip          DOM_UUID        NULL,
    idstatustype    DOM_INTEGER     NULL,
    idreason        DOM_INTEGER     NULL,
    reason          DOM_LONGDESC    NULL,
    dtfrom          DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK182 PRIMARY KEY NONCLUSTERED (idstatus)
)
go



IF OBJECT_ID('inventory_movement_status') IS NOT NULL
    PRINT '<<< CREATED TABLE inventory_movement_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE inventory_movement_status >>>'
go

/* 
 * TABLE: moving_slip 
 */

CREATE TABLE moving_slip(
    idslip             DOM_UUID    NOT NULL,
    idinviteto         DOM_UUID    NULL,
    idcontainerfrom    DOM_UUID    NULL,
    idcontainerto      DOM_UUID    NULL,
    idproduct          DOM_ID      NULL,
    idfacilityto       DOM_UUID    NULL,
    idfacilityfrom     DOM_UUID    NULL,
    idinvite           DOM_UUID    NULL,
    idreqitem          DOM_UUID    NULL,
    qty                DOM_QTY     NULL,
    CONSTRAINT PK181 PRIMARY KEY NONCLUSTERED (idslip)
)
go



IF OBJECT_ID('moving_slip') IS NOT NULL
    PRINT '<<< CREATED TABLE moving_slip >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE moving_slip >>>'
go

/* 
 * TABLE: party_facility_purpose 
 */

CREATE TABLE party_facility_purpose(
    idparfacpur      DOM_UUID        NOT NULL,
    idparty          DOM_UUID        NULL,
    idpurposetype    DOM_IDENTITY    NULL,
    idfacility       DOM_UUID        NULL,
    dtfrom           DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru           DOM_DATETHRU    DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PK658 PRIMARY KEY CLUSTERED (idparfacpur)
)
go



IF OBJECT_ID('party_facility_purpose') IS NOT NULL
    PRINT '<<< CREATED TABLE party_facility_purpose >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE party_facility_purpose >>>'
go

/* 
 * TABLE: picking_slip 
 */

CREATE TABLE picking_slip(
    idslip              DOM_UUID       NOT NULL,
    idinvite            DOM_UUID       NULL,
    idordite            DOM_UUID       NULL,
    idshipto            DOM_ID         NULL,
    qty                 DOM_QTY        NULL,
    acc1                DOM_BOOLEAN    NULL,
    acc2                DOM_BOOLEAN    NULL,
    acc3                DOM_BOOLEAN    NULL,
    acc4                DOM_BOOLEAN    NULL,
    acc5                DOM_BOOLEAN    NULL,
    acc6                DOM_BOOLEAN    NULL,
    acc7                DOM_BOOLEAN    NULL,
    promat1             DOM_BOOLEAN    NULL,
    promat2             DOM_BOOLEAN    NULL,
    promatdirectgift    DOM_BOOLEAN    NULL,
    idinternalmecha     DOM_UUID       NULL,
    idexternalmecha     DOM_UUID       NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idslip)
)
go



IF OBJECT_ID('picking_slip') IS NOT NULL
    PRINT '<<< CREATED TABLE picking_slip >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE picking_slip >>>'
go

/* 
 * TABLE: prospect 
 */

CREATE TABLE prospect(
    idprospect            DOM_UUID        NOT NULL,
    idbroker              DOM_UUID        NULL,
    idprosou              DOM_INTEGER     NULL,
    idevetyp              DOM_IDENTITY    NULL,
    idfacility            DOM_UUID        NULL,
    idsalesman            DOM_UUID        NULL,
    idsuspect             DOM_UUID        NULL,
    idsalescoordinator    DOM_UUID        NULL,
    idinternal            DOM_ID          NULL,
    prospectnumber        DOM_ID          NULL,
    prospectcount         DOM_INTEGER     NULL,
    eveloc                varchar(600)    NULL,
    dtfollowup            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idprospect)
)
go



IF OBJECT_ID('prospect') IS NOT NULL
    PRINT '<<< CREATED TABLE prospect >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE prospect >>>'
go

/* 
 * TABLE: request 
 */

CREATE TABLE request(
    idrequest      DOM_UUID           NOT NULL,
    idreqtype      DOM_INTEGER        NULL,
    idinternal     DOM_ID             NULL,
    reqnum         DOM_ID             NULL,
    dtcreate       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtrequest      DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request') IS NOT NULL
    PRINT '<<< CREATED TABLE request >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request >>>'
go

/* 
 * TABLE: request_item 
 */

CREATE TABLE request_item(
    idreqitem      DOM_UUID           NOT NULL,
    idrequest      DOM_UUID           NULL,
    idproduct      DOM_ID             NULL,
    idfeature      DOM_IDENTITY       NULL,
    seq            DOM_INTEGER        NULL,
    description    DOM_DESCRIPTION    NULL,
    qtyreq         DOM_QTY            NULL,
    qtytransfer    DOM_QTY            NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idreqitem)
)
go



IF OBJECT_ID('request_item') IS NOT NULL
    PRINT '<<< CREATED TABLE request_item >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_item >>>'
go

/* 
 * TABLE: request_product 
 */

CREATE TABLE request_product(
    idrequest         DOM_UUID    NOT NULL,
    idfacilityfrom    DOM_UUID    NULL,
    idfacilityto      DOM_UUID    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request_product') IS NOT NULL
    PRINT '<<< CREATED TABLE request_product >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_product >>>'
go

/* 
 * TABLE: request_requirement 
 */

CREATE TABLE request_requirement(
    idrequest        DOM_UUID    NOT NULL,
    idrequirement    DOM_UUID    NULL,
    qty              DOM_QTY     NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('request_requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE request_requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_requirement >>>'
go

/* 
 * TABLE: request_role 
 */

CREATE TABLE request_role(
    idrole        DOM_UUID           NOT NULL,
    idrequest     DOM_UUID           NULL,
    idroletype    DOM_INTEGER        NULL,
    idparty       DOM_UUID           NULL,
    username      DOM_DESCRIPTION    NULL,
    dtfrom        DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru        DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idrole)
)
go



IF OBJECT_ID('request_role') IS NOT NULL
    PRINT '<<< CREATED TABLE request_role >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_role >>>'
go

/* 
 * TABLE: request_status 
 */

CREATE TABLE request_status(
    idstatus        binary(16)         NOT NULL,
    idrequest       DOM_UUID           NULL,
    idreason        DOM_INTEGER        NULL,
    idstatustype    DOM_INTEGER        NULL,
    reason          DOM_DESCRIPTION    NULL,
    dtfrom          DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtthru          DOM_DATETHRU       DEFAULT '9999-12-31 23:59:59' NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idstatus)
)
go



IF OBJECT_ID('request_status') IS NOT NULL
    PRINT '<<< CREATED TABLE request_status >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_status >>>'
go

/* 
 * TABLE: request_type 
 */

CREATE TABLE request_type(
    idreqtype      DOM_INTEGER        NOT NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY CLUSTERED (idreqtype)
)
go



IF OBJECT_ID('request_type') IS NOT NULL
    PRINT '<<< CREATED TABLE request_type >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE request_type >>>'
go

/* 
 * TABLE: requirement 
 */

CREATE TABLE requirement(
    idreq                DOM_UUID           NOT NULL,
    idfacility           DOM_UUID           NULL,
    idparentreq          DOM_UUID           NULL,
    idreqtyp             DOM_IDENTITY       NULL,
    requirementnumber    DOM_ID             NULL,
    description          DOM_DESCRIPTION    NULL,
    dtcreate             DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    dtrequired           DOM_DATE           NULL,
    budget               DOM_MONEY          NULL,
    qty                  DOM_QTY            NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idreq)
)
go



IF OBJECT_ID('requirement') IS NOT NULL
    PRINT '<<< CREATED TABLE requirement >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE requirement >>>'
go

/* 
 * TABLE: shipment 
 */

CREATE TABLE shipment(
    idshipment        DOM_UUID           NOT NULL,
    idshityp          DOM_IDENTITY       NULL,
    idfacility        DOM_UUID           NULL,
    idposaddfro       DOM_UUID           NULL,
    idposaddto        DOM_UUID           NULL,
    idshito           DOM_ID             NULL,
    idshifro          DOM_ID             NULL,
    shipmentnumber    DOM_ID             NULL,
    description       DOM_DESCRIPTION    NULL,
    dtschedulle       DOM_DATEFROM       DEFAULT CURRENT_TIMESTAMP NULL,
    reason            DOM_LONGDESC       NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idshipment)
)
go



IF OBJECT_ID('shipment') IS NOT NULL
    PRINT '<<< CREATED TABLE shipment >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE shipment >>>'
go

/* 
 * TABLE: stock_opname 
 */

CREATE TABLE stock_opname(
    idstkop              DOM_UUID        NOT NULL,
    idstkopntyp          DOM_INTEGER     NULL,
    idinternal           DOM_ID          NULL,
    idfacility           DOM_UUID        NULL,
    idcalendar           DOM_IDENTITY    NULL,
    dtcreated            DOM_DATEFROM    DEFAULT CURRENT_TIMESTAMP NULL,
    stockopnamenumber    DOM_ID          NULL,
    CONSTRAINT PK658 PRIMARY KEY CLUSTERED (idstkop)
)
go



IF OBJECT_ID('stock_opname') IS NOT NULL
    PRINT '<<< CREATED TABLE stock_opname >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE stock_opname >>>'
go

/* 
 * TABLE: vehicle_customer_request 
 */

CREATE TABLE vehicle_customer_request(
    idrequest        DOM_UUID    NOT NULL,
    idcustomer       DOM_ID      NULL,
    customerorder    DOM_ID      NULL,
    CONSTRAINT PK790 PRIMARY KEY CLUSTERED (idrequest)
)
go



IF OBJECT_ID('vehicle_customer_request') IS NOT NULL
    PRINT '<<< CREATED TABLE vehicle_customer_request >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE vehicle_customer_request >>>'
go

/* 
 * TABLE: work_effort 
 */

CREATE TABLE work_effort(
    idwe           DOM_UUID           NOT NULL,
    idfacility     DOM_UUID           NULL,
    idwetyp        DOM_IDENTITY       NULL,
    name           DOM_NAME           NULL,
    description    DOM_DESCRIPTION    NULL,
    CONSTRAINT PRIMARY PRIMARY KEY NONCLUSTERED (idwe)
)
go



IF OBJECT_ID('work_effort') IS NOT NULL
    PRINT '<<< CREATED TABLE work_effort >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE work_effort >>>'
go

/* 
 * INDEX: idordite 
 */

CREATE INDEX idordite ON picking_slip(idordite)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('picking_slip') AND name='idordite')
    PRINT '<<< CREATED INDEX picking_slip.idordite >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX picking_slip.idordite >>>'
go

/* 
 * INDEX: idinvite 
 */

CREATE INDEX idinvite ON picking_slip(idinvite)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('picking_slip') AND name='idinvite')
    PRINT '<<< CREATED INDEX picking_slip.idinvite >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX picking_slip.idinvite >>>'
go

/* 
 * TABLE: container 
 */

ALTER TABLE container ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: facility 
 */

ALTER TABLE facility ADD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
go

ALTER TABLE facility ADD 
    FOREIGN KEY (idfacilitytype)
    REFERENCES facility_type(idfacilitytype)
go

ALTER TABLE facility ADD 
    FOREIGN KEY (idpartof)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: facility_contact_mechanism 
 */

ALTER TABLE facility_contact_mechanism ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: facility_status 
 */

ALTER TABLE facility_status ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go


/* 
 * TABLE: inventory_item 
 */

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idcontainer)
    REFERENCES container(idcontainer)
go

ALTER TABLE inventory_item ADD 
    FOREIGN KEY (idfa)
    REFERENCES fixed_asset(idfa)
go


/* 
 * TABLE: inventory_movement_status 
 */

ALTER TABLE inventory_movement_status ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

/* 
 * TABLE: moving_slip 
 */

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idreqitem)
    REFERENCES request_item(idreqitem)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idinviteto)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idcontainerfrom)
    REFERENCES container(idcontainer)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idcontainerto)
    REFERENCES container(idcontainer)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
go

ALTER TABLE moving_slip ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go


/* 
 * TABLE: party_facility_purpose 
 */

ALTER TABLE party_facility_purpose ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

/* 
 * TABLE: picking_slip 
 */

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idinvite)
    REFERENCES inventory_item(idinvite)
go

ALTER TABLE picking_slip ADD 
    FOREIGN KEY (idslip)
    REFERENCES inventory_movement(idslip)
go

/* 
 * TABLE: prospect 
 */

ALTER TABLE prospect ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE prospect ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

/* 
 * TABLE: request 
 */

ALTER TABLE request ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

ALTER TABLE request ADD 
    FOREIGN KEY (idreqtype)
    REFERENCES request_type(idreqtype)
go


/* 
 * TABLE: request_item 
 */

ALTER TABLE request_item ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

/* 
 * TABLE: request_product 
 */

ALTER TABLE request_product ADD 
    FOREIGN KEY (idfacilityfrom)
    REFERENCES facility(idfacility)
go

ALTER TABLE request_product ADD 
    FOREIGN KEY (idfacilityto)
    REFERENCES facility(idfacility)
go

ALTER TABLE request_product ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_requirement 
 */

ALTER TABLE request_requirement ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go

ALTER TABLE request_requirement ADD 
    FOREIGN KEY (idrequirement)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: request_role 
 */

ALTER TABLE request_role ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: request_status 
 */

ALTER TABLE request_status ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: requirement 
 */

ALTER TABLE requirement ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE requirement ADD 
    FOREIGN KEY (idparentreq)
    REFERENCES requirement(idreq)
go


/* 
 * TABLE: shipment 
 */

ALTER TABLE shipment ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

/* 
 * TABLE: stock_opname 
 */

ALTER TABLE stock_opname ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

ALTER TABLE stock_opname ADD 
    FOREIGN KEY (idinternal)
    REFERENCES internal(idinternal)
go

/* 
 * TABLE: vehicle_customer_request 
 */

ALTER TABLE vehicle_customer_request ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go


/* 
 * TABLE: work_effort 
 */

ALTER TABLE work_effort ADD 
    FOREIGN KEY (idfacility)
    REFERENCES facility(idfacility)
go

