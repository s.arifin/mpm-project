UID UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID()

use sdms;

SELECT refkey, count(*) FROM [sdms].[dbo].[feature] group by refkey
having count(*) > 1;

select * from feature where refkey in ('AG', 'BG', 'KC', 'VO');

delete from feature where idfeature in (118, 171, 231, 237);

BEGIN TRANSACTION;
INSERT INTO shipment_package_type (idshipactyp, description) VALUES (101, 'Init Stock Unit');
INSERT INTO shipment_package_type (idshipactyp, description) VALUES (102, 'Init Stock Product');
SET IDENTITY_INSERT order_type ON;
INSERT INTO order_type (idordtyp, description) VALUES (9999, 'Init Order');
SET IDENTITY_INSERT order_type OFF;
COMMIT;

use sdms;
alter table customer add idmpm DOM_ID;

alter table sales_unit_requirement add podate DATETIME;

alter table sales_unit_requirement add ponumber DOM_DESCRIPTION;

ALTER TABLE vehicle_sales_order 
	ADD idsalesman DOM_UUID;
	
ALTER TABLE vehicle_sales_order
	ADD FOREIGN KEY (idsalesman) REFERENCES salesman (idparrol);


Alter table vehicle_sales_order add idbirojasa DOM_ID;

USE master;
ALTER DATABASE sdms SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DROP DATABASE sdms;

USE [sdms-merge];

ALTER TABLE vehicle_sales_order ADD idsalesman DOM_UUID
go
alter table orders_status ADD idordite DOM_UUID
go
ALTER TABLE orders_status ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go
CREATE TABLE requirement_order_item(
    idreqordite    DOM_UUID    NOT NULL,
    idreq          DOM_UUID    NULL,
    idordite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idreqordite)
)
go
ALTER TABLE requirement_order_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go
ALTER TABLE requirement_order_item ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go
alter table leasing_company add idleasingahm DOM_ID
go
alter table leasing_company add idleasingfincoy DOM_ID
go
alter table salesman add ahmcode DOM_ID
go
alter table orders_status add description DOM_DESCRIPTION
go
alter table vehicle add servicebooknumber DOM_ID
go
alter table vehicle add merkoil DOM_DESCRIPTION
go
alter table billing_item add cashdiscamount DOM_MONEY
go
alter table billing_item add dppamount DOM_MONEY
go
alter table billing_item add dppduedt DOM_DATE
go 
alter table billing_item add taxduedt DOM_DATE
go 
alter table billing_item add disproplineamount DOM_MONEY
go
alter table billing_item add discdtlieamount DOM_MONEY
go
alter table billing_item add discdtlitemamoun DOM_MONEY
go
alter table sales_unit_requirement add idrequest DOM_UUID
go
ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go
alter table billing_item add seq DOM_INTEGER
go
alter table approval add reason DOM_LONGDESC
go
alter table billing_item add seq DOM_INTEGER
go
alter table customer add idcustomertype DOM_INTEGER
go
alter table customer_order add idvehicle DOM_UUID
go
alter table mechanic add passwordmechanic DOM_DESCRIPTION
go
alter table part_sales_order add total DOM_MONEY
go
alter table part_sales_order add qty DOM_QTY
go
alter table purchase_order add reason DOM_LONGDESC
go
alter table rem_part add minimumstock DOM_QTY
go
alter table rem_part add safetystock DOM_QTY
go
alter table rem_part add qtysuggest DOM_QTY
go
alter table request_item add qtyorder DOM_QTY
go
alter table return_purchase_order add idissue DOM_INTEGER
go
alter table vehicle add fuel DOM_ID
go
alter table service add merkoil DOM_ID
go
alter table service add servicebooknumber DOM_ID
go
alter table vehicle_identification add dealerpurchase DOM_DESCRIPTION
go
alter table vehicle_identification add dtpurchase DOM_DATE
go
USE [sdms-merge];

ALTER TABLE vehicle_sales_order ADD idsalesman DOM_UUID
go
alter table orders_status ADD idordite DOM_UUID
go
ALTER TABLE orders_status ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go
CREATE TABLE requirement_order_item(
    idreqordite    DOM_UUID    NOT NULL,
    idreq          DOM_UUID    NULL,
    idordite       DOM_UUID    NULL,
    qty            DOM_QTY     NULL,
    PRIMARY KEY CLUSTERED (idreqordite)
)
go
ALTER TABLE requirement_order_item ADD 
    FOREIGN KEY (idordite)
    REFERENCES order_item(idordite)
go
ALTER TABLE requirement_order_item ADD 
    FOREIGN KEY (idreq)
    REFERENCES requirement(idreq)
go
alter table leasing_company add idleasingahm DOM_ID
go
alter table leasing_company add idleasingfincoy DOM_ID
go
alter table salesman add ahmcode DOM_ID
go
alter table orders_status add description DOM_DESCRIPTION
go
alter table vehicle add servicebooknumber DOM_ID
go
alter table vehicle add merkoil DOM_DESCRIPTION
go
alter table billing_item add cashdiscamount DOM_MONEY
go
alter table billing_item add dppamount DOM_MONEY
go
alter table billing_item add dppduedt DOM_DATE
go 
alter table billing_item add taxduedt DOM_DATE
go 
alter table billing_item add disproplineamount DOM_MONEY
go
alter table billing_item add discdtlieamount DOM_MONEY
go
alter table billing_item add discdtlitemamoun DOM_MONEY
go
alter table sales_unit_requirement add idrequest DOM_UUID
go
ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idrequest)
    REFERENCES request(idrequest)
go
alter table billing_item add seq DOM_INTEGER
go
alter table approval add reason DOM_LONGDESC
go
alter table billing_item add seq DOM_INTEGER
go
alter table customer add idcustomertype DOM_INTEGER
go
alter table customer_order add idvehicle DOM_UUID
go
alter table mechanic add passwordmechanic DOM_DESCRIPTION
go
alter table part_sales_order add total DOM_MONEY
go
alter table part_sales_order add qty DOM_QTY
go
alter table purchase_order add reason DOM_LONGDESC
go
alter table rem_part add minimumstock DOM_QTY
go
alter table rem_part add safetystock DOM_QTY
go
alter table rem_part add qtysuggest DOM_QTY
go
alter table request_item add qtyorder DOM_QTY
go
alter table return_purchase_order add idissue DOM_INTEGER
go
alter table vehicle add fuel DOM_ID
go
alter table service add merkoil DOM_ID
go
alter table service add servicebooknumber DOM_ID
go
alter table vehicle_identification add dealerpurchase DOM_DESCRIPTION
go
alter table vehicle_identification add dtpurchase DOM_DATE
go
ALTER TABLE salesman ALTER COLUMN ahmcode DOM_ID
go
ALTER TABLE salesman ALTER COLUMN idsalesman DOM_ID
go
ALTER TABLE salesman ALTER COLUMN coordinator DOM_ID
go
ALTER TABLE leasing_company ALTER COLUMN idleasingfincoy DOM_ID
go
ALTER TABLE leasing_company ALTER COLUMN idleasingahm DOM_ID
go
alter table sales_unit_requirement add idorganizationowner DOM_UUID
go
ALTER TABLE sales_unit_requirement ADD 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
go
alter table vehicle_document_requirement add idorganizationowner DOM_UUID
go
ALTER TABLE vehicle_document_requirement ADD 
    FOREIGN KEY (idorganizationowner)
    REFERENCES organization(idparty)
go
alter table vehicle_customer_request add idsalesbroker DOM_UUID
go
ALTER TABLE vehicle_customer_request ADD 
    FOREIGN KEY (idsalesbroker)
    REFERENCES sales_broker(idparrol)
go


select a.* from person a
 where exists (
 select b.fname, b.lname, b.pob, b.dtob from person b
 where (b.fname = a.fname) and (a.lname = b.lname) and (a.pob = b.pob) and (a.dtob = b.dtob)
group by b.fname, b.lname, b.pob, b.dtob
having count(*) > 1
 )
 or (a.fname like 'guntur' and (a.lname is null or 1=2))
 order by a.fname, a.lname, a.dtob, a.personalidnumber, a.idparty ;
 
 alter table vendor add ismaindealer DOM_BOOLEAN
go
alter table vendor add isbirojasa DOM_BOOLEAN
go